/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // { path: '', redirectTo: 'home', pathMatch: 'full' },
  // { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: '',
    loadChildren: () => import('./pages/slider/slider.module').then(m => m.SliderPageModule)
  },
  // {
  //   path: '',
  //   loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  // },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterPageModule)
  },
  {
    path: 'photo-gallery',
    loadChildren: () => import('./pages/photo-gallery/photo-gallery.module').then(m => m.PhotoGalleryPageModule)
  },
  {
    path: 'join-with-google',
    loadChildren: () => import('./pages/join-with-google/join-with-google.module').then(m => m.JoinWithGooglePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'home-new',
    loadChildren: () => import('./pages/home-new/home-new.module').then(m => m.HomeNewPageModule)
  },
  {
    path: 'group-feed',
    loadChildren: () => import('./pages/group-feed/group-feed.module').then(m => m.GroupFeedPageModule)
  },
  {
    path: 'group-update',
    loadChildren: () => import('./pages/group-update/group-update.module').then(m => m.GroupUpdatePageModule)
  },
  {
    path: 'news-feed',
    loadChildren: () => import('./pages/news-feed/news-feed.module').then(m => m.NewsFeedPageModule)
  },
  {
    path: 'forum-reply',
    loadChildren: () => import('./pages/forum-reply/forum-reply.module').then(m => m.ForumReplyPageModule)
  },
  {
    path: 'single-feed',
    loadChildren: () => import('./pages/single-feed/single-feed.module').then(m => m.SingleFeedPageModule)
  },
  {
    path: 'my-network',
    loadChildren: () => import('./pages/my-network/my-network.module').then(m => m.MyNetworkPageModule)
  },
  {
    path: 'members-activity',
    loadChildren: () => import('./pages/members-activity/members-activity.module').then(m => m.MembersActivityPageModule)
  },
  {
    path: 'my-profile',
    loadChildren: () => import('./pages/my-profile/my-profile.module').then(m => m.MyProfilePageModule)
  },
  {
    path: 'edit-profile',
    loadChildren: () => import('./pages/edit-profile/edit-profile.module').then(m => m.EditProfilePageModule)
  },
  {
    path: 'job-personal',
    loadChildren: () => import('./pages/job-personal/job-personal.module').then(m => m.JobPersonalPageModule)
  },
  {
    path: 'job-presentadd',
    loadChildren: () => import('./pages/job-presentadd/job-presentadd.module').then(m => m.JobPresentaddPageModule)
  },
  {
    path: 'job-education',
    loadChildren: () => import('./pages/job-education/job-education.module').then(m => m.JobEducationPageModule)
  },
  {
    path: 'job-seagoing',
    loadChildren: () => import('./pages/job-seagoing/job-seagoing.module').then(m => m.JobSeagoingPageModule)
  },
  {
    path: 'job-endorsement',
    loadChildren: () => import('./pages/job-endorsement/job-endorsement.module').then(m => m.JobEndorsementPageModule)
  },
  {
    path: 'job-portsea',
    loadChildren: () => import('./pages/job-portsea/job-portsea.module').then(m => m.JobPortseaPageModule)
  },
  {
    path: 'job-authorised',
    loadChildren: () => import('./pages/job-authorised/job-authorised.module').then(m => m.JobAuthorisedPageModule)
  },
  {
    path: 'job-verification',
    loadChildren: () => import('./pages/job-verification/job-verification.module').then(m => m.JobVerificationPageModule)
  },
  {
    path: 'job-competency',
    loadChildren: () => import('./pages/job-competency/job-competency.module').then(m => m.JobCompetencyPageModule)
  },
  {
    path: 'job-preseatraining',
    loadChildren: () => import('./pages/job-preseatraining/job-preseatraining.module').then(m => m.JobPreSeaTrainingPageModule)
  },
  {
    path: 'job-modularcoursedetails',
    loadChildren: () => import('./pages/job-modularcoursedetails/job-modularcoursedetails.module').then(m => m.JobModularcoursedetailsPageModule)
  },
  {
    path: 'job-permenantadd',
    loadChildren: () => import('./pages/job-permenantadd/job-permenantadd.module').then(m => m.JobPermenantaddPageModule)
  },
  {
    path: 'job-physical',
    loadChildren: () => import('./pages/job-physical/job-physical.module').then(m => m.JobPhysicalPageModule)
  },
  {
    path: 'post',
    loadChildren: () => import('./pages/post/post.module').then(m => m.PostPageModule)
  },
  {
    path: 'groups-list',
    loadChildren: () => import('./pages/groups-list/groups-list.module').then(m => m.GroupsListPageModule)
  },
  {
    path: 'group-detail/:id',
    loadChildren: () => import('./pages/group-detail/group-detail.module').then(m => m.GroupDetailPageModule)
  },
  {
    path: 'profile-detail',
    loadChildren: () => import('./pages/profile-detail/profile-detail.module').then(m => m.ProfileDetailPageModule)
  },
  {
    path: 'group-invites/:id',
    loadChildren: () => import('./pages/group-invites/group-invites.module').then(m => m.GroupInvitesPageModule)
  },
  {
    path: 'forum-detail/:id',
    loadChildren: () => import('./pages/forum-detail/forum-detail.module').then(m => m.ForumDetailPageModule)
  },
  {
    path: 'members-profile/:id',
    loadChildren: () => import('./pages/members-profile/members-profile.module').then(m => m.MembersProfilePageModule)
  },
  {
    path: 'member-detail',
    loadChildren: () => import('./pages/member-detail/member-detail.module').then(m => m.MemberDetailPageModule)
  },
  {
    path: 'members-detail/:id',
    loadChildren: () => import('./pages/members-detail/members-detail.module').then(m => m.MembersDetailPageModule)
  },
  {
    path: 'member-profile',
    loadChildren: () => import('./pages/member-profile/member-profile.module').then(m => m.MemberProfilePageModule)
  },
  {
    path: 'members-list',
    loadChildren: () => import('./pages/members-list/members-list.module').then(m => m.MembersListPageModule)
  },
  {
    path: 'activity-list',
    loadChildren: () => import('./pages/activity-list/activity-list.module').then(m => m.ActivityListPageModule)
  },
  {
    path: 'forums-list',
    loadChildren: () => import('./pages/forums-list/forums-list.module').then(m => m.ForumsListPageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./pages/notification/notification.module').then(m => m.NotificationPageModule)
  },
  {
    path: 'jobs',
    loadChildren: () => import('./pages/jobs/jobs.module').then(m => m.JobsPageModule)
  },
  {
    path: 'people-profile',
    loadChildren: () => import('./pages/people-profile/people-profile.module').then(m => m.PeopleProfilePageModule)
  },
  {
    path: 'job-detail',
    loadChildren: () => import('./pages/job-detail/job-detail.module').then(m => m.JobDetailPageModule)
  },
  {
    path: 'share-post',
    loadChildren: () => import('./pages/share-post/share-post.module').then(m => m.SharePostPageModule)
  },
  {
    path: 'new-post',
    loadChildren: () => import('./pages/new-post/new-post.module').then(m => m.NewPostPageModule)
  },
  {
    path: 'manage-network',
    loadChildren: () => import('./pages/manage-network/manage-network.module').then(m => m.ManageNetworkPageModule)
  },
  {
    path: 'manage-detail',
    loadChildren: () => import('./pages/manage-detail/manage-detail.module').then(m => m.ManageDetailPageModule)
  },
  {
    path: 'invitations',
    loadChildren: () => import('./pages/invitations/invitations.module').then(m => m.InvitationsPageModule)
  },
  {
    path: 'create-event',
    loadChildren: () => import('./pages/create-event/create-event.module').then(m => m.CreateEventPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./pages/forgot-password/forgot-password.module').then(m => m.ForgotPasswordPageModule)
  },
  {
    path: 'chatlist',
    loadChildren: () => import('./pages/chatlist/chatlist.module').then(m => m.ChatlistPageModule)
  },
  {
    path: 'chat',
    loadChildren: () => import('./pages/chat/chat.module').then(m => m.ChatPageModule)
  },
  {
    path: 'single-post',
    loadChildren: () => import('./pages/single-post/single-post.module').then(m => m.SinglePostPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

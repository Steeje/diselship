/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/




import { Component } from '@angular/core';
import { NavigationEnd,Router } from '@angular/router';
import { Platform, ModalController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CreateEventPage } from '../app/pages/create-event/create-event.page';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { environment } from 'src/environments/environment';

import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { apiService } from '../app/services/api.service';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  data: any = {};
  closed$ = new Subject<any>();
  showTabs1 = true; // <-- show tabs by default
  public href: string = "";
  public sidemenu = [
    {
      title: 'Home',
      // url: '/home',
      icon: 'home'
    },
    {
      title: 'Firebase',
      // url: '/firebase',
      icon: 'logo-firebase'
    },
    {
      title: 'Chat',
      // url: '/chat-home',
      icon: 'chatbubbles'
    },
    {
      title: 'Slider',
      // url: '/slider-home',
      icon: 'albums'
    },
    {
      title: 'Login',
      // url: '/login-home',
      icon: 'log-in'
    },
  ];
  authState = new BehaviorSubject(false);
  selectTab = 'tab1';
  user = this.api.getCurrentUser();
  constructor(
    
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private modalController: ModalController,
    private oneSignal: OneSignal,
    private api: apiService,
    private storage: Storage,
    
    private router: Router
    // private authenticationService: authenticationService
  ) {
  
    this.initializeApp();
   
  }

  ngOnInit() {
    this.router.events.pipe(
      filter(e => e instanceof NavigationEnd),
      takeUntil(this.closed$)
    ).subscribe(event => {
      if (event['url'] === '/login' || event['url'] === '/register') {
        this.showTabs1 = false; // <-- hide tabs on specific pages
      }
    });
  }
  
  ngOnDestroy() {
    this.closed$.next(); // <-- close subscription when component is destroyed
  }


  tabChange(e) {
    this.selectTab = e.tab;
  }
  initializeApp() {
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        setTimeout(async () => {
          await this.oneSignal.startInit(environment.onesignal.appId, environment.onesignal.googleProjectNumber);
          this.oneSignal.getIds().then((data) => {
            localStorage.setItem('onesignal', data.userId);
          });
          await this.oneSignal.endInit();
        }, 1000);
      }
      this.statusBar.backgroundColorByHexString('#0177B7');
      this.splashScreen.hide();

      this.user.subscribe(user => {
        if (user) {
          // location.href = "/home-new";
         
          this.storage.get('USER_INFO').then(result => {
            if (result != null) {
            console.log( result);
            this.data.id = result.id;
            this.data.nickname = result.user_nicename;
            
            this.data.name=result.first_name+" "+ result.last_name;
            this.data.description = result.description;
            this.data.avatar=result.avatar_urls[24];
            if(typeof result.avatar_urls[24] === "undefined" )
              this.data.avatar=result.avatar_urls['full'];
            this.data.email = result.email;
            console.log(result.first_name);
          
            if(typeof result.first_name === "undefined" ){
          
             
                console.log( user);
                this.data.email = user.user_email;
                this.data.nickname = user.user_nicename;
              
                this.data.name=user.user_display_name;
               
            
              }

              this.data.name= this.truncate(this.data.name,18);
            }
            }).catch(e => {
            console.log('error: '+ e);
            // Handle errors here
            });
            this.showTabs1 = true; 
            this.router.navigate(['members-activity']);
           
        } else {
          this.showTabs1 = false; 
          this.router.navigate(['login']);
        }
      });

    
  


    });
  }
  truncate(source, size) {
    return source.length > size ? source.slice(0, size - 1) + "…" : source;
  }
  myprofile() {
    this.router.navigate(['member-detail']);
  }
  editprofile() {
    this.router.navigate(['edit-profile']);
  }
  logout() {
    this.api.logout();
    this.storage.remove('USER_INFO').then(() => {
      this.router.navigate(['login']);
      this.authState.next(false);
    });
  }
  async goToCreateEvents() {
    const modal = await this.modalController.create({
      component: CreateEventPage
    });
    return await modal.present();
  }
}

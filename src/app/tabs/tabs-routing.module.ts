import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home-new',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/home-new/home-new.module').then(m => m.HomeNewPageModule)
          }
        ]
      },
      {
        path: 'my-network',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/my-network/my-network.module').then(m => m.MyNetworkPageModule)
          }
        ]
      },
      {
        path: 'share-post',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/share-post/share-post.module').then(m => m.SharePostPageModule)
          }
        ]
      },
      {
        path: 'notification',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/notification/notification.module').then(m => m.NotificationPageModule)
          }
        ]
      },
      {
        path: 'jobs',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/jobs/jobs.module').then(m => m.JobsPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/home-new',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home-new',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}

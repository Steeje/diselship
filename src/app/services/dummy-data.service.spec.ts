import { TestBed } from '@angular/core/testing';

import { dummyDataService } from './dummy-data.service';

describe('dummyDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: dummyDataService = TestBed.get(dummyDataService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';


import { BehaviorSubject, from } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Platform } from '@ionic/angular';
import { environment } from '../../environments/environment';
import { map, switchMap, tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
 
const JWT_KEY = 'v+vr@c8f,D+P`Gt/v>t(5|:]Gq5[@]o}9|#`({|>V7d *sLS-j&%h*i-(u5H,&z_';
const ACCESS_TOKEN_KEY = 'my-access-token';
const REFRESH_TOKEN_KEY = 'my-refresh-token';
@Injectable({
  providedIn: 'root'
})
export class apiService {
  token:any="";
  idArray:any = [];
  user_id = "";
  private user = new BehaviorSubject(null);
  isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  currentAccessToken = null;
  url = 'http://localhost:3000';
  constructor(private http: HttpClient, private storage: Storage, private plt: Platform) {
    this.plt.ready().then(() => {
      this.storage.get(JWT_KEY).then(data => {
        if (data) {
          this.user.next(data);
        }
      })
    })
    this.loadToken();
  }
 
  // Load accessToken on startup
  async loadToken() {
    const token = await this.storage.get(ACCESS_TOKEN_KEY );    
    if (token && token.value) {
      this.currentAccessToken = token.value;
      this.isAuthenticated.next(true);
    } else {
      this.isAuthenticated.next(false);
    }
  }
 
  
  groupCreate(groupname, groupdescription) {
  
    const formData = new FormData();
    formData.append('name', groupname);
    formData.append('description', groupdescription);
    // formData.append('creator_id', id);
    return this.http.post(`${environment.apiUrl}/buddyboss/v1/groups`, formData);
  }
  signIn(username, password) {
  
    const formData = new FormData();
    formData.append('username', username);
    formData.append('password', password);
    return this.http.post(`${environment.apiUrl}/jwt-auth/v1/token`, formData).pipe(
      switchMap(data => {
        return from(this.storage.set(JWT_KEY, data));
      }),
      tap(data => {
        this.user.next(data);
      })
    );
  }
  
  signUp( field_1, field_2, field_3,signup_email,signup_password) {

   
    let httpOptions = {
      headers: new HttpHeaders({
        'content-type':'application/json',
        'Accept':'application/json'
      
      })
  };

  
    // const dataJson = {
    //   'signup_email': signup_email,
    //   'signup_password': signup_password,
    //   'field_1': field_1,
    //   'field_2': field_2,
    //   'field_3': field_3
     
    // };
    // console.log('dataJson:', dataJson);
    const formData = new FormData();
    formData.append('signup_email', signup_email);
    formData.append('signup_password', signup_password);
    formData.append('field_1', field_1);
    formData.append('field_2', field_2);
    formData.append('field_3', field_3);
 //   formData.append('data', JSON.stringify(dataJson));

    return this.http.post(`${environment.apiUrl}/buddyboss/v1/signup`,formData
     ).pipe(
      switchMap(data => {
        return from(this.storage.set(JWT_KEY, data));
      }),
      tap(data => {
        this.user.next(data);
      })
    );












    
    // let headers = new HttpHeaders();
    // headers.append("Accept", 'application/json');
    // headers.append('Content-Type', 'application/json' );
  
    
    // return this.http.post(`${environment.apiUrl}/buddyboss/v1/signup`, {  
    //   field_1, 
    //   field_2, 
    //   field_3,
    //   signup_email,
    //   signup_password },
    //    { headers:headers
    // }).pipe(
    //   switchMap(data => {
    //     return from(this.storage.set(JWT_KEY, data));
    //   }),
    //   tap(data => {
    //     this.user.next(data);
    //   })
    // );








    
    // headers.append("Cache-Control", 'no-cache');
    // headers.append("Postman-Token", '<calculated when request is sent>');
    // headers.append("Content-Length", '0');
    // headers.append("Host", '<calculated when request is sent>');
    // headers.append("User-Agent", 'PostmanRuntime/7.26.8');
    // headers.append("Accept-Encoding", 'gzip, deflate, br');
    // headers.append("Connection", 'keep-alive');
    // headers.append("Accept", '*/*');
 
    // headers.append("Accept", 'application/json');
    // headers.append('Content-Type', 'application/json');
    // headers.append('Access-Control-Allow-Origin', '*');


    // return this.http.post(`https://dev-linkedin.dieselship.com/?rest_route=/simple-jwt-login/v1/users`, {  email, password });
  }
  userupdate(id,first_name, email, password, last_name, nick_name, description) {
    if(password!="")
    return this.http.post(`${environment.apiUrl}/wp/v2/users/`+id, { first_name, email, password, last_name, nick_name, description });
    else 
    return this.http.post(`${environment.apiUrl}/wp/v2/users/`+id, { first_name, email, last_name, nick_name, description });
  }
  getuser(id){
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
    
     
      this.token=result.token;
      
    
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);

    return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/members/`+id , { headers:headers
    }).pipe(
      map(data => {
      return data;
  })
  );
  }
  resetPassword(usernameOrEmail) {
    
    return this.http.post(`${environment.apiUrl}/wp/v2/users/lostpassword`, { user_login: usernameOrEmail });
  }
  uploadgroup2(file: File,action,id,url) {
    //   let headers = new HttpHeaders();
    // headers.append("Authorization", 'Bearer '+token);
      const ext = file.name.split('.').pop();
      const formData = new FormData();
      formData.append('file', file, `myimage.${ext}`);
      formData.append( 'action', action );
      formData.append('name', file.name);
   
     
    return this.http.post(`${environment.apiUrl}/buddyboss/v1/groups/`+id+`/`+url, formData);
    }
    uploadgroup(file: Blob, type, name,token,action,id,url) {
        let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+token);
      //  const ext = file.name.split('.').pop();
        const formData = new FormData();
        formData.append('file', file, name+"."+type);
        formData.append( 'action', action );
       
        formData.append('name', name);
     
      
    return this.http.post(`${environment.apiUrl}/buddyboss/v1/groups/`+id+`/`+url, formData);
      }
  uploadImage(blobData, name, ext,id) {
    const formData = new FormData();
    formData.append('file', blobData, `myimage.${ext}`);
    formData.append( 'action', 'bp_avatar_upload' );
    // formData.append('name', name);
 
    return this.http.post(`${environment.apiUrl}/buddyboss/v1/members/`+id+`/avatar`, formData);
  }
  uploadImage2(blobData, name, ext,id) {
    const formData = new FormData();
    formData.append('file', blobData, `myimage.${ext}`);
    formData.append( 'action', 'bp_cover_image_upload' );
    // formData.append('name', name);
 
    return this.http.post(`${environment.apiUrl}/buddyboss/v1/members/`+id+`/cover`, formData);
  }
  uploadImageFile(file: File, id) {
    const ext = file.name.split('.').pop();
    const formData = new FormData();
    formData.append('file', file, `myimage.${ext}`);
    formData.append( 'action', 'bp_avatar_upload' );
    // formData.append('name', file.name);
 
    return this.http.post(`${environment.apiUrl}/buddyboss/v1/members/`+id+`/avatar`, formData);
  }
  uploadImageFile2(file: File, id) {
    const ext = file.name.split('.').pop();
    const formData = new FormData();
    formData.append('file', file, `myimage.${ext}`);
    formData.append( 'action', 'bp_cover_image_upload' );
    // formData.append('name', file.name);
 
    return this.http.post(`${environment.apiUrl}/buddyboss/v1/members/`+id+`/cover`, formData);
  }
  getPrivatePosts() {
    return this.http.get<any[]>(`${environment.apiUrl}/wp/v2/posts?_embed&status=private`).pipe(
      map(data => {
        for (let post of data) {
          if (post['_embedded']['wp:featuredmedia']) {
            post.media_url =
              post['_embedded']['wp:featuredmedia'][0]['media_details'].sizes['medium'].source_url;
          }
        }
        return data;
      })
    );
  }
  retrieveCategories(){
    return this.http.get(`${environment.apiUrl}/wp/v2/categories`).pipe(
      map(data => {
      return data;
  })
  );
  }
  retrievePostsInCategory(categoryId: number){
    return this.http.get(`${environment.apiUrl}/wp/v2/posts?categories=` + categoryId)
    .pipe(
      map(data => {
      return data;
  })
  );
  }
  uploadMedia(file: File) {
  //   let headers = new HttpHeaders();
  // headers.append("Authorization", 'Bearer '+token);
    const ext = file.name.split('.').pop();
    const formData = new FormData();
    formData.append('file', file, `myimage.${ext}`);
   
    formData.append('name', file.name);
 
    return this.http.post(`${environment.apiUrl}/buddyboss/v1/media/upload`, formData);
  }
  uploadMedia1(file: Blob, type, name,token) {
      let headers = new HttpHeaders();
    headers.append("Authorization", 'Bearer '+token);
    //  const ext = file.name.split('.').pop();
      const formData = new FormData();
      formData.append('file', file, name+"."+type);
     
      formData.append('name', name);
   
      return this.http.post(`${environment.apiUrl}/buddyboss/v1/media/upload`, formData, {headers:headers});
    }
  createPost(user_id,bp_media_ids,content,privacy) {
    //   let headers = new HttpHeaders();
    // headers.append("Authorization", 'Bearer '+token);
    
      const formData = new FormData();
      formData.append('component', "activity");
      formData.append('type',"activity_update");
      formData.append('user_id', user_id);
      formData.append('bp_media_ids', bp_media_ids);
      formData.append('content', content);
      formData.append('privacy', privacy);
     
     
     
   
      return this.http.post(`${environment.apiUrl}/buddyboss/v1/activity`, formData);
    }
  photogallery(id, type){
    return this.http.get(`${environment.apiUrl}/buddyboss/v1/media?`+type+`=` + id)
    .pipe(
      map(data => {
      return data;
  })
  );
  }
  getRecentPosts(categoryId: number, page: number = 1) {
    // if we want to query posts by category
    let category_url = categoryId? ("&categories=" + categoryId): "";
    
    return this.http.get(
      environment.apiUrl
      + '/wp/v2/posts?page=' + page
      + '&orderby=modified' // order by last modified date
      + category_url)
    }
    getgroupMembers(id,scope="") {
      this.storage.get('COMPLETE_USER_INFO').then(result => {
        if (result != null) {
        console.log( result);
        this.token=result.token;
        }
        }).catch(e => {
        console.log('error: '+ e);
        // Handle errors here
        });
       
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);
      let scopeurl='';
      if(scope!="")
      scopeurl= `?scope=` + scope;
    return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/groups/`+id+`/members`+scopeurl, { headers:headers
     }).pipe(
        map(data => {
          console.log( data);
          for (let post of data) {
            if (post['avatar_urls']) {
              post.media_url =
                post['avatar_urls']['thumb'];
            }
            if (post['cover_url']) {
             
              post.cover =post['cover_url'];
            }else{
              post.cover = "assets/imgs/back1.jpg";
            }
          }
          return data;
        })
      );
    }

  joblistings(token,page: number = 1,types=[],search='') {
  
     
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+token);
      let ssearchurl='';
      if(search!="")
      ssearchurl= `&search=` + search;
      let scopeurl='';
      if(types!=[]){
      for (let type of types) {
      scopeurl+= `&job-types[]=` + type;
      }}

     
   
    return this.http.get<any[]>(`${environment.apiUrl}/wp/v2/job-listings?page=` + page +ssearchurl+scopeurl, { headers:headers
     }).pipe(
        map(data => {
          console.log( data);
          // let jobtypes;
          // this.jobtypes(token).subscribe((res : any) => {
             
            
              
          //   jobtypes= res.media_url;
           
          // });
          for (let post of data) {

            post.location = post['meta']['_job_location'];
            post.company_name = post['meta']['_company_name'];
            post.title = post['title']['rendered'];
            post.content = this.removeTags(post['content']['rendered']);
            post.date = this.timeSince(new Date(post['date']));

         
            
            // var dataval :any =  jobtypes.find(  data => data.id ===  postcmt.secondary_item_id);
            // if(typeof (dataval) !== 'undefined') 
            // postcmt.replycomment_text = this.truncate(dataval.content_stripped,10);
            if (post['better_featured_image']) {
              post.media_url =
              post['better_featured_image']['media_details']['sizes']['medium']['source_url'];
            }else{
              post.media_url = "assets/imgs/linkedin.png";
            }
           
          }
          return data;
        })
      );
  }

  jobtypes(token) {
  
     
    let headers = new HttpHeaders();
    headers.append("Authorization", 'Bearer '+token);

  return this.http.get<any[]>(`${environment.apiUrl}/wp/v2/job-types`, { headers:headers
   }).pipe(
      map(data => {
        console.log( data);
        for (let post of data) {

          post.isChecked = false;
          // post.company_name = post['meta']['_company_name'];
          // post.title = post['title']['rendered'];
          // post.content = this.removeTags(post['content']['rendered']);
          // post.date = this.timeSince(new Date(post['date']));

       

          // if (post['better_featured_image']) {
          //   post.media_url =
          //   post['better_featured_image']['media_details']['sizes']['medium']['source_url'];
          // }else{
          //   post.media_url = "assets/imgs/linkedin.png";
          // }
         
        }
        return data;
      })
    );
}
  getMembers(token) {
  
     
    let headers = new HttpHeaders();
    headers.append("Authorization", 'Bearer '+token);

  return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/members`, { headers:headers
   }).pipe(
      map(data => {
        console.log( data);
        for (let post of data) {
          if (post['avatar_urls']) {
            post.media_url =
              post['avatar_urls']['thumb'];
          }
          if (post['cover_url']) {
           
            post.cover =post['cover_url'];
          }else{
            post.cover = "assets/imgs/back1.jpg";
          }
          post.name= this.truncate(post['name'],15);
          post.profile_name= this.truncate(post['profile_name'],15);
        }
        return data;
      })
    );
  }
  followuser(action,id) {
  
    const formData = new FormData();
    formData.append('action', action);
    formData.append('user_id', id);
    return this.http.post(`${environment.apiUrl}/buddyboss/v1/members/action/`+id, formData
  );
}
Groupinvite(group_id,id) {
  
  const formData = new FormData();
  formData.append('group_id', group_id);
  formData.append('user_id', id);
  return this.http.post(`${environment.apiUrl}/buddyboss/v1/groups/invites`, formData
);
}
deletefrds(id) {
  
  const formData = new FormData();
    
    formData.append('user_id', id);

  return this.http.delete(`${environment.apiUrl}/buddyboss/v1/friends/`+id
);
}
deltmedia(id,token){
  let headers = new HttpHeaders();
  headers.append("Authorization", 'Bearer '+token);
  return this.http.delete(`${environment.apiUrl}/buddyboss/v1/media/`+id, {headers:headers}
  );
}

connectfrds(id,token,user_id){
  

  const formData = new FormData();
  formData.append('initiator_id', user_id);
  formData.append('friend_id', id);

  return this.http.post(`${environment.apiUrl}/buddyboss/v1/friends`, formData
);
}
leavegroup(action,id,token,user_id) {
  

 
 
  let headers = new HttpHeaders();
  headers.append("Authorization", 'Bearer '+token);
const formData = new FormData();
formData.append('add', action);

return this.http.delete(`${environment.apiUrl}/buddyboss/v1/groups/`+id+'/members/'+ user_id,{ headers:headers}).pipe(
  map((data:any) => {
   
    return data;
  })
  );
}
joingroup(action,id) {
  

  this.storage.get('COMPLETE_USER_INFO').then(result => {
    if (result != null) {
    console.log( result);
    this.token=result.token;
   
    this.user_id=result.user_id;
    }
    }).catch(e => {
    console.log('error: '+ e);
    // Handle errors here
    });
     let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);
  const formData = new FormData();
  formData.append('add', action);

  return this.http.post(`${environment.apiUrl}/buddyboss/v1/groups/`+id+'/members', formData,{ headers:headers}
  ).pipe(
    map((data:any) => {
     
      return data;
    })
    );
}
postactcomment(comment,id,commentid:any=0,bp_media_ids:any=[],user_id) {
  
  const formData = new FormData();
  formData.append('content', comment);
  formData.append('primary_item_id', id);
  if(bp_media_ids)
  formData.append('bp_media_ids', bp_media_ids);
  formData.append('component', "activity");
  formData.append('type',"activity_comment");
  formData.append('user_id', user_id);

  if(commentid!=0)
  formData.append('secondary_item_id', commentid);
  return this.http.post(`${environment.apiUrl}/buddyboss/v1/activity/`, formData).pipe(
    map((data:any) => {
     
    
      console.log( data);
    return data;
})
);
}


postreply(comment,id,commentid:any=0,bp_media_ids:any=[]) {
  
  const formData = new FormData();
  formData.append('content', comment);
  formData.append('topic_id', id);
  // if(bp_media_ids)
  // formData.append('bp_media_ids', bp_media_ids);
 
  if(commentid!=0)
  formData.append('reply_to', commentid);
  return this.http.post(`${environment.apiUrl}/buddyboss/v1/reply`, formData).pipe(
    map((post:any) => {
     
     
         
    return post;
})
);
}

  postcomment(comment,id,commentid:any=0,bp_media_ids:any=[]) {
  
    const formData = new FormData();
    formData.append('content', comment);
    formData.append('id', id);
    // if(bp_media_ids)
    // formData.append('bp_media_ids', bp_media_ids);
   
    if(commentid!=0)
    formData.append('parent_id', commentid);
    return this.http.post(`${environment.apiUrl}/buddyboss/v1/activity/`+id+`/comment`, formData).pipe(
      map((data:any) => {
       
      
        for (let post of data.comments) {
          post.title=post['title'];
          post.name=post['name'];
          post.group_name=post['activity_data']['group_name'];;
          post.date=post['date'];
          if(post['type'] == "joined_group")
          post.type_name="joined the group";
         else if(post['type'] == "bbp_reply_create")
          post.type_name="replied to the discussion";
          else if(post['type'] == "bbp_topic_create")
          post.type_name="started the discussion";
          else if(post['type'] == "activity_update")
          post.type_name="posted an update";
          else if(post['type'] == "group_details_updated")
          post.type_name="changed the name and description";


         post.discussion = "";  post.header_title =post['name']+" "+post.type_name;
          post.title=this.removeTags(post['title']);
          if(post.title) post.discussion = post.title.replace(post['name'], "");
          if(post.type_name)  post.discussion = post.discussion.replace(post.type_name, "");
          if(post.group_name)  post.discussion = post.discussion.replace(post.group_name, "");
          if(post.discussion)  post.discussion = post.discussion.replace("in the group", "");
          var n=0;   if(post.discussion) n = post.discussion.length;
          if(n<4)
          post.discussion ="";

          if (post['user_avatar']) {
            post.user_avatar =
              post['user_avatar']['thumb'];
          }
          let media=[];
          if (post['bp_media_ids']) {
            for (let image of post['bp_media_ids']) {
             
              media.push(image['attachment_data']['full']);
            }
            post.media=media;
          }
          post.date=this.timeSince(new Date(post['date']));
        }
        console.log( data);
      return data.comments;
  })
  );
}
  
  activitygetcomment(id){
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);
    return this.http.get(`${environment.apiUrl}/buddyboss/v1/activity/`+id+`/comment`, { headers:headers
    }).pipe(
      map((data:any) => {
      //   let count =data.comments.length;
      // let i ;
       let comments:any =[];
      // comments =data.comments;
      //   for (i = 0; i < data.comments.length; i++) {
      //   if(data.comments[i].comments){
      //     data.comments[count+i]=data.comments[i].comments.flat(Infinity);

      //   }
      //   }
      
     
       
        for (let post of  data.comments) {
          post.title=post['title'];
          post.name=post['name'];
          post.group_name=post['activity_data']['group_name'];;
          post.date=post['date'];
          if(post['type'] == "joined_group")
          post.type_name="joined the group";
         else if(post['type'] == "bbp_reply_create")
          post.type_name="replied to the discussion";
          else if(post['type'] == "bbp_topic_create")
          post.type_name="started the discussion";
          else if(post['type'] == "activity_update")
          post.type_name="posted an update";
          else if(post['type'] == "group_details_updated")
          post.type_name="changed the name and description";

          if(post.comments){
            console.log(post.comments);
            this.idArray =[];
            this.funcarray(post);
      this.idArray = this.idArray.flat(Infinity)
      this.idArray = this.idArray.filter(function( element ) {
        return element !== undefined;
     });
     let children = data.comments.concat(this.idArray);
     console.log(this.idArray);
          for (let postcmt of  this.idArray) {
            postcmt.title=postcmt['title'];
            postcmt.name=postcmt['name'];
            postcmt.group_name=postcmt['activity_data']['group_name'];;
            postcmt.date=postcmt['date'];
            if(postcmt['type'] == "joined_group")
            postcmt.type_name="joined the group";
           else if(postcmt['type'] == "bbp_reply_create")
            postcmt.type_name="replied to the discussion";
            else if(postcmt['type'] == "bbp_topic_create")
            postcmt.type_name="started the discussion";
            else if(postcmt['type'] == "activity_update")
            postcmt.type_name="posted an update";
            else if(postcmt['type'] == "group_details_updated")
            postcmt.type_name="changed the name and description";
  
          
              postcmt.replycomment = true;
             
              var dataval :any =  children.find(  data => data.id ===  postcmt.secondary_item_id);
              if(typeof (dataval) !== 'undefined') 
              postcmt.replycomment_text = this.truncate(dataval.content_stripped,10);
            
            
  
  
            postcmt.header_title =postcmt['name']+" "+postcmt.type_name;
            postcmt.title=this.removeTags(postcmt['title']);
            postcmt.discussion = postcmt.title.replace(postcmt['name'], "");
            postcmt.discussion = postcmt.discussion.replace(postcmt.type_name, "");
            postcmt.discussion = postcmt.discussion.replace(postcmt.group_name, "");
            postcmt.discussion = postcmt.discussion.replace("in the group", "");
            var n = postcmt.discussion.length;
            if(n<4)
            postcmt.discussion ="";
  
            if (postcmt['user_avatar']) {
              postcmt.user_avatar =
                postcmt['user_avatar']['thumb'];
            }
            let media=[];
            if (postcmt['bp_media_ids']) {
              for (let image of postcmt['bp_media_ids']) {
               
                media.push(image['attachment_data']['full']);
              }
              postcmt.media=media;
            }
            postcmt.date=this.timeSince(new Date(postcmt['date']));
          }

          post.replycomments = this.idArray;
        }

         post.discussion = "";  post.header_title =post['name']+" "+post.type_name;
          post.title=this.removeTags(post['title']);
          if(post.title) post.discussion = post.title.replace(post['name'], "");
          if(post.type_name)  post.discussion = post.discussion.replace(post.type_name, "");
          if(post.group_name)  post.discussion = post.discussion.replace(post.group_name, "");
          if(post.discussion)  post.discussion = post.discussion.replace("in the group", "");
          var n:any=0;   if(post.discussion) n = post.discussion.length;
          if(n<4)
          post.discussion ="";

          if (post['user_avatar']) {
            post.user_avatar =
              post['user_avatar']['thumb'];
          }
          let media=[];
          if (post['bp_media_ids']) {
            for (let image of post['bp_media_ids']) {
             
              media.push(image['attachment_data']['full']);
            }
            post.media=media;
          }
          post.date=this.timeSince(new Date(post['date']));
        }
        console.log( data.comments);
        
        console.log(this.idArray);
      return data.comments;
  })
  );
  }


   funcarray(obj) {
    console.log(obj.comments);
   
    this.idArray.push(obj.comments);
    if (!obj.comments) {
      return
    }
  
    obj.comments.forEach(child => this.funcarray(child))
  }
   
  activitydelete(id){
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);
    return this.http.delete(`${environment.apiUrl}/buddyboss/v1/activity/`+id, { headers:headers
    });
  }
  Replydelete(id){
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);
    return this.http.delete(`${environment.apiUrl}/buddyboss/v1/reply/`+id, { headers:headers
    });
  }
  activitylike(id){
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);
    return this.http.patch(`${environment.apiUrl}/buddyboss/v1/activity/`+id+`/favorite`, { headers:headers
    }).pipe(
      map((post:any) => {
       
          post.title=post['title'];
          post.name=post['name'];
          post.group_name=post['activity_data']['group_name'];;
          post.date=post['date'];
          if(post['type'] == "joined_group")
          post.type_name="joined the group";
         else if(post['type'] == "bbp_reply_create")
          post.type_name="replied to the discussion";
          else if(post['type'] == "bbp_topic_create")
          post.type_name="started the discussion";
          else if(post['type'] == "activity_update")
          post.type_name="posted an update";
          else if(post['type'] == "group_details_updated")
          post.type_name="changed the name and description";
          if(post.component=='groups'){
            post.group_name=post['activity_data']['group_name'];
            post.grpid=post['activity_data']['group_id'];
            this.getGroupdetails(post['activity_data']['group_id']).subscribe((res : any) => {
             
            
              
              post.media_url= res.media_url;
             
            });
          } 

         post.discussion = "";  post.header_title =post['name']+" "+post.type_name;
          post.title=this.removeTags(post['title']);
          if(post.title) post.discussion = post.title.replace(post['name'], "");
          if(post.type_name)  post.discussion = post.discussion.replace(post.type_name, "");
          if(post.group_name)  post.discussion = post.discussion.replace(post.group_name, "");
          if(post.discussion)  post.discussion = post.discussion.replace("in the group", "");
          var n=0;   if(post.discussion) n = post.discussion.length;
          if(post.title)
          post.title = post.title.replace("in the group", "");
          if(n<4)
          post.discussion ="";

          if (post['user_avatar']) {
            post.user_avatar =
              post['user_avatar']['thumb'];
          }
          let media=[];
          if (post['bp_media_ids']) {
            for (let image of post['bp_media_ids']) {
             
              media.push(image['attachment_data']['full']);
            }
            post.media=media;
          }
          post.date=this.timeSince(new Date(post['date']));
        
      return post;
  })
  );
  }
  getActivities(page: number = 1) {
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);

    return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/activity?page=` + page +'&orderby=modified', { headers:headers
     }).pipe(
      map(data => {
        console.log( data);
        for (let post of data) {
          if (post['user_avatar']) {
            post.media_url =
              post['user_avatar']['thumb'];
          }
          post.title=this.removeTags(post['title']);
          post.name=this.removeTags(post['name']);
          post.content=this.removeTags(post['content']['rendered']);
          post.date=this.timeSince(new Date(post['date']));
        }
        return data;
      })
    );
  }
  getActivitydetails(tag,id) {
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);

    return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/activity?`+tag+`=`+id, { headers:headers
     }).pipe(
      map(data => {
        console.log( data);
      
        for (let post of data) {
          post.title=post['title'];
          post.name=post['name'];
          post.group_name=post['activity_data']['group_name'];;
          post.date=post['date'];
          if(post['type'] == "joined_group")
          post.type_name="joined the group";
         else if(post['type'] == "bbp_reply_create")
          post.type_name="replied to the discussion";
          else if(post['type'] == "bbp_topic_create")
          post.type_name="started the discussion";
          else if(post['type'] == "activity_update")
          post.type_name="posted an update";
          else if(post['type'] == "group_details_updated")
          post.type_name="changed the name and description";


         post.discussion = "";  post.header_title =post['name']+" "+post.type_name;
          post.title=this.removeTags(post['title']);
          if(post.title) post.discussion = post.title.replace(post['name'], "");
          if(post.type_name)  post.discussion = post.discussion.replace(post.type_name, "");
          if(post.group_name)  post.discussion = post.discussion.replace(post.group_name, "");
          if(post.discussion)  post.discussion = post.discussion.replace("in the group", "");
          var n=0;   if(post.discussion) n = post.discussion.length;
          if(n<4)
          post.discussion ="";

          if (post['user_avatar']) {
            post.user_avatar =
              post['user_avatar']['thumb'];
          }
          let media=[];
          if (post['bp_media_ids']) {
            for (let image of post['bp_media_ids']) {
             
              media.push(image['attachment_data']['full']);
            }
            post.media=media;
          }
          post.date=this.timeSince(new Date(post['date']));
        }
        console.log("getActivitydetails",  data);
        return data;
      })
    );
  }
  
  getActivityall(page: number = 1,scope='',search='') {
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);

      let ssearchurl='';
      if(search!="")
      ssearchurl= `&search=` + search;
      let scopeurl='';
      if(scope!="")
      scopeurl= `&scope=` + scope;
    return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/activity?page=` + page +'&orderby=modified'+ssearchurl+scopeurl, { headers:headers
     }).pipe(
      map(data => {
        console.log( data);
      
        for (let post of data) {
          post.title=post['title'];
          post.name=post['name'];

          if(post.component=='groups'){
          post.group_name=post['activity_data']['group_name'];
          post.grpid=post['activity_data']['group_id'];
          this.getGroupdetails(post['activity_data']['group_id']).subscribe((res : any) => {
           
          
            
            post.media_url= res.media_url;
           
          });
        }
          post.date=post['date'];
          if(post['type'] == "joined_group")
          post.type_name="joined the group";
         else if(post['type'] == "bbp_reply_create")
          post.type_name="replied to the discussion";
          else if(post['type'] == "bbp_topic_create")
          post.type_name="started the discussion";
          else if(post['type'] == "activity_update")
          post.type_name="posted an update";
          else if(post['type'] == "group_details_updated")
          post.type_name="changed the name and description";


         post.discussion = "";  post.header_title =post['name']+" "+post.type_name;
          post.title=this.removeTags(post['title']);
          if(post.title) post.discussion = post.title.replace(post['name'], "");
          if(post.type_name)  post.discussion = post.discussion.replace(post.type_name, "");
          if(post.group_name)  post.discussion = post.discussion.replace(post.group_name, "");
          if(post.discussion)  post.discussion = post.discussion.replace("in the group", "");
          var n=0;   if(post.discussion) n = post.discussion.length;
          if(n<4)
          post.discussion ="";
          if(post.title)
          post.title = post.title.replace("in the group", "");
          if (post['user_avatar']) {
            post.user_avatar =
              post['user_avatar']['thumb'];
          }
          let media=[];
          if (post['bp_media_ids']) {
            for (let image of post['bp_media_ids']) {
             
              media.push(image['attachment_data']['full']);
            }
            post.media=media;
          }
          post.date=this.timeSince(new Date(post['date']));



        


        }
        console.log("getActivityall", data);
        return data;
      })
    );
  }
  getgroupfeeds(tag,id, page: number = 1) {
    // if we want to query posts by category
  

    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);

    return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/activity?`+tag+`=`+id+`&page=` + page +'&orderby=modified', { headers:headers
     }).pipe(
      map(data => {
        console.log( data);
      
        for (let post of data) {
          post.title=post['title'];
          post.name=post['name'];
          post.group_name=post['activity_data']['group_name'];;
          post.date=post['date'];
          if(post['type'] == "joined_group")
          post.type_name="joined the group";
         else if(post['type'] == "bbp_reply_create")
          post.type_name="replied to the discussion";
          else if(post['type'] == "bbp_topic_create")
          post.type_name="started the discussion";
          else if(post['type'] == "activity_update")
          post.type_name="posted an update";
          else if(post['type'] == "group_details_updated")
          post.type_name="changed the name and description";


         post.discussion = "";  post.header_title =post['name']+" "+post.type_name;
          post.title=this.removeTags(post['title']);
          if(post.title) post.discussion = post.title.replace(post['name'], "");
          if(post.type_name)  post.discussion = post.discussion.replace(post.type_name, "");
          if(post.group_name)  post.discussion = post.discussion.replace(post.group_name, "");
          if(post.discussion)  post.discussion = post.discussion.replace("in the group", "");
          var n=0;   if(post.discussion) n = post.discussion.length;
          if(n<2)
          post.discussion ="";
          if (post['user_avatar']) {
            post.user_avatar =
              post['user_avatar']['thumb'];
          }
          let media=[];
          if (post['bp_media_ids']) {
            for (let image of post['bp_media_ids']) {
             
              media.push(image['attachment_data']['full']);
            }
            post.media=media;
          }
          post.date=this.timeSince(new Date(post['date']));
        }
        console.log( data);
        return data;
      })
    );
  }
  getNotifications() {
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);

    return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/notifications`, { headers:headers
     }).pipe(
      map(data => {
        console.log( data);
        for (let post of data) {
         
         
          post.avatar=post['avatar_urls']['thumb'];
          post.content=this.removeTags(post['description']['rendered']);
          post.date=this.timeSince(new Date(post['date']));
        }
        return data;
      })
    );
  }
  getForums(page: number = 1) {
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);

    return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/forums?page=` + page +'&orderby=modified', { headers:headers
     }).pipe(
      map(data => {
        console.log( data);
        for (let post of data) {
         
          post.title=post['title']['raw'];
          post.count=post['total_topic_count'];
          post.avatar=post['featured_media']['thumb'];
          post.content= this.truncate(this.removeTags(post['content']['rendered']), 100);
          post.date=this.timeSince(new Date(post['date']));
        }
        return data;
      })
    );
  }
  getGroups(page: number = 1,scope='',search='') {
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
     
    let headers = new HttpHeaders();
    headers.append("Authorization", 'Bearer '+this.token);
    let ssearchurl='';
    if(search!="")
    ssearchurl= `&search=` + search;
    let scopeurl='';
    if(scope!="")
    scopeurl= `&typeoptional=` + scope;
  return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/groups?page=` + page +ssearchurl+scopeurl, { headers:headers
   }).pipe(
      map(data => {
        console.log( data);
        for (let post of data) {
          if (post['avatar_urls']) {
            post.media_url =
              post['avatar_urls']['thumb'];
          }
          if (post['description']) {
            var res = this.truncate(post['description']['raw'], 40);
            post.description =res;
          }
          if (post['cover_url']) {
           
            post.cover =post['cover_url'];
          }else{
            post.cover = "assets/imgs/back1.jpg";
          }
        }
        return data;
      })
    );
  }

  
  getForumReplies(id) {
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
     
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
     
    let headers = new HttpHeaders();
    headers.append("Authorization", 'Bearer '+this.token);

  return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/reply?parent=`+id, { headers:headers
   }).pipe(
      map((data : any) => {
        console.log(data);
        
       
        for (let post of  data) {
      
        
          post.date=this.timeSince(new Date(post['date']));
          post.content = post['content']['raw'];
          this.getuser(post['author']).subscribe((res : any) => {
            post.author_name= res.name;
            post.author_image= res.avatar_urls['thumb'];
          });
          if(post['reply_to']==0){
            var index  =  data.find(  data => data.reply_to ===  post['id']);
            if(typeof (index) !== 'undefined') 
              index.replycomment_text=this.removeTags(post.content);
          }
          
          // var index  =  data.find(  data => data.reply_to ===  post['id']);
          // if(typeof (index) !== 'undefined') {
          //   index.replycomment_text=post['content']['raw'];
          //   index.date=this.timeSince(new Date(index['date']));
          //   index.content = index['content']['raw'];
          //   this.getuser(index['author']).subscribe((res : any) => {
          //     index.author_name= res.name;
          //     index.author_image= res.avatar_urls['thumb'];
          //   });
          //   var index2  =  data.find(  data => data.reply_to ===  index['id']);
          //   if(typeof (index2) !== 'undefined') {
             
          //     index2.date=this.timeSince(new Date(index2['date']));
          //     index2.content = index2['content']['raw'];
          //     this.getuser(index2['author']).subscribe((res : any) => {
          //       index2.author_name= res.name;
          //       index2.author_image= res.avatar_urls['thumb'];
          //     });
          //     index2.replycomment_text=index['content']['raw'];
          //     index['reply']= index2;}


          //   post['reply']= index;
          
          
          // }
          
          
   
          }
      return data;
      })
    );
  }
  getForumTopics(id) {
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
     
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
     
    let headers = new HttpHeaders();
    headers.append("Authorization", 'Bearer '+this.token);

  return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/topics?parent=`+id, { headers:headers
   }).pipe(
      map((data : any) => {
        for (let post of data) {
      
        post.title=post['title']['raw'];
     


        
          if (post['content']) {
            var res = this.removeTags(post['content']['raw']);
            post.description =res;
            post.shortdescription =this.truncate(res, 100);
          }
          post.date=this.timeSince(new Date(post['modified_gmt']));
          this.getuser(post['author']).subscribe((res : any) => {
           
          
         
            post.author_name= res.name;
            post.author_image= res.avatar_urls['thumb'];
          });
          
        }

        return data;
      })
    );
  }

  getForumDetails(id) {
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
     
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
     
    let headers = new HttpHeaders();
    headers.append("Authorization", 'Bearer '+this.token);

  return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/forums/`+id, { headers:headers
   }).pipe(
      map((data : any) => {
       
        console.log("getForumDetails", data);
        data.title=data['title']['raw'];
        data.count=data['total_topic_count'];


          if (data['featured_media']) {
            data.cover =
              data['featured_media']['thumb'];
          }
          if (data['content']) {
            var res = data['content']['raw'];
            data.description =res;
            data.shortdescription =this.truncate(res, 100);
          }
          data.date=this.timeSince(new Date(data['last_active_time']));
        
          


        return data;
      })
    );
  }
 //   Job Profile
 getJobdetails(token,api) {
   
     
  let headers = new HttpHeaders();
  headers.append("Authorization", 'Bearer '+token);

return this.http.get<any[]>(`${environment.apiUrl}/dieselship/v1/`+api, { headers:headers
 }).pipe(
    map((data : any) => {
      let formdat:any=[];
      for (let post of data) {
       let key = post['meta_key'];
     
      formdat[key] = post['meta_value'];
      
      }
console.log(formdat);

      return formdat;
    })
  );
}
userjobupdate(
  data,token) {
    let headers = new HttpHeaders();
  headers.append("Authorization", 'Bearer '+token);

  
  
      // const formData = new FormData();
      // formData.append('meta_data', formdat);
    return this.http.post(`${environment.apiUrl}/dieselship/v1/updatedetails`, data, {headers:headers});
  }
  userpersonalupdate(
    surname, 
    givenname, 
    indosno,
    gender,
    date_of_birth,
    place_of_birth,
    birth_country, 
    disc,
    birth_state,
    nationality,token) {
      let headers = new HttpHeaders();
    headers.append("Authorization", 'Bearer '+token);

    
    let formdat:any=[];
    formdat.push({
      meta_key: 'surname', 
      meta_value:  surname
      },{
        meta_key: 'givenname', 
        meta_value:  givenname
        },{
          meta_key: 'indosno', 
          meta_value:  indosno
          },{
            meta_key: 'gender', 
            meta_value:  gender
            },{
              meta_key: 'date_of_birth', 
              meta_value:  date_of_birth
              },{
                meta_key: 'place_of_birth', 
                meta_value:  place_of_birth
                },{
                  meta_key: 'birth_country', 
                  meta_value:  birth_country
                  },{
                    meta_key: 'birth_state', 
                    meta_value:  birth_state
                    },{
                      meta_key: 'disc', 
                      meta_value:  disc
                      }
                      ,{
                        meta_key: 'nationality', 
                        meta_value:  nationality
                        }
        );
        let formdata:any=[];
        formdata.push( {meta_data: formdat});
        // const formData = new FormData();
        // formData.append('meta_data', formdat);
      return this.http.post(`${environment.apiUrl}/dieselship/v1/updatedetails`, formdata, {headers:headers});
    }











  getGroupdetails(id) {
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
     
    let headers = new HttpHeaders();
    headers.append("Authorization", 'Bearer '+this.token);

  return this.http.get<any[]>(`${environment.apiUrl}/buddyboss/v1/groups/`+id, { headers:headers
   }).pipe(
      map((data : any) => {
        console.log( data);
      
          if (data['avatar_urls']) {
            data.media_url =
              data['avatar_urls']['thumb'];
          }
          if (data['description']) {
            data.full_description = data['description']['raw'];
            var res = this.truncate(data['description']['raw'], 40);
            data.description =res;
          }
         
          if (data['cover_url']) {
           
            data.cover =data['cover_url'];
          }else{
            data.cover = "assets/imgs/back1.jpg";
          }
        
          console.log( this.getuser(data['creator_id']));


        return data;
      })
    );
  }
   truncate(source, size) {
    return source.length > size ? source.slice(0, size - 1) + "…" : source;
  }
  
 
  removeTags(str) {
    if ((str===null) || (str===''))
    return false;
    else
    str = str.toString();
    return str.replace( /(<([^>]+)>)/ig, '');
    }
   timeSince(date) {

    var seconds = Math.floor(( Date.now() -  date) / 1000);
  
    var interval = seconds / 31536000;
  
    if (interval > 1) {
      return Math.floor(interval) + " years";
    }
    interval = seconds / 2592000;
    if (interval > 1) {
      return Math.floor(interval) + " months";
    }
    interval = seconds / 86400;
    if (interval > 1) {
      return Math.floor(interval) + " days";
    }
    interval = seconds / 3600;
    if (interval > 1) {
      return Math.floor(interval) + " hours";
    }
    interval = seconds / 60;
    if (interval > 1) {
      return Math.floor(interval) + " minutes";
    }
    return Math.floor(seconds) + " seconds";
  }
  getPosts() {
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
    
     
      this.token=result.token;
      
    
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
      let headers = new HttpHeaders();
      headers.append("Authorization", 'Bearer '+this.token);
     
  


    return this.http.get<any[]>(`${environment.apiUrl}/wp/v2/posts?_embed`, { headers:headers}).pipe(
      map(data => {
        for (let post of data) {
          if (post['_embedded']['wp:featuredmedia']) {
            post.media_url =
              post['_embedded']['wp:featuredmedia'][0]['media_details'].sizes['medium'].source_url;
          }
        }
        return data;
      })
    );
  }
  getCurrentUser() {
    return this.user.asObservable();
  }
 
  getUserValue() {
    return this.user.getValue();
  }
 
  logout() {
    this.storage.remove(JWT_KEY).then(() => {
      this.user.next(null);
    });
  }
}
/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class dummyDataService {

  users = [
    {
      img: 'assets/imgs/user.jpg',
      cover: 'assets/imgs/back1.jpg',
      btn_text: 'VIEW JOBS'
    },
    {
      img: 'assets/imgs/user2.jpg',
      cover: 'assets/imgs/back2.jpg',
      btn_text: 'FOLLOW HASHTAG'
    },
    {
      img: 'assets/imgs/user3.jpg',
      cover: 'assets/imgs/back3.jpg',
      btn_text: ''
    },
    {
      img: 'assets/imgs/user4.jpg',
      cover: 'assets/imgs/back4.jpg',
      btn_text: 'SEE ALL VIEW'
    },
    {
      img: 'assets/imgs/user.jpg',
      cover: 'assets/imgs/back1.jpg',
      btn_text: ''
    },
    {
      img: 'assets/imgs/user2.jpg',
      cover: 'assets/imgs/back2.jpg',
      btn_text: ''
    },
    {
      img: 'assets/imgs/user3.jpg',
      cover: 'assets/imgs/back3.jpg',
      btn_text: 'EXPAND YOUR NETWORK'
    },
    {
      img: 'assets/imgs/user4.jpg',
      cover: 'assets/imgs/back4.jpg',
      btn_text: 'SEE ALL VIEW'
    }
  ];

  jobs = [
    {
      img: 'assets/imgs/company/initappz.png',
      title: 'SQL Server',
      addr: 'Bhavnagar, Gujrat, India',
      time: '2 school alumni',
      status: '1 day'
    },
    {
      img: 'assets/imgs/company/google.png',
      title: 'Web Designer',
      addr: 'Vadodara, Gujrat, India',
      time: 'Be an early applicant',
      status: '2 days'
    },
    {
      img: 'assets/imgs/company/microsoft.png',
      title: 'Business Analyst',
      addr: 'Ahmedabad, Gujrat, India',
      time: 'Be an early applicant',
      status: '1 week'
    },
    {
      img: 'assets/imgs/company/initappz.png',
      title: 'PHP Developer',
      addr: 'Pune, Maharashtra, India',
      time: '2 school alumni',
      status: 'New'
    },
    {
      img: 'assets/imgs/company/google.png',
      title: 'Graphics Designer',
      addr: 'Bhavnagar, Gujrat, India',
      time: 'Be an early applicant',
      status: '1 day'
    },
    {
      img: 'assets/imgs/company/microsoft.png',
      title: 'Phython Developer',
      addr: 'Ahmedabad, Gujrat, India',
      time: '2 school alumni',
      status: '5 days'
    },

  ];

  company = [
    {
      img: 'assets/imgs/company/initappz.png',
      name: 'Initappz Shop'
    },
    {
      img: 'assets/imgs/company/microsoft.png',
      name: 'Microsoft'
    },
    {
      img: 'assets/imgs/company/google.png',
      name: 'Google'
    },
  ];
  constructor() { }
}

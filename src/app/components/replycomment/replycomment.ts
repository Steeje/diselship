
import { ProcessComponent } from './../process';
import { Component, Input } from '@angular/core';
 
@Component({
  selector: 'replycomment',
  templateUrl: 'replycomment.html',
  styleUrls: ['./replycomment.scss']
})
export class ReplyComponent implements ProcessComponent {
 
  @Input() data: any;
 
  constructor() {
 
  }
 
}
/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { dummyDataService } from 'src/app/services/dummy-data.service';
import { Router,ActivatedRoute } from '@angular/router';
import { MenuController, LoadingController } from '@ionic/angular';
import { apiService } from '../../services/api.service';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.page.html',
  styleUrls: ['./jobs.page.scss'],
})
export class JobsPage implements OnInit {
  token;user_id;
  jobs=["0"];jobtypes;
  actionDiv;

  fillter="";
  searchtext="";


  newest: boolean=false; alphabetical : boolean=false;
  active : boolean=true;
   popular : boolean=false;
  constructor(  private api: apiService,
    private storage: Storage, private router: Router, private menuCtrl: MenuController,public loadingController: LoadingController) {



      this.storage.get('COMPLETE_USER_INFO').then(result => {
        if (result != null) {
        console.log( result);
        this.token=result.token;
       
        this.user_id=result.user_id;
       
        console.log('user_id: '+  result.user_id);
        console.log('token: '+  result.token);
        }
        }).catch(e => {
        console.log('error: '+ e);
        // Handle errors here
        });
  
  }
  async onSearchChange(event) {
    const loading = await this.loadingController.create({
  
      message: 'Please wait...',
      duration: 1500
    });
    await loading.present();
    var dataval :any =  this.jobtypes.filter(  data => data.isChecked ===  true);
    console.log("filter",dataval);
    if(typeof (dataval) !== 'undefined' ){
  
  let result = dataval.map(a => a.id);;

  this.api.joblistings(this.token,1,result,this.searchtext).subscribe(res => {
    this.jobs = res;
  });

console.log("filter",result);

 
    console.log("searchtext",this.searchtext);
    console.log("filter",this.fillter);
}else {
  this.api.joblistings(this.token,1,[],this.searchtext).subscribe(res => {
    this.jobs = res;
  });

console.log("searchtext",this.searchtext);
}
      }

  async onFillterChange($event) {
    const loading = await this.loadingController.create({
  
      message: 'Please wait...',
      duration: 1500
    });
    await loading.present();
     var dataval :any =  this.jobtypes.filter(  data => data.isChecked ===  true);
     console.log("filter",dataval);
      if(typeof (dataval) !== 'undefined') {
      let result = dataval.map(a => a.id);
   
      this.api.joblistings(this.token,1,result,this.searchtext).subscribe(res => {
        this.jobs = res;
      });
   
    console.log("filter",result);
  
}else{
 
      this.api.joblistings(this.token,1,[],this.searchtext).subscribe(res => {
        this.jobs = res;
      });
    




  }


  }
  action(val) {
    this.actionDiv = val;
  }

  ngOnInit() {
    this.api.joblistings(this.token).subscribe(res => {
      console.log(res);
    
    this.jobs = res;});
    this.api.jobtypes( this.token).subscribe(res => {
      console.log(res);
    
    this.jobtypes = res;});
  }

  goToJobDetail() {
    this.router.navigate(['/job-detail']);
  }

  openMenu() {
    this.menuCtrl.open();
  }


  goToChatList() {
    this.router.navigate(['/chatlist']);
  }
}

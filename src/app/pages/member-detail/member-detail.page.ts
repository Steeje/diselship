/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { apiService } from '../../services/api.service';
import { Router } from '@angular/router';
import { Location } from "@angular/common";

@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.page.html',
  styleUrls: ['./member-detail.page.scss'],
})
export class MemberDetailPage implements OnInit {
  data: any = {};
  user = this.api.getCurrentUser();
  constructor(  private api: apiService,
    private storage: Storage, private location: Location, private router: Router) {

    this.user.subscribe(user => {
      if (user) {
        // location.href = "/home-new";
       
        this.storage.get('USER_INFO').then(result => {
          if (result != null) {
          console.log( result);
          this.data.id = result.id;
          this.data.username = result.nickname;
          if(typeof result.nickname === "undefined" )
          this.data.username = result.user_login;
          this.data.cover=result.cover_url;
          this.data.name=result.first_name+" "+ result.last_name;
          this.data.description = result.description;
          this.data.avatar=result.avatar_urls[24];
          if(typeof result.avatar_urls[24] === "undefined" )
            this.data.avatar=result.avatar_urls['full'];
          this.data.email = result.email;
          console.log(result.first_name);
        
          if(typeof result.first_name === "undefined" ){
        
           
              console.log( user);
              this.data.email = user.user_email;
              this.data.nickname = user.user_nicename;
            
              this.data.name=user.user_display_name;
             
          
            }
          }
          }).catch(e => {
          console.log('error: '+ e);
          // Handle errors here
          });
        }
   });
  }
  BackButton(){
    this.location.back();
  } 
  editProfile() {
    this.router.navigate(['/member-profile']);
  }
  detailsProfile() {
    this.router.navigate(['/profile-detail']);
  }
  ngOnInit() {
  }

}

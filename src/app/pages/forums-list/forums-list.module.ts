import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForumsListPageRoutingModule } from './forums-list-routing.module';

import { ForumsListPage } from './forums-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ForumsListPageRoutingModule
  ],
  declarations: [ForumsListPage]
})
export class ForumsListPageModule {}

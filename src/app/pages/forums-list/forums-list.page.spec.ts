import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ForumsListPage } from './forums-list.page';

describe('ForumsListPage', () => {
  let component: ForumsListPage;
  let fixture: ComponentFixture<ForumsListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForumsListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ForumsListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

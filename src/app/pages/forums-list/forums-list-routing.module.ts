import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForumsListPage } from './forums-list.page';

const routes: Routes = [
  {
    path: '',
    component: ForumsListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ForumsListPageRoutingModule {}

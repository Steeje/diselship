/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from "@angular/common";
import { apiService } from '../../services/api.service';
import { dummyDataService } from 'src/app/services/dummy-data.service';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController , ToastController} from '@ionic/angular';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-forums-list',
  templateUrl: './forums-list.page.html',
  styleUrls: ['./forums-list.page.scss'],
})
export class ForumsListPage implements OnInit {
  members = [];
  activities = [];
  groups = [];
  notifications = [];
  forums = [];
  users = [
    {
      img: 'assets/imgs/user.jpg',
      cover: 'assets/imgs/back1.jpg'
    },
    {
      img: 'assets/imgs/user2.jpg',
      cover: 'assets/imgs/back2.jpg'
    },
    {
      img: 'assets/imgs/user3.jpg',
      cover: 'assets/imgs/back3.jpg'
    },
    {
      img: 'assets/imgs/user4.jpg',
      cover: 'assets/imgs/back4.jpg'
    }
  ];
  dummyData;
  load= false;
  showSlider = false;
   options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
   showtab:boolean = true;
   token;user_id;
  constructor(private location: Location,
    public navCtrl: NavController,    private toastCtrl: ToastController,
    private nativePageTransitions: NativePageTransitions, 
    private router: Router,  private storage: Storage,private dummy:   dummyDataService,private api: apiService, private menuCtrl: MenuController) {
    this.load=true;
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
     
      this.user_id=result.user_id;
     
      console.log('user_id: '+  result.user_id);
      console.log('token: '+  result.token);
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
    this.loadMembers();
    this.dummyData = this.dummy.users;
   }
   ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options);
   
   }  BackButton(){
    this.location.back();
  }
   connectuser(id){
    this.api.connectfrds( id,this.token,this.user_id).subscribe( async (res:any)  => {
     
      console.log( res);
      var index =  this.members.findIndex(  data => data.id ===  id);
      var data :any =  this.members.find(  data => data.id ===  id);
      console.log( data);
      if(typeof (index) !== 'undefined') {
        data.friendship_status= "pending";
       
      this.members.splice(index, 1, data);}
      const toast = await this.toastCtrl.create({
        message: "Friend Request Sent Successfully",
        duration: 3000
      });
      await toast.present();
     });
  }
    
  loadData(event: any) {
    const page = (Math.ceil(this.activities.length / 10)) + 1;
  
 
      this.api.getForums(page).subscribe((newPagePosts: []) => {
        this.forums.push(...newPagePosts);
        event.target.complete();
      }, err => {
        // there are no more posts available
        event.target.disabled = true;
      })
   
     
  }
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options);
     this.navCtrl.navigateForward(page);
   
   }
  loadMembers() {
    
    this.api.getForums().subscribe(res => {
      console.log(res);
      this.forums = res;
      
    });
   
    this.load=false;
  }
  ngOnInit() {
  }
  myHeaderFn(record, recordIndex, records) {
    if (recordIndex % 20 === 0) {
      return 'Header ' + recordIndex;
    }
    return null;
  }
  goToPeopleProfile() {
    this.router.navigate(['/people-profile']);
  }

  openMenu() {
    this.menuCtrl.open();
  }

  goToMemberslist() {
    this.router.navigate(['/members-list']);
  }
  goToGroupslist() {
    this.router.navigate(['/groups-list']);
  }
  sliderShow(value) {
    this.showSlider = value;
  }
  gotoInvitations() {
    this.router.navigate(['/invitations']);
  }
  gotoGroup() {
    this.router.navigate(['/group-detail']);
  }
  onSearchChange(event) {

  }
  goToChatList() {
    this.router.navigate(['/chatlist']);
  }
}

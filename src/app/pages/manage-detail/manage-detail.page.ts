/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { dummyDataService } from 'src/app/services/dummy-data.service';

@Component({
  selector: 'app-manage-detail',
  templateUrl: './manage-detail.page.html',
  styleUrls: ['./manage-detail.page.scss'],
})
export class ManageDetailPage implements OnInit {

  text;
  users;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dummy: dummyDataService) {
    this.users = this.dummy.users;
  }

  ngOnInit() {
    this.route.queryParams.subscribe(data => {
      console.log(data);
      this.text = data.id;
    });
  }
  goToChatList() {
    this.router.navigate(['/chatlist']);
  }
}

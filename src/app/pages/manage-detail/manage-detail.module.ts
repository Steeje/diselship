import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManageDetailPageRoutingModule } from './manage-detail-routing.module';

import { ManageDetailPage } from './manage-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManageDetailPageRoutingModule
  ],
  declarations: [ManageDetailPage]
})
export class ManageDetailPageModule {}

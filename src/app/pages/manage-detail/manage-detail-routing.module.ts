import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManageDetailPage } from './manage-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ManageDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageDetailPageRoutingModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JobEndorsementPage } from './job-endorsement.page';

describe('RegisterPage', () => {
  let component: JobEndorsementPage;
  let fixture: ComponentFixture<JobEndorsementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobEndorsementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JobEndorsementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

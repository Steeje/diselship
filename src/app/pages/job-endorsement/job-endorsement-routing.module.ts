import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobEndorsementPage } from './job-endorsement.page';

const routes: Routes = [
  {
    path: '',
    component: JobEndorsementPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobEndorsementPageRoutingModule {}
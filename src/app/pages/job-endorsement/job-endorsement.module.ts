import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobEndorsementPageRoutingModule } from './job-endorsement-routing.module';

import { JobEndorsementPage } from './job-endorsement.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule ,
    IonicModule,
    JobEndorsementPageRoutingModule
  ],
  declarations: [JobEndorsementPage]
})
export class JobEndorsementPageModule {}

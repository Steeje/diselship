/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { apiService } from '../../services/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, ToastController, Platform  } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import swal from 'sweetalert2';
import { BehaviorSubject } from 'rxjs';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  authState = new BehaviorSubject(false);
  loading: boolean;
  userForm: FormGroup;
 
  user = this.api.getCurrentUser();
  posts = [];

  constructor(
    private router: Router,
    private api: apiService,
    private platform: Platform,
    private storage: Storage,
    private fb: FormBuilder,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController
  ) {
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
    this.user.subscribe(user => {
      if (user) {
        this.router.navigate(['members-activity']);
       
      } else {
    //     this.api.logout();
    //     this.storage.remove('COMPLETE_USER_INFO');
       
    // this.storage.remove('USER_INFO').then(() => {
     
    //   this.authState.next(false);
    // });
        this.posts = [];
      }
    });
  }
  ngOnInit() {
    this.userForm = this.fb.group({
      username: ['', Validators.required],
      email: '',
      password: ['', Validators.required]
    });
  }

  ifLoggedIn() {
    this.storage.get('USER_INFO').then((response) => {
      if (response) {
        this.authState.next(true);
      }
    });
  }
  


  isAuthenticated() {
    return this.authState.value;
  }

  goToRegister() {
    this.router.navigate(['/register']);

  }

  goToTabs() {
    this.router.navigate(['/tabs/home-new']);
  }

  goToForgotPassword() {
    this.router.navigate(['/forgot-password']);
  }

 
  login() {
    
    this.loading=true;
    this.api.signIn(this.userForm.value.username, this.userForm.value.password).subscribe(
      res => {
        console.log("-------COMPLETE_USER_INFO--------");
        console.log(res);
        this.storage.set('COMPLETE_USER_INFO', res);
        swal.fire({
          title: 'Success',
          text: 'Login Successfully',
          icon: 'success',
          backdrop: false,
        });
        this.loading=false;
        // this.router.navigate(['/tabs/home-new']);
        this.authState.next(true);
       
        this.api.getuser(res.user_id).subscribe(
          res => {
        console.log("-------USER_INFO--------");
        console.log(res);
      
        this.storage.set('USER_INFO', res);
        window.location.reload(); 
      });
      //  window.location.reload(); 
      //  location.href = "http://localhost:8100/";
      
      // this.router.navigate(['/']);
      },
      err => {
        this.loading=false;
        console.log(err);
        this.showError(err);
      }
    );
  }
  

  async openPwReset() {
    const alert = await this.alertCtrl.create({
      header: 'Forgot password?',
      message: 'Enter your email or username to retrieve a new password',
      inputs: [
        {
          type: 'text',
          name: 'usernameOrEmail'
        }
      ],
      buttons: [
        {
          role: 'cancel',
          text: 'Back'
        },
        {
          text: 'Reset Password',
          handler: (data) => {
            this.resetPw(data['usernameOrEmail']);
          }
        }
      ]
    });
  
    await alert.present();
  }
 
  resetPw(usernameOrEmail) {
    this.api.resetPassword(usernameOrEmail).subscribe(
      async res => {
        const toast = await this.toastCtrl.create({
          message: res['message'],
          duration: 2000
        });
        toast.present();
      },
      err => {
        this.showError(err);
      }
    );
  }
 
  loadPrivatePosts() {
    this.api.getPrivatePosts().subscribe(res => {
      this.posts = res;
    });
  }
 
  logout() {
   
    this.storage.remove('USER_INFO').then(() => {
      this.storage.remove('COMPLETE_USER_INFO');
      this.router.navigate(['login']);
      this.authState.next(false);
    });
    this.api.logout();
  }
 
  async showError(err) {
    const alert = await this.alertCtrl.create({
      header: "Invalid Username",
  
      message: err.error.message,
      buttons: ['OK']
    });
    await alert.present();
  }
}

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { ActionSheetController, MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { apiService } from '../../services/api.service';

@Component({
  selector: 'app-home-new',
  templateUrl: './home-new.page.html',
  styleUrls: ['./home-new.page.scss'],
})
export class HomeNewPage implements OnInit {
  user = this.api.getCurrentUser();
  posts = [];
  categoryId: any;
  categoryTitle: any;
  slideoptions = {
    slidesPerView: 1.2,
  };
  route: any;
  categories: any;
  showSlider = false;
  chips = ['ADD PHOTOS', 'ADD CONNECTIONS', 'HASHTAGS FOLLOWED'];
  constructor(private actionSheet: ActionSheetController, private api: apiService, private menuCtrl: MenuController, private router: Router) 
  {   this.user.subscribe(user => {
    if (user) {
      
      console.log(user);
      this.loadPrivatePosts();
    } else {
      this.posts = [];
    }
  }); }


  ngOnInit() {
    
    // this.route.data.subscribe(routeData => {
    //   const data = routeData['data'];
    
    //     for (let post of data) {
    //       if (post['_embedded']['wp:featuredmedia']) {
    //         post.media_url =
    //           post['_embedded']['wp:featuredmedia'][0]['media_details'].sizes['medium'].source_url;
    //       }
    //     }
       
    //   this.posts = data.posts;
    //   this.categoryId = data.categoryId;
    //   this.categoryTitle = data.categoryTitle;
    // })
  }
  sliderShow(value) {
    this.showSlider = value;
  }
  
  loadData(event: any) {
    const page = (Math.ceil(this.posts.length / 10)) + 1;

    this.api.getRecentPosts(this.categoryId, page)
    .subscribe((newPagePosts: []) => {
      this.posts.push(...newPagePosts);
      event.target.complete();
    }, err => {
      // there are no more posts available
      event.target.disabled = true;
    })
  }
  ionViewDidLoad(){
    this.api.retrieveCategories().subscribe(results => {
      this.categories = results;
    });
   }
 
  loadPrivatePosts() {
    this.api.getPosts().subscribe(res => {
      this.posts = res;
    });
  }
  async openActionSheet() {
    const actionSheet = await this.actionSheet.create({
      mode: 'md',
      buttons: [
        {
          text: 'Save',
          role: 'destructive',
          icon: 'bookmark-outline',
          handler: () => {
            console.log('Delete clicked');
          }
        },
        {
          text: 'Send in a private Message',
          icon: 'chatbox-ellipses-outline',
          handler: () => {
            console.log('Share clicked');
          }
        },
        {
          text: 'Share via',
          icon: 'share-social',
          handler: () => {
            console.log('Play clicked');
          }
        },
        {
          text: 'Hide this post',
          icon: 'close-circle-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Unfollow',
          icon: 'person-remove-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Report this post',
          icon: 'flag-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Improve my feed',
          icon: 'options-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Who can see this post?',
          icon: 'eye-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  openMenu() {
    this.menuCtrl.open();
  }

  onSearchChange(event) {

  }

  goToChatList() {
    this.router.navigate(['/chatlist']);
  }
  goTosinglePost() {
    this.router.navigate(['/single-post']);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeNewPageRoutingModule } from './home-new-routing.module';



import { Routes, RouterModule } from '@angular/router';



import { HomeNewPage } from './home-new.page';
import { PostsResolver } from './home-new.resolver';

const routes: Routes = [
  {
    path: '',
    component: HomeNewPage,
    resolve: {
      data: PostsResolver
    },
    runGuardsAndResolvers: 'paramsOrQueryParamsChange' // because we use the same route for all posts and for category posts, we need to add this to refetch data 
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeNewPageRoutingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeNewPage],
  providers: [PostsResolver]
})
export class HomeNewPageModule {}



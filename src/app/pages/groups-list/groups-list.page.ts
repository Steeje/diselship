/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ToastController, Platform  } from '@ionic/angular';
import { dummyDataService } from 'src/app/services/dummy-data.service';
import { apiService } from '../../services/api.service';
import { Location } from "@angular/common";
import { Storage } from '@ionic/storage';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-groups-list',
  templateUrl: './groups-list.page.html',
  styleUrls: ['./groups-list.page.scss'],
})
export class GroupsListPage implements OnInit {

  
  id;
  users;
  name;
  user_id;
  token;
  fillter="";
  searchtext="";
groups :any = [];

  newest: boolean=false; alphabetical : boolean=false;
  active : boolean=true;
   popular : boolean=false;
  options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  constructor(
    private route: ActivatedRoute,
    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, 
    private router: Router, public loadingController: LoadingController,
    private toastCtrl: ToastController,
    private api: apiService,
    private location: Location,private storage: Storage,
    private dummy: dummyDataService) {
      var members2=[];
      members2.push({
       thumb: 'surname', 
      
       });
       this.groups.push({
         avatar_urls: members2, 
        
         });
      this.storage.get('COMPLETE_USER_INFO').then(result => {
        if (result != null) {
        console.log( result);
        this.token=result.token;
       
        this.user_id=result.user_id;
       
        console.log('user_id: '+  result.user_id);
        console.log('token: '+  result.token);
        }
        }).catch(e => {
        console.log('error: '+ e);
        // Handle errors here
        });
    this.users = this.dummy.users;
   
  }
  ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options);
   
   }
   
   
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options);
     this.navCtrl.navigateForward(page);
   
   }


   loadData(event: any) {
    const page = (Math.ceil(this.groups.length / 10)) + 1;
  
    if(this.searchtext!="" && this.fillter!=""){
      this.api.getGroups(page,this.fillter,this.searchtext).subscribe((newPagePosts: []) => {
        this.groups.push(...newPagePosts);
        event.target.complete();
      }, err => {
        // there are no more posts available
        event.target.disabled = true;
      })
   
      console.log("searchtext",this.searchtext);
      console.log("filter",this.fillter);
  }else if(this.searchtext!="" ){
    this.api.getGroups(page,"",this.searchtext).subscribe((newPagePosts: []) => {
      this.groups.push(...newPagePosts);
      event.target.complete();
    }, err => {
      // there are no more posts available
      event.target.disabled = true;
    })
 
  console.log("searchtext",this.searchtext);
}else{
     
      this.api.getGroups(page).subscribe((newPagePosts: []) => {
        this.groups.push(...newPagePosts);
        event.target.complete();
      }, err => {
        // there are no more posts available
        event.target.disabled = true;
      })
     
    }




  
  
  }

   async fillternews(filter) {
    const loading = await this.loadingController.create({
  
      message: 'Please wait...',
      duration: 1500
    });
    await loading.present();
    if(filter=="newest"){
      this.newest=true;  this.alphabetical =false;
      this.active =false;
      this.popular =false;
    }else  if(filter=="alphabetical"){
      this.newest=false;  this.alphabetical =true;
      this.active =false;
      this.popular =false;
    }else  if(filter=="active"){
      this.newest=false;  this.alphabetical =false;
      this.active =true;
      this.popular =false;
    }if(filter=="popular"){
      this.newest=false;  this.alphabetical =false;
      this.active =false;
      this.popular =true;
    } 

    if(filter!="active" ){
      this.fillter=filter;
    if(this.searchtext==""){
      this.api.getGroups(1,filter,"").subscribe(res => {
        this.groups = res;
      });
   
    console.log("filter",filter);
  }else if(this.searchtext!=""){
      this.api.getGroups(1,filter,this.searchtext).subscribe(res => {
        this.groups = res;
      });
   
    console.log("filter",filter);
  }
}else{
  if(this.searchtext==""){
    this.fillter="";
    this.api.getGroups().subscribe(res => {
      this.groups = res;
    });
    console.log("filter11",filter); }
    else if(this.searchtext!=""){
      this.api.getGroups(1,"",this.searchtext).subscribe(res => {
        this.groups = res;
      });
    }




  }


  }
 async onSearchChange(event) {
    const loading = await this.loadingController.create({
  
      message: 'Please wait...',
      duration: 1500
    });
    await loading.present();
if(this.searchtext!="" && this.fillter!=""){
    this.api.getGroups(1,this.fillter,this.searchtext).subscribe(res => {
      this.groups = res;
    });
 
    console.log("searchtext",this.searchtext);
    console.log("filter",this.fillter);
}else if(this.searchtext!="" ){
  this.api.getGroups(1,"",this.searchtext).subscribe(res => {
    this.groups = res;
  });

console.log("searchtext",this.searchtext);
}else{
   
    this.api.getGroups().subscribe(res => {
      this.groups = res;
    });
   
  }
      }




  leave(id){

    this.api.leavegroup("leave",id,this.token,this.user_id).subscribe(  async (res:any)  => {
    
      var index  =  this.groups.findIndex(  data => data.id ===  id);
      var data :any =  this.groups.find(  data => data.id ===  id);
      if(typeof (index) !== 'undefined') {
        data.is_member=false;
        data.members_count-=1;
      this.groups.splice(index, 1, data);}
      const toast = await this.toastCtrl.create({
        message: "Leave Group Successfully",
        duration: 3000
      });
      await toast.present();
     });
  }
  join(id){
    this.api.joingroup("join", id).subscribe( async (res:any)  => {
      console.log( res.data);
      var index  =  this.groups.findIndex(  data => data.id ===  id);
      var data :any =  this.groups.find(  data => data.id ===  id);
      if(typeof (index) !== 'undefined') {
        data.is_member=true;
        data.members_count+=1;
      this.groups.splice(index, 1, data);}
      const toast = await this.toastCtrl.create({
        message: "Join Group Successfully",
        duration: 3000
      });
      await toast.present();
     });
  }
  ngOnInit() {
    this.route.queryParams.subscribe(data => {
      console.log("data.id",data.id);
      this.id = data.id;
      this.name = data.name;
    });
    if(this.id){
      this.api.getgroupMembers(this.id).subscribe(res => {
        console.log(res);
        this.groups = res;
        
      });}else{
      this.api.getGroups().subscribe(res => {
        console.log(res);
        this.groups = res;
        
      });}
  }
  goTonewgroup() {
    this.router.navigate(['/group-update']);
  }
  goTomemberDetail() {
    this.router.navigate(['/member-detail']);
  }
  BackButton(){
    this.location.back();
  } 
}

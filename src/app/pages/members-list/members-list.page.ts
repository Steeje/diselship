/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ToastController, Platform  } from '@ionic/angular';
import { dummyDataService } from 'src/app/services/dummy-data.service';
import { apiService } from '../../services/api.service';
import { Location } from "@angular/common";
import { Storage } from '@ionic/storage';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController } from '@ionic/angular';

@Component({
  selector: 'app-members-list',
  templateUrl: './members-list.page.html',
  styleUrls: ['./members-list.page.scss'],
})
export class MembersListPage implements OnInit {
  members:any = [];
  id;
  users;
  name;
  user_id;
  token;
  options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  constructor(
    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, 
    private route: ActivatedRoute,
    private router: Router,
    private storage: Storage,
    private toastCtrl: ToastController,
    private api: apiService,
    private location: Location,
    private dummy: dummyDataService) {
    this.users = this.dummy.users;

    var members2=[];
     members2.push({
      thumb: 'surname', 
     
      });
      this.members.push({
        avatar_urls: members2, 
       
        });
        console.log( this.members);
    this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
     
      this.user_id=result.user_id;
     
      console.log('user_id: '+  result.user_id);
      console.log('token: '+  result.token);
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
  }
  ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options);
   
   }
   
   
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options);
     this.navCtrl.navigateForward(page);
   
   }
  unfollowuser(id){
    this.api.followuser("unfollow",id).subscribe(  async (res:any)  => {
      console.log( res.data);
      var index =  this.members.findIndex(  ({ id }) => id ===  res.data.id);

      if(typeof (index) !== 'undefined') 
      this.members.splice(index, 1, res.data);
      const toast = await this.toastCtrl.create({
        message: "Unfollow User Successfully",
        duration: 3000
      });
      await toast.present();
     });
  }
  connectuser(id){
    this.api.connectfrds( id,this.token,this.user_id).subscribe( async (res:any)  => {
     
      console.log( res);
      var index =  this.members.findIndex(  data => data.id ===  id);
      var data :any =  this.members.find(  data => data.id ===  id);
      console.log( data);
      if(typeof (index) !== 'undefined') {
        data.friendship_status= "pending";
       
      this.members.splice(index, 1, data);}
      const toast = await this.toastCtrl.create({
        message: "Friend Request Sent Successfully",
        duration: 3000
      });
      await toast.present();
     });
  }
  deleteuser(id){
    this.api.deletefrds( id).subscribe( async (res:any)  => {
     
      var index =  this.members.findIndex(  data => data.id ===  id);
      var data :any =  this.members.find(  data => data.id ===  id);
      console.log( data);
      if(typeof (index) !== 'undefined') {
        data.friendship_status= "not_friends";
       
      this.members.splice(index, 1, data);}
      const toast = await this.toastCtrl.create({
        message: "Unfriend Successfully",
        duration: 3000
      });
      await toast.present();
     });
  }
  followuser(id){
    this.api.followuser("follow", id).subscribe( async (res:any)  => {
      console.log( res.data);
      var index =  this.members.findIndex(  ({ id }) => id ===  res.data.id);

      if(typeof (index) !== 'undefined') 
      this.members.splice(index, 1, res.data);
      const toast = await this.toastCtrl.create({
        message: "Follow User Successfully",
        duration: 3000
      });
      await toast.present();
     });
  }
  ngOnInit() {
    this.route.queryParams.subscribe(data => {
      console.log("data.id",data.id);
      this.id = data.id;
      this.name = data.name;
    });
    if(this.id){
      this.api.getgroupMembers(this.id).subscribe(res => {
        console.log(res);
        this.members = res;
        
      });}else{
      this.api.getMembers(this.token).subscribe(res => {
        console.log(res);
        this.members = res;
        
      });}
  }
  goToChatList() {
    this.router.navigate(['/chatlist']);
  }
  goTomemberDetail() {
    this.router.navigate(['/member-detail']);
  }
  BackButton(){
    this.location.back();
  } 
}

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { apiService } from '../../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-detail',
  templateUrl: './profile-detail.page.html',
  styleUrls: ['./profile-detail.page.scss'],
})
export class ProfileDetailPage implements OnInit {
  data: any = {};
  user = this.api.getCurrentUser();
  constructor(  private api: apiService,
    private storage: Storage, private router: Router) {

  }
  editProfile() {
    this.router.navigate(['/job-personal']);
  }
  presentadd() {
    this.router.navigate(['/job-presentadd']);
  }
  permenantadd() {
    this.router.navigate(['/job-permenantadd']);
  }
  preseatraining() {
    this.router.navigate(['/job-preseatraining']);
  }
  education() {
    this.router.navigate(['/job-education']);
  }
  jobphysical() {
    this.router.navigate(['/job-physical']);
  }
  modularcoursedetails() {
    this.router.navigate(['/job-modularcoursedetails']);
  }
  seagoing() {
    this.router.navigate(['/job-seagoing']);
  }
  portsea() {
    this.router.navigate(['/job-portsea']);
  }
  endorsement() {
    this.router.navigate(['/job-endorsement']);
  }
  competency() {
    this.router.navigate(['/job-competency']);
  }
  authorised() {
    this.router.navigate(['/job-authorised']);
  }
  verification() {
    this.router.navigate(['/job-verification']);
  }
  ngOnInit() {
  }

}

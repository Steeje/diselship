import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PeopleProfilePageRoutingModule } from './people-profile-routing.module';

import { PeopleProfilePage } from './people-profile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PeopleProfilePageRoutingModule
  ],
  declarations: [PeopleProfilePage]
})
export class PeopleProfilePageModule {}

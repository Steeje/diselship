/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { dummyDataService } from 'src/app/services/dummy-data.service';

@Component({
  selector: 'app-people-profile',
  templateUrl: './people-profile.page.html',
  styleUrls: ['./people-profile.page.scss'],
})
export class PeopleProfilePage implements OnInit {

  users;
  company;
  constructor(private dummy: dummyDataService) { }

  ngOnInit() {
    this.users = this.dummy.users;
    this.company = this.dummy.company;
  }

}

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { apiService } from '../../services/api.service';
import { Location } from "@angular/common";
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController } from '@ionic/angular';


@Component({
  selector: 'app-forum-detail',
  templateUrl: './forum-detail.page.html',
  styleUrls: ['./forum-detail.page.scss'],
})
export class ForumDetailPage implements OnInit {
  data: any = {};
  topics = [];
  creator: any = {};
  id1: any;
  options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  constructor(
    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, private router: Router, private location: Location, private route: ActivatedRoute, private api: apiService) { 
    
    
  }
  // gotoGroupFeed() {
  //   this.router.navigate(['/group-feed']);
  // }
  ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options);
   
   }
   
   singlediscussion(idd) {
    // var index =  this.topics.find(  ({ id }) => id ===  idd);
    var index:any  =  this.topics.find(  data => data.id ===  idd);
     index.forum_name=this.data.title;
    console.log(index);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(index)
      }
    };
    this.router.navigate(['forum-reply'], navigationExtras);
  }
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options);
     this.navCtrl.navigateForward(page);
   
   }
  ngOnInit() {
  
      this.id1  =this.route.snapshot.paramMap.get('id');
   

   
    this.api.getForumDetails( this.id1).subscribe(res => {
      console.log(res);
      this.data = res;
      console.log( this.data);
      this.api.getuser(this.data['author']).subscribe((res : any) => {
        console.log(res);
        this.creator = res;
        console.log( this.creator['name']);
        this.data.admin_name= res.name;
        this.data.admin_image= res.avatar_urls['full'];
      });
      this.api.getForumTopics( this.id1).subscribe((res : any) => {

        this.topics = res;
        
  
        console.log("topics",this.topics);
      });
    });
    
   
   
  }
  BackButton(){
    this.location.back();
  }
  members() {
   
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: this.id1,
        name: this.data.name
      }
    };
    this.router.navigate(['members-list'], navigationExtras);
  }
  photos() {
   
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: this.id1,
        name: this.data.name,
        type: "group_id"
      }
    };
    this.router.navigate(['photo-gallery'], navigationExtras);
  }
}

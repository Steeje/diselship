/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { apiService } from '../../services/api.service';
import { Storage } from '@ionic/storage';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, ToastController } from '@ionic/angular';
import swal from 'sweetalert2';
import { Platform, ActionSheetController, LoadingController } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import {File, FileEntry} from '@ionic-native/file/ngx';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController } from '@ionic/angular';
import { Location } from "@angular/common";

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  data: any = {};
  loading:boolean; loading2:boolean;
  loading3:boolean;
  imageResponse: any;
  options: any;
  userForm: FormGroup;
  edit_icon:boolean;
  edit_icon2:boolean;
  images: apiService[] = [];
  user = this.api.getCurrentUser();
  posts = [];
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
  @ViewChild('fileInput2', { static: false }) fileInput2: ElementRef;
  options1: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  constructor(
    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, 
    private imagePicker: ImagePicker,
    private plt: Platform, private location: Location,
    private file: File,
    private router: Router,
    private api: apiService,
    private camera: Camera,
    private androidPermissions: AndroidPermissions,
    private loadingController: LoadingController,
    private storage: Storage,
    private fb: FormBuilder,
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController
  ) {
    
    
    this.edit_icon=true;
    this.edit_icon2=true;
    this.user.subscribe(user => {
      if (user) {
    
          console.log( user);
      
       
      } else {
        this.posts = [];
      }
      this.storage.get('USER_INFO').then(result => {
        if (result != null) {
        console.log( result);
        this.data.id = result.id;
        this.data.nickname = result.nickname;
        if(typeof result.nickname === "undefined" )
        this.data.nickname = result.user_login;
        this.data.firstname = result.first_name;
        if(typeof result.first_name === "undefined" )
        this.data.firstname = result.xprofile.groups[1].fields[1].value.raw;
        this.data.lastname = result.last_name;
        if(typeof result.last_name === "undefined" )
        this.data.lastname =  result.xprofile.groups[1].fields[2].value.raw;
        this.data.description = result.description;
        this.data.avatar=result.avatar_urls[24];
        this.data.cover=result.cover_url;
        console.log(result.avatar_urls[24]);
        if(typeof result.avatar_urls[24] === "undefined" )
        this.data.avatar=result.avatar_urls['full'];

        this.data.email = result.email;
        if(typeof result.email === "undefined" )
        this.data.email = user.user_email;
          
        }
        }).catch(e => {
        console.log('error: '+ e);
        // Handle errors here
        });
       
          
    });


  }
  BackButton(){
    this.location.back();
  } 
  ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options1);
   
   }
   
   
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options1);
     this.navCtrl.navigateForward(page);
   
   }
  ngOnInit() {
    this.userForm = this.fb.group({
      nickname: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      description: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  loadPrivatePosts() {
    this.api.getPrivatePosts().subscribe(res => {
      this.posts = res;
    });
  }
  // Used for browser direct file upload
  


















//   readFile(file) {
//     console.log(file.type);
//     const reader = new FileReader();
//     reader.onload = () => {
//       const blob = new Blob([reader.result], {
//         type: file.type
//       });
//       this.api.uploadImage(blob, file.name, file.type,this.data.id).subscribe(  async (res : any)  => {
//         console.log("---------------");
//         console.log(res);
//         this.loading=false;
//         this.edit_icon=true;
//   // Get the entire data
//   this.storage.get('USER_INFO').then(valueStr => {
//     let value = valueStr ? JSON.parse(valueStr) : {};
  
//      // Modify just that property
//      value.avatar_urls[24] = res.thumb;
//      value.avatar_urls[96] = res.full;
  
//      // Save the entire data again
//      this.storage.set('USER_INFO', JSON.stringify(value));
//      window.location.reload();
//   });
  
       
//       },
//       err => {
//         console.log(err);
//         this.showError(err);
//       }
//       );


//     };
//     reader.readAsArrayBuffer(file);
//   };

//   async addImage2() {
//     this.loading3=true;
//     this.edit_icon2=false;
//     const options: CameraOptions = {
//       quality: 100,
//       destinationType: this.camera.DestinationType.FILE_URI,
//       encodingType: this.camera.EncodingType.JPEG,
//       mediaType: this.camera.MediaType.PICTURE
//     }
    
//     this.camera.getPicture(options).then((imageData) => {
     
//       this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => 
//       {
//         entry.file(file => {
//           console.log(file);
//           this.readFile2(file);
//         });
//       });

//   }, (err) => {
//     // Handle error
//    });
//   }
//   readFile2(file) {
//     console.log(file.type);
//     const reader = new FileReader();
//     reader.onload = () => {
//       const blob = new Blob([reader.result], {
//         type: file.type
//       });
//       this.api.uploadImage2(blob, file.name, file.type,this.data.id).subscribe(  async (res : any)  => {
//         console.log("---------------");
//         console.log(res);
//         this.loading=false;
//         this.edit_icon=true;
//   // Get the entire data
//   this.storage.get('USER_INFO').then(valueStr => {
//     let value = valueStr ? JSON.parse(valueStr) : {};
  
//      // Modify just that property
//      value.avatar_urls[24] = res.thumb;
//      value.avatar_urls[96] = res.full;
  
//      // Save the entire data again
//      this.storage.set('USER_INFO', JSON.stringify(value));
//      window.location.reload();
//   });
  
       
//       },
//       err => {
//         console.log(err);
//         this.showError(err);
//       }
//       );


//     };
//     reader.readAsArrayBuffer(file);
//   };

//   // Helper function
//   // https://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
//   b64toBlob(b64Data, contentType = '', sliceSize = 512) {
//     const byteCharacters = atob(b64Data);
//     const byteArrays = [];
 
//     for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
//       const slice = byteCharacters.slice(offset, offset + sliceSize);
 
//       const byteNumbers = new Array(slice.length);
//       for (let i = 0; i < slice.length; i++) {
//         byteNumbers[i] = slice.charCodeAt(i);
//       }
 
//       const byteArray = new Uint8Array(byteNumbers);
//       byteArrays.push(byteArray);
//     }
 
//     const blob = new Blob(byteArrays, { type: contentType });
//     return blob;
//   }
//   async selectImageSource() {
//     const buttons = [
//       {
//         text: 'Take Photo',
//         icon: 'camera',
//         handler: () => {
//           this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
//             result => console.log('Has permission?',result.hasPermission),
//             err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
//           );
          
//           this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
//           this.addImage();
//         }
//       },
//       {
//         text: 'Choose From Photos Photo',
//         icon: 'image',
//         handler: () => {
//           this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
//             result => console.log('Has permission?',result.hasPermission),
//             err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
//           );
          
//           this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
//           this.selectImage();
//         }
//       }
//     ];
 
//     // Only allow file selection inside a browser
//     if (!this.plt.is('hybrid')) {
//       buttons.push({
//         text: 'Choose a File',
//         icon: 'attach',
//         handler: () => {
//           this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
//             result => console.log('Has permission?',result.hasPermission),
//             err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
//           );
          
//           this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
//           this.fileInput.nativeElement.click();
//         }
//       });
//     }
 
//     const actionSheet = await this.actionSheetCtrl.create({
//       header: 'Select Image Source',
//       buttons
//     });
//     await actionSheet.present();
//   }
//   async selectcoverImageSource() {
//     const buttons = [
//       {
//         text: 'Take Photo',
//         icon: 'camera',
//         handler: () => {
//           this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
//             result => console.log('Has permission?',result.hasPermission),
//             err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
//           );
          
//           this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
//           this.addImage2();
//         }
//       },
//       {
//         text: 'Choose From Photos Photo',
//         icon: 'image',
//         handler: () => {
//           this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
//             result => console.log('Has permission?',result.hasPermission),
//             err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
//           );
          
//           this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
//           this.selectImage2();
//         }
//       }
//     ];
 
//     // Only allow file selection inside a browser
//     if (!this.plt.is('hybrid')) {
//       buttons.push({
//         text: 'Choose a File',
//         icon: 'attach',
//         handler: () => {
//           this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
//             result => console.log('Has permission?',result.hasPermission),
//             err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
//           );
          
//           this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
//           this.fileInput2.nativeElement.click();
//         }
//       });
//     }
 
//     const actionSheet = await this.actionSheetCtrl.create({
//       header: 'Select Image Source',
//       buttons
//     });
//     await actionSheet.present();
//   }



//   async selectImage() {
//     this.loading=true;
//     this.edit_icon=false;
//     this.options = {
//       // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
//       // selection of a single image, the plugin will return it.
//       //maximumImagesCount: 3,

//       // max width and height to allow the images to be.  Will keep aspect
//       // ratio no matter what.  So if both are 800, the returned image
//       // will be at most 800 pixels wide and 800 pixels tall.  If the width is
//       // 800 and height 0 the image will be 800 pixels wide if the source
//       // is at least that wide.
//       width: 200,
//       //height: 200,

//       // quality of resized image, defaults to 100
//       quality: 25,

//       // output type, defaults to FILE_URIs.
//       // available options are 
//       // window.imagePicker.OutputType.FILE_URI (0) or 
//       // window.imagePicker.OutputType.BASE64_STRING (1)
//       outputType: 1
//     };
//     this.imageResponse = [];
//     this.imagePicker.getPictures(this.options).then((results) => {
//       for (var i = 0; i < results.length; i++) {
     
//         this.file.resolveLocalFilesystemUrl(results[i]).then((entry: FileEntry) => 
//       {
//         entry.file(file => {
//           console.log(file);
//           this.readFile(file);
//         });
//       });
//   } }, (err) => {
//     alert(err);
//   });
  
//   }
//   async selectImage2() {
//     this.loading3=true;
//     this.edit_icon2=false;
//     this.options = {
//       // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
//       // selection of a single image, the plugin will return it.
//       //maximumImagesCount: 3,

//       // max width and height to allow the images to be.  Will keep aspect
//       // ratio no matter what.  So if both are 800, the returned image
//       // will be at most 800 pixels wide and 800 pixels tall.  If the width is
//       // 800 and height 0 the image will be 800 pixels wide if the source
//       // is at least that wide.
//       width: 200,
//       //height: 200,

//       // quality of resized image, defaults to 100
//       quality: 25,

//       // output type, defaults to FILE_URIs.
//       // available options are 
//       // window.imagePicker.OutputType.FILE_URI (0) or 
//       // window.imagePicker.OutputType.BASE64_STRING (1)
//       outputType: 1
//     };
//     this.imageResponse = [];
//     this.imagePicker.getPictures(this.options).then((results) => {
//       for (var i = 0; i < results.length; i++) {
      
//         this.file.resolveLocalFilesystemUrl(results[i]).then((entry: FileEntry) => 
//       {
//         entry.file(file => {
//           console.log(file);
//           this.readFile2(file);
//         });
//       });
//   }
// }, (err) => {
//   alert(err);
// });
//   }
//   goToprofile() {
//     this.router.navigate(['/member-detail']);
//   }
//   goToHome(res) {
//     swal.fire({
//       title: 'Success',
//       text: 'Thank you for registrations',
//       icon: 'success',
//       backdrop: false,
//     });
//     this.router.navigate(['/tabs/home-new']);
//   }
// //   uploadCoverFile(event: EventTarget) {
// //     this.loading3=true;
// //     this.edit_icon2=false;
// //     const eventObj: MSInputMethodContext = event as MSInputMethodContext;
// //     const target: HTMLInputElement = eventObj.target as HTMLInputElement;
// //     const file: File = target.files[0];
// //     this.api.uploadImageFile2(file,this.data.id).subscribe(
// //       async (res : any) => {
// //         this.loading3=false;
// //         this.edit_icon2=true;
// //       console.log("---------------");
// //       console.log(res);
// //       const toast = await this.toastCtrl.create({
// //         message: 'Profile Cover Image Updated',
// //         duration: 3000
// //       });
// //       toast.present();
// // // Get the entire data
// // this.storage.get('USER_INFO').then(valueStr => {
// //   let value = valueStr ? valueStr : {};

// //    // Modify just that property
// //    value.cover_url = res.image;
  
// // this.data.cover_url=res.image;
// //    // Save the entire data again
// //    this.storage.set('USER_INFO', value);
// //     window.location.reload();
// // });

     
// //     },
// //     err => {
// //       this.loading3=false;
// //       this.edit_icon2=true;
// //       console.log(err);
// //       this.showError(err);
// //     }
    
// //     );
// //   }
//   uploadFile(event: EventTarget) {
//     this.loading=true;
//     this.edit_icon=false;
//     const eventObj: MSInputMethodContext = event as MSInputMethodContext;
//     const target: HTMLInputElement = eventObj.target as HTMLInputElement;
//     const file1 = target.files[0];
//     this.api.uploadImageFile(file1,this.data.id).subscribe(
//       async (res : any) => {
//         this.loading=false;
//         this.edit_icon=true;
//       console.log("---------------");
//       console.log(res);
//       const toast = await this.toastCtrl.create({
//         message: 'Profile Image Updated',
//         duration: 3000
//       });
//       toast.present();
// // Get the entire data
// this.storage.get('USER_INFO').then(valueStr => {
//   let value = valueStr ? valueStr : {};

//    // Modify just that property
//    value.avatar_urls[24] = res.thumb;
//    value.avatar_urls[96] = res.full;
// this.data.avatar=res.thumb;
//    // Save the entire data again
//    this.storage.set('USER_INFO', value);
//    window.location.reload();
// });

     
//     },
//     err => {
//       this.loading=false;
//       this.edit_icon=true;
//       console.log(err);
//       this.showError(err);
//     }
    
//     );
//   }




















  
  async selectImage() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
      result => console.log('Has permission?',result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
    );
    
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    // this.loading=true;
  
    let options = {
    
      maximumImagesCount: 1,
 
      width: 200,
     
      quality: 25,

      outputType: 1
    };
    let imageResponse = [];
    this.imagePicker.getPictures(options).then(async (results) => {
      
  
  
  
        var imageData = results[0].toString();
      //  var imageData1 = imageData.toString();
        var byteCharacters = atob(imageData.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
        var byteNumbers = new Array(byteCharacters.length);
        for (var i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        
        var byteArray = new Uint8Array(byteNumbers);
        var blob1 = new Blob([ byteArray ], {
           type : "jpg"
        });
        console.log(blob1);

        this.api.uploadImage(blob1,"test","jpg",this.data.id).subscribe(  async (res : any)  => {
          console.log("---------------");
          console.log(res);
          this.loading=false;
          this.edit_icon=true;
    // Get the entire data
    this.storage.get('USER_INFO').then(valueStr => {
      let value = valueStr ? JSON.parse(valueStr) : {};
    
       // Modify just that property
       value.avatar_urls[24] = res.thumb;
       value.avatar_urls[96] = res.full;
       this.data.avatar=res.thumb;
     
       // Save the entire data again
       this.storage.set('USER_INFO', JSON.stringify(value));
       window.location.reload();
    });



      // Get the entire data
      
      
           
          },
          err => {
            console.log(err);
            this.showError(err);
          }
          
          );
     //   });
    //  });
  //  } 
  }, (err) => {
    alert(err);
  });
  
  }
   async uploadFile(event: EventTarget) {
  
       this.loading=true;
    this.edit_icon=false;
        const loading = await this.loadingController.create({
        
          message: 'Please wait...',
          duration: 2000
        });
        await loading.present();
      const eventObj: MSInputMethodContext = event as MSInputMethodContext;
      const target: HTMLInputElement = eventObj.target as HTMLInputElement;
      const file1 = target.files[0];
      this.api.uploadImageFile(file1,this.data.id).subscribe(
        async (res : any) => {
          this.loading=false;
          this.edit_icon=true;
        const toast = await this.toastCtrl.create({
          message: 'Image Uploaded',
          duration: 3000
        });
        toast.present();
  // Get the entire data
  
  this.storage.get('USER_INFO').then(valueStr => {
    let value = valueStr ? valueStr : {};
  
     // Modify just that property
     value.avatar_urls[24] = res.thumb;
     value.avatar_urls[96] = res.full;
  this.data.avatar=res.thumb;
     // Save the entire data again
     this.storage.set('USER_INFO', value);
     
  });
       
      },
      err => {
        console.log(err);
        this.showError(err);
      }
      
      );
    }



    async selectImage2() {
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
        result => console.log('Has permission?',result.hasPermission),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
      );
      
      this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
      // this.loading=true;
    
      let options = {
      
        maximumImagesCount: 1,
   
        width: 200,
       
        quality: 25,
  
        outputType: 1
      };
      let imageResponse = [];
      this.imagePicker.getPictures(options).then(async (results) => {
        this.loading=true;
        this.edit_icon=false;
    
    
    
          var imageData = results[0].toString();
        //  var imageData1 = imageData.toString();
          var byteCharacters = atob(imageData.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
          var byteNumbers = new Array(byteCharacters.length);
          for (var i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
          }
          
          var byteArray = new Uint8Array(byteNumbers);
          var blob1 = new Blob([ byteArray ], {
             type : "jpg"
          });
          console.log(blob1);


          this.api.uploadImage2(blob1,"test", "jpg",this.data.id).subscribe(  async (res : any)  => {
            console.log("---------------");
            console.log(res);
            this.loading=false;
            this.edit_icon=true;
            this.data.cover=res.image;
      // Get the entire data
      this.storage.get('USER_INFO').then(valueStr => {
        let value = valueStr ? JSON.parse(valueStr) : {};
      
         // Modify just that property
         value.cover_url = res.image;
         this.data.cover=res.image;
         // Save the entire data again
         this.storage.set('USER_INFO', JSON.stringify(value));
         window.location.reload();
      });





        // Get the entire data
        
        
             
            },
            err => {
              console.log(err);
              this.showError(err);
            }
            
            );
       //   });
      //  });
    //  } 
    }, (err) => {
      alert(err);
    });
    
    }
     async uploadCoverFile(event: EventTarget) {
      this.loading3=true;
              this.edit_icon2=false;
     
          const loading = await this.loadingController.create({
          
            message: 'Please wait...',
            duration: 2000
          });
          await loading.present();
        const eventObj: MSInputMethodContext = event as MSInputMethodContext;
        const target: HTMLInputElement = eventObj.target as HTMLInputElement;
        const file1 = target.files[0];
   
    // Get the entire data
    this.api.uploadImageFile2(file1,this.data.id).subscribe(
            async (res : any) => {
              this.loading3=false;
              this.edit_icon2=true;
            console.log("---------------");
            console.log(res);
            const toast = await this.toastCtrl.create({
              message: 'Profile Cover Image Updated',
              duration: 3000
            });
            toast.present();
      // Get the entire data
      this.data.cover=res.image;
      this.storage.get('USER_INFO').then(valueStr => {
        let value = valueStr ? valueStr : {};
      
         // Modify just that property
         value.cover_url = res.image;
        
    
         // Save the entire data again
         this.storage.set('USER_INFO', value);
          // window.location.reload();
      });
    
         
        },
        err => {
          console.log(err);
          this.showError(err);
        }
        
        );
      }
  








  async selectImageSource() {
    const buttons = [
      
      {
        text: 'Choose From Photos Photo',
        icon: 'image',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          this.selectImage();
        }
      }
    ];
 
    // Only allow file selection inside a browser
    if (!this.plt.is('hybrid')) {
      buttons.push({
        text: 'Choose a File',
        icon: 'attach',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          this.fileInput.nativeElement.click();
        }
      });
    }
 
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons
    });
    await actionSheet.present();
  }
  async selectcoverImageSource() {
    const buttons = [
     
      {
        text: 'Choose From Photos Photo',
        icon: 'image',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          this.selectImage2();
        }
      }
    ];
 
    // Only allow file selection inside a browser
    if (!this.plt.is('hybrid')) {
      buttons.push({
        text: 'Choose a File',
        icon: 'attach',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          this.fileInput2.nativeElement.click();
        }
      });
    }
 
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons
    });
    await actionSheet.present();
  }
  goToprofile() {
    this.router.navigate(['/member-detail']);
  }
  goToHome(res) {
    swal.fire({
      title: 'Success',
      text: 'Thank you for registrations',
      icon: 'success',
      backdrop: false,
    });
    this.router.navigate(['/tabs/home-new']);
  }
//   uploadCoverFile(event: EventTarget) {
//     this.loading3=true;
//     this.edit_icon2=false;
//     const eventObj: MSInputMethodContext = event as MSInputMethodContext;
//     const target: HTMLInputElement = eventObj.target as HTMLInputElement;
//     const file: File = target.files[0];
//     this.api.uploadImageFile2(file,this.data.id).subscribe(
//       async (res : any) => {
//         this.loading3=false;
//         this.edit_icon2=true;
//       console.log("---------------");
//       console.log(res);
//       const toast = await this.toastCtrl.create({
//         message: 'Profile Cover Image Updated',
//         duration: 3000
//       });
//       toast.present();
// // Get the entire data
// this.storage.get('USER_INFO').then(valueStr => {
//   let value = valueStr ? valueStr : {};

//    // Modify just that property
//    value.cover_url = res.image;
  
// this.data.cover_url=res.image;
//    // Save the entire data again
//    this.storage.set('USER_INFO', value);
//     window.location.reload();
// });

     
//     },
//     err => {
//       this.loading3=false;
//       this.edit_icon2=true;
//       console.log(err);
//       this.showError(err);
//     }
    
//     );
//   }
  
  

  userupdate() {
    this.loading2=true;

    this.api.userupdate(this.data.id,
      this.userForm.value.firstname, 
      this.userForm.value.email, 
      this.userForm.value.password,
      this.userForm.value.lastname,
      this.userForm.value.nick_name,
      this.userForm.value.description).subscribe(
      async res => {
        this.loading2=false;
        console.log(res);
        const toast = await this.toastCtrl.create({
          message: 'Profile Updated',
          duration: 3000
        });
        toast.present();
     
        
        this.storage.set('USER_INFO', res);
        this.router.navigate(['/member-detail']);
        
        window.location.reload();
      
      },
      err => {
        this.loading2=false;
        console.log(err);
        this.showError(err);
      }
    );
  }
 
  async showError(err) {
    const alert = await this.alertCtrl.create({
      header: err.error.code,
      subHeader: err.error.data,
      message: err.error.message,
      buttons: ['OK']
    });
    await alert.present();
  }
  // goToHome() {
  //   swal.fire({
  //     title: 'Success',
  //     text: 'Thank you for registrations',
  //     icon: 'success',
  //     backdrop: false,
  //   });
  //   this.router.navigate(['/tabs/home-new']);
  // }

}

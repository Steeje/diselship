import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MembersActivityPage } from './members-activity.page';

describe('MembersActivityPage', () => {
  let component: MembersActivityPage;
  let fixture: ComponentFixture<MembersActivityPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MembersActivityPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MembersActivityPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MembersActivityPageRoutingModule } from './members-activity-routing.module';

import { MembersActivityPage } from './members-activity.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MembersActivityPageRoutingModule
  ],
  declarations: [MembersActivityPage]
})
export class MembersActivityPageModule {}

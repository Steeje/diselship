/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { apiService } from '../../services/api.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Location } from "@angular/common";
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { ActionSheetController, MenuController,NavController } from '@ionic/angular';

@Component({
  selector: 'app-group-feed',
  templateUrl: './group-feed.page.html',
  styleUrls: ['./group-feed.page.scss'],
})
export class GroupFeedPage implements OnInit {
  user: any = this.api.getCurrentUser();
  posts :any = ["0"];
  categoryId: any;
  categoryTitle: any;
  slideoptions = {
    slidesPerView: 1.2,
  };
  
  grpid;
  categories: any;
  data: any;
  showSlider = false;
  chips = ['ADD PHOTOS', 'ADD CONNECTIONS', 'HASHTAGS FOLLOWED'];
  options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  constructor(
    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, private location: Location, private socialSharing: SocialSharing, private actionSheet: ActionSheetController,private route: ActivatedRoute, private api: apiService, private menuCtrl: MenuController, private router: Router) 
  {  this.posts.avatar_urls.thumb=""; }

  ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options);
   
   }
   
   
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options);
     this.navCtrl.navigateForward(page);
   
   }
  ngOnInit() {
    this.route.queryParams.subscribe(data => {
      console.log("data.id",data.id);
      this.grpid = data.id;
     
    });
    if(this.grpid){
      this.api.getActivitydetails('group_id', this.grpid).subscribe(res => {
        this.posts = res;
      });
      this.api.getGroupdetails( this.grpid).subscribe(res => {
       
        this.data = res;
      
      });
    }
      else{
        this.api.getActivityall().subscribe(res => {
          this.posts = res;
        });}


    
    // this.route.data.subscribe(routeData => {
    //   const data = routeData['data'];
    
    //     for (let post of data) {
    //       if (post['_embedded']['wp:featuredmedia']) {
    //         post.media_url =
    //           post['_embedded']['wp:featuredmedia'][0]['media_details'].sizes['medium'].source_url;
    //       }
    //     }
       
    //   this.posts = data.posts;
    //   this.categoryId = data.categoryId;
    //   this.categoryTitle = data.categoryTitle;
    // })
  }
  BackButton(){
    this.location.back();
  }
  sliderShow(value) {
    this.showSlider = value;
  }
  
  loadData(event: any) {
    const page = (Math.ceil(this.posts.length / 10)) + 1;
    if(this.grpid){
    this.api.getgroupfeeds('group_id', this.grpid, page)
    .subscribe((newPagePosts: []) => {
      this.posts.push(...newPagePosts);
      event.target.complete();
    }, err => {
      // there are no more posts available
      event.target.disabled = true;
    })
  }else{
    this.api.getActivityall( page)
    .subscribe((newPagePosts: []) => {
      this.posts.push(...newPagePosts);
      event.target.complete();
    }, err => {
      // there are no more posts available
      event.target.disabled = true;
    })
  }
  }
  ionViewDidLoad(){
    this.api.retrieveCategories().subscribe(results => {
      this.categories = results;
    });
   }
 

  singlefeed(idd) {
    var index =  this.posts.find(  ({ id }) => id ===  idd);
    if(this.grpid)
    index.media_url= this.data.media_url;
    let navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(index)
      }
    };
    this.router.navigate(['single-feed'], navigationExtras);
  }
  likefeed(idd) {
    this.api.activitylike(idd).subscribe((res:any) => {
     console.log(res);
     var index =  this.posts.findIndex(  ({ id }) => id ===  res.id);

        if(typeof (index) !== 'undefined') 
        this.posts.splice(index, 1, res);
                console.log(this.posts);
    });
  }
  async openActionSheet() {
    const actionSheet = await this.actionSheet.create({
      mode: 'md',
      buttons: [
        {
          text: 'Save',
          role: 'destructive',
          icon: 'bookmark-outline',
          handler: () => {
            console.log('Delete clicked');
          }
        },
        {
          text: 'Send in a private Message',
          icon: 'chatbox-ellipses-outline',
          handler: () => {
            console.log('Share clicked');
          }
        },
        {
          text: 'Share via',
          icon: 'share-social',
          handler: () => {
            console.log('Play clicked');
          }
        },
        {
          text: 'Hide this post',
          icon: 'close-circle-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Unfollow',
          icon: 'person-remove-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Report this post',
          icon: 'flag-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Improve my feed',
          icon: 'options-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Who can see this post?',
          icon: 'eye-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }
  async openShareSheet(idd) {
    var data =  this.posts.find(  ({ id }) => id ===  idd);

    const actionSheet = await this.actionSheet.create({
      mode: 'md',
      buttons: [
        {
          text: 'Share as Text',
          icon: 'share-social-outline',
          handler: () => {
            console.log('Delete clicked');
            if(typeof (data) !== 'undefined') 
            this.socialSharing.share(data.content_stripped, 'MEDIUM', null, data.title)
          }
        },
        {
          text: 'Send Mail',
          icon: 'mail-outline',
          handler: () => {
            console.log('Share clicked');
            this.socialSharing.shareViaEmail(data.content_stripped, data.title, [this.user.user_email])
          }
        },
        {
          text: 'Share via Facebook',
          icon: 'logo-facebook',
          handler: () => {
            console.log('facebook clicked');
            this.socialSharing.shareViaFacebookWithPasteMessageHint(data.content_stripped, this.data.media_url, data.link);
          }
        },

 
        {
          text: 'Share via Whatsapp',
          icon: 'logo-whatsapp',
          handler: () => {
            if(typeof (data) !== 'undefined') 
            this.socialSharing.shareViaWhatsApp(data.content_stripped, this.data.media_url, data.link);
            console.log('whatsapp clicked');
          }
        },
        {
          text: 'Share via Twitter',
          icon: 'logo-twitter',
          handler: () => {
            if(typeof (data) !== 'undefined') 
            this.socialSharing.shareViaTwitter(data.content_stripped, this.data.media_url, data.link);
          }
        },
        {
          text: 'Share via Instagram',
          icon: 'logo-instagram',
          handler: () => {
            if(typeof (data) !== 'undefined') 
            this.socialSharing.shareViaTwitter(data.content_stripped, this.data.media_url);
          }
        },
      ]
    });
    await actionSheet.present();
  }

  openMenu() {
    this.menuCtrl.open();
  }

  onSearchChange(event) {

  }

  goToChatList() {
    this.router.navigate(['/chatlist']);
  }
  goTosinglePost() {
    this.router.navigate(['/single-post']);
  }

}

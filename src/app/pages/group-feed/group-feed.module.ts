import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GroupFeedPageRoutingModule } from './group-feed-routing.module';



import { Routes, RouterModule } from '@angular/router';



import { GroupFeedPage } from './group-feed.page';
import { PostsResolver } from './group-feed.resolver';

const routes: Routes = [
  {
    path: '',
    component: GroupFeedPage,
    resolve: {
      data: PostsResolver
    },
    runGuardsAndResolvers: 'paramsOrQueryParamsChange' // because we use the same route for all posts and for category posts, we need to add this to refetch data 
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GroupFeedPageRoutingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GroupFeedPage],
  providers: [PostsResolver]
})
export class GroupFeedPageModule {}



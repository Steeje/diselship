import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GroupFeedPage } from './group-feed.page';

describe('GroupFeedPage', () => {
  let component: GroupFeedPage;
  let fixture: ComponentFixture<GroupFeedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupFeedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GroupFeedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

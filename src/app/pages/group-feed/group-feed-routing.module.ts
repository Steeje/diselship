import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GroupFeedPage } from './group-feed.page';

const routes: Routes = [
  {
    path: '',
    component: GroupFeedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GroupFeedPageRoutingModule {}

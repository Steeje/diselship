import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { apiService } from '../../services/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable()
export class PostsResolver implements Resolve<any> {

  constructor(private api: apiService,) { }

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    const categoryId = route.queryParams['categoryId'];
    const categoryTitle = route.queryParams['title'];

    return this.api.getRecentPosts(categoryId)
    .pipe(
      map((posts) => {
        return { posts, categoryTitle, categoryId };
      })
    )
  }
}
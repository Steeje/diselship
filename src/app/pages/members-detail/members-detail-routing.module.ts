import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MembersDetailPage } from './members-detail.page';

const routes: Routes = [
  {
    path: '',
    component: MembersDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MembersDetailPageRoutingModule {}

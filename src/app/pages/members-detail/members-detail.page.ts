/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { AlertController, ToastController, Platform  } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { apiService } from '../../services/api.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { Location } from "@angular/common";

@Component({
  selector: 'app-members-detail',
  templateUrl: './members-detail.page.html',
  styleUrls: ['./members-detail.page.scss'],
})
export class MembersDetailPage implements OnInit {
  data: any = {};
  user = this.api.getCurrentUser();
  id;token;user_id;
  constructor(  private api: apiService,
    private storage: Storage,    private location: Location, private route : ActivatedRoute , private toastCtrl: ToastController, private router: Router) {
      this.id  =this.route.snapshot.paramMap.get('id');
      this.storage.get('COMPLETE_USER_INFO').then(result => {
        if (result != null) {
        console.log( result);
        this.token=result.token;
       
        this.user_id=result.user_id;
       
        console.log('user_id: '+  result.user_id);
        console.log('token: '+  result.token);
        }
        }).catch(e => {
        console.log('error: '+ e);
        // Handle errors here
        });
    this.user.subscribe(user => {
      if (user) {
        // location.href = "/home-new";
      
        this.api.getuser( this.id).subscribe((result:any) => {
          console.log(result);
          this.data = result;
          console.log( this.data);
          this.data.id = result.id;
         
          this.data.username = result.user_login;
          this.data.cover=result.cover_url;
          this.data.fullname=result.xprofile.groups[1].fields[1].value.raw+" "+ result.xprofile.groups[1].fields[2].value.raw;
          this.data.description = result.description;
        
            this.data.avatar=result.avatar_urls['full'];
   
        });  
        }
   });
  }
  unfollowuser(){
    this.api.followuser("unfollow",this.data.id).subscribe(  async (res:any)  => {
      console.log( res.data);
      var result = res.data;
        this.data = result;
          console.log( this.data);
          this.data.id = result.id;
         
          this.data.username = result.user_login;
          this.data.cover=result.cover_url;
          this.data.fullname=result.xprofile.groups[1].fields[1].value.raw+" "+ result.xprofile.groups[1].fields[2].value.raw;
          this.data.description = result.description;
        
            this.data.avatar=result.avatar_urls['full'];
      const toast = await this.toastCtrl.create({
        message: "Unfollow User Successfully",
        duration: 3000
      });
      await toast.present();
     });
  }
  connectuser(){
    this.api.connectfrds( this.data.id,this.token,this.user_id).subscribe( async (res:any)  => {
      var result = res.data;
        // this.data = result;
        //   console.log( this.data);
        //   this.data.id = result.id;
         
        //   this.data.username = result.user_login;
        //   this.data.cover=result.cover_url;
        //   this.data.fullname=result.xprofile.groups[1].fields[1].value.raw+" "+ result.xprofile.groups[1].fields[2].value.raw;
        //   this.data.description = result.description;
        this.data.friendship_status= "pending";
          
      const toast = await this.toastCtrl.create({
        message: "Friend Request Sent Successfully",
        duration: 3000
      });
      await toast.present();
     });
  }
  deleteuser(){
    this.api.deletefrds( this.data.id).subscribe( async (res:any)  => {
      var result = res.data;
      this.data.friendship_status= "not_friends";
      const toast = await this.toastCtrl.create({
        message: "Unfriend Successfully",
        duration: 3000
      });
      await toast.present();
     });
  }
  BackButton(){
    this.location.back();
  } 
  followuser(){
    this.api.followuser("follow", this.data.id).subscribe( async (res:any)  => {
      console.log( res.data);
     var result = res.data;
        this.data = result;
          console.log( this.data);
          this.data.id = result.id;
         
          this.data.username = result.user_login;
          this.data.cover=result.cover_url;
          this.data.fullname=result.xprofile.groups[1].fields[1].value.raw+" "+ result.xprofile.groups[1].fields[2].value.raw;
          this.data.description = result.description;
        
            this.data.avatar=result.avatar_urls['full'];
   
      const toast = await this.toastCtrl.create({
        message: "Follow User Successfully",
        duration: 3000
      });
      await toast.present();
     });
  }


  editProfile() {
    this.router.navigate(['/members-profile']);
  }
  detailsProfile() {
    this.router.navigate(['/profile-detail']);
  }
  ngOnInit() {
  }

}

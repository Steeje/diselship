import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MembersDetailPageRoutingModule } from './members-detail-routing.module';

import { MembersDetailPage } from './members-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MembersDetailPageRoutingModule
  ],
  declarations: [MembersDetailPage]
})
export class MembersDetailPageModule {}

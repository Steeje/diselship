import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MembersDetailPage } from './members-detail.page';

describe('MembersDetailPage', () => {
  let component: MembersDetailPage;
  let fixture: ComponentFixture<MembersDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MembersDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MembersDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { apiService } from '../../services/api.service';
import { Location } from "@angular/common";
import { File } from '@ionic-native/file/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController } from '@ionic/angular';

@Component({
  selector: 'app-photo-gallery',
  templateUrl: './photo-gallery.page.html',
  styleUrls: ['./photo-gallery.page.scss'],
})
export class PhotoGalleryPage implements OnInit {
  data: any = {};
  creator: any = {};
  id1: any;
  id;
  type;
  name;
  galleryType: String;
  options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  constructor(
    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, private router: Router,  private photoViewer: PhotoViewer,
    private file: File,private location: Location, private route: ActivatedRoute, private api: apiService) { 
      setTimeout(()=>{
        console.log("Hello world")
        this.galleryType = "regular";
  //2000 is  millisecond it  means this program print hello world  every 2 second
  } ,  3000)
    
  }
  // gotoGroupFeed() {
  //   this.router.navigate(['/group-feed']);
  // }
  ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options);
   
   }
   
   openimage(src){
    this.photoViewer.show(src);
   }
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options);
     this.navCtrl.navigateForward(page);
   
   }
  ngOnInit() {
  
    this.route.queryParams.subscribe(data => {
      console.log("data.id",data.id);
      this.id = data.id;
      this.name = data.name;
      this.type = data.type;
    });
   

   
    this.api.photogallery( this.id,this.type).subscribe(res => {
      console.log(res);
      this.data=res;
    });  
  }
  viewPhoto(imageName) {
   
    const ROOT_DIRECTORY = this.file.applicationStorageDirectory;//'file:///sdcard//';
    const downloadFolderName = 'tempDownloadFolder';
    
    //Create a folder in memory location
    this.file.createDir(ROOT_DIRECTORY, downloadFolderName, true)
      .then((entries) => {
 
        //Copy our asset/img/logo.jpg to folder we created
        this.file.copyFile(this.file.applicationDirectory + "www/assets/images/", imageName, ROOT_DIRECTORY + downloadFolderName + '//', imageName)
          .then((entries) => {
 
            this.photoViewer.show(ROOT_DIRECTORY + downloadFolderName + "/" + imageName, 'Do you want to Share', {share: true});
            
           })
          .catch((error) => {
            alert('1 error ' + JSON.stringify(error));
          });
      })
      .catch((error) => {
        alert('2 error' + JSON.stringify(error));
      });
  }
  BackButton(){
    this.location.back();
  }
  members() {
   
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: this.id1,
        name: this.data.name
      }
    };
    this.router.navigate(['members-list'], navigationExtras);
  }
}

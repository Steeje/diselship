import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JobVerificationPage } from './job-verification.page';

describe('RegisterPage', () => {
  let component: JobVerificationPage;
  let fixture: ComponentFixture<JobVerificationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobVerificationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JobVerificationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

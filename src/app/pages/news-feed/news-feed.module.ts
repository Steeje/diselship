import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewsFeedPageRoutingModule } from './news-feed-routing.module';



import { Routes, RouterModule } from '@angular/router';



import { NewsFeedPage } from './news-feed.page';
import { PostsResolver } from './news-feed.resolver';

const routes: Routes = [
  {
    path: '',
    component: NewsFeedPage,
    resolve: {
      data: PostsResolver
    },
    runGuardsAndResolvers: 'paramsOrQueryParamsChange' // because we use the same route for all posts and for category posts, we need to add this to refetch data 
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewsFeedPageRoutingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NewsFeedPage],
  providers: [PostsResolver]
})
export class NewsFeedPageModule {}



/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { apiService } from '../../services/api.service';
import { Location } from "@angular/common";
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController , ToastController} from '@ionic/angular';


@Component({
  selector: 'app-group-invites',
  templateUrl: './group-invites.page.html',
  styleUrls: ['./group-invites.page.scss'],
})
export class GroupInvitesPage implements OnInit {
  data: any = {};
  members: any = {};
  creator: any = {};
  id1: any;
  segId: any;
  options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  constructor(
    public navCtrl: NavController,private toastCtrl: ToastController,
    private nativePageTransitions: NativePageTransitions, private router: Router, private location: Location, private route: ActivatedRoute, private api: apiService) { 
    
    
  }
  // gotoGroupFeed() {
  //   this.router.navigate(['/group-feed']);
  // }
  ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options);
   
   }
   
  segmentChanged(eve) {
    console.log(eve.detail.value);
    this.segId = eve.detail.value;

if(this.segId == 'ion-sb-0'){
  this.api.getgroupMembers(this.id1,"invited").subscribe(res => {
    console.log(res);
    this.members = res;
    
  });
}else{
  this.api.getgroupMembers(this.id1,"invite").subscribe(res => {
    console.log(res);
    this.members = res;
    
  });
}


  }
   
  invite(id){
  this.api.Groupinvite(this.id1,id).subscribe( async res => {
    console.log(res);
    const toast = await this.toastCtrl.create({
      message: "Invite Sent Successfully",
      duration: 3000
    });
    await toast.present();
    this.api.getgroupMembers(this.id1,"invite").subscribe(res => {
      console.log(res);
      this.members = res;
      
    });
    
  });
  }
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options);
     this.navCtrl.navigateForward(page);
   
   }
  ngOnInit() {
  
      this.id1  =this.route.snapshot.paramMap.get('id');
      this.api.getgroupMembers(this.id1,"invited").subscribe(res => {
        console.log(res);
        this.members = res;
        this.segId ='ion-sb-0';
      });

  }
  BackButton(){
    this.location.back();
  }

 
}

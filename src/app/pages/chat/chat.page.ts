/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { dummyDataService } from 'src/app/services/dummy-data.service';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController } from '@ionic/angular';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  users;

  messages = [
    {
      user: 'Rahul Jograna',
      img: 'assets/imgs/user.jpg',
      msg: 'Hello'
    },
    {
      user: 'Jonh Doe',
      img: 'assets/imgs/user2.jpg',
      msg: 'Hii'
    },
    {
      user: 'Rahul Jograna',
      img: 'assets/imgs/user.jpg',
      msg: 'Are you nearby ?'
    },
    {
      user: 'Jonh Doe',
      img: 'assets/imgs/user2.jpg',
      msg: 'I will be there in few mins'
    },
    {
      user: 'Rahul Jograna',
      img: 'assets/imgs/user.jpg',
      msg: 'Ok, I am waitimg at vinmark Store'
    },
    {
      user: 'Jonh Doe',
      img: 'assets/imgs/user2.jpg',
      msg: 'Sorry I am stuck in traffic. Please give me a moment.'
    }
  ];

  options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  constructor(
    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, private dummy: dummyDataService, private router: Router) {
    this.users = this.dummy.users;
  }
  ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options);
   
   }
   
   
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options);
     this.navCtrl.navigateForward(page);
   
   }
  ngOnInit() {
  }
  goToChat() {

  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JobAuthorisedPage } from './job-authorised.page';

describe('RegisterPage', () => {
  let component: JobAuthorisedPage;
  let fixture: ComponentFixture<JobAuthorisedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobAuthorisedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JobAuthorisedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

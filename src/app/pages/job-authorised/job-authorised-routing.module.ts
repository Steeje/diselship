import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobAuthorisedPage } from './job-authorised.page';

const routes: Routes = [
  {
    path: '',
    component: JobAuthorisedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobAuthorisedPageRoutingModule {}
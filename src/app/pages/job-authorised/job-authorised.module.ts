import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobAuthorisedPageRoutingModule } from './job-authorised-routing.module';

import { JobAuthorisedPage } from './job-authorised.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule ,
    IonicModule,
    JobAuthorisedPageRoutingModule
  ],
  declarations: [JobAuthorisedPage]
})
export class JobAuthorisedPageModule {}

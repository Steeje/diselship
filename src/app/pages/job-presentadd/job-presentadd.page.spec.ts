import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JobPresentaddPage } from './job-presentadd.page';

describe('RegisterPage', () => {
  let component: JobPresentaddPage;
  let fixture: ComponentFixture<JobPresentaddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobPresentaddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JobPresentaddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

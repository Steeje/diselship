/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { apiService } from '../../services/api.service';
import { Storage } from '@ionic/storage';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import swal from 'sweetalert2';
import { Platform, ActionSheetController } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import {File, FileEntry} from '@ionic-native/file/ngx';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController } from '@ionic/angular';

@Component({
  selector: 'app-job-presentadd',
  templateUrl: './job-presentadd.page.html',
  styleUrls: ['./job-presentadd.page.scss'],
})
export class JobPresentaddPage implements OnInit {
  data: any = {};
  loading:boolean; loading2:boolean;
  loading3:boolean;
  imageResponse: any;
  token;
  options: any;
  userForm: FormGroup;
  edit_icon:boolean;
  edit_icon2:boolean;
  images: apiService[] = [];
  user = this.api.getCurrentUser();
  posts = [];
  country = [
    {
      value: 'TELENGANA',
    },
    {
      value: 'India',
    },
    {
      value: 'USA',
    },
    {
      value: 'Pakistan',
    },
    {
      value: 'China',
    },
    ];
    cities = [
      {
        value: 'Kakinada',
      },
      {
        value: 'vijayawada',
      },
      {
        value: 'Hyderabad',
      },
      {
        value: 'Tokyo',
      },
     
      ];
      states = [
        {
          value: 'Andhrapradesh',
        },
        {
          value: 'Telangana',
        },
        {
          value: 'Chennai',
        },
        {
          value: 'Mumbai',
        },
       
        ];
    defaultDate = "1987-06-30";
    isSubmitted = false;
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
  @ViewChild('fileInput2', { static: false }) fileInput2: ElementRef;
  options1: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  constructor(
    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, 
    public loadingController: LoadingController,
    private plt: Platform,
    private file: File,
    private router: Router,
    private api: apiService,
    private location: Location,
    private androidPermissions: AndroidPermissions,
    private storage: Storage,
    private fb: FormBuilder,
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController
  ) {
    this.loader();

    this.edit_icon=true;
    this.edit_icon2=true;
    this.user.subscribe(user => {
      if (user) {
        this.api.getJobdetails( this.token,"presentaddress").subscribe(
          async res => { this.data = res;console.log(res); },
          err => {
            this.loading2=false;
            console.log(err);
            this.showError(err);
          }
        );
          console.log( user);
          this.token = user.token;
       
      } 
      // this.storage.get('USER_INFO').then(result => {
      //   if (result != null) {
      //   console.log( result);
      //   this.data.id = result.id;
       
      //   this.data.nickname = result.nickname;
      //   if(typeof result.nickname === "undefined" )
      //   this.data.nickname = result.user_login;
      //   this.data.firstname = result.first_name;
      //   if(typeof result.first_name === "undefined" )
      //   this.data.firstname = result.xprofile.groups[1].fields[1].value.raw;
      //   this.data.lastname = result.last_name;
      //   if(typeof result.last_name === "undefined" )
      //   this.data.lastname =  result.xprofile.groups[1].fields[2].value.raw;
      //   this.data.description = result.description;
      //   this.data.avatar=result.avatar_urls[24];
      //   this.data.cover=result.cover_url;
      //   console.log(result.avatar_urls[24]);
      //   if(typeof result.avatar_urls[24] === "undefined" )
      //   this.data.avatar=result.avatar_urls['full'];

      //   this.data.email = result.email;
      //   if(typeof result.email === "undefined" )
      //   this.data.email = user.user_email;
          
      //   }
      //   }).catch(e => {
      //   console.log('error: '+ e);
      //   // Handle errors here
      //   });
       
          
    });


  }
  // getDate(e) {
  //   let date = new Date(e.target.value).toISOString().substring(0, 10);
  //   this.userForm.get('date_of_birth').setValue(date, {
  //     onlyself: true
  //   })
  // }

  get errorControl() {
    return this.userForm.controls;
  }

  async loader(){
    const loading = await this.loadingController.create({
      
      message: 'Please wait...',
      duration: 1500
    });
    await loading.present();
}
  userupdate() {
    this.loading2=true;
    this.isSubmitted = true;
    if (!this.userForm.valid) {
      console.log('Please provide all the required values!')
      this.loading2=false;
      return false;
    } else {
      console.log(this.userForm.value)
    }
    
    let formdat:any=[];
    formdat.push({
      meta_key: 'present_address', 
       meta_value:  this.userForm.value.present_address
      },{
        meta_key: 'present_city', 
         meta_value:  this.userForm.value.present_city
        },{
          meta_key: 'present_pin', 
           meta_value:  this.userForm.value.present_pin
          },{
            meta_key: 'present_telephone', 
             meta_value:  this.userForm.value.present_telephone
            },{
              meta_key: 'present_mobile', 
               meta_value:  this.userForm.value.present_mobile
              },{
                meta_key: 'present_email', 
                 meta_value:  this.userForm.value.present_email
                },{
                  meta_key: 'present_state', 
                   meta_value:  this.userForm.value.present_state
                  },{
                    meta_key: 'present_country', 
                     meta_value:  this.userForm.value.present_country
                    }
        );
        let formdata:any=[];
        formdata.push( {meta_data: formdat});
   
    this.api.userjobupdate(
      formdata, this.token).subscribe(
   
      async res => {
        this.loading2=false;
        console.log(res);
        const toast = await this.toastCtrl.create({
          message: 'Profile Updated',
          duration: 3000
        });
        toast.present();
     
        
      console.log('Profile Updated', res);
        // this.router.navigate(['/member-detail']);
        
        // window.location.reload();
      
      },
      err => {
        this.loading2=false;
        console.log(err);
        this.showError(err);
      }
    );
  }
  BackButton(){
    this.location.back();
  } 
  async showError(err) {
    const alert = await this.alertCtrl.create({
      header: err.error.code,
      subHeader: err.error.data,
      message: err.error.message,
      buttons: ['OK']
    });
    await alert.present();
  }
  ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options1);
   
   }
   
   
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options1);
     this.navCtrl.navigateForward(page);
   
   }
  ngOnInit() {

    

    this.userForm = this.fb.group({
      present_address: ['', Validators.required],
      present_city: ['', Validators.required],
      present_pin: ['',[Validators.required, Validators.pattern('^[0-9]+$')]],
      present_telephone: ['',[Validators.required, Validators.pattern('^[0-9]+$')]],
      present_mobile: ['',[Validators.required, Validators.pattern('^[0-9]+$')]],
      present_email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      present_state: ['', Validators.required],
      present_country: ['', Validators.required]
     
    });
  }
  loadPrivatePosts() {
    this.api.getPrivatePosts().subscribe(res => {
      this.posts = res;
    });
  }


  async selectImageSource() {
    const buttons = [
      {
        text: 'Take Photo',
        icon: 'camera',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          // this.addImage();
        }
      },
      {
        text: 'Choose From Photos Photo',
        icon: 'image',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          // this.selectImage();
        }
      }
    ];
 
    // Only allow file selection inside a browser
    if (!this.plt.is('hybrid')) {
      buttons.push({
        text: 'Choose a File',
        icon: 'attach',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          // this.fileInput.nativeElement.click();
        }
      });
    }
 
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons
    });
    await actionSheet.present();
  }

  goToprofile() {
    this.router.navigate(['/member-detail']);
  }
  goToHome(res) {
    swal.fire({
      title: 'Success',
      text: 'Thank you for registrations',
      icon: 'success',
      backdrop: false,
    });
    this.router.navigate(['/tabs/home-new']);
  }

  


}

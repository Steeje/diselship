import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobPresentaddPageRoutingModule } from './job-presentadd-routing.module';

import { JobPresentaddPage } from './job-presentadd.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule ,
    IonicModule,
    JobPresentaddPageRoutingModule
  ],
  declarations: [JobPresentaddPage]
})
export class JobPresentaddPageModule {}

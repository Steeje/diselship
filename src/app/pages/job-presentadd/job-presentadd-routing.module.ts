import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobPresentaddPage } from './job-presentadd.page';

const routes: Routes = [
  {
    path: '',
    component: JobPresentaddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobPresentaddPageRoutingModule {}

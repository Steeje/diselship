/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { dummyDataService } from 'src/app/services/dummy-data.service';
import { ActionSheetController } from '@ionic/angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController } from '@ionic/angular';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.page.html',
  styleUrls: ['./single-post.page.scss'],
})
export class SinglePostPage implements OnInit {
  options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  users;
  constructor(    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, private dummy: dummyDataService, private actionSheet: ActionSheetController) {
    this.users = this.dummy.users;
  }

  ngOnInit() {
  }
  ionViewWillLeave() {
    this.nativePageTransitions.slide(this.options);
   
   }
   
   // example of adding a transition when pushing a new page
   openPage(page: any) {
     this.nativePageTransitions.slide(this.options);
     this.navCtrl.navigateForward(page);
   
   }
   
  async openActionSheet() {
    const actionSheet = await this.actionSheet.create({
      mode: 'md',
      buttons: [
        {
          text: 'Save',
          role: 'destructive',
          icon: 'bookmark-outline',
          handler: () => {
            console.log('Delete clicked');
          }
        },
        {
          text: 'Send in a private Message',
          icon: 'chatbox-ellipses-outline',
          handler: () => {
            console.log('Share clicked');
          }
        },
        {
          text: 'Share via',
          icon: 'share-social',
          handler: () => {
            console.log('Play clicked');
          }
        },
        {
          text: 'Hide this post',
          icon: 'close-circle-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Unfollow',
          icon: 'person-remove-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Report this post',
          icon: 'flag-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Improve my feed',
          icon: 'options-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Who can see this post?',
          icon: 'eye-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

}

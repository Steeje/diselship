import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForumReplyPageRoutingModule } from './forum-reply-routing.module';

import { ForumReplyPage } from './forum-reply.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ForumReplyPageRoutingModule
  ],
  declarations: [ForumReplyPage]
})
export class ForumReplyPageModule {}

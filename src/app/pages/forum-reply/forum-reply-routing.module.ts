import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForumReplyPage } from './forum-reply.page';

const routes: Routes = [
  {
    path: '',
    component: ForumReplyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ForumReplyPageRoutingModule {}

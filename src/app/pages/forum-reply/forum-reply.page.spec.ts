import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ForumReplyPage } from './forum-reply.page';

describe('ForumReplyPage', () => {
  let component: ForumReplyPage;
  let fixture: ComponentFixture<ForumReplyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForumReplyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ForumReplyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

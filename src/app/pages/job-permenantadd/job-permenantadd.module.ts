import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobPermenantaddPageRoutingModule } from './job-permenantadd-routing.module';

import { JobPermenantaddPage } from './job-permenantadd.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule ,
    IonicModule,
    JobPermenantaddPageRoutingModule
  ],
  declarations: [JobPermenantaddPage]
})
export class JobPermenantaddPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobPermenantaddPage } from './job-permenantadd.page';

const routes: Routes = [
  {
    path: '',
    component: JobPermenantaddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobPermenantaddPageRoutingModule {}

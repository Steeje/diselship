import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JobPermenantaddPage } from './job-permenantadd.page';

describe('RegisterPage', () => {
  let component: JobPermenantaddPage;
  let fixture: ComponentFixture<JobPermenantaddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobPermenantaddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JobPermenantaddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

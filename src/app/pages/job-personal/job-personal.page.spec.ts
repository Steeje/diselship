import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JobPersonalPage } from './job-personal.page';

describe('RegisterPage', () => {
  let component: JobPersonalPage;
  let fixture: ComponentFixture<JobPersonalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobPersonalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JobPersonalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

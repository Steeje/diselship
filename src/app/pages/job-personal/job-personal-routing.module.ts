import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobPersonalPage } from './job-personal.page';

const routes: Routes = [
  {
    path: '',
    component: JobPersonalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobPersonalPageRoutingModule {}

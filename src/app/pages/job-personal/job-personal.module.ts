import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobPersonalPageRoutingModule } from './job-personal-routing.module';

import { JobPersonalPage } from './job-personal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule ,
    IonicModule,
    JobPersonalPageRoutingModule
  ],
  declarations: [JobPersonalPage]
})
export class JobPersonalPageModule {}

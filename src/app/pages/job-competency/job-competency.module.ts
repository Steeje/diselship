import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobCompetencyPageRoutingModule } from './job-competency-routing.module';

import { JobCompetencyPage } from './job-competency.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule ,
    IonicModule,
    JobCompetencyPageRoutingModule
  ],
  declarations: [JobCompetencyPage]
})
export class JobCompetencyPageModule {}

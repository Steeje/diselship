import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobCompetencyPage } from './job-competency.page';

const routes: Routes = [
  {
    path: '',
    component: JobCompetencyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobCompetencyPageRoutingModule {}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JobCompetencyPage } from './job-competency.page';

describe('RegisterPage', () => {
  let component: JobCompetencyPage;
  let fixture: ComponentFixture<JobCompetencyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobCompetencyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JobCompetencyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

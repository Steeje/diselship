/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { dummyDataService } from 'src/app/services/dummy-data.service';

@Component({
  selector: 'app-invitations',
  templateUrl: './invitations.page.html',
  styleUrls: ['./invitations.page.scss'],
})
export class InvitationsPage implements OnInit {

  users;
  chipId;
  chipId2;
  segId = 'ion-sb-0';

  constructor(private dummy: dummyDataService) {
    this.users = this.dummy.users;
  }

  ngOnInit() {
  }

  segmentChanged(eve) {
    console.log(eve.detail.value);
    this.segId = eve.detail.value;
  }

  chipChange(val) {
    this.chipId = val;
  }

  chipChange1(val) {
    this.chipId2 = val;
  }

}

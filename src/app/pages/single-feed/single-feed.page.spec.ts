import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SingleFeedPage } from './single-feed.page';

describe('SingleFeedPage', () => {
  let component: SingleFeedPage;
  let fixture: ComponentFixture<SingleFeedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleFeedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SingleFeedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, ViewChild, ViewContainerRef, ComponentFactoryResolver, OnInit, ElementRef } from '@angular/core';
import {  ActivatedRoute, Router } from '@angular/router';
import { dummyDataService } from 'src/app/services/dummy-data.service';
import { ActionSheetController } from '@ionic/angular';
import { apiService } from '../../services/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, ToastController, Platform, LoadingController  } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Location } from "@angular/common";
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController } from '@ionic/angular';
import { ReplyComponent } from '../../components/replycomment/replycomment';
import { ProcessComponent } from './../../components/process';
import * as $ from 'jquery';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import {File, FileEntry} from '@ionic-native/file/ngx';

@Component({
  selector: 'app-single-feed',
  templateUrl: './single-feed.page.html',
  styleUrls: ['./single-feed.page.scss'],
})
export class SingleFeedPage implements OnInit {
  post:any;
  users;
  comment:string;
  commentid:string='';
  commenttext:string;
  commentavatar:string;
  data:any;
  userdata:any;
  mydata:any;
  loading: boolean;
  blob : Blob;
   file_name:string;
    file_type:string;
  follow: boolean;
  unfollow: boolean;
  commentForm: FormGroup;
  @ViewChild('commentfoc', {static: false}) searchInput: { setFocus: () => void; } ;
  options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
   @ViewChild('processContainer', {read: ViewContainerRef, static: true}) container;
   file_ids=[];
   file_imgs: any=[];
  
   content:string;
   privacy:string="public";
  
    token:string;
    user_id:string;
    @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
   constructor(  private file: File,private camera: Camera,   
     private imagePicker: ImagePicker,    private plt: Platform, 
   
    private androidPermissions: AndroidPermissions,public loadingController: LoadingController,
   private resolver: ComponentFactoryResolver,
    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, private dummy: dummyDataService,      private storage: Storage,  private fb: FormBuilder,
    private api: apiService,
    private alertCtrl: AlertController, private location: Location, private toastCtrl: ToastController, private router: Router, private route: ActivatedRoute, private actionSheet: ActionSheetController) {
    this.users = this.dummy.users;
    this.storage.get('USER_INFO').then(result => {
      console.log("USER_INFO",result);
      this.mydata=result.avatar_urls[24];
      console.log(result.avatar_urls[24]);
      if(typeof result.avatar_urls[24] === "undefined" )
      this.mydata=result.avatar_urls['full'];
      this.user_id=result.id;
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
  }
  ionViewWillLeave() {

    document.getElementById('autocomplete')
   
    this.nativePageTransitions.slide(this.options);
   
   }
   
   
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options);
     this.navCtrl.navigateForward(page);
   
   }

loaddata(data1){
 
      const factory = this.resolver.resolveComponentFactory(ReplyComponent);
      let componentRef = this.container.createComponent(factory);
      (componentRef.instance as ProcessComponent).data = data1;
    
    
}

  ngOnInit() {
    
    this.commentForm = this.fb.group({
      comment: ['', Validators.required]
    
    });
    this.loading=false;
    this.route.queryParams.subscribe(params => {
     
      if (params && params.special) {
        this.post = JSON.parse(params.special);
      }
      console.log("data pass", this.post);
      this.api.activitygetcomment(this.post.id).subscribe(res => {
       
        this.data = res;
        console.log("data",res);
      });
      console.log("data",this.data);
     
     
    });
    this.api.getuser(this.post.user_id).subscribe(res => {
       
      this.userdata = res;
      
if(this.userdata.is_following)
this.unfollow=true;
else
this.follow=true;


      console.log("data",this.userdata.is_following);
    });

  }

  BackButton(){
    this.location.back();
  }
  async selectImage() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
      result => console.log('Has permission?',result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
    );
    
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    // this.loading=true;
 
    let options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      //maximumImagesCount: 3,

      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      width: 200,
      //height: 200,

      // quality of resized image, defaults to 100
      quality: 25,

      // output type, defaults to FILE_URIs.
      // available options are 
      // window.imagePicker.OutputType.FILE_URI (0) or 
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    let imageResponse = [];
    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
     
        this.file.resolveLocalFilesystemUrl(results[i]).then((entry: FileEntry) => 
      {
        entry.file(file => {
          console.log(file);
          this.readFile(file);
        });
      });
  } }, (err) => {
    alert(err);
  });
  
  }
  async addImage() {
   
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    
    this.camera.getPicture(options).then((imageData) => {


      this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => 
      {
        entry.file(file => {
          console.log(file);
          this.readFile(file);
        });
      });

  }, (err) => {
    // Handle error
   });
  
  }
  async selectcoverImageSource() {
    const buttons = [
      {
        text: 'Take Photo',
        icon: 'camera',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          this.addImage();
        }
      },
      {
        text: 'Choose From Photos Photo',
        icon: 'image',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          this.selectImage();
        }
      }
    ];
 
    // Only allow file selection inside a browser
    if (!this.plt.is('hybrid')) {
      buttons.push({
        text: 'Choose a File',
        icon: 'attach',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          this.fileInput.nativeElement.click();
        }
      });
    }
 
    const actionSheet = await this.actionSheet.create({
      header: 'Select Image Source',
      buttons
    });
    await actionSheet.present();
  }
  dltmedia(id){
    var index  =  this.file_imgs.findIndex(  data => data.id ===  id);
     this.file_imgs.splice (index, 1);
    var index2  =  this.file_ids.findIndex(  data => data ===  id);
    this.file_ids.splice (index2, 1);
   
    // console.log(this.file_imgs);
    // console.log(index);
//     this.api.deltmedia(id,this.token).subscribe(
//       async (res : any) => {
     
//       console.log("---------------");
//       console.log(res);
//       const toast = await this.toastCtrl.create({
//         message: 'Image Sucessfully Removed',
//         duration: 3000
//       });
//       toast.present();
// // Get the entire data


     
//     },
//     err => {
//       console.log(err);
//       this.showError(err);
//     }
    
//     );
  }
attach(){
  this.fileInput.nativeElement.click();
}
 async uploadFile(event: EventTarget) {
  console.log(this.file_ids);
 
      const loading = await this.loadingController.create({
      
        message: 'Please wait...',
        duration: 2000
      });
      await loading.present();
    const eventObj: MSInputMethodContext = event as MSInputMethodContext;
    const target: HTMLInputElement = eventObj.target as HTMLInputElement;
    const file1 = target.files[0];
    this.api.uploadMedia(file1).subscribe(
      async (res : any) => {
       this.file_ids.push(res.upload_id);
       this.file_imgs.push({
        img: res.upload_thumb, 
        id:  res.upload_id
    });
    
    
      console.log("---------------");
      console.log(this.file_ids);
      const toast = await this.toastCtrl.create({
        message: 'Image Uploaded',
        duration: 3000
      });
      toast.present();
// Get the entire data


     
    },
    err => {
      console.log(err);
      this.showError(err);
    }
    
    );
  }

  readFile(file) {
    console.log(file.type);
    const reader = new FileReader();
    reader.onload = () => {
      this.blob = new Blob([reader.result], {
        type: file.type
      });
this.file_name=file.name;
this.file_type=file.type;
    };
    reader.readAsArrayBuffer(file);
  };


  ionViewWillEnter() {
    // this.selectcoverImageSource();
  }
  unfollowuser(){
    console.log($("#id75").text());
    this.api.followuser("unfollow",this.userdata.id).subscribe(  async (res:any)  => {
      console.log(res);
      this.follow=true;
      this.unfollow=false;
      const toast = await this.toastCtrl.create({
        message: "Unfollow User Successfully",
        duration: 3000
      });
      await toast.present();
     });
  }
  hightlight(id) {  console.log(id); $('#id'+id).delay(100).fadeOut().fadeIn('slow') }
  followuser(){
    this.api.followuser("follow",this.userdata.id).subscribe( async (res:any)  => {
      console.log(res);
      this.unfollow=true;
      this.follow=false;
      const toast = await this.toastCtrl.create({
        message: "Follow User Successfully",
        duration: 3000
      });
      await toast.present();
     });
  }
  likefeed(idd) {
  
    this.api.activitylike(idd).subscribe((res:any) => {
     console.log(res);
   
     this.post.favorited=res.favorited;
     this.post.favorite_count=res.favorite_count;
    });
  }
  deletepost(idd) {
  
    this.api.activitydelete(idd).subscribe((res:any) => {
     console.log(res);
     res.media_url= this.post.media_url;
     this.post=res;
     this.router.navigate(['group-feed']);
    });
  }
  commentfocus(){
    this.searchInput.setFocus();
  }
  commentreply(id,comment,commentavatar){
    this.commenttext=comment;
    this.commentid=id;
    this.commentavatar=commentavatar;
    console.log(id,comment);
    this.searchInput.setFocus();
  }
  postcomment() {
    
    this.loading=true;
    if(this.commentid!=''){
      this.api.postcomment(this.comment,this.post.id,this.commentid,this.file_ids).subscribe(
        async (res:any) => {
          console.log(res);
          this.data = res;
          const toast = await this.toastCtrl.create({
            message: "Comment Posted Successfully",
            duration: 3000
          });
          await toast.present();
          this.comment="";
          this.loading=false;
         
          console.log("data",this.data);
        //  window.location.reload(); 
        //  location.href = "http://localhost:8100/";
        
        // this.router.navigate(['/']);
        },
        err => {
          this.loading=false;
          console.log(err);
          this.showError(err);
        }
      );
    }else{
    this.api.postcomment(this.comment,this.post.id,0,this.file_ids).subscribe(
      async (res:any) => {
        console.log(res);
        this.data = res;
        const toast = await this.toastCtrl.create({
          message: "Comment Posted Successfully",
          duration: 3000
        });
        await toast.present();
        this.comment="";
        this.loading=false;
      //  window.location.reload(); 
      //  location.href = "http://localhost:8100/";
   
      console.log("data",this.data);
      // this.router.navigate(['/']);
      },
      err => {
        this.loading=false;
        console.log(err);
        this.showError(err);
      }
    );}}
    async showError(err) {
      const alert = await this.alertCtrl.create({
        header: "Post Comment Error",
    
        message: err.error.message,
        buttons: ['OK']
      });
      await alert.present();
    }
  async openActionSheet() {
    const actionSheet = await this.actionSheet.create({
      mode: 'md',
      buttons: [
        {
          text: 'Save',
          role: 'destructive',
          icon: 'bookmark-outline',
          handler: () => {
            console.log('Delete clicked');
          }
        },
        {
          text: 'Send in a private Message',
          icon: 'chatbox-ellipses-outline',
          handler: () => {
            console.log('Share clicked');
          }
        },
        {
          text: 'Share via',
          icon: 'share-social',
          handler: () => {
            console.log('Play clicked');
          }
        },
        {
          text: 'Hide this post',
          icon: 'close-circle-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Unfollow',
          icon: 'person-remove-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Report this post',
          icon: 'flag-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Improve my feed',
          icon: 'options-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Who can see this post?',
          icon: 'eye-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

}

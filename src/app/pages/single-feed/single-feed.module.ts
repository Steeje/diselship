import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SingleFeedPageRoutingModule } from './single-feed-routing.module';

import { SingleFeedPage } from './single-feed.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SingleFeedPageRoutingModule
  ],
  declarations: [SingleFeedPage]
})
export class SingleFeedPageModule {}

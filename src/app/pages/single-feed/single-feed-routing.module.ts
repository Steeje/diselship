import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SingleFeedPage } from './single-feed.page';

const routes: Routes = [
  {
    path: '',
    component: SingleFeedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SingleFeedPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SharePostPageRoutingModule } from './share-post-routing.module';

import { SharePostPage } from './share-post.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharePostPageRoutingModule
  ],
  declarations: [SharePostPage]
})
export class SharePostPageModule {}

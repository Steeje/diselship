import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SharePostPage } from './share-post.page';

describe('SharePostPage', () => {
  let component: SharePostPage;
  let fixture: ComponentFixture<SharePostPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharePostPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SharePostPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import swal from 'sweetalert2';
@Component({
  selector: 'app-share-post',
  templateUrl: './share-post.page.html',
  styleUrls: ['./share-post.page.scss'],
})
export class SharePostPage implements OnInit {

  constructor(private actionSheet: ActionSheetController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.openActionSheet();
  }

  async openActionSheet() {
    const actionSheet = await this.actionSheet.create({
      mode: 'md',
      buttons: [
        {
          text: 'Add Photo',
          icon: 'image-outline',
          handler: () => {
            console.log('Delete clicked');
          }
        },
        {
          text: 'Take a video',
          icon: 'videocam-outline',
          handler: () => {
            console.log('Share clicked');
          }
        },
        {
          text: 'Celebrate a teammate',
          icon: 'medal-outline',
          handler: () => {
            console.log('Play clicked');
          }
        },
        {
          text: 'Add a document',
          icon: 'document-text-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Share that you are hiring',
          icon: 'briefcase-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Find an expert',
          icon: 'person-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }
  post() {
    swal.fire({
      title: 'Success',
      text: 'post created successfully',
      icon: 'success',
      backdrop: false,
    });
  }

}

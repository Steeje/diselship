import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SharePostPage } from './share-post.page';

const routes: Routes = [
  {
    path: '',
    component: SharePostPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SharePostPageRoutingModule {}

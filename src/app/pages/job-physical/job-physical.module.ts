import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobPhysicalPageRoutingModule } from './job-physical-routing.module';

import { JobPhysicalPage } from './job-physical.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule ,
    IonicModule,
    JobPhysicalPageRoutingModule
  ],
  declarations: [JobPhysicalPage]
})
export class JobPhysicalPageModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JobPhysicalPage } from './job-physical.page';

describe('RegisterPage', () => {
  let component: JobPhysicalPage;
  let fixture: ComponentFixture<JobPhysicalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobPhysicalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JobPhysicalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

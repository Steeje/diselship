import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobPhysicalPage } from './job-physical.page';

const routes: Routes = [
  {
    path: '',
    component: JobPhysicalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobPhysicalPageRoutingModule {}
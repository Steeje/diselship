import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobEducationPage } from './job-education.page';

const routes: Routes = [
  {
    path: '',
    component: JobEducationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobEducationPageRoutingModule {}
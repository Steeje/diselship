import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobEducationPageRoutingModule } from './job-education-routing.module';

import { JobEducationPage } from './job-education.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule ,
    IonicModule,
    JobEducationPageRoutingModule
  ],
  declarations: [JobEducationPage]
})
export class JobEducationPageModule {}

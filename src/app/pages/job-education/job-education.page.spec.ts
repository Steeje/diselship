import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JobEducationPage } from './job-education.page';

describe('RegisterPage', () => {
  let component: JobEducationPage;
  let fixture: ComponentFixture<JobEducationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobEducationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JobEducationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

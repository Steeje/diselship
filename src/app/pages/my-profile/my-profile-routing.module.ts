import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PeopleProfilePage } from './my-profile.page';

const routes: Routes = [
  {
    path: '',
    component: PeopleProfilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PeopleProfilePageRoutingModule {}

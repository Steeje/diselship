import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PeopleProfilePage } from './my-profile.page';

describe('PeopleProfilePage', () => {
  let component: PeopleProfilePage;
  let fixture: ComponentFixture<PeopleProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeopleProfilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PeopleProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { dummyDataService } from 'src/app/services/dummy-data.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController } from '@ionic/angular';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class PeopleProfilePage implements OnInit {
  data: any = {};
  avatar;
  users;
  company;
  options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  constructor(
    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, private dummy: dummyDataService,   private storage: Storage, private router: Router,) {


    this.storage.get('USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.data.id = result.id;
      this.data.nickname = result.user_nicename;
      
      this.data.name=result.first_name+" "+ result.last_name;
      this.data.description = result.description;
      this.data.avatar=result.avatar_urls[24];
      this.data.email = result.email;
      console.log(result.first_name);
    
      if(typeof result.first_name === "undefined" ){
    
        this.storage.get('COMPLETE_USER_INFO').then(result => {
          if (result != null) {
          console.log( result);
          this.data.email = result.user_email;
          this.data.nickname = result.user_nicename;
          if(typeof result.first_name !== "undefined" ){
          this.data.name=result.first_name+" "+ result.last_name;
          this.data.description=result.description;
          
          }else{
            this.data.name=" ";
            this.data.description="";
          }}
          }).catch(e => {
          console.log('error: '+ e);
          // Handle errors here
          });
        }
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });


     
   }
   ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options);
   
   }
   
   
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options);
     this.navCtrl.navigateForward(page);
   
   }
  ngOnInit() {
    this.users = this.dummy.users;
    this.company = this.dummy.company;
  }
  editProfile() {
    this.router.navigate(['/edit-profile']);
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobModularcoursedetailsPage } from './job-modularcoursedetails.page';

const routes: Routes = [
  {
    path: '',
    component: JobModularcoursedetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobModularcoursedetailsPageRoutingModule {}
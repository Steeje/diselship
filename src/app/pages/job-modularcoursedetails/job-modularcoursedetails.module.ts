import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobModularcoursedetailsPageRoutingModule } from './job-modularcoursedetails-routing.module';

import { JobModularcoursedetailsPage } from './job-modularcoursedetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule ,
    IonicModule,
    JobModularcoursedetailsPageRoutingModule
  ],
  declarations: [JobModularcoursedetailsPage]
})
export class JobModularcoursedetailsPageModule {}

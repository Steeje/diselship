/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-my-network',
  templateUrl: './my-network.page.html',
  styleUrls: ['./my-network.page.scss'],
})
export class MyNetworkPage implements OnInit {

  users = [
    {
      img: 'assets/imgs/user.jpg',
      cover: 'assets/imgs/back1.jpg'
    },
    {
      img: 'assets/imgs/user2.jpg',
      cover: 'assets/imgs/back2.jpg'
    },
    {
      img: 'assets/imgs/user3.jpg',
      cover: 'assets/imgs/back3.jpg'
    },
    {
      img: 'assets/imgs/user4.jpg',
      cover: 'assets/imgs/back4.jpg'
    }
  ];
  constructor(private router: Router, private menuCtrl: MenuController) { }

  ngOnInit() {
  }

  goToPeopleProfile() {
    this.router.navigate(['/people-profile']);
  }

  openMenu() {
    this.menuCtrl.open();
  }

  goToManageNetwork() {
    this.router.navigate(['/manage-network']);
  }

  gotoInvitations() {
    this.router.navigate(['/invitations']);
  }

  onSearchChange(event) {

  }
  goToChatList() {
    this.router.navigate(['/chatlist']);
  }
}

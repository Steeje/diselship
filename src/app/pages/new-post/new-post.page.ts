/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActionSheetController, AlertController, ToastController, Platform,LoadingController  } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import swal from 'sweetalert2';
import {  ActivatedRoute, Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Location } from "@angular/common";
import { apiService } from '../../services/api.service';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import {File, FileEntry} from '@ionic-native/file/ngx';
import { async } from '@angular/core/testing';
@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.page.html',
  styleUrls: ['./new-post.page.scss'],
})

export class NewPostPage implements OnInit {
  file_ids=[];
  file_imgs: any=[];
  mydata :any =[];
  blob : Blob;
  file_name:string;
  content:string;
  privacy:string="public";
   file_type:string;
   token:string;
   user_id:string;
   @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
  constructor(  private file: File,private camera: Camera,   private router: Router,
    private imagePicker: ImagePicker,    private plt: Platform, private storage: Storage, 
    private androidPermissions: AndroidPermissions, private actionSheet: ActionSheetController,    private alertCtrl: AlertController,
    private toastCtrl: ToastController,  private api: apiService,public loadingController: LoadingController) { this.storage.get('COMPLETE_USER_INFO').then(result => {
      if (result != null) {
      console.log( result);
      this.token=result.token;
     
      this.user_id=result.user_id;
     
      console.log('user_id: '+  result.user_id);
      console.log('token: '+  result.token);
      }
      }).catch(e => {
      console.log('error: '+ e);
      // Handle errors here
      });
    
      this.storage.get('USER_INFO').then(result => {
        console.log("USER_INFO",result);
        this.mydata.avatar=result.avatar_urls[24];
        console.log(result.avatar_urls[24]);
        if(typeof result.avatar_urls[24] === "undefined" )
        this.mydata.avatar=result.avatar_urls['full'];
        this.mydata.name=result.profile_name;
       
        }).catch(e => {
        console.log('error: '+ e);
        // Handle errors here
        });
    
    
    }

  async selectImage() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
      result => console.log('Has permission?',result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
    );
    
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    // this.loading=true;
 
    let options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      maximumImagesCount: 1,

      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      width: 200,
      //height: 200,

      // quality of resized image, defaults to 100
      quality: 25,

      // output type, defaults to FILE_URIs.
      // available options are 
      // window.imagePicker.OutputType.FILE_URI (0) or 
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    let imageResponse = [];
    this.imagePicker.getPictures(options).then(async (results) => {
      // for (var i = 0; i < results.length; i++) {
     
      //   this.file.resolveLocalFilesystemUrl(results[i]).then((entry: FileEntry) => 
      // {
      //   entry.file(file => {
      //     console.log(file);
      //    this.readFile(file);

      // this.file_imgs.push({
      //   img: 'data:image/jpeg;base64,' + results[0]});
      // const base64 = await fetch(results[0]);
      // const blob = await base64.blob();
    //   let blob = this.getBlob(results, ".jpg")
    
    //     this.api.uploadMedia1(blob,"test","jpg").subscribe(
    //       async (res : any) => {
    //        this.file_ids.push(res.upload_id);
    //        this.file_imgs.push({
    //         img: res.upload_thumb, 
    //         id:  res.upload_id
    //     });
        
        
    //       console.log("---------------");
    //       console.log(this.file_ids);
    //       const toast = await this.toastCtrl.create({
    //         message: 'Image Uploaded',
    //         duration: 3000
    //       });
    //       toast.present();
    // // Get the entire data
    
    
         
    //     },
    //     err => {
    //       console.log(err);
    //       this.showError(err);
    //     }
        
    //     );




        var imageData = results[0].toString();
      //  var imageData1 = imageData.toString();
        var byteCharacters = atob(imageData.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
        var byteNumbers = new Array(byteCharacters.length);
        for (var i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        
        var byteArray = new Uint8Array(byteNumbers);
        var blob1 = new Blob([ byteArray ], {
           type : "jpg"
        });
        console.log(blob1);
      this.api.uploadMedia1(blob1,"jpg","test", this.token).subscribe(
            async (res : any) => {
             this.file_ids.push(res.upload_id);
             this.file_imgs.push({
              img: res.upload_thumb, 
              id:  res.upload_id
          });
          
          
            console.log("---------------");
            console.log(this.file_ids);
            const toast = await this.toastCtrl.create({
              message: 'Image Uploaded',
              duration: 3000
            });
            toast.present();
      // Get the entire data
      
      
           
          },
          err => {
            console.log(err);
            this.showError(err);
          }
          
          );
     //   });
    //  });
//  } 
}, (err) => {
    alert(err);
  });
  
  }
  async addImage() {
   
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    
    this.camera.getPicture(options).then((imageData) => {
// console.log(imageData);
//       let mimeType = imageData.profilepic.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
//       mimeType = mimeType.replace("image/", "");
//       console.log(mimeType);
      // let blob = this.getBlob(imageData, ".jpg")
      var imageData1 = imageData.toString();
        var byteCharacters = atob(imageData1.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
        var byteNumbers = new Array(byteCharacters.length);
        for (var i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        
        var byteArray = new Uint8Array(byteNumbers);
        var blob1 = new Blob([ byteArray ], {
           type : "jpg"
        });
        console.log(blob1);
      this.api.uploadMedia1(blob1,"jpg","test",this.token).subscribe(
        async (res : any) => {
         this.file_ids.push(res.upload_id);
         this.file_imgs.push({
          img: res.upload_thumb, 
          id:  res.upload_id
      });

      // this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => 
      // {
      //   entry.file(file => {
      //     console.log(file);
      //     this.readFile(file);
      //     this.api.uploadMedia1(this.blob,this.file_name,this.file_type).subscribe(
      //       async (res : any) => {
      //        this.file_ids.push(res.upload_id);
      //        this.file_imgs.push({
      //         img: res.upload_thumb, 
      //         id:  res.upload_id
      //     });
          
          
            console.log("---------------");
            console.log(this.file_ids);
            const toast = await this.toastCtrl.create({
              message: 'Image Uploaded',
              duration: 3000
            });
            toast.present();
      // Get the entire data
      
      
           
          },
          err => {
            console.log(err);
            this.showError(err);
          }
          
          );
        //  this.readFile(file);
      //   });
      // });

  }, (err) => {
    // Handle error
   });
  
  }


  getBlob(b64Data:string, contentType:string, sliceSize:number= 512) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    let byteCharacters = atob(b64Data);
    let byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        let slice = byteCharacters.slice(offset, offset + sliceSize);

        let byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        let byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }
    let blob = new Blob(byteArrays, {type: contentType});
    return blob;
}
  async selectcoverImageSource() {
    const buttons = [
      // {
      //   text: 'Take Photo',
      //   icon: 'camera',
      //   handler: () => {
      //     this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
      //       result => console.log('Has permission?',result.hasPermission),
      //       err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
      //     );
          
      //     this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
      //     this.addImage();
      //   }
      // },
      {
        text: 'Choose From Photos Photo',
        icon: 'image',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          this.selectImage();
        }
      }
    ];
 
    // Only allow file selection inside a browser
    if (!this.plt.is('hybrid')) {
      buttons.push({
        text: 'Choose a File',
        icon: 'attach',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          this.fileInput.nativeElement.click();
        }
      });
    }
 
    const actionSheet = await this.actionSheet.create({
      header: 'Select Image Source',
      buttons
    });
    await actionSheet.present();
  }
  dltmedia(id){
    var index  =  this.file_imgs.findIndex(  data => data.id ===  id);
     this.file_imgs.splice (index, 1);
    var index2  =  this.file_ids.findIndex(  data => data ===  id);
    this.file_ids.splice (index2, 1);
   
    // console.log(this.file_imgs);
    // console.log(index);
//     this.api.deltmedia(id,this.token).subscribe(
//       async (res : any) => {
     
//       console.log("---------------");
//       console.log(res);
//       const toast = await this.toastCtrl.create({
//         message: 'Image Sucessfully Removed',
//         duration: 3000
//       });
//       toast.present();
// // Get the entire data


     
//     },
//     err => {
//       console.log(err);
//       this.showError(err);
//     }
    
//     );
  }
 async createpost(){
    const loading = await this.loadingController.create({
      
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();
    this.api.createPost(this.user_id,this.file_ids,this.content,this.privacy).subscribe(
      async (res : any) => {
     
      console.log("---------------");
      console.log(res);
      const toast = await this.toastCtrl.create({
        message: 'Post Sucessfully created',
        duration: 3000
      });
      toast.present();
// Get the entire data

this.router.navigate(['/news-feed']);
     
    },
    err => {
      console.log(err);
      this.showError(err);
    }
    
    );
  }
attach(){
  this.fileInput.nativeElement.click();
}
 async uploadFile(event: EventTarget) {
  console.log(this.file_ids);
 
      const loading = await this.loadingController.create({
      
        message: 'Please wait...',
        duration: 2000
      });
      await loading.present();
    const eventObj: MSInputMethodContext = event as MSInputMethodContext;
    const target: HTMLInputElement = eventObj.target as HTMLInputElement;
    const file1 = target.files[0];
    this.api.uploadMedia(file1).subscribe(
      async (res : any) => {
       this.file_ids.push(res.upload_id);
       this.file_imgs.push({
        img: res.upload_thumb, 
        id:  res.upload_id
    });
    
    
      console.log("---------------");
      console.log(this.file_ids);
      const toast = await this.toastCtrl.create({
        message: 'Image Uploaded',
        duration: 3000
      });
      toast.present();
// Get the entire data


     
    },
    err => {
      console.log(err);
      this.showError(err);
    }
    
    );
  }
  async showError(err) {
    const alert = await this.alertCtrl.create({
      header: err.error.code,
      subHeader: err.error.data,
      message: err.error.message,
      buttons: ['OK']
    });
    await alert.present();
  }
  readFile(file) {
    console.log(file.type);
    const reader = new FileReader();
    reader.onload = () => {
      this.blob = new Blob([reader.result], {
        type: file.type
      });
this.file_name=file.name;
this.file_type=file.type;
    };
    reader.readAsArrayBuffer(file);

  
  };
  ngOnInit() {
  }

  ionViewWillEnter() {
    this.selectcoverImageSource();
  }

  async openActionSheet() {
    const actionSheet = await this.actionSheet.create({
      mode: 'md',
      buttons: [
        {
          text: 'Add Photo',
          icon: 'image-outline',
          handler: () => {
            console.log('Delete clicked');
          }
        },
        {
          text: 'Take a video',
          icon: 'videocam-outline',
          handler: () => {
            console.log('Share clicked');
          }
        },
        {
          text: 'Celebrate a teammate',
          icon: 'medal-outline',
          handler: () => {
            console.log('Play clicked');
          }
        },
        {
          text: 'Add a document',
          icon: 'document-text-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Share that you are hiring',
          icon: 'briefcase-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Find an expert',
          icon: 'person-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }
  post() {
    swal.fire({
      title: 'Success',
      text: 'post created successfully',
      icon: 'success',
      backdrop: false,
    });
  }

}

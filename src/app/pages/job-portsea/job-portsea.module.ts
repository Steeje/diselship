import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobPortseaPageRoutingModule } from './job-portsea-routing.module';

import { JobPortseaPage } from './job-portsea.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule ,
    IonicModule,
    JobPortseaPageRoutingModule
  ],
  declarations: [JobPortseaPage]
})
export class JobPortseaPageModule {}

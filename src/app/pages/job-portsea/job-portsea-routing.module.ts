import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobPortseaPage } from './job-portsea.page';

const routes: Routes = [
  {
    path: '',
    component: JobPortseaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobPortseaPageRoutingModule {}
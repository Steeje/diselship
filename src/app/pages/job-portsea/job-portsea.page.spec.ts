import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JobPortseaPage } from './job-portsea.page';

describe('RegisterPage', () => {
  let component: JobPortseaPage;
  let fixture: ComponentFixture<JobPortseaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobPortseaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JobPortseaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

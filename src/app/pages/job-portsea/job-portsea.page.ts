/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { apiService } from '../../services/api.service';
import { Storage } from '@ionic/storage';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import swal from 'sweetalert2';
import { Platform, ActionSheetController } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import {File, FileEntry} from '@ionic-native/file/ngx';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController } from '@ionic/angular';

@Component({
  selector: 'app-job-portsea',
  templateUrl: './job-portsea.page.html',
  styleUrls: ['./job-portsea.page.scss'],
})
export class JobPortseaPage implements OnInit {
  data: any = {};
  loading:boolean; loading2:boolean;
  loading3:boolean;
  imageResponse: any;
  token;
  options: any;
  userForm: FormGroup;
  edit_icon:boolean;
  edit_icon2:boolean;
  images: apiService[] = [];
  user = this.api.getCurrentUser();
  posts = [];
  country = [
    {
      value: 'TELENGANA',
    },
    {
      value: 'India',
    },
    {
      value: 'USA',
    },
    {
      value: 'Pakistan',
    },
    {
      value: 'China',
    },
    ];
    defaultDate = "1987-06-30";
    isSubmitted = false;
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
  @ViewChild('fileInput2', { static: false }) fileInput2: ElementRef;
  options1: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  constructor(
    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, 
    public loadingController: LoadingController,
    private plt: Platform,
    private file: File,
    private router: Router,
    private api: apiService,
    private location: Location,
    private androidPermissions: AndroidPermissions,
    private storage: Storage,
    private fb: FormBuilder,
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController
  ) {
    this.loader();
    
    
    this.api.getJobdetails( this.token,"portsea").subscribe(
      async res => { this.data = res;console.log(res); },
      err => {
        this.loading2=false;
        console.log(err);
        this.showError(err);
      }
    );
    this.edit_icon=true;
    this.edit_icon2=true;
    this.user.subscribe(user => {
      if (user) {
    
          console.log( user);
          this.token = user.token;
       
      } else {
        this.posts = [];
      }
      
          
    });


  }
  

  get errorControl() {
    return this.userForm.controls;
  }

  async loader(){
    const loading = await this.loadingController.create({
      
      message: 'Please wait...',
      duration: 1500
    });
    await loading.present();
}

getDate(e) {
  let date = new Date(e.target.value).toISOString().substring(0, 10);
  this.userForm.get('port_sea_attended_from1').setValue(date, {
    onlyself: true
  })
}

getDate4(e) {
  let date = new Date(e.target.value).toISOString().substring(0, 10);
  this.userForm.get('port_sea_attended_to_date1').setValue(date, {
    onlyself: true
  })
}
ngOnInit() {

  

  this.userForm = this.fb.group({
    post_sea_grade1: ['', Validators.required],
    port_sea_part_phase1: ['', Validators.required],
    port_sea_certificate_no1:  ['',[Validators.required, Validators.pattern('^[0-9]+$')]],
    port_sea_course_name1: ['', Validators.required],
    port_sea_training_institute1:  ['', Validators.required],
    port_sea_attended_from1:   ['', Validators.required],
    port_sea_attended_to_date1:  ['', Validators.required]
  });
}
  userupdate() {
    this.loading2=true;
    this.isSubmitted = true;
    if (!this.userForm.valid) {
      console.log('Please provide all the required values!')
      this.loading2=false;
      return false;
    } else {
      console.log(this.userForm.value)
    }


    let formdat:any=[];
    formdat.push({
      meta_key: 'post_sea_grade1', 
       meta_value:  this.userForm.value.post_sea_grade1
      },{
        meta_key: 'port_sea_part_phase1', 
         meta_value:  this.userForm.value.port_sea_part_phase1
        },{
          meta_key: 'port_sea_course_name1', 
           meta_value:  this.userForm.value.port_sea_course_name1
          },{
            meta_key: 'port_sea_training_institute1', 
             meta_value:  this.userForm.value.port_sea_training_institute1
            },{
              meta_key: 'port_sea_certificate_no1', 
               meta_value:  this.userForm.value.port_sea_certificate_no1
              },{
                meta_key: 'port_sea_attended_from1', 
                 meta_value:  this.userForm.value.port_sea_attended_from1
                },{
                  meta_key: 'port_sea_attended_to_date1', 
                   meta_value:  this.userForm.value.port_sea_attended_to_date1
                  }
        );
        let formdata:any=[];
        formdata.push( {meta_data: formdat});
   
    this.api.userjobupdate(
      formdata, this.token).subscribe(
      async res => {
        this.loading2=false;
        console.log(res);
        const toast = await this.toastCtrl.create({
          message: 'Profile Updated',
          duration: 3000
        });
        toast.present();
     
        
      console.log('Profile Updated', res);
        // this.router.navigate(['/member-detail']);
        
        // window.location.reload();
      
      },
      err => {
        this.loading2=false;
        console.log(err);
        this.showError(err);
      }
    );
  }
  BackButton(){
    this.location.back();
  } 
  
  async showError(err) {
    const alert = await this.alertCtrl.create({
      header: err.error.code,
      subHeader: err.error.data,
      message: err.error.message,
      buttons: ['OK']
    });
    await alert.present();
  }
  ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options1);
   
   }
   
   
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options1);
     this.navCtrl.navigateForward(page);
   
   }
 
  loadPrivatePosts() {
    this.api.getPrivatePosts().subscribe(res => {
      this.posts = res;
    });
  }


  async selectImageSource() {
    const buttons = [
      {
        text: 'Take Photo',
        icon: 'camera',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          // this.addImage();
        }
      },
      {
        text: 'Choose From Photos Photo',
        icon: 'image',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          // this.selectImage();
        }
      }
    ];
 
    // Only allow file selection inside a browser
    if (!this.plt.is('hybrid')) {
      buttons.push({
        text: 'Choose a File',
        icon: 'attach',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          // this.fileInput.nativeElement.click();
        }
      });
    }
 
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons
    });
    await actionSheet.present();
  }

  goToprofile() {
    this.router.navigate(['/member-detail']);
  }
  goToHome(res) {
    swal.fire({
      title: 'Success',
      text: 'Thank you for registrations',
      icon: 'success',
      backdrop: false,
    });
    this.router.navigate(['/tabs/home-new']);
  }

  


}

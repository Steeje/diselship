import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobSeagoingPage } from './job-seagoing.page';

const routes: Routes = [
  {
    path: '',
    component: JobSeagoingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobSeagoingPageRoutingModule {}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JobSeagoingPage } from './job-seagoing.page';

describe('RegisterPage', () => {
  let component: JobSeagoingPage;
  let fixture: ComponentFixture<JobSeagoingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobSeagoingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JobSeagoingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

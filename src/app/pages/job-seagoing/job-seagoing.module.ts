import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobSeagoingPageRoutingModule } from './job-seagoing-routing.module';

import { JobSeagoingPage } from './job-seagoing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule ,
    IonicModule,
    JobSeagoingPageRoutingModule
  ],
  declarations: [JobSeagoingPage]
})
export class JobSeagoingPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobPreSeaTrainingPage } from './job-preseatraining.page';

const routes: Routes = [
  {
    path: '',
    component: JobPreSeaTrainingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobPreSeaTrainingPageRoutingModule {}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JobPreSeaTrainingPage } from './job-preseatraining.page';

describe('RegisterPage', () => {
  let component: JobPreSeaTrainingPage;
  let fixture: ComponentFixture<JobPreSeaTrainingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobPreSeaTrainingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JobPreSeaTrainingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobPreSeaTrainingPageRoutingModule } from './job-preseatraining-routing.module';

import { JobPreSeaTrainingPage } from './job-preseatraining.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule ,
    IonicModule,
    JobPreSeaTrainingPageRoutingModule
  ],
  declarations: [JobPreSeaTrainingPage]
})
export class JobPreSeaTrainingPageModule {}

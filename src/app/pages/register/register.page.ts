/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { apiService } from '../../services/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, ToastController } from '@ionic/angular';
import swal from 'sweetalert2';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  userForm: FormGroup;
  loading: boolean;
  user = this.api.getCurrentUser();
  posts = [];
 
  options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
   showtab:boolean = false;
  constructor(
    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, 
    private router: Router,
    private api: apiService,
    private fb: FormBuilder,
    private storage: Storage,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController
  ) {
    this.user.subscribe(user => {
      if (user) {
        
      } else {
        this.posts = [];
      }
    });
  }
  ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options);
   
   }
   
   
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options);
     this.navCtrl.navigateForward(page);
   
   }
  ngOnInit() {
    this.userForm = this.fb.group({
      signup_password: ['', Validators.required],
      field_1: ['', Validators.required],
      field_2: ['', Validators.required],
      field_3: ['', Validators.required],
      signup_email: ['', Validators.required],
      cnpassword: ['', Validators.required]
    });
  }

  goToLogin() {
    this.router.navigate(['/login']);
  }
  goToHome(res) {
    swal.fire({
      title: 'Success',
      text: 'Thank you for registrations',
      icon: 'success',
      backdrop: false,
    });
    this.router.navigate(['/tabs/home-new']);
  }
  
  async signUp() {
if(this.userForm.value.signup_password!=this.userForm.value.cnpassword){
const toast = await this.toastCtrl.create({
  message: 'Password Not Match !!',
  duration: 3000
});
toast.present();
}
else{

    this.loading=true;
    this.api.signUp( this.userForm.value.field_1, 
      this.userForm.value.field_2, 
      this.userForm.value.field_3,
      this.userForm.value.signup_email,
      this.userForm.value.signup_password,).subscribe(
      async  (res : any) => {
        console.log(res);
        let code =res.code;
       if(code != "bp_rest_register_errors"){
        this.storage.set('COMPLETE_USER_INFO', res);
        this.api.getuser(res.user_id).subscribe(
          (res : any)=> {
        console.log("---------------");
        console.log(res);
      
        this.storage.set('USER_INFO', res.data);});
        this.loading=false;
        swal.fire({
          title:'Signup Successfully',
          text: res.message,
          icon: 'success',
          backdrop: false,
        });
          const toast = await this.toastCtrl.create({
            message: res.message,
            duration: 3000
          });
          toast.present();
          this.loading=false;
          this.router.navigate(['/login']);
        }else{
          this.loading=false;
          this.showError(res);
        }
      },
      err => {
        console.log(err);
        this.loading=false;
        this.showError(err);
      }
    );
}
  }
 
  async showError(err) {
    let message="";
    if(err.message.field_1){
    message +=err.message.field_1;}
    else  if(err.message.field_2){
      message +=err.message.field_2;}
      else  if(err.message.field_3){
        message +=err.message.field_3;}
        else  if(err.message.signup_email){
          message +=err.message.signup_email;}
          else  if(err.message.signup_password){
            message +=err.message.signup_password;}
             
    const alert = await this.alertCtrl.create({
      header: "Registration Failed",
      subHeader: err.code,
      
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }
  // goToHome() {
  //   swal.fire({
  //     title: 'Success',
  //     text: 'Thank you for registrations',
  //     icon: 'success',
  //     backdrop: false,
  //   });
  //   this.router.navigate(['/tabs/home-new']);
  // }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JoinWithGooglePageRoutingModule } from './join-with-google-routing.module';

import { JoinWithGooglePage } from './join-with-google.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JoinWithGooglePageRoutingModule
  ],
  declarations: [JoinWithGooglePage]
})
export class JoinWithGooglePageModule {}

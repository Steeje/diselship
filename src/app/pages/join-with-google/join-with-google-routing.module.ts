import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JoinWithGooglePage } from './join-with-google.page';

const routes: Routes = [
  {
    path: '',
    component: JoinWithGooglePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JoinWithGooglePageRoutingModule {}

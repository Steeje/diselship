import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JoinWithGooglePage } from './join-with-google.page';

describe('JoinWithGooglePage', () => {
  let component: JoinWithGooglePage;
  let fixture: ComponentFixture<JoinWithGooglePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinWithGooglePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JoinWithGooglePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

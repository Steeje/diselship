/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { apiService } from '../../services/api.service';
import { Storage } from '@ionic/storage';
import { Location } from "@angular/common";

@Component({
  selector: 'app-member-profile',
  templateUrl: './member-profile.page.html',
  styleUrls: ['./member-profile.page.scss'],
})
export class MemberProfilePage implements OnInit {
  data: any = {};
  user = this.api.getCurrentUser();
  constructor( private router: Router,
    private api: apiService,private location: Location,
    private storage: Storage,) {
      this.user.subscribe(user => {
        if (user) {
      
            console.log( user);
        
         
        
        this.storage.get('USER_INFO').then(result => {
          if (result != null) {
          console.log( result);
          this.data.id = result.id;
          this.data.followers = result.followers;
          this.data.registered_date = this.getdate(result.registered_date);
          this.data.following = result.following;
          this.data.nickname = result.nickname;
          if(typeof result.nickname === "undefined" )
          this.data.nickname = result.user_login;
          this.data.firstname = result.first_name;
          if(typeof result.first_name === "undefined" )
          this.data.firstname = result.xprofile.groups[1].fields[1].value.raw;
          this.data.lastname = result.last_name;
          if(typeof result.last_name === "undefined" )
          this.data.lastname =  result.xprofile.groups[1].fields[2].value.raw;
          this.data.description = result.description;
          this.data.avatar=result.avatar_urls[24];
          console.log(result.avatar_urls[24]);
          if(typeof result.avatar_urls[24] === "undefined" )
          this.data.avatar=result.avatar_urls['full'];
          this.data.cover=result.cover_url;
          this.data.email = result.email;
          if(typeof result.email === "undefined" )
          this.data.email = user.user_email;
          console.log( this.data);
          }
          }).catch(e => {
          console.log('error: '+ e);
          // Handle errors here
          });
         } else {
          this.router.navigate(['/login']);
          }
            
      });
     }
     BackButton(){
      this.location.back();
    } 
     truncate(source, size) {
      return source.length > size ? source.slice(0, size - 1) + "…" : source;
    }
  ngOnInit() {
  }
   getdate(date) {
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
  
    var fulld = new Date(date);
    var n = month[fulld.getMonth()];
     var  d = fulld.getDate();
     var  y = fulld.getFullYear();
   return d+" "+n+" "+y;
  }
  goToeditprofile() {
    this.router.navigate(['/edit-profile']);
  }
}

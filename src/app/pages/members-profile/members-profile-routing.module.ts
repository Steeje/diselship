import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MembersProfilePage } from './members-profile.page';

const routes: Routes = [
  {
    path: '',
    component: MembersProfilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MembersProfilePageRoutingModule {}

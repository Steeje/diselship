import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MembersProfilePageRoutingModule } from './members-profile-routing.module';

import { MembersProfilePage } from './members-profile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MembersProfilePageRoutingModule
  ],
  declarations: [MembersProfilePage]
})
export class MembersProfilePageModule {}

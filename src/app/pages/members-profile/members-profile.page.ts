/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { apiService } from '../../services/api.service';
import { Storage } from '@ionic/storage';
import { Location } from "@angular/common";

@Component({
  selector: 'app-members-profile',
  templateUrl: './members-profile.page.html',
  styleUrls: ['./members-profile.page.scss'],
})
export class MembersProfilePage implements OnInit {
  data: any = {};
  id;
  user = this.api.getCurrentUser();
  constructor( private router: Router,
    private api: apiService,private route: ActivatedRoute,private location: Location,
    private storage: Storage,) {


      this.id  =this.route.snapshot.paramMap.get('id');
      this.api.getuser( this.id).subscribe((result:any) => {
        console.log(result);
      
     
          this.data.id = result.id;
          this.data.followers = result.followers;
          this.data.registered_date = this.getdate(result.registered_date);
          this.data.following = result.following;
    
          this.data.nickname = result.user_login;
     
          this.data.firstname = result.xprofile.groups[1].fields[1].value.raw;
   
          this.data.lastname =  result.xprofile.groups[1].fields[2].value.raw;
          this.data.description = result.description;

          this.data.avatar=result.avatar_urls['full'];
          this.data.cover=result.cover_url;
          this.data.email = result.email;
     
          console.log( this.data);
          
          });
      
            
   
     }
     BackButton(){
      this.location.back();
    } 
     truncate(source, size) {
      return source.length > size ? source.slice(0, size - 1) + "…" : source;
    }
  ngOnInit() {
  }
   getdate(date) {
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "Septembers";
    month[9] = "October";
    month[10] = "Novembers";
    month[11] = "Decembers";
  
    var fulld = new Date(date);
    var n = month[fulld.getMonth()];
     var  d = fulld.getDate();
     var  y = fulld.getFullYear();
   return d+" "+n+" "+y;
  }
  goToeditprofile() {
    this.router.navigate(['/edit-profile']);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MembersProfilePage } from './members-profile.page';

describe('MembersProfilePage', () => {
  let component: MembersProfilePage;
  let fixture: ComponentFixture<MembersProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MembersProfilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MembersProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

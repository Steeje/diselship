import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ManageNetworkPage } from './manage-network.page';

describe('ManageNetworkPage', () => {
  let component: ManageNetworkPage;
  let fixture: ComponentFixture<ManageNetworkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageNetworkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ManageNetworkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManageNetworkPage } from './manage-network.page';

const routes: Routes = [
  {
    path: '',
    component: ManageNetworkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageNetworkPageRoutingModule {}

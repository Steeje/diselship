/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-manage-network',
  templateUrl: './manage-network.page.html',
  styleUrls: ['./manage-network.page.scss'],
})
export class ManageNetworkPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToManageDetail(val) {
    console.log(val);

    const navData: NavigationExtras = {
      queryParams: {
        id: val
      }
    };
    this.router.navigate(['/manage-detail'], navData);
  }

}

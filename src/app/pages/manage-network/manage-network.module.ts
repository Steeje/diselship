import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManageNetworkPageRoutingModule } from './manage-network-routing.module';

import { ManageNetworkPage } from './manage-network.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManageNetworkPageRoutingModule
  ],
  declarations: [ManageNetworkPage]
})
export class ManageNetworkPageModule {}

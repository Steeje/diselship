import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GroupUpdatePage } from './group-update.page';

describe('RegisterPage', () => {
  let component: GroupUpdatePage;
  let fixture: ComponentFixture<GroupUpdatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupUpdatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GroupUpdatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

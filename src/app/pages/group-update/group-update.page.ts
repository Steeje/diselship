/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the 
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { apiService } from '../../services/api.service';
import { Storage } from '@ionic/storage';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import swal from 'sweetalert2';
import { Platform, ActionSheetController } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import {File, FileEntry} from '@ionic-native/file/ngx';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { MenuController,NavController } from '@ionic/angular';
import * as $ from 'jquery';

@Component({
  selector: 'app-group-update',
  templateUrl: './group-update.page.html',
  styleUrls: ['./group-update.page.scss'],
})
export class GroupUpdatePage implements OnInit {
  data: any = {};
  loading:boolean; loading2:boolean;
  loading3:boolean;
  imageResponse: any;
  update = false;
  options: any;
  userForm: FormGroup;
  edit_icon:boolean;
  edit_icon2:boolean;
  images: apiService[] = [];
  user = this.api.getCurrentUser();
  posts = [];
  token;user_id;
  groupdata : any = [];
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
  @ViewChild('fileInput2', { static: false }) fileInput2: ElementRef;
  options1: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  constructor(
    public navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions, 
    private imagePicker: ImagePicker,
    private plt: Platform,
    private file: File,
    public loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute,
    private api: apiService,
    private camera: Camera,
    private androidPermissions: AndroidPermissions,
    private storage: Storage,
    private fb: FormBuilder,
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController
  ) {


    this.route.queryParams.subscribe(data => {
      console.log("data.id",data.id);
   
      if(typeof data.id !== "undefined" )
      this.update=true;
      console.log(data);
      // this.userForm.value.groupname = data.name;
      // this.userForm.value.groupdescription = data.full_description;
      this.groupdata.id= data.id;
      this.groupdata.name= data.name;
      this.groupdata.description= data.description;
      this.groupdata.cover= data.cover;
      this.groupdata.media_url= data.media_url;
     
    });



   
  
    this.edit_icon=true;
    this.edit_icon2=true;
    this.user.subscribe(user => {
      if (user) {
        //
          console.log( user);
    
          this.storage.get('COMPLETE_USER_INFO').then(result => {
            if (result != null) {
            console.log( result);
            this.token=result.token;
           
            this.user_id=result.user_id;
           
            console.log('user_id: '+  result.user_id);
            console.log('token: '+  result.token);
            }
            }).catch(e => {
            console.log('error: '+ e);
            // Handle errors here
            });
      } else {
        this.posts = [];
      }
     
       
          
    });


  }
  userupdate() {
    this.loading2=true;

    this.api.groupCreate(
      this.userForm.value.groupname, 

      this.userForm.value.groupdescription).subscribe(
      async (res :any) => {
        this.loading2=false;
        console.log(res);
        const toast = await this.toastCtrl.create({
          message: 'Profile Updated',
          duration: 3000
        });
        toast.present();
        this.update=true;
        this.groupdata=res;
        this.groupdata.id= res.id;
        this.groupdata.name= res.name;
        this.groupdata.description= res.description.raw;
        this.groupdata.media_url = res.avatar_urls.thumb;
        this.groupdata.cover = res.cover_url.raw;
      },
      err => {
        this.loading2=false;
        console.log(err);
        this.showError(err);
      }
    );
  }
  ionViewWillLeave() {

   
   
    this.nativePageTransitions.slide(this.options1);
   
   }
   
   
   // example of adding a transition when pushing a new page
   openPage(page: any) {
   
     this.nativePageTransitions.slide(this.options1);
     this.navCtrl.navigateForward(page);
   
   }
  ngOnInit() {
    this.userForm = this.fb.group({
     
     
      groupname: ['', Validators.required],
      groupdescription: ['', Validators.required],
     
    });
  }
  loadPrivatePosts() {
    this.api.getPrivatePosts().subscribe(res => {
      this.posts = res;
    });
  }
  // Used for browser direct file upload
 



  async selectImage() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
      result => console.log('Has permission?',result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
    );
    
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    // this.loading=true;
  
    let options = {
    
      maximumImagesCount: 1,
 
      width: 200,
     
      quality: 25,

      outputType: 1
    };
    let imageResponse = [];
    this.imagePicker.getPictures(options).then(async (results) => {
      
  
  
  
        var imageData = results[0].toString();
      //  var imageData1 = imageData.toString();
        var byteCharacters = atob(imageData.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
        var byteNumbers = new Array(byteCharacters.length);
        for (var i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        
        var byteArray = new Uint8Array(byteNumbers);
        var blob1 = new Blob([ byteArray ], {
           type : "jpg"
        });
        console.log(blob1);
      this.api.uploadgroup(blob1,"jpg","test", this.token,"bp_avatar_upload",this.groupdata.id,"avatar").subscribe(
            async (res : any) => {
              this.groupdata.media_url=res.thumb;
          
          
            const toast = await this.toastCtrl.create({
              message: 'Image Uploaded',
              duration: 3000
            });
            toast.present();
      // Get the entire data
      
      
           
          },
          err => {
            console.log(err);
            this.showError(err);
          }
          
          );
     //   });
    //  });
  //  } 
  }, (err) => {
    alert(err);
  });
  
  }
   async uploadFile(event: EventTarget) {
  
   
        const loading = await this.loadingController.create({
        
          message: 'Please wait...',
          duration: 2000
        });
        await loading.present();
      const eventObj: MSInputMethodContext = event as MSInputMethodContext;
      const target: HTMLInputElement = eventObj.target as HTMLInputElement;
      const file1 = target.files[0];
      this.api.uploadgroup2(file1,"bp_avatar_upload",this.groupdata.id,"avatar").subscribe(
        async (res : any) => {
          this.groupdata.media_url=res.thumb;
        const toast = await this.toastCtrl.create({
          message: 'Image Uploaded',
          duration: 3000
        });
        toast.present();
  // Get the entire data
  
  
       
      },
      err => {
        console.log(err);
        this.showError(err);
      }
      
      );
    }



    async selectImage2() {
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
        result => console.log('Has permission?',result.hasPermission),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
      );
      
      this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
      // this.loading=true;
    
      let options = {
      
        maximumImagesCount: 1,
   
        width: 200,
       
        quality: 25,
  
        outputType: 1
      };
      let imageResponse = [];
      this.imagePicker.getPictures(options).then(async (results) => {
        
    
    
    
          var imageData = results[0].toString();
        //  var imageData1 = imageData.toString();
          var byteCharacters = atob(imageData.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
          var byteNumbers = new Array(byteCharacters.length);
          for (var i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
          }
          
          var byteArray = new Uint8Array(byteNumbers);
          var blob1 = new Blob([ byteArray ], {
             type : "jpg"
          });
          console.log(blob1);
        this.api.uploadgroup(blob1,"jpg","test", this.token,"bp_cover_image_upload",this.groupdata.id,"cover").subscribe(
              async (res : any) => {
                this.groupdata.cover=res.image;
            
            
              const toast = await this.toastCtrl.create({
                message: 'Image Uploaded',
                duration: 3000
              });
              toast.present();
        // Get the entire data
        
        
             
            },
            err => {
              console.log(err);
              this.showError(err);
            }
            
            );
       //   });
      //  });
    //  } 
    }, (err) => {
      alert(err);
    });
    
    }
     async uploadCoverFile(event: EventTarget) {
    
     
          const loading = await this.loadingController.create({
          
            message: 'Please wait...',
            duration: 2000
          });
          await loading.present();
        const eventObj: MSInputMethodContext = event as MSInputMethodContext;
        const target: HTMLInputElement = eventObj.target as HTMLInputElement;
        const file1 = target.files[0];
        this.api.uploadgroup2(file1,"bp_cover_image_upload",this.groupdata.id,"cover").subscribe(
          async (res : any) => {
            this.groupdata.cover=res.image;
          const toast = await this.toastCtrl.create({
            message: 'Image Uploaded',
            duration: 3000
          });
          toast.present();
    // Get the entire data
    
    
         
        },
        err => {
          console.log(err);
          this.showError(err);
        }
        
        );
      }
  








  async selectImageSource() {
    const buttons = [
      
      {
        text: 'Choose From Photos Photo',
        icon: 'image',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          this.selectImage();
        }
      }
    ];
 
    // Only allow file selection inside a browser
    if (!this.plt.is('hybrid')) {
      buttons.push({
        text: 'Choose a File',
        icon: 'attach',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          this.fileInput.nativeElement.click();
        }
      });
    }
 
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons
    });
    await actionSheet.present();
  }
  async selectcoverImageSource() {
    const buttons = [
     
      {
        text: 'Choose From Photos Photo',
        icon: 'image',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          this.selectImage2();
        }
      }
    ];
 
    // Only allow file selection inside a browser
    if (!this.plt.is('hybrid')) {
      buttons.push({
        text: 'Choose a File',
        icon: 'attach',
        handler: () => {
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?',result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
         
          this.fileInput2.nativeElement.click();
        }
      });
    }
 
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons
    });
    await actionSheet.present();
  }


  goToprofile() {
    this.router.navigate(['/member-detail']);
  }
  goToHome(res) {
    swal.fire({
      title: 'Success',
      text: 'Thank you for registrations',
      icon: 'success',
      backdrop: false,
    });
    this.router.navigate(['/tabs/home-new']);
  }
//   uploadCoverFile(event: EventTarget) {
//     this.loading3=true;
//     this.edit_icon2=false;
//     const eventObj: MSInputMethodContext = event as MSInputMethodContext;
//     const target: HTMLInputElement = eventObj.target as HTMLInputElement;
//     const file: File = target.files[0];
//     this.api.uploadImageFile2(file,this.data.id).subscribe(
//       async (res : any) => {
//         this.loading3=false;
//         this.edit_icon2=true;
//       console.log("---------------");
//       console.log(res);
//       const toast = await this.toastCtrl.create({
//         message: 'Profile Cover Image Updated',
//         duration: 3000
//       });
//       toast.present();
// // Get the entire data
// this.storage.get('USER_INFO').then(valueStr => {
//   let value = valueStr ? valueStr : {};

//    // Modify just that property
//    value.cover_url = res.image;
  
// this.data.cover_url=res.image;
//    // Save the entire data again
//    this.storage.set('USER_INFO', value);
//     window.location.reload();
// });

     
//     },
//     err => {
//       this.loading3=false;
//       this.edit_icon2=true;
//       console.log(err);
//       this.showError(err);
//     }
    
//     );
//   }


 
  async showError(err) {
    const alert = await this.alertCtrl.create({
      header: err.error.code,
      subHeader: err.error.data,
      message: err.error.message,
      buttons: ['OK']
    });
    await alert.present();
  }
  // goToHome() {
  //   swal.fire({
  //     title: 'Success',
  //     text: 'Thank you for registrations',
  //     icon: 'success',
  //     backdrop: false,
  //   });
  //   this.router.navigate(['/tabs/home-new']);
  // }

}

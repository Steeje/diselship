import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GroupUpdatePageRoutingModule } from './group-update-routing.module';

import { GroupUpdatePage } from './group-update.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule ,
    IonicModule,
    GroupUpdatePageRoutingModule
  ],
  declarations: [GroupUpdatePage]
})
export class GroupUpdatePageModule {}

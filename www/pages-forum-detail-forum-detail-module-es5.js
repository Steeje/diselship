function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-forum-detail-forum-detail-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forum-detail/forum-detail.page.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forum-detail/forum-detail.page.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesForumDetailForumDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-button (click)=\"BackButton()\" slot=\"start\">\n      <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n    </ion-button>\n    <ion-title color=\"light\">{{data.name}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n    <div class=\"cover\" [style.backgroundImage]=\"'url('+data.cover+')'\">\n     \n      <div class=\"data_div\">\n        <ion-label class=\"group_title\">{{data.title}}</ion-label>\n      \n        <ion-label class=\"description\">\n          {{data.shortdescription}}\n          \n          </ion-label>\n          <ion-chip style=\"background-color: white; margin-left: 42%;\">\n            <ion-avatar>\n              <img src=\"{{data.admin_image}}\">\n            </ion-avatar>\n            <ion-label>{{data.admin_name}}</ion-label>\n          </ion-chip>\n        <!-- <div class=\"admin\">\n          <img src=\"assets/imgs/clock.png\">\n          <ion-label class=\"admin_name\">John</ion-label>\n        </div> -->\n        </div>\n    </div>\n\n    <div class=\"personal_div\">\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px; border-top: 0px\"  >\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"chatbubbles-outline\"></ion-icon>\n         \n          <p class=\"lable-icon\"> Discussions </p>\n         \n        </ion-label>\n\n        <ion-label class=\"admin_name\">{{data.total_reply_count}} Discussions . Updated {{data.date}} ago</ion-label>\n      </div>\n      \n      <div class=\"notification_div\"    *ngFor=\"let item of topics\" >\n   \n        <div class=\"flex_div\" (click)=\"singlediscussion(item.id)\" style=\"justify-content: flex-start;\">\n          <div class=\"back_image\" [ngStyle]= \"{'background-image': 'url('+ item.author_image +')'}\" ></div>\n          <div>\n            <ion-label class=\"header_lbl\">{{item.title}}\n            </ion-label>\n            <ion-label class=\"desc count\">{{item.voice_count}} Members  . {{item.total_reply_count}} Replies</ion-label>\n            <ion-label class=\"desc \">{{item.date}}\n            </ion-label>\n           \n          </div>\n        </div>\n        <!-- <div class=\"btn_div\" *ngIf=\"item.btn_text != ''\">\n          <ion-button fill=\"outline\" size=\"small\">\n            {{item.btn_text}}\n          </ion-button>\n        </div> -->\n      </div>\n\n      <!-- <ion-grid fixed>\n        <ion-row>\n          <ion-col size=\"6\">\n            <ion-button expand=\"block\" class=\"save\" size=\"small\" fill=\"outline\">\n              SAVE\n            </ion-button>\n          </ion-col>\n          <ion-col size=\"6\">\n            <ion-button size=\"small\" class=\"apply\" expand=\"block\">\n              APPLY\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid> -->\n    </div>\n\n    <!-- <div class=\"job_div\">\n      <ion-label class=\"job_header\">Job Description</ion-label>\n      <ion-label>Web Designer - No of opening : 6</ion-label>\n      <ion-label>Required Experience : 1 - 3 Years</ion-label>\n      <ion-label>Job brief We are looking for a talented Web..</ion-label>\n    </div>\n\n    <div class=\"white_div\">\n      <ion-label class=\"blue_lbl\" style=\"text-align: center;\">SEE MORE</ion-label>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>Set alert for similar jobs</ion-label>\n      <ion-label class=\"light_lbl\">Web Designer, Bhavnagar, India</ion-label>\n      <ion-toggle></ion-toggle>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>See company insights for Initappz Shop with Premiun</ion-label>\n      <ion-button class=\"premium_btn\" expand=\"block\">\n        TRY PREMIUM FOR FREE\n      </ion-button>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>Keep up with updates and jobs</ion-label>\n      <div class=\"flex_div\">\n        <img src=\"assets/imgs/user.jpg\" class=\"image\">\n        <div class=\"content_div\">\n          <ion-label class=\"head_lbl\">Initappz Shop</ion-label>\n          <ion-label class=\"small_lbl\">E-learning 22,300 followers</ion-label>\n        </div>\n        <div>\n          <ion-button class=\"fallow_btn\" expand=\"block\" fill=\"clear\" size=\"small\">\n            + Follow\n          </ion-button>\n        </div>\n      </div>\n\n    </div> -->\n  </div>\n</ion-content>\n<!-- <ion-footer>\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button expand=\"block\" class=\"save\" size=\"small\" fill=\"outline\">\n          SAVE\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-button size=\"small\" class=\"apply\" expand=\"block\">\n          APPLY\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer> -->";
    /***/
  },

  /***/
  "./src/app/pages/forum-detail/forum-detail-routing.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/pages/forum-detail/forum-detail-routing.module.ts ***!
    \*******************************************************************/

  /*! exports provided: ForumDetailPageRoutingModule */

  /***/
  function srcAppPagesForumDetailForumDetailRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ForumDetailPageRoutingModule", function () {
      return ForumDetailPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _forum_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./forum-detail.page */
    "./src/app/pages/forum-detail/forum-detail.page.ts");

    var routes = [{
      path: '',
      component: _forum_detail_page__WEBPACK_IMPORTED_MODULE_3__["ForumDetailPage"]
    }];

    var ForumDetailPageRoutingModule = function ForumDetailPageRoutingModule() {
      _classCallCheck(this, ForumDetailPageRoutingModule);
    };

    ForumDetailPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ForumDetailPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/forum-detail/forum-detail.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/pages/forum-detail/forum-detail.module.ts ***!
    \***********************************************************/

  /*! exports provided: ForumDetailPageModule */

  /***/
  function srcAppPagesForumDetailForumDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ForumDetailPageModule", function () {
      return ForumDetailPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _forum_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./forum-detail-routing.module */
    "./src/app/pages/forum-detail/forum-detail-routing.module.ts");
    /* harmony import */


    var _forum_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./forum-detail.page */
    "./src/app/pages/forum-detail/forum-detail.page.ts");

    var ForumDetailPageModule = function ForumDetailPageModule() {
      _classCallCheck(this, ForumDetailPageModule);
    };

    ForumDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _forum_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["ForumDetailPageRoutingModule"]],
      declarations: [_forum_detail_page__WEBPACK_IMPORTED_MODULE_6__["ForumDetailPage"]]
    })], ForumDetailPageModule);
    /***/
  },

  /***/
  "./src/app/pages/forum-detail/forum-detail.page.scss":
  /*!***********************************************************!*\
    !*** ./src/app/pages/forum-detail/forum-detail.page.scss ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesForumDetailForumDetailPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\nion-toolbar ion-input {\n  border-bottom: 1px solid white;\n  --color: white;\n  --placeholder-color: white;\n  --padding-start: 8px;\n}\nion-toolbar ion-icon {\n  color: white;\n  font-size: 24px;\n}\n.notification_div {\n  padding: 15px 10px;\n  position: relative;\n  background: white;\n  padding: 15px 10px;\n  position: relative;\n  border-radius: 5px;\n  margin-top: 20px;\n  box-shadow: 1px 2px 6px -1px #848484;\n  background: white;\n}\n.notification_div .time_div {\n  position: absolute;\n  right: 10px;\n  top: 5px;\n  color: gray;\n}\n.notification_div .flex_div {\n  display: flex;\n  width: 100%;\n  align-items: center;\n}\n.notification_div .flex_div .back_image {\n  height: 50px;\n  width: 50px;\n  min-width: 50px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.notification_div .flex_div .header_lbl {\n  font-weight: 600;\n  margin-left: 10px;\n  font-size: 14px;\n  width: 100%;\n}\n.notification_div .flex_div .desc {\n  margin-left: 10px;\n  font-size: 14px;\n  width: 100%;\n}\n.notification_div .btn_div {\n  padding-left: 60px;\n  margin-top: 5px;\n}\n.notification_div .btn_div ion-button {\n  font-weight: 600;\n  --border-radius: 3px;\n  --border-color: var(--ion-color-main);\n  --color: var(--ion-color-main);\n  font-size: 16px;\n}\n.save {\n  --border-color: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 16px;\n  --border-radius: 3px;\n  color: var(--ion-color-main);\n}\n.apply {\n  --background: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 16px;\n  --border-radius: 3px;\n}\nion-content {\n  --background: #F5F5F5;\n}\n.white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n.white_div ion-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  right: 16px;\n  font-size: 20px;\n}\n.white_div ion-button {\n  color: var(--ion-color-main);\n  font-size: 600;\n}\n.main_content_div {\n  width: 100%;\n}\n.main_content_div .no_border {\n  border-bottom: none !important;\n}\n.main_content_div ion-label {\n  display: block;\n}\n.main_content_div .cover {\n  box-shadow: 0px -2px 12px black;\n  width: 100%;\n  height: 250px;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  position: relative;\n}\n.main_content_div .cover .user_image {\n  width: 70px;\n  height: 70px;\n  border-radius: 15px;\n  position: absolute;\n  left: 40%;\n  top: 25%;\n  background: white;\n  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.2);\n}\n.main_content_div .cover .data_div {\n  padding: 16px;\n  padding-top: 114px;\n  padding-right: 10%;\n}\n.main_content_div .cover .data_div ion-label {\n  margin-bottom: 2px;\n}\n.main_content_div .cover .data_div .group_title {\n  text-align: center;\n  font-size: 20px;\n  padding-top: 30px;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n  font-weight: bolder;\n}\n.main_content_div .cover .data_div .group_type {\n  font-size: 14px;\n  text-align: center;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n}\n.main_content_div .cover .data_div .description {\n  font-size: 14px;\n  text-align: center;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n}\n.main_content_div .cover .data_div .admin {\n  display: flex;\n  padding-bottom: 5px;\n  text-align: center;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n}\n.main_content_div .cover .data_div .admin img {\n  width: 15px;\n  height: 15px;\n  text-align: center;\n}\n.main_content_div .cover .data_div .admin .admin_name {\n  text-align: center;\n  margin-left: 10px;\n  padding: 0;\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .personal_div {\n  background: white;\n  padding: 16px;\n}\n.main_content_div .personal_div ion-label {\n  margin-bottom: 2px;\n}\n.main_content_div .personal_div .job_title {\n  font-size: 20px;\n  padding-top: 30px;\n}\n.main_content_div .personal_div .compamy_name {\n  font-size: 14px;\n}\n.main_content_div .personal_div .address {\n  font-size: 14px;\n}\n.main_content_div .personal_div .easy_div {\n  display: flex;\n  padding-bottom: 5px;\n}\n.main_content_div .personal_div .easy_div img {\n  width: 15px;\n  height: 15px;\n}\n.main_content_div .personal_div .easy_div .easy {\n  font-size: 12px;\n  margin-left: 10px;\n  padding: 0;\n}\n.main_content_div .personal_div .easy_div .time {\n  margin-left: 10px;\n  padding: 0;\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .job_div {\n  background: white;\n  padding: 16px;\n  margin-top: 10px;\n}\n.main_content_div .job_div .job_header {\n  padding-bottom: 5px;\n  font-size: 16px !important;\n}\n.main_content_div .job_div ion-label {\n  font-size: 14px;\n  padding-bottom: 5px;\n}\n.main_content_div .white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n  border-top: 1px solid lightgray;\n}\n.main_content_div .white_div .blue_lbl {\n  color: var(--ion-color-main);\n  padding-top: 15px;\n  padding-bottom: 15px;\n  font-weight: 600;\n}\n.main_content_div .white_div .left-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  left: 16px;\n  font-size: 26px;\n}\n.main_content_div .white_div .lable-icon {\n  padding-left: 22px;\n  padding-top: 5px;\n  color: var(--ion-color-main);\n  font-size: 17px;\n  cursor: pointer;\n}\n.main_content_div .white_div .right-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  right: 16px;\n  font-size: 20px;\n}\n.main_content_div .desc_div {\n  margin-top: 10px;\n  padding: 16px;\n  background: white;\n  position: relative;\n}\n.main_content_div .desc_div .light_lbl {\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .desc_div ion-toggle {\n  position: absolute;\n  right: 16px;\n  top: 50%;\n  transform: translateY(-50%);\n}\n.main_content_div .desc_div .premium_btn {\n  margin-top: 10px;\n  --border-radius: 3px;\n  height: 40px;\n  --background: #78710c;\n}\n.main_content_div .desc_div .flex_div {\n  display: flex;\n  align-items: center;\n  padding-top: 10px;\n  width: 100%;\n  justify-content: space-between;\n}\n.main_content_div .desc_div .flex_div .image {\n  height: 45px;\n  width: 45px;\n  max-width: 45px;\n  border-radius: 3px;\n}\n.main_content_div .desc_div .flex_div .content_div {\n  margin-left: 10px;\n}\n.main_content_div .desc_div .flex_div .content_div .head_lbl {\n  font-weight: 600;\n}\n.main_content_div .desc_div .flex_div .content_div .small_lbl {\n  font-size: 14px;\n}\n.main_content_div .desc_div .flex_div .fallow_btn {\n  font-size: 14px;\n  font-weight: 600;\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZm9ydW0tZGV0YWlsL0Q6XFxSYWd1dmFyYW4gaW1hZ2VcXGRpc2Vsc2hpcC9zcmNcXGFwcFxccGFnZXNcXGZvcnVtLWRldGFpbFxcZm9ydW0tZGV0YWlsLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZm9ydW0tZGV0YWlsL2ZvcnVtLWRldGFpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQ0FBQTtBQ0NKO0FEQUk7RUFDSSw4QkFBQTtFQUNBLGNBQUE7RUFDQSwwQkFBQTtFQUNBLG9CQUFBO0FDRVI7QURBSTtFQUNJLFlBQUE7RUFDQSxlQUFBO0FDRVI7QURDQTtFQUNJLGtCQUFBO0VBRUEsa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esb0NBQUE7RUFDQSxpQkFBQTtBQ0NKO0FEQUk7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtBQ0VSO0FEQUk7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0FDRVI7QUREUTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDR1o7QURBUTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ0VaO0FEQVE7RUFDSSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDRVo7QURFSTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtBQ0FSO0FERVE7RUFDSSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EscUNBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7QUNBWjtBREtBO0VBQ0kscUNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtFQUNBLDRCQUFBO0FDRko7QURJQTtFQUNJLG1DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7QUNESjtBREdBO0VBQ0kscUJBQUE7QUNBSjtBREVBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNDSjtBRGNJO0VBQ0ksNEJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FDWlI7QURlSTtFQUNJLDRCQUFBO0VBQ0EsY0FBQTtBQ2JSO0FEZ0JBO0VBQ0ksV0FBQTtBQ2JKO0FEZUk7RUFDSSw4QkFBQTtBQ2JSO0FEZ0JJO0VBQ0ksY0FBQTtBQ2RSO0FEaUJJO0VBQ0ksK0JBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0FDZlI7QURnQlE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUlBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLGlCQUFBO0VBQ0EsMENBQUE7QUNqQlo7QURtQlE7RUFDSSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ2pCWjtBRG1CWTtFQUNJLGtCQUFBO0FDakJoQjtBRG9CWTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUNsQmhCO0FEc0JZO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0FDcEJoQjtBRHNCWTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtBQ3BCaEI7QURzQlk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtBQ3BCaEI7QURxQmdCO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ25CcEI7QURxQmdCO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ25CcEI7QUQyQkk7RUFDSSxpQkFBQTtFQUNBLGFBQUE7QUN6QlI7QUQyQlE7RUFDSSxrQkFBQTtBQ3pCWjtBRDRCUTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtBQzFCWjtBRDRCUTtFQUNJLGVBQUE7QUMxQlo7QUQ0QlE7RUFDSSxlQUFBO0FDMUJaO0FENEJRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FDMUJaO0FEMkJZO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUN6QmhCO0FEMkJZO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtBQ3pCaEI7QUQyQlk7RUFDSSxpQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ3pCaEI7QUQ4Qkk7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQzVCUjtBRCtCUTtFQUNJLG1CQUFBO0VBQ0EsMEJBQUE7QUM3Qlo7QURnQ1E7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QUM5Qlo7QURpQ0k7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLCtCQUFBO0FDL0JSO0FEZ0NRO0VBQ0ksNEJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7QUM5Qlo7QURpQ0k7RUFDSSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUMvQlI7QURpQ0k7RUFDTSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQy9CVjtBRGlDSTtFQUNJLDRCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQy9CUjtBRG9DSTtFQUNJLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNsQ1I7QURvQ1E7RUFDSSxlQUFBO0VBQ0EsV0FBQTtBQ2xDWjtBRHFDUTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtBQ25DWjtBRHNDUTtFQUNJLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7QUNwQ1o7QUR1Q1E7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSw4QkFBQTtBQ3JDWjtBRHNDWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDcENoQjtBRHVDWTtFQUNJLGlCQUFBO0FDckNoQjtBRHNDZ0I7RUFDSSxnQkFBQTtBQ3BDcEI7QURzQ2dCO0VBQ0ksZUFBQTtBQ3BDcEI7QUR3Q1k7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0FDdENoQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2ZvcnVtLWRldGFpbC9mb3J1bS1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGlvbi1pbnB1dHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAgICAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgLS1wbGFjZWhvbGRlci1jb2xvciA6IHdoaXRlO1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQgOiA4cHg7XG4gICAgfVxuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICB9XG59XG4ubm90aWZpY2F0aW9uX2RpdntcbiAgICBwYWRkaW5nOiAxNXB4IDEwcHg7XG4gICBcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgcGFkZGluZzogMTVweCAxMHB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICBib3gtc2hhZG93OiAxcHggMnB4IDZweCAtMXB4ICM4NDg0ODQ7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgLnRpbWVfZGl2e1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHJpZ2h0OiAxMHB4O1xuICAgICAgICB0b3A6IDVweDtcbiAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgfVxuICAgIC5mbGV4X2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIC5iYWNrX2ltYWdle1xuICAgICAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICAgICAgICBtaW4td2lkdGg6IDUwcHg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgfVxuXG4gICAgICAgIC5oZWFkZXJfbGJse1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgICAgLmRlc2N7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmJ0bl9kaXZ7XG4gICAgICAgIHBhZGRpbmctbGVmdDogNjBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuXG4gICAgICAgIGlvbi1idXR0b257XG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICB9XG4gICAgfVxuICAgIFxufVxuLnNhdmV7XG4gICAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuLmFwcGx5e1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogM3B4O1xufVxuaW9uLWNvbnRlbnR7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRjVGNUY1O1xufVxuLndoaXRlX2RpdntcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIC8vIGlvbi1idXR0b257XG4gICAgLy8gICAgIC0tYm9yZGVyLXJhZGl1czogMHB4O1xuICAgIC8vICAgICBpb24tbGFiZWwge1xuICAgIC8vICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAvLyAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgIC8vICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgIC8vICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAvLyAgICAgfSAgIFxuICAgIC8vICAgICBpb24taWNvbntcbiAgICAvLyAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgLy8gICAgIH1cbiAgICAvLyB9XG5cbiAgICBcbiAgICBpb24taWNvbntcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICByaWdodDogMTZweDtcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgIH1cblxuICAgIGlvbi1idXR0b257XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIGZvbnQtc2l6ZTogNjAwO1xuICAgIH1cbn1cbi5tYWluX2NvbnRlbnRfZGl2e1xuICAgIHdpZHRoOiAxMDAlO1xuICBcbiAgICAubm9fYm9yZGVye1xuICAgICAgICBib3JkZXItYm90dG9tOiBub25lICFpbXBvcnRhbnQ7XG4gICAgfVxuXG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuXG4gICAgLmNvdmVye1xuICAgICAgICBib3gtc2hhZG93OiAwcHggLTJweCAxMnB4IGJsYWNrO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAyNTBweDtcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIC51c2VyX2ltYWdle1xuICAgICAgICAgICAgd2lkdGg6IDcwcHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDcwcHg7XG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICBsZWZ0OiA0MCU7XG4gICAgICAgICAgICB0b3A6IDI1JTtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDNweCA2cHggcmdiYSgwLDAsMCwwLjIpO1xuICAgICAgICB9XG4gICAgICAgIC5kYXRhX2RpdntcbiAgICAgICAgICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTE0cHg7XG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMCU7XG4gICAgXG4gICAgICAgICAgICBpb24tbGFiZWwge1xuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDJweDtcbiAgICAgICAgICAgIH1cbiAgICBcbiAgICAgICAgICAgIC5ncm91cF90aXRsZXtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgICAgIHBhZGRpbmctdG9wOiAzMHB4O1xuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgICAgICB0ZXh0LXNoYWRvdzogMnB4IDJweCA4cHggYmxhY2s7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgLmdyb3VwX3R5cGV7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgICAgICAgICAgdGV4dC1zaGFkb3c6IDJweCAycHggOHB4IGJsYWNrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmRlc2NyaXB0aW9ue1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgICAgIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5hZG1pbntcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgICAgICB0ZXh0LXNoYWRvdzogMnB4IDJweCA4cHggYmxhY2s7XG4gICAgICAgICAgICAgICAgaW1ne1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTVweDtcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxNXB4O1xuICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5hZG1pbl9uYW1le1xuICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIFxuICAgIH1cblxuICAgICAgIFxuICAgIC5wZXJzb25hbF9kaXZ7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuXG4gICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAycHg7XG4gICAgICAgIH1cblxuICAgICAgICAuam9iX3RpdGxle1xuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDMwcHg7XG4gICAgICAgIH1cbiAgICAgICAgLmNvbXBhbXlfbmFtZXtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgfVxuICAgICAgICAuYWRkcmVzc3tcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgfVxuICAgICAgICAuZWFzeV9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgICAgICAgICAgIGltZ3tcbiAgICAgICAgICAgICAgICB3aWR0aDogMTVweDtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDE1cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuZWFzeXtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC50aW1le1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmpvYl9kaXZ7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuXG5cbiAgICAgICAgLmpvYl9oZWFkZXJ7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cblxuICAgICAgICBpb24tbGFiZWwge1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgICAgICAgfVxuICAgIH1cbiAgICAud2hpdGVfZGl2e1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICAuYmx1ZV9sYmx7XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgLmxlZnQtaWNvbntcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBsZWZ0OiAxNnB4O1xuICAgICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgfVxuICAgIC5sYWJsZS1pY29ue1xuICAgICAgICAgIHBhZGRpbmctbGVmdDogMjJweDtcbiAgICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xuICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgZm9udC1zaXplOiAxN3B4O1xuICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB9XG4gICAgLnJpZ2h0LWljb257XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgcmlnaHQ6IDE2cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICB9XG5cbiAgICB9XG5cbiAgICAuZGVzY19kaXZ7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgICAgICAgLmxpZ2h0X2xibHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICB9XG5cbiAgICAgICAgaW9uLXRvZ2dsZXtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIHJpZ2h0OiAxNnB4O1xuICAgICAgICAgICAgdG9wOiA1MCU7XG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gICAgICAgIH1cblxuICAgICAgICAucHJlbWl1bV9idG57XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQ6ICM3ODcxMGM7XG4gICAgICAgIH1cblxuICAgICAgICAuZmxleF9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgICAgICAuaW1hZ2V7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA0NXB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiA0NXB4O1xuICAgICAgICAgICAgICAgIG1heC13aWR0aDogNDVweDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5jb250ZW50X2RpdntcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgICAgICAgICAuaGVhZF9sYmx7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5zbWFsbF9sYmx7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5mYWxsb3dfYnRue1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0iLCJpb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXRvb2xiYXIgaW9uLWlucHV0IHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAtLWNvbG9yOiB3aGl0ZTtcbiAgLS1wbGFjZWhvbGRlci1jb2xvcjogd2hpdGU7XG4gIC0tcGFkZGluZy1zdGFydDogOHB4O1xufVxuaW9uLXRvb2xiYXIgaW9uLWljb24ge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMjRweDtcbn1cblxuLm5vdGlmaWNhdGlvbl9kaXYge1xuICBwYWRkaW5nOiAxNXB4IDEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDE1cHggMTBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIGJveC1zaGFkb3c6IDFweCAycHggNnB4IC0xcHggIzg0ODQ4NDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG4ubm90aWZpY2F0aW9uX2RpdiAudGltZV9kaXYge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxMHB4O1xuICB0b3A6IDVweDtcbiAgY29sb3I6IGdyYXk7XG59XG4ubm90aWZpY2F0aW9uX2RpdiAuZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5ub3RpZmljYXRpb25fZGl2IC5mbGV4X2RpdiAuYmFja19pbWFnZSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDUwcHg7XG4gIG1pbi13aWR0aDogNTBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4ubm90aWZpY2F0aW9uX2RpdiAuZmxleF9kaXYgLmhlYWRlcl9sYmwge1xuICBmb250LXdlaWdodDogNjAwO1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICB3aWR0aDogMTAwJTtcbn1cbi5ub3RpZmljYXRpb25fZGl2IC5mbGV4X2RpdiAuZGVzYyB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm5vdGlmaWNhdGlvbl9kaXYgLmJ0bl9kaXYge1xuICBwYWRkaW5nLWxlZnQ6IDYwcHg7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5ub3RpZmljYXRpb25fZGl2IC5idG5fZGl2IGlvbi1idXR0b24ge1xuICBmb250LXdlaWdodDogNjAwO1xuICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXNpemU6IDE2cHg7XG59XG5cbi5zYXZlIHtcbiAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxNnB4O1xuICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuLmFwcGx5IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG59XG5cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjRjVGNUY1O1xufVxuXG4ud2hpdGVfZGl2IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cbi53aGl0ZV9kaXYgaW9uLWljb24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxNnB4O1xuICBmb250LXNpemU6IDIwcHg7XG59XG4ud2hpdGVfZGl2IGlvbi1idXR0b24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXNpemU6IDYwMDtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5ub19ib3JkZXIge1xuICBib3JkZXItYm90dG9tOiBub25lICFpbXBvcnRhbnQ7XG59XG4ubWFpbl9jb250ZW50X2RpdiBpb24tbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciB7XG4gIGJveC1zaGFkb3c6IDBweCAtMnB4IDEycHggYmxhY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDI1MHB4O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAudXNlcl9pbWFnZSB7XG4gIHdpZHRoOiA3MHB4O1xuICBoZWlnaHQ6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNDAlO1xuICB0b3A6IDI1JTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJveC1zaGFkb3c6IDBweCAzcHggNnB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYge1xuICBwYWRkaW5nOiAxNnB4O1xuICBwYWRkaW5nLXRvcDogMTE0cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgaW9uLWxhYmVsIHtcbiAgbWFyZ2luLWJvdHRvbTogMnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvdmVyIC5kYXRhX2RpdiAuZ3JvdXBfdGl0bGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgcGFkZGluZy10b3A6IDMwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggOHB4IGJsYWNrO1xuICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvdmVyIC5kYXRhX2RpdiAuZ3JvdXBfdHlwZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgLmRlc2NyaXB0aW9uIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggOHB4IGJsYWNrO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvdmVyIC5kYXRhX2RpdiAuYWRtaW4ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggOHB4IGJsYWNrO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvdmVyIC5kYXRhX2RpdiAuYWRtaW4gaW1nIHtcbiAgd2lkdGg6IDE1cHg7XG4gIGhlaWdodDogMTVweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvdmVyIC5kYXRhX2RpdiAuYWRtaW4gLmFkbWluX25hbWUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nOiAwO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOiBncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nOiAxNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiBpb24tbGFiZWwge1xuICBtYXJnaW4tYm90dG9tOiAycHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfZGl2IC5qb2JfdGl0bGUge1xuICBmb250LXNpemU6IDIwcHg7XG4gIHBhZGRpbmctdG9wOiAzMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiAuY29tcGFteV9uYW1lIHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiAuYWRkcmVzcyB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgLmVhc3lfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgLmVhc3lfZGl2IGltZyB7XG4gIHdpZHRoOiAxNXB4O1xuICBoZWlnaHQ6IDE1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfZGl2IC5lYXN5X2RpdiAuZWFzeSB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIHBhZGRpbmc6IDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfZGl2IC5lYXN5X2RpdiAudGltZSB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nOiAwO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOiBncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmpvYl9kaXYge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZzogMTZweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5qb2JfZGl2IC5qb2JfaGVhZGVyIHtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgZm9udC1zaXplOiAxNnB4ICFpbXBvcnRhbnQ7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuam9iX2RpdiBpb24tbGFiZWwge1xuICBmb250LXNpemU6IDE0cHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAud2hpdGVfZGl2IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC53aGl0ZV9kaXYgLmJsdWVfbGJsIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLndoaXRlX2RpdiAubGVmdC1pY29uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAxNnB4O1xuICBmb250LXNpemU6IDI2cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAud2hpdGVfZGl2IC5sYWJsZS1pY29uIHtcbiAgcGFkZGluZy1sZWZ0OiAyMnB4O1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXNpemU6IDE3cHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC53aGl0ZV9kaXYgLnJpZ2h0LWljb24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxNnB4O1xuICBmb250LXNpemU6IDIwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBwYWRkaW5nOiAxNnB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5saWdodF9sYmwge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOiBncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IGlvbi10b2dnbGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxNnB4O1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5wcmVtaXVtX2J0biB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIC0tYmFja2dyb3VuZDogIzc4NzEwYztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYgLmltYWdlIHtcbiAgaGVpZ2h0OiA0NXB4O1xuICB3aWR0aDogNDVweDtcbiAgbWF4LXdpZHRoOiA0NXB4O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiAuY29udGVudF9kaXYgLmhlYWRfbGJsIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IC5zbWFsbF9sYmwge1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IC5mYWxsb3dfYnRuIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBtYXJnaW46IDA7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/pages/forum-detail/forum-detail.page.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/forum-detail/forum-detail.page.ts ***!
    \*********************************************************/

  /*! exports provided: ForumDetailPage */

  /***/
  function srcAppPagesForumDetailForumDetailPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ForumDetailPage", function () {
      return ForumDetailPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic-native/native-page-transitions/ngx */
    "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var ForumDetailPage = /*#__PURE__*/function () {
      function ForumDetailPage(navCtrl, nativePageTransitions, router, location, route, api) {
        _classCallCheck(this, ForumDetailPage);

        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.router = router;
        this.location = location;
        this.route = route;
        this.api = api;
        this.data = {};
        this.topics = [];
        this.creator = {};
        this.options = {
          direction: 'up',
          duration: 500,
          slowdownfactor: 3,
          slidePixels: 20,
          iosdelay: 100,
          androiddelay: 150,
          fixedPixelsTop: 0,
          fixedPixelsBottom: 60
        };
      } // gotoGroupFeed() {
      //   this.router.navigate(['/group-feed']);
      // }


      _createClass(ForumDetailPage, [{
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.nativePageTransitions.slide(this.options);
        }
      }, {
        key: "singlediscussion",
        value: function singlediscussion(idd) {
          // var index =  this.topics.find(  ({ id }) => id ===  idd);
          var index = this.topics.find(function (data) {
            return data.id === idd;
          });
          index.forum_name = this.data.title;
          console.log(index);
          var navigationExtras = {
            queryParams: {
              special: JSON.stringify(index)
            }
          };
          this.router.navigate(['forum-reply'], navigationExtras);
        } // example of adding a transition when pushing a new page

      }, {
        key: "openPage",
        value: function openPage(page) {
          this.nativePageTransitions.slide(this.options);
          this.navCtrl.navigateForward(page);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.id1 = this.route.snapshot.paramMap.get('id');
          this.api.getForumDetails(this.id1).subscribe(function (res) {
            console.log(res);
            _this.data = res;
            console.log(_this.data);

            _this.api.getuser(_this.data['author']).subscribe(function (res) {
              console.log(res);
              _this.creator = res;
              console.log(_this.creator['name']);
              _this.data.admin_name = res.name;
              _this.data.admin_image = res.avatar_urls['full'];
            });

            _this.api.getForumTopics(_this.id1).subscribe(function (res) {
              _this.topics = res;
              console.log("topics", _this.topics);
            });
          });
        }
      }, {
        key: "BackButton",
        value: function BackButton() {
          this.location.back();
        }
      }, {
        key: "members",
        value: function members() {
          var navigationExtras = {
            queryParams: {
              id: this.id1,
              name: this.data.name
            }
          };
          this.router.navigate(['members-list'], navigationExtras);
        }
      }, {
        key: "photos",
        value: function photos() {
          var navigationExtras = {
            queryParams: {
              id: this.id1,
              name: this.data.name,
              type: "group_id"
            }
          };
          this.router.navigate(['photo-gallery'], navigationExtras);
        }
      }]);

      return ForumDetailPage;
    }();

    ForumDetailPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"]
      }, {
        type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__["NativePageTransitions"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"]
      }];
    };

    ForumDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-forum-detail',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./forum-detail.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forum-detail/forum-detail.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./forum-detail.page.scss */
      "./src/app/pages/forum-detail/forum-detail.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"], _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__["NativePageTransitions"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"]])], ForumDetailPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-forum-detail-forum-detail-module-es5.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-job-education-job-education-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-education/job-education.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-education/job-education.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-button (click)=\"BackButton()\" slot=\"start\">\n      <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n    </ion-button>\n    <ion-title color=\"light\">Education Details</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n   \n\n\n\n    <div class=\"form_div\">\n   \n      <form [formGroup]=\"userForm\" novalidate (ngSubmit)=\"userupdate()\">\n\n\n        <ion-item>\n          <ion-label position=\"floating\">Main Subjects</ion-label>\n          <ion-input  formControlName=\"main_subjects\" [(ngModel)]=\"data.main_subjects\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.main_subjects.errors?.required\">\n          Main Subjects is required.\n        </span>\n\n      <ion-item>\n        <ion-label position=\"floating\">Certificate No </ion-label>\n        <ion-input  formControlName=\"certificate_no\" [(ngModel)]=\"data.certificate_no\" class=\"ip\" required></ion-input>\n      </ion-item>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.certificate_no.errors?.required\">\n        Certificate No is required.\n      </span>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.certificate_no.errors?.pattern\">\n        Only numerical values allowed.\n      </span>\n\n      <ion-item>\n        <ion-label position=\"floating\">PCM Mark(%) </ion-label>\n        <ion-input  formControlName=\"pcm_mark\" [(ngModel)]=\"data.pcm_mark\" class=\"ip\" required></ion-input>\n      </ion-item>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.pcm_mark.errors?.required\">\n        PCM Mark(%) is required.\n      </span>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.pcm_mark.errors?.pattern\">\n        Only numerical values allowed.\n      </span>\n\n\n      <ion-item>\n        <ion-label position=\"floating\">Result(%)/Grade</ion-label>\n        <ion-input type=\"text\" formControlName=\"result_grade\" [(ngModel)]=\"data.result_grade\" class=\"ip\" required></ion-input>\n      </ion-item>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.result_grade.errors?.required\">\n        Result(%)/Grade is required.\n      </span>\n\n\n      <ion-item>\n        <ion-label position=\"floating\">Name of University</ion-label>\n        <ion-input  formControlName=\"name_of_university\" [(ngModel)]=\"data.name_of_university\" class=\"ip\" required></ion-input>\n      </ion-item>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.name_of_university.errors?.required\">\n        Name of University is required.\n      </span>\n\n      <ion-item>\n        <ion-label position=\"floating\">Name of Institution </ion-label>\n        <ion-input type=\"text\" formControlName=\"name_of_institution\" [(ngModel)]=\"data.name_of_institution\" class=\"ip\" required></ion-input>\n      </ion-item>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.name_of_institution.errors?.required\">\n        Name of Institution is required.\n      </span>\n      <ion-item>\n        <ion-label position=\"floating\">Institute Address </ion-label>\n        <ion-input type=\"text\" formControlName=\"institute_address\" [(ngModel)]=\"data.institute_address\" class=\"ip\" required></ion-input>\n      </ion-item>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.institute_address.errors?.required\">\n        Institute Address is required.\n      </span>\n\n      <ion-item>\n        <ion-label position=\"floating\">Year of Passing </ion-label>\n        <ion-input  formControlName=\"year_of_passing\" [(ngModel)]=\"data.year_of_passing\" class=\"ip\" required></ion-input>\n      </ion-item>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.year_of_passing.errors?.required\">\n        Year of Passing is required.\n      </span>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.year_of_passing.errors?.pattern\">\n        Only numerical values allowed.\n      </span>\n\n      <ion-item>\n        <ion-label position=\"floating\">English Mark(%) : </ion-label>\n        <ion-input  formControlName=\"english_mark\" [(ngModel)]=\"data.english_mark\" class=\"ip\" required></ion-input>\n      </ion-item>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.english_mark.errors?.required\">\n        English Mark(%) : is required.\n      </span>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.english_mark.errors?.pattern\">\n        Only numerical values allowed.\n      </span>\n     \n\n      <ion-button class=\"join_now\"  type=\"submit\" expand=\"full\">\n         <ion-spinner *ngIf=\"loading2\"></ion-spinner> <ion-label >   Update </ion-label>\n      </ion-button>\n      </form>\n    </div>\n\n  \n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/job-education/job-education-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/job-education/job-education-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: JobEducationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobEducationPageRoutingModule", function() { return JobEducationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _job_education_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./job-education.page */ "./src/app/pages/job-education/job-education.page.ts");




const routes = [
    {
        path: '',
        component: _job_education_page__WEBPACK_IMPORTED_MODULE_3__["JobEducationPage"]
    }
];
let JobEducationPageRoutingModule = class JobEducationPageRoutingModule {
};
JobEducationPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], JobEducationPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/job-education/job-education.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/job-education/job-education.module.ts ***!
  \*************************************************************/
/*! exports provided: JobEducationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobEducationPageModule", function() { return JobEducationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _job_education_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./job-education-routing.module */ "./src/app/pages/job-education/job-education-routing.module.ts");
/* harmony import */ var _job_education_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./job-education.page */ "./src/app/pages/job-education/job-education.page.ts");







let JobEducationPageModule = class JobEducationPageModule {
};
JobEducationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _job_education_routing_module__WEBPACK_IMPORTED_MODULE_5__["JobEducationPageRoutingModule"]
        ],
        declarations: [_job_education_page__WEBPACK_IMPORTED_MODULE_6__["JobEducationPage"]]
    })
], JobEducationPageModule);



/***/ }),

/***/ "./src/app/pages/job-education/job-education.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/job-education/job-education.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header_div {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n.header_div .logo_div {\n  display: flex;\n}\n.header_div .logo_div .header_lbl {\n  color: var(--ion-color-main);\n  font-weight: bold;\n  font-size: 18px;\n  margin-left: 10px;\n}\n.header_div .logo_div .logo_img {\n  margin-left: 1px;\n  width: 20px;\n  height: 20px;\n}\n.header_div ion-button {\n  color: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 14px;\n}\n.error {\n  color: red;\n  padding: 5px;\n}\nion-toolbar {\n  --background: var(--ion-color-main);\n}\nion-toolbar ion-input {\n  border-bottom: 1px solid white;\n  --color: white;\n  --placeholder-color: white;\n  --padding-start: 8px;\n}\nion-toolbar ion-icon {\n  color: white;\n  font-size: 24px;\n}\n#content {\n  background-color: #ffffff;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n  height: 305px;\n  overflow: unset;\n  display: block;\n  border-radius: 15px;\n  background-size: cover;\n}\n.edit-btn {\n  font-size: 23px;\n  float: right;\n  margin: 10px;\n  color: white;\n  padding: 7px;\n  background: var(--ion-color-main);\n  border-radius: 10px;\n  position: absolute;\n  right: 0%;\n}\n.edit-btn2 {\n  font-size: 23px;\n  float: right;\n  margin: 10px;\n  color: white;\n  padding: 7px;\n  background: var(--ion-color-main);\n  border-radius: 10px;\n  position: absolute;\n  right: 28%;\n  z-index: 10;\n  top: 51%;\n}\n#profile-info {\n  width: 100%;\n  z-index: 2;\n  padding-top: 1px;\n  text-align: center;\n  position: absolute;\n  top: 45%;\n}\n#profile-image {\n  display: block;\n  border-radius: 120px;\n  border: 3px solid #fff;\n  width: 128px;\n  background: white;\n  height: 128px;\n  margin: 30px auto 0;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n}\n.main_content_div {\n  width: 100%;\n  padding: 20px;\n  height: 100%;\n}\n.main_content_div .header_lbl {\n  font-size: 26px;\n  font-weight: 600;\n}\n.main_content_div .join_btn {\n  --background: white;\n  --color: #0077B5;\n  border: 1px solid var(--ion-color-main);\n}\n.main_content_div .btn_search {\n  height: 25px;\n  width: 25px;\n  margin-right: 20px;\n}\n.main_content_div .or_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  height: 50px;\n  align-items: center;\n  justify-content: space-between;\n}\n.main_content_div .or_div .line_img {\n  height: 10px;\n  width: 40%;\n}\n.main_content_div .or_div .or_text {\n  color: black;\n}\n.main_content_div .form_div ion-item {\n  border-bottom: none;\n  --padding-start: 0px;\n}\n.main_content_div .form_div ion-item .ip {\n  border-bottom: 1px solid white;\n}\n.main_content_div .form_div .join_now {\n  margin-top: 20px;\n  border: 1px solid white;\n  --background: var(--ion-color-main);\n  font-weight: bold;\n  font-size: 18px;\n}\n.main_content_div .form_div .bottom_lbl {\n  display: block;\n  font-size: 14px;\n  margin-top: 20px;\n}\n.main_content_div .form_div .bottom_lbl span {\n  color: var(--ion-color-main) !important;\n  font-weight: bold;\n  color: gra;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvam9iLWVkdWNhdGlvbi9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxqb2ItZWR1Y2F0aW9uXFxqb2ItZWR1Y2F0aW9uLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvam9iLWVkdWNhdGlvbi9qb2ItZWR1Y2F0aW9uLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUNDSjtBREFJO0VBQ0ksYUFBQTtBQ0VSO0FERFE7RUFDSSw0QkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDR1o7QUREUTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNHWjtBREFJO0VBQ0ksNEJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNFUjtBRENBO0VBQ0ksVUFBQTtFQUNBLFlBQUE7QUNFSjtBREFBO0VBQ0ksbUNBQUE7QUNHSjtBREZJO0VBQ0ksOEJBQUE7RUFDQSxjQUFBO0VBQ0EsMEJBQUE7RUFDQSxvQkFBQTtBQ0lSO0FERkk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtBQ0lSO0FEREE7RUFDSSx5QkFBQTtFQUNBLDRDQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0lKO0FERkU7RUFDRSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlDQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7QUNLSjtBREhBO0VBQ0ksZUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxpQ0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7QUNNSjtBREpFO0VBQ0UsV0FBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFFQSxRQUFBO0FDTUo7QURKRTtFQUNFLGNBQUE7RUFDQSxvQkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtFQUVBLG1CQUFBO0VBQ0EsNENBQUE7QUNNSjtBREZBO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0FDS0o7QURISTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtBQ0tSO0FESEk7RUFDSSxtQkFBQTtFQUNBLGdCQUFBO0VBRUEsdUNBQUE7QUNJUjtBREZJO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQ0lSO0FERkk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUNJUjtBREZRO0VBQ0ksWUFBQTtFQUNBLFVBQUE7QUNJWjtBREZRO0VBQ0ksWUFBQTtBQ0laO0FEQ1E7RUFDSSxtQkFBQTtFQUNBLG9CQUFBO0FDQ1o7QURBWTtFQUNJLDhCQUFBO0FDRWhCO0FERVE7RUFDSSxnQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUNBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNBWjtBREdRO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0RaO0FER1k7RUFDSSx1Q0FBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtBQ0RoQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2pvYi1lZHVjYXRpb24vam9iLWVkdWNhdGlvbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyX2RpdntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgLmxvZ29fZGl2e1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAuaGVhZGVyX2xibHtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICB9XG4gICAgICAgIC5sb2dvX2ltZ3tcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxcHg7XG4gICAgICAgICAgICB3aWR0aDogMjBweDtcbiAgICAgICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgICAgfVxuICAgIH1cbiAgICBpb24tYnV0dG9ue1xuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgfVxufVxuLmVycm9ye1xuICAgIGNvbG9yOiByZWQ7XG4gICAgcGFkZGluZzogNXB4O1xufVxuaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGlvbi1pbnB1dHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAgICAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgLS1wbGFjZWhvbGRlci1jb2xvciA6IHdoaXRlO1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQgOiA4cHg7XG4gICAgfVxuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICB9XG59XG4jY29udGVudCB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYigwIDAgMCAvIDQwJSk7XG4gICAgaGVpZ2h0OiAzMDVweDtcbiAgICBvdmVyZmxvdzogdW5zZXQ7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICB9XG4gIC5lZGl0LWJ0bntcbiAgICBmb250LXNpemU6IDIzcHg7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIG1hcmdpbjogMTBweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogN3B4O1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogMCU7XG59XG4uZWRpdC1idG4ye1xuICAgIGZvbnQtc2l6ZTogMjNweDtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgbWFyZ2luOiAxMHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBwYWRkaW5nOiA3cHg7XG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAyOCU7XG4gICAgei1pbmRleDogMTA7XG4gICAgdG9wOiA1MSU7XG59XG4gICNwcm9maWxlLWluZm8ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHotaW5kZXg6IDI7XG4gICAgcGFkZGluZy10b3A6IDFweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIFxuICAgIHRvcDogNDUlO1xuICB9XG4gICNwcm9maWxlLWltYWdlIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBib3JkZXItcmFkaXVzOiAxMjBweDtcbiAgICBib3JkZXI6IDNweCBzb2xpZCAjZmZmO1xuICAgIHdpZHRoOiAxMjhweDtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICBoZWlnaHQ6IDEyOHB4O1xuICAgXG4gICAgbWFyZ2luOiAzMHB4IGF1dG8gMDtcbiAgICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgIC8vIGJveC1zaGFkb3c6IDBweCAwcHggNHB4IHJnYmEoMCwgMCwgMCwgMC43KTtcbiAgfVxuXG4ubWFpbl9jb250ZW50X2RpdntcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAyMHB4O1xuICAgIGhlaWdodDogMTAwJTtcblxuICAgIC5oZWFkZXJfbGJse1xuICAgICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgfVxuICAgIC5qb2luX2J0bntcbiAgICAgICAgLS1iYWNrZ3JvdW5kIDogd2hpdGU7XG4gICAgICAgIC0tY29sb3I6ICMwMDc3QjU7XG4gICAgICAgIC8vIC0tYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIH1cbiAgICAuYnRuX3NlYXJjaHtcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgICAgICB3aWR0aDogMjVweDtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xuICAgIH1cbiAgICAub3JfZGl2e1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG5cbiAgICAgICAgLmxpbmVfaW1ne1xuICAgICAgICAgICAgaGVpZ2h0OiAxMHB4O1xuICAgICAgICAgICAgd2lkdGg6IDQwJTtcbiAgICAgICAgfVxuICAgICAgICAub3JfdGV4dHtcbiAgICAgICAgICAgIGNvbG9yOiBibGFjaztcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5mb3JtX2RpdntcbiAgICAgICAgaW9uLWl0ZW17XG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiBub25lO1xuICAgICAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gICAgICAgICAgICAuaXB7XG4gICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLmpvaW5fbm93e1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHdoaXRlO1xuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIH1cblxuICAgICAgICAuYm90dG9tX2xibHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcblxuICAgICAgICAgICAgc3BhbntcbiAgICAgICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgICAgICAgICAgY29sb3I6IGdyYTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0iLCIuaGVhZGVyX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5oZWFkZXJfZGl2IC5sb2dvX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uaGVhZGVyX2RpdiAubG9nb19kaXYgLmhlYWRlcl9sYmwge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5oZWFkZXJfZGl2IC5sb2dvX2RpdiAubG9nb19pbWcge1xuICBtYXJnaW4tbGVmdDogMXB4O1xuICB3aWR0aDogMjBweDtcbiAgaGVpZ2h0OiAyMHB4O1xufVxuLmhlYWRlcl9kaXYgaW9uLWJ1dHRvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLmVycm9yIHtcbiAgY29sb3I6IHJlZDtcbiAgcGFkZGluZzogNXB4O1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXRvb2xiYXIgaW9uLWlucHV0IHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAtLWNvbG9yOiB3aGl0ZTtcbiAgLS1wbGFjZWhvbGRlci1jb2xvcjogd2hpdGU7XG4gIC0tcGFkZGluZy1zdGFydDogOHB4O1xufVxuaW9uLXRvb2xiYXIgaW9uLWljb24ge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMjRweDtcbn1cblxuI2NvbnRlbnQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgaGVpZ2h0OiAzMDVweDtcbiAgb3ZlcmZsb3c6IHVuc2V0O1xuICBkaXNwbGF5OiBibG9jaztcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cblxuLmVkaXQtYnRuIHtcbiAgZm9udC1zaXplOiAyM3B4O1xuICBmbG9hdDogcmlnaHQ7XG4gIG1hcmdpbjogMTBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiA3cHg7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMCU7XG59XG5cbi5lZGl0LWJ0bjIge1xuICBmb250LXNpemU6IDIzcHg7XG4gIGZsb2F0OiByaWdodDtcbiAgbWFyZ2luOiAxMHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDdweDtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAyOCU7XG4gIHotaW5kZXg6IDEwO1xuICB0b3A6IDUxJTtcbn1cblxuI3Byb2ZpbGUtaW5mbyB7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAyO1xuICBwYWRkaW5nLXRvcDogMXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA0NSU7XG59XG5cbiNwcm9maWxlLWltYWdlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJvcmRlci1yYWRpdXM6IDEyMHB4O1xuICBib3JkZXI6IDNweCBzb2xpZCAjZmZmO1xuICB3aWR0aDogMTI4cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEyOHB4O1xuICBtYXJnaW46IDMwcHggYXV0byAwO1xuICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMjBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmhlYWRlcl9sYmwge1xuICBmb250LXNpemU6IDI2cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuam9pbl9idG4ge1xuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLWNvbG9yOiAjMDA3N0I1O1xuICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuYnRuX3NlYXJjaCB7XG4gIGhlaWdodDogMjVweDtcbiAgd2lkdGg6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5vcl9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA1MHB4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4ubWFpbl9jb250ZW50X2RpdiAub3JfZGl2IC5saW5lX2ltZyB7XG4gIGhlaWdodDogMTBweDtcbiAgd2lkdGg6IDQwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5vcl9kaXYgLm9yX3RleHQge1xuICBjb2xvcjogYmxhY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZm9ybV9kaXYgaW9uLWl0ZW0ge1xuICBib3JkZXItYm90dG9tOiBub25lO1xuICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mb3JtX2RpdiBpb24taXRlbSAuaXAge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZm9ybV9kaXYgLmpvaW5fbm93IHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgd2hpdGU7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxOHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZvcm1fZGl2IC5ib3R0b21fbGJsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mb3JtX2RpdiAuYm90dG9tX2xibCBzcGFuIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IGdyYTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/job-education/job-education.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/job-education/job-education.page.ts ***!
  \***********************************************************/
/*! exports provided: JobEducationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobEducationPage", function() { return JobEducationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/native-page-transitions/ngx */ "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/














let JobEducationPage = class JobEducationPage {
    constructor(navCtrl, nativePageTransitions, loadingController, plt, file, router, api, location, androidPermissions, storage, fb, actionSheetCtrl, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.loadingController = loadingController;
        this.plt = plt;
        this.file = file;
        this.router = router;
        this.api = api;
        this.location = location;
        this.androidPermissions = androidPermissions;
        this.storage = storage;
        this.fb = fb;
        this.actionSheetCtrl = actionSheetCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.data = {};
        this.images = [];
        this.user = this.api.getCurrentUser();
        this.posts = [];
        this.country = [
            {
                value: 'TELENGANA',
            },
            {
                value: 'India',
            },
            {
                value: 'USA',
            },
            {
                value: 'Pakistan',
            },
            {
                value: 'China',
            },
        ];
        this.defaultDate = "1987-06-30";
        this.isSubmitted = false;
        this.options1 = {
            direction: 'up',
            duration: 500,
            slowdownfactor: 3,
            slidePixels: 20,
            iosdelay: 100,
            androiddelay: 150,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 60
        };
        this.loader();
        this.api.getJobdetails(this.token, "educationdetails").subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () { this.data = res; console.log(res); }), err => {
            this.loading2 = false;
            console.log(err);
            this.showError(err);
        });
        this.edit_icon = true;
        this.edit_icon2 = true;
        this.user.subscribe(user => {
            if (user) {
                console.log(user);
                this.token = user.token;
            }
            else {
                this.posts = [];
            }
            // this.storage.get('USER_INFO').then(result => {
            //   if (result != null) {
            //   console.log( result);
            //   this.data.id = result.id;
            //   this.data.nickname = result.nickname;
            //   if(typeof result.nickname === "undefined" )
            //   this.data.nickname = result.user_login;
            //   this.data.firstname = result.first_name;
            //   if(typeof result.first_name === "undefined" )
            //   this.data.firstname = result.xprofile.groups[1].fields[1].value.raw;
            //   this.data.lastname = result.last_name;
            //   if(typeof result.last_name === "undefined" )
            //   this.data.lastname =  result.xprofile.groups[1].fields[2].value.raw;
            //   this.data.description = result.description;
            //   this.data.avatar=result.avatar_urls[24];
            //   this.data.cover=result.cover_url;
            //   console.log(result.avatar_urls[24]);
            //   if(typeof result.avatar_urls[24] === "undefined" )
            //   this.data.avatar=result.avatar_urls['full'];
            //   this.data.email = result.email;
            //   if(typeof result.email === "undefined" )
            //   this.data.email = user.user_email;
            //   }
            //   }).catch(e => {
            //   console.log('error: '+ e);
            //   // Handle errors here
            //   });
        });
    }
    get errorControl() {
        return this.userForm.controls;
    }
    loader() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Please wait...',
                duration: 1500
            });
            yield loading.present();
        });
    }
    ngOnInit() {
        this.userForm = this.fb.group({
            main_subjects: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            certificate_no: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern('^[0-9]+$')]],
            pcm_mark: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern('^[0-9]+$')]],
            result_grade: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            name_of_university: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            name_of_institution: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            institute_address: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            year_of_passing: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern('^[0-9]+$')]],
            english_mark: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern('^[0-9]+$')]]
        });
    }
    userupdate() {
        this.loading2 = true;
        this.isSubmitted = true;
        if (!this.userForm.valid) {
            console.log('Please provide all the required values!');
            this.loading2 = false;
            return false;
        }
        else {
            console.log(this.userForm.value);
        }
        let formdat = [];
        formdat.push({
            meta_key: 'main_subjects',
            meta_value: this.userForm.value.main_subjects
        }, {
            meta_key: 'certificate_no',
            meta_value: this.userForm.value.certificate_no
        }, {
            meta_key: 'pcm_mark',
            meta_value: this.userForm.value.pcm_mark
        }, {
            meta_key: 'result_grade',
            meta_value: this.userForm.value.result_grade
        }, {
            meta_key: 'name_of_university',
            meta_value: this.userForm.value.name_of_university
        }, {
            meta_key: 'name_of_institution',
            meta_value: this.userForm.value.name_of_institution
        }, {
            meta_key: 'institute_address',
            meta_value: this.userForm.value.institute_address
        }, {
            meta_key: 'year_of_passing',
            meta_value: this.userForm.value.year_of_passing
        }, {
            meta_key: 'english_mark',
            meta_value: this.userForm.value.english_mark
        });
        let formdata = [];
        formdata.push({ meta_data: formdat });
        this.api.userjobupdate(formdata, this.token).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.loading2 = false;
            console.log(res);
            const toast = yield this.toastCtrl.create({
                message: 'Profile Updated',
                duration: 3000
            });
            toast.present();
            console.log('Profile Updated', res);
            // this.router.navigate(['/member-detail']);
            // window.location.reload();
        }), err => {
            this.loading2 = false;
            console.log(err);
            this.showError(err);
        });
    }
    BackButton() {
        this.location.back();
    }
    showError(err) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: err.error.code,
                subHeader: err.error.data,
                message: err.error.message,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    ionViewWillLeave() {
        this.nativePageTransitions.slide(this.options1);
    }
    // example of adding a transition when pushing a new page
    openPage(page) {
        this.nativePageTransitions.slide(this.options1);
        this.navCtrl.navigateForward(page);
    }
    loadPrivatePosts() {
        this.api.getPrivatePosts().subscribe(res => {
            this.posts = res;
        });
    }
    selectImageSource() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const buttons = [
                {
                    text: 'Take Photo',
                    icon: 'camera',
                    handler: () => {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA));
                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                        // this.addImage();
                    }
                },
                {
                    text: 'Choose From Photos Photo',
                    icon: 'image',
                    handler: () => {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE));
                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                        // this.selectImage();
                    }
                }
            ];
            // Only allow file selection inside a browser
            if (!this.plt.is('hybrid')) {
                buttons.push({
                    text: 'Choose a File',
                    icon: 'attach',
                    handler: () => {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE));
                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                        // this.fileInput.nativeElement.click();
                    }
                });
            }
            const actionSheet = yield this.actionSheetCtrl.create({
                header: 'Select Image Source',
                buttons
            });
            yield actionSheet.present();
        });
    }
    goToprofile() {
        this.router.navigate(['/member-detail']);
    }
    goToHome(res) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
            title: 'Success',
            text: 'Thank you for registrations',
            icon: 'success',
            backdrop: false,
        });
        this.router.navigate(['/tabs/home-new']);
    }
};
JobEducationPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"] },
    { type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_11__["NativePageTransitions"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"] },
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"] },
    { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__["AndroidPermissions"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ActionSheetController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], JobEducationPage.prototype, "fileInput", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput2', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], JobEducationPage.prototype, "fileInput2", void 0);
JobEducationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-job-education',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./job-education.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-education/job-education.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./job-education.page.scss */ "./src/app/pages/job-education/job-education.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"],
        _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_11__["NativePageTransitions"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"],
        _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"],
        _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"],
        _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__["AndroidPermissions"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ActionSheetController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"]])
], JobEducationPage);



/***/ })

}]);
//# sourceMappingURL=pages-job-education-job-education-module-es2015.js.map
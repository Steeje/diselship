(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-chatlist-chatlist-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/chatlist/chatlist.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/chatlist/chatlist.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button color=\"light\"></ion-back-button>\n    </ion-buttons>\n    <ion-title color=\"light\">Messaging</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n    <div class=\"flex_div\">\n      <ion-input type=\"text\" placeholder=\"Search Message\">\n        <ion-icon name=\"search-outline\"></ion-icon>\n      </ion-input>\n      <ion-icon name=\"options-outline\"></ion-icon>\n    </div>\n\n    <div class=\"flex_div2\">\n      <ion-chip *ngFor=\"let item of chips\">\n        <ion-label>{{item}}</ion-label>\n      </ion-chip>\n    </div>\n\n    <div class=\"content_div\">\n      <div class=\"list_div\" *ngFor=\"let item of users\" (click)=\"goToChat()\">\n        <div class=\"back_image\" [style.backgroundImage]=\"'url('+item.img+')'\"></div>\n        <div class=\"desc\">\n          <ion-label class=\"usernane\">Jonh Doe</ion-label>\n          <ion-label class=\"msg\">You: Hii</ion-label>\n          <ion-label class=\"time\">07:00 PM</ion-label>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/chatlist/chatlist-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/chatlist/chatlist-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: ChatlistPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatlistPageRoutingModule", function() { return ChatlistPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _chatlist_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chatlist.page */ "./src/app/pages/chatlist/chatlist.page.ts");




const routes = [
    {
        path: '',
        component: _chatlist_page__WEBPACK_IMPORTED_MODULE_3__["ChatlistPage"]
    }
];
let ChatlistPageRoutingModule = class ChatlistPageRoutingModule {
};
ChatlistPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ChatlistPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/chatlist/chatlist.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/chatlist/chatlist.module.ts ***!
  \***************************************************/
/*! exports provided: ChatlistPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatlistPageModule", function() { return ChatlistPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _chatlist_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chatlist-routing.module */ "./src/app/pages/chatlist/chatlist-routing.module.ts");
/* harmony import */ var _chatlist_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chatlist.page */ "./src/app/pages/chatlist/chatlist.page.ts");







let ChatlistPageModule = class ChatlistPageModule {
};
ChatlistPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _chatlist_routing_module__WEBPACK_IMPORTED_MODULE_5__["ChatlistPageRoutingModule"]
        ],
        declarations: [_chatlist_page__WEBPACK_IMPORTED_MODULE_6__["ChatlistPage"]]
    })
], ChatlistPageModule);



/***/ }),

/***/ "./src/app/pages/chatlist/chatlist.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/chatlist/chatlist.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div .flex_div {\n  display: flex;\n  align-items: center;\n  padding: 5px 16px;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .flex_div2 {\n  display: inline-flex;\n  border-bottom: 1px solid lightgray;\n  padding: 5px 16px;\n}\n\n.main_content_div .flex_div2 ion-label {\n  white-space: nowrap;\n}\n\n.main_content_div ion-icon {\n  font-size: 22px;\n  color: #505050;\n}\n\n.main_content_div .content_div .list_div {\n  display: flex;\n  padding: 10px 16px;\n  border-bottom: 1px solid lightgray;\n  align-items: center;\n}\n\n.main_content_div .content_div .list_div .back_image {\n  height: 50px;\n  width: 50px;\n  min-width: 50px;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  border-radius: 50%;\n}\n\n.main_content_div .content_div .list_div .desc {\n  width: 100%;\n  padding-left: 16px;\n  position: relative;\n  width: 100%;\n}\n\n.main_content_div .content_div .list_div .desc ion-label {\n  display: block;\n}\n\n.main_content_div .content_div .list_div .desc .usernane {\n  font-weight: 600;\n}\n\n.main_content_div .content_div .list_div .desc .msg {\n  font-size: 14px;\n  margin-top: 3px;\n}\n\n.main_content_div .content_div .list_div .desc .time {\n  position: absolute;\n  font-size: 12px;\n  color: #505050;\n  right: 0;\n  top: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY2hhdGxpc3QvRDpcXFJhZ3V2YXJhbiBpbWFnZVxcZGlzZWxzaGlwL3NyY1xcYXBwXFxwYWdlc1xcY2hhdGxpc3RcXGNoYXRsaXN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvY2hhdGxpc3QvY2hhdGxpc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUNBQUE7QUNDSjs7QURDQTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUNFSjs7QURDQTtFQUNJLFdBQUE7QUNFSjs7QURBSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0NBQUE7QUNFUjs7QURDSTtFQUNJLG9CQUFBO0VBQ0Esa0NBQUE7RUFDQSxpQkFBQTtBQ0NSOztBREVRO0VBQ0ksbUJBQUE7QUNBWjs7QURJSTtFQUNJLGVBQUE7RUFDQSxjQUFBO0FDRlI7O0FETVE7RUFDSSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQ0FBQTtFQUNBLG1CQUFBO0FDSlo7O0FES1k7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtBQ0hoQjs7QURNWTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ0poQjs7QURLZ0I7RUFDSSxjQUFBO0FDSHBCOztBRE1nQjtFQUNJLGdCQUFBO0FDSnBCOztBRE1nQjtFQUNJLGVBQUE7RUFDQSxlQUFBO0FDSnBCOztBRE9nQjtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxRQUFBO0VBQ0EsTUFBQTtBQ0xwQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NoYXRsaXN0L2NoYXRsaXN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZCA6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cbmlvbi10b29sYmFyLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgsIGlvbi10b29sYmFyIC5zYy1pb24tc2VhcmNoYmFyLWlvcy1oe1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcbn1cblxuLm1haW5fY29udGVudF9kaXZ7XG4gICAgd2lkdGg6IDEwMCU7XG5cbiAgICAuZmxleF9kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDVweCAxNnB4O1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgIH1cblxuICAgIC5mbGV4X2RpdjJ7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICBwYWRkaW5nOiA1cHggMTZweDtcblxuXG4gICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgaW9uLWljb257XG4gICAgICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICAgICAgY29sb3I6ICM1MDUwNTA7XG4gICAgfVxuXG4gICAgLmNvbnRlbnRfZGl2e1xuICAgICAgICAubGlzdF9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgcGFkZGluZzogMTBweCAxNnB4O1xuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgICAuYmFja19pbWFnZXtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICAgICAgICAgICAgbWluLXdpZHRoOiA1MHB4O1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAuZGVzY3tcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC51c2VybmFuZXtcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLm1zZ3tcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAzcHg7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLnRpbWV7XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzUwNTA1MDtcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDA7XG4gICAgICAgICAgICAgICAgICAgIHRvcDogMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG4ubWFpbl9jb250ZW50X2RpdiB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZzogNXB4IDE2cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYyIHtcbiAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIHBhZGRpbmc6IDVweCAxNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2MiBpb24tbGFiZWwge1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xufVxuLm1haW5fY29udGVudF9kaXYgaW9uLWljb24ge1xuICBmb250LXNpemU6IDIycHg7XG4gIGNvbG9yOiAjNTA1MDUwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5saXN0X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBhZGRpbmc6IDEwcHggMTZweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAubGlzdF9kaXYgLmJhY2tfaW1hZ2Uge1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiA1MHB4O1xuICBtaW4td2lkdGg6IDUwcHg7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5saXN0X2RpdiAuZGVzYyB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmxpc3RfZGl2IC5kZXNjIGlvbi1sYWJlbCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5saXN0X2RpdiAuZGVzYyAudXNlcm5hbmUge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5saXN0X2RpdiAuZGVzYyAubXNnIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tdG9wOiAzcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmxpc3RfZGl2IC5kZXNjIC50aW1lIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNTA1MDUwO1xuICByaWdodDogMDtcbiAgdG9wOiAwO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/chatlist/chatlist.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/chatlist/chatlist.page.ts ***!
  \*************************************************/
/*! exports provided: ChatlistPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatlistPage", function() { return ChatlistPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/dummy-data.service */ "./src/app/services/dummy-data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/native-page-transitions/ngx */ "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/





let ChatlistPage = class ChatlistPage {
    constructor(navCtrl, nativePageTransitions, dummy, router) {
        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.dummy = dummy;
        this.router = router;
        this.chips = ['All', 'Unread', 'My Connection', 'InMail', 'Archieved', 'Spam'];
        this.options = {
            direction: 'up',
            duration: 500,
            slowdownfactor: 3,
            slidePixels: 20,
            iosdelay: 100,
            androiddelay: 150,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 60
        };
        this.users = this.dummy.users;
    }
    ionViewWillLeave() {
        this.nativePageTransitions.slide(this.options);
    }
    // example of adding a transition when pushing a new page
    openPage(page) {
        this.nativePageTransitions.slide(this.options);
        this.navCtrl.navigateForward(page);
    }
    ngOnInit() {
    }
    goToChat() {
        this.router.navigate(['/chat']);
    }
};
ChatlistPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_4__["NativePageTransitions"] },
    { type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__["dummyDataService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
ChatlistPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chatlist',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./chatlist.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/chatlist/chatlist.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./chatlist.page.scss */ "./src/app/pages/chatlist/chatlist.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"],
        _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_4__["NativePageTransitions"], src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__["dummyDataService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], ChatlistPage);



/***/ }),

/***/ "./src/app/services/dummy-data.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/dummy-data.service.ts ***!
  \************************************************/
/*! exports provided: dummyDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dummyDataService", function() { return dummyDataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

let dummyDataService = class dummyDataService {
    constructor() {
        this.users = [
            {
                img: 'assets/imgs/user.jpg',
                cover: 'assets/imgs/back1.jpg',
                btn_text: 'VIEW JOBS'
            },
            {
                img: 'assets/imgs/user2.jpg',
                cover: 'assets/imgs/back2.jpg',
                btn_text: 'FOLLOW HASHTAG'
            },
            {
                img: 'assets/imgs/user3.jpg',
                cover: 'assets/imgs/back3.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user4.jpg',
                cover: 'assets/imgs/back4.jpg',
                btn_text: 'SEE ALL VIEW'
            },
            {
                img: 'assets/imgs/user.jpg',
                cover: 'assets/imgs/back1.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user2.jpg',
                cover: 'assets/imgs/back2.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user3.jpg',
                cover: 'assets/imgs/back3.jpg',
                btn_text: 'EXPAND YOUR NETWORK'
            },
            {
                img: 'assets/imgs/user4.jpg',
                cover: 'assets/imgs/back4.jpg',
                btn_text: 'SEE ALL VIEW'
            }
        ];
        this.jobs = [
            {
                img: 'assets/imgs/company/initappz.png',
                title: 'SQL Server',
                addr: 'Bhavnagar, Gujrat, India',
                time: '2 school alumni',
                status: '1 day'
            },
            {
                img: 'assets/imgs/company/google.png',
                title: 'Web Designer',
                addr: 'Vadodara, Gujrat, India',
                time: 'Be an early applicant',
                status: '2 days'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                title: 'Business Analyst',
                addr: 'Ahmedabad, Gujrat, India',
                time: 'Be an early applicant',
                status: '1 week'
            },
            {
                img: 'assets/imgs/company/initappz.png',
                title: 'PHP Developer',
                addr: 'Pune, Maharashtra, India',
                time: '2 school alumni',
                status: 'New'
            },
            {
                img: 'assets/imgs/company/google.png',
                title: 'Graphics Designer',
                addr: 'Bhavnagar, Gujrat, India',
                time: 'Be an early applicant',
                status: '1 day'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                title: 'Phython Developer',
                addr: 'Ahmedabad, Gujrat, India',
                time: '2 school alumni',
                status: '5 days'
            },
        ];
        this.company = [
            {
                img: 'assets/imgs/company/initappz.png',
                name: 'Initappz Shop'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                name: 'Microsoft'
            },
            {
                img: 'assets/imgs/company/google.png',
                name: 'Google'
            },
        ];
    }
};
dummyDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], dummyDataService);



/***/ })

}]);
//# sourceMappingURL=pages-chatlist-chatlist-module-es2015.js.map
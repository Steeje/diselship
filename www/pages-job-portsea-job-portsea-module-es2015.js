(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-job-portsea-job-portsea-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-portsea/job-portsea.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-portsea/job-portsea.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-button (click)=\"BackButton()\" slot=\"start\">\n      <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n    </ion-button>\n    <ion-title color=\"light\"> \n      Port Sea Training Details </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n\n\n\n    <div class=\"form_div\">\n   \n      <form [formGroup]=\"userForm\" novalidate (ngSubmit)=\"userupdate()\">\n\n        \n\n        \n      \n\n        <ion-item>\n          <ion-label position=\"floating\">Grade </ion-label>\n          <ion-input  formControlName=\"post_sea_grade1\" [(ngModel)]=\"data.post_sea_grade1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.post_sea_grade1.errors?.required\">\n          Grade is required.\n        </span>\n        <ion-item>\n          <ion-label position=\"floating\">Part/Phase</ion-label>\n          <ion-input type=\"text\" formControlName=\"port_sea_part_phase1\" [(ngModel)]=\"data.port_sea_part_phase1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.port_sea_part_phase1.errors?.required\">\n          Part/Phase is required.\n        </span>\n\n\n\n\n        <ion-item>\n          <ion-label position=\"floating\">Course Name</ion-label>\n          <ion-input  formControlName=\"port_sea_course_name1\" [(ngModel)]=\"data.port_sea_course_name1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.port_sea_course_name1.errors?.required\">\n          Course Name is required.\n        </span>\n        <ion-item>\n          <ion-label position=\"floating\">Training Institute</ion-label>\n          <ion-input type=\"text\" formControlName=\"port_sea_training_institute1\" [(ngModel)]=\"data.port_sea_training_institute1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.port_sea_training_institute1.errors?.required\">\n          Training Institute is required.\n        </span>\n    \n\n\n        \n      <ion-item>\n        <ion-label position=\"floating\">Certificate No </ion-label>\n        <ion-input  formControlName=\"port_sea_certificate_no1\" [(ngModel)]=\"data.port_sea_certificate_no1\" class=\"ip\" required></ion-input>\n      </ion-item>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.port_sea_certificate_no1.errors?.required\">\n        Certificate No is required.\n      </span>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.port_sea_certificate_no1.errors?.pattern\">\n        Only numerical values allowed.\n      </span>\n\n      <ion-item>\n        <ion-label position=\"floating\">Attended From (Date)</ion-label>\n        <ion-datetime (ionChange)=\"getDate($event)\"  [(ngModel)]=\"data.port_sea_attended_from1\"  class=\"ip\" formControlName=\"port_sea_attended_from1\" [value]=\"defaultDate\" required></ion-datetime>\n      </ion-item>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.port_sea_attended_from1.errors?.required\">\n        Attended From (Date) is required.\n      </span>\n\n      <ion-item>\n        <ion-label position=\"floating\">Attended To (Date) </ion-label>\n        <ion-datetime (ionChange)=\"getDate4($event)\"  [(ngModel)]=\"data.port_sea_attended_to_date1\"  class=\"ip\" formControlName=\"port_sea_attended_to_date1\" [value]=\"defaultDate\" required></ion-datetime>\n      \n       \n      </ion-item>\n      <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.port_sea_attended_to_date1.errors?.required\">\n        Attended To (Date) is required.\n      </span>\n\n     \n\n      <ion-button class=\"join_now\"  type=\"submit\" expand=\"full\">\n         <ion-spinner *ngIf=\"loading2\"></ion-spinner> <ion-label >   Update </ion-label>\n      </ion-button>\n      </form>\n    </div>\n\n  \n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/job-portsea/job-portsea-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/job-portsea/job-portsea-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: JobPortseaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobPortseaPageRoutingModule", function() { return JobPortseaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _job_portsea_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./job-portsea.page */ "./src/app/pages/job-portsea/job-portsea.page.ts");




const routes = [
    {
        path: '',
        component: _job_portsea_page__WEBPACK_IMPORTED_MODULE_3__["JobPortseaPage"]
    }
];
let JobPortseaPageRoutingModule = class JobPortseaPageRoutingModule {
};
JobPortseaPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], JobPortseaPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/job-portsea/job-portsea.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/job-portsea/job-portsea.module.ts ***!
  \*********************************************************/
/*! exports provided: JobPortseaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobPortseaPageModule", function() { return JobPortseaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _job_portsea_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./job-portsea-routing.module */ "./src/app/pages/job-portsea/job-portsea-routing.module.ts");
/* harmony import */ var _job_portsea_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./job-portsea.page */ "./src/app/pages/job-portsea/job-portsea.page.ts");







let JobPortseaPageModule = class JobPortseaPageModule {
};
JobPortseaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _job_portsea_routing_module__WEBPACK_IMPORTED_MODULE_5__["JobPortseaPageRoutingModule"]
        ],
        declarations: [_job_portsea_page__WEBPACK_IMPORTED_MODULE_6__["JobPortseaPage"]]
    })
], JobPortseaPageModule);



/***/ }),

/***/ "./src/app/pages/job-portsea/job-portsea.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/job-portsea/job-portsea.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header_div {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n.header_div .logo_div {\n  display: flex;\n}\n.header_div .logo_div .header_lbl {\n  color: var(--ion-color-main);\n  font-weight: bold;\n  font-size: 18px;\n  margin-left: 10px;\n}\n.header_div .logo_div .logo_img {\n  margin-left: 1px;\n  width: 20px;\n  height: 20px;\n}\n.header_div ion-button {\n  color: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 14px;\n}\n.error {\n  color: red;\n  padding: 5px;\n}\nion-toolbar {\n  --background: var(--ion-color-main);\n}\nion-toolbar ion-input {\n  border-bottom: 1px solid white;\n  --color: white;\n  --placeholder-color: white;\n  --padding-start: 8px;\n}\nion-toolbar ion-icon {\n  color: white;\n  font-size: 24px;\n}\n#content {\n  background-color: #ffffff;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n  height: 305px;\n  overflow: unset;\n  display: block;\n  border-radius: 15px;\n  background-size: cover;\n}\n.edit-btn {\n  font-size: 23px;\n  float: right;\n  margin: 10px;\n  color: white;\n  padding: 7px;\n  background: var(--ion-color-main);\n  border-radius: 10px;\n  position: absolute;\n  right: 0%;\n}\n.edit-btn2 {\n  font-size: 23px;\n  float: right;\n  margin: 10px;\n  color: white;\n  padding: 7px;\n  background: var(--ion-color-main);\n  border-radius: 10px;\n  position: absolute;\n  right: 28%;\n  z-index: 10;\n  top: 51%;\n}\n#profile-info {\n  width: 100%;\n  z-index: 2;\n  padding-top: 1px;\n  text-align: center;\n  position: absolute;\n  top: 45%;\n}\n#profile-image {\n  display: block;\n  border-radius: 120px;\n  border: 3px solid #fff;\n  width: 128px;\n  background: white;\n  height: 128px;\n  margin: 30px auto 0;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n}\n.main_content_div {\n  width: 100%;\n  padding: 20px;\n  height: 100%;\n}\n.main_content_div .header_lbl {\n  font-size: 26px;\n  font-weight: 600;\n}\n.main_content_div .join_btn {\n  --background: white;\n  --color: #0077B5;\n  border: 1px solid var(--ion-color-main);\n}\n.main_content_div .btn_search {\n  height: 25px;\n  width: 25px;\n  margin-right: 20px;\n}\n.main_content_div .or_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  height: 50px;\n  align-items: center;\n  justify-content: space-between;\n}\n.main_content_div .or_div .line_img {\n  height: 10px;\n  width: 40%;\n}\n.main_content_div .or_div .or_text {\n  color: black;\n}\n.main_content_div .form_div ion-item {\n  border-bottom: none;\n  --padding-start: 0px;\n}\n.main_content_div .form_div ion-item .ip {\n  border-bottom: 1px solid white;\n}\n.main_content_div .form_div .join_now {\n  margin-top: 20px;\n  border: 1px solid white;\n  --background: var(--ion-color-main);\n  font-weight: bold;\n  font-size: 18px;\n}\n.main_content_div .form_div .bottom_lbl {\n  display: block;\n  font-size: 14px;\n  margin-top: 20px;\n}\n.main_content_div .form_div .bottom_lbl span {\n  color: var(--ion-color-main) !important;\n  font-weight: bold;\n  color: gra;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvam9iLXBvcnRzZWEvRDpcXFJhZ3V2YXJhbiBpbWFnZVxcZGlzZWxzaGlwL3NyY1xcYXBwXFxwYWdlc1xcam9iLXBvcnRzZWFcXGpvYi1wb3J0c2VhLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvam9iLXBvcnRzZWEvam9iLXBvcnRzZWEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtBQ0NKO0FEQUk7RUFDSSxhQUFBO0FDRVI7QUREUTtFQUNJLDRCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNHWjtBRERRO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ0daO0FEQUk7RUFDSSw0QkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQ0VSO0FEQ0E7RUFDSSxVQUFBO0VBQ0EsWUFBQTtBQ0VKO0FEQUE7RUFDSSxtQ0FBQTtBQ0dKO0FERkk7RUFDSSw4QkFBQTtFQUNBLGNBQUE7RUFDQSwwQkFBQTtFQUNBLG9CQUFBO0FDSVI7QURGSTtFQUNJLFlBQUE7RUFDQSxlQUFBO0FDSVI7QUREQTtFQUNJLHlCQUFBO0VBQ0EsNENBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDSUo7QURGRTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUNBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtBQ0tKO0FESEE7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlDQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtBQ01KO0FESkU7RUFDRSxXQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUVBLFFBQUE7QUNNSjtBREpFO0VBQ0UsY0FBQTtFQUNBLG9CQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBRUEsbUJBQUE7RUFDQSw0Q0FBQTtBQ01KO0FERkE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7QUNLSjtBREhJO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0FDS1I7QURISTtFQUNJLG1CQUFBO0VBQ0EsZ0JBQUE7RUFFQSx1Q0FBQTtBQ0lSO0FERkk7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDSVI7QURGSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtBQ0lSO0FERlE7RUFDSSxZQUFBO0VBQ0EsVUFBQTtBQ0laO0FERlE7RUFDSSxZQUFBO0FDSVo7QURDUTtFQUNJLG1CQUFBO0VBQ0Esb0JBQUE7QUNDWjtBREFZO0VBQ0ksOEJBQUE7QUNFaEI7QURFUTtFQUNJLGdCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQ0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ0FaO0FER1E7RUFDSSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDRFo7QURHWTtFQUNJLHVDQUFBO0VBQ0EsaUJBQUE7RUFDQSxVQUFBO0FDRGhCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvam9iLXBvcnRzZWEvam9iLXBvcnRzZWEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlcl9kaXZ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIC5sb2dvX2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgLmhlYWRlcl9sYmx7XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgfVxuICAgICAgICAubG9nb19pbWd7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMXB4O1xuICAgICAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDIwcHg7XG4gICAgICAgIH1cbiAgICB9XG4gICAgaW9uLWJ1dHRvbntcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgIH1cbn1cbi5lcnJvcntcbiAgICBjb2xvcjogcmVkO1xuICAgIHBhZGRpbmc6IDVweDtcbn1cbmlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZCA6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICBpb24taW5wdXR7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcbiAgICAgICAgLS1jb2xvcjogd2hpdGU7XG4gICAgICAgIC0tcGxhY2Vob2xkZXItY29sb3IgOiB3aGl0ZTtcbiAgICAgICAgLS1wYWRkaW5nLXN0YXJ0IDogOHB4O1xuICAgIH1cbiAgICBpb24taWNvbntcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgfVxufVxuI2NvbnRlbnQge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gICAgYm94LXNoYWRvdzogMHB4IC0xcHggMTBweCByZ2IoMCAwIDAgLyA0MCUpO1xuICAgIGhlaWdodDogMzA1cHg7XG4gICAgb3ZlcmZsb3c6IHVuc2V0O1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgfVxuICAuZWRpdC1idG57XG4gICAgZm9udC1zaXplOiAyM3B4O1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBtYXJnaW46IDEwcHg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHBhZGRpbmc6IDdweDtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDAlO1xufVxuLmVkaXQtYnRuMntcbiAgICBmb250LXNpemU6IDIzcHg7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIG1hcmdpbjogMTBweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogN3B4O1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogMjglO1xuICAgIHotaW5kZXg6IDEwO1xuICAgIHRvcDogNTElO1xufVxuICAjcHJvZmlsZS1pbmZvIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB6LWluZGV4OiAyO1xuICAgIHBhZGRpbmctdG9wOiAxcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBcbiAgICB0b3A6IDQ1JTtcbiAgfVxuICAjcHJvZmlsZS1pbWFnZSB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgYm9yZGVyLXJhZGl1czogMTIwcHg7XG4gICAgYm9yZGVyOiAzcHggc29saWQgI2ZmZjtcbiAgICB3aWR0aDogMTI4cHg7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgaGVpZ2h0OiAxMjhweDtcbiAgIFxuICAgIG1hcmdpbjogMzBweCBhdXRvIDA7XG4gICAgYm94LXNoYWRvdzogMHB4IC0xcHggMTBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gICAvLyBib3gtc2hhZG93OiAwcHggMHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuNyk7XG4gIH1cblxuLm1haW5fY29udGVudF9kaXZ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMjBweDtcbiAgICBoZWlnaHQ6IDEwMCU7XG5cbiAgICAuaGVhZGVyX2xibHtcbiAgICAgICAgZm9udC1zaXplOiAyNnB4O1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgIH1cbiAgICAuam9pbl9idG57XG4gICAgICAgIC0tYmFja2dyb3VuZCA6IHdoaXRlO1xuICAgICAgICAtLWNvbG9yOiAjMDA3N0I1O1xuICAgICAgICAvLyAtLWJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICB9XG4gICAgLmJ0bl9zZWFyY2h7XG4gICAgICAgIGhlaWdodDogMjVweDtcbiAgICAgICAgd2lkdGg6IDI1cHg7XG4gICAgICAgIG1hcmdpbi1yaWdodDogMjBweDtcbiAgICB9XG4gICAgLm9yX2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuXG4gICAgICAgIC5saW5lX2ltZ3tcbiAgICAgICAgICAgIGhlaWdodDogMTBweDtcbiAgICAgICAgICAgIHdpZHRoOiA0MCU7XG4gICAgICAgIH1cbiAgICAgICAgLm9yX3RleHR7XG4gICAgICAgICAgICBjb2xvcjogYmxhY2s7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuZm9ybV9kaXZ7XG4gICAgICAgIGlvbi1pdGVte1xuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogbm9uZTtcbiAgICAgICAgICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xuICAgICAgICAgICAgLmlwe1xuICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5qb2luX25vd3tcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCB3aGl0ZTtcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZCA6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICB9XG5cbiAgICAgICAgLmJvdHRvbV9sYmx7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG5cbiAgICAgICAgICAgIHNwYW57XG4gICAgICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgICAgIGNvbG9yOiBncmE7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59IiwiLmhlYWRlcl9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uaGVhZGVyX2RpdiAubG9nb19kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLmhlYWRlcl9kaXYgLmxvZ29fZGl2IC5oZWFkZXJfbGJsIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG4uaGVhZGVyX2RpdiAubG9nb19kaXYgLmxvZ29faW1nIHtcbiAgbWFyZ2luLWxlZnQ6IDFweDtcbiAgd2lkdGg6IDIwcHg7XG4gIGhlaWdodDogMjBweDtcbn1cbi5oZWFkZXJfZGl2IGlvbi1idXR0b24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5lcnJvciB7XG4gIGNvbG9yOiByZWQ7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cbmlvbi10b29sYmFyIGlvbi1pbnB1dCB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcbiAgLS1jb2xvcjogd2hpdGU7XG4gIC0tcGxhY2Vob2xkZXItY29sb3I6IHdoaXRlO1xuICAtLXBhZGRpbmctc3RhcnQ6IDhweDtcbn1cbmlvbi10b29sYmFyIGlvbi1pY29uIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDI0cHg7XG59XG5cbiNjb250ZW50IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgYm94LXNoYWRvdzogMHB4IC0xcHggMTBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gIGhlaWdodDogMzA1cHg7XG4gIG92ZXJmbG93OiB1bnNldDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5lZGl0LWJ0biB7XG4gIGZvbnQtc2l6ZTogMjNweDtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW46IDEwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogN3B4O1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDAlO1xufVxuXG4uZWRpdC1idG4yIHtcbiAgZm9udC1zaXplOiAyM3B4O1xuICBmbG9hdDogcmlnaHQ7XG4gIG1hcmdpbjogMTBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiA3cHg7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMjglO1xuICB6LWluZGV4OiAxMDtcbiAgdG9wOiA1MSU7XG59XG5cbiNwcm9maWxlLWluZm8ge1xuICB3aWR0aDogMTAwJTtcbiAgei1pbmRleDogMjtcbiAgcGFkZGluZy10b3A6IDFweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNDUlO1xufVxuXG4jcHJvZmlsZS1pbWFnZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBib3JkZXItcmFkaXVzOiAxMjBweDtcbiAgYm9yZGVyOiAzcHggc29saWQgI2ZmZjtcbiAgd2lkdGg6IDEyOHB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgaGVpZ2h0OiAxMjhweDtcbiAgbWFyZ2luOiAzMHB4IGF1dG8gMDtcbiAgYm94LXNoYWRvdzogMHB4IC0xcHggMTBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5oZWFkZXJfbGJsIHtcbiAgZm9udC1zaXplOiAyNnB4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmpvaW5fYnRuIHtcbiAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgLS1jb2xvcjogIzAwNzdCNTtcbiAgYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuLm1haW5fY29udGVudF9kaXYgLmJ0bl9zZWFyY2gge1xuICBoZWlnaHQ6IDI1cHg7XG4gIHdpZHRoOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAub3JfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogNTBweDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLm1haW5fY29udGVudF9kaXYgLm9yX2RpdiAubGluZV9pbWcge1xuICBoZWlnaHQ6IDEwcHg7XG4gIHdpZHRoOiA0MCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAub3JfZGl2IC5vcl90ZXh0IHtcbiAgY29sb3I6IGJsYWNrO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZvcm1fZGl2IGlvbi1pdGVtIHtcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZm9ybV9kaXYgaW9uLWl0ZW0gLmlwIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZvcm1fZGl2IC5qb2luX25vdyB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHdoaXRlO1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mb3JtX2RpdiAuYm90dG9tX2xibCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZm9ybV9kaXYgLmJvdHRvbV9sYmwgc3BhbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbikgIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiBncmE7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/job-portsea/job-portsea.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/job-portsea/job-portsea.page.ts ***!
  \*******************************************************/
/*! exports provided: JobPortseaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobPortseaPage", function() { return JobPortseaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/native-page-transitions/ngx */ "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/














let JobPortseaPage = class JobPortseaPage {
    constructor(navCtrl, nativePageTransitions, loadingController, plt, file, router, api, location, androidPermissions, storage, fb, actionSheetCtrl, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.loadingController = loadingController;
        this.plt = plt;
        this.file = file;
        this.router = router;
        this.api = api;
        this.location = location;
        this.androidPermissions = androidPermissions;
        this.storage = storage;
        this.fb = fb;
        this.actionSheetCtrl = actionSheetCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.data = {};
        this.images = [];
        this.user = this.api.getCurrentUser();
        this.posts = [];
        this.country = [
            {
                value: 'TELENGANA',
            },
            {
                value: 'India',
            },
            {
                value: 'USA',
            },
            {
                value: 'Pakistan',
            },
            {
                value: 'China',
            },
        ];
        this.defaultDate = "1987-06-30";
        this.isSubmitted = false;
        this.options1 = {
            direction: 'up',
            duration: 500,
            slowdownfactor: 3,
            slidePixels: 20,
            iosdelay: 100,
            androiddelay: 150,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 60
        };
        this.loader();
        this.api.getJobdetails(this.token, "portsea").subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () { this.data = res; console.log(res); }), err => {
            this.loading2 = false;
            console.log(err);
            this.showError(err);
        });
        this.edit_icon = true;
        this.edit_icon2 = true;
        this.user.subscribe(user => {
            if (user) {
                console.log(user);
                this.token = user.token;
            }
            else {
                this.posts = [];
            }
        });
    }
    get errorControl() {
        return this.userForm.controls;
    }
    loader() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Please wait...',
                duration: 1500
            });
            yield loading.present();
        });
    }
    getDate(e) {
        let date = new Date(e.target.value).toISOString().substring(0, 10);
        this.userForm.get('port_sea_attended_from1').setValue(date, {
            onlyself: true
        });
    }
    getDate4(e) {
        let date = new Date(e.target.value).toISOString().substring(0, 10);
        this.userForm.get('port_sea_attended_to_date1').setValue(date, {
            onlyself: true
        });
    }
    ngOnInit() {
        this.userForm = this.fb.group({
            post_sea_grade1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            port_sea_part_phase1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            port_sea_certificate_no1: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern('^[0-9]+$')]],
            port_sea_course_name1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            port_sea_training_institute1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            port_sea_attended_from1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            port_sea_attended_to_date1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]
        });
    }
    userupdate() {
        this.loading2 = true;
        this.isSubmitted = true;
        if (!this.userForm.valid) {
            console.log('Please provide all the required values!');
            this.loading2 = false;
            return false;
        }
        else {
            console.log(this.userForm.value);
        }
        let formdat = [];
        formdat.push({
            meta_key: 'post_sea_grade1',
            meta_value: this.userForm.value.post_sea_grade1
        }, {
            meta_key: 'port_sea_part_phase1',
            meta_value: this.userForm.value.port_sea_part_phase1
        }, {
            meta_key: 'port_sea_course_name1',
            meta_value: this.userForm.value.port_sea_course_name1
        }, {
            meta_key: 'port_sea_training_institute1',
            meta_value: this.userForm.value.port_sea_training_institute1
        }, {
            meta_key: 'port_sea_certificate_no1',
            meta_value: this.userForm.value.port_sea_certificate_no1
        }, {
            meta_key: 'port_sea_attended_from1',
            meta_value: this.userForm.value.port_sea_attended_from1
        }, {
            meta_key: 'port_sea_attended_to_date1',
            meta_value: this.userForm.value.port_sea_attended_to_date1
        });
        let formdata = [];
        formdata.push({ meta_data: formdat });
        this.api.userjobupdate(formdata, this.token).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.loading2 = false;
            console.log(res);
            const toast = yield this.toastCtrl.create({
                message: 'Profile Updated',
                duration: 3000
            });
            toast.present();
            console.log('Profile Updated', res);
            // this.router.navigate(['/member-detail']);
            // window.location.reload();
        }), err => {
            this.loading2 = false;
            console.log(err);
            this.showError(err);
        });
    }
    BackButton() {
        this.location.back();
    }
    showError(err) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: err.error.code,
                subHeader: err.error.data,
                message: err.error.message,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    ionViewWillLeave() {
        this.nativePageTransitions.slide(this.options1);
    }
    // example of adding a transition when pushing a new page
    openPage(page) {
        this.nativePageTransitions.slide(this.options1);
        this.navCtrl.navigateForward(page);
    }
    loadPrivatePosts() {
        this.api.getPrivatePosts().subscribe(res => {
            this.posts = res;
        });
    }
    selectImageSource() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const buttons = [
                {
                    text: 'Take Photo',
                    icon: 'camera',
                    handler: () => {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA));
                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                        // this.addImage();
                    }
                },
                {
                    text: 'Choose From Photos Photo',
                    icon: 'image',
                    handler: () => {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE));
                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                        // this.selectImage();
                    }
                }
            ];
            // Only allow file selection inside a browser
            if (!this.plt.is('hybrid')) {
                buttons.push({
                    text: 'Choose a File',
                    icon: 'attach',
                    handler: () => {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE));
                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                        // this.fileInput.nativeElement.click();
                    }
                });
            }
            const actionSheet = yield this.actionSheetCtrl.create({
                header: 'Select Image Source',
                buttons
            });
            yield actionSheet.present();
        });
    }
    goToprofile() {
        this.router.navigate(['/member-detail']);
    }
    goToHome(res) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
            title: 'Success',
            text: 'Thank you for registrations',
            icon: 'success',
            backdrop: false,
        });
        this.router.navigate(['/tabs/home-new']);
    }
};
JobPortseaPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"] },
    { type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_11__["NativePageTransitions"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"] },
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"] },
    { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__["AndroidPermissions"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ActionSheetController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], JobPortseaPage.prototype, "fileInput", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput2', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], JobPortseaPage.prototype, "fileInput2", void 0);
JobPortseaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-job-portsea',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./job-portsea.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-portsea/job-portsea.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./job-portsea.page.scss */ "./src/app/pages/job-portsea/job-portsea.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"],
        _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_11__["NativePageTransitions"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"],
        _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"],
        _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"],
        _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__["AndroidPermissions"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ActionSheetController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"]])
], JobPortseaPage);



/***/ })

}]);
//# sourceMappingURL=pages-job-portsea-job-portsea-module-es2015.js.map
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-single-feed-single-feed-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/single-feed/single-feed.page.html":
  /*!***********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/single-feed/single-feed.page.html ***!
    \***********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesSingleFeedSingleFeedPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-button (click)=\"BackButton()\" slot=\"start\">\n      <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n    </ion-button>\n    <ion-title>{{post.group_name}}</ion-title>\n   \n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n\n    <div class=\"desc_div\">\n      <div class=\"flex_div\">\n        <img src=\"{{post.user_avatar}}\" [routerLink]=\"['/members-detail', post.user_id]\" class=\"image\">\n        <div class=\"content_div\">\n     \n          <p *ngIf=\"post.discussion!='' && grpid\" class=\"head_lbl\">{{post.name}} <ion-label style=\"display: initial;color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.type_name}}</ion-label> \n            {{post.discussion}} <ion-label style=\"display: initial;color: #959595; margin-right: 8px;\" class=\"head_type\">in the group</ion-label> <img src=\"{{data.media_url}}\" [routerLink]=\"['/group-detail', grpid]\" style=\"margin: -5px; border-radius: 10px;\"  width=\"20\" height=\"20\" alt=\"Group logo of {{post.group_name}}\">\n           <ion-label style=\"display: initial; margin-left: 8px;\" [routerLink]=\"['/group-detail', grpid]\">{{post.group_name}}</ion-label></p>\n\n\n          <p *ngIf=\"post.discussion=='' && grpid \" class=\"head_lbl\">{{post.name}} <ion-label style=\"display: initial;color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.type_name}}</ion-label> \n          <img src=\"{{data.media_url}}\" style=\"margin: -5px; border-radius: 10px;\"  width=\"20\" height=\"20\" [routerLink]=\"['/group-detail', grpid]\" alt=\"Group logo of {{post.group_name}}\">\n          <ion-label style=\"display: initial; margin-left: 8px;\" [routerLink]=\"['/group-detail', grpid]\">{{post.group_name}}</ion-label></p>\n\n\n\n          <p *ngIf=\"post.component=='bbpress' && !grpid\" class=\"head_lbl\">{{post.name}} <ion-label style=\"display: initial;color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.type_name}}</ion-label> \n            {{post.discussion}} </p>\n\n\n          <p *ngIf=\"post.component=='groups' && !grpid\" class=\"head_lbl\">{{post.name}} <ion-label style=\"display: initial;color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.type_name}}</ion-label> \n          <img src=\"{{post.media_url}}\" style=\"margin: -5px; border-radius: 10px;\" [routerLink]=\"['/group-detail', post.grpid]\" width=\"20\" height=\"20\" alt=\"Group logo of {{post.group_name}}\">\n          <ion-label style=\"display: initial; margin-left: 8px;\" [routerLink]=\"['/group-detail', post.grpid]\">{{post.group_name}}</ion-label></p>\n          \n          <p *ngIf=\"post.component!='groups' && post.component!='bbpress' && !grpid\"  class=\"head_lbl\">{{post.name}} <ion-label style=\"display: initial;color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.title}}</ion-label> \n            </p>\n        \n          <ion-label class=\"small_lbl\" *ngIf=\"!post.modified\">{{post.date}}</ion-label>\n          \n \n        </div>\n\n        <div>\n          <ion-button class=\"fallow_btn\" (click)=\"followuser()\" *ngIf=\"follow\" expand=\"block\" fill=\"clear\" size=\"small\">\n            + Follow\n          </ion-button>\n          <ion-button class=\"fallow_btn\" (click)=\"unfollowuser()\" *ngIf=\"unfollow\" expand=\"block\" fill=\"clear\" size=\"small\">\n            - Unfollow\n          </ion-button>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"content_div\">\n      <ion-label  [innerHTML]=\"post.content_stripped\" class=\"desc_lbl\">\n       \n      </ion-label>\n\n      <!-- (click)=\"openActionSheet()\" <span>#India</span>\n      <span>#Engineering</span>\n      <span>#Ionic</span>\n      <span>#Initappz</span>\n      <span>#Linux</span> -->\n\n      <div (click)=\"singlefeed(post.id)\" *ngIf=\"post.media\">\n        <div *ngFor=\"let item of post.media\">\n                  <div class=\"card_image\" [style.backgroundImage]=\"'url('+item+')'\"></div>\n        \n                </div> </div> \n                \n                <div class=\"like_div2\">\n        <div>\n          <img src=\"assets/imgs/like.png\" alt=\"\">\n          {{post.favorite_count}}\n        </div>\n        <div>{{post.comment_count}} Comments</div>\n      </div>\n\n      <div class=\"like_div\" style=\"padding: 0;\">\n        <div  >\n          <ion-button *ngIf=\"post.favorited\" fill=\"clear\" size=\"small\" class=\"liked\"  (click)=\"likefeed(post.id)\">\n            <ion-icon name=\"thumbs-up-sharp\" style=\"margin-right:5px\"></ion-icon>Like\n          </ion-button>\n          <ion-button *ngIf=\"!post.favorited\" fill=\"clear\" size=\"small\" class=\"unliked\"  (click)=\"likefeed(post.id)\">\n            <ion-icon name=\"thumbs-up-sharp\" style=\"margin-right:5px\"></ion-icon>Like\n          </ion-button>\n        </div>\n\n        <div>\n          <ion-button (click)=\"commentfocus()\" fill=\"clear\" size=\"small\">\n            <ion-icon  name=\"chatbox-ellipses-sharp\"></ion-icon>&nbsp;&nbsp;Comment\n          </ion-button>\n        </div>\n        <div>\n          <ion-button  (click)=\"deletepost(post.id)\" [class.button-disabled]=\"!post.can_delete\" fill=\"clear\" size=\"small\">\n            <ion-icon name=\"trash\"></ion-icon>&nbsp;&nbsp;Delete\n          </ion-button>\n        </div>\n      </div>\n\n      <!-- <div class=\"reaction_div\">\n        <ion-label>Reaction</ion-label>\n\n        <div class=\"users_div\">\n          <div class=\"user_img\" *ngFor=\"let item of (users | slice: 0:5)\" [style.backgroundImage]=\"'url('+item.img+')'\">\n          </div>\n          <div class=\"round\">\n            <ion-icon name=\"ellipsis-horizontal\" class=\"dots\"></ion-icon>\n          </div>\n        </div>\n      </div> -->\n\n      <div class=\"comments_div\">\n        <div class=\"lbls\">\n          <ion-label>Comments</ion-label>\n          <!-- <ion-label class=\"relevent\">Most Relevant</ion-label> -->\n        </div>\n        <div #processContainer>\n        </div>\n        <div *ngFor=\"let item of data\">\n        <div class=\"flex_div\" >\n          <div class=\"user_img\" [style.backgroundImage]=\"'url('+item.user_avatar+')'\" [routerLink]=\"['/members-detail', item.user_id]\"></div>\n          <div id=\"id{{item.id}}\" class=\"blue_div\" >\n            <ion-icon (click)=\"deletepost(item.id)\"   *ngIf=\"item.can_delete\" class=\"abs_icn2\" name=\"trash\"></ion-icon>\n            <ion-icon (click)=\"commentreply(item.id,item.content_stripped,item.user_avatar)\" name=\"arrow-undo-outline\" class=\"abs_icn\"></ion-icon>\n            <ion-label class=\"head_lbl\">{{item.name}}</ion-label>\n           \n            <ion-label  class=\"small_lbl\">{{item.date}}</ion-label>\n\n            <ion-label [innerHTML]=\"item.content_stripped\"  class=\"text\">\n                 </ion-label>\n          </div>\n        \n\n         </div> <div style=\"padding-left: 50px;\" *ngIf=\"item.replycomments\"> \n          <div class=\"flex_div\" *ngFor=\"let itemreply of item.replycomments\">\n            <div class=\"user_img\" [style.backgroundImage]=\"'url('+itemreply.user_avatar+')'\"></div>\n           \n            <div  id=\"id{{itemreply.id}}\" class=\"blue_div\" style=\"\n            background: #aed8f3;\n        \">\n              <ion-icon (click)=\"deletepost(itemreply.id)\" *ngIf=\"itemreply.can_delete\" class=\"abs_icn2\" name=\"trash\"></ion-icon>\n              <ion-icon (click)=\"commentreply(itemreply.id,itemreply.content_stripped,itemreply.user_avatar)\" name=\"arrow-undo-outline\" class=\"abs_icn\"></ion-icon>\n              <ion-label class=\"replytxt\" (click)=\"hightlight(itemreply.secondary_item_id)\">Reply Comment For <p>{{itemreply.replycomment_text}}</p></ion-label>\n              <ion-label style=\"\n              padding-top: 5px;\n          \" class=\"head_lbl\">{{itemreply.name}}</ion-label>\n             \n              <ion-label  class=\"small_lbl\">{{itemreply.date}}</ion-label>\n  \n              <ion-label  [innerHTML]=\"itemreply.content_stripped\"  class=\"text\">\n                   </ion-label>\n            </div>\n           </div>\n          </div></div>\n        </div>\n        </div>\n\n      </div>\n    \n</ion-content>\n\n<ion-footer>\n  <ion-grid>\n    <ion-row>\n    <ion-col size=\"4\" style=\"margin-top: 15px;\" *ngFor=\"let item of file_imgs\">\n      <ion-icon  (click)=\"dltmedia(item.id)\" style=\"position: relative; bottom: 75px; left: 85px;\" name=\"close-circle-outline\"></ion-icon>\n      <img style=\"height: 80px;\n      width: 80px; border-radius: 10px;\" src=\"{{item.img}}\" >\n    </ion-col>\n  </ion-row>\n  </ion-grid>\n  <input\n  type=\"file\"\n  #fileInput\n  (change)=\"uploadFile($event)\"\n  hidden=\"true\"\n  accept=\"image/*\"\n/>\n  <!-- <div style=\"position: relative;\">\n     <ion-button  (click)=\"addImage()\" size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"camera-outline\"></ion-icon>\n    </ion-button> -->\n  <!--  <ion-button size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"videocam-outline\"></ion-icon>\n    </ion-button> -->\n    <!-- <ion-button (click)=\"selectImage()\" size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"image-outline\"></ion-icon>\n    </ion-button>\n    <ion-button *ngIf=\"!plt.is('hybrid')\" (click)=\"attach()\" size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"ellipsis-horizontal\"></ion-icon>\n    </ion-button> -->\n    <!-- <ion-button size=\"small\" fill=\"clear\" class=\"at_btn\">\n      <ion-icon slot=\"icon-only\" name=\"at-outline\"></ion-icon>\n    </ion-button> -->\n  <!-- </div> -->\n  <div class=\"footer_div\">\n    <ion-label class=\"reply\" *ngIf=\"commentid !='' \">Reply To</ion-label> \n      <div class=\"flex_div\" *ngIf=\"commentid!=''\">\n        <div class=\"user_img\" [style.backgroundImage]=\"'url('+commentavatar+')'\"></div>\n      <div class=\"blue_div\">\n        <ion-label *ngIf=\"commenttext!=''\"\n         #commenttextid [innerHTML]=\"commenttext\"  class=\"text\"></ion-label>\n          <ion-input #commentidid type=\"hidden\" *ngIf=\"commentid!=''\" [(ngModel)]=\"commentid\" placeholder=\"Leave your thoughts\"></ion-input>\n        \n </div>\n\n</div>  <div class=\"flex_foot\" > <div class=\"first_div\">\n      <div class=\"user_img\" [style.backgroundImage]=\"'url('+mydata+')'\"></div>\n      <ion-input #commentfoc type=\"text\" [(ngModel)]=\"comment\" placeholder=\"Leave your thoughts\"></ion-input>\n    </div>\n    <div class=\"second_div\">\n      <!-- <ion-label class=\"at\">@</ion-label> -->\n      <ion-button (click)=\"postcomment()\" fill=\"clear\">\n        <ion-spinner *ngIf=\"loading\"></ion-spinner> Post\n      </ion-button>\n    </div>\n  </div>\n  </div>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/pages/single-feed/single-feed-routing.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/single-feed/single-feed-routing.module.ts ***!
    \*****************************************************************/

  /*! exports provided: SingleFeedPageRoutingModule */

  /***/
  function srcAppPagesSingleFeedSingleFeedRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SingleFeedPageRoutingModule", function () {
      return SingleFeedPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _single_feed_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./single-feed.page */
    "./src/app/pages/single-feed/single-feed.page.ts");

    var routes = [{
      path: '',
      component: _single_feed_page__WEBPACK_IMPORTED_MODULE_3__["SingleFeedPage"]
    }];

    var SingleFeedPageRoutingModule = function SingleFeedPageRoutingModule() {
      _classCallCheck(this, SingleFeedPageRoutingModule);
    };

    SingleFeedPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], SingleFeedPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/single-feed/single-feed.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/single-feed/single-feed.module.ts ***!
    \*********************************************************/

  /*! exports provided: SingleFeedPageModule */

  /***/
  function srcAppPagesSingleFeedSingleFeedModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SingleFeedPageModule", function () {
      return SingleFeedPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _single_feed_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./single-feed-routing.module */
    "./src/app/pages/single-feed/single-feed-routing.module.ts");
    /* harmony import */


    var _single_feed_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./single-feed.page */
    "./src/app/pages/single-feed/single-feed.page.ts");

    var SingleFeedPageModule = function SingleFeedPageModule() {
      _classCallCheck(this, SingleFeedPageModule);
    };

    SingleFeedPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _single_feed_routing_module__WEBPACK_IMPORTED_MODULE_5__["SingleFeedPageRoutingModule"]],
      declarations: [_single_feed_page__WEBPACK_IMPORTED_MODULE_6__["SingleFeedPage"]]
    })], SingleFeedPageModule);
    /***/
  },

  /***/
  "./src/app/pages/single-feed/single-feed.page.scss":
  /*!*********************************************************!*\
    !*** ./src/app/pages/single-feed/single-feed.page.scss ***!
    \*********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesSingleFeedSingleFeedPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\nion-button {\n  --box-shadow:none;\n}\n\nion-title {\n  color: white;\n}\n\n.main_content_div {\n  width: 100%;\n  padding: 16px;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div .liked {\n  color: #047ead !important;\n}\n\n.main_content_div .unliked {\n  color: gray;\n}\n\n.main_content_div .head_lbl {\n  display: block;\n  font-weight: 500;\n  text-align: left;\n  font-size: 14px;\n  -webkit-margin-after: 5px;\n          margin-block-end: 5px;\n}\n\n.main_content_div .desc_div {\n  background: white;\n  position: relative;\n}\n\n.main_content_div .desc_div .flex_div {\n  display: flex;\n  align-items: center;\n  width: 100%;\n  justify-content: space-between;\n}\n\n.main_content_div .desc_div .flex_div .image {\n  height: 45px;\n  width: 45px;\n  min-width: 45px;\n  border-radius: 3px;\n}\n\n.main_content_div .desc_div .flex_div .content_div {\n  margin-left: 10px;\n}\n\n.main_content_div .desc_div .flex_div .content_div .head_lbl {\n  font-weight: 600;\n}\n\n.main_content_div .desc_div .flex_div .content_div .small_lbl {\n  font-size: 14px;\n}\n\n.main_content_div .desc_div .flex_div .fallow_btn {\n  font-size: 14px;\n  font-weight: 600;\n  margin: 0;\n}\n\n.main_content_div .content_div {\n  margin-top: 10px;\n}\n\n.main_content_div .content_div .desc_lbl {\n  font-size: 14px;\n  margin-bottom: 20px;\n}\n\n.main_content_div .content_div span {\n  margin-right: 10px;\n  font-weight: 600;\n  color: var(--ion-color-main);\n}\n\n.main_content_div .content_div .card_image {\n  height: 200px;\n  width: 100%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.main_content_div .content_div .like_div {\n  padding-top: 10px;\n  padding-bottom: 5px;\n  display: flex;\n  justify-content: space-evenly;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .content_div .like_div img {\n  width: 20px;\n  margin-right: 5px;\n}\n\n.main_content_div .content_div .like_div ion-button {\n  color: gray;\n  font-size: 16px;\n}\n\n.main_content_div .content_div .like_div2 {\n  padding-top: 10px;\n  padding-bottom: 5px;\n  display: flex;\n  justify-content: space-between;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .content_div .like_div2 img {\n  width: 20px;\n  margin-right: 5px;\n}\n\n.main_content_div .content_div .like_div2 ion-button {\n  color: gray;\n  font-size: 16px;\n}\n\n.main_content_div .content_div .reaction_div {\n  margin-top: 15px;\n}\n\n.main_content_div .content_div .reaction_div .users_div {\n  margin-top: 5px;\n  display: flex;\n}\n\n.main_content_div .content_div .reaction_div .users_div .user_img {\n  height: 40px;\n  width: 40px;\n  min-width: 40px;\n  border-radius: 50%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-right: 10px;\n}\n\n.main_content_div .content_div .reaction_div .users_div .round {\n  height: 40px;\n  width: 40px;\n  min-width: 40px;\n  border-radius: 50%;\n  border: 2px solid #505050;\n  position: relative;\n}\n\n.main_content_div .content_div .reaction_div .users_div .round .dots {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  color: #505050;\n  font-size: 20px;\n}\n\n.main_content_div .content_div .comments_div {\n  margin-top: 15px;\n}\n\n.main_content_div .content_div .comments_div .lbls {\n  display: flex;\n  justify-content: space-between;\n}\n\n.main_content_div .content_div .comments_div .lbls .relevent {\n  font-weight: 600;\n  color: #505050;\n}\n\n.main_content_div .content_div .comments_div .flex_div {\n  display: flex;\n  margin-top: 15px;\n}\n\n.main_content_div .content_div .comments_div .flex_div .user_img {\n  height: 40px;\n  width: 40px;\n  min-width: 40px;\n  border-radius: 50%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-right: 10px;\n}\n\n.main_content_div .content_div .comments_div .flex_div .blue_div {\n  padding: 10px;\n  background: #E1E9EE;\n  border-radius: 10px;\n  border-top-left-radius: 0px;\n  position: relative;\n  width: 100%;\n}\n\n.main_content_div .content_div .comments_div .flex_div .blue_div .abs_icn {\n  right: 10px;\n  top: 10px;\n  position: absolute;\n  color: #505050;\n  font-size: 17px;\n}\n\n.main_content_div .content_div .comments_div .flex_div .blue_div .abs_icn2 {\n  right: 30px;\n  top: 10px;\n  position: absolute;\n  color: #505050;\n  font-size: 17px;\n}\n\n.main_content_div .content_div .comments_div .flex_div .blue_div .head_lbl {\n  font-weight: 600;\n}\n\n.main_content_div .content_div .comments_div .flex_div .blue_div .small_lbl {\n  font-size: 14px;\n  color: gray;\n}\n\n.main_content_div .content_div .comments_div .flex_div .blue_div .text {\n  margin-top: 7px;\n}\n\n.replytxt {\n  font-size: 12px;\n  color: #0a46a0;\n  border-bottom: 3px solid #fffefe;\n}\n\n.footer_div {\n  padding-left: 16px;\n  padding-right: 16px;\n}\n\n.footer_div .reply {\n  font-size: 13px;\n  font-weight: bold;\n}\n\n.footer_div .flex_div {\n  display: flex;\n  margin-top: 15px;\n  margin-bottom: 10px;\n}\n\n.footer_div .flex_div .user_img {\n  height: 40px;\n  width: 40px;\n  min-width: 40px;\n  border-radius: 50%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-right: 10px;\n}\n\n.footer_div .flex_div .blue_div {\n  padding: 10px;\n  background: #E1E9EE;\n  border-radius: 10px;\n  border-top-left-radius: 0px;\n  position: relative;\n  width: 100%;\n}\n\n.footer_div .flex_foot {\n  display: flex;\n  justify-content: space-between;\n}\n\n.footer_div .first_div {\n  display: flex;\n  align-items: center;\n}\n\n.footer_div .first_div .user_img {\n  height: 30px;\n  width: 30px;\n  min-width: 30px;\n  border-radius: 50%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-right: 10px;\n}\n\n.footer_div .second_div {\n  display: flex;\n  align-items: center;\n}\n\n.footer_div .second_div .at {\n  font-size: 20px;\n  color: #505050;\n  font-weight: 600;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2luZ2xlLWZlZWQvRDpcXFJhZ3V2YXJhbiBpbWFnZVxcZGlzZWxzaGlwL3NyY1xcYXBwXFxwYWdlc1xcc2luZ2xlLWZlZWRcXHNpbmdsZS1mZWVkLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvc2luZ2xlLWZlZWQvc2luZ2xlLWZlZWQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUNBQUE7QUNDSjs7QURDQTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUNFSjs7QURBQTtFQUNJLGlCQUFBO0FDR0o7O0FEREE7RUFDSSxZQUFBO0FDSUo7O0FERkE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtBQ0tKOztBREhJO0VBQ0ksY0FBQTtBQ0tSOztBREhJO0VBQ0kseUJBQUE7QUNLUjs7QURKSTtFQUNRLFdBQUE7QUNNWjs7QURKSTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO1VBQUEscUJBQUE7QUNNUjs7QURKSTtFQUVJLGlCQUFBO0VBQ0Esa0JBQUE7QUNLUjs7QURIUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSw4QkFBQTtBQ0taOztBREpZO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNNaEI7O0FESFk7RUFDSSxpQkFBQTtBQ0toQjs7QURKZ0I7RUFDSSxnQkFBQTtBQ01wQjs7QURKZ0I7RUFDSSxlQUFBO0FDTXBCOztBREZZO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsU0FBQTtBQ0loQjs7QURDSTtFQUNJLGdCQUFBO0FDQ1I7O0FEQVE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QUNFWjs7QURDUTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSw0QkFBQTtBQ0NaOztBREVRO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUNBWjs7QURHUTtFQUNJLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxrQ0FBQTtBQ0RaOztBREVZO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0FDQWhCOztBREdZO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUNEaEI7O0FESVE7RUFDSSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0VBQ0Esa0NBQUE7QUNGWjs7QURHWTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtBQ0RoQjs7QURJWTtFQUNJLFdBQUE7RUFDQSxlQUFBO0FDRmhCOztBRE1RO0VBQ0ksZ0JBQUE7QUNKWjs7QURLWTtFQUNJLGVBQUE7RUFDQSxhQUFBO0FDSGhCOztBRElnQjtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7QUNGcEI7O0FESWdCO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0FDRnBCOztBRElvQjtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDRnhCOztBRFFRO0VBQ0ksZ0JBQUE7QUNOWjs7QURPWTtFQUNJLGFBQUE7RUFDQSw4QkFBQTtBQ0xoQjs7QURPZ0I7RUFDSSxnQkFBQTtFQUNBLGNBQUE7QUNMcEI7O0FEU1k7RUFDSSxhQUFBO0VBQ0EsZ0JBQUE7QUNQaEI7O0FEUWdCO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtBQ05wQjs7QURTZ0I7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDUHBCOztBRFFvQjtFQUNJLFdBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQ054Qjs7QURRb0I7RUFDSSxXQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNOeEI7O0FEU29CO0VBQ0ksZ0JBQUE7QUNQeEI7O0FEU29CO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUNQeEI7O0FEU29CO0VBQ0ksZUFBQTtBQ1B4Qjs7QURjQTtFQUNJLGVBQUE7RUFFQSxjQUFBO0VBQ0EsZ0NBQUE7QUNaSjs7QURjQTtFQUVJLGtCQUFBO0VBQ0EsbUJBQUE7QUNaSjs7QURlQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtBQ2JKOztBRGNJO0VBQ0ksYUFBQTtFQUNKLGdCQUFBO0VBQ0EsbUJBQUE7QUNaSjs7QURhUTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7QUNYWjs7QURjUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUNaWjs7QURhSTtFQUNJLGFBQUE7RUFDQSw4QkFBQTtBQ1hSOztBRGFJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FDWFI7O0FEWVE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0FDVlo7O0FEYUk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUNYUjs7QURhUTtFQUNJLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUNYWiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NpbmdsZS1mZWVkL3NpbmdsZS1mZWVkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZCA6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cbmlvbi10b29sYmFyLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgsIGlvbi10b29sYmFyIC5zYy1pb24tc2VhcmNoYmFyLWlvcy1oe1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcbn1cbmlvbi1idXR0b257XG4gICAgLS1ib3gtc2hhZG93Om5vbmU7XG59XG5pb24tdGl0bGV7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuLm1haW5fY29udGVudF9kaXZ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMTZweDtcblxuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIH1cbiAgICAubGlrZWR7XG4gICAgICAgIGNvbG9yOiAjMDQ3ZWFkICFpbXBvcnRhbnQ7fVxuICAgIC51bmxpa2Vke1xuICAgICAgICAgICAgY29sb3I6IGdyYXk7fVxuXG4gICAgLmhlYWRfbGJse1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBtYXJnaW4tYmxvY2stZW5kOiA1cHg7XG4gICAgfVxuICAgIC5kZXNjX2RpdntcbiAgICAgICAgLy8gcGFkZGluZzogMTZweDtcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICAgICAgICAuZmxleF9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICAgICAgLmltYWdle1xuICAgICAgICAgICAgICAgIGhlaWdodDogNDVweDtcbiAgICAgICAgICAgICAgICB3aWR0aDogNDVweDtcbiAgICAgICAgICAgICAgICBtaW4td2lkdGg6IDQ1cHg7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAuY29udGVudF9kaXZ7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgLmhlYWRfbGJse1xuICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAuc21hbGxfbGJse1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAuZmFsbG93X2J0bntcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuY29udGVudF9kaXZ7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIC5kZXNjX2xibHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHhcbiAgICAgICAgfVxuXG4gICAgICAgIHNwYW57XG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC5jYXJkX2ltYWdle1xuICAgICAgICAgICAgaGVpZ2h0OiAyMDBweDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgIH1cblxuICAgICAgICAubGlrZV9kaXZ7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICAgICAgaW1ne1xuICAgICAgICAgICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogNXB4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmxpa2VfZGl2MntcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICAgICAgaW1ne1xuICAgICAgICAgICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogNXB4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAucmVhY3Rpb25fZGl2e1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTVweDtcbiAgICAgICAgICAgIC51c2Vyc19kaXZ7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAgLnVzZXJfaW1ne1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgICAgICAgICBtaW4td2lkdGg6IDQwcHg7XG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5yb3VuZHtcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogNDBweDtcbiAgICAgICAgICAgICAgICAgICAgbWluLXdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgICAgIGJvcmRlcjogMnB4IHNvbGlkICM1MDUwNTA7XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICAgICAgICAgICAgICAgICAgICAuZG90c3tcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogNTAlO1xuICAgICAgICAgICAgICAgICAgICAgICAgbGVmdDogNTAlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwtNTAlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjNTA1MDUwO1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLmNvbW1lbnRzX2RpdntcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDE1cHg7XG4gICAgICAgICAgICAubGJsc3tcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcblxuICAgICAgICAgICAgICAgIC5yZWxldmVudHtcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM1MDUwNTA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAuZmxleF9kaXZ7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxNXB4O1xuICAgICAgICAgICAgICAgIC51c2VyX2ltZ3tcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogNDBweDtcbiAgICAgICAgICAgICAgICAgICAgbWluLXdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC5ibHVlX2RpdntcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMTBweDtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI0UxRTlFRTtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMHB4O1xuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgICAgICAuYWJzX2ljbntcbiAgICAgICAgICAgICAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgdG9wOiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM1MDUwNTA7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE3cHg7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLmFic19pY24ye1xuICAgICAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDMwcHg7XG4gICAgICAgICAgICAgICAgICAgICAgICB0b3A6IDEwcHg7XG4gICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzUwNTA1MDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIC5oZWFkX2xibHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLnNtYWxsX2xibHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC50ZXh0e1xuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogN3B4O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufVxuLnJlcGx5dHh0e1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgIFxuICAgIGNvbG9yOiAjMGE0NmEwO1xuICAgIGJvcmRlci1ib3R0b206IDNweCBzb2xpZCAjZmZmZWZlO1xufVxuLmZvb3Rlcl9kaXZ7XG4gICBcbiAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTZweDtcblxuICAgIFxuLnJlcGx5IHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7fVxuICAgIC5mbGV4X2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIC51c2VyX2ltZ3tcbiAgICAgICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgbWluLXdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgICAgIH1cblxuICAgICAgICAuYmx1ZV9kaXZ7XG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICAgICAgYmFja2dyb3VuZDogI0UxRTlFRTtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgICAgICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwcHg7XG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTt9fVxuICAgIC5mbGV4X2Zvb3R7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICB9XG4gICAgLmZpcnN0X2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgLnVzZXJfaW1ne1xuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgICAgICBtaW4td2lkdGg6IDMwcHg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICAgICAgfVxuICAgIH1cbiAgICAuc2Vjb25kX2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcblxuICAgICAgICAuYXR7XG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICBjb2xvcjogIzUwNTA1MDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG5pb24tYnV0dG9uIHtcbiAgLS1ib3gtc2hhZG93Om5vbmU7XG59XG5cbmlvbi10aXRsZSB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IGlvbi1sYWJlbCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuLm1haW5fY29udGVudF9kaXYgLmxpa2VkIHtcbiAgY29sb3I6ICMwNDdlYWQgIWltcG9ydGFudDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51bmxpa2VkIHtcbiAgY29sb3I6IGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGVhZF9sYmwge1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tYmxvY2stZW5kOiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IC5pbWFnZSB7XG4gIGhlaWdodDogNDVweDtcbiAgd2lkdGg6IDQ1cHg7XG4gIG1pbi13aWR0aDogNDVweDtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiAuY29udGVudF9kaXYge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IC5oZWFkX2xibCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiAuc21hbGxfbGJsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiAuZmFsbG93X2J0biB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbWFyZ2luOiAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAuZGVzY19sYmwge1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgc3BhbiB7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAuY2FyZF9pbWFnZSB7XG4gIGhlaWdodDogMjAwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmxpa2VfZGl2IHtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtZXZlbmx5O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5saWtlX2RpdiBpbWcge1xuICB3aWR0aDogMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmxpa2VfZGl2IGlvbi1idXR0b24ge1xuICBjb2xvcjogZ3JheTtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5saWtlX2RpdjIge1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5saWtlX2RpdjIgaW1nIHtcbiAgd2lkdGg6IDIwcHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5saWtlX2RpdjIgaW9uLWJ1dHRvbiB7XG4gIGNvbG9yOiBncmF5O1xuICBmb250LXNpemU6IDE2cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLnJlYWN0aW9uX2RpdiB7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLnJlYWN0aW9uX2RpdiAudXNlcnNfZGl2IHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5yZWFjdGlvbl9kaXYgLnVzZXJzX2RpdiAudXNlcl9pbWcge1xuICBoZWlnaHQ6IDQwcHg7XG4gIHdpZHRoOiA0MHB4O1xuICBtaW4td2lkdGg6IDQwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLnJlYWN0aW9uX2RpdiAudXNlcnNfZGl2IC5yb3VuZCB7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDQwcHg7XG4gIG1pbi13aWR0aDogNDBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBib3JkZXI6IDJweCBzb2xpZCAjNTA1MDUwO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLnJlYWN0aW9uX2RpdiAudXNlcnNfZGl2IC5yb3VuZCAuZG90cyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1MCU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIGNvbG9yOiAjNTA1MDUwO1xuICBmb250LXNpemU6IDIwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmNvbW1lbnRzX2RpdiB7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmNvbW1lbnRzX2RpdiAubGJscyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAuY29tbWVudHNfZGl2IC5sYmxzIC5yZWxldmVudCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjNTA1MDUwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5jb21tZW50c19kaXYgLmZsZXhfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAuY29tbWVudHNfZGl2IC5mbGV4X2RpdiAudXNlcl9pbWcge1xuICBoZWlnaHQ6IDQwcHg7XG4gIHdpZHRoOiA0MHB4O1xuICBtaW4td2lkdGg6IDQwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmNvbW1lbnRzX2RpdiAuZmxleF9kaXYgLmJsdWVfZGl2IHtcbiAgcGFkZGluZzogMTBweDtcbiAgYmFja2dyb3VuZDogI0UxRTlFRTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5jb21tZW50c19kaXYgLmZsZXhfZGl2IC5ibHVlX2RpdiAuYWJzX2ljbiB7XG4gIHJpZ2h0OiAxMHB4O1xuICB0b3A6IDEwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgY29sb3I6ICM1MDUwNTA7XG4gIGZvbnQtc2l6ZTogMTdweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAuY29tbWVudHNfZGl2IC5mbGV4X2RpdiAuYmx1ZV9kaXYgLmFic19pY24yIHtcbiAgcmlnaHQ6IDMwcHg7XG4gIHRvcDogMTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBjb2xvcjogIzUwNTA1MDtcbiAgZm9udC1zaXplOiAxN3B4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5jb21tZW50c19kaXYgLmZsZXhfZGl2IC5ibHVlX2RpdiAuaGVhZF9sYmwge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5jb21tZW50c19kaXYgLmZsZXhfZGl2IC5ibHVlX2RpdiAuc21hbGxfbGJsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogZ3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAuY29tbWVudHNfZGl2IC5mbGV4X2RpdiAuYmx1ZV9kaXYgLnRleHQge1xuICBtYXJnaW4tdG9wOiA3cHg7XG59XG5cbi5yZXBseXR4dCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICMwYTQ2YTA7XG4gIGJvcmRlci1ib3R0b206IDNweCBzb2xpZCAjZmZmZWZlO1xufVxuXG4uZm9vdGVyX2RpdiB7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbn1cbi5mb290ZXJfZGl2IC5yZXBseSB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uZm9vdGVyX2RpdiAuZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBtYXJnaW4tdG9wOiAxNXB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuLmZvb3Rlcl9kaXYgLmZsZXhfZGl2IC51c2VyX2ltZyB7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDQwcHg7XG4gIG1pbi13aWR0aDogNDBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbi5mb290ZXJfZGl2IC5mbGV4X2RpdiAuYmx1ZV9kaXYge1xuICBwYWRkaW5nOiAxMHB4O1xuICBiYWNrZ3JvdW5kOiAjRTFFOUVFO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgd2lkdGg6IDEwMCU7XG59XG4uZm9vdGVyX2RpdiAuZmxleF9mb290IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmZvb3Rlcl9kaXYgLmZpcnN0X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uZm9vdGVyX2RpdiAuZmlyc3RfZGl2IC51c2VyX2ltZyB7XG4gIGhlaWdodDogMzBweDtcbiAgd2lkdGg6IDMwcHg7XG4gIG1pbi13aWR0aDogMzBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbi5mb290ZXJfZGl2IC5zZWNvbmRfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5mb290ZXJfZGl2IC5zZWNvbmRfZGl2IC5hdCB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgY29sb3I6ICM1MDUwNTA7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/pages/single-feed/single-feed.page.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/single-feed/single-feed.page.ts ***!
    \*******************************************************/

  /*! exports provided: SingleFeedPage */

  /***/
  function srcAppPagesSingleFeedSingleFeedPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SingleFeedPage", function () {
      return SingleFeedPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/services/dummy-data.service */
    "./src/app/services/dummy-data.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/native-page-transitions/ngx */
    "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
    /* harmony import */


    var _components_replycomment_replycomment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../components/replycomment/replycomment */
    "./src/app/components/replycomment/replycomment.ts");
    /* harmony import */


    var jquery__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! jquery */
    "./node_modules/jquery/dist/jquery.js");
    /* harmony import */


    var jquery__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_11__);
    /* harmony import */


    var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @ionic-native/camera/ngx */
    "./node_modules/@ionic-native/camera/ngx/index.js");
    /* harmony import */


    var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @ionic-native/image-picker/ngx */
    "./node_modules/@ionic-native/image-picker/ngx/index.js");
    /* harmony import */


    var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @ionic-native/android-permissions/ngx */
    "./node_modules/@ionic-native/android-permissions/ngx/index.js");
    /* harmony import */


    var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @ionic-native/file/ngx */
    "./node_modules/@ionic-native/file/ngx/index.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var SingleFeedPage = /*#__PURE__*/function () {
      function SingleFeedPage(file, camera, imagePicker, plt, androidPermissions, loadingController, resolver, navCtrl, nativePageTransitions, dummy, storage, fb, api, alertCtrl, location, toastCtrl, router, route, actionSheet) {
        var _this = this;

        _classCallCheck(this, SingleFeedPage);

        this.file = file;
        this.camera = camera;
        this.imagePicker = imagePicker;
        this.plt = plt;
        this.androidPermissions = androidPermissions;
        this.loadingController = loadingController;
        this.resolver = resolver;
        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.dummy = dummy;
        this.storage = storage;
        this.fb = fb;
        this.api = api;
        this.alertCtrl = alertCtrl;
        this.location = location;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.route = route;
        this.actionSheet = actionSheet;
        this.commentid = '';
        this.options = {
          direction: 'up',
          duration: 500,
          slowdownfactor: 3,
          slidePixels: 20,
          iosdelay: 100,
          androiddelay: 150,
          fixedPixelsTop: 0,
          fixedPixelsBottom: 60
        };
        this.file_ids = [];
        this.file_imgs = [];
        this.privacy = "public";
        this.users = this.dummy.users;
        this.storage.get('USER_INFO').then(function (result) {
          console.log("USER_INFO", result);
          _this.mydata = result.avatar_urls[24];
          console.log(result.avatar_urls[24]);
          if (typeof result.avatar_urls[24] === "undefined") _this.mydata = result.avatar_urls['full'];
          _this.user_id = result.id;
        })["catch"](function (e) {
          console.log('error: ' + e); // Handle errors here
        });
      }

      _createClass(SingleFeedPage, [{
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          document.getElementById('autocomplete');
          this.nativePageTransitions.slide(this.options);
        } // example of adding a transition when pushing a new page

      }, {
        key: "openPage",
        value: function openPage(page) {
          this.nativePageTransitions.slide(this.options);
          this.navCtrl.navigateForward(page);
        }
      }, {
        key: "loaddata",
        value: function loaddata(data1) {
          var factory = this.resolver.resolveComponentFactory(_components_replycomment_replycomment__WEBPACK_IMPORTED_MODULE_10__["ReplyComponent"]);
          var componentRef = this.container.createComponent(factory);
          componentRef.instance.data = data1;
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this2 = this;

          this.commentForm = this.fb.group({
            comment: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]
          });
          this.loading = false;
          this.route.queryParams.subscribe(function (params) {
            if (params && params.special) {
              _this2.post = JSON.parse(params.special);
            }

            console.log("data pass", _this2.post);

            _this2.api.activitygetcomment(_this2.post.id).subscribe(function (res) {
              _this2.data = res;
              console.log("data", res);
            });

            console.log("data", _this2.data);
          });
          this.api.getuser(this.post.user_id).subscribe(function (res) {
            _this2.userdata = res;
            if (_this2.userdata.is_following) _this2.unfollow = true;else _this2.follow = true;
            console.log("data", _this2.userdata.is_following);
          });
        }
      }, {
        key: "BackButton",
        value: function BackButton() {
          this.location.back();
        }
      }, {
        key: "selectImage",
        value: function selectImage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var _this3 = this;

            var options, imageResponse;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                      return console.log('Has permission?', result.hasPermission);
                    }, function (err) {
                      return _this3.androidPermissions.requestPermission(_this3.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                    });
                    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]); // this.loading=true;

                    options = {
                      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
                      // selection of a single image, the plugin will return it.
                      //maximumImagesCount: 3,
                      // max width and height to allow the images to be.  Will keep aspect
                      // ratio no matter what.  So if both are 800, the returned image
                      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
                      // 800 and height 0 the image will be 800 pixels wide if the source
                      // is at least that wide.
                      width: 200,
                      //height: 200,
                      // quality of resized image, defaults to 100
                      quality: 25,
                      // output type, defaults to FILE_URIs.
                      // available options are 
                      // window.imagePicker.OutputType.FILE_URI (0) or 
                      // window.imagePicker.OutputType.BASE64_STRING (1)
                      outputType: 1
                    };
                    imageResponse = [];
                    this.imagePicker.getPictures(options).then(function (results) {
                      for (var i = 0; i < results.length; i++) {
                        _this3.file.resolveLocalFilesystemUrl(results[i]).then(function (entry) {
                          entry.file(function (file) {
                            console.log(file);

                            _this3.readFile(file);
                          });
                        });
                      }
                    }, function (err) {
                      alert(err);
                    });

                  case 5:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "addImage",
        value: function addImage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this4 = this;

            var options;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    options = {
                      quality: 100,
                      destinationType: this.camera.DestinationType.FILE_URI,
                      encodingType: this.camera.EncodingType.JPEG,
                      mediaType: this.camera.MediaType.PICTURE
                    };
                    this.camera.getPicture(options).then(function (imageData) {
                      _this4.file.resolveLocalFilesystemUrl(imageData).then(function (entry) {
                        entry.file(function (file) {
                          console.log(file);

                          _this4.readFile(file);
                        });
                      });
                    }, function (err) {// Handle error
                    });

                  case 2:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "selectcoverImageSource",
        value: function selectcoverImageSource() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this5 = this;

            var buttons, actionSheet;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    buttons = [{
                      text: 'Take Photo',
                      icon: 'camera',
                      handler: function handler() {
                        _this5.androidPermissions.checkPermission(_this5.androidPermissions.PERMISSION.CAMERA).then(function (result) {
                          return console.log('Has permission?', result.hasPermission);
                        }, function (err) {
                          return _this5.androidPermissions.requestPermission(_this5.androidPermissions.PERMISSION.CAMERA);
                        });

                        _this5.androidPermissions.requestPermissions([_this5.androidPermissions.PERMISSION.CAMERA, _this5.androidPermissions.PERMISSION.GET_ACCOUNTS]);

                        _this5.addImage();
                      }
                    }, {
                      text: 'Choose From Photos Photo',
                      icon: 'image',
                      handler: function handler() {
                        _this5.androidPermissions.checkPermission(_this5.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                          return console.log('Has permission?', result.hasPermission);
                        }, function (err) {
                          return _this5.androidPermissions.requestPermission(_this5.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                        });

                        _this5.androidPermissions.requestPermissions([_this5.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, _this5.androidPermissions.PERMISSION.GET_ACCOUNTS]);

                        _this5.selectImage();
                      }
                    }]; // Only allow file selection inside a browser

                    if (!this.plt.is('hybrid')) {
                      buttons.push({
                        text: 'Choose a File',
                        icon: 'attach',
                        handler: function handler() {
                          _this5.androidPermissions.checkPermission(_this5.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                            return console.log('Has permission?', result.hasPermission);
                          }, function (err) {
                            return _this5.androidPermissions.requestPermission(_this5.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                          });

                          _this5.androidPermissions.requestPermissions([_this5.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, _this5.androidPermissions.PERMISSION.GET_ACCOUNTS]);

                          _this5.fileInput.nativeElement.click();
                        }
                      });
                    }

                    _context3.next = 4;
                    return this.actionSheet.create({
                      header: 'Select Image Source',
                      buttons: buttons
                    });

                  case 4:
                    actionSheet = _context3.sent;
                    _context3.next = 7;
                    return actionSheet.present();

                  case 7:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "dltmedia",
        value: function dltmedia(id) {
          var index = this.file_imgs.findIndex(function (data) {
            return data.id === id;
          });
          this.file_imgs.splice(index, 1);
          var index2 = this.file_ids.findIndex(function (data) {
            return data === id;
          });
          this.file_ids.splice(index2, 1); // console.log(this.file_imgs);
          // console.log(index);
          //     this.api.deltmedia(id,this.token).subscribe(
          //       async (res : any) => {
          //       console.log("---------------");
          //       console.log(res);
          //       const toast = await this.toastCtrl.create({
          //         message: 'Image Sucessfully Removed',
          //         duration: 3000
          //       });
          //       toast.present();
          // // Get the entire data
          //     },
          //     err => {
          //       console.log(err);
          //       this.showError(err);
          //     }
          //     );
        }
      }, {
        key: "attach",
        value: function attach() {
          this.fileInput.nativeElement.click();
        }
      }, {
        key: "uploadFile",
        value: function uploadFile(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var _this6 = this;

            var loading, eventObj, target, file1;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    console.log(this.file_ids);
                    _context5.next = 3;
                    return this.loadingController.create({
                      message: 'Please wait...',
                      duration: 2000
                    });

                  case 3:
                    loading = _context5.sent;
                    _context5.next = 6;
                    return loading.present();

                  case 6:
                    eventObj = event;
                    target = eventObj.target;
                    file1 = target.files[0];
                    this.api.uploadMedia(file1).subscribe(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this6, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                        var toast;
                        return regeneratorRuntime.wrap(function _callee4$(_context4) {
                          while (1) {
                            switch (_context4.prev = _context4.next) {
                              case 0:
                                this.file_ids.push(res.upload_id);
                                this.file_imgs.push({
                                  img: res.upload_thumb,
                                  id: res.upload_id
                                });
                                console.log("---------------");
                                console.log(this.file_ids);
                                _context4.next = 6;
                                return this.toastCtrl.create({
                                  message: 'Image Uploaded',
                                  duration: 3000
                                });

                              case 6:
                                toast = _context4.sent;
                                toast.present(); // Get the entire data

                              case 8:
                              case "end":
                                return _context4.stop();
                            }
                          }
                        }, _callee4, this);
                      }));
                    }, function (err) {
                      console.log(err);

                      _this6.showError(err);
                    });

                  case 10:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "readFile",
        value: function readFile(file) {
          var _this7 = this;

          console.log(file.type);
          var reader = new FileReader();

          reader.onload = function () {
            _this7.blob = new Blob([reader.result], {
              type: file.type
            });
            _this7.file_name = file.name;
            _this7.file_type = file.type;
          };

          reader.readAsArrayBuffer(file);
        }
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {// this.selectcoverImageSource();
        }
      }, {
        key: "unfollowuser",
        value: function unfollowuser() {
          var _this8 = this;

          console.log(jquery__WEBPACK_IMPORTED_MODULE_11__("#id75").text());
          this.api.followuser("unfollow", this.userdata.id).subscribe(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this8, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var toast;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      console.log(res);
                      this.follow = true;
                      this.unfollow = false;
                      _context6.next = 5;
                      return this.toastCtrl.create({
                        message: "Unfollow User Successfully",
                        duration: 3000
                      });

                    case 5:
                      toast = _context6.sent;
                      _context6.next = 8;
                      return toast.present();

                    case 8:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          });
        }
      }, {
        key: "hightlight",
        value: function hightlight(id) {
          console.log(id);
          jquery__WEBPACK_IMPORTED_MODULE_11__('#id' + id).delay(100).fadeOut().fadeIn('slow');
        }
      }, {
        key: "followuser",
        value: function followuser() {
          var _this9 = this;

          this.api.followuser("follow", this.userdata.id).subscribe(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this9, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var toast;
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      console.log(res);
                      this.unfollow = true;
                      this.follow = false;
                      _context7.next = 5;
                      return this.toastCtrl.create({
                        message: "Follow User Successfully",
                        duration: 3000
                      });

                    case 5:
                      toast = _context7.sent;
                      _context7.next = 8;
                      return toast.present();

                    case 8:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          });
        }
      }, {
        key: "likefeed",
        value: function likefeed(idd) {
          var _this10 = this;

          this.api.activitylike(idd).subscribe(function (res) {
            console.log(res);
            _this10.post.favorited = res.favorited;
            _this10.post.favorite_count = res.favorite_count;
          });
        }
      }, {
        key: "deletepost",
        value: function deletepost(idd) {
          var _this11 = this;

          this.api.activitydelete(idd).subscribe(function (res) {
            console.log(res);
            res.media_url = _this11.post.media_url;
            _this11.post = res;

            _this11.router.navigate(['group-feed']);
          });
        }
      }, {
        key: "commentfocus",
        value: function commentfocus() {
          this.searchInput.setFocus();
        }
      }, {
        key: "commentreply",
        value: function commentreply(id, comment, commentavatar) {
          this.commenttext = comment;
          this.commentid = id;
          this.commentavatar = commentavatar;
          console.log(id, comment);
          this.searchInput.setFocus();
        }
      }, {
        key: "postcomment",
        value: function postcomment() {
          var _this12 = this;

          this.loading = true;

          if (this.commentid != '') {
            this.api.postcomment(this.comment, this.post.id, this.commentid, this.file_ids).subscribe(function (res) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this12, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
                var toast;
                return regeneratorRuntime.wrap(function _callee8$(_context8) {
                  while (1) {
                    switch (_context8.prev = _context8.next) {
                      case 0:
                        console.log(res);
                        this.data = res;
                        _context8.next = 4;
                        return this.toastCtrl.create({
                          message: "Comment Posted Successfully",
                          duration: 3000
                        });

                      case 4:
                        toast = _context8.sent;
                        _context8.next = 7;
                        return toast.present();

                      case 7:
                        this.comment = "";
                        this.loading = false;
                        console.log("data", this.data); //  window.location.reload(); 
                        //  location.href = "http://localhost:8100/";
                        // this.router.navigate(['/']);

                      case 10:
                      case "end":
                        return _context8.stop();
                    }
                  }
                }, _callee8, this);
              }));
            }, function (err) {
              _this12.loading = false;
              console.log(err);

              _this12.showError(err);
            });
          } else {
            this.api.postcomment(this.comment, this.post.id, 0, this.file_ids).subscribe(function (res) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this12, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
                var toast;
                return regeneratorRuntime.wrap(function _callee9$(_context9) {
                  while (1) {
                    switch (_context9.prev = _context9.next) {
                      case 0:
                        console.log(res);
                        this.data = res;
                        _context9.next = 4;
                        return this.toastCtrl.create({
                          message: "Comment Posted Successfully",
                          duration: 3000
                        });

                      case 4:
                        toast = _context9.sent;
                        _context9.next = 7;
                        return toast.present();

                      case 7:
                        this.comment = "";
                        this.loading = false; //  window.location.reload(); 
                        //  location.href = "http://localhost:8100/";

                        console.log("data", this.data); // this.router.navigate(['/']);

                      case 10:
                      case "end":
                        return _context9.stop();
                    }
                  }
                }, _callee9, this);
              }));
            }, function (err) {
              _this12.loading = false;
              console.log(err);

              _this12.showError(err);
            });
          }
        }
      }, {
        key: "showError",
        value: function showError(err) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
            var alert;
            return regeneratorRuntime.wrap(function _callee10$(_context10) {
              while (1) {
                switch (_context10.prev = _context10.next) {
                  case 0:
                    _context10.next = 2;
                    return this.alertCtrl.create({
                      header: "Post Comment Error",
                      message: err.error.message,
                      buttons: ['OK']
                    });

                  case 2:
                    alert = _context10.sent;
                    _context10.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context10.stop();
                }
              }
            }, _callee10, this);
          }));
        }
      }, {
        key: "openActionSheet",
        value: function openActionSheet() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
            var actionSheet;
            return regeneratorRuntime.wrap(function _callee11$(_context11) {
              while (1) {
                switch (_context11.prev = _context11.next) {
                  case 0:
                    _context11.next = 2;
                    return this.actionSheet.create({
                      mode: 'md',
                      buttons: [{
                        text: 'Save',
                        role: 'destructive',
                        icon: 'bookmark-outline',
                        handler: function handler() {
                          console.log('Delete clicked');
                        }
                      }, {
                        text: 'Send in a private Message',
                        icon: 'chatbox-ellipses-outline',
                        handler: function handler() {
                          console.log('Share clicked');
                        }
                      }, {
                        text: 'Share via',
                        icon: 'share-social',
                        handler: function handler() {
                          console.log('Play clicked');
                        }
                      }, {
                        text: 'Hide this post',
                        icon: 'close-circle-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Unfollow',
                        icon: 'person-remove-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Report this post',
                        icon: 'flag-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Improve my feed',
                        icon: 'options-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Who can see this post?',
                        icon: 'eye-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }]
                    });

                  case 2:
                    actionSheet = _context11.sent;
                    _context11.next = 5;
                    return actionSheet.present();

                  case 5:
                  case "end":
                    return _context11.stop();
                }
              }
            }, _callee11, this);
          }));
        }
      }]);

      return SingleFeedPage;
    }();

    SingleFeedPage.ctorParameters = function () {
      return [{
        type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_15__["File"]
      }, {
        type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_12__["Camera"]
      }, {
        type: _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_13__["ImagePicker"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"]
      }, {
        type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_14__["AndroidPermissions"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
      }, {
        type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_9__["NativePageTransitions"]
      }, {
        type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_3__["dummyDataService"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"]
      }, {
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_5__["apiService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
      }, {
        type: _angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('commentfoc', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], SingleFeedPage.prototype, "searchInput", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('processContainer', {
      read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"],
      "static": true
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], SingleFeedPage.prototype, "container", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], SingleFeedPage.prototype, "fileInput", void 0);
    SingleFeedPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-single-feed',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./single-feed.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/single-feed/single-feed.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./single-feed.page.scss */
      "./src/app/pages/single-feed/single-feed.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_15__["File"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_12__["Camera"], _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_13__["ImagePicker"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_14__["AndroidPermissions"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_9__["NativePageTransitions"], src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_3__["dummyDataService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"], _services_api_service__WEBPACK_IMPORTED_MODULE_5__["apiService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"]])], SingleFeedPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-single-feed-single-feed-module-es5.js.map
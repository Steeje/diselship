(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-new-home-new-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-new/home-new.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-new/home-new.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n\n      <ion-buttons slot=\"start\">\n        <!-- <ion-menu-button color=\"light\"></ion-menu-button> -->\n        <img src=\"assets/imgs/user.png\" class=\"user_image\" (click)=\"openMenu()\">\n      </ion-buttons>\n      <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar>\n      <ion-icon name=\"chatbubbles\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\" (click)=\"goToChatList()\"></ion-icon>\n    </div>\n    <!-- <ion-title *ngIf=\"!categoryTitle\">Recent posts</ion-title> -->\n    <ion-title *ngIf=\"categoryTitle\">{{categoryTitle}} posts</ion-title>\n    <ion-buttons slot=\"start\" *ngIf=\"categoryTitle\">\n      <ion-back-button defaultHref=\"posts\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n    <div class=\"up_lbl\">\n      <ion-label>Get More out of Diselship Job</ion-label>\n      <ion-buttons slot=\"start\" *ngIf=\"!showSlider\">\n        <ion-button (click)=\"sliderShow(true)\" color=\"dark\">\n          <ion-icon name=\"chevron-up-outline\" mode=\"ios\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n\n      <ion-buttons slot=\"start\" *ngIf=\"showSlider\">\n        <ion-button (click)=\"sliderShow(false)\" color=\"dark\">\n          <ion-icon name=\"chevron-down-outline\" mode=\"ios\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n\n    </div>\n\n    <div class=\"chips_div\" *ngIf=\"!showSlider\">\n      <ion-label class=\"chip\" *ngFor=\"let item of chips\">{{item}}</ion-label>\n    </div>\n\n    <div class=\"slider_div\" *ngIf=\"showSlider\">\n      <ion-slides mode=\"ios\" pager=\"ios\" [options]=\"slideoptions\">\n\n        <ion-slide>\n          <div class=\"slide_div\">\n            <div class=\"up_div\">\n              <div class=\"img_div\">\n                <img src=\"assets/imgs/right.png\" class=\"slider_img\">\n              </div>\n              <div class=\"content_div\">\n                <ion-label class=\"head_lbl\">Add a photo to get recognize</ion-label>\n                <ion-label class=\"small_lbl\">A picture helps you build your network</ion-label>\n              </div>\n            </div>\n\n            <div class=\"lower_div\">\n              <ion-button expand=\"block\" fill=\"clear\" size=\"small\">\n                ADD PHOTO\n              </ion-button>\n            </div>\n\n          </div>\n        </ion-slide>\n\n        <ion-slide>\n          <div class=\"slide_div\">\n            <div class=\"up_div\">\n              <div class=\"img_div\">\n                <img src=\"assets/imgs/user.jpg\" class=\"slider_img\">\n              </div>\n              <div class=\"content_div\">\n                <ion-label class=\"head_lbl\">Jonh Doe</ion-label>\n                <ion-label class=\"small_lbl\">Lorem Ipsum is simply dummy text</ion-label>\n              </div>\n            </div>\n\n            <div class=\"lower_div\">\n              <ion-button expand=\"block\" fill=\"clear\" size=\"small\">\n                IGNORE\n              </ion-button>\n              <ion-button expand=\"block\" fill=\"clear\" size=\"small\">\n                ACCEPT\n              </ion-button>\n            </div>\n\n          </div>\n        </ion-slide>\n\n        <ion-slide>\n          <div class=\"slide_div\" style=\"flex-direction: row;padding: 0px 15px;\">\n            <div class=\"img_div\">\n              <img src=\"assets/imgs/right.png\" class=\"slider_img\">\n            </div>\n            <div class=\"content_div\">\n              <ion-label class=\"head_lbl\">Hashtags followed!</ion-label>\n              <ion-label class=\"small_lbl\">Lorem Ipsum is simply dummy text of the</ion-label>\n            </div>\n          </div>\n        </ion-slide>\n      </ion-slides>\n    </div>\n\n\n\n\n    \n    <div style=\"margin-top: 10px;\">\n      \n        <!-- Card 1 -->\n        <ion-content class=\"page-content\"></ion-content>\n        <div class=\"div_card\" *ngFor=\"let post of posts\" [routerLink]=\"['/post', post.id]\" >\n          <div class=\"head_div\">\n\n            <div class=\"img_div\" [style.backgroundImage]=\"'url(assets/imgs/user.jpg)'\"></div>\n\n            <div class=\"content_div\">\n              <ion-label class=\"head_lbl\">Infosys</ion-label>\n              <ion-label class=\"small_lbl\">2,34,567 followers</ion-label>\n              <ion-label class=\"small_lbl\" *ngIf=\"!post.modified\">{{post.date | date}}</ion-label>\n              <ion-label  class=\"small_lbl\" *ngIf=\"post.modified\">{{post.modified | date}}</ion-label>\n     \n            </div>\n\n            <ion-buttons class=\"card_arrow\" (click)=\"openActionSheet()\">\n              <ion-button color=\"dark\">\n                <ion-icon name=\"chevron-down-outline\" mode=\"ios\"></ion-icon>\n              </ion-button>\n            </ion-buttons>\n          </div>\n          <ion-card-title [innerHTML]=\"post.title.rendered\"></ion-card-title>\n          <div class=\"desc_div\">\n            <ion-label  [innerHTML]=\"post.excerpt.rendered\">\n              \n            </ion-label>\n\n\n           \n            <ion-label class=\"see_lbl\">See more</ion-label>\n          </div>\n\n          <div class=\"card_image\" [style.backgroundImage]=\"'url('+post.media_url+')'\"></div>\n          <div class=\"like_div2\">\n            <div>\n              <img src=\"assets/imgs/like.png\" alt=\"\">\n              <img src=\"assets/imgs/heart.png\" alt=\"\">\n            </div>\n            <div>25 Comments</div>\n          </div>\n\n          <div class=\"like_div\" style=\"padding: 0;border: none;\">\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"thumbs-up-sharp\" style=\"margin-right:5px\"></ion-icon>Like\n              </ion-button>\n            </div>\n\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"chatbox-ellipses-sharp\" style=\"margin-right:5px\"></ion-icon>Comment\n              </ion-button>\n            </div>\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"share-social\" style=\"margin-right:5px\"></ion-icon>Share\n              </ion-button>\n            </div>\n\n          </div>\n        </div>\n        \n\n        <!-- card 2 -->\n <!--        <div class=\"div_card\">\n          <div class=\"head_div\">\n\n            <div class=\"img_div\" [style.backgroundImage]=\"'url(assets/imgs/user2.jpg)'\"></div>\n\n            <div class=\"content_div\">\n              <ion-label class=\"head_lbl\">John Doe</ion-label>\n              <ion-label class=\"small_lbl\">InitApps shop</ion-label>\n              <ion-label class=\"small_lbl\">1 w • Edited</ion-label>\n            </div>\n\n            <ion-buttons class=\"card_arrow\" (click)=\"openActionSheet()\">\n              <ion-button color=\"dark\">\n                <ion-icon name=\"chevron-down-outline\" mode=\"ios\"></ion-icon>\n              </ion-button>\n            </ion-buttons>\n          </div>\n\n          <div class=\"desc_div\">\n            <ion-label>\n              Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n              Lorem Ipsum has been the industry's standard dummy text\n            </ion-label>\n            <ion-label class=\"see_lbl\">See more</ion-label>\n          </div>\n\n          <div class=\"masonry\">\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid1.jpg\">\n            </div>\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid2.jpg\">\n            </div>\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid3.jpg\">\n            </div>\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid4.jpg\">\n            </div>\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid5.jpg\">\n            </div>\n          </div>\n\n          <div class=\"like_div2\">\n            <div>\n              <img src=\"assets/imgs/like.png\" alt=\"\">\n              <img src=\"assets/imgs/heart.png\" alt=\"\">\n            </div>\n            <div>25 Comments</div>\n          </div>\n\n          <div class=\"like_div\" style=\"padding: 0;border: none;\">\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"thumbs-up-sharp\" style=\"margin-right:5px\"></ion-icon>Like\n              </ion-button>\n            </div>\n\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"chatbox-ellipses-sharp\" style=\"margin-right:5px\"></ion-icon>Comment\n              </ion-button>\n            </div>\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"share-social\" style=\"margin-right:5px\"></ion-icon>Share\n              </ion-button>\n            </div>\n\n          </div>\n        </div>\n\n        Card 3 \n\n        <div class=\"div_card\">\n          <div class=\"head_div\">\n\n            <div class=\"img_div\" [style.backgroundImage]=\"'url(assets/imgs/user3.jpg)'\"></div>\n\n            <div class=\"content_div\">\n              <ion-label class=\"head_lbl\">John Doe</ion-label>\n              <ion-label class=\"small_lbl\">InitApps shop</ion-label>\n              <ion-label class=\"small_lbl\">1 w • Edited</ion-label>\n            </div>\n\n            <ion-buttons class=\"card_arrow\" (click)=\"openActionSheet()\">\n              <ion-button color=\"dark\">\n                <ion-icon name=\"chevron-down-outline\" mode=\"ios\"></ion-icon>\n              </ion-button>\n            </ion-buttons>\n          </div>\n\n          <div class=\"desc_div\">\n            <ion-label>\n              Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n              Lorem Ipsum has been the industry's standard dummy text\n            </ion-label>\n            <ion-label class=\"see_lbl\">See more</ion-label>\n          </div>\n\n          <video width=\"100%\" height=\"240\" controls poster=\"assets/imgs/grid/grid4.jpg\">\n            <source src=\"assets/imgs/video.mp4\" type=\"video/mp4\">\n          </video>\n\n          <div class=\"like_div2\">\n            <div>\n              <img src=\"assets/imgs/like.png\" alt=\"\">\n              <img src=\"assets/imgs/heart.png\" alt=\"\">\n            </div>\n            <div>25 Comments</div>\n          </div>\n\n          <div class=\"like_div\" style=\"padding: 0;border: none;\">\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"thumbs-up-sharp\"></ion-icon>Like\n              </ion-button>\n            </div>\n\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"chatbox-ellipses-sharp\"></ion-icon>Comment\n              </ion-button>\n            </div>\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"share-social\"></ion-icon>Share\n              </ion-button>\n            </div>\n\n          </div>\n        </div> -->\n      \n    </div>\n  </div>\n  <ion-infinite-scroll (ionInfinite)=\"loadData($event)\">\n    <ion-infinite-scroll-content\n    loadingSpinner=\"bubbles\"\n    loadingText=\"Loading more posts ...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/home-new/home-new-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/home-new/home-new-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: HomeNewPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeNewPageRoutingModule", function() { return HomeNewPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_new_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-new.page */ "./src/app/pages/home-new/home-new.page.ts");




const routes = [
    {
        path: '',
        component: _home_new_page__WEBPACK_IMPORTED_MODULE_3__["HomeNewPage"]
    }
];
let HomeNewPageRoutingModule = class HomeNewPageRoutingModule {
};
HomeNewPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomeNewPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/home-new/home-new.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/home-new/home-new.module.ts ***!
  \***************************************************/
/*! exports provided: HomeNewPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeNewPageModule", function() { return HomeNewPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _home_new_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-new-routing.module */ "./src/app/pages/home-new/home-new-routing.module.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_new_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home-new.page */ "./src/app/pages/home-new/home-new.page.ts");
/* harmony import */ var _home_new_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./home-new.resolver */ "./src/app/pages/home-new/home-new.resolver.ts");









const routes = [
    {
        path: '',
        component: _home_new_page__WEBPACK_IMPORTED_MODULE_7__["HomeNewPage"],
        resolve: {
            data: _home_new_resolver__WEBPACK_IMPORTED_MODULE_8__["PostsResolver"]
        },
        runGuardsAndResolvers: 'paramsOrQueryParamsChange' // because we use the same route for all posts and for category posts, we need to add this to refetch data 
    }
];
let HomeNewPageModule = class HomeNewPageModule {
};
HomeNewPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_new_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeNewPageRoutingModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(routes)
        ],
        declarations: [_home_new_page__WEBPACK_IMPORTED_MODULE_7__["HomeNewPage"]],
        providers: [_home_new_resolver__WEBPACK_IMPORTED_MODULE_8__["PostsResolver"]]
    })
], HomeNewPageModule);



/***/ }),

/***/ "./src/app/pages/home-new/home-new.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/home-new/home-new.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\n.header_div {\n  display: flex;\n  width: 100%;\n  justify-content: space-between;\n  align-items: center;\n  padding-left: 16px;\n  padding-right: 16px;\n  height: 40px;\n}\n\n.header_div .user_image {\n  height: 25px;\n  width: 25px;\n}\n\n.header_div ion-searchbar {\n  --background: white;\n  --placeholder-color: #707070;\n  --icon-color: #707070;\n  border-radius: 0px;\n  height: 30px;\n}\n\nion-content {\n  --background: #F5F5F5;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div .up_lbl {\n  padding-left: 16px;\n  padding-right: 16px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n  background: #e1e9ee;\n}\n\n.main_content_div .chips_div {\n  display: flex;\n  flex-direction: row;\n  overflow: scroll;\n  padding-left: 16px;\n  padding-right: 16px;\n  background: #e1e9ee;\n  padding-top: 10px;\n  padding-bottom: 20px;\n}\n\n.main_content_div .chips_div .chip {\n  background-color: white;\n  border-radius: 3px;\n  padding: 5px 20px;\n  margin-right: 10px;\n  white-space: nowrap;\n  color: var(--ion-color-main);\n  font-size: 12px;\n  font-weight: bold;\n}\n\n.main_content_div .slider_div {\n  background: #e1e9ee;\n  padding-top: 10px;\n}\n\n.main_content_div .slider_div ion-slides {\n  height: 200px;\n  margin-top: -35px;\n}\n\n.main_content_div .slider_div .slide_div {\n  display: flex;\n  flex-direction: column;\n  width: 90%;\n  align-items: center;\n  border: 1px solid lightgray;\n  background: white;\n  height: 125px;\n  justify-content: center;\n}\n\n.main_content_div .slider_div .slide_div .up_div {\n  display: flex;\n  flex-direction: row;\n  padding: 15px;\n}\n\n.main_content_div .slider_div .slide_div .img_div {\n  width: 40px;\n}\n\n.main_content_div .slider_div .slide_div .img_div .slider_img {\n  width: 30px;\n  height: 30px;\n  min-width: 30px;\n}\n\n.main_content_div .slider_div .slide_div .content_div {\n  margin-left: 10px;\n}\n\n.main_content_div .slider_div .slide_div .content_div .abs_lbl {\n  position: absolute;\n  left: 5px;\n  top: 0;\n  font-size: 12px;\n}\n\n.main_content_div .slider_div .slide_div .content_div .head_lbl {\n  display: block;\n  font-weight: 500;\n  text-align: left;\n  font-size: 14px;\n}\n\n.main_content_div .slider_div .slide_div .content_div .small_lbl {\n  color: #707070;\n  text-align: left;\n  display: block;\n  font-size: 14px;\n}\n\n.main_content_div .slider_div .slide_div .lower_div {\n  border-top: 1px solid lightgray;\n  padding: 3px;\n  display: flex;\n  width: 100%;\n  justify-content: space-around;\n}\n\n.main_content_div .slider_div .slide_div .lower_div ion-button {\n  color: var(--ion-color-main);\n  font-weight: 600;\n}\n\n.main_content_div .div_card {\n  width: 100%;\n  background: white;\n  margin-bottom: 10px;\n  padding-left: 16px;\n  padding-right: 16px;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .div_card .head_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  padding-top: 10px;\n  padding-bottom: 10px;\n}\n\n.main_content_div .div_card .head_div .img_div {\n  height: 40px;\n  width: 40px;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover;\n  border-radius: 50%;\n  min-width: 40px;\n}\n\n.main_content_div .div_card .head_div .img_div .slider_img {\n  width: 40px;\n  height: 40px;\n  min-width: 40px;\n}\n\n.main_content_div .div_card .head_div .content_div {\n  width: 88%;\n  margin-left: 10px;\n  position: relative;\n}\n\n.main_content_div .div_card .head_div .content_div .head_lbl {\n  display: block;\n  font-weight: 500;\n  text-align: left;\n  font-size: 14px;\n}\n\n.main_content_div .div_card .head_div .content_div .small_lbl {\n  color: #707070;\n  text-align: left;\n  display: block;\n  font-size: 14px;\n}\n\n.main_content_div .div_card .head_div .content_div .card_arrow {\n  position: absolute;\n  right: 0;\n}\n\n.main_content_div .div_card .desc_div .small_lbl {\n  color: #707070;\n  text-align: left;\n  display: block;\n  font-size: 14px;\n}\n\n.main_content_div .div_card .desc_div .see_lbl {\n  color: #707070;\n  text-align: right;\n  display: block;\n  font-size: 14px;\n  margin-bottom: 5px;\n}\n\n.main_content_div .div_card .card_image {\n  height: 200px;\n  width: 100%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.main_content_div .div_card .like_div {\n  padding-top: 10px;\n  padding-bottom: 5px;\n  display: flex;\n  justify-content: space-evenly;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .div_card .like_div img {\n  width: 20px;\n  margin-right: 5px;\n}\n\n.main_content_div .div_card .like_div ion-button {\n  color: gray;\n  font-size: 16px;\n}\n\n.main_content_div .div_card .like_div2 {\n  padding-top: 10px;\n  padding-bottom: 5px;\n  display: flex;\n  justify-content: space-between;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .div_card .like_div2 img {\n  width: 20px;\n  margin-right: 5px;\n}\n\n.main_content_div .div_card .like_div2 ion-button {\n  color: gray;\n  font-size: 16px;\n}\n\n.main_content_div .masonry {\n  /* Masonry container */\n  -moz-column-count: 4;\n  column-count: 2;\n  -moz-column-gap: 1em;\n  column-gap: 1em;\n  margin: 0px;\n  padding: 0;\n  -moz-column-gap: 1.5em;\n  column-gap: 3px;\n  font-size: 0.85em;\n}\n\n.main_content_div .item {\n  display: inline-block;\n  background: #fff;\n  padding: 0px;\n  margin: 0px;\n  width: 100%;\n  box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  -webkit-box-sizing: border-box;\n}\n\n.main_content_div .item img {\n  max-width: 100%;\n}\n\nion-slides {\n  --bullet-background-active: black;\n  --bullet-background: transparent;\n}\n\n.swiper-pagination-bullet {\n  border: 1px solid;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1uZXcvRDpcXFJhZ3V2YXJhbiBpbWFnZVxcZGlzZWxzaGlwL3NyY1xcYXBwXFxwYWdlc1xcaG9tZS1uZXdcXGhvbWUtbmV3LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvaG9tZS1uZXcvaG9tZS1uZXcucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUNBQUE7QUNDSjs7QURDQTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUNFSjs7QURBQTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FDR0o7O0FEREk7RUFDSSxZQUFBO0VBQ0EsV0FBQTtBQ0dSOztBRERJO0VBQ0ksbUJBQUE7RUFDQSw0QkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDR1I7O0FEQ0E7RUFDSSxxQkFBQTtBQ0VKOztBRENBO0VBQ0ksV0FBQTtBQ0VKOztBRERJO0VBQ0ksa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0dSOztBRERJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0FDR1I7O0FEQVE7RUFDSSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNFWjs7QURFSTtFQUNJLG1CQUFBO0VBQ0EsaUJBQUE7QUNBUjs7QURFUTtFQUNJLGFBQUE7RUFDQSxpQkFBQTtBQ0FaOztBREdRO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7RUFFQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtBQ0ZaOztBRElZO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtBQ0ZoQjs7QURLWTtFQUNJLFdBQUE7QUNIaEI7O0FES2dCO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDSHBCOztBRE1ZO0VBQ0ksaUJBQUE7QUNKaEI7O0FET2dCO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsTUFBQTtFQUNBLGVBQUE7QUNMcEI7O0FET2dCO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDTHBCOztBRE9nQjtFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDTHBCOztBRFFZO0VBQ0ksK0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSw2QkFBQTtBQ05oQjs7QURRZ0I7RUFDSSw0QkFBQTtFQUNBLGdCQUFBO0FDTnBCOztBRFlJO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0NBQUE7QUNWUjs7QURhUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0FDWFo7O0FEYVk7RUFHSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLDRCQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ2JoQjs7QURjZ0I7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNacEI7O0FEZVk7RUFDSSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ2JoQjs7QURjZ0I7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNacEI7O0FEY2dCO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNacEI7O0FEY2dCO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0FDWnBCOztBRGlCWTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDZmhCOztBRGlCWTtFQUNJLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNmaEI7O0FEa0JRO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUNoQlo7O0FEbUJRO0VBQ0ksaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSw2QkFBQTtFQUNBLGtDQUFBO0FDakJaOztBRGtCWTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtBQ2hCaEI7O0FEbUJZO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUNqQmhCOztBRG9CUztFQUNHLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQ0FBQTtBQ2xCWjs7QURtQlk7RUFDSSxXQUFBO0VBQ0EsaUJBQUE7QUNqQmhCOztBRG9CWTtFQUNJLFdBQUE7RUFDQSxlQUFBO0FDbEJoQjs7QURtQ0k7RUFBVyxzQkFBQTtFQUVQLG9CQUFBO0VBQ0EsZUFBQTtFQUVBLG9CQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0Esc0JBQUE7RUFFQSxlQUFBO0VBQ0EsaUJBQUE7QUNoQ1I7O0FEa0NJO0VBQ0kscUJBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUVBLHNCQUFBO0VBQ0EsMkJBQUE7RUFDQSw4QkFBQTtBQ2pDUjs7QURtQ0k7RUFBVSxlQUFBO0FDaENkOztBRHFDQTtFQUNJLGlDQUFBO0VBQ0EsZ0NBQUE7QUNsQ0o7O0FEcUNBO0VBQ0ksaUJBQUE7QUNsQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob21lLW5ldy9ob21lLW5ldy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaHtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG4uaGVhZGVyX2RpdntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyOyAgICBcbiAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG5cbiAgICAudXNlcl9pbWFnZXtcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgICAgICB3aWR0aDogMjVweDtcbiAgICB9XG4gICAgaW9uLXNlYXJjaGJhcntcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTsgICBcbiAgICAgICAgLS1wbGFjZWhvbGRlci1jb2xvcjogICM3MDcwNzA7IFxuICAgICAgICAtLWljb24tY29sb3IgOiAjNzA3MDcwO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgICAgIGhlaWdodDogMzBweDtcbiAgICB9XG59XG5cbmlvbi1jb250ZW50e1xuICAgIC0tYmFja2dyb3VuZDogI0Y1RjVGNTtcbn1cblxuLm1haW5fY29udGVudF9kaXZ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgLnVwX2xibHtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGJhY2tncm91bmQ6ICNlMWU5ZWU7XG4gICAgfVxuICAgIC5jaGlwc19kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIG92ZXJmbG93OiBzY3JvbGw7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMTZweDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICAgICAgYmFja2dyb3VuZDogI2UxZTllZTtcbiAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xuXG5cbiAgICAgICAgLmNoaXB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICAgICAgICAgIHBhZGRpbmc6IDVweCAyMHB4O1xuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5zbGlkZXJfZGl2e1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZTFlOWVlOyBcbiAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG5cbiAgICAgICAgaW9uLXNsaWRlc3tcbiAgICAgICAgICAgIGhlaWdodDogMjAwcHg7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtMzVweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5zbGlkZV9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAgIHdpZHRoOiA5MCU7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICAgICAgLy8gcGFkZGluZzogMjBweCAxNXB4O1xuICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgICAgICBoZWlnaHQ6IDEyNXB4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cbiAgICAgICAgICAgIC51cF9kaXZ7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5pbWdfZGl2e1xuICAgICAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgICAgIC8vIHBhZGRpbmc6IDE1cHg7XG4gICAgICAgICAgICAgICAgLnNsaWRlcl9pbWd7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgICAgICAgICAgICAgICAgIG1pbi13aWR0aDogMzBweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuY29udGVudF9kaXZ7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgLy8gcGFkZGluZzogMTVweDtcblxuICAgICAgICAgICAgICAgIC5hYnNfbGJse1xuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgICAgIGxlZnQ6IDVweDtcbiAgICAgICAgICAgICAgICAgICAgdG9wOiAwO1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5oZWFkX2xibHtcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLnNtYWxsX2xibHtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM3MDcwNzA7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmxvd2VyX2RpdntcbiAgICAgICAgICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDNweDtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuXG4gICAgICAgICAgICAgICAgaW9uLWJ1dHRvbntcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuZGl2X2NhcmR7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuXG5cbiAgICAgICAgLmhlYWRfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XG5cbiAgICAgICAgICAgIC5pbWdfZGl2e1xuICAgICAgICAgICAgICAgIC8vIHdpZHRoOiAxMiU7XG5cbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDQwcHg7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgbWluLXdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgICAgIC5zbGlkZXJfaW1ne1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogNDBweDtcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgICAgICAgICBtaW4td2lkdGg6IDQwcHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmNvbnRlbnRfZGl2e1xuICAgICAgICAgICAgICAgIHdpZHRoOiA4OCU7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgICAgIC5oZWFkX2xibHtcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLnNtYWxsX2xibHtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM3MDcwNzA7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5jYXJkX2Fycm93e1xuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgICAgIHJpZ2h0OiAwO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuZGVzY19kaXZ7XG4gICAgICAgICAgICAuc21hbGxfbGJse1xuICAgICAgICAgICAgICAgIGNvbG9yOiAjNzA3MDcwO1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnNlZV9sYmx7XG4gICAgICAgICAgICAgICAgY29sb3I6ICM3MDcwNzA7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuY2FyZF9pbWFnZXtcbiAgICAgICAgICAgIGhlaWdodDogMjAwcHg7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICB9XG5cbiAgICAgICAgLmxpa2VfZGl2e1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtZXZlbmx5O1xuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgICAgIGltZ3tcbiAgICAgICAgICAgICAgICB3aWR0aDogMjBweDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaW9uLWJ1dHRvbiB7XG4gICAgICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgICAubGlrZV9kaXYye1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgICAgICBpbWd7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlvbi1idXR0b24ge1xuICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIC5tYXNvbnJ5IHsgLyogTWFzb25yeSBjb250YWluZXIgKi9cbiAgICAvLyAgICAgY29sdW1uLWNvdW50OiA0O1xuICAgIC8vICAgICBjb2x1bW4tZ2FwOiAxZW07XG4gICAgLy8gICB9XG4gICAgICBcbiAgICAvLyAgIC5pdGVtIHsgLyogTWFzb25yeSBicmlja3Mgb3IgY2hpbGQgZWxlbWVudHMgKi9cbiAgICAvLyAgICAgYmFja2dyb3VuZC1jb2xvcjogI2VlZTtcbiAgICAvLyAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIC8vICAgICBtYXJnaW46IDAgMCAxZW07XG4gICAgLy8gICAgIHdpZHRoOiAxMDAlO1xuICAgIC8vICAgfVxuXG4gICAgLm1hc29ucnkgeyAvKiBNYXNvbnJ5IGNvbnRhaW5lciAqL1xuICAgICAgICAtd2Via2l0LWNvbHVtbi1jb3VudDogNDtcbiAgICAgICAgLW1vei1jb2x1bW4tY291bnQ6NDtcbiAgICAgICAgY29sdW1uLWNvdW50OiAyO1xuICAgICAgICAtd2Via2l0LWNvbHVtbi1nYXA6IDFlbTtcbiAgICAgICAgLW1vei1jb2x1bW4tZ2FwOiAxZW07XG4gICAgICAgIGNvbHVtbi1nYXA6IDFlbTtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgIC1tb3otY29sdW1uLWdhcDogMS41ZW07XG4gICAgICAgIC13ZWJraXQtY29sdW1uLWdhcDogMS41ZW07XG4gICAgICAgIGNvbHVtbi1nYXA6IDNweDtcbiAgICAgICAgZm9udC1zaXplOiAuODVlbTtcbiAgICB9XG4gICAgLml0ZW0ge1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgIHBhZGRpbmc6IDBweDtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAvLyAtd2Via2l0LXRyYW5zaXRpb246MXMgZWFzZSBhbGw7XG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgICAgIC1tb3otYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAgICAgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIH1cbiAgICAuaXRlbSBpbWd7bWF4LXdpZHRoOjEwMCU7fVxuICAgICAgXG59XG5cblxuaW9uLXNsaWRlc3tcbiAgICAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZSA6IGJsYWNrO1xuICAgIC0tYnVsbGV0LWJhY2tncm91bmQgOiB0cmFuc3BhcmVudDsgICBcbn1cblxuLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHtcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcbn1cbiIsImlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5cbmlvbi10b29sYmFyLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgsIGlvbi10b29sYmFyIC5zYy1pb24tc2VhcmNoYmFyLWlvcy1oIHtcbiAgcGFkZGluZy10b3A6IDBweDtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbn1cblxuLmhlYWRlcl9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIGhlaWdodDogNDBweDtcbn1cbi5oZWFkZXJfZGl2IC51c2VyX2ltYWdlIHtcbiAgaGVpZ2h0OiAyNXB4O1xuICB3aWR0aDogMjVweDtcbn1cbi5oZWFkZXJfZGl2IGlvbi1zZWFyY2hiYXIge1xuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLXBsYWNlaG9sZGVyLWNvbG9yOiAjNzA3MDcwO1xuICAtLWljb24tY29sb3I6ICM3MDcwNzA7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgaGVpZ2h0OiAzMHB4O1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogI0Y1RjVGNTtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51cF9sYmwge1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYmFja2dyb3VuZDogI2UxZTllZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jaGlwc19kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBvdmVyZmxvdzogc2Nyb2xsO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIGJhY2tncm91bmQ6ICNlMWU5ZWU7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jaGlwc19kaXYgLmNoaXAge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICBwYWRkaW5nOiA1cHggMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYge1xuICBiYWNrZ3JvdW5kOiAjZTFlOWVlO1xuICBwYWRkaW5nLXRvcDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IGlvbi1zbGlkZXMge1xuICBoZWlnaHQ6IDIwMHB4O1xuICBtYXJnaW4tdG9wOiAtMzVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IC5zbGlkZV9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB3aWR0aDogOTAlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEyNXB4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLnVwX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5pbWdfZGl2IHtcbiAgd2lkdGg6IDQwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5pbWdfZGl2IC5zbGlkZXJfaW1nIHtcbiAgd2lkdGg6IDMwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgbWluLXdpZHRoOiAzMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAuY29udGVudF9kaXYge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmNvbnRlbnRfZGl2IC5hYnNfbGJsIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1cHg7XG4gIHRvcDogMDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAuY29udGVudF9kaXYgLmhlYWRfbGJsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmNvbnRlbnRfZGl2IC5zbWFsbF9sYmwge1xuICBjb2xvcjogIzcwNzA3MDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmxvd2VyX2RpdiB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIHBhZGRpbmc6IDNweDtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAubG93ZXJfZGl2IGlvbi1idXR0b24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmhlYWRfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuaGVhZF9kaXYgLmltZ19kaXYge1xuICBoZWlnaHQ6IDQwcHg7XG4gIHdpZHRoOiA0MHB4O1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgbWluLXdpZHRoOiA0MHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5oZWFkX2RpdiAuaW1nX2RpdiAuc2xpZGVyX2ltZyB7XG4gIHdpZHRoOiA0MHB4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIG1pbi13aWR0aDogNDBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuaGVhZF9kaXYgLmNvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDg4JTtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuaGVhZF9kaXYgLmNvbnRlbnRfZGl2IC5oZWFkX2xibCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmhlYWRfZGl2IC5jb250ZW50X2RpdiAuc21hbGxfbGJsIHtcbiAgY29sb3I6ICM3MDcwNzA7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmhlYWRfZGl2IC5jb250ZW50X2RpdiAuY2FyZF9hcnJvdyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmRlc2NfZGl2IC5zbWFsbF9sYmwge1xuICBjb2xvcjogIzcwNzA3MDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuZGVzY19kaXYgLnNlZV9sYmwge1xuICBjb2xvcjogIzcwNzA3MDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuY2FyZF9pbWFnZSB7XG4gIGhlaWdodDogMjAwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmxpa2VfZGl2IHtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtZXZlbmx5O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5saWtlX2RpdiBpbWcge1xuICB3aWR0aDogMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmxpa2VfZGl2IGlvbi1idXR0b24ge1xuICBjb2xvcjogZ3JheTtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5saWtlX2RpdjIge1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5saWtlX2RpdjIgaW1nIHtcbiAgd2lkdGg6IDIwcHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5saWtlX2RpdjIgaW9uLWJ1dHRvbiB7XG4gIGNvbG9yOiBncmF5O1xuICBmb250LXNpemU6IDE2cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAubWFzb25yeSB7XG4gIC8qIE1hc29ucnkgY29udGFpbmVyICovXG4gIC13ZWJraXQtY29sdW1uLWNvdW50OiA0O1xuICAtbW96LWNvbHVtbi1jb3VudDogNDtcbiAgY29sdW1uLWNvdW50OiAyO1xuICAtd2Via2l0LWNvbHVtbi1nYXA6IDFlbTtcbiAgLW1vei1jb2x1bW4tZ2FwOiAxZW07XG4gIGNvbHVtbi1nYXA6IDFlbTtcbiAgbWFyZ2luOiAwcHg7XG4gIHBhZGRpbmc6IDA7XG4gIC1tb3otY29sdW1uLWdhcDogMS41ZW07XG4gIC13ZWJraXQtY29sdW1uLWdhcDogMS41ZW07XG4gIGNvbHVtbi1nYXA6IDNweDtcbiAgZm9udC1zaXplOiAwLjg1ZW07XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaXRlbSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgcGFkZGluZzogMHB4O1xuICBtYXJnaW46IDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIC1tb3otYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xufVxuLm1haW5fY29udGVudF9kaXYgLml0ZW0gaW1nIHtcbiAgbWF4LXdpZHRoOiAxMDAlO1xufVxuXG5pb24tc2xpZGVzIHtcbiAgLS1idWxsZXQtYmFja2dyb3VuZC1hY3RpdmU6IGJsYWNrO1xuICAtLWJ1bGxldC1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/home-new/home-new.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/home-new/home-new.page.ts ***!
  \*************************************************/
/*! exports provided: HomeNewPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeNewPage", function() { return HomeNewPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/




let HomeNewPage = class HomeNewPage {
    constructor(actionSheet, api, menuCtrl, router) {
        this.actionSheet = actionSheet;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.router = router;
        this.user = this.api.getCurrentUser();
        this.posts = [];
        this.slideoptions = {
            slidesPerView: 1.2,
        };
        this.showSlider = false;
        this.chips = ['ADD PHOTOS', 'ADD CONNECTIONS', 'HASHTAGS FOLLOWED'];
        this.user.subscribe(user => {
            if (user) {
                console.log(user);
                this.loadPrivatePosts();
            }
            else {
                this.posts = [];
            }
        });
    }
    ngOnInit() {
        // this.route.data.subscribe(routeData => {
        //   const data = routeData['data'];
        //     for (let post of data) {
        //       if (post['_embedded']['wp:featuredmedia']) {
        //         post.media_url =
        //           post['_embedded']['wp:featuredmedia'][0]['media_details'].sizes['medium'].source_url;
        //       }
        //     }
        //   this.posts = data.posts;
        //   this.categoryId = data.categoryId;
        //   this.categoryTitle = data.categoryTitle;
        // })
    }
    sliderShow(value) {
        this.showSlider = value;
    }
    loadData(event) {
        const page = (Math.ceil(this.posts.length / 10)) + 1;
        this.api.getRecentPosts(this.categoryId, page)
            .subscribe((newPagePosts) => {
            this.posts.push(...newPagePosts);
            event.target.complete();
        }, err => {
            // there are no more posts available
            event.target.disabled = true;
        });
    }
    ionViewDidLoad() {
        this.api.retrieveCategories().subscribe(results => {
            this.categories = results;
        });
    }
    loadPrivatePosts() {
        this.api.getPosts().subscribe(res => {
            this.posts = res;
        });
    }
    openActionSheet() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheet.create({
                mode: 'md',
                buttons: [
                    {
                        text: 'Save',
                        role: 'destructive',
                        icon: 'bookmark-outline',
                        handler: () => {
                            console.log('Delete clicked');
                        }
                    },
                    {
                        text: 'Send in a private Message',
                        icon: 'chatbox-ellipses-outline',
                        handler: () => {
                            console.log('Share clicked');
                        }
                    },
                    {
                        text: 'Share via',
                        icon: 'share-social',
                        handler: () => {
                            console.log('Play clicked');
                        }
                    },
                    {
                        text: 'Hide this post',
                        icon: 'close-circle-outline',
                        handler: () => {
                            console.log('Favorite clicked');
                        }
                    },
                    {
                        text: 'Unfollow',
                        icon: 'person-remove-outline',
                        handler: () => {
                            console.log('Favorite clicked');
                        }
                    },
                    {
                        text: 'Report this post',
                        icon: 'flag-outline',
                        handler: () => {
                            console.log('Favorite clicked');
                        }
                    },
                    {
                        text: 'Improve my feed',
                        icon: 'options-outline',
                        handler: () => {
                            console.log('Favorite clicked');
                        }
                    },
                    {
                        text: 'Who can see this post?',
                        icon: 'eye-outline',
                        handler: () => {
                            console.log('Favorite clicked');
                        }
                    }
                ]
            });
            yield actionSheet.present();
        });
    }
    openMenu() {
        this.menuCtrl.open();
    }
    onSearchChange(event) {
    }
    goToChatList() {
        this.router.navigate(['/chatlist']);
    }
    goTosinglePost() {
        this.router.navigate(['/single-post']);
    }
};
HomeNewPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
    { type: _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
HomeNewPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-new',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home-new.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-new/home-new.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home-new.page.scss */ "./src/app/pages/home-new/home-new.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"], _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], HomeNewPage);



/***/ }),

/***/ "./src/app/pages/home-new/home-new.resolver.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/home-new/home-new.resolver.ts ***!
  \*****************************************************/
/*! exports provided: PostsResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostsResolver", function() { return PostsResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");




let PostsResolver = class PostsResolver {
    constructor(api) {
        this.api = api;
    }
    resolve(route) {
        const categoryId = route.queryParams['categoryId'];
        const categoryTitle = route.queryParams['title'];
        return this.api.getRecentPosts(categoryId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((posts) => {
            return { posts, categoryTitle, categoryId };
        }));
    }
};
PostsResolver.ctorParameters = () => [
    { type: _services_api_service__WEBPACK_IMPORTED_MODULE_2__["apiService"] }
];
PostsResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_2__["apiService"]])
], PostsResolver);



/***/ })

}]);
//# sourceMappingURL=pages-home-new-home-new-module-es2015.js.map
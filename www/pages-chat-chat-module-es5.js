function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-chat-chat-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/chat/chat.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/chat/chat.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesChatChatPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <div class=\"div_header\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button mode=\"md\" color=\"light\"></ion-back-button>\n      <div>\n        <ion-label class=\"username\">Rahul Jograna</ion-label>\n        <ion-label class=\"active_session\">Mobile • 45 min ago</ion-label>\n      </div>\n    </ion-buttons>\n  </div>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n    <div class=\"image_div\">\n      <div class=\"back_image\" [style.backgroundImage]=\"'url(assets/imgs/user3.jpg)'\"></div>\n      <ion-label class=\"username\">Rahul Jograna</ion-label>\n      <ion-label class=\"active_session\">Software Engineer</ion-label>\n    </div>\n\n    <div class=\"or_div\">\n      <img src=\"assets/imgs/line.png\" class=\"line_img\">\n      <ion-label class=\"or_text\">Today</ion-label>\n      <img src=\"assets/imgs/line.png\" class=\"line_img\">\n    </div>\n\n    <div class=\"content_div\">\n      <div class=\"list_div\" *ngFor=\"let item of messages\" (click)=\"goToChat()\">\n        <div class=\"back_image\" [style.backgroundImage]=\"'url('+item.img+')'\"></div>\n        <div class=\"desc\">\n          <ion-label class=\"usernane\">{{item.user}} • <span class=\"time\">07:00 PM</span></ion-label>\n          <ion-label class=\"msg\">{{item.msg}}</ion-label>\n          <!-- <ion-label class=\"time\">07:00 PM</ion-label> -->\n        </div>\n      </div>\n    </div>\n\n  </div>\n</ion-content>\n\n<ion-footer>\n  <div class=\"footer_div\">\n    <div class=\"add_div\">\n      <ion-icon slot=\"start\" class=\"add_icn\" name=\"add\"></ion-icon>\n    </div>\n    <ion-input type=\"text\" placeholder=\"Type message\"></ion-input>\n    <div>\n      <ion-icon class=\"send\" name=\"send-sharp\"></ion-icon>\n    </div>\n  </div>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/pages/chat/chat-routing.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/chat/chat-routing.module.ts ***!
    \***************************************************/

  /*! exports provided: ChatPageRoutingModule */

  /***/
  function srcAppPagesChatChatRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ChatPageRoutingModule", function () {
      return ChatPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _chat_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./chat.page */
    "./src/app/pages/chat/chat.page.ts");

    var routes = [{
      path: '',
      component: _chat_page__WEBPACK_IMPORTED_MODULE_3__["ChatPage"]
    }];

    var ChatPageRoutingModule = function ChatPageRoutingModule() {
      _classCallCheck(this, ChatPageRoutingModule);
    };

    ChatPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ChatPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/chat/chat.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/pages/chat/chat.module.ts ***!
    \*******************************************/

  /*! exports provided: ChatPageModule */

  /***/
  function srcAppPagesChatChatModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ChatPageModule", function () {
      return ChatPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./chat-routing.module */
    "./src/app/pages/chat/chat-routing.module.ts");
    /* harmony import */


    var _chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./chat.page */
    "./src/app/pages/chat/chat.page.ts");

    var ChatPageModule = function ChatPageModule() {
      _classCallCheck(this, ChatPageModule);
    };

    ChatPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__["ChatPageRoutingModule"]],
      declarations: [_chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]]
    })], ChatPageModule);
    /***/
  },

  /***/
  "./src/app/pages/chat/chat.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/pages/chat/chat.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesChatChatPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".div_header {\n  background: var(--ion-color-main);\n  height: 100px;\n  align-items: center;\n  padding-top: 40px;\n}\n.div_header ion-label {\n  display: block;\n  color: white;\n}\n.div_header .username {\n  font-weight: 600;\n}\n.div_header .active_session {\n  font-size: 14px;\n}\n.main_content_div {\n  width: 100%;\n}\n.main_content_div .image_div {\n  padding: 16px;\n}\n.main_content_div .image_div .back_image {\n  height: 70px;\n  width: 70px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.main_content_div .image_div ion-label {\n  display: block;\n  margin-top: 7px;\n}\n.main_content_div .image_div .username {\n  font-weight: 600;\n}\n.main_content_div .image_div .active_session {\n  font-size: 14px;\n}\n.main_content_div .or_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  align-items: center;\n  justify-content: space-between;\n}\n.main_content_div .or_div .line_img {\n  height: 10px;\n  width: 40%;\n}\n.main_content_div .or_div .or_text {\n  color: black;\n}\n.main_content_div .content_div {\n  padding: 16px;\n}\n.main_content_div .content_div .list_div {\n  display: flex;\n  padding: 6px 0px;\n  align-items: center;\n}\n.main_content_div .content_div .list_div .back_image {\n  height: 30px;\n  width: 30px;\n  min-width: 30px;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  border-radius: 50%;\n}\n.main_content_div .content_div .list_div .desc {\n  width: 100%;\n  padding-left: 8px;\n  position: relative;\n  width: 100%;\n}\n.main_content_div .content_div .list_div .desc ion-label {\n  display: block;\n}\n.main_content_div .content_div .list_div .desc .usernane {\n  font-weight: 600;\n  font-size: 14px;\n}\n.main_content_div .content_div .list_div .desc .msg {\n  font-size: 14px;\n  margin-top: 3px;\n}\n.main_content_div .content_div .list_div .desc .time {\n  font-size: 12px;\n  color: #505050;\n}\n.footer_div {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  background: #E1E9EE;\n  padding: 16px;\n}\n.footer_div .add_div {\n  width: 30px;\n  height: 30px;\n  border-radius: 50%;\n  background: white;\n  position: relative;\n}\n.footer_div .add_icn {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  font-size: 25px;\n  color: #505050;\n}\n.footer_div ion-input {\n  background: white;\n  border-radius: 5px;\n  margin-left: 10px;\n  margin-right: 10px;\n  --padding-start: 8px;\n}\n.footer_div .send {\n  font-size: 25px;\n  color: var(--ion-color-main);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY2hhdC9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxjaGF0XFxjaGF0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvY2hhdC9jaGF0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlDQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNDSjtBREFJO0VBQ0ksY0FBQTtFQUNBLFlBQUE7QUNFUjtBRENJO0VBQ0ksZ0JBQUE7QUNDUjtBRENJO0VBQ0ksZUFBQTtBQ0NSO0FER0E7RUFDSSxXQUFBO0FDQUo7QURDSTtFQUNJLGFBQUE7QUNDUjtBRENRO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQ0NaO0FEQ1E7RUFDSSxjQUFBO0VBQ0EsZUFBQTtBQ0NaO0FERVE7RUFDSSxnQkFBQTtBQ0FaO0FERVE7RUFDSSxlQUFBO0FDQVo7QURJSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FDRlI7QURJUTtFQUNJLFlBQUE7RUFDQSxVQUFBO0FDRlo7QURJUTtFQUNJLFlBQUE7QUNGWjtBRE1JO0VBQ0ksYUFBQTtBQ0pSO0FES1E7RUFDSSxhQUFBO0VBQ0EsZ0JBQUE7RUFFQSxtQkFBQTtBQ0paO0FES1k7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtBQ0hoQjtBRE1ZO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDSmhCO0FES2dCO0VBQ0ksY0FBQTtBQ0hwQjtBRE1nQjtFQUNJLGdCQUFBO0VBQ0EsZUFBQTtBQ0pwQjtBRE1nQjtFQUNJLGVBQUE7RUFDQSxlQUFBO0FDSnBCO0FET2dCO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUNMcEI7QURZQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0FDVEo7QURXSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDVFI7QURZSTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDVlI7QURZSTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7QUNWUjtBRFlJO0VBQ0ksZUFBQTtFQUNBLDRCQUFBO0FDVlIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9jaGF0L2NoYXQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpdl9oZWFkZXJ7XG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogNDBweDtcbiAgICBpb24tbGFiZWwge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgIH1cblxuICAgIC51c2VybmFtZXtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICB9XG4gICAgLmFjdGl2ZV9zZXNzaW9ue1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgfVxufVxuXG4ubWFpbl9jb250ZW50X2RpdntcbiAgICB3aWR0aDogMTAwJTtcbiAgICAuaW1hZ2VfZGl2e1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuXG4gICAgICAgIC5iYWNrX2ltYWdle1xuICAgICAgICAgICAgaGVpZ2h0OiA3MHB4O1xuICAgICAgICAgICAgd2lkdGg6IDcwcHg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgfSAgIFxuICAgICAgICBpb24tbGFiZWwge1xuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA3cHg7XG4gICAgICAgIH1cbiAgICBcbiAgICAgICAgLnVzZXJuYW1le1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgfVxuICAgICAgICAuYWN0aXZlX3Nlc3Npb257XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAub3JfZGl2e1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuXG4gICAgICAgIC5saW5lX2ltZ3tcbiAgICAgICAgICAgIGhlaWdodDogMTBweDtcbiAgICAgICAgICAgIHdpZHRoOiA0MCU7XG4gICAgICAgIH1cbiAgICAgICAgLm9yX3RleHR7XG4gICAgICAgICAgICBjb2xvcjogYmxhY2s7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuY29udGVudF9kaXZ7XG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgICAgIC5saXN0X2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBwYWRkaW5nOiA2cHggMHB4O1xuICAgICAgICAgICAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgICAuYmFja19pbWFnZXtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgICAgICAgICAgbWluLXdpZHRoOiAzMHB4O1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAuZGVzY3tcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDhweDtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAgICAgaW9uLWxhYmVsIHtcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLnVzZXJuYW5le1xuICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5tc2d7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogM3B4O1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC50aW1le1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjNTA1MDUwO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cblxuLmZvb3Rlcl9kaXZ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBiYWNrZ3JvdW5kOiAjRTFFOUVFO1xuICAgIHBhZGRpbmc6IDE2cHg7XG5cbiAgICAuYWRkX2RpdntcbiAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIH1cblxuICAgIC5hZGRfaWNue1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHRvcDogNTAlO1xuICAgICAgICBsZWZ0OiA1MCU7XG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsLTUwJSk7XG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICAgICAgY29sb3I6ICM1MDUwNTA7XG4gICAgfVxuICAgIGlvbi1pbnB1dHtcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiA4cHg7XG4gICAgfVxuICAgIC5zZW5ke1xuICAgICAgICBmb250LXNpemU6IDI1cHg7XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgfVxufVxuIiwiLmRpdl9oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGhlaWdodDogMTAwcHg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmctdG9wOiA0MHB4O1xufVxuLmRpdl9oZWFkZXIgaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5kaXZfaGVhZGVyIC51c2VybmFtZSB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4uZGl2X2hlYWRlciAuYWN0aXZlX3Nlc3Npb24ge1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaW1hZ2VfZGl2IHtcbiAgcGFkZGluZzogMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5pbWFnZV9kaXYgLmJhY2tfaW1hZ2Uge1xuICBoZWlnaHQ6IDcwcHg7XG4gIHdpZHRoOiA3MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5pbWFnZV9kaXYgaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbi10b3A6IDdweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5pbWFnZV9kaXYgLnVzZXJuYW1lIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5pbWFnZV9kaXYgLmFjdGl2ZV9zZXNzaW9uIHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLm9yX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIHdpZHRoOiAxMDAlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4ubWFpbl9jb250ZW50X2RpdiAub3JfZGl2IC5saW5lX2ltZyB7XG4gIGhlaWdodDogMTBweDtcbiAgd2lkdGg6IDQwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5vcl9kaXYgLm9yX3RleHQge1xuICBjb2xvcjogYmxhY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYge1xuICBwYWRkaW5nOiAxNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5saXN0X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBhZGRpbmc6IDZweCAwcHg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmxpc3RfZGl2IC5iYWNrX2ltYWdlIHtcbiAgaGVpZ2h0OiAzMHB4O1xuICB3aWR0aDogMzBweDtcbiAgbWluLXdpZHRoOiAzMHB4O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAubGlzdF9kaXYgLmRlc2Mge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZy1sZWZ0OiA4cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmxpc3RfZGl2IC5kZXNjIGlvbi1sYWJlbCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5saXN0X2RpdiAuZGVzYyAudXNlcm5hbmUge1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmxpc3RfZGl2IC5kZXNjIC5tc2cge1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi10b3A6IDNweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAubGlzdF9kaXYgLmRlc2MgLnRpbWUge1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNTA1MDUwO1xufVxuXG4uZm9vdGVyX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYmFja2dyb3VuZDogI0UxRTlFRTtcbiAgcGFkZGluZzogMTZweDtcbn1cbi5mb290ZXJfZGl2IC5hZGRfZGl2IHtcbiAgd2lkdGg6IDMwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmZvb3Rlcl9kaXYgLmFkZF9pY24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNTAlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICBmb250LXNpemU6IDI1cHg7XG4gIGNvbG9yOiAjNTA1MDUwO1xufVxuLmZvb3Rlcl9kaXYgaW9uLWlucHV0IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiA4cHg7XG59XG4uZm9vdGVyX2RpdiAuc2VuZCB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/chat/chat.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/pages/chat/chat.page.ts ***!
    \*****************************************/

  /*! exports provided: ChatPage */

  /***/
  function srcAppPagesChatChatPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ChatPage", function () {
      return ChatPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/services/dummy-data.service */
    "./src/app/services/dummy-data.service.ts");
    /* harmony import */


    var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic-native/native-page-transitions/ngx */
    "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var ChatPage = /*#__PURE__*/function () {
      function ChatPage(navCtrl, nativePageTransitions, dummy, router) {
        _classCallCheck(this, ChatPage);

        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.dummy = dummy;
        this.router = router;
        this.messages = [{
          user: 'Rahul Jograna',
          img: 'assets/imgs/user.jpg',
          msg: 'Hello'
        }, {
          user: 'Jonh Doe',
          img: 'assets/imgs/user2.jpg',
          msg: 'Hii'
        }, {
          user: 'Rahul Jograna',
          img: 'assets/imgs/user.jpg',
          msg: 'Are you nearby ?'
        }, {
          user: 'Jonh Doe',
          img: 'assets/imgs/user2.jpg',
          msg: 'I will be there in few mins'
        }, {
          user: 'Rahul Jograna',
          img: 'assets/imgs/user.jpg',
          msg: 'Ok, I am waitimg at vinmark Store'
        }, {
          user: 'Jonh Doe',
          img: 'assets/imgs/user2.jpg',
          msg: 'Sorry I am stuck in traffic. Please give me a moment.'
        }];
        this.options = {
          direction: 'up',
          duration: 500,
          slowdownfactor: 3,
          slidePixels: 20,
          iosdelay: 100,
          androiddelay: 150,
          fixedPixelsTop: 0,
          fixedPixelsBottom: 60
        };
        this.users = this.dummy.users;
      }

      _createClass(ChatPage, [{
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.nativePageTransitions.slide(this.options);
        } // example of adding a transition when pushing a new page

      }, {
        key: "openPage",
        value: function openPage(page) {
          this.nativePageTransitions.slide(this.options);
          this.navCtrl.navigateForward(page);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "goToChat",
        value: function goToChat() {}
      }]);

      return ChatPage;
    }();

    ChatPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
      }, {
        type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_4__["NativePageTransitions"]
      }, {
        type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_3__["dummyDataService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }];
    };

    ChatPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-chat',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./chat.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/chat/chat.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./chat.page.scss */
      "./src/app/pages/chat/chat.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"], _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_4__["NativePageTransitions"], src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_3__["dummyDataService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])], ChatPage);
    /***/
  },

  /***/
  "./src/app/services/dummy-data.service.ts":
  /*!************************************************!*\
    !*** ./src/app/services/dummy-data.service.ts ***!
    \************************************************/

  /*! exports provided: dummyDataService */

  /***/
  function srcAppServicesdummyDataServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "dummyDataService", function () {
      return dummyDataService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var dummyDataService = function dummyDataService() {
      _classCallCheck(this, dummyDataService);

      this.users = [{
        img: 'assets/imgs/user.jpg',
        cover: 'assets/imgs/back1.jpg',
        btn_text: 'VIEW JOBS'
      }, {
        img: 'assets/imgs/user2.jpg',
        cover: 'assets/imgs/back2.jpg',
        btn_text: 'FOLLOW HASHTAG'
      }, {
        img: 'assets/imgs/user3.jpg',
        cover: 'assets/imgs/back3.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user4.jpg',
        cover: 'assets/imgs/back4.jpg',
        btn_text: 'SEE ALL VIEW'
      }, {
        img: 'assets/imgs/user.jpg',
        cover: 'assets/imgs/back1.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user2.jpg',
        cover: 'assets/imgs/back2.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user3.jpg',
        cover: 'assets/imgs/back3.jpg',
        btn_text: 'EXPAND YOUR NETWORK'
      }, {
        img: 'assets/imgs/user4.jpg',
        cover: 'assets/imgs/back4.jpg',
        btn_text: 'SEE ALL VIEW'
      }];
      this.jobs = [{
        img: 'assets/imgs/company/initappz.png',
        title: 'SQL Server',
        addr: 'Bhavnagar, Gujrat, India',
        time: '2 school alumni',
        status: '1 day'
      }, {
        img: 'assets/imgs/company/google.png',
        title: 'Web Designer',
        addr: 'Vadodara, Gujrat, India',
        time: 'Be an early applicant',
        status: '2 days'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        title: 'Business Analyst',
        addr: 'Ahmedabad, Gujrat, India',
        time: 'Be an early applicant',
        status: '1 week'
      }, {
        img: 'assets/imgs/company/initappz.png',
        title: 'PHP Developer',
        addr: 'Pune, Maharashtra, India',
        time: '2 school alumni',
        status: 'New'
      }, {
        img: 'assets/imgs/company/google.png',
        title: 'Graphics Designer',
        addr: 'Bhavnagar, Gujrat, India',
        time: 'Be an early applicant',
        status: '1 day'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        title: 'Phython Developer',
        addr: 'Ahmedabad, Gujrat, India',
        time: '2 school alumni',
        status: '5 days'
      }];
      this.company = [{
        img: 'assets/imgs/company/initappz.png',
        name: 'Initappz Shop'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        name: 'Microsoft'
      }, {
        img: 'assets/imgs/company/google.png',
        name: 'Google'
      }];
    };

    dummyDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], dummyDataService);
    /***/
  }
}]);
//# sourceMappingURL=pages-chat-chat-module-es5.js.map
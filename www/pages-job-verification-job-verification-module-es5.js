function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-job-verification-job-verification-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-verification/job-verification.page.html":
  /*!*********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-verification/job-verification.page.html ***!
    \*********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesJobVerificationJobVerificationPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-button (click)=\"BackButton()\" slot=\"start\">\n      <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n    </ion-button>\n    <ion-title color=\"light\"> Pre Sea Training Detail </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n\n\n\n    <div class=\"form_div\">\n   \n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"4\">\n            <ion-label position=\"floating\">Status</ion-label>\n          </ion-col>\n          <ion-col size=\"4\">\n            <span class=\"red\" >Inactive</span>\n            <span class=\"green\" >Inactive</span>\n            <span class=\"bule\" >Inactive</span>\n          </ion-col>\n        </ion-row>\n      \n       \n      </ion-grid>\n\n      <ion-button class=\"join_now\" (click)=\"userupdate()\"  expand=\"full\">\n         <ion-spinner *ngIf=\"loading2\"></ion-spinner> <ion-label >   Click to Start Verification </ion-label>\n      </ion-button>\n     \n    </div>\n\n  \n  </div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/job-verification/job-verification-routing.module.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/pages/job-verification/job-verification-routing.module.ts ***!
    \***************************************************************************/

  /*! exports provided: JobVerificationPageRoutingModule */

  /***/
  function srcAppPagesJobVerificationJobVerificationRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "JobVerificationPageRoutingModule", function () {
      return JobVerificationPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _job_verification_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./job-verification.page */
    "./src/app/pages/job-verification/job-verification.page.ts");

    var routes = [{
      path: '',
      component: _job_verification_page__WEBPACK_IMPORTED_MODULE_3__["JobVerificationPage"]
    }];

    var JobVerificationPageRoutingModule = function JobVerificationPageRoutingModule() {
      _classCallCheck(this, JobVerificationPageRoutingModule);
    };

    JobVerificationPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], JobVerificationPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/job-verification/job-verification.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/pages/job-verification/job-verification.module.ts ***!
    \*******************************************************************/

  /*! exports provided: JobVerificationPageModule */

  /***/
  function srcAppPagesJobVerificationJobVerificationModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "JobVerificationPageModule", function () {
      return JobVerificationPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _job_verification_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./job-verification-routing.module */
    "./src/app/pages/job-verification/job-verification-routing.module.ts");
    /* harmony import */


    var _job_verification_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./job-verification.page */
    "./src/app/pages/job-verification/job-verification.page.ts");

    var JobVerificationPageModule = function JobVerificationPageModule() {
      _classCallCheck(this, JobVerificationPageModule);
    };

    JobVerificationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _job_verification_routing_module__WEBPACK_IMPORTED_MODULE_5__["JobVerificationPageRoutingModule"]],
      declarations: [_job_verification_page__WEBPACK_IMPORTED_MODULE_6__["JobVerificationPage"]]
    })], JobVerificationPageModule);
    /***/
  },

  /***/
  "./src/app/pages/job-verification/job-verification.page.scss":
  /*!*******************************************************************!*\
    !*** ./src/app/pages/job-verification/job-verification.page.scss ***!
    \*******************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesJobVerificationJobVerificationPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".header_div {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n.header_div .logo_div {\n  display: flex;\n}\n.header_div .logo_div .header_lbl {\n  color: var(--ion-color-main);\n  font-weight: bold;\n  font-size: 18px;\n  margin-left: 10px;\n}\n.header_div .logo_div .logo_img {\n  margin-left: 1px;\n  width: 20px;\n  height: 20px;\n}\n.header_div ion-button {\n  color: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 14px;\n}\n.red {\n  background-color: #f23d3d;\n  color: white;\n  border-radius: 25px;\n  padding: 7px;\n}\n.green {\n  background-color: green;\n  color: white;\n  border-radius: 25px;\n  padding: 7px;\n}\n.blue {\n  background-color: var(--ion-color-main);\n  color: white;\n  border-radius: 25px;\n  padding: 7px;\n}\n.error {\n  color: red;\n  padding: 5px;\n}\nion-toolbar {\n  --background: var(--ion-color-main);\n}\nion-toolbar ion-input {\n  border-bottom: 1px solid white;\n  --color: white;\n  --placeholder-color: white;\n  --padding-start: 8px;\n}\nion-toolbar ion-icon {\n  color: white;\n  font-size: 24px;\n}\n#content {\n  background-color: #ffffff;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n  height: 305px;\n  overflow: unset;\n  display: block;\n  border-radius: 15px;\n  background-size: cover;\n}\n.edit-btn {\n  font-size: 23px;\n  float: right;\n  margin: 10px;\n  color: white;\n  padding: 7px;\n  background: var(--ion-color-main);\n  border-radius: 10px;\n  position: absolute;\n  right: 0%;\n}\n.edit-btn2 {\n  font-size: 23px;\n  float: right;\n  margin: 10px;\n  color: white;\n  padding: 7px;\n  background: var(--ion-color-main);\n  border-radius: 10px;\n  position: absolute;\n  right: 28%;\n  z-index: 10;\n  top: 51%;\n}\n#profile-info {\n  width: 100%;\n  z-index: 2;\n  padding-top: 1px;\n  text-align: center;\n  position: absolute;\n  top: 45%;\n}\n#profile-image {\n  display: block;\n  border-radius: 120px;\n  border: 3px solid #fff;\n  width: 128px;\n  background: white;\n  height: 128px;\n  margin: 30px auto 0;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n}\n.main_content_div {\n  width: 100%;\n  padding: 20px;\n  height: 100%;\n}\n.main_content_div .header_lbl {\n  font-size: 26px;\n  font-weight: 600;\n}\n.main_content_div .join_btn {\n  --background: white;\n  --color: #0077B5;\n  border: 1px solid var(--ion-color-main);\n}\n.main_content_div .btn_search {\n  height: 25px;\n  width: 25px;\n  margin-right: 20px;\n}\n.main_content_div .or_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  height: 50px;\n  align-items: center;\n  justify-content: space-between;\n}\n.main_content_div .or_div .line_img {\n  height: 10px;\n  width: 40%;\n}\n.main_content_div .or_div .or_text {\n  color: black;\n}\n.main_content_div .form_div ion-item {\n  border-bottom: none;\n  --padding-start: 0px;\n}\n.main_content_div .form_div ion-item .ip {\n  border-bottom: 1px solid white;\n}\n.main_content_div .form_div .join_now {\n  margin-top: 20px;\n  border: 1px solid white;\n  --background: var(--ion-color-main);\n  font-weight: bold;\n  font-size: 18px;\n}\n.main_content_div .form_div .bottom_lbl {\n  display: block;\n  font-size: 14px;\n  margin-top: 20px;\n}\n.main_content_div .form_div .bottom_lbl span {\n  color: var(--ion-color-main) !important;\n  font-weight: bold;\n  color: gra;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvam9iLXZlcmlmaWNhdGlvbi9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxqb2ItdmVyaWZpY2F0aW9uXFxqb2ItdmVyaWZpY2F0aW9uLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvam9iLXZlcmlmaWNhdGlvbi9qb2ItdmVyaWZpY2F0aW9uLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUNDSjtBREFJO0VBQ0ksYUFBQTtBQ0VSO0FERFE7RUFDSSw0QkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDR1o7QUREUTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNHWjtBREFJO0VBQ0ksNEJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNFUjtBRENBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FDRUo7QURBQTtFQUNJLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ0dKO0FEREE7RUFDSSx1Q0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNJSjtBREZBO0VBQ0ksVUFBQTtFQUNBLFlBQUE7QUNLSjtBREhBO0VBQ0ksbUNBQUE7QUNNSjtBRExJO0VBQ0ksOEJBQUE7RUFDQSxjQUFBO0VBQ0EsMEJBQUE7RUFDQSxvQkFBQTtBQ09SO0FETEk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtBQ09SO0FESkE7RUFDSSx5QkFBQTtFQUNBLDRDQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ09KO0FETEU7RUFDRSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlDQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7QUNRSjtBRE5BO0VBQ0ksZUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxpQ0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7QUNTSjtBRFBFO0VBQ0UsV0FBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFFQSxRQUFBO0FDU0o7QURQRTtFQUNFLGNBQUE7RUFDQSxvQkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtFQUVBLG1CQUFBO0VBQ0EsNENBQUE7QUNTSjtBRExBO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0FDUUo7QUROSTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtBQ1FSO0FETkk7RUFDSSxtQkFBQTtFQUNBLGdCQUFBO0VBRUEsdUNBQUE7QUNPUjtBRExJO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQ09SO0FETEk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUNPUjtBRExRO0VBQ0ksWUFBQTtFQUNBLFVBQUE7QUNPWjtBRExRO0VBQ0ksWUFBQTtBQ09aO0FERlE7RUFDSSxtQkFBQTtFQUNBLG9CQUFBO0FDSVo7QURIWTtFQUNJLDhCQUFBO0FDS2hCO0FERFE7RUFDSSxnQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUNBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNHWjtBREFRO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0VaO0FEQVk7RUFDSSx1Q0FBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtBQ0VoQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2pvYi12ZXJpZmljYXRpb24vam9iLXZlcmlmaWNhdGlvbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyX2RpdntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgLmxvZ29fZGl2e1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAuaGVhZGVyX2xibHtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICB9XG4gICAgICAgIC5sb2dvX2ltZ3tcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxcHg7XG4gICAgICAgICAgICB3aWR0aDogMjBweDtcbiAgICAgICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgICAgfVxuICAgIH1cbiAgICBpb24tYnV0dG9ue1xuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgfVxufVxuLnJlZHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjIzZDNkO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgIHBhZGRpbmc6IDdweDtcbn1cbi5ncmVlbntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbjtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBwYWRkaW5nOiA3cHg7XG59XG4uYmx1ZXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgcGFkZGluZzogN3B4O1xufVxuLmVycm9ye1xuICAgIGNvbG9yOiByZWQ7XG4gICAgcGFkZGluZzogNXB4O1xufVxuaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGlvbi1pbnB1dHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAgICAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgLS1wbGFjZWhvbGRlci1jb2xvciA6IHdoaXRlO1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQgOiA4cHg7XG4gICAgfVxuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICB9XG59XG4jY29udGVudCB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYigwIDAgMCAvIDQwJSk7XG4gICAgaGVpZ2h0OiAzMDVweDtcbiAgICBvdmVyZmxvdzogdW5zZXQ7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICB9XG4gIC5lZGl0LWJ0bntcbiAgICBmb250LXNpemU6IDIzcHg7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIG1hcmdpbjogMTBweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogN3B4O1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogMCU7XG59XG4uZWRpdC1idG4ye1xuICAgIGZvbnQtc2l6ZTogMjNweDtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgbWFyZ2luOiAxMHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBwYWRkaW5nOiA3cHg7XG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAyOCU7XG4gICAgei1pbmRleDogMTA7XG4gICAgdG9wOiA1MSU7XG59XG4gICNwcm9maWxlLWluZm8ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHotaW5kZXg6IDI7XG4gICAgcGFkZGluZy10b3A6IDFweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIFxuICAgIHRvcDogNDUlO1xuICB9XG4gICNwcm9maWxlLWltYWdlIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBib3JkZXItcmFkaXVzOiAxMjBweDtcbiAgICBib3JkZXI6IDNweCBzb2xpZCAjZmZmO1xuICAgIHdpZHRoOiAxMjhweDtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICBoZWlnaHQ6IDEyOHB4O1xuICAgXG4gICAgbWFyZ2luOiAzMHB4IGF1dG8gMDtcbiAgICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgIC8vIGJveC1zaGFkb3c6IDBweCAwcHggNHB4IHJnYmEoMCwgMCwgMCwgMC43KTtcbiAgfVxuXG4ubWFpbl9jb250ZW50X2RpdntcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAyMHB4O1xuICAgIGhlaWdodDogMTAwJTtcblxuICAgIC5oZWFkZXJfbGJse1xuICAgICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgfVxuICAgIC5qb2luX2J0bntcbiAgICAgICAgLS1iYWNrZ3JvdW5kIDogd2hpdGU7XG4gICAgICAgIC0tY29sb3I6ICMwMDc3QjU7XG4gICAgICAgIC8vIC0tYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIH1cbiAgICAuYnRuX3NlYXJjaHtcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgICAgICB3aWR0aDogMjVweDtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xuICAgIH1cbiAgICAub3JfZGl2e1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG5cbiAgICAgICAgLmxpbmVfaW1ne1xuICAgICAgICAgICAgaGVpZ2h0OiAxMHB4O1xuICAgICAgICAgICAgd2lkdGg6IDQwJTtcbiAgICAgICAgfVxuICAgICAgICAub3JfdGV4dHtcbiAgICAgICAgICAgIGNvbG9yOiBibGFjaztcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5mb3JtX2RpdntcbiAgICAgICAgaW9uLWl0ZW17XG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiBub25lO1xuICAgICAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gICAgICAgICAgICAuaXB7XG4gICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLmpvaW5fbm93e1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHdoaXRlO1xuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIH1cblxuICAgICAgICAuYm90dG9tX2xibHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcblxuICAgICAgICAgICAgc3BhbntcbiAgICAgICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgICAgICAgICAgY29sb3I6IGdyYTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0iLCIuaGVhZGVyX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5oZWFkZXJfZGl2IC5sb2dvX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uaGVhZGVyX2RpdiAubG9nb19kaXYgLmhlYWRlcl9sYmwge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5oZWFkZXJfZGl2IC5sb2dvX2RpdiAubG9nb19pbWcge1xuICBtYXJnaW4tbGVmdDogMXB4O1xuICB3aWR0aDogMjBweDtcbiAgaGVpZ2h0OiAyMHB4O1xufVxuLmhlYWRlcl9kaXYgaW9uLWJ1dHRvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLnJlZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMjNkM2Q7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgcGFkZGluZzogN3B4O1xufVxuXG4uZ3JlZW4ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbjtcbiAgY29sb3I6IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICBwYWRkaW5nOiA3cHg7XG59XG5cbi5ibHVlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIHBhZGRpbmc6IDdweDtcbn1cblxuLmVycm9yIHtcbiAgY29sb3I6IHJlZDtcbiAgcGFkZGluZzogNXB4O1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXRvb2xiYXIgaW9uLWlucHV0IHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAtLWNvbG9yOiB3aGl0ZTtcbiAgLS1wbGFjZWhvbGRlci1jb2xvcjogd2hpdGU7XG4gIC0tcGFkZGluZy1zdGFydDogOHB4O1xufVxuaW9uLXRvb2xiYXIgaW9uLWljb24ge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMjRweDtcbn1cblxuI2NvbnRlbnQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgaGVpZ2h0OiAzMDVweDtcbiAgb3ZlcmZsb3c6IHVuc2V0O1xuICBkaXNwbGF5OiBibG9jaztcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cblxuLmVkaXQtYnRuIHtcbiAgZm9udC1zaXplOiAyM3B4O1xuICBmbG9hdDogcmlnaHQ7XG4gIG1hcmdpbjogMTBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiA3cHg7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMCU7XG59XG5cbi5lZGl0LWJ0bjIge1xuICBmb250LXNpemU6IDIzcHg7XG4gIGZsb2F0OiByaWdodDtcbiAgbWFyZ2luOiAxMHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDdweDtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAyOCU7XG4gIHotaW5kZXg6IDEwO1xuICB0b3A6IDUxJTtcbn1cblxuI3Byb2ZpbGUtaW5mbyB7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAyO1xuICBwYWRkaW5nLXRvcDogMXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA0NSU7XG59XG5cbiNwcm9maWxlLWltYWdlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJvcmRlci1yYWRpdXM6IDEyMHB4O1xuICBib3JkZXI6IDNweCBzb2xpZCAjZmZmO1xuICB3aWR0aDogMTI4cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEyOHB4O1xuICBtYXJnaW46IDMwcHggYXV0byAwO1xuICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMjBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmhlYWRlcl9sYmwge1xuICBmb250LXNpemU6IDI2cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuam9pbl9idG4ge1xuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLWNvbG9yOiAjMDA3N0I1O1xuICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuYnRuX3NlYXJjaCB7XG4gIGhlaWdodDogMjVweDtcbiAgd2lkdGg6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5vcl9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA1MHB4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4ubWFpbl9jb250ZW50X2RpdiAub3JfZGl2IC5saW5lX2ltZyB7XG4gIGhlaWdodDogMTBweDtcbiAgd2lkdGg6IDQwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5vcl9kaXYgLm9yX3RleHQge1xuICBjb2xvcjogYmxhY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZm9ybV9kaXYgaW9uLWl0ZW0ge1xuICBib3JkZXItYm90dG9tOiBub25lO1xuICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mb3JtX2RpdiBpb24taXRlbSAuaXAge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZm9ybV9kaXYgLmpvaW5fbm93IHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgd2hpdGU7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxOHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZvcm1fZGl2IC5ib3R0b21fbGJsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mb3JtX2RpdiAuYm90dG9tX2xibCBzcGFuIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IGdyYTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/job-verification/job-verification.page.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/job-verification/job-verification.page.ts ***!
    \*****************************************************************/

  /*! exports provided: JobVerificationPage */

  /***/
  function srcAppPagesJobVerificationJobVerificationPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "JobVerificationPage", function () {
      return JobVerificationPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! sweetalert2 */
    "./node_modules/sweetalert2/dist/sweetalert2.all.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_8__);
    /* harmony import */


    var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/android-permissions/ngx */
    "./node_modules/@ionic-native/android-permissions/ngx/index.js");
    /* harmony import */


    var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/file/ngx */
    "./node_modules/@ionic-native/file/ngx/index.js");
    /* harmony import */


    var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @ionic-native/native-page-transitions/ngx */
    "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var JobVerificationPage = /*#__PURE__*/function () {
      function JobVerificationPage(navCtrl, nativePageTransitions, loadingController, plt, file, router, api, location, androidPermissions, storage, fb, actionSheetCtrl, alertCtrl, toastCtrl) {
        var _this = this;

        _classCallCheck(this, JobVerificationPage);

        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.loadingController = loadingController;
        this.plt = plt;
        this.file = file;
        this.router = router;
        this.api = api;
        this.location = location;
        this.androidPermissions = androidPermissions;
        this.storage = storage;
        this.fb = fb;
        this.actionSheetCtrl = actionSheetCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.data = {};
        this.images = [];
        this.user = this.api.getCurrentUser();
        this.posts = [];
        this.country = [{
          value: 'TELENGANA'
        }, {
          value: 'India'
        }, {
          value: 'USA'
        }, {
          value: 'Pakistan'
        }, {
          value: 'China'
        }];
        this.defaultDate = "1987-06-30";
        this.isSubmitted = false;
        this.options1 = {
          direction: 'up',
          duration: 500,
          slowdownfactor: 3,
          slidePixels: 20,
          iosdelay: 100,
          androiddelay: 150,
          fixedPixelsTop: 0,
          fixedPixelsBottom: 60
        };
        this.loader();
        this.api.getJobdetails(this.token, "preseadetails").subscribe(function (res) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    this.data = res;
                    console.log(res);

                  case 2:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }, function (err) {
          _this.loading2 = false;
          console.log(err);

          _this.showError(err);
        });
        this.edit_icon = true;
        this.edit_icon2 = true;
        this.user.subscribe(function (user) {
          if (user) {
            console.log(user);
            _this.token = user.token;
          } else {
            _this.posts = [];
          }
        });
      }

      _createClass(JobVerificationPage, [{
        key: "errorControl",
        get: function get() {
          return this.userForm.controls;
        }
      }, {
        key: "loader",
        value: function loader() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.loadingController.create({
                      message: 'Please wait...',
                      duration: 1500
                    });

                  case 2:
                    loading = _context2.sent;
                    _context2.next = 5;
                    return loading.present();

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "getDate",
        value: function getDate(e) {
          var date = new Date(e.target.value).toISOString().substring(0, 10);
          this.userForm.get('attended_from_date').setValue(date, {
            onlyself: true
          });
        }
      }, {
        key: "getDate2",
        value: function getDate2(e) {
          var date = new Date(e.target.value).toISOString().substring(0, 10);
          this.userForm.get('attended_to_date').setValue(date, {
            onlyself: true
          });
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.userForm = this.fb.group({
            training_institute: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            training_institute_other: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            course_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            attended_from_date: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            pre_certificate_no: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern('^[0-9]+$')]],
            pre_sea_tar_book_no: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern('^[0-9]+$')]],
            attended_to_date: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            result: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]
          });
        }
      }, {
        key: "userupdate",
        value: function userupdate() {
          var _this2 = this;

          this.loading2 = true;
          this.isSubmitted = true;

          if (!this.userForm.valid) {
            console.log('Please provide all the required values!');
            this.loading2 = false;
            return false;
          } else {
            console.log(this.userForm.value);
          }

          var formdat = [];
          formdat.push({
            meta_key: 'training_institute',
            meta_value: this.userForm.value.training_institute
          }, {
            meta_key: 'training_institute_other',
            meta_value: this.userForm.value.training_institute_other
          }, {
            meta_key: 'course_name',
            meta_value: this.userForm.value.course_name
          }, {
            meta_key: 'attended_from_date',
            meta_value: this.userForm.value.attended_from_date
          }, {
            meta_key: 'pre_certificate_no',
            meta_value: this.userForm.value.pre_certificate_no
          }, {
            meta_key: 'pre_sea_tar_book_no',
            meta_value: this.userForm.value.pre_sea_tar_book_no
          }, {
            meta_key: 'attended_to_date',
            meta_value: this.userForm.value.attended_to_date
          }, {
            meta_key: 'result',
            meta_value: this.userForm.value.result
          }, {
            meta_key: 'english_mark',
            meta_value: this.userForm.value.english_mark
          });
          var formdata = [];
          formdata.push({
            meta_data: formdat
          });
          this.api.userjobupdate(formdata, this.token).subscribe(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var toast;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      this.loading2 = false;
                      console.log(res);
                      _context3.next = 4;
                      return this.toastCtrl.create({
                        message: 'Profile Updated',
                        duration: 3000
                      });

                    case 4:
                      toast = _context3.sent;
                      toast.present();
                      console.log('Profile Updated', res); // this.router.navigate(['/member-detail']);
                      // window.location.reload();

                    case 7:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }, function (err) {
            _this2.loading2 = false;
            console.log(err);

            _this2.showError(err);
          });
        }
      }, {
        key: "BackButton",
        value: function BackButton() {
          this.location.back();
        }
      }, {
        key: "showError",
        value: function showError(err) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var alert;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.alertCtrl.create({
                      header: err.error.code,
                      subHeader: err.error.data,
                      message: err.error.message,
                      buttons: ['OK']
                    });

                  case 2:
                    alert = _context4.sent;
                    _context4.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.nativePageTransitions.slide(this.options1);
        } // example of adding a transition when pushing a new page

      }, {
        key: "openPage",
        value: function openPage(page) {
          this.nativePageTransitions.slide(this.options1);
          this.navCtrl.navigateForward(page);
        }
      }, {
        key: "loadPrivatePosts",
        value: function loadPrivatePosts() {
          var _this3 = this;

          this.api.getPrivatePosts().subscribe(function (res) {
            _this3.posts = res;
          });
        }
      }, {
        key: "selectImageSource",
        value: function selectImageSource() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var _this4 = this;

            var buttons, actionSheet;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    buttons = [{
                      text: 'Take Photo',
                      icon: 'camera',
                      handler: function handler() {
                        _this4.androidPermissions.checkPermission(_this4.androidPermissions.PERMISSION.CAMERA).then(function (result) {
                          return console.log('Has permission?', result.hasPermission);
                        }, function (err) {
                          return _this4.androidPermissions.requestPermission(_this4.androidPermissions.PERMISSION.CAMERA);
                        });

                        _this4.androidPermissions.requestPermissions([_this4.androidPermissions.PERMISSION.CAMERA, _this4.androidPermissions.PERMISSION.GET_ACCOUNTS]); // this.addImage();

                      }
                    }, {
                      text: 'Choose From Photos Photo',
                      icon: 'image',
                      handler: function handler() {
                        _this4.androidPermissions.checkPermission(_this4.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                          return console.log('Has permission?', result.hasPermission);
                        }, function (err) {
                          return _this4.androidPermissions.requestPermission(_this4.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                        });

                        _this4.androidPermissions.requestPermissions([_this4.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, _this4.androidPermissions.PERMISSION.GET_ACCOUNTS]); // this.selectImage();

                      }
                    }]; // Only allow file selection inside a browser

                    if (!this.plt.is('hybrid')) {
                      buttons.push({
                        text: 'Choose a File',
                        icon: 'attach',
                        handler: function handler() {
                          _this4.androidPermissions.checkPermission(_this4.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                            return console.log('Has permission?', result.hasPermission);
                          }, function (err) {
                            return _this4.androidPermissions.requestPermission(_this4.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                          });

                          _this4.androidPermissions.requestPermissions([_this4.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, _this4.androidPermissions.PERMISSION.GET_ACCOUNTS]); // this.fileInput.nativeElement.click();

                        }
                      });
                    }

                    _context5.next = 4;
                    return this.actionSheetCtrl.create({
                      header: 'Select Image Source',
                      buttons: buttons
                    });

                  case 4:
                    actionSheet = _context5.sent;
                    _context5.next = 7;
                    return actionSheet.present();

                  case 7:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "goToprofile",
        value: function goToprofile() {
          this.router.navigate(['/member-detail']);
        }
      }, {
        key: "goToHome",
        value: function goToHome(res) {
          sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
            title: 'Success',
            text: 'Thank you for registrations',
            icon: 'success',
            backdrop: false
          });
          this.router.navigate(['/tabs/home-new']);
        }
      }]);

      return JobVerificationPage;
    }();

    JobVerificationPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"]
      }, {
        type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_11__["NativePageTransitions"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]
      }, {
        type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"]
      }, {
        type: _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"]
      }, {
        type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__["AndroidPermissions"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ActionSheetController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], JobVerificationPage.prototype, "fileInput", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput2', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], JobVerificationPage.prototype, "fileInput2", void 0);
    JobVerificationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-job-verification',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./job-verification.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-verification/job-verification.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./job-verification.page.scss */
      "./src/app/pages/job-verification/job-verification.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"], _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_11__["NativePageTransitions"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__["AndroidPermissions"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ActionSheetController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"]])], JobVerificationPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-job-verification-job-verification-module-es5.js.map
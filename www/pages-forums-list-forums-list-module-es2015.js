(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-forums-list-forums-list-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forums-list/forums-list.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forums-list/forums-list.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n      <img src=\"assets/imgs/user.png\" class=\"user_image\">\n      <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar>\n      <ion-icon name=\"chatbubbles\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\"></ion-icon>\n    </div>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n      <ion-button (click)=\"BackButton()\" slot=\"start\">\n        <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n      </ion-button>\n      <!-- <ion-buttons slot=\"start\"> -->\n        <!-- <ion-menu-button color=\"light\"></ion-menu-button> -->\n        <!-- <img src=\"assets/imgs/user.png\" class=\"user_image\" (click)=\"openMenu()\">\n      </ion-buttons> -->\n      <ion-title color=\"light\">Forums</ion-title>\n      <!-- <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar> -->\n      <!-- <ion-icon name=\"chatbubbles\" (click)=\"goToChatList()\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\"></ion-icon> -->\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n\n   \n     \n  \n\n    <div class=\"notification_div\"    *ngFor=\"let item of forums\" >\n   \n      <div class=\"flex_div\" [routerLink]=\"['/forum-detail', item.id]\" style=\"justify-content: flex-start;\">\n        <div class=\"back_image\" [ngStyle]= \"{'background-image': 'url('+ item.avatar +')'}\" ></div>\n        <div>\n          <ion-label class=\"header_lbl\">{{item.title}}\n          </ion-label>\n          <ion-label class=\"desc \">{{item.content}}\n          </ion-label>\n          <ion-label class=\"desc count\">{{item.count}} Discussions</ion-label>\n        </div>\n      </div>\n      <!-- <div class=\"btn_div\" *ngIf=\"item.btn_text != ''\">\n        <ion-button fill=\"outline\" size=\"small\">\n          {{item.btn_text}}\n        </ion-button>\n      </div> -->\n    </div>\n\n\n\n  </div>\n  <ion-infinite-scroll (ionInfinite)=\"loadData($event)\">\n    <ion-infinite-scroll-content\n    loadingSpinner=\"bubbles\"\n    loadingText=\"Loading more posts ...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/forums-list/forums-list-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/forums-list/forums-list-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: ForumsListPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForumsListPageRoutingModule", function() { return ForumsListPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _forums_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./forums-list.page */ "./src/app/pages/forums-list/forums-list.page.ts");




const routes = [
    {
        path: '',
        component: _forums_list_page__WEBPACK_IMPORTED_MODULE_3__["ForumsListPage"]
    }
];
let ForumsListPageRoutingModule = class ForumsListPageRoutingModule {
};
ForumsListPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ForumsListPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/forums-list/forums-list.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/forums-list/forums-list.module.ts ***!
  \*********************************************************/
/*! exports provided: ForumsListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForumsListPageModule", function() { return ForumsListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _forums_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./forums-list-routing.module */ "./src/app/pages/forums-list/forums-list-routing.module.ts");
/* harmony import */ var _forums_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./forums-list.page */ "./src/app/pages/forums-list/forums-list.page.ts");







let ForumsListPageModule = class ForumsListPageModule {
};
ForumsListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _forums_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["ForumsListPageRoutingModule"]
        ],
        declarations: [_forums_list_page__WEBPACK_IMPORTED_MODULE_6__["ForumsListPage"]]
    })
], ForumsListPageModule);



/***/ }),

/***/ "./src/app/pages/forums-list/forums-list.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/forums-list/forums-list.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\nion-card-header {\n  padding: 5px;\n}\n\nion-card-title {\n  font-size: 17px;\n}\n\nion-card-content {\n  font-size: 14px;\n}\n\n.count {\n  font-size: 13px;\n  padding: 5px;\n  color: var(--ion-color-main);\n}\n\nion-scroll[scrollX] {\n  white-space: nowrap;\n  height: 120px;\n  overflow: hidden;\n}\n\nion-scroll[scrollX] .scroll-item {\n  display: inline-block;\n}\n\nion-scroll[scrollX] .selectable-icon {\n  margin: 10px 20px 10px 20px;\n  color: red;\n  font-size: 100px;\n}\n\nion-scroll[scrollX] ion-avatar img {\n  margin: 10px;\n}\n\n.up_lbl {\n  padding-left: 16px;\n  padding-right: 16px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n  background: #e1e9ee;\n}\n\n.chips_div {\n  display: flex;\n  flex-direction: row;\n  overflow: scroll;\n  padding-left: 16px;\n  padding-right: 16px;\n  background: #e1e9ee;\n  padding-top: 10px;\n  padding-bottom: 20px;\n}\n\n.chips_div .chip {\n  background-color: white;\n  border-radius: 3px;\n  padding: 5px 20px;\n  margin-right: 10px;\n  white-space: nowrap;\n  color: var(--ion-color-main);\n  font-size: 12px;\n  font-weight: bold;\n}\n\n.slider_div {\n  background: #e1e9ee;\n  padding-top: 10px;\n}\n\n.slider_div ion-slides {\n  height: 200px;\n  margin-top: -35px;\n}\n\n.slider_div .slide_div {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  border: 1px solid lightgray;\n  background: white;\n  justify-content: center;\n}\n\n.slider_div .slide_div .up_div {\n  display: flex;\n  flex-direction: row;\n  padding: 2px;\n  width: 395px;\n}\n\n.slider_div .slide_div .img_div {\n  width: 40px;\n}\n\n.slider_div .slide_div .img_div .slider_img {\n  width: 30px;\n  height: 30px;\n  min-width: 30px;\n}\n\n.slider_div .slide_div .content_div {\n  margin-left: 10px;\n}\n\n.slider_div .slide_div .content_div .abs_lbl {\n  position: absolute;\n  left: 5px;\n  top: 0;\n  font-size: 12px;\n}\n\n.slider_div .slide_div .content_div .head_lbl {\n  display: block;\n  font-weight: 500;\n  text-align: left;\n  font-size: 14px;\n}\n\n.slider_div .slide_div .content_div .small_lbl {\n  color: #707070;\n  text-align: left;\n  display: block;\n  font-size: 14px;\n}\n\n.slider_div .slide_div .lower_div {\n  border-top: 1px solid lightgray;\n  padding: 3px;\n  display: flex;\n  width: 100%;\n  justify-content: space-around;\n}\n\n.slider_div .slide_div .lower_div ion-button {\n  color: var(--ion-color-main);\n  font-weight: 600;\n}\n\nion-scroll[scroll-avatar] {\n  height: 60px;\n}\n\n.notification_div {\n  padding: 15px 10px;\n  position: relative;\n  background: white;\n}\n\n.notification_div .time_div {\n  position: absolute;\n  right: 10px;\n  top: 5px;\n  color: gray;\n}\n\n.notification_div .flex_div {\n  display: flex;\n  width: 100%;\n  align-items: center;\n}\n\n.notification_div .flex_div .back_image {\n  height: 50px;\n  width: 50px;\n  min-width: 50px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.notification_div .flex_div .header_lbl {\n  font-weight: 600;\n  margin-left: 10px;\n  font-size: 14px;\n  width: 88%;\n}\n\n.notification_div .flex_div .desc {\n  margin-left: 10px;\n  font-size: 14px;\n  width: 88%;\n}\n\n.notification_div .btn_div {\n  padding-left: 60px;\n  margin-top: 5px;\n}\n\n.notification_div .btn_div ion-button {\n  font-weight: 600;\n  --border-radius: 3px;\n  --border-color: var(--ion-color-main);\n  --color: var(--ion-color-main);\n  font-size: 16px;\n}\n\n/* Hide ion-content scrollbar */\n\n::-webkit-scrollbar {\n  display: none;\n}\n\n.bck-orange {\n  background: #ff9800;\n}\n\n.bck-red {\n  background: #f4511e;\n}\n\n.bck-green {\n  background: #afb42b;\n}\n\n.bck-yellow {\n  background: #ffc400;\n}\n\n.bck-blue {\n  background: #64b5f6;\n}\n\n.bck-pink {\n  background: #f48fb1;\n}\n\n.md-layout .mbsc-scv-item {\n  color: #fff;\n  font-weight: bold;\n  text-align: center;\n}\n\n.md-layout .mbsc-scv-c {\n  margin: 10px 0;\n}\n\n.variable-1 {\n  width: 60px;\n  height: 60px;\n}\n\n.variable-2 {\n  width: 80px;\n  height: 80px;\n}\n\n.variable-3 {\n  width: 70px;\n  height: 70px;\n}\n\n.variable-4 {\n  width: 50px;\n  height: 50px;\n}\n\n.variable-5 {\n  width: 100px;\n  height: 100px;\n}\n\n.variable-6 {\n  width: 40px;\n  height: 40px;\n}\n\n.md-fixed .mbsc-scv-item {\n  height: 80px;\n  margin: 0 10px;\n}\n\n.md-variable .mbsc-scv-item {\n  margin: auto 10px;\n}\n\n.md-pages .mbsc-scv-item {\n  height: 80px;\n}\n\n.md-fullpage .mbsc-scv-item {\n  height: 630px;\n  font-size: 28px;\n}\n\n.header_div {\n  display: flex;\n  width: 100%;\n  justify-content: space-between;\n  align-items: center;\n  padding-left: 16px;\n  padding-right: 16px;\n  height: 40px;\n}\n\n.header_div .user_image {\n  height: 25px;\n  width: 25px;\n}\n\n.header_div ion-searchbar {\n  --background: white;\n  --placeholder-color: #707070;\n  --icon-color: #707070;\n  border-radius: 0px;\n  height: 30px;\n}\n\nion-content {\n  --background: #F5F5F5;\n}\n\n.blue_lbl {\n  color: var(--ion-color-main);\n  padding-top: 15px;\n  padding-bottom: 15px;\n  font-weight: 600;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div .head_lbl {\n  padding-top: 10px;\n  padding-bottom: 10px;\n}\n\n.main_content_div .white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n\n.main_content_div .white_div ion-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  right: 16px;\n  font-size: 20px;\n}\n\n.main_content_div .white_div ion-button {\n  color: var(--ion-color-main);\n  font-size: 600;\n}\n\n.main_content_div .flex_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  justify-content: space-between;\n  padding-top: 10px;\n  padding-bottom: 15px;\n  border-bottom: 1px solid lightgray;\n  background: white;\n}\n\n.main_content_div .flex_div .f_div {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding-left: 16px;\n}\n\n.main_content_div .flex_div .f_div .detail {\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  margin-left: 10px;\n  line-height: 20px;\n}\n\n.main_content_div .flex_div .f_div .detail .desc {\n  color: gray;\n}\n\n.main_content_div .flex_div .f_div .back_image {\n  height: 60px;\n  width: 60px;\n  min-width: 60px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.main_content_div .flex_div .f_div .time {\n  margin-left: 2px;\n}\n\n.main_content_div .flex_div .s_div {\n  display: flex;\n  align-items: center;\n  padding-right: 16px;\n}\n\n.main_content_div .flex_div .s_div ion-icon {\n  font-size: 40px;\n}\n\n.main_content_div .flex_div .s_div .cross {\n  color: gray;\n}\n\n.main_content_div .flex_div .s_div .check {\n  color: var(--ion-color-main);\n}\n\n.main_content_div .user_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n\n.main_content_div .user_div .col_div {\n  border: 1px solid lightgray;\n}\n\n.main_content_div .user_div ion-grid {\n  padding: 0;\n}\n\n.main_content_div .user_div .cover {\n  height: 50px;\n  width: 100%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  position: relative;\n}\n\n.main_content_div .user_div .cover .close {\n  font-size: 25px;\n  color: black;\n  position: absolute;\n  right: 5px;\n  top: 5px;\n}\n\n.main_content_div .user_div .back_image {\n  height: 75px;\n  width: 75px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  position: absolute;\n  left: 50%;\n  transform: translate(-50%);\n  top: 40%;\n  border: 3px solid white;\n}\n\n.main_content_div .user_div .username {\n  margin-top: 45px;\n  text-align: center;\n  text-align: center;\n}\n\n.main_content_div .user_div .detail {\n  margin-top: 5px;\n  color: gray;\n  text-align: center;\n  font-size: 14px;\n}\n\n.main_content_div .user_div .mutual_div {\n  display: flex;\n  justify-content: center;\n  margin-top: 8px;\n}\n\n.main_content_div .user_div .mutual_div .link {\n  width: 15px;\n  height: 15px;\n}\n\n.main_content_div .user_div .mutual_div .link_lbl {\n  font-size: 12px;\n  color: gray;\n  margin-left: 5px;\n}\n\n.main_content_div .user_div ion-button {\n  --border-radius: 3px;\n  color: var(--ion-color-main);\n  --border-color: var(--ion-color-main);\n  font-weight: 600px;\n  margin: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZm9ydW1zLWxpc3QvRDpcXFJhZ3V2YXJhbiBpbWFnZVxcZGlzZWxzaGlwL3NyY1xcYXBwXFxwYWdlc1xcZm9ydW1zLWxpc3RcXGZvcnVtcy1saXN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZm9ydW1zLWxpc3QvZm9ydW1zLWxpc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUNBQUE7QUNDSjs7QURDQTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUNFSjs7QURBQTtFQUNJLFlBQUE7QUNHSjs7QUREQTtFQUNJLGVBQUE7QUNJSjs7QURGQTtFQUNJLGVBQUE7QUNLSjs7QURIQTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsNEJBQUE7QUNNSjs7QURKQTtFQUNJLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0FDT0o7O0FETEk7RUFDRSxxQkFBQTtBQ09OOztBREpJO0VBQ0UsMkJBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUNNTjs7QURISTtFQUNFLFlBQUE7QUNLTjs7QURGRTtFQUNFLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNLSjs7QURIQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtBQ01KOztBREhJO0VBQ0ksdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDS1I7O0FEREE7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0FDSUo7O0FERkk7RUFDSSxhQUFBO0VBQ0EsaUJBQUE7QUNJUjs7QURESTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUVBLG1CQUFBO0VBQ0EsMkJBQUE7RUFFQSxpQkFBQTtFQUVBLHVCQUFBO0FDQVI7O0FERVE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQ0FaOztBREdRO0VBQ0ksV0FBQTtBQ0RaOztBREdZO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDRGhCOztBRElRO0VBQ0ksaUJBQUE7QUNGWjs7QURLWTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLE1BQUE7RUFDQSxlQUFBO0FDSGhCOztBREtZO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDSGhCOztBREtZO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNIaEI7O0FETVE7RUFDSSwrQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDZCQUFBO0FDSlo7O0FETVk7RUFDSSw0QkFBQTtFQUNBLGdCQUFBO0FDSmhCOztBRFNFO0VBQ0UsWUFBQTtBQ05KOztBRFFFO0VBQ0Usa0JBQUE7RUFFQSxrQkFBQTtFQUNBLGlCQUFBO0FDTko7O0FEUUk7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtBQ05SOztBRFFJO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQ05SOztBRE9RO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUNMWjs7QURRUTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtBQ05aOztBRFFRO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtBQ05aOztBRFVJO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0FDUlI7O0FEVVE7RUFDSSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EscUNBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7QUNSWjs7QURhRSwrQkFBQTs7QUFDQTtFQUNFLGFBQUE7QUNWSjs7QURZRTtFQUNFLG1CQUFBO0FDVEo7O0FEWUE7RUFDSSxtQkFBQTtBQ1RKOztBRFlBO0VBQ0ksbUJBQUE7QUNUSjs7QURZQTtFQUNJLG1CQUFBO0FDVEo7O0FEWUE7RUFDSSxtQkFBQTtBQ1RKOztBRFlBO0VBQ0ksbUJBQUE7QUNUSjs7QURZQTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDVEo7O0FEWUE7RUFDSSxjQUFBO0FDVEo7O0FEWUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ1RKOztBRFlBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNUSjs7QURZQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDVEo7O0FEWUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ1RKOztBRFlBO0VBQ0ksWUFBQTtFQUNBLGFBQUE7QUNUSjs7QURZQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDVEo7O0FEWUE7RUFDSSxZQUFBO0VBQ0EsY0FBQTtBQ1RKOztBRFlBO0VBQ0ksaUJBQUE7QUNUSjs7QURZQTtFQUNJLFlBQUE7QUNUSjs7QURZQTtFQUNJLGFBQUE7RUFDQSxlQUFBO0FDVEo7O0FEV0E7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ1JKOztBRFVJO0VBQ0ksWUFBQTtFQUNBLFdBQUE7QUNSUjs7QURVSTtFQUNJLG1CQUFBO0VBQ0EsNEJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ1JSOztBRFlBO0VBQ0kscUJBQUE7QUNUSjs7QURXQTtFQUNJLDRCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0FDUko7O0FEVUE7RUFDSSxXQUFBO0FDUEo7O0FEU0k7RUFDSSxjQUFBO0FDUFI7O0FEVUk7RUFDSSxpQkFBQTtFQUNBLG9CQUFBO0FDUlI7O0FEV0k7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ1RSOztBRHdCUTtFQUNJLDRCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQ3RCWjs7QUR5QlE7RUFDSSw0QkFBQTtFQUNBLGNBQUE7QUN2Qlo7O0FEMkJJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtDQUFBO0VBQ0EsaUJBQUE7QUN6QlI7O0FENEJRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQzFCWjs7QUQ0Qlk7RUFDSSwwQkFBQTtFQUFBLHVCQUFBO0VBQUEsa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FDMUJoQjs7QUQyQmdCO0VBQ0ksV0FBQTtBQ3pCcEI7O0FENEJZO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUMxQmhCOztBRDRCWTtFQUNJLGdCQUFBO0FDMUJoQjs7QUQrQlE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQzdCWjs7QUQ4Qlk7RUFDSSxlQUFBO0FDNUJoQjs7QUQrQlk7RUFDSSxXQUFBO0FDN0JoQjs7QUQrQlk7RUFDSSw0QkFBQTtBQzdCaEI7O0FEa0NJO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNoQ1I7O0FEa0NRO0VBQ0ksMkJBQUE7QUNoQ1o7O0FEbUNRO0VBQ0ksVUFBQTtBQ2pDWjs7QURvQ1E7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0FDbENaOztBRG9DWTtFQUNJLGVBQUE7RUFFQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtBQ25DaEI7O0FEc0NRO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLDBCQUFBO0VBQ0EsUUFBQTtFQUNBLHVCQUFBO0FDcENaOztBRHVDUTtFQUNJLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ3JDWjs7QUR1Q1E7RUFDSSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ3JDWjs7QUR3Q1E7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FDdENaOztBRHVDWTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDckNoQjs7QUR1Q1k7RUFDSSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDckNoQjs7QUR5Q1E7RUFDSSxvQkFBQTtFQUNBLDRCQUFBO0VBQ0EscUNBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUN2Q1oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9mb3J1bXMtbGlzdC9mb3J1bXMtbGlzdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaHtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5pb24tY2FyZC1oZWFkZXJ7XG4gICAgcGFkZGluZzogNXB4O1xufVxuaW9uLWNhcmQtdGl0bGV7XG4gICAgZm9udC1zaXplOiAxN3B4O1xufVxuaW9uLWNhcmQtY29udGVudHtcbiAgICBmb250LXNpemU6IDE0cHg7XG59XG4uY291bnR7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXNjcm9sbFtzY3JvbGxYXSB7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICBoZWlnaHQ6IDEyMHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG5cbiAgICAuc2Nyb2xsLWl0ZW0ge1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIH1cblxuICAgIC5zZWxlY3RhYmxlLWljb257XG4gICAgICBtYXJnaW46IDEwcHggMjBweCAxMHB4IDIwcHg7XG4gICAgICBjb2xvcjogcmVkO1xuICAgICAgZm9udC1zaXplOiAxMDBweDtcbiAgICB9XG5cbiAgICBpb24tYXZhdGFyIGltZ3tcbiAgICAgIG1hcmdpbjogMTBweDtcbiAgICB9XG4gIH1cbiAgLnVwX2xibHtcbiAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZDogI2UxZTllZTtcbn1cbi5jaGlwc19kaXZ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIG92ZXJmbG93OiBzY3JvbGw7XG4gICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgYmFja2dyb3VuZDogI2UxZTllZTtcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcblxuXG4gICAgLmNoaXB7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgIHBhZGRpbmc6IDVweCAyMHB4O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgfVxufVxuXG4uc2xpZGVyX2RpdntcbiAgICBiYWNrZ3JvdW5kOiAjZTFlOWVlOyBcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcblxuICAgIGlvbi1zbGlkZXN7XG4gICAgICAgIGhlaWdodDogMjAwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IC0zNXB4O1xuICAgIH1cblxuICAgIC5zbGlkZV9kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgLy8gcGFkZGluZzogMjBweCAxNXB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgIFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAgICAgICAudXBfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgICAgICBwYWRkaW5nOiAycHg7XG4gICAgICAgICAgICB3aWR0aDogMzk1cHg7XG4gICAgICAgIH1cblxuICAgICAgICAuaW1nX2RpdntcbiAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgLy8gcGFkZGluZzogMTVweDtcbiAgICAgICAgICAgIC5zbGlkZXJfaW1ne1xuICAgICAgICAgICAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgICAgICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgICAgICAgICBtaW4td2lkdGg6IDMwcHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmNvbnRlbnRfZGl2e1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAvLyBwYWRkaW5nOiAxNXB4O1xuXG4gICAgICAgICAgICAuYWJzX2xibHtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgbGVmdDogNXB4O1xuICAgICAgICAgICAgICAgIHRvcDogMDtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuaGVhZF9sYmx7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5zbWFsbF9sYmx7XG4gICAgICAgICAgICAgICAgY29sb3I6ICM3MDcwNzA7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmxvd2VyX2RpdntcbiAgICAgICAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgICAgICBwYWRkaW5nOiAzcHg7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcblxuICAgICAgICAgICAgaW9uLWJ1dHRvbntcbiAgICAgICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG4gIGlvbi1zY3JvbGxbc2Nyb2xsLWF2YXRhcl17XG4gICAgaGVpZ2h0OiA2MHB4O1xuICB9XG4gIC5ub3RpZmljYXRpb25fZGl2e1xuICAgIHBhZGRpbmc6IDE1cHggMTBweDtcbiAgIFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgXG4gICAgLnRpbWVfZGl2e1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHJpZ2h0OiAxMHB4O1xuICAgICAgICB0b3A6IDVweDtcbiAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgfVxuICAgIC5mbGV4X2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIC5iYWNrX2ltYWdle1xuICAgICAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICAgICAgICBtaW4td2lkdGg6IDUwcHg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgfVxuXG4gICAgICAgIC5oZWFkZXJfbGJse1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgd2lkdGg6IDg4JTtcbiAgICAgICAgfVxuICAgICAgICAuZGVzY3tcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgd2lkdGg6IDg4JTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5idG5fZGl2e1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDYwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDVweDtcblxuICAgICAgICBpb24tYnV0dG9ue1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICAgICAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgfVxuICAgIH1cbiAgICBcbn1cbiAgLyogSGlkZSBpb24tY29udGVudCBzY3JvbGxiYXIgKi9cbiAgOjotd2Via2l0LXNjcm9sbGJhcntcbiAgICBkaXNwbGF5Om5vbmU7XG4gIH1cbiAgLmJjay1vcmFuZ2Uge1xuICAgIGJhY2tncm91bmQ6ICNmZjk4MDA7XG59XG5cbi5iY2stcmVkIHtcbiAgICBiYWNrZ3JvdW5kOiAjZjQ1MTFlO1xufVxuXG4uYmNrLWdyZWVuIHtcbiAgICBiYWNrZ3JvdW5kOiAjYWZiNDJiO1xufVxuXG4uYmNrLXllbGxvdyB7XG4gICAgYmFja2dyb3VuZDogI2ZmYzQwMDtcbn1cblxuLmJjay1ibHVlIHtcbiAgICBiYWNrZ3JvdW5kOiAjNjRiNWY2O1xufVxuXG4uYmNrLXBpbmsge1xuICAgIGJhY2tncm91bmQ6ICNmNDhmYjE7XG59XG5cbi5tZC1sYXlvdXQgLm1ic2Mtc2N2LWl0ZW0ge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLm1kLWxheW91dCAubWJzYy1zY3YtYyB7XG4gICAgbWFyZ2luOiAxMHB4IDA7XG59XG5cbi52YXJpYWJsZS0xIHtcbiAgICB3aWR0aDogNjBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG59XG5cbi52YXJpYWJsZS0yIHtcbiAgICB3aWR0aDogODBweDtcbiAgICBoZWlnaHQ6IDgwcHg7XG59XG5cbi52YXJpYWJsZS0zIHtcbiAgICB3aWR0aDogNzBweDtcbiAgICBoZWlnaHQ6IDcwcHg7XG59XG5cbi52YXJpYWJsZS00IHtcbiAgICB3aWR0aDogNTBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG59XG5cbi52YXJpYWJsZS01IHtcbiAgICB3aWR0aDogMTAwcHg7XG4gICAgaGVpZ2h0OiAxMDBweDtcbn1cblxuLnZhcmlhYmxlLTYge1xuICAgIHdpZHRoOiA0MHB4O1xuICAgIGhlaWdodDogNDBweDtcbn1cblxuLm1kLWZpeGVkIC5tYnNjLXNjdi1pdGVtIHtcbiAgICBoZWlnaHQ6IDgwcHg7XG4gICAgbWFyZ2luOiAwIDEwcHg7XG59XG5cbi5tZC12YXJpYWJsZSAubWJzYy1zY3YtaXRlbSB7XG4gICAgbWFyZ2luOiBhdXRvIDEwcHg7XG59XG5cbi5tZC1wYWdlcyAubWJzYy1zY3YtaXRlbSB7XG4gICAgaGVpZ2h0OiA4MHB4O1xufVxuXG4ubWQtZnVsbHBhZ2UgLm1ic2Mtc2N2LWl0ZW0ge1xuICAgIGhlaWdodDogNjMwcHg7XG4gICAgZm9udC1zaXplOiAyOHB4O1xufVxuLmhlYWRlcl9kaXZ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgICAgXG4gICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xuXG4gICAgLnVzZXJfaW1hZ2V7XG4gICAgICAgIGhlaWdodDogMjVweDtcbiAgICAgICAgd2lkdGg6IDI1cHg7XG4gICAgfVxuICAgIGlvbi1zZWFyY2hiYXJ7XG4gICAgICAgIC0tYmFja2dyb3VuZDogd2hpdGU7ICAgXG4gICAgICAgIC0tcGxhY2Vob2xkZXItY29sb3I6ICAjNzA3MDcwOyBcbiAgICAgICAgLS1pY29uLWNvbG9yIDogIzcwNzA3MDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMHB4O1xuICAgICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgfVxufVxuXG5pb24tY29udGVudHtcbiAgICAtLWJhY2tncm91bmQ6ICNGNUY1RjU7XG59XG4uYmx1ZV9sYmx7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICBwYWRkaW5nLXRvcDogMTVweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXZ7XG4gICAgd2lkdGg6IDEwMCU7XG5cbiAgICBpb24tbGFiZWwge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG5cbiAgICAuaGVhZF9sYmx7XG4gICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgICB9XG5cbiAgICAud2hpdGVfZGl2e1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICAvLyBpb24tYnV0dG9ue1xuICAgICAgICAvLyAgICAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgICAgIC8vICAgICBpb24tbGFiZWwge1xuICAgICAgICAvLyAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIC8vICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIC8vICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAvLyAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIC8vICAgICB9ICAgXG4gICAgICAgIC8vICAgICBpb24taWNvbntcbiAgICAgICAgLy8gICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAvLyAgICAgfVxuICAgICAgICAvLyB9XG5cbiAgICAgICAgXG4gICAgICAgIGlvbi1pY29ue1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIHJpZ2h0OiAxNnB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICB9XG5cbiAgICAgICAgaW9uLWJ1dHRvbntcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBmb250LXNpemU6IDYwMDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5mbGV4X2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcblxuXG4gICAgICAgIC5mX2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTZweDtcblxuICAgICAgICAgICAgLmRldGFpbHtcbiAgICAgICAgICAgICAgICB3aWR0aDogbWF4LWNvbnRlbnQ7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICAgICAgICAgICAgLmRlc2N7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOmdyYXk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmJhY2tfaW1hZ2V7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA2MHB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiA2MHB4O1xuICAgICAgICAgICAgICAgIG1pbi13aWR0aDogNjBweDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC50aW1le1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAycHg7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAuc19kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgICAgICAgICBpb24taWNvbntcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDQwcHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5jcm9zc3tcbiAgICAgICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5jaGVja3tcbiAgICAgICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLnVzZXJfZGl2e1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuXG4gICAgICAgIC5jb2xfZGl2e1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICB9XG5cbiAgICAgICAgaW9uLWdyaWR7XG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICAuY292ZXJ7XG4gICAgICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICAgICAgICAgICAuY2xvc2V7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgICAgICAgICAgIC8vIGNvbG9yOiAjNTA1MDUwO1xuICAgICAgICAgICAgICAgIGNvbG9yOiBibGFjaztcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IDVweDtcbiAgICAgICAgICAgICAgICB0b3A6IDVweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuYmFja19pbWFnZXtcbiAgICAgICAgICAgIGhlaWdodDogNzVweDtcbiAgICAgICAgICAgIHdpZHRoOiA3NXB4O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICBsZWZ0OiA1MCU7XG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlKTtcbiAgICAgICAgICAgIHRvcDogNDAlO1xuICAgICAgICAgICAgYm9yZGVyOiAzcHggc29saWQgd2hpdGU7XG4gICAgICAgIH1cblxuICAgICAgICAudXNlcm5hbWV7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA0NXB4O1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICB9XG4gICAgICAgIC5kZXRhaWx7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5tdXR1YWxfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogOHB4O1xuICAgICAgICAgICAgLmxpbmt7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDE1cHg7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmxpbmtfbGJse1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaW9uLWJ1dHRvbntcbiAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIC0tYm9yZGVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwcHg7XG4gICAgICAgICAgICBtYXJnaW46IDEwcHg7XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG5pb24tY2FyZC1oZWFkZXIge1xuICBwYWRkaW5nOiA1cHg7XG59XG5cbmlvbi1jYXJkLXRpdGxlIHtcbiAgZm9udC1zaXplOiAxN3B4O1xufVxuXG5pb24tY2FyZC1jb250ZW50IHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uY291bnQge1xuICBmb250LXNpemU6IDEzcHg7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuaW9uLXNjcm9sbFtzY3JvbGxYXSB7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIGhlaWdodDogMTIwcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5pb24tc2Nyb2xsW3Njcm9sbFhdIC5zY3JvbGwtaXRlbSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cbmlvbi1zY3JvbGxbc2Nyb2xsWF0gLnNlbGVjdGFibGUtaWNvbiB7XG4gIG1hcmdpbjogMTBweCAyMHB4IDEwcHggMjBweDtcbiAgY29sb3I6IHJlZDtcbiAgZm9udC1zaXplOiAxMDBweDtcbn1cbmlvbi1zY3JvbGxbc2Nyb2xsWF0gaW9uLWF2YXRhciBpbWcge1xuICBtYXJnaW46IDEwcHg7XG59XG5cbi51cF9sYmwge1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYmFja2dyb3VuZDogI2UxZTllZTtcbn1cblxuLmNoaXBzX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIG92ZXJmbG93OiBzY3JvbGw7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgYmFja2dyb3VuZDogI2UxZTllZTtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xufVxuLmNoaXBzX2RpdiAuY2hpcCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIHBhZGRpbmc6IDVweCAyMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5zbGlkZXJfZGl2IHtcbiAgYmFja2dyb3VuZDogI2UxZTllZTtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uc2xpZGVyX2RpdiBpb24tc2xpZGVzIHtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgbWFyZ2luLXRvcDogLTM1cHg7XG59XG4uc2xpZGVyX2RpdiAuc2xpZGVfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4uc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC51cF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBwYWRkaW5nOiAycHg7XG4gIHdpZHRoOiAzOTVweDtcbn1cbi5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmltZ19kaXYge1xuICB3aWR0aDogNDBweDtcbn1cbi5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmltZ19kaXYgLnNsaWRlcl9pbWcge1xuICB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBtaW4td2lkdGg6IDMwcHg7XG59XG4uc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5jb250ZW50X2RpdiB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAuY29udGVudF9kaXYgLmFic19sYmwge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDVweDtcbiAgdG9wOiAwO1xuICBmb250LXNpemU6IDEycHg7XG59XG4uc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5jb250ZW50X2RpdiAuaGVhZF9sYmwge1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAuY29udGVudF9kaXYgLnNtYWxsX2xibCB7XG4gIGNvbG9yOiAjNzA3MDcwO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAubG93ZXJfZGl2IHtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgcGFkZGluZzogM3B4O1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG59XG4uc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5sb3dlcl9kaXYgaW9uLWJ1dHRvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbmlvbi1zY3JvbGxbc2Nyb2xsLWF2YXRhcl0ge1xuICBoZWlnaHQ6IDYwcHg7XG59XG5cbi5ub3RpZmljYXRpb25fZGl2IHtcbiAgcGFkZGluZzogMTVweCAxMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuLm5vdGlmaWNhdGlvbl9kaXYgLnRpbWVfZGl2IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTBweDtcbiAgdG9wOiA1cHg7XG4gIGNvbG9yOiBncmF5O1xufVxuLm5vdGlmaWNhdGlvbl9kaXYgLmZsZXhfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ubm90aWZpY2F0aW9uX2RpdiAuZmxleF9kaXYgLmJhY2tfaW1hZ2Uge1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiA1MHB4O1xuICBtaW4td2lkdGg6IDUwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuLm5vdGlmaWNhdGlvbl9kaXYgLmZsZXhfZGl2IC5oZWFkZXJfbGJsIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgd2lkdGg6IDg4JTtcbn1cbi5ub3RpZmljYXRpb25fZGl2IC5mbGV4X2RpdiAuZGVzYyB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIHdpZHRoOiA4OCU7XG59XG4ubm90aWZpY2F0aW9uX2RpdiAuYnRuX2RpdiB7XG4gIHBhZGRpbmctbGVmdDogNjBweDtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuLm5vdGlmaWNhdGlvbl9kaXYgLmJ0bl9kaXYgaW9uLWJ1dHRvbiB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn1cblxuLyogSGlkZSBpb24tY29udGVudCBzY3JvbGxiYXIgKi9cbjo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4uYmNrLW9yYW5nZSB7XG4gIGJhY2tncm91bmQ6ICNmZjk4MDA7XG59XG5cbi5iY2stcmVkIHtcbiAgYmFja2dyb3VuZDogI2Y0NTExZTtcbn1cblxuLmJjay1ncmVlbiB7XG4gIGJhY2tncm91bmQ6ICNhZmI0MmI7XG59XG5cbi5iY2steWVsbG93IHtcbiAgYmFja2dyb3VuZDogI2ZmYzQwMDtcbn1cblxuLmJjay1ibHVlIHtcbiAgYmFja2dyb3VuZDogIzY0YjVmNjtcbn1cblxuLmJjay1waW5rIHtcbiAgYmFja2dyb3VuZDogI2Y0OGZiMTtcbn1cblxuLm1kLWxheW91dCAubWJzYy1zY3YtaXRlbSB7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubWQtbGF5b3V0IC5tYnNjLXNjdi1jIHtcbiAgbWFyZ2luOiAxMHB4IDA7XG59XG5cbi52YXJpYWJsZS0xIHtcbiAgd2lkdGg6IDYwcHg7XG4gIGhlaWdodDogNjBweDtcbn1cblxuLnZhcmlhYmxlLTIge1xuICB3aWR0aDogODBweDtcbiAgaGVpZ2h0OiA4MHB4O1xufVxuXG4udmFyaWFibGUtMyB7XG4gIHdpZHRoOiA3MHB4O1xuICBoZWlnaHQ6IDcwcHg7XG59XG5cbi52YXJpYWJsZS00IHtcbiAgd2lkdGg6IDUwcHg7XG4gIGhlaWdodDogNTBweDtcbn1cblxuLnZhcmlhYmxlLTUge1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMTAwcHg7XG59XG5cbi52YXJpYWJsZS02IHtcbiAgd2lkdGg6IDQwcHg7XG4gIGhlaWdodDogNDBweDtcbn1cblxuLm1kLWZpeGVkIC5tYnNjLXNjdi1pdGVtIHtcbiAgaGVpZ2h0OiA4MHB4O1xuICBtYXJnaW46IDAgMTBweDtcbn1cblxuLm1kLXZhcmlhYmxlIC5tYnNjLXNjdi1pdGVtIHtcbiAgbWFyZ2luOiBhdXRvIDEwcHg7XG59XG5cbi5tZC1wYWdlcyAubWJzYy1zY3YtaXRlbSB7XG4gIGhlaWdodDogODBweDtcbn1cblxuLm1kLWZ1bGxwYWdlIC5tYnNjLXNjdi1pdGVtIHtcbiAgaGVpZ2h0OiA2MzBweDtcbiAgZm9udC1zaXplOiAyOHB4O1xufVxuXG4uaGVhZGVyX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgaGVpZ2h0OiA0MHB4O1xufVxuLmhlYWRlcl9kaXYgLnVzZXJfaW1hZ2Uge1xuICBoZWlnaHQ6IDI1cHg7XG4gIHdpZHRoOiAyNXB4O1xufVxuLmhlYWRlcl9kaXYgaW9uLXNlYXJjaGJhciB7XG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gIC0tcGxhY2Vob2xkZXItY29sb3I6ICM3MDcwNzA7XG4gIC0taWNvbi1jb2xvcjogIzcwNzA3MDtcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xuICBoZWlnaHQ6IDMwcHg7XG59XG5cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjRjVGNUY1O1xufVxuXG4uYmx1ZV9sYmwge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiBpb24tbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5oZWFkX2xibCB7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC53aGl0ZV9kaXYge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLndoaXRlX2RpdiBpb24taWNvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDE2cHg7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC53aGl0ZV9kaXYgaW9uLWJ1dHRvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtc2l6ZTogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgd2lkdGg6IDEwMCU7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuZl9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmZfZGl2IC5kZXRhaWwge1xuICB3aWR0aDogbWF4LWNvbnRlbnQ7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBsaW5lLWhlaWdodDogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuZl9kaXYgLmRldGFpbCAuZGVzYyB7XG4gIGNvbG9yOiBncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5mX2RpdiAuYmFja19pbWFnZSB7XG4gIGhlaWdodDogNjBweDtcbiAgd2lkdGg6IDYwcHg7XG4gIG1pbi13aWR0aDogNjBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmZfZGl2IC50aW1lIHtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuc19kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5zX2RpdiBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogNDBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuc19kaXYgLmNyb3NzIHtcbiAgY29sb3I6IGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLnNfZGl2IC5jaGVjayB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IC5jb2xfZGl2IHtcbiAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IGlvbi1ncmlkIHtcbiAgcGFkZGluZzogMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiAuY292ZXIge1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiAuY292ZXIgLmNsb3NlIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDVweDtcbiAgdG9wOiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgLmJhY2tfaW1hZ2Uge1xuICBoZWlnaHQ6IDc1cHg7XG4gIHdpZHRoOiA3NXB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUpO1xuICB0b3A6IDQwJTtcbiAgYm9yZGVyOiAzcHggc29saWQgd2hpdGU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgLnVzZXJuYW1lIHtcbiAgbWFyZ2luLXRvcDogNDVweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgLmRldGFpbCB7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgY29sb3I6IGdyYXk7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IC5tdXR1YWxfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDhweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiAubXV0dWFsX2RpdiAubGluayB7XG4gIHdpZHRoOiAxNXB4O1xuICBoZWlnaHQ6IDE1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgLm11dHVhbF9kaXYgLmxpbmtfbGJsIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogZ3JheTtcbiAgbWFyZ2luLWxlZnQ6IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiBpb24tYnV0dG9uIHtcbiAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIC0tYm9yZGVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtd2VpZ2h0OiA2MDBweDtcbiAgbWFyZ2luOiAxMHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/forums-list/forums-list.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/forums-list/forums-list.page.ts ***!
  \*******************************************************/
/*! exports provided: ForumsListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForumsListPage", function() { return ForumsListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/dummy-data.service */ "./src/app/services/dummy-data.service.ts");
/* harmony import */ var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/native-page-transitions/ngx */ "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/








let ForumsListPage = class ForumsListPage {
    constructor(location, navCtrl, toastCtrl, nativePageTransitions, router, storage, dummy, api, menuCtrl) {
        this.location = location;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.router = router;
        this.storage = storage;
        this.dummy = dummy;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.members = [];
        this.activities = [];
        this.groups = [];
        this.notifications = [];
        this.forums = [];
        this.users = [
            {
                img: 'assets/imgs/user.jpg',
                cover: 'assets/imgs/back1.jpg'
            },
            {
                img: 'assets/imgs/user2.jpg',
                cover: 'assets/imgs/back2.jpg'
            },
            {
                img: 'assets/imgs/user3.jpg',
                cover: 'assets/imgs/back3.jpg'
            },
            {
                img: 'assets/imgs/user4.jpg',
                cover: 'assets/imgs/back4.jpg'
            }
        ];
        this.load = false;
        this.showSlider = false;
        this.options = {
            direction: 'up',
            duration: 500,
            slowdownfactor: 3,
            slidePixels: 20,
            iosdelay: 100,
            androiddelay: 150,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 60
        };
        this.showtab = true;
        this.load = true;
        this.storage.get('COMPLETE_USER_INFO').then(result => {
            if (result != null) {
                console.log(result);
                this.token = result.token;
                this.user_id = result.user_id;
                console.log('user_id: ' + result.user_id);
                console.log('token: ' + result.token);
            }
        }).catch(e => {
            console.log('error: ' + e);
            // Handle errors here
        });
        this.loadMembers();
        this.dummyData = this.dummy.users;
    }
    ionViewWillLeave() {
        this.nativePageTransitions.slide(this.options);
    }
    BackButton() {
        this.location.back();
    }
    connectuser(id) {
        this.api.connectfrds(id, this.token, this.user_id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(res);
            var index = this.members.findIndex(data => data.id === id);
            var data = this.members.find(data => data.id === id);
            console.log(data);
            if (typeof (index) !== 'undefined') {
                data.friendship_status = "pending";
                this.members.splice(index, 1, data);
            }
            const toast = yield this.toastCtrl.create({
                message: "Friend Request Sent Successfully",
                duration: 3000
            });
            yield toast.present();
        }));
    }
    loadData(event) {
        const page = (Math.ceil(this.activities.length / 10)) + 1;
        this.api.getForums(page).subscribe((newPagePosts) => {
            this.forums.push(...newPagePosts);
            event.target.complete();
        }, err => {
            // there are no more posts available
            event.target.disabled = true;
        });
    }
    // example of adding a transition when pushing a new page
    openPage(page) {
        this.nativePageTransitions.slide(this.options);
        this.navCtrl.navigateForward(page);
    }
    loadMembers() {
        this.api.getForums().subscribe(res => {
            console.log(res);
            this.forums = res;
        });
        this.load = false;
    }
    ngOnInit() {
    }
    myHeaderFn(record, recordIndex, records) {
        if (recordIndex % 20 === 0) {
            return 'Header ' + recordIndex;
        }
        return null;
    }
    goToPeopleProfile() {
        this.router.navigate(['/people-profile']);
    }
    openMenu() {
        this.menuCtrl.open();
    }
    goToMemberslist() {
        this.router.navigate(['/members-list']);
    }
    goToGroupslist() {
        this.router.navigate(['/groups-list']);
    }
    sliderShow(value) {
        this.showSlider = value;
    }
    gotoInvitations() {
        this.router.navigate(['/invitations']);
    }
    gotoGroup() {
        this.router.navigate(['/group-detail']);
    }
    onSearchChange(event) {
    }
    goToChatList() {
        this.router.navigate(['/chatlist']);
    }
};
ForumsListPage.ctorParameters = () => [
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"] },
    { type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_6__["NativePageTransitions"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"] },
    { type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_5__["dummyDataService"] },
    { type: _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"] }
];
ForumsListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-forums-list',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./forums-list.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forums-list/forums-list.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./forums-list.page.scss */ "./src/app/pages/forums-list/forums-list.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"],
        _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_6__["NativePageTransitions"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"], src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_5__["dummyDataService"], _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"]])
], ForumsListPage);



/***/ }),

/***/ "./src/app/services/dummy-data.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/dummy-data.service.ts ***!
  \************************************************/
/*! exports provided: dummyDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dummyDataService", function() { return dummyDataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

let dummyDataService = class dummyDataService {
    constructor() {
        this.users = [
            {
                img: 'assets/imgs/user.jpg',
                cover: 'assets/imgs/back1.jpg',
                btn_text: 'VIEW JOBS'
            },
            {
                img: 'assets/imgs/user2.jpg',
                cover: 'assets/imgs/back2.jpg',
                btn_text: 'FOLLOW HASHTAG'
            },
            {
                img: 'assets/imgs/user3.jpg',
                cover: 'assets/imgs/back3.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user4.jpg',
                cover: 'assets/imgs/back4.jpg',
                btn_text: 'SEE ALL VIEW'
            },
            {
                img: 'assets/imgs/user.jpg',
                cover: 'assets/imgs/back1.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user2.jpg',
                cover: 'assets/imgs/back2.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user3.jpg',
                cover: 'assets/imgs/back3.jpg',
                btn_text: 'EXPAND YOUR NETWORK'
            },
            {
                img: 'assets/imgs/user4.jpg',
                cover: 'assets/imgs/back4.jpg',
                btn_text: 'SEE ALL VIEW'
            }
        ];
        this.jobs = [
            {
                img: 'assets/imgs/company/initappz.png',
                title: 'SQL Server',
                addr: 'Bhavnagar, Gujrat, India',
                time: '2 school alumni',
                status: '1 day'
            },
            {
                img: 'assets/imgs/company/google.png',
                title: 'Web Designer',
                addr: 'Vadodara, Gujrat, India',
                time: 'Be an early applicant',
                status: '2 days'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                title: 'Business Analyst',
                addr: 'Ahmedabad, Gujrat, India',
                time: 'Be an early applicant',
                status: '1 week'
            },
            {
                img: 'assets/imgs/company/initappz.png',
                title: 'PHP Developer',
                addr: 'Pune, Maharashtra, India',
                time: '2 school alumni',
                status: 'New'
            },
            {
                img: 'assets/imgs/company/google.png',
                title: 'Graphics Designer',
                addr: 'Bhavnagar, Gujrat, India',
                time: 'Be an early applicant',
                status: '1 day'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                title: 'Phython Developer',
                addr: 'Ahmedabad, Gujrat, India',
                time: '2 school alumni',
                status: '5 days'
            },
        ];
        this.company = [
            {
                img: 'assets/imgs/company/initappz.png',
                name: 'Initappz Shop'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                name: 'Microsoft'
            },
            {
                img: 'assets/imgs/company/google.png',
                name: 'Google'
            },
        ];
    }
};
dummyDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], dummyDataService);



/***/ })

}]);
//# sourceMappingURL=pages-forums-list-forums-list-module-es2015.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-my-network-my-network-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-network/my-network.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-network/my-network.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n      <img src=\"assets/imgs/user.png\" class=\"user_image\">\n      <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar>\n      <ion-icon name=\"chatbubbles\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\"></ion-icon>\n    </div>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n\n      <ion-buttons slot=\"start\">\n        <!-- <ion-menu-button color=\"light\"></ion-menu-button> -->\n        <img src=\"assets/imgs/user.png\" class=\"user_image\" (click)=\"openMenu()\">\n      </ion-buttons>\n      <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar>\n      <ion-icon name=\"chatbubbles\" (click)=\"goToChatList()\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\"></ion-icon>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n\n    <!-- <div class=\"white_div\" style=\"margin-top: -4px;\">\n      <ion-button (click)=\"onClick()\" fill=\"clear\" expand=\"block\" >\n        <ion-label>Manage my network</ion-label>\n         <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n         <ion-ripple-effect></ion-ripple-effect>\n      </ion-button>\n    </div> -->\n\n    <div class=\"white_div\" (click)=\"goToManageNetwork()\">\n      <ion-label class=\"blue_lbl\">Manage my network <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n      </ion-label>\n    </div>\n\n    <div class=\"white_div\" style=\"margin-bottom: 0px;\">\n      <ion-label class=\"blue_lbl\">Invitations <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n      </ion-label>\n    </div>\n\n    <div class=\"flex_div\" *ngFor=\"let item of (users|slice : 0:3)\">\n      <div class=\"f_div\">\n        <div class=\"back_image\" [style.backgroundImage]=\"'url( ' + item.img+ ' )'\"></div>\n        <div class=\"detail\">\n          <ion-label class=\"name\">John Doe</ion-label>\n          <ion-label class=\"desc\">At Initapps Shop</ion-label>\n        </div>\n      </div>\n      <div class=\"s_div\">\n        <ion-icon slot=\"icon-only\" name=\"close-circle-outline\" class=\"cross\"></ion-icon>\n        <ion-icon slot=\"icon-only\" name=\"checkmark-circle-outline\" class=\"check\"></ion-icon>\n      </div>\n    </div>\n    <div class=\"white_div\">\n      <ion-label class=\"blue_lbl\" style=\"text-align: center;\" (click)=\"gotoInvitations()\">SHOW MORE</ion-label>\n    </div>\n\n    <div class=\"user_div\" *ngFor=\"let item of [1,2,3]\">\n      <ion-label class=\"head_lbl\">People you nay know from GOVERNMENT ENVIRONMENT COLLEGE, BHAVNAGAR</ion-label>\n\n      <ion-grid fixed>\n        <ion-row>\n          <ion-col size=\"6\" *ngFor=\"let item of users\" (click)=\"goToPeopleProfile()\">\n            <div class=\"col_div\">\n              <div class=\"cover\" [style.backgroundImage]=\"'url( ' + item.cover+ ' )'\">\n                <ion-icon slot=\"start\" name=\"close-circle\" class=\"close\"></ion-icon>\n                <div class=\"back_image\" [style.backgroundImage]=\"'url( ' + item.img+ ' )'\"></div>\n              </div>\n\n              <ion-label class=\"username\">John Doe</ion-label>\n              <ion-label class=\"detail\">GEC, BHAVNAGAR</ion-label>\n\n              <div class=\"mutual_div\">\n                <img src=\"assets/imgs/link.png\" class=\"link\">\n                <ion-label class=\"link_lbl\">2 Mutual Connection</ion-label>\n              </div>\n\n              <ion-button expand=\"block\" fill=\"outline\" size=\"small\">\n                CONNECT\n              </ion-button>\n\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/my-network/my-network-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/my-network/my-network-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: MyNetworkPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyNetworkPageRoutingModule", function() { return MyNetworkPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _my_network_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./my-network.page */ "./src/app/pages/my-network/my-network.page.ts");




const routes = [
    {
        path: '',
        component: _my_network_page__WEBPACK_IMPORTED_MODULE_3__["MyNetworkPage"]
    }
];
let MyNetworkPageRoutingModule = class MyNetworkPageRoutingModule {
};
MyNetworkPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MyNetworkPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/my-network/my-network.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/my-network/my-network.module.ts ***!
  \*******************************************************/
/*! exports provided: MyNetworkPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyNetworkPageModule", function() { return MyNetworkPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _my_network_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-network-routing.module */ "./src/app/pages/my-network/my-network-routing.module.ts");
/* harmony import */ var _my_network_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./my-network.page */ "./src/app/pages/my-network/my-network.page.ts");







let MyNetworkPageModule = class MyNetworkPageModule {
};
MyNetworkPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _my_network_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyNetworkPageRoutingModule"]
        ],
        declarations: [_my_network_page__WEBPACK_IMPORTED_MODULE_6__["MyNetworkPage"]]
    })
], MyNetworkPageModule);



/***/ }),

/***/ "./src/app/pages/my-network/my-network.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/my-network/my-network.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\n.header_div {\n  display: flex;\n  width: 100%;\n  justify-content: space-between;\n  align-items: center;\n  padding-left: 16px;\n  padding-right: 16px;\n  height: 40px;\n}\n\n.header_div .user_image {\n  height: 25px;\n  width: 25px;\n}\n\n.header_div ion-searchbar {\n  --background: white;\n  --placeholder-color: #707070;\n  --icon-color: #707070;\n  border-radius: 0px;\n  height: 30px;\n}\n\nion-content {\n  --background: #F5F5F5;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div .head_lbl {\n  padding-top: 10px;\n  padding-bottom: 10px;\n}\n\n.main_content_div .white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n\n.main_content_div .white_div .blue_lbl {\n  color: var(--ion-color-main);\n  padding-top: 15px;\n  padding-bottom: 15px;\n  font-weight: 600;\n}\n\n.main_content_div .white_div ion-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  right: 16px;\n  font-size: 20px;\n}\n\n.main_content_div .white_div ion-button {\n  color: var(--ion-color-main);\n  font-size: 600;\n}\n\n.main_content_div .flex_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  justify-content: space-between;\n  padding-top: 10px;\n  padding-bottom: 15px;\n  border-bottom: 1px solid lightgray;\n  background: white;\n}\n\n.main_content_div .flex_div .f_div {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding-left: 16px;\n}\n\n.main_content_div .flex_div .f_div .detail {\n  margin-left: 10px;\n}\n\n.main_content_div .flex_div .f_div .detail .desc {\n  color: gray;\n}\n\n.main_content_div .flex_div .f_div .back_image {\n  height: 60px;\n  width: 60px;\n  min-width: 60px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.main_content_div .flex_div .s_div {\n  display: flex;\n  align-items: center;\n  padding-right: 16px;\n}\n\n.main_content_div .flex_div .s_div ion-icon {\n  font-size: 40px;\n}\n\n.main_content_div .flex_div .s_div .cross {\n  color: gray;\n}\n\n.main_content_div .flex_div .s_div .check {\n  color: var(--ion-color-main);\n}\n\n.main_content_div .user_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n\n.main_content_div .user_div .col_div {\n  border: 1px solid lightgray;\n}\n\n.main_content_div .user_div ion-grid {\n  padding: 0;\n}\n\n.main_content_div .user_div .cover {\n  height: 50px;\n  width: 100%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  position: relative;\n}\n\n.main_content_div .user_div .cover .close {\n  font-size: 25px;\n  color: black;\n  position: absolute;\n  right: 5px;\n  top: 5px;\n}\n\n.main_content_div .user_div .back_image {\n  height: 75px;\n  width: 75px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  position: absolute;\n  left: 50%;\n  transform: translate(-50%);\n  top: 40%;\n  border: 3px solid white;\n}\n\n.main_content_div .user_div .username {\n  margin-top: 45px;\n  text-align: center;\n  text-align: center;\n}\n\n.main_content_div .user_div .detail {\n  margin-top: 5px;\n  color: gray;\n  text-align: center;\n  font-size: 14px;\n}\n\n.main_content_div .user_div .mutual_div {\n  display: flex;\n  justify-content: center;\n  margin-top: 8px;\n}\n\n.main_content_div .user_div .mutual_div .link {\n  width: 15px;\n  height: 15px;\n}\n\n.main_content_div .user_div .mutual_div .link_lbl {\n  font-size: 12px;\n  color: gray;\n  margin-left: 5px;\n}\n\n.main_content_div .user_div ion-button {\n  --border-radius: 3px;\n  color: var(--ion-color-main);\n  --border-color: var(--ion-color-main);\n  font-weight: 600px;\n  margin: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbXktbmV0d29yay9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxteS1uZXR3b3JrXFxteS1uZXR3b3JrLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbXktbmV0d29yay9teS1uZXR3b3JrLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1DQUFBO0FDQ0o7O0FEQ0E7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0FDRUo7O0FEQUE7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ0dKOztBRERJO0VBQ0ksWUFBQTtFQUNBLFdBQUE7QUNHUjs7QURESTtFQUNJLG1CQUFBO0VBQ0EsNEJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ0dSOztBRENBO0VBQ0kscUJBQUE7QUNFSjs7QURDQTtFQUNJLFdBQUE7QUNFSjs7QURBSTtFQUNJLGNBQUE7QUNFUjs7QURDSTtFQUNJLGlCQUFBO0VBQ0Esb0JBQUE7QUNDUjs7QURFSTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDQVI7O0FEY1E7RUFDSSw0QkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtBQ1paOztBRGNRO0VBQ0ksNEJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FDWlo7O0FEZVE7RUFDSSw0QkFBQTtFQUNBLGNBQUE7QUNiWjs7QURpQkk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsOEJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0NBQUE7RUFDQSxpQkFBQTtBQ2ZSOztBRGtCUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUNoQlo7O0FEa0JZO0VBQ0ksaUJBQUE7QUNoQmhCOztBRGlCZ0I7RUFDSSxXQUFBO0FDZnBCOztBRGtCWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDaEJoQjs7QURvQlE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ2xCWjs7QURtQlk7RUFDSSxlQUFBO0FDakJoQjs7QURvQlk7RUFDSSxXQUFBO0FDbEJoQjs7QURvQlk7RUFDSSw0QkFBQTtBQ2xCaEI7O0FEdUJJO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNyQlI7O0FEdUJRO0VBQ0ksMkJBQUE7QUNyQlo7O0FEd0JRO0VBQ0ksVUFBQTtBQ3RCWjs7QUR5QlE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0FDdkJaOztBRHlCWTtFQUNJLGVBQUE7RUFFQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtBQ3hCaEI7O0FEMkJRO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLDBCQUFBO0VBQ0EsUUFBQTtFQUNBLHVCQUFBO0FDekJaOztBRDRCUTtFQUNJLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQzFCWjs7QUQ0QlE7RUFDSSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQzFCWjs7QUQ2QlE7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FDM0JaOztBRDRCWTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDMUJoQjs7QUQ0Qlk7RUFDSSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDMUJoQjs7QUQ4QlE7RUFDSSxvQkFBQTtFQUNBLDRCQUFBO0VBQ0EscUNBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUM1QloiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9teS1uZXR3b3JrL215LW5ldHdvcmsucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWh7XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuLmhlYWRlcl9kaXZ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgICAgXG4gICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xuXG4gICAgLnVzZXJfaW1hZ2V7XG4gICAgICAgIGhlaWdodDogMjVweDtcbiAgICAgICAgd2lkdGg6IDI1cHg7XG4gICAgfVxuICAgIGlvbi1zZWFyY2hiYXJ7XG4gICAgICAgIC0tYmFja2dyb3VuZDogd2hpdGU7ICAgXG4gICAgICAgIC0tcGxhY2Vob2xkZXItY29sb3I6ICAjNzA3MDcwOyBcbiAgICAgICAgLS1pY29uLWNvbG9yIDogIzcwNzA3MDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMHB4O1xuICAgICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgfVxufVxuXG5pb24tY29udGVudHtcbiAgICAtLWJhY2tncm91bmQ6ICNGNUY1RjU7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2e1xuICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuXG4gICAgLmhlYWRfbGJse1xuICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XG4gICAgfVxuXG4gICAgLndoaXRlX2RpdntcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMTZweDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgLy8gaW9uLWJ1dHRvbntcbiAgICAgICAgLy8gICAgIC0tYm9yZGVyLXJhZGl1czogMHB4O1xuICAgICAgICAvLyAgICAgaW9uLWxhYmVsIHtcbiAgICAgICAgLy8gICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgICAvLyAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAvLyAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgLy8gICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAvLyAgICAgfSAgIFxuICAgICAgICAvLyAgICAgaW9uLWljb257XG4gICAgICAgIC8vICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgLy8gICAgIH1cbiAgICAgICAgLy8gfVxuXG4gICAgICAgIC5ibHVlX2xibHtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTVweDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgfVxuICAgICAgICBpb24taWNvbntcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICByaWdodDogMTZweDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlvbi1idXR0b257XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgZm9udC1zaXplOiA2MDA7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuZmxleF9kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG5cblxuICAgICAgICAuZl9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG5cbiAgICAgICAgICAgIC5kZXRhaWx7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgLmRlc2N7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOmdyYXk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmJhY2tfaW1hZ2V7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA2MHB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiA2MHB4O1xuICAgICAgICAgICAgICAgIG1pbi13aWR0aDogNjBweDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5zX2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICAgICAgICAgIGlvbi1pY29ue1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogNDBweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmNyb3Nze1xuICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmNoZWNre1xuICAgICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAudXNlcl9kaXZ7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG5cbiAgICAgICAgLmNvbF9kaXZ7XG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgIH1cblxuICAgICAgICBpb24tZ3JpZHtcbiAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIC5jb3ZlcntcbiAgICAgICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgICAgICAgICAgIC5jbG9zZXtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDI1cHg7XG4gICAgICAgICAgICAgICAgLy8gY29sb3I6ICM1MDUwNTA7XG4gICAgICAgICAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICByaWdodDogNXB4O1xuICAgICAgICAgICAgICAgIHRvcDogNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5iYWNrX2ltYWdle1xuICAgICAgICAgICAgaGVpZ2h0OiA3NXB4O1xuICAgICAgICAgICAgd2lkdGg6IDc1cHg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIGxlZnQ6IDUwJTtcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUpO1xuICAgICAgICAgICAgdG9wOiA0MCU7XG4gICAgICAgICAgICBib3JkZXI6IDNweCBzb2xpZCB3aGl0ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIC51c2VybmFtZXtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDQ1cHg7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgICAgICAgLmRldGFpbHtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICB9XG5cbiAgICAgICAgLm11dHVhbF9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA4cHg7XG4gICAgICAgICAgICAubGlua3tcbiAgICAgICAgICAgICAgICB3aWR0aDogMTVweDtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDE1cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAubGlua19sYmx7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpb24tYnV0dG9ue1xuICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDBweDtcbiAgICAgICAgICAgIG1hcmdpbjogMTBweDtcbiAgICAgICAgfVxuICAgIH1cbn0iLCJpb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuXG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCB7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5cbi5oZWFkZXJfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICBoZWlnaHQ6IDQwcHg7XG59XG4uaGVhZGVyX2RpdiAudXNlcl9pbWFnZSB7XG4gIGhlaWdodDogMjVweDtcbiAgd2lkdGg6IDI1cHg7XG59XG4uaGVhZGVyX2RpdiBpb24tc2VhcmNoYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgLS1wbGFjZWhvbGRlci1jb2xvcjogIzcwNzA3MDtcbiAgLS1pY29uLWNvbG9yOiAjNzA3MDcwO1xuICBib3JkZXItcmFkaXVzOiAwcHg7XG4gIGhlaWdodDogMzBweDtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6ICNGNUY1RjU7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiBpb24tbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5oZWFkX2xibCB7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC53aGl0ZV9kaXYge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLndoaXRlX2RpdiAuYmx1ZV9sYmwge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAud2hpdGVfZGl2IGlvbi1pY29uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTZweDtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLndoaXRlX2RpdiBpb24tYnV0dG9uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC1zaXplOiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICB3aWR0aDogMTAwJTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5mX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuZl9kaXYgLmRldGFpbCB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5mX2RpdiAuZGV0YWlsIC5kZXNjIHtcbiAgY29sb3I6IGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmZfZGl2IC5iYWNrX2ltYWdlIHtcbiAgaGVpZ2h0OiA2MHB4O1xuICB3aWR0aDogNjBweDtcbiAgbWluLXdpZHRoOiA2MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuc19kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5zX2RpdiBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogNDBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuc19kaXYgLmNyb3NzIHtcbiAgY29sb3I6IGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLnNfZGl2IC5jaGVjayB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IC5jb2xfZGl2IHtcbiAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IGlvbi1ncmlkIHtcbiAgcGFkZGluZzogMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiAuY292ZXIge1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiAuY292ZXIgLmNsb3NlIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDVweDtcbiAgdG9wOiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgLmJhY2tfaW1hZ2Uge1xuICBoZWlnaHQ6IDc1cHg7XG4gIHdpZHRoOiA3NXB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUpO1xuICB0b3A6IDQwJTtcbiAgYm9yZGVyOiAzcHggc29saWQgd2hpdGU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgLnVzZXJuYW1lIHtcbiAgbWFyZ2luLXRvcDogNDVweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgLmRldGFpbCB7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgY29sb3I6IGdyYXk7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IC5tdXR1YWxfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDhweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiAubXV0dWFsX2RpdiAubGluayB7XG4gIHdpZHRoOiAxNXB4O1xuICBoZWlnaHQ6IDE1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgLm11dHVhbF9kaXYgLmxpbmtfbGJsIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogZ3JheTtcbiAgbWFyZ2luLWxlZnQ6IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiBpb24tYnV0dG9uIHtcbiAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIC0tYm9yZGVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtd2VpZ2h0OiA2MDBweDtcbiAgbWFyZ2luOiAxMHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/my-network/my-network.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/my-network/my-network.page.ts ***!
  \*****************************************************/
/*! exports provided: MyNetworkPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyNetworkPage", function() { return MyNetworkPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/



let MyNetworkPage = class MyNetworkPage {
    constructor(router, menuCtrl) {
        this.router = router;
        this.menuCtrl = menuCtrl;
        this.users = [
            {
                img: 'assets/imgs/user.jpg',
                cover: 'assets/imgs/back1.jpg'
            },
            {
                img: 'assets/imgs/user2.jpg',
                cover: 'assets/imgs/back2.jpg'
            },
            {
                img: 'assets/imgs/user3.jpg',
                cover: 'assets/imgs/back3.jpg'
            },
            {
                img: 'assets/imgs/user4.jpg',
                cover: 'assets/imgs/back4.jpg'
            }
        ];
    }
    ngOnInit() {
    }
    goToPeopleProfile() {
        this.router.navigate(['/people-profile']);
    }
    openMenu() {
        this.menuCtrl.open();
    }
    goToManageNetwork() {
        this.router.navigate(['/manage-network']);
    }
    gotoInvitations() {
        this.router.navigate(['/invitations']);
    }
    onSearchChange(event) {
    }
    goToChatList() {
        this.router.navigate(['/chatlist']);
    }
};
MyNetworkPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] }
];
MyNetworkPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-my-network',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./my-network.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-network/my-network.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./my-network.page.scss */ "./src/app/pages/my-network/my-network.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]])
], MyNetworkPage);



/***/ })

}]);
//# sourceMappingURL=pages-my-network-my-network-module-es2015.js.map
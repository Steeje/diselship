function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-my-profile-my-profile-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-profile/my-profile.page.html":
  /*!*********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-profile/my-profile.page.html ***!
    \*********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesMyProfileMyProfilePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button mode='md' color=\"light\"></ion-back-button>\n    </ion-buttons>\n    <h3 style=\"color: #ffffff;\">Profile</h3>\n    <!-- <ion-searchbar></ion-searchbar> -->\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n    <div class=\"cover\" [style.backgroundImage]=\"'url(assets/imgs/back1.jpg)'\">\n      <div class=\"user_image\" [style.backgroundImage]=\"'url(' + data.avatar + ')'\" ></div>\n    </div>\n\n    <div class=\"personal_info\">\n      <ion-label class=\"username\">{{data.name}} <span style=\"color: #505050;\">•</span><span style=\"color: #505050;font-size: 14px;\"> {{data.nickname}}</span></ion-label>\n      <ion-label>{{data.email}}</ion-label>\n      <ion-label>About • <span style=\"font-size: 14px;\">{{data.description}}</span></ion-label>\n      <!-- <ion-label>Bhavnagar, Gujrat India • <span style=\"color: var(--ion-color-main); font-weight: 600\"> 20 Connections</span></ion-label> -->\n\n      <ion-button size=\"small\" (click)=\"editProfile()\"  class=\"msg_btn\" >\n        Edit Profile\n      </ion-button>\n\n      <ion-button size=\"small\" class=\"more_btn\"  fill=\"outline\">\n        MORE...\n      </ion-button>\n    </div>\n    <div class=\"highlight_div\">\n    <div class=\"desc_div\">\n      <div class=\"flex_div\">\n        <div class=\"content_div\">\n          <ion-label class=\"head_lbl ion-text-center\">500</ion-label>\n          <ion-label class=\"small_lbl ion-text-center\">Posts</ion-label>\n        </div>\n        <div class=\"content_div\">\n          <ion-label class=\"head_lbl ion-text-center\">300</ion-label>\n          <ion-label class=\"small_lbl ion-text-center\">Followers</ion-label>\n        </div>\n       \n          <div class=\"content_div\">\n            <ion-label class=\"head_lbl ion-text-center\">120</ion-label>\n            <ion-label class=\"small_lbl ion-text-center\">Following</ion-label>\n          </div>\n        \n      </div>\n    </div>\n  </div>\n    <div class=\"highlight_div\">\n      <ion-label>Highlights</ion-label>\n\n      <div class=\"flex_div\">\n        <div class=\"multi_images\">\n          <div class=\"user_image2\" [style.backgroundImage]=\"'url(assets/imgs/user3.jpg)'\"></div>\n          <div class=\"user_image3\" [style.backgroundImage]=\"'url(assets/imgs/user2.jpg)'\"></div>\n        </div>\n        <div class=\"content_div\">\n          <ion-label class=\"head_lbl\">3 mutual connections</ion-label>\n          <ion-label class=\"light_lbl\">You and Jonh Both know Mike and 2 others</ion-label>\n        </div>\n      </div>\n\n      <div class=\"flex_div\">\n        <div class=\"multi_images\">\n          <div class=\"user_image\" [style.backgroundImage]=\"'url(assets/imgs/user3.jpg)'\"></div>\n        </div>\n        <div class=\"content_div\" style=\"border: none;padding-bottom: 0px;\">\n          <ion-label class=\"head_lbl\">You both worked at Initappz Shop</ion-label>\n          <ion-label class=\"light_lbl\">You both worked at Initappz from February 2019 to March 2020</ion-label>\n          <ion-button size=\"small\"   fill=\"outline\">\n            SAY HELLO\n          </ion-button>\n        </div>\n      </div>\n    </div>\n    <div class=\"about_div\">\n      <ion-label>About</ion-label>\n      <ion-label class=\"about_detail\">\n        Lorem Ipsum is simply dummy text of the printing and typesetting industry. \n        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...\n      </ion-label>\n      <ion-label class=\"see_more_lbl\">See more</ion-label>\n    </div>\n\n    <div class=\"highlight_div\">\n      <ion-label>Experience</ion-label>\n\n      <div class=\"flex_div\" *ngFor=\"let item of (company | slice : 0: 3);let i = index\">\n        <div class=\"multi_images\">\n          <div class=\"user_image\" [style.backgroundImage]=\"'url('+item.img+')'\"></div>\n        </div>\n        <div class=\"content_div\" [class.no_border]=\"i == 2\">\n          <ion-label class=\"head_lbl\">Software Engineer</ion-label>\n          <ion-label class=\"simple_lbl\">{{item.name}} • Full-time</ion-label>\n          <ion-label class=\"light_lbl\" style=\"font-size: 14px;\">Feb 2020 - Present • 2 mos</ion-label>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"highlight_div\">\n      <ion-label>Education</ion-label>\n\n      <div class=\"flex_div\" *ngFor=\"let item of [1,2,3];let i = index\">\n        <div class=\"multi_images\">\n          <div class=\"user_image\" [style.backgroundImage]=\"'url(assets/imgs/university.png)'\"></div>\n        </div>\n        <div class=\"content_div\" [class.no_border]=\"i == 2\">\n          <ion-label class=\"head_lbl\" style=\"font-size: 13px;\">GOVERNMENT ENGINEERING COLLEGE, BHAVNAGAR</ion-label>\n          <ion-label class=\"simple_lbl\">Bachelor's degree, Information Technology</ion-label>\n          <ion-label class=\"light_lbl\" style=\"font-size: 14px;\">2015 - 2018</ion-label>\n        </div>\n      </div>\n    </div>\n    <div class=\"white_div\">\n      <ion-label class=\"blue_lbl\" style=\"text-align: center;\">SHOW MORE</ion-label>\n    </div>\n\n    <div class=\"skill_div\">\n      <ion-label>Skills & Endorsements</ion-label>\n\n      <div *ngFor=\"let item of (users | slice : 0: 3); let i = index\" class=\"main_div\" [class.no_border]=\"i == 2\">\n        <div class=\"inner_div\">\n          <ion-icon name=\"add-circle-outline\"></ion-icon> <ion-label class=\"head_lbl\">BootStrap</ion-label> \n        </div>\n        <div class=\"inner_div\" style=\"padding-left: 25px;\">\n          <div class=\"user_image\" [style.backgroundImage]=\"'url('+item.img+')'\"></div> <ion-label class=\"small_lbl\">Endorsed by Mike (mutual Connection)</ion-label> \n        </div>\n      </div>\n    </div>\n    <div class=\"white_div\">\n      <ion-label class=\"blue_lbl\" style=\"text-align: center;\">SEE 5 MORE SKILLS</ion-label>\n    </div>\n\n    <div class=\"skill_div\">\n      <ion-label>Accomplishments</ion-label>\n\n        <div class=\"inner_div\" style=\"padding-top: 10px;\">\n          <ion-label class=\"blue_lbl\">3</ion-label> <ion-label class=\"blue_lbl\" style=\"margin-left: 20px;\">LANGUAGES</ion-label> \n        </div>  \n         <ion-label class=\"small_lbl\" style=\"padding-left: 30px;\">English</ion-label> \n    </div>\n\n    <div class=\"highlight_div\">\n      <ion-label>Contact</ion-label>\n\n      <div class=\"flex_div\">\n        <div class=\"multi_images\">\n          <img src=\"assets/imgs/linkedin.png\" class=\"image\">\n        </div>\n        <div class=\"content_div\">\n          <ion-label class=\"head_lbl\">Jonh's Profile</ion-label>\n          <ion-label class=\"blue_lbl\">https://www.linkedin.com/test</ion-label>\n        </div>\n      </div>\n\n      <div class=\"flex_div\">\n        <div class=\"multi_images\">\n          <img src=\"assets/imgs/email_2.png\" class=\"image\">\n        </div>\n        <div class=\"content_div\" style=\"border-bottom: none;\">\n          <ion-label class=\"head_lbl\">Email</ion-label>\n          <ion-label class=\"blue_lbl\">jonh@gmail.com</ion-label>\n        </div>\n      </div>\n\n    </div>\n\n    <div class=\"highlight_div\">\n      <ion-label>Interests</ion-label>\n\n      <div class=\"flex_div\" style=\"border-bottom: 1px solid lightgray;padding-top: 10px;padding-bottom: 10px;\">\n        <img src=\"{{item.img}}\" *ngFor=\"let item of company\" class=\"interest\">\n      </div>\n    \n      <ion-label style=\"padding-top: 10px;\">Causes Jonh cares about</ion-label>\n      <div class=\"flex_div\" style=\"padding-top: 10px;\">\n        <img src=\"assets/imgs/heart_2.png\" class=\"image\">\n        <ion-label style=\"margin-left: 15px;\">Health, Social Service</ion-label>\n      </div>\n\n    </div>\n    <div class=\"white_div\">\n      <ion-label class=\"blue_lbl\" style=\"text-align: center;\">SEE ALL</ion-label>\n    </div>\n\n    <div class=\"people_div\">\n      <ion-label style=\"padding-bottom: 10px;\">People also viewed</ion-label>\n\n      <div class=\"flex_div\" *ngFor=\"let item of (users | slice : 0: 3);let i = index\">\n        <div class=\"user_image\" [style.backgroundImage]=\"'url('+item.img+')'\"></div>\n        <div class=\"content_div\" [class.no_border]=\"i == 2\">\n          <ion-label class=\"head_lbl\">Jonh Doe • <span style=\"font-weight: normal\">3rd</span></ion-label>\n          <ion-label class=\"simple_lbl\">Student at GEC-BHAVNAGAR</ion-label>\n        </div>\n      </div>\n    </div>\n\n\n  </div>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/pages/my-profile/my-profile-routing.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/pages/my-profile/my-profile-routing.module.ts ***!
    \***************************************************************/

  /*! exports provided: PeopleProfilePageRoutingModule */

  /***/
  function srcAppPagesMyProfileMyProfileRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PeopleProfilePageRoutingModule", function () {
      return PeopleProfilePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _my_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./my-profile.page */
    "./src/app/pages/my-profile/my-profile.page.ts");

    var routes = [{
      path: '',
      component: _my_profile_page__WEBPACK_IMPORTED_MODULE_3__["PeopleProfilePage"]
    }];

    var PeopleProfilePageRoutingModule = function PeopleProfilePageRoutingModule() {
      _classCallCheck(this, PeopleProfilePageRoutingModule);
    };

    PeopleProfilePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], PeopleProfilePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/my-profile/my-profile.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/my-profile/my-profile.module.ts ***!
    \*******************************************************/

  /*! exports provided: MyProfilePageModule */

  /***/
  function srcAppPagesMyProfileMyProfileModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyProfilePageModule", function () {
      return MyProfilePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _my_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./my-profile-routing.module */
    "./src/app/pages/my-profile/my-profile-routing.module.ts");
    /* harmony import */


    var _my_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./my-profile.page */
    "./src/app/pages/my-profile/my-profile.page.ts");

    var MyProfilePageModule = function MyProfilePageModule() {
      _classCallCheck(this, MyProfilePageModule);
    };

    MyProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _my_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["PeopleProfilePageRoutingModule"]],
      declarations: [_my_profile_page__WEBPACK_IMPORTED_MODULE_6__["PeopleProfilePage"]]
    })], MyProfilePageModule);
    /***/
  },

  /***/
  "./src/app/pages/my-profile/my-profile.page.scss":
  /*!*******************************************************!*\
    !*** ./src/app/pages/my-profile/my-profile.page.scss ***!
    \*******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesMyProfileMyProfilePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\nion-toolbar ion-input {\n  border-bottom: 1px solid white;\n  --color: white;\n  --placeholder-color: white;\n  --padding-start: 8px;\n}\nion-toolbar ion-icon {\n  color: white;\n  font-size: 24px;\n}\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\nion-content {\n  --background: #F5F5F5;\n}\n.main_content_div {\n  width: 100%;\n}\n.main_content_div .no_border {\n  border-bottom: none !important;\n}\n.main_content_div ion-label {\n  display: block;\n}\n.main_content_div .cover {\n  width: 100%;\n  height: 100px;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  position: relative;\n}\n.main_content_div .cover .user_image {\n  width: 100px;\n  height: 100px;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  border-radius: 50%;\n  border: 4px solid white;\n  position: absolute;\n  left: 16px;\n  top: 50%;\n}\n.main_content_div .personal_info {\n  padding: 16px;\n  padding-top: 55px;\n  background: white;\n}\n.main_content_div .personal_info ion-label {\n  margin-bottom: 7px;\n}\n.main_content_div .personal_info .username {\n  font-size: 20px;\n}\n.main_content_div .personal_info .msg_btn {\n  --background: var(--ion-color-main);\n  font-weight: 600;\n  --border-radius: 3px;\n  font-size: 18px;\n}\n.main_content_div .personal_info .more_btn {\n  color: gray;\n  --border-color: gray;\n  font-weight: 600;\n  --border-radius: 3px;\n  --border-width: 2px;\n  margin-left: 10px;\n  font-size: 18px;\n}\n.main_content_div .highlight_div {\n  margin-top: 10px;\n  padding: 16px;\n  background: white;\n  border-bottom: 1px solid lightgray;\n}\n.main_content_div .highlight_div .image {\n  width: 20px;\n}\n.main_content_div .highlight_div .flex_div {\n  display: flex;\n  align-items: flex-start;\n}\n.main_content_div .highlight_div .flex_div .interest {\n  width: 50px;\n  margin-right: 15px;\n}\n.main_content_div .highlight_div .flex_div .multi_images {\n  height: 45px;\n  width: 45px;\n  position: relative;\n  padding-top: 10px;\n}\n.main_content_div .highlight_div .flex_div .multi_images .user_image2 {\n  height: 25px;\n  width: 25px;\n  border-radius: 50%;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n}\n.main_content_div .highlight_div .flex_div .multi_images .user_image3 {\n  height: 25px;\n  width: 25px;\n  border-radius: 50%;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  position: absolute;\n  right: 5px;\n}\n.main_content_div .highlight_div .flex_div .multi_images .user_image {\n  height: 45px;\n  width: 45px;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  border-radius: 3px;\n}\n.main_content_div .highlight_div .flex_div .content_div {\n  padding-left: 10px;\n  border-bottom: 1px solid lightgray;\n  padding: 10px;\n  width: 100%;\n}\n.main_content_div .highlight_div .flex_div .content_div .head_lbl {\n  font-weight: 600;\n}\n.main_content_div .highlight_div .flex_div .content_div .light_lbl {\n  color: gray;\n}\n.main_content_div .highlight_div .flex_div .content_div .simple_lbl {\n  font-size: 14px;\n  color: #505050;\n}\n.main_content_div .highlight_div .flex_div .content_div .blue_lbl {\n  color: var(--ion-color-main);\n  font-size: 14px;\n}\n.main_content_div .highlight_div .flex_div .content_div ion-button {\n  color: var(--ion-color-main);\n  --border-color: var(--ion-color-main);\n  font-weight: 600;\n  --border-radius: 3px;\n  --border-width: 2px;\n  font-size: 16px;\n  margin-top: 5px;\n}\n.main_content_div .about_div {\n  margin-top: 10px;\n  background: white;\n  padding: 16px;\n}\n.main_content_div .about_div .about_detail {\n  font-size: 14px;\n  margin-top: 5px;\n  color: #505050;\n}\n.main_content_div .about_div .see_more_lbl {\n  text-align: right;\n  color: gray;\n  font-size: 14px;\n}\n.main_content_div .white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n.main_content_div .white_div .blue_lbl {\n  color: var(--ion-color-main);\n  padding-top: 15px;\n  padding-bottom: 15px;\n  font-weight: 600;\n}\n.main_content_div .skill_div {\n  margin-top: 10px;\n  padding: 16px;\n  background: white;\n  border-bottom: 1px solid lightgray;\n}\n.main_content_div .skill_div .main_div {\n  padding-top: 10px;\n  padding-bottom: 10px;\n  border-bottom: 1px solid lightgray;\n}\n.main_content_div .skill_div .inner_div {\n  display: flex;\n  align-items: center;\n}\n.main_content_div .skill_div .inner_div ion-icon {\n  font-size: 30px;\n  color: var(--ion-color-main);\n}\n.main_content_div .skill_div .inner_div .head_lbl {\n  font-weight: 600;\n  padding-left: 10px;\n}\n.main_content_div .skill_div .inner_div .user_image {\n  width: 25px;\n  height: 25px;\n  min-width: 25px;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  border-radius: 50%;\n}\n.main_content_div .skill_div .inner_div .blue_lbl {\n  color: var(--ion-color-main);\n  font-weight: 600;\n}\n.main_content_div .skill_div .small_lbl {\n  font-size: 14px;\n  padding-left: 10px;\n}\n.main_content_div .people_div {\n  padding: 16px;\n}\n.main_content_div .people_div .flex_div {\n  display: flex;\n  width: 100%;\n  align-items: center;\n}\n.main_content_div .people_div .flex_div .user_image {\n  height: 45px;\n  width: 45px;\n  min-width: 45px;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  border-radius: 50%;\n}\n.main_content_div .people_div .content_div {\n  padding-left: 10px;\n  border-bottom: 1px solid lightgray;\n  padding: 10px;\n  width: 100%;\n}\n.main_content_div .people_div .content_div .head_lbl {\n  font-weight: 600;\n  color: #505050;\n  font-size: 15px;\n}\n.main_content_div .people_div .content_div .simple_lbl {\n  font-size: 14px;\n  color: #505050;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbXktcHJvZmlsZS9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxteS1wcm9maWxlXFxteS1wcm9maWxlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbXktcHJvZmlsZS9teS1wcm9maWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1DQUFBO0FDQ0o7QURBSTtFQUNJLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0VBQ0Esb0JBQUE7QUNFUjtBREFJO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUNFUjtBRENBO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtBQ0VKO0FEQUE7RUFDSSxxQkFBQTtBQ0dKO0FEQUE7RUFDSSxXQUFBO0FDR0o7QURESTtFQUNJLDhCQUFBO0FDR1I7QURBSTtFQUNJLGNBQUE7QUNFUjtBRENJO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtBQ0NSO0FEQVE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0FDRVo7QURDSTtFQUNJLGFBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FDQ1I7QURDUTtFQUNJLGtCQUFBO0FDQ1o7QURFUTtFQUNJLGVBQUE7QUNBWjtBREdRO0VBQ0ksbUNBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZUFBQTtBQ0RaO0FER1E7RUFDSSxXQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNEWjtBREtJO0VBQ0ksZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQ0FBQTtBQ0hSO0FES1E7RUFDSSxXQUFBO0FDSFo7QURLUTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtBQ0haO0FES1k7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7QUNIaEI7QURNWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0poQjtBRFFnQjtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7QUNOcEI7QURRZ0I7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FDTnBCO0FEU2dCO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtBQ1BwQjtBRFdZO0VBQ0ksa0JBQUE7RUFDQSxrQ0FBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0FDVGhCO0FEV2dCO0VBQ0ksZ0JBQUE7QUNUcEI7QURXZ0I7RUFDSSxXQUFBO0FDVHBCO0FEV2dCO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUNUcEI7QURXZ0I7RUFDSSw0QkFBQTtFQUNBLGVBQUE7QUNUcEI7QURZZ0I7RUFDSSw0QkFBQTtFQUNBLHFDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7QUNWcEI7QURnQkk7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtBQ2RSO0FEZ0JRO0VBQ0ksZUFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDZFo7QURnQlE7RUFDSSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FDZFo7QURrQkk7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ2hCUjtBRGtCUTtFQUNJLDRCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0FDaEJaO0FEb0JJO0VBQ0ksZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQ0FBQTtBQ2xCUjtBRG9CUTtFQUNJLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQ0FBQTtBQ2xCWjtBRHFCUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtBQ25CWjtBRG9CWTtFQUNJLGVBQUE7RUFDQSw0QkFBQTtBQ2xCaEI7QURvQlk7RUFDSSxnQkFBQTtFQUNBLGtCQUFBO0FDbEJoQjtBRHFCWTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0FDbkJoQjtBRHNCWTtFQUNJLDRCQUFBO0VBQ0EsZ0JBQUE7QUNwQmhCO0FEeUJRO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0FDdkJaO0FEMkJJO0VBQ0ksYUFBQTtBQ3pCUjtBRDJCUTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7QUN6Qlo7QUQyQlk7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtBQ3pCaEI7QUQ0QlE7RUFDSSxrQkFBQTtFQUNBLGtDQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7QUMxQlo7QUQ0Qlk7RUFDSSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDMUJoQjtBRDRCWTtFQUNJLGVBQUE7RUFDQSxjQUFBO0FDMUJoQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL215LXByb2ZpbGUvbXktcHJvZmlsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgaW9uLWlucHV0e1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG4gICAgICAgIC0tY29sb3I6IHdoaXRlO1xuICAgICAgICAtLXBsYWNlaG9sZGVyLWNvbG9yIDogd2hpdGU7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydCA6IDhweDtcbiAgICB9XG4gICAgaW9uLWljb257XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xuICAgIH1cbn1cbmlvbi10b29sYmFyLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgsIGlvbi10b29sYmFyIC5zYy1pb24tc2VhcmNoYmFyLWlvcy1oe1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcbn1cbmlvbi1jb250ZW50e1xuICAgIC0tYmFja2dyb3VuZDogI0Y1RjVGNTtcbn1cblxuLm1haW5fY29udGVudF9kaXZ7XG4gICAgd2lkdGg6IDEwMCU7XG5cbiAgICAubm9fYm9yZGVye1xuICAgICAgICBib3JkZXItYm90dG9tOiBub25lICFpbXBvcnRhbnQ7XG4gICAgfVxuXG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuXG4gICAgLmNvdmVye1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAxMDBweDtcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIC51c2VyX2ltYWdle1xuICAgICAgICAgICAgd2lkdGg6IDEwMHB4O1xuICAgICAgICAgICAgaGVpZ2h0OiAxMDBweDtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgYm9yZGVyOiA0cHggc29saWQgd2hpdGU7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICBsZWZ0OiAxNnB4O1xuICAgICAgICAgICAgdG9wOiA1MCU7XG4gICAgICAgIH1cbiAgICB9XG4gICAgLnBlcnNvbmFsX2luZm97XG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgICAgIHBhZGRpbmctdG9wOiA1NXB4O1xuICAgICAgICBiYWNrZ3JvdW5kIDogd2hpdGU7XG5cbiAgICAgICAgaW9uLWxhYmVsIHtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDdweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC51c2VybmFtZXtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5tc2dfYnRue1xuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIH1cbiAgICAgICAgLm1vcmVfYnRue1xuICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgICAgICAtLWJvcmRlci1jb2xvcjogZ3JheTtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgICAgICAgICAgIC0tYm9yZGVyLXdpZHRoOiAycHg7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5oaWdobGlnaHRfZGl2e1xuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcblxuICAgICAgICAuaW1hZ2V7XG4gICAgICAgICAgICB3aWR0aDogMjBweDtcbiAgICAgICAgfVxuICAgICAgICAuZmxleF9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG5cbiAgICAgICAgICAgIC5pbnRlcmVzdHtcbiAgICAgICAgICAgICAgICB3aWR0aDogNTBweDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5tdWx0aV9pbWFnZXN7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA0NXB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiA0NXB4O1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgICAgICBwYWRkaW5nLXRvcCA6IDEwcHg7XG5cbiAgICAgICAgICAgICAgICBcblxuICAgICAgICAgICAgICAgIC51c2VyX2ltYWdlMntcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMjVweDtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC51c2VyX2ltYWdlM3tcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMjVweDtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDVweDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAudXNlcl9pbWFnZXtcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA0NXB4O1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogNDVweDtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAuY29udGVudF9kaXZ7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogMTBweDtcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcblxuICAgICAgICAgICAgICAgIC5oZWFkX2xibHtcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLmxpZ2h0X2xibHtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5zaW1wbGVfbGJse1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjNTA1MDUwO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAuYmx1ZV9sYmx7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpb24tYnV0dG9ue1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgICAgICAgICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgICAgICAgICAgICAgICAgICAgLS1ib3JkZXItd2lkdGg6IDJweDtcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmFib3V0X2RpdntcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XG5cbiAgICAgICAgLmFib3V0X2RldGFpbHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICAgICAgICAgIGNvbG9yOiAjNTA1MDUwO1xuICAgICAgICB9XG4gICAgICAgIC5zZWVfbW9yZV9sYmx7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLndoaXRlX2RpdntcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMTZweDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcblxuICAgICAgICAuYmx1ZV9sYmx7XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuc2tpbGxfZGl2e1xuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcblxuICAgICAgICAubWFpbl9kaXZ7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgLmlubmVyX2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyOyAgICBcbiAgICAgICAgICAgIGlvbi1pY29ue1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICAgICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmhlYWRfbGJse1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAudXNlcl9pbWFnZXtcbiAgICAgICAgICAgICAgICB3aWR0aDogMjVweDtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDI1cHg7XG4gICAgICAgICAgICAgICAgbWluLXdpZHRoOiAyNXB4O1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAuYmx1ZV9sYmx7XG4gICAgICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgIH1cblxuICAgICAgICAuc21hbGxfbGJse1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLnBlb3BsZV9kaXZ7XG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgICAgIFxuICAgICAgICAuZmxleF9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuXG4gICAgICAgICAgICAudXNlcl9pbWFnZXtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDQ1cHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDQ1cHg7XG4gICAgICAgICAgICAgICAgbWluLXdpZHRoOiA0NXB4O1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5jb250ZW50X2RpdntcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG5cbiAgICAgICAgICAgIC5oZWFkX2xibHtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgICAgIGNvbG9yOiAjNTA1MDUwO1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5zaW1wbGVfbGJse1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICBjb2xvcjogIzUwNTA1MDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0iLCJpb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXRvb2xiYXIgaW9uLWlucHV0IHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAtLWNvbG9yOiB3aGl0ZTtcbiAgLS1wbGFjZWhvbGRlci1jb2xvcjogd2hpdGU7XG4gIC0tcGFkZGluZy1zdGFydDogOHB4O1xufVxuaW9uLXRvb2xiYXIgaW9uLWljb24ge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMjRweDtcbn1cblxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogI0Y1RjVGNTtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5ub19ib3JkZXIge1xuICBib3JkZXItYm90dG9tOiBub25lICFpbXBvcnRhbnQ7XG59XG4ubWFpbl9jb250ZW50X2RpdiBpb24tbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHB4O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAudXNlcl9pbWFnZSB7XG4gIHdpZHRoOiAxMDBweDtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJvcmRlcjogNHB4IHNvbGlkIHdoaXRlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDE2cHg7XG4gIHRvcDogNTAlO1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2luZm8ge1xuICBwYWRkaW5nOiAxNnB4O1xuICBwYWRkaW5nLXRvcDogNTVweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfaW5mbyBpb24tbGFiZWwge1xuICBtYXJnaW4tYm90dG9tOiA3cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfaW5mbyAudXNlcm5hbWUge1xuICBmb250LXNpemU6IDIwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfaW5mbyAubXNnX2J0biB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogNjAwO1xuICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgZm9udC1zaXplOiAxOHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2luZm8gLm1vcmVfYnRuIHtcbiAgY29sb3I6IGdyYXk7XG4gIC0tYm9yZGVyLWNvbG9yOiBncmF5O1xuICBmb250LXdlaWdodDogNjAwO1xuICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgLS1ib3JkZXItd2lkdGg6IDJweDtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5oaWdobGlnaHRfZGl2IHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgcGFkZGluZzogMTZweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGlnaGxpZ2h0X2RpdiAuaW1hZ2Uge1xuICB3aWR0aDogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5oaWdobGlnaHRfZGl2IC5mbGV4X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xufVxuLm1haW5fY29udGVudF9kaXYgLmhpZ2hsaWdodF9kaXYgLmZsZXhfZGl2IC5pbnRlcmVzdCB7XG4gIHdpZHRoOiA1MHB4O1xuICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGlnaGxpZ2h0X2RpdiAuZmxleF9kaXYgLm11bHRpX2ltYWdlcyB7XG4gIGhlaWdodDogNDVweDtcbiAgd2lkdGg6IDQ1cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGlnaGxpZ2h0X2RpdiAuZmxleF9kaXYgLm11bHRpX2ltYWdlcyAudXNlcl9pbWFnZTIge1xuICBoZWlnaHQ6IDI1cHg7XG4gIHdpZHRoOiAyNXB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5oaWdobGlnaHRfZGl2IC5mbGV4X2RpdiAubXVsdGlfaW1hZ2VzIC51c2VyX2ltYWdlMyB7XG4gIGhlaWdodDogMjVweDtcbiAgd2lkdGg6IDI1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGlnaGxpZ2h0X2RpdiAuZmxleF9kaXYgLm11bHRpX2ltYWdlcyAudXNlcl9pbWFnZSB7XG4gIGhlaWdodDogNDVweDtcbiAgd2lkdGg6IDQ1cHg7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmhpZ2hsaWdodF9kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiB7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgcGFkZGluZzogMTBweDtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGlnaGxpZ2h0X2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IC5oZWFkX2xibCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGlnaGxpZ2h0X2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IC5saWdodF9sYmwge1xuICBjb2xvcjogZ3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5oaWdobGlnaHRfZGl2IC5mbGV4X2RpdiAuY29udGVudF9kaXYgLnNpbXBsZV9sYmwge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOiAjNTA1MDUwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmhpZ2hsaWdodF9kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiAuYmx1ZV9sYmwge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGlnaGxpZ2h0X2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IGlvbi1idXR0b24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogNjAwO1xuICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgLS1ib3JkZXItd2lkdGg6IDJweDtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuYWJvdXRfZGl2IHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDE2cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuYWJvdXRfZGl2IC5hYm91dF9kZXRhaWwge1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgY29sb3I6ICM1MDUwNTA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuYWJvdXRfZGl2IC5zZWVfbW9yZV9sYmwge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgY29sb3I6IGdyYXk7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC53aGl0ZV9kaXYge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLndoaXRlX2RpdiAuYmx1ZV9sYmwge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2tpbGxfZGl2IHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgcGFkZGluZzogMTZweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2tpbGxfZGl2IC5tYWluX2RpdiB7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5za2lsbF9kaXYgLmlubmVyX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2tpbGxfZGl2IC5pbm5lcl9kaXYgaW9uLWljb24ge1xuICBmb250LXNpemU6IDMwcHg7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2tpbGxfZGl2IC5pbm5lcl9kaXYgLmhlYWRfbGJsIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnNraWxsX2RpdiAuaW5uZXJfZGl2IC51c2VyX2ltYWdlIHtcbiAgd2lkdGg6IDI1cHg7XG4gIGhlaWdodDogMjVweDtcbiAgbWluLXdpZHRoOiAyNXB4O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5za2lsbF9kaXYgLmlubmVyX2RpdiAuYmx1ZV9sYmwge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLnNraWxsX2RpdiAuc21hbGxfbGJsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVvcGxlX2RpdiB7XG4gIHBhZGRpbmc6IDE2cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVvcGxlX2RpdiAuZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZW9wbGVfZGl2IC5mbGV4X2RpdiAudXNlcl9pbWFnZSB7XG4gIGhlaWdodDogNDVweDtcbiAgd2lkdGg6IDQ1cHg7XG4gIG1pbi13aWR0aDogNDVweDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVvcGxlX2RpdiAuY29udGVudF9kaXYge1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlb3BsZV9kaXYgLmNvbnRlbnRfZGl2IC5oZWFkX2xibCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjNTA1MDUwO1xuICBmb250LXNpemU6IDE1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVvcGxlX2RpdiAuY29udGVudF9kaXYgLnNpbXBsZV9sYmwge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOiAjNTA1MDUwO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/my-profile/my-profile.page.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/my-profile/my-profile.page.ts ***!
    \*****************************************************/

  /*! exports provided: PeopleProfilePage */

  /***/
  function srcAppPagesMyProfileMyProfilePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PeopleProfilePage", function () {
      return PeopleProfilePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/services/dummy-data.service */
    "./src/app/services/dummy-data.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic-native/native-page-transitions/ngx */
    "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var PeopleProfilePage = /*#__PURE__*/function () {
      function PeopleProfilePage(navCtrl, nativePageTransitions, dummy, storage, router) {
        var _this = this;

        _classCallCheck(this, PeopleProfilePage);

        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.dummy = dummy;
        this.storage = storage;
        this.router = router;
        this.data = {};
        this.options = {
          direction: 'up',
          duration: 500,
          slowdownfactor: 3,
          slidePixels: 20,
          iosdelay: 100,
          androiddelay: 150,
          fixedPixelsTop: 0,
          fixedPixelsBottom: 60
        };
        this.storage.get('USER_INFO').then(function (result) {
          if (result != null) {
            console.log(result);
            _this.data.id = result.id;
            _this.data.nickname = result.user_nicename;
            _this.data.name = result.first_name + " " + result.last_name;
            _this.data.description = result.description;
            _this.data.avatar = result.avatar_urls[24];
            _this.data.email = result.email;
            console.log(result.first_name);

            if (typeof result.first_name === "undefined") {
              _this.storage.get('COMPLETE_USER_INFO').then(function (result) {
                if (result != null) {
                  console.log(result);
                  _this.data.email = result.user_email;
                  _this.data.nickname = result.user_nicename;

                  if (typeof result.first_name !== "undefined") {
                    _this.data.name = result.first_name + " " + result.last_name;
                    _this.data.description = result.description;
                  } else {
                    _this.data.name = " ";
                    _this.data.description = "";
                  }
                }
              })["catch"](function (e) {
                console.log('error: ' + e); // Handle errors here
              });
            }
          }
        })["catch"](function (e) {
          console.log('error: ' + e); // Handle errors here
        });
      }

      _createClass(PeopleProfilePage, [{
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.nativePageTransitions.slide(this.options);
        } // example of adding a transition when pushing a new page

      }, {
        key: "openPage",
        value: function openPage(page) {
          this.nativePageTransitions.slide(this.options);
          this.navCtrl.navigateForward(page);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.users = this.dummy.users;
          this.company = this.dummy.company;
        }
      }, {
        key: "editProfile",
        value: function editProfile() {
          this.router.navigate(['/edit-profile']);
        }
      }]);

      return PeopleProfilePage;
    }();

    PeopleProfilePage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"]
      }, {
        type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__["NativePageTransitions"]
      }, {
        type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__["dummyDataService"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }];
    };

    PeopleProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-my-profile',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./my-profile.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-profile/my-profile.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./my-profile.page.scss */
      "./src/app/pages/my-profile/my-profile.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"], _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__["NativePageTransitions"], src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__["dummyDataService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])], PeopleProfilePage);
    /***/
  },

  /***/
  "./src/app/services/dummy-data.service.ts":
  /*!************************************************!*\
    !*** ./src/app/services/dummy-data.service.ts ***!
    \************************************************/

  /*! exports provided: dummyDataService */

  /***/
  function srcAppServicesdummyDataServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "dummyDataService", function () {
      return dummyDataService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var dummyDataService = function dummyDataService() {
      _classCallCheck(this, dummyDataService);

      this.users = [{
        img: 'assets/imgs/user.jpg',
        cover: 'assets/imgs/back1.jpg',
        btn_text: 'VIEW JOBS'
      }, {
        img: 'assets/imgs/user2.jpg',
        cover: 'assets/imgs/back2.jpg',
        btn_text: 'FOLLOW HASHTAG'
      }, {
        img: 'assets/imgs/user3.jpg',
        cover: 'assets/imgs/back3.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user4.jpg',
        cover: 'assets/imgs/back4.jpg',
        btn_text: 'SEE ALL VIEW'
      }, {
        img: 'assets/imgs/user.jpg',
        cover: 'assets/imgs/back1.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user2.jpg',
        cover: 'assets/imgs/back2.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user3.jpg',
        cover: 'assets/imgs/back3.jpg',
        btn_text: 'EXPAND YOUR NETWORK'
      }, {
        img: 'assets/imgs/user4.jpg',
        cover: 'assets/imgs/back4.jpg',
        btn_text: 'SEE ALL VIEW'
      }];
      this.jobs = [{
        img: 'assets/imgs/company/initappz.png',
        title: 'SQL Server',
        addr: 'Bhavnagar, Gujrat, India',
        time: '2 school alumni',
        status: '1 day'
      }, {
        img: 'assets/imgs/company/google.png',
        title: 'Web Designer',
        addr: 'Vadodara, Gujrat, India',
        time: 'Be an early applicant',
        status: '2 days'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        title: 'Business Analyst',
        addr: 'Ahmedabad, Gujrat, India',
        time: 'Be an early applicant',
        status: '1 week'
      }, {
        img: 'assets/imgs/company/initappz.png',
        title: 'PHP Developer',
        addr: 'Pune, Maharashtra, India',
        time: '2 school alumni',
        status: 'New'
      }, {
        img: 'assets/imgs/company/google.png',
        title: 'Graphics Designer',
        addr: 'Bhavnagar, Gujrat, India',
        time: 'Be an early applicant',
        status: '1 day'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        title: 'Phython Developer',
        addr: 'Ahmedabad, Gujrat, India',
        time: '2 school alumni',
        status: '5 days'
      }];
      this.company = [{
        img: 'assets/imgs/company/initappz.png',
        name: 'Initappz Shop'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        name: 'Microsoft'
      }, {
        img: 'assets/imgs/company/google.png',
        name: 'Google'
      }];
    };

    dummyDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], dummyDataService);
    /***/
  }
}]);
//# sourceMappingURL=pages-my-profile-my-profile-module-es5.js.map
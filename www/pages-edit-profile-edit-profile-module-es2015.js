(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-edit-profile-edit-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/edit-profile/edit-profile.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/edit-profile/edit-profile.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-button (click)=\"BackButton()\" slot=\"start\">\n      <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n    </ion-button>\n    <ion-title color=\"light\">Edit profile</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n   \n\n\n    <div class=\"form_div\">\n   \n\n\n       <ion-card id=\"content\" [style.backgroundImage]=\"'url('+data.cover+')'\">\n        <ion-label (click)=\"selectcoverImageSource()\" class=\"edit-btn\"> <ion-spinner style=\"color:white\" *ngIf=\"loading3\"></ion-spinner> <ion-icon *ngIf=\"edit_icon2\" name=\"create-outline\"></ion-icon></ion-label>\n        \n        <ion-label (click)=\"selectImageSource()\" class=\"edit-btn2\"><ion-spinner style=\"color:white\" *ngIf=\"loading\"></ion-spinner>  <ion-icon *ngIf=\"edit_icon\" name=\"create-outline\"></ion-icon></ion-label>\n          \n        <ion-avatar id=\"profile-info\">\n          <img id=\"profile-image\" src=\"{{data.avatar}}\">\n        </ion-avatar>\n        <input\n      type=\"file\"\n      #fileInput\n      (change)=\"uploadFile($event)\"\n      hidden=\"true\"\n      accept=\"image/*\"\n    />\n    <input\n    type=\"file\"\n    #fileInput2\n    (change)=\"uploadCoverFile($event)\"\n    hidden=\"true\"\n    accept=\"image/*\"\n  />\n      </ion-card>\n      <form [formGroup]=\"userForm\" (ngSubmit)=\"userupdate()\">\n      <ion-item>\n        <ion-label position=\"floating\">First Name</ion-label>\n        <ion-input  formControlName=\"firstname\" [(ngModel)]=\"data.firstname\" class=\"ip\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"floating\">Last Name</ion-label>\n        <ion-input  formControlName=\"lastname\" [(ngModel)]=\"data.lastname \" class=\"ip\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"floating\">Nick Name</ion-label>\n        <ion-input formControlName=\"nickname\" [(ngModel)]=\"data.nickname\" class=\"ip\"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label position=\"floating\">Email</ion-label>\n        <ion-input formControlName=\"email\" [(ngModel)]=\"data.email\" class=\"ip\"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label position=\"floating\">Password (6+ Character)</ion-label>\n        <ion-input type=\"password\"  formControlName=\"password\" class=\"ip\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"floating\">About Me</ion-label>\n        <ion-textarea formControlName=\"description\" [(ngModel)]=\"data.description\" class=\"ip\"></ion-textarea>\n      </ion-item>\n     \n\n      <ion-button class=\"join_now\"  type=\"submit\" expand=\"full\">\n         <ion-spinner *ngIf=\"loading2\"></ion-spinner> <ion-label >   Update </ion-label>\n      </ion-button>\n      </form>\n    </div>\n\n  \n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/edit-profile/edit-profile-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/edit-profile/edit-profile-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: EditProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePageRoutingModule", function() { return EditProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _edit_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./edit-profile.page */ "./src/app/pages/edit-profile/edit-profile.page.ts");




const routes = [
    {
        path: '',
        component: _edit_profile_page__WEBPACK_IMPORTED_MODULE_3__["EditProfilePage"]
    }
];
let EditProfilePageRoutingModule = class EditProfilePageRoutingModule {
};
EditProfilePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EditProfilePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/edit-profile/edit-profile.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/edit-profile/edit-profile.module.ts ***!
  \***********************************************************/
/*! exports provided: EditProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePageModule", function() { return EditProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit-profile-routing.module */ "./src/app/pages/edit-profile/edit-profile-routing.module.ts");
/* harmony import */ var _edit_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edit-profile.page */ "./src/app/pages/edit-profile/edit-profile.page.ts");







let EditProfilePageModule = class EditProfilePageModule {
};
EditProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditProfilePageRoutingModule"]
        ],
        declarations: [_edit_profile_page__WEBPACK_IMPORTED_MODULE_6__["EditProfilePage"]]
    })
], EditProfilePageModule);



/***/ }),

/***/ "./src/app/pages/edit-profile/edit-profile.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/edit-profile/edit-profile.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header_div {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n.header_div .logo_div {\n  display: flex;\n}\n.header_div .logo_div .header_lbl {\n  color: var(--ion-color-main);\n  font-weight: bold;\n  font-size: 18px;\n  margin-left: 10px;\n}\n.header_div .logo_div .logo_img {\n  margin-left: 1px;\n  width: 20px;\n  height: 20px;\n}\n.header_div ion-button {\n  color: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 14px;\n}\nion-toolbar {\n  --background: var(--ion-color-main);\n}\nion-toolbar ion-input {\n  border-bottom: 1px solid white;\n  --color: white;\n  --placeholder-color: white;\n  --padding-start: 8px;\n}\nion-toolbar ion-icon {\n  color: white;\n  font-size: 24px;\n}\n#content {\n  background-color: #ffffff;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n  height: 305px;\n  overflow: unset;\n  display: block;\n  border-radius: 15px;\n  background-size: cover;\n}\n.edit-btn {\n  font-size: 23px;\n  float: right;\n  margin: 10px;\n  color: white;\n  padding: 7px;\n  background: var(--ion-color-main);\n  border-radius: 10px;\n  position: absolute;\n  right: 0%;\n}\n.edit-btn2 {\n  font-size: 23px;\n  float: right;\n  margin: 10px;\n  color: white;\n  padding: 7px;\n  background: var(--ion-color-main);\n  border-radius: 10px;\n  position: absolute;\n  right: 28%;\n  z-index: 10;\n  top: 51%;\n}\n#profile-info {\n  width: 100%;\n  z-index: 2;\n  padding-top: 1px;\n  text-align: center;\n  position: absolute;\n  top: 45%;\n}\n#profile-image {\n  display: block;\n  border-radius: 120px;\n  border: 3px solid #fff;\n  width: 128px;\n  background: white;\n  height: 128px;\n  margin: 30px auto 0;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n}\n.main_content_div {\n  width: 100%;\n  padding: 20px;\n  height: 100%;\n}\n.main_content_div .header_lbl {\n  font-size: 26px;\n  font-weight: 600;\n}\n.main_content_div .join_btn {\n  --background: white;\n  --color: #0077B5;\n  border: 1px solid var(--ion-color-main);\n}\n.main_content_div .btn_search {\n  height: 25px;\n  width: 25px;\n  margin-right: 20px;\n}\n.main_content_div .or_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  height: 50px;\n  align-items: center;\n  justify-content: space-between;\n}\n.main_content_div .or_div .line_img {\n  height: 10px;\n  width: 40%;\n}\n.main_content_div .or_div .or_text {\n  color: black;\n}\n.main_content_div .form_div ion-item {\n  border-bottom: none;\n  --padding-start: 0px;\n}\n.main_content_div .form_div ion-item .ip {\n  border-bottom: 1px solid white;\n}\n.main_content_div .form_div .join_now {\n  margin-top: 20px;\n  border: 1px solid white;\n  --background: var(--ion-color-main);\n  font-weight: bold;\n  font-size: 18px;\n}\n.main_content_div .form_div .bottom_lbl {\n  display: block;\n  font-size: 14px;\n  margin-top: 20px;\n}\n.main_content_div .form_div .bottom_lbl span {\n  color: var(--ion-color-main) !important;\n  font-weight: bold;\n  color: gra;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZWRpdC1wcm9maWxlL0Q6XFxSYWd1dmFyYW4gaW1hZ2VcXGRpc2Vsc2hpcC9zcmNcXGFwcFxccGFnZXNcXGVkaXQtcHJvZmlsZVxcZWRpdC1wcm9maWxlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZWRpdC1wcm9maWxlL2VkaXQtcHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FDQ0o7QURBSTtFQUNJLGFBQUE7QUNFUjtBRERRO0VBQ0ksNEJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0daO0FERFE7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDR1o7QURBSTtFQUNJLDRCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDRVI7QURDQTtFQUNJLG1DQUFBO0FDRUo7QURESTtFQUNJLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0VBQ0Esb0JBQUE7QUNHUjtBRERJO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUNHUjtBREFBO0VBQ0kseUJBQUE7RUFDQSw0Q0FBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNHSjtBRERFO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxpQ0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0FDSUo7QURGQTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUNBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0FDS0o7QURIRTtFQUNFLFdBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBRUEsUUFBQTtBQ0tKO0FESEU7RUFDRSxjQUFBO0VBQ0Esb0JBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFFQSxtQkFBQTtFQUNBLDRDQUFBO0FDS0o7QUREQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQ0lKO0FERkk7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUNJUjtBREZJO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtFQUVBLHVDQUFBO0FDR1I7QURESTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUNHUjtBRERJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FDR1I7QUREUTtFQUNJLFlBQUE7RUFDQSxVQUFBO0FDR1o7QUREUTtFQUNJLFlBQUE7QUNHWjtBREVRO0VBQ0ksbUJBQUE7RUFDQSxvQkFBQTtBQ0FaO0FEQ1k7RUFDSSw4QkFBQTtBQ0NoQjtBREdRO0VBQ0ksZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1DQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDRFo7QURJUTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNGWjtBRElZO0VBQ0ksdUNBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7QUNGaEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9lZGl0LXByb2ZpbGUvZWRpdC1wcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXJfZGl2e1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAubG9nb19kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIC5oZWFkZXJfbGJse1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgIH1cbiAgICAgICAgLmxvZ29faW1ne1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDFweDtcbiAgICAgICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICAgICAgaGVpZ2h0OiAyMHB4O1xuICAgICAgICB9XG4gICAgfVxuICAgIGlvbi1idXR0b257XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICB9XG59XG5pb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgaW9uLWlucHV0e1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG4gICAgICAgIC0tY29sb3I6IHdoaXRlO1xuICAgICAgICAtLXBsYWNlaG9sZGVyLWNvbG9yIDogd2hpdGU7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydCA6IDhweDtcbiAgICB9XG4gICAgaW9uLWljb257XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xuICAgIH1cbn1cbiNjb250ZW50IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgIGJveC1zaGFkb3c6IDBweCAtMXB4IDEwcHggcmdiKDAgMCAwIC8gNDAlKTtcbiAgICBoZWlnaHQ6IDMwNXB4O1xuICAgIG92ZXJmbG93OiB1bnNldDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIH1cbiAgLmVkaXQtYnRue1xuICAgIGZvbnQtc2l6ZTogMjNweDtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgbWFyZ2luOiAxMHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBwYWRkaW5nOiA3cHg7XG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAwJTtcbn1cbi5lZGl0LWJ0bjJ7XG4gICAgZm9udC1zaXplOiAyM3B4O1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBtYXJnaW46IDEwcHg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHBhZGRpbmc6IDdweDtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDI4JTtcbiAgICB6LWluZGV4OiAxMDtcbiAgICB0b3A6IDUxJTtcbn1cbiAgI3Byb2ZpbGUtaW5mbyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgei1pbmRleDogMjtcbiAgICBwYWRkaW5nLXRvcDogMXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgXG4gICAgdG9wOiA0NSU7XG4gIH1cbiAgI3Byb2ZpbGUtaW1hZ2Uge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGJvcmRlci1yYWRpdXM6IDEyMHB4O1xuICAgIGJvcmRlcjogM3B4IHNvbGlkICNmZmY7XG4gICAgd2lkdGg6IDEyOHB4O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIGhlaWdodDogMTI4cHg7XG4gICBcbiAgICBtYXJnaW46IDMwcHggYXV0byAwO1xuICAgIGJveC1zaGFkb3c6IDBweCAtMXB4IDEwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAgLy8gYm94LXNoYWRvdzogMHB4IDBweCA0cHggcmdiYSgwLCAwLCAwLCAwLjcpO1xuICB9XG5cbi5tYWluX2NvbnRlbnRfZGl2e1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgaGVpZ2h0OiAxMDAlO1xuXG4gICAgLmhlYWRlcl9sYmx7XG4gICAgICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICB9XG4gICAgLmpvaW5fYnRue1xuICAgICAgICAtLWJhY2tncm91bmQgOiB3aGl0ZTtcbiAgICAgICAgLS1jb2xvcjogIzAwNzdCNTtcbiAgICAgICAgLy8gLS1ib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgfVxuICAgIC5idG5fc2VhcmNoe1xuICAgICAgICBoZWlnaHQ6IDI1cHg7XG4gICAgICAgIHdpZHRoOiAyNXB4O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG4gICAgfVxuICAgIC5vcl9kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcblxuICAgICAgICAubGluZV9pbWd7XG4gICAgICAgICAgICBoZWlnaHQ6IDEwcHg7XG4gICAgICAgICAgICB3aWR0aDogNDAlO1xuICAgICAgICB9XG4gICAgICAgIC5vcl90ZXh0e1xuICAgICAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmZvcm1fZGl2e1xuICAgICAgICBpb24taXRlbXtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IG5vbmU7XG4gICAgICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgICAgICAgICAgIC5pcHtcbiAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAuam9pbl9ub3d7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgd2hpdGU7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5ib3R0b21fbGJse1xuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuXG4gICAgICAgICAgICBzcGFue1xuICAgICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbikgIWltcG9ydGFudDtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgICAgICBjb2xvcjogZ3JhO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufSIsIi5oZWFkZXJfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmhlYWRlcl9kaXYgLmxvZ29fZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5oZWFkZXJfZGl2IC5sb2dvX2RpdiAuaGVhZGVyX2xibCB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE4cHg7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLmhlYWRlcl9kaXYgLmxvZ29fZGl2IC5sb2dvX2ltZyB7XG4gIG1hcmdpbi1sZWZ0OiAxcHg7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG59XG4uaGVhZGVyX2RpdiBpb24tYnV0dG9uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXRvb2xiYXIgaW9uLWlucHV0IHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAtLWNvbG9yOiB3aGl0ZTtcbiAgLS1wbGFjZWhvbGRlci1jb2xvcjogd2hpdGU7XG4gIC0tcGFkZGluZy1zdGFydDogOHB4O1xufVxuaW9uLXRvb2xiYXIgaW9uLWljb24ge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMjRweDtcbn1cblxuI2NvbnRlbnQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgaGVpZ2h0OiAzMDVweDtcbiAgb3ZlcmZsb3c6IHVuc2V0O1xuICBkaXNwbGF5OiBibG9jaztcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cblxuLmVkaXQtYnRuIHtcbiAgZm9udC1zaXplOiAyM3B4O1xuICBmbG9hdDogcmlnaHQ7XG4gIG1hcmdpbjogMTBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiA3cHg7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMCU7XG59XG5cbi5lZGl0LWJ0bjIge1xuICBmb250LXNpemU6IDIzcHg7XG4gIGZsb2F0OiByaWdodDtcbiAgbWFyZ2luOiAxMHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDdweDtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAyOCU7XG4gIHotaW5kZXg6IDEwO1xuICB0b3A6IDUxJTtcbn1cblxuI3Byb2ZpbGUtaW5mbyB7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAyO1xuICBwYWRkaW5nLXRvcDogMXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA0NSU7XG59XG5cbiNwcm9maWxlLWltYWdlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJvcmRlci1yYWRpdXM6IDEyMHB4O1xuICBib3JkZXI6IDNweCBzb2xpZCAjZmZmO1xuICB3aWR0aDogMTI4cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEyOHB4O1xuICBtYXJnaW46IDMwcHggYXV0byAwO1xuICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMjBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmhlYWRlcl9sYmwge1xuICBmb250LXNpemU6IDI2cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuam9pbl9idG4ge1xuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLWNvbG9yOiAjMDA3N0I1O1xuICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuYnRuX3NlYXJjaCB7XG4gIGhlaWdodDogMjVweDtcbiAgd2lkdGg6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5vcl9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA1MHB4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4ubWFpbl9jb250ZW50X2RpdiAub3JfZGl2IC5saW5lX2ltZyB7XG4gIGhlaWdodDogMTBweDtcbiAgd2lkdGg6IDQwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5vcl9kaXYgLm9yX3RleHQge1xuICBjb2xvcjogYmxhY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZm9ybV9kaXYgaW9uLWl0ZW0ge1xuICBib3JkZXItYm90dG9tOiBub25lO1xuICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mb3JtX2RpdiBpb24taXRlbSAuaXAge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZm9ybV9kaXYgLmpvaW5fbm93IHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgd2hpdGU7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxOHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZvcm1fZGl2IC5ib3R0b21fbGJsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mb3JtX2RpdiAuYm90dG9tX2xibCBzcGFuIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IGdyYTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/edit-profile/edit-profile.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/edit-profile/edit-profile.page.ts ***!
  \*********************************************************/
/*! exports provided: EditProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePage", function() { return EditProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/ngx/index.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/native-page-transitions/ngx */ "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
















let EditProfilePage = class EditProfilePage {
    constructor(navCtrl, nativePageTransitions, imagePicker, plt, location, file, router, api, camera, androidPermissions, loadingController, storage, fb, actionSheetCtrl, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.imagePicker = imagePicker;
        this.plt = plt;
        this.location = location;
        this.file = file;
        this.router = router;
        this.api = api;
        this.camera = camera;
        this.androidPermissions = androidPermissions;
        this.loadingController = loadingController;
        this.storage = storage;
        this.fb = fb;
        this.actionSheetCtrl = actionSheetCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.data = {};
        this.images = [];
        this.user = this.api.getCurrentUser();
        this.posts = [];
        this.options1 = {
            direction: 'up',
            duration: 500,
            slowdownfactor: 3,
            slidePixels: 20,
            iosdelay: 100,
            androiddelay: 150,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 60
        };
        this.edit_icon = true;
        this.edit_icon2 = true;
        this.user.subscribe(user => {
            if (user) {
                console.log(user);
            }
            else {
                this.posts = [];
            }
            this.storage.get('USER_INFO').then(result => {
                if (result != null) {
                    console.log(result);
                    this.data.id = result.id;
                    this.data.nickname = result.nickname;
                    if (typeof result.nickname === "undefined")
                        this.data.nickname = result.user_login;
                    this.data.firstname = result.first_name;
                    if (typeof result.first_name === "undefined")
                        this.data.firstname = result.xprofile.groups[1].fields[1].value.raw;
                    this.data.lastname = result.last_name;
                    if (typeof result.last_name === "undefined")
                        this.data.lastname = result.xprofile.groups[1].fields[2].value.raw;
                    this.data.description = result.description;
                    this.data.avatar = result.avatar_urls[24];
                    this.data.cover = result.cover_url;
                    console.log(result.avatar_urls[24]);
                    if (typeof result.avatar_urls[24] === "undefined")
                        this.data.avatar = result.avatar_urls['full'];
                    this.data.email = result.email;
                    if (typeof result.email === "undefined")
                        this.data.email = user.user_email;
                }
            }).catch(e => {
                console.log('error: ' + e);
                // Handle errors here
            });
        });
    }
    BackButton() {
        this.location.back();
    }
    ionViewWillLeave() {
        this.nativePageTransitions.slide(this.options1);
    }
    // example of adding a transition when pushing a new page
    openPage(page) {
        this.nativePageTransitions.slide(this.options1);
        this.navCtrl.navigateForward(page);
    }
    ngOnInit() {
        this.userForm = this.fb.group({
            nickname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            firstname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            lastname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            description: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]
        });
    }
    loadPrivatePosts() {
        this.api.getPrivatePosts().subscribe(res => {
            this.posts = res;
        });
    }
    // Used for browser direct file upload
    //   readFile(file) {
    //     console.log(file.type);
    //     const reader = new FileReader();
    //     reader.onload = () => {
    //       const blob = new Blob([reader.result], {
    //         type: file.type
    //       });
    //       this.api.uploadImage(blob, file.name, file.type,this.data.id).subscribe(  async (res : any)  => {
    //         console.log("---------------");
    //         console.log(res);
    //         this.loading=false;
    //         this.edit_icon=true;
    //   // Get the entire data
    //   this.storage.get('USER_INFO').then(valueStr => {
    //     let value = valueStr ? JSON.parse(valueStr) : {};
    //      // Modify just that property
    //      value.avatar_urls[24] = res.thumb;
    //      value.avatar_urls[96] = res.full;
    //      // Save the entire data again
    //      this.storage.set('USER_INFO', JSON.stringify(value));
    //      window.location.reload();
    //   });
    //       },
    //       err => {
    //         console.log(err);
    //         this.showError(err);
    //       }
    //       );
    //     };
    //     reader.readAsArrayBuffer(file);
    //   };
    //   async addImage2() {
    //     this.loading3=true;
    //     this.edit_icon2=false;
    //     const options: CameraOptions = {
    //       quality: 100,
    //       destinationType: this.camera.DestinationType.FILE_URI,
    //       encodingType: this.camera.EncodingType.JPEG,
    //       mediaType: this.camera.MediaType.PICTURE
    //     }
    //     this.camera.getPicture(options).then((imageData) => {
    //       this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => 
    //       {
    //         entry.file(file => {
    //           console.log(file);
    //           this.readFile2(file);
    //         });
    //       });
    //   }, (err) => {
    //     // Handle error
    //    });
    //   }
    //   readFile2(file) {
    //     console.log(file.type);
    //     const reader = new FileReader();
    //     reader.onload = () => {
    //       const blob = new Blob([reader.result], {
    //         type: file.type
    //       });
    //       this.api.uploadImage2(blob, file.name, file.type,this.data.id).subscribe(  async (res : any)  => {
    //         console.log("---------------");
    //         console.log(res);
    //         this.loading=false;
    //         this.edit_icon=true;
    //   // Get the entire data
    //   this.storage.get('USER_INFO').then(valueStr => {
    //     let value = valueStr ? JSON.parse(valueStr) : {};
    //      // Modify just that property
    //      value.avatar_urls[24] = res.thumb;
    //      value.avatar_urls[96] = res.full;
    //      // Save the entire data again
    //      this.storage.set('USER_INFO', JSON.stringify(value));
    //      window.location.reload();
    //   });
    //       },
    //       err => {
    //         console.log(err);
    //         this.showError(err);
    //       }
    //       );
    //     };
    //     reader.readAsArrayBuffer(file);
    //   };
    //   // Helper function
    //   // https://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
    //   b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    //     const byteCharacters = atob(b64Data);
    //     const byteArrays = [];
    //     for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    //       const slice = byteCharacters.slice(offset, offset + sliceSize);
    //       const byteNumbers = new Array(slice.length);
    //       for (let i = 0; i < slice.length; i++) {
    //         byteNumbers[i] = slice.charCodeAt(i);
    //       }
    //       const byteArray = new Uint8Array(byteNumbers);
    //       byteArrays.push(byteArray);
    //     }
    //     const blob = new Blob(byteArrays, { type: contentType });
    //     return blob;
    //   }
    //   async selectImageSource() {
    //     const buttons = [
    //       {
    //         text: 'Take Photo',
    //         icon: 'camera',
    //         handler: () => {
    //           this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
    //             result => console.log('Has permission?',result.hasPermission),
    //             err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
    //           );
    //           this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    //           this.addImage();
    //         }
    //       },
    //       {
    //         text: 'Choose From Photos Photo',
    //         icon: 'image',
    //         handler: () => {
    //           this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
    //             result => console.log('Has permission?',result.hasPermission),
    //             err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
    //           );
    //           this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    //           this.selectImage();
    //         }
    //       }
    //     ];
    //     // Only allow file selection inside a browser
    //     if (!this.plt.is('hybrid')) {
    //       buttons.push({
    //         text: 'Choose a File',
    //         icon: 'attach',
    //         handler: () => {
    //           this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
    //             result => console.log('Has permission?',result.hasPermission),
    //             err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
    //           );
    //           this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    //           this.fileInput.nativeElement.click();
    //         }
    //       });
    //     }
    //     const actionSheet = await this.actionSheetCtrl.create({
    //       header: 'Select Image Source',
    //       buttons
    //     });
    //     await actionSheet.present();
    //   }
    //   async selectcoverImageSource() {
    //     const buttons = [
    //       {
    //         text: 'Take Photo',
    //         icon: 'camera',
    //         handler: () => {
    //           this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
    //             result => console.log('Has permission?',result.hasPermission),
    //             err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
    //           );
    //           this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    //           this.addImage2();
    //         }
    //       },
    //       {
    //         text: 'Choose From Photos Photo',
    //         icon: 'image',
    //         handler: () => {
    //           this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
    //             result => console.log('Has permission?',result.hasPermission),
    //             err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
    //           );
    //           this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    //           this.selectImage2();
    //         }
    //       }
    //     ];
    //     // Only allow file selection inside a browser
    //     if (!this.plt.is('hybrid')) {
    //       buttons.push({
    //         text: 'Choose a File',
    //         icon: 'attach',
    //         handler: () => {
    //           this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
    //             result => console.log('Has permission?',result.hasPermission),
    //             err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
    //           );
    //           this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    //           this.fileInput2.nativeElement.click();
    //         }
    //       });
    //     }
    //     const actionSheet = await this.actionSheetCtrl.create({
    //       header: 'Select Image Source',
    //       buttons
    //     });
    //     await actionSheet.present();
    //   }
    //   async selectImage() {
    //     this.loading=true;
    //     this.edit_icon=false;
    //     this.options = {
    //       // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
    //       // selection of a single image, the plugin will return it.
    //       //maximumImagesCount: 3,
    //       // max width and height to allow the images to be.  Will keep aspect
    //       // ratio no matter what.  So if both are 800, the returned image
    //       // will be at most 800 pixels wide and 800 pixels tall.  If the width is
    //       // 800 and height 0 the image will be 800 pixels wide if the source
    //       // is at least that wide.
    //       width: 200,
    //       //height: 200,
    //       // quality of resized image, defaults to 100
    //       quality: 25,
    //       // output type, defaults to FILE_URIs.
    //       // available options are 
    //       // window.imagePicker.OutputType.FILE_URI (0) or 
    //       // window.imagePicker.OutputType.BASE64_STRING (1)
    //       outputType: 1
    //     };
    //     this.imageResponse = [];
    //     this.imagePicker.getPictures(this.options).then((results) => {
    //       for (var i = 0; i < results.length; i++) {
    //         this.file.resolveLocalFilesystemUrl(results[i]).then((entry: FileEntry) => 
    //       {
    //         entry.file(file => {
    //           console.log(file);
    //           this.readFile(file);
    //         });
    //       });
    //   } }, (err) => {
    //     alert(err);
    //   });
    //   }
    //   async selectImage2() {
    //     this.loading3=true;
    //     this.edit_icon2=false;
    //     this.options = {
    //       // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
    //       // selection of a single image, the plugin will return it.
    //       //maximumImagesCount: 3,
    //       // max width and height to allow the images to be.  Will keep aspect
    //       // ratio no matter what.  So if both are 800, the returned image
    //       // will be at most 800 pixels wide and 800 pixels tall.  If the width is
    //       // 800 and height 0 the image will be 800 pixels wide if the source
    //       // is at least that wide.
    //       width: 200,
    //       //height: 200,
    //       // quality of resized image, defaults to 100
    //       quality: 25,
    //       // output type, defaults to FILE_URIs.
    //       // available options are 
    //       // window.imagePicker.OutputType.FILE_URI (0) or 
    //       // window.imagePicker.OutputType.BASE64_STRING (1)
    //       outputType: 1
    //     };
    //     this.imageResponse = [];
    //     this.imagePicker.getPictures(this.options).then((results) => {
    //       for (var i = 0; i < results.length; i++) {
    //         this.file.resolveLocalFilesystemUrl(results[i]).then((entry: FileEntry) => 
    //       {
    //         entry.file(file => {
    //           console.log(file);
    //           this.readFile2(file);
    //         });
    //       });
    //   }
    // }, (err) => {
    //   alert(err);
    // });
    //   }
    //   goToprofile() {
    //     this.router.navigate(['/member-detail']);
    //   }
    //   goToHome(res) {
    //     swal.fire({
    //       title: 'Success',
    //       text: 'Thank you for registrations',
    //       icon: 'success',
    //       backdrop: false,
    //     });
    //     this.router.navigate(['/tabs/home-new']);
    //   }
    // //   uploadCoverFile(event: EventTarget) {
    // //     this.loading3=true;
    // //     this.edit_icon2=false;
    // //     const eventObj: MSInputMethodContext = event as MSInputMethodContext;
    // //     const target: HTMLInputElement = eventObj.target as HTMLInputElement;
    // //     const file: File = target.files[0];
    // //     this.api.uploadImageFile2(file,this.data.id).subscribe(
    // //       async (res : any) => {
    // //         this.loading3=false;
    // //         this.edit_icon2=true;
    // //       console.log("---------------");
    // //       console.log(res);
    // //       const toast = await this.toastCtrl.create({
    // //         message: 'Profile Cover Image Updated',
    // //         duration: 3000
    // //       });
    // //       toast.present();
    // // // Get the entire data
    // // this.storage.get('USER_INFO').then(valueStr => {
    // //   let value = valueStr ? valueStr : {};
    // //    // Modify just that property
    // //    value.cover_url = res.image;
    // // this.data.cover_url=res.image;
    // //    // Save the entire data again
    // //    this.storage.set('USER_INFO', value);
    // //     window.location.reload();
    // // });
    // //     },
    // //     err => {
    // //       this.loading3=false;
    // //       this.edit_icon2=true;
    // //       console.log(err);
    // //       this.showError(err);
    // //     }
    // //     );
    // //   }
    //   uploadFile(event: EventTarget) {
    //     this.loading=true;
    //     this.edit_icon=false;
    //     const eventObj: MSInputMethodContext = event as MSInputMethodContext;
    //     const target: HTMLInputElement = eventObj.target as HTMLInputElement;
    //     const file1 = target.files[0];
    //     this.api.uploadImageFile(file1,this.data.id).subscribe(
    //       async (res : any) => {
    //         this.loading=false;
    //         this.edit_icon=true;
    //       console.log("---------------");
    //       console.log(res);
    //       const toast = await this.toastCtrl.create({
    //         message: 'Profile Image Updated',
    //         duration: 3000
    //       });
    //       toast.present();
    // // Get the entire data
    // this.storage.get('USER_INFO').then(valueStr => {
    //   let value = valueStr ? valueStr : {};
    //    // Modify just that property
    //    value.avatar_urls[24] = res.thumb;
    //    value.avatar_urls[96] = res.full;
    // this.data.avatar=res.thumb;
    //    // Save the entire data again
    //    this.storage.set('USER_INFO', value);
    //    window.location.reload();
    // });
    //     },
    //     err => {
    //       this.loading=false;
    //       this.edit_icon=true;
    //       console.log(err);
    //       this.showError(err);
    //     }
    //     );
    //   }
    selectImage() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE));
            this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
            // this.loading=true;
            let options = {
                maximumImagesCount: 1,
                width: 200,
                quality: 25,
                outputType: 1
            };
            let imageResponse = [];
            this.imagePicker.getPictures(options).then((results) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                var imageData = results[0].toString();
                //  var imageData1 = imageData.toString();
                var byteCharacters = atob(imageData.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
                var byteNumbers = new Array(byteCharacters.length);
                for (var i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }
                var byteArray = new Uint8Array(byteNumbers);
                var blob1 = new Blob([byteArray], {
                    type: "jpg"
                });
                console.log(blob1);
                this.api.uploadImage(blob1, "test", "jpg", this.data.id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    console.log("---------------");
                    console.log(res);
                    this.loading = false;
                    this.edit_icon = true;
                    // Get the entire data
                    this.storage.get('USER_INFO').then(valueStr => {
                        let value = valueStr ? JSON.parse(valueStr) : {};
                        // Modify just that property
                        value.avatar_urls[24] = res.thumb;
                        value.avatar_urls[96] = res.full;
                        this.data.avatar = res.thumb;
                        // Save the entire data again
                        this.storage.set('USER_INFO', JSON.stringify(value));
                        window.location.reload();
                    });
                    // Get the entire data
                }), err => {
                    console.log(err);
                    this.showError(err);
                });
                //   });
                //  });
                //  } 
            }), (err) => {
                alert(err);
            });
        });
    }
    uploadFile(event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.loading = true;
            this.edit_icon = false;
            const loading = yield this.loadingController.create({
                message: 'Please wait...',
                duration: 2000
            });
            yield loading.present();
            const eventObj = event;
            const target = eventObj.target;
            const file1 = target.files[0];
            this.api.uploadImageFile(file1, this.data.id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                this.loading = false;
                this.edit_icon = true;
                const toast = yield this.toastCtrl.create({
                    message: 'Image Uploaded',
                    duration: 3000
                });
                toast.present();
                // Get the entire data
                this.storage.get('USER_INFO').then(valueStr => {
                    let value = valueStr ? valueStr : {};
                    // Modify just that property
                    value.avatar_urls[24] = res.thumb;
                    value.avatar_urls[96] = res.full;
                    this.data.avatar = res.thumb;
                    // Save the entire data again
                    this.storage.set('USER_INFO', value);
                });
            }), err => {
                console.log(err);
                this.showError(err);
            });
        });
    }
    selectImage2() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE));
            this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
            // this.loading=true;
            let options = {
                maximumImagesCount: 1,
                width: 200,
                quality: 25,
                outputType: 1
            };
            let imageResponse = [];
            this.imagePicker.getPictures(options).then((results) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                this.loading = true;
                this.edit_icon = false;
                var imageData = results[0].toString();
                //  var imageData1 = imageData.toString();
                var byteCharacters = atob(imageData.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
                var byteNumbers = new Array(byteCharacters.length);
                for (var i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }
                var byteArray = new Uint8Array(byteNumbers);
                var blob1 = new Blob([byteArray], {
                    type: "jpg"
                });
                console.log(blob1);
                this.api.uploadImage2(blob1, "test", "jpg", this.data.id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    console.log("---------------");
                    console.log(res);
                    this.loading = false;
                    this.edit_icon = true;
                    this.data.cover = res.image;
                    // Get the entire data
                    this.storage.get('USER_INFO').then(valueStr => {
                        let value = valueStr ? JSON.parse(valueStr) : {};
                        // Modify just that property
                        value.cover_url = res.image;
                        this.data.cover = res.image;
                        // Save the entire data again
                        this.storage.set('USER_INFO', JSON.stringify(value));
                        window.location.reload();
                    });
                    // Get the entire data
                }), err => {
                    console.log(err);
                    this.showError(err);
                });
                //   });
                //  });
                //  } 
            }), (err) => {
                alert(err);
            });
        });
    }
    uploadCoverFile(event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.loading3 = true;
            this.edit_icon2 = false;
            const loading = yield this.loadingController.create({
                message: 'Please wait...',
                duration: 2000
            });
            yield loading.present();
            const eventObj = event;
            const target = eventObj.target;
            const file1 = target.files[0];
            // Get the entire data
            this.api.uploadImageFile2(file1, this.data.id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                this.loading3 = false;
                this.edit_icon2 = true;
                console.log("---------------");
                console.log(res);
                const toast = yield this.toastCtrl.create({
                    message: 'Profile Cover Image Updated',
                    duration: 3000
                });
                toast.present();
                // Get the entire data
                this.data.cover = res.image;
                this.storage.get('USER_INFO').then(valueStr => {
                    let value = valueStr ? valueStr : {};
                    // Modify just that property
                    value.cover_url = res.image;
                    // Save the entire data again
                    this.storage.set('USER_INFO', value);
                    // window.location.reload();
                });
            }), err => {
                console.log(err);
                this.showError(err);
            });
        });
    }
    selectImageSource() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const buttons = [
                {
                    text: 'Choose From Photos Photo',
                    icon: 'image',
                    handler: () => {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE));
                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                        this.selectImage();
                    }
                }
            ];
            // Only allow file selection inside a browser
            if (!this.plt.is('hybrid')) {
                buttons.push({
                    text: 'Choose a File',
                    icon: 'attach',
                    handler: () => {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE));
                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                        this.fileInput.nativeElement.click();
                    }
                });
            }
            const actionSheet = yield this.actionSheetCtrl.create({
                header: 'Select Image Source',
                buttons
            });
            yield actionSheet.present();
        });
    }
    selectcoverImageSource() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const buttons = [
                {
                    text: 'Choose From Photos Photo',
                    icon: 'image',
                    handler: () => {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE));
                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                        this.selectImage2();
                    }
                }
            ];
            // Only allow file selection inside a browser
            if (!this.plt.is('hybrid')) {
                buttons.push({
                    text: 'Choose a File',
                    icon: 'attach',
                    handler: () => {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE));
                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                        this.fileInput2.nativeElement.click();
                    }
                });
            }
            const actionSheet = yield this.actionSheetCtrl.create({
                header: 'Select Image Source',
                buttons
            });
            yield actionSheet.present();
        });
    }
    goToprofile() {
        this.router.navigate(['/member-detail']);
    }
    goToHome(res) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire({
            title: 'Success',
            text: 'Thank you for registrations',
            icon: 'success',
            backdrop: false,
        });
        this.router.navigate(['/tabs/home-new']);
    }
    //   uploadCoverFile(event: EventTarget) {
    //     this.loading3=true;
    //     this.edit_icon2=false;
    //     const eventObj: MSInputMethodContext = event as MSInputMethodContext;
    //     const target: HTMLInputElement = eventObj.target as HTMLInputElement;
    //     const file: File = target.files[0];
    //     this.api.uploadImageFile2(file,this.data.id).subscribe(
    //       async (res : any) => {
    //         this.loading3=false;
    //         this.edit_icon2=true;
    //       console.log("---------------");
    //       console.log(res);
    //       const toast = await this.toastCtrl.create({
    //         message: 'Profile Cover Image Updated',
    //         duration: 3000
    //       });
    //       toast.present();
    // // Get the entire data
    // this.storage.get('USER_INFO').then(valueStr => {
    //   let value = valueStr ? valueStr : {};
    //    // Modify just that property
    //    value.cover_url = res.image;
    // this.data.cover_url=res.image;
    //    // Save the entire data again
    //    this.storage.set('USER_INFO', value);
    //     window.location.reload();
    // });
    //     },
    //     err => {
    //       this.loading3=false;
    //       this.edit_icon2=true;
    //       console.log(err);
    //       this.showError(err);
    //     }
    //     );
    //   }
    userupdate() {
        this.loading2 = true;
        this.api.userupdate(this.data.id, this.userForm.value.firstname, this.userForm.value.email, this.userForm.value.password, this.userForm.value.lastname, this.userForm.value.nick_name, this.userForm.value.description).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.loading2 = false;
            console.log(res);
            const toast = yield this.toastCtrl.create({
                message: 'Profile Updated',
                duration: 3000
            });
            toast.present();
            this.storage.set('USER_INFO', res);
            this.router.navigate(['/member-detail']);
            window.location.reload();
        }), err => {
            this.loading2 = false;
            console.log(err);
            this.showError(err);
        });
    }
    showError(err) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: err.error.code,
                subHeader: err.error.data,
                message: err.error.message,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
};
EditProfilePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] },
    { type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_12__["NativePageTransitions"] },
    { type: _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_10__["ImagePicker"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_13__["Location"] },
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_11__["File"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"] },
    { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_8__["AndroidPermissions"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ActionSheetController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], EditProfilePage.prototype, "fileInput", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput2', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], EditProfilePage.prototype, "fileInput2", void 0);
EditProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit-profile',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./edit-profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/edit-profile/edit-profile.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./edit-profile.page.scss */ "./src/app/pages/edit-profile/edit-profile.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"],
        _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_12__["NativePageTransitions"],
        _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_10__["ImagePicker"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"], _angular_common__WEBPACK_IMPORTED_MODULE_13__["Location"],
        _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_11__["File"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"],
        _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"],
        _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_8__["AndroidPermissions"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ActionSheetController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"]])
], EditProfilePage);



/***/ })

}]);
//# sourceMappingURL=pages-edit-profile-edit-profile-module-es2015.js.map
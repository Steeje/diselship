function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-members-list-members-list-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/members-list/members-list.page.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/members-list/members-list.page.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesMembersListMembersListPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-button (click)=\"BackButton()\" slot=\"start\">\n      <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n    </ion-button>\n    <ion-title color=\"light\">{{name}} Members</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n<img style=\"width: -webkit-fill-available;\" *ngIf=\"!members.length>0\" src=\"assets/imgs/no_result.jpg\">\n  <div *ngIf=\"members.length>0\" class=\"main_content_div\">\n\n\n    <span >\n      <div class=\"flex_div\" *ngFor=\"let item of members\">\n        <div class=\"first_div\">\n          <div [style.backgroundImage]=\"'url('+item.avatar_urls.thumb+')'\"  [routerLink]=\"['/members-detail', item.id]\" class=\"image\"></div>\n          <div class=\"content_div\">\n            <ion-label class=\"head_lbl\">{{item.profile_name}}</ion-label>\n            <ion-label class=\"small_lbl\"> {{item.followers}} Followers</ion-label>\n          </div>\n        </div>\n        <div class=\"second_div\">\n          <div class=\"chat_div\">\n            <ion-icon (click)=\"goToChatList()\" name=\"chatbubbles\" mode=\"md\"></ion-icon>\n          </div>\n        \n          <div class=\"menu_div\">\n            <ion-icon (click)=\"followuser(item.id)\" *ngIf=\"!item.is_following\" name=\"newspaper-outline\" mode=\"md\"></ion-icon>\n            <ion-icon (click)=\"unfollowuser(item.id)\" *ngIf=\"item.is_following\" name=\"newspaper\" mode=\"md\"></ion-icon>\n          </div>\n          <div class=\"menu_div\"  *ngIf=\"item.create_friendship\">\n            <ion-icon (click)=\"connectuser(item.id)\" *ngIf=\"item.friendship_status=='not_friends'\" name=\"person-add-outline\" mode=\"md\"></ion-icon>\n            <ion-icon (click)=\"deleteuser(item.id)\" *ngIf=\"item.friendship_status!='not_friends'\" name=\"person-remove\" mode=\"md\"></ion-icon>\n          </div>\n        </div>\n      </div>\n    </span>\n\n  </div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/members-list/members-list-routing.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/pages/members-list/members-list-routing.module.ts ***!
    \*******************************************************************/

  /*! exports provided: MembersListPageRoutingModule */

  /***/
  function srcAppPagesMembersListMembersListRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MembersListPageRoutingModule", function () {
      return MembersListPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _members_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./members-list.page */
    "./src/app/pages/members-list/members-list.page.ts");

    var routes = [{
      path: '',
      component: _members_list_page__WEBPACK_IMPORTED_MODULE_3__["MembersListPage"]
    }];

    var MembersListPageRoutingModule = function MembersListPageRoutingModule() {
      _classCallCheck(this, MembersListPageRoutingModule);
    };

    MembersListPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], MembersListPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/members-list/members-list.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/pages/members-list/members-list.module.ts ***!
    \***********************************************************/

  /*! exports provided: MembersListPageModule */

  /***/
  function srcAppPagesMembersListMembersListModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MembersListPageModule", function () {
      return MembersListPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _members_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./members-list-routing.module */
    "./src/app/pages/members-list/members-list-routing.module.ts");
    /* harmony import */


    var _members_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./members-list.page */
    "./src/app/pages/members-list/members-list.page.ts");

    var MembersListPageModule = function MembersListPageModule() {
      _classCallCheck(this, MembersListPageModule);
    };

    MembersListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _members_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["MembersListPageRoutingModule"]],
      declarations: [_members_list_page__WEBPACK_IMPORTED_MODULE_6__["MembersListPage"]]
    })], MembersListPageModule);
    /***/
  },

  /***/
  "./src/app/pages/members-list/members-list.page.scss":
  /*!***********************************************************!*\
    !*** ./src/app/pages/members-list/members-list.page.scss ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesMembersListMembersListPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div .flex_div {\n  display: flex;\n  align-items: center;\n  padding-top: 10px;\n  width: 100%;\n  justify-content: space-between;\n  padding: 10px 16px;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .flex_div .first_div {\n  display: flex;\n  align-items: center;\n}\n\n.main_content_div .flex_div .second_div {\n  display: flex;\n  align-items: center;\n}\n\n.main_content_div .flex_div .second_div ion-icon {\n  font-size: 20px;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n\n.main_content_div .flex_div .second_div .chat_div {\n  height: 35px;\n  width: 35px;\n  border-radius: 50%;\n  border: 1px solid var(--ion-color-main);\n  position: relative;\n  margin-right: 5px;\n}\n\n.main_content_div .flex_div .second_div .chat_div ion-icon {\n  color: var(--ion-color-main);\n}\n\n.main_content_div .flex_div .second_div .menu_div {\n  height: 35px;\n  width: 35px;\n  border-radius: 50%;\n  border: 1px solid var(--ion-color-main);\n  position: relative;\n}\n\n.main_content_div .flex_div .second_div .menu_div ion-icon {\n  color: var(--ion-color-main);\n}\n\n.main_content_div .flex_div .image {\n  height: 45px;\n  width: 45px;\n  max-width: 45px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.main_content_div .flex_div .content_div {\n  margin-left: 10px;\n}\n\n.main_content_div .flex_div .content_div .head_lbl {\n  font-weight: 600;\n}\n\n.main_content_div .flex_div .content_div .small_lbl {\n  font-size: 13px;\n  margin-top: 5px;\n}\n\n.main_content_div .flex_div .fallow_btn {\n  font-size: 14px;\n  font-weight: 600;\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVtYmVycy1saXN0L0Q6XFxSYWd1dmFyYW4gaW1hZ2VcXGRpc2Vsc2hpcC9zcmNcXGFwcFxccGFnZXNcXG1lbWJlcnMtbGlzdFxcbWVtYmVycy1saXN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbWVtYmVycy1saXN0L21lbWJlcnMtbGlzdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQ0FBQTtBQ0NKOztBRENBO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtBQ0VKOztBRENBO0VBQ0ksV0FBQTtBQ0VKOztBREFJO0VBQ0ksY0FBQTtBQ0VSOztBREFJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLGtDQUFBO0FDRVI7O0FEQVE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUNFWjs7QURDUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtBQ0NaOztBRENZO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtBQ0NoQjs7QURFWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSx1Q0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNBaEI7O0FERWdCO0VBQ0ksNEJBQUE7QUNBcEI7O0FER1k7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsdUNBQUE7RUFDQSxrQkFBQTtBQ0RoQjs7QURHZ0I7RUFDSSw0QkFBQTtBQ0RwQjs7QURNUTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDSlo7O0FET1E7RUFDSSxpQkFBQTtBQ0xaOztBRE1ZO0VBQ0ksZ0JBQUE7QUNKaEI7O0FETVk7RUFDSSxlQUFBO0VBQ0EsZUFBQTtBQ0poQjs7QURRUTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFNBQUE7QUNOWiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21lbWJlcnMtbGlzdC9tZW1iZXJzLWxpc3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWh7XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG4ubWFpbl9jb250ZW50X2RpdntcbiAgICB3aWR0aDogMTAwJTtcblxuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIH1cbiAgICAuZmxleF9kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBwYWRkaW5nOiAxMHB4IDE2cHg7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG5cbiAgICAgICAgLmZpcnN0X2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICB9XG5cbiAgICAgICAgLnNlY29uZF9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcblxuICAgICAgICAgICAgaW9uLWljb257XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICB0b3A6IDUwJTtcbiAgICAgICAgICAgICAgICBsZWZ0OiA1MCU7XG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwtNTAlKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmNoYXRfZGl2e1xuICAgICAgICAgICAgICAgIGhlaWdodDogMzVweDtcbiAgICAgICAgICAgICAgICB3aWR0aDogMzVweDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcblxuICAgICAgICAgICAgICAgIGlvbi1pY29ue1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5tZW51X2RpdntcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDM1cHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDM1cHg7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgICAgICAgICAgICAgICBpb24taWNvbntcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAuaW1hZ2V7XG4gICAgICAgICAgICBoZWlnaHQ6IDQ1cHg7XG4gICAgICAgICAgICB3aWR0aDogNDVweDtcbiAgICAgICAgICAgIG1heC13aWR0aDogNDVweDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICB9XG5cbiAgICAgICAgLmNvbnRlbnRfZGl2e1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAuaGVhZF9sYmx7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5zbWFsbF9sYmx7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5mYWxsb3dfYnRue1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgfVxuICAgIH1cbn0iLCJpb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuXG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCB7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiBpb24tbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICB3aWR0aDogMTAwJTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBwYWRkaW5nOiAxMHB4IDE2cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmZpcnN0X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLnNlY29uZF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5zZWNvbmRfZGl2IGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNTAlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5zZWNvbmRfZGl2IC5jaGF0X2RpdiB7XG4gIGhlaWdodDogMzVweDtcbiAgd2lkdGg6IDM1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5zZWNvbmRfZGl2IC5jaGF0X2RpdiBpb24taWNvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLnNlY29uZF9kaXYgLm1lbnVfZGl2IHtcbiAgaGVpZ2h0OiAzNXB4O1xuICB3aWR0aDogMzVweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuc2Vjb25kX2RpdiAubWVudV9kaXYgaW9uLWljb24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5pbWFnZSB7XG4gIGhlaWdodDogNDVweDtcbiAgd2lkdGg6IDQ1cHg7XG4gIG1heC13aWR0aDogNDVweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IC5oZWFkX2xibCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IC5zbWFsbF9sYmwge1xuICBmb250LXNpemU6IDEzcHg7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuZmFsbG93X2J0biB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbWFyZ2luOiAwO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/members-list/members-list.page.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/members-list/members-list.page.ts ***!
    \*********************************************************/

  /*! exports provided: MembersListPage */

  /***/
  function srcAppPagesMembersListMembersListPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MembersListPage", function () {
      return MembersListPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/services/dummy-data.service */
    "./src/app/services/dummy-data.service.ts");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/native-page-transitions/ngx */
    "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var MembersListPage = /*#__PURE__*/function () {
      function MembersListPage(navCtrl, nativePageTransitions, route, router, storage, toastCtrl, api, location, dummy) {
        var _this = this;

        _classCallCheck(this, MembersListPage);

        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.route = route;
        this.router = router;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.api = api;
        this.location = location;
        this.dummy = dummy;
        this.members = [];
        this.options = {
          direction: 'up',
          duration: 500,
          slowdownfactor: 3,
          slidePixels: 20,
          iosdelay: 100,
          androiddelay: 150,
          fixedPixelsTop: 0,
          fixedPixelsBottom: 60
        };
        this.users = this.dummy.users;
        var members2 = [];
        members2.push({
          thumb: 'surname'
        });
        this.members.push({
          avatar_urls: members2
        });
        console.log(this.members);
        this.storage.get('COMPLETE_USER_INFO').then(function (result) {
          if (result != null) {
            console.log(result);
            _this.token = result.token;
            _this.user_id = result.user_id;
            console.log('user_id: ' + result.user_id);
            console.log('token: ' + result.token);
          }
        })["catch"](function (e) {
          console.log('error: ' + e); // Handle errors here
        });
      }

      _createClass(MembersListPage, [{
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.nativePageTransitions.slide(this.options);
        } // example of adding a transition when pushing a new page

      }, {
        key: "openPage",
        value: function openPage(page) {
          this.nativePageTransitions.slide(this.options);
          this.navCtrl.navigateForward(page);
        }
      }, {
        key: "unfollowuser",
        value: function unfollowuser(id) {
          var _this2 = this;

          this.api.followuser("unfollow", id).subscribe(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var index, toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      console.log(res.data);
                      index = this.members.findIndex(function (_ref) {
                        var id = _ref.id;
                        return id === res.data.id;
                      });
                      if (typeof index !== 'undefined') this.members.splice(index, 1, res.data);
                      _context.next = 5;
                      return this.toastCtrl.create({
                        message: "Unfollow User Successfully",
                        duration: 3000
                      });

                    case 5:
                      toast = _context.sent;
                      _context.next = 8;
                      return toast.present();

                    case 8:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          });
        }
      }, {
        key: "connectuser",
        value: function connectuser(id) {
          var _this3 = this;

          this.api.connectfrds(id, this.token, this.user_id).subscribe(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var index, data, toast;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      console.log(res);
                      index = this.members.findIndex(function (data) {
                        return data.id === id;
                      });
                      data = this.members.find(function (data) {
                        return data.id === id;
                      });
                      console.log(data);

                      if (typeof index !== 'undefined') {
                        data.friendship_status = "pending";
                        this.members.splice(index, 1, data);
                      }

                      _context2.next = 7;
                      return this.toastCtrl.create({
                        message: "Friend Request Sent Successfully",
                        duration: 3000
                      });

                    case 7:
                      toast = _context2.sent;
                      _context2.next = 10;
                      return toast.present();

                    case 10:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          });
        }
      }, {
        key: "deleteuser",
        value: function deleteuser(id) {
          var _this4 = this;

          this.api.deletefrds(id).subscribe(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var index, data, toast;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      index = this.members.findIndex(function (data) {
                        return data.id === id;
                      });
                      data = this.members.find(function (data) {
                        return data.id === id;
                      });
                      console.log(data);

                      if (typeof index !== 'undefined') {
                        data.friendship_status = "not_friends";
                        this.members.splice(index, 1, data);
                      }

                      _context3.next = 6;
                      return this.toastCtrl.create({
                        message: "Unfriend Successfully",
                        duration: 3000
                      });

                    case 6:
                      toast = _context3.sent;
                      _context3.next = 9;
                      return toast.present();

                    case 9:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          });
        }
      }, {
        key: "followuser",
        value: function followuser(id) {
          var _this5 = this;

          this.api.followuser("follow", id).subscribe(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var index, toast;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      console.log(res.data);
                      index = this.members.findIndex(function (_ref2) {
                        var id = _ref2.id;
                        return id === res.data.id;
                      });
                      if (typeof index !== 'undefined') this.members.splice(index, 1, res.data);
                      _context4.next = 5;
                      return this.toastCtrl.create({
                        message: "Follow User Successfully",
                        duration: 3000
                      });

                    case 5:
                      toast = _context4.sent;
                      _context4.next = 8;
                      return toast.present();

                    case 8:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          });
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this6 = this;

          this.route.queryParams.subscribe(function (data) {
            console.log("data.id", data.id);
            _this6.id = data.id;
            _this6.name = data.name;
          });

          if (this.id) {
            this.api.getgroupMembers(this.id).subscribe(function (res) {
              console.log(res);
              _this6.members = res;
            });
          } else {
            this.api.getMembers(this.token).subscribe(function (res) {
              console.log(res);
              _this6.members = res;
            });
          }
        }
      }, {
        key: "goToChatList",
        value: function goToChatList() {
          this.router.navigate(['/chatlist']);
        }
      }, {
        key: "goTomemberDetail",
        value: function goTomemberDetail() {
          this.router.navigate(['/member-detail']);
        }
      }, {
        key: "BackButton",
        value: function BackButton() {
          this.location.back();
        }
      }]);

      return MembersListPage;
    }();

    MembersListPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_8__["NativePageTransitions"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
      }, {
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_5__["apiService"]
      }, {
        type: _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"]
      }, {
        type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__["dummyDataService"]
      }];
    };

    MembersListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-members-list',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./members-list.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/members-list/members-list.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./members-list.page.scss */
      "./src/app/pages/members-list/members-list.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_8__["NativePageTransitions"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _services_api_service__WEBPACK_IMPORTED_MODULE_5__["apiService"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"], src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__["dummyDataService"]])], MembersListPage);
    /***/
  },

  /***/
  "./src/app/services/dummy-data.service.ts":
  /*!************************************************!*\
    !*** ./src/app/services/dummy-data.service.ts ***!
    \************************************************/

  /*! exports provided: dummyDataService */

  /***/
  function srcAppServicesdummyDataServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "dummyDataService", function () {
      return dummyDataService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var dummyDataService = function dummyDataService() {
      _classCallCheck(this, dummyDataService);

      this.users = [{
        img: 'assets/imgs/user.jpg',
        cover: 'assets/imgs/back1.jpg',
        btn_text: 'VIEW JOBS'
      }, {
        img: 'assets/imgs/user2.jpg',
        cover: 'assets/imgs/back2.jpg',
        btn_text: 'FOLLOW HASHTAG'
      }, {
        img: 'assets/imgs/user3.jpg',
        cover: 'assets/imgs/back3.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user4.jpg',
        cover: 'assets/imgs/back4.jpg',
        btn_text: 'SEE ALL VIEW'
      }, {
        img: 'assets/imgs/user.jpg',
        cover: 'assets/imgs/back1.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user2.jpg',
        cover: 'assets/imgs/back2.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user3.jpg',
        cover: 'assets/imgs/back3.jpg',
        btn_text: 'EXPAND YOUR NETWORK'
      }, {
        img: 'assets/imgs/user4.jpg',
        cover: 'assets/imgs/back4.jpg',
        btn_text: 'SEE ALL VIEW'
      }];
      this.jobs = [{
        img: 'assets/imgs/company/initappz.png',
        title: 'SQL Server',
        addr: 'Bhavnagar, Gujrat, India',
        time: '2 school alumni',
        status: '1 day'
      }, {
        img: 'assets/imgs/company/google.png',
        title: 'Web Designer',
        addr: 'Vadodara, Gujrat, India',
        time: 'Be an early applicant',
        status: '2 days'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        title: 'Business Analyst',
        addr: 'Ahmedabad, Gujrat, India',
        time: 'Be an early applicant',
        status: '1 week'
      }, {
        img: 'assets/imgs/company/initappz.png',
        title: 'PHP Developer',
        addr: 'Pune, Maharashtra, India',
        time: '2 school alumni',
        status: 'New'
      }, {
        img: 'assets/imgs/company/google.png',
        title: 'Graphics Designer',
        addr: 'Bhavnagar, Gujrat, India',
        time: 'Be an early applicant',
        status: '1 day'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        title: 'Phython Developer',
        addr: 'Ahmedabad, Gujrat, India',
        time: '2 school alumni',
        status: '5 days'
      }];
      this.company = [{
        img: 'assets/imgs/company/initappz.png',
        name: 'Initappz Shop'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        name: 'Microsoft'
      }, {
        img: 'assets/imgs/company/google.png',
        name: 'Google'
      }];
    };

    dummyDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], dummyDataService);
    /***/
  }
}]);
//# sourceMappingURL=pages-members-list-members-list-module-es5.js.map
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-members-activity-members-activity-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/members-activity/members-activity.page.html":
  /*!*********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/members-activity/members-activity.page.html ***!
    \*********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesMembersActivityMembersActivityPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n      <img src=\"assets/imgs/user.png\" class=\"user_image\">\n      <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar>\n      <ion-icon name=\"chatbubbles\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\"></ion-icon>\n    </div>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n\n      <ion-buttons slot=\"start\">\n        <!-- <ion-menu-button color=\"light\"></ion-menu-button> -->\n        <img src=\"assets/imgs/user.png\" class=\"user_image\" (click)=\"openMenu()\">\n      </ion-buttons>\n      <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar>\n      <ion-icon name=\"chatbubbles\" (click)=\"goToChatList()\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\"></ion-icon>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n\n    <!-- <div class=\"white_div\" style=\"margin-top: -4px;\">\n      <ion-button (click)=\"onClick()\" fill=\"clear\" expand=\"block\" >\n        <ion-label>Manage my network</ion-label>\n         <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n         <ion-ripple-effect></ion-ripple-effect>\n      </ion-button>\n    </div> -->\n    <div class=\"up_lbl\" style=\" padding-left: 10px; background: white;\">\n      <ion-label (click)=\"goToMemberslist()\" class=\"blue_lbl\">Members</ion-label>\n      <ion-buttons slot=\"start\" >\n        <ion-button (click)=\"goToMemberslist()\" style=\"    font-size: 10px;\n        text-transform: none;\" color=\"dark\">\n          See All\n        </ion-button>\n        <ion-button (click)=\"sliderShow(true)\" *ngIf=\"!showSlider\" color=\"dark\">\n          <ion-icon name=\"chevron-up-outline\" mode=\"ios\"></ion-icon>\n        </ion-button>\n      \n        <ion-button (click)=\"sliderShow(false)\"  *ngIf=\"showSlider\" color=\"dark\">\n          <ion-icon name=\"chevron-down-outline\" mode=\"ios\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n\n    </div>\n    <div *ngIf=\"load\">\n      <div style=\"background: white;\" class=\"ion-padding custom-skeleton chips_div\">\n\n\n        <ion-label class=\"chip\"> <ion-avatar >\n          <ion-skeleton-text animated></ion-skeleton-text>\n        </ion-avatar></ion-label>\n        <ion-label class=\"chip\"> <ion-avatar >\n          <ion-skeleton-text animated></ion-skeleton-text>\n        </ion-avatar></ion-label>\n        <ion-label class=\"chip\"> <ion-avatar >\n          <ion-skeleton-text animated></ion-skeleton-text>\n        </ion-avatar></ion-label>\n        <ion-label class=\"chip\"> <ion-avatar >\n          <ion-skeleton-text animated></ion-skeleton-text>\n        </ion-avatar></ion-label>\n\n      </div>\n      </div>\n    <div style=\"background: white;\" class=\"chips_div\" *ngIf=\"!showSlider\">\n      <ion-grid *ngIf=\"members.length>0\" fixed>\n        <ion-row>\n      <ion-col size=\"3\"  *ngFor=\"let item of (members|slice : 0:4)\">\n        <ion-label [routerLink]=\"['/members-detail', item.id]\" class=\"chip\" > <ion-avatar >\n          <img src=\"{{item.media_url}}\"  class=\"scroll-item\"/>\n          <ion-label style=\"text-align: center;\">{{item.name}}</ion-label>\n        </ion-avatar></ion-label>\n        \n      </ion-col>\n   \n    \n    \n    </ion-row>\n      </ion-grid>\n     \n    </div>\n    <ion-label class=\"notfound\" *ngIf=\"!members.length>0\">\n      <ion-icon name=\"alert-circle-outline\"></ion-icon> <ion-label>No Members Found. </ion-label>\n    </ion-label>\n    <div class=\"slider_div\" style=\"background: white; height: 300px;\"  *ngIf=\"showSlider && members.length>0\">\n      <ion-slides mode=\"ios\" style=\" height: 340px;\" pager=\"ios\" [options]=\"slideoptions\">\n\n        <ion-slide >\n          <div class=\"slide_div\">\n            <div class=\"up_div user_div\">\n              <ion-grid fixed>\n                <ion-row>\n              <ion-col size=\"6\" *ngFor=\"let item of (members|slice : 0:2)\">\n              <div class=\"col_div\" >\n                <div class=\"cover\"  [routerLink]=\"['/members-detail', item.id]\" [ngStyle]= \"{'background-image': 'url('+ item.cover +')'}\" >\n                  <ion-icon slot=\"start\" name=\"close-circle\" class=\"close\"></ion-icon>\n                  <div class=\"back_image\" \n                 \n                [ngStyle]= \"{'background-image': 'url('+ item.media_url +')'}\"></div>\n                </div>\n  \n                <ion-label class=\"username\">{{item.profile_name}}</ion-label>\n                <ion-label class=\"detail\">{{item.user_login}}</ion-label>\n  \n                <div class=\"mutual_div\">\n                  <img src=\"assets/imgs/link.png\" class=\"link\">\n                  <ion-label class=\"link_lbl\">{{item.friendship_status}}</ion-label>\n                </div>\n  \n                <ion-button *ngIf=\"item.friendship_status=='not_friends'\" (click)=\"connectuser(item.id)\" expand=\"block\" fill=\"outline\" size=\"small\">\n                  CONNECT\n                </ion-button>\n                <ion-button *ngIf=\"item.friendship_status!='not_friends'\" expand=\"block\" fill=\"outline\" size=\"small\">\n                  Requset Sent\n                </ion-button>\n  \n              </div>\n              </ion-col> \n             \n\n              </ion-row>\n              </ion-grid>\n            </div>\n\n            <!-- <div class=\"lower_div\">\n              <ion-avatar *ngFor=\"let item of (members|slice : 0:2)\" >\n                <img src=\"{{item.media_url}}\"  class=\"scroll-item\"/>\n              </ion-avatar>\n            </div> -->\n\n          </div>\n        </ion-slide >\n        <ion-slide *ngIf=\"members.length>2\" >\n        <div class=\"slide_div\">\n          <div class=\"up_div user_div\">\n            <ion-grid fixed>\n              <ion-row>\n            <ion-col size=\"6\" *ngFor=\"let item of (members|slice : 2:4)\">\n            <div class=\"col_div\">\n              <div class=\"cover\"  [routerLink]=\"['/members-detail', item.id]\" [ngStyle]= \"{'background-image': 'url('+ item.cover +')'}\" >\n                <ion-icon slot=\"start\" name=\"close-circle\" class=\"close\"></ion-icon>\n                <div class=\"back_image\" \n               \n              [ngStyle]= \"{'background-image': 'url('+ item.media_url +')'}\"></div>\n              </div>\n\n              <ion-label class=\"username\">{{item.profile_name}}</ion-label>\n              <ion-label class=\"detail\">{{item.user_login}}</ion-label>\n\n              <div class=\"mutual_div\">\n                <img src=\"assets/imgs/link.png\" class=\"link\">\n                <ion-label class=\"link_lbl\">{{item.friendship_status}}</ion-label>\n              </div>\n\n              <ion-button *ngIf=\"item.friendship_status=='not_friends'\" (click)=\"connectuser(item.id)\" expand=\"block\" fill=\"outline\" size=\"small\">\n                CONNECT\n              </ion-button>\n              <ion-button *ngIf=\"item.friendship_status!='not_friends'\" expand=\"block\" fill=\"outline\" size=\"small\">\n                Requset Sent\n              </ion-button>\n\n\n            </div>\n            </ion-col>\n     \n            </ion-row>\n            </ion-grid>\n          </div>\n\n         \n\n        </div>\n      </ion-slide>\n      \n    \n      </ion-slides>\n    \n      <ion-label class=\"notfound\" *ngIf=\"!members.length>0\">\n        <ion-icon name=\"alert-circle-outline\"></ion-icon> <ion-label>No Members Found. </ion-label>\n      </ion-label>\n    </div>\n\n     \n    \n    <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n      <ion-label class=\"blue_lbl\">Activities \n      </ion-label>\n    </div>\n\n    <div class=\"flex_div\" *ngFor=\"let item of (activities|slice : 0:6)\">\n      <div class=\"f_div\">\n        <div class=\"back_image\"  [routerLink]=\"['/members-detail', item.user_id]\" [ngStyle]= \"{'background-image': 'url('+ item.media_url +')'}\" ></div>\n        <!-- <ion-grid><ion-row>\n          <ion-col size=\"\">\n\n          </ion-col>\n        </ion-row>\n      </ion-grid> -->\n        <div class=\"detail\">\n          <ion-label  style=\"font-size: 15px;\" class=\"name\">{{item.name}}</ion-label>\n          <ion-label style=\"font-size: 14px;\" class=\"desc\">{{item.title}}</ion-label>\n          <ion-label style=\"font-size: small; \" class=\"desc\" >{{item.content}}</ion-label>\n          <ion-label style=\"font-size: small; color: black;\" class=\"desc\" >{{item.date}} ago</ion-label>\n        </div>\n        <!-- <div class=\"time\">\n          \n         \n        </div> -->\n      </div>\n    \n    </div>\n    <ion-label class=\"notfound\" *ngIf=\"!activities.length>0\">\n      <ion-icon name=\"alert-circle-outline\"></ion-icon> <ion-label>No Activities Found. </ion-label>\n    </ion-label>\n    \n    <div class=\"white_div\">\n      <ion-label class=\"blue_lbl\" style=\"text-align: center;\" (click)=\"gotoactivitylist()\">SHOW MORE</ion-label>\n    </div>\n\n\n    <div   style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px; background: white;\">\n      <ion-label (click)=\"goToGroupslist()\" class=\"blue_lbl\">Groups <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n      </ion-label>\n    </div>\n    <div class=\"slider_div\" style=\"background: white;\" >\n      \n\n      <ion-slides *ngIf=\"groups.length>0\" style=\"height: 320px;\" mode=\"ios\"  pager=\"ios\" [options]=\"slideoptions\">\n\n        <ion-slide  >\n          <ion-grid>\n            <ion-row > \n              <ion-col size=\"6\" *ngFor=\"let item of (groups|slice : 0:2)\">\n          <ion-card [routerLink]=\"['/group-detail', item.id]\" >\n    <img style=\"\n    width: 100%;\" src=\"{{item.media_url}}\"  alt=\"ion\">\n    <ion-card-header>\n      <ion-card-title>{{item.name}}</ion-card-title>\n    </ion-card-header>\n    <!-- <ion-card-content>\n      {{item.description}}\n    </ion-card-content> -->\n  </ion-card>\n  </ion-col>\n  </ion-row>\n  </ion-grid>\n        </ion-slide>\n\n        <ion-slide *ngIf=\"groups.length>2\" >\n          <ion-grid >\n            <ion-row > \n              <ion-col size=\"6\" *ngFor=\"let item of (groups|slice : 2:4)\">\n                <ion-card [routerLink]=\"['/group-detail', item.id]\"    >\n                  <img src=\"{{item.image}}\"  alt=\"ion\">\n                  <ion-card-header>\n                    <ion-card-title>{{item.name}}</ion-card-title>\n                  </ion-card-header>\n                  <ion-card-content>\n                    {{item.description}}\n                  </ion-card-content>\n        </ion-card>\n        </ion-col>\n        </ion-row>\n        </ion-grid>\n        </ion-slide>\n</ion-slides>\n\n<ion-label class=\"notfound\" *ngIf=\"!groups.length>0\">\n  <ion-icon name=\"alert-circle-outline\"></ion-icon> <ion-label>No Groups Found. </ion-label>\n</ion-label>\n\n\n\n</div>\n\n<div class=\"slider_div\" style=\"background: white; margin-top: 10px;\" >\n  <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n    <ion-label class=\"blue_lbl\">Notifications <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n    </ion-label>\n  </div>\n  <div *ngIf=\"notifications.length>0\" >\n  <div class=\"notification_div\"   *ngFor=\"let item of notifications\" (click)=\"goToPost()\">\n   \n    <div class=\"flex_div\" style=\"justify-content: flex-start;\">\n      <div class=\"back_image\" [ngStyle]= \"{'background-image': 'url('+ item.avatar +')'}\" ></div>\n      <div>\n        \n        <ion-label class=\"desc\">{{item.content}}\n        </ion-label>\n        <ion-label class=\"header_lbl\">{{item.date}}</ion-label>\n      </div>\n    </div>\n    <!-- <div class=\"btn_div\" *ngIf=\"item.btn_text != ''\">\n      <ion-button fill=\"outline\" size=\"small\">\n        {{item.btn_text}}\n      </ion-button>\n    </div> -->\n  </div></div>\n  <ion-label class=\"notfound\" *ngIf=\"!notifications.length>0\">\n    <ion-icon name=\"alert-circle-outline\"></ion-icon> <ion-label> You have no unread notifications. </ion-label>\n  </ion-label>\n\n \n</div>\n\n<div class=\"slider_div\" style=\"background: white; margin-top: 10px;\" >\n  <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px; display: flex; justify-content: space-between;\">\n    <ion-label class=\"blue_lbl\">Forums \n    </ion-label>\n    <ion-buttons slot=\"start\" >\n    <ion-button  (click)=\"goToForumslist()\" style=\"    font-size: 10px;\n      text-transform: none;\" color=\"dark\">\n        See All\n      </ion-button>\n    </ion-buttons>\n  </div>\n  <div  *ngIf=\"forums.length>0\" >\n\n  <div class=\"notification_div\"   *ngFor=\"let item of forums\" >\n   \n    <div class=\"flex_div\" [routerLink]=\"['/forum-detail', item.id]\" style=\"justify-content: flex-start;\">\n      <div class=\"back_image\" [ngStyle]= \"{'background-image': 'url('+ item.avatar +')'}\" ></div>\n      <div>\n        <ion-label class=\"header_lbl\">{{item.title}}\n        </ion-label>\n        <ion-label class=\"desc \">{{item.content}}\n        </ion-label>\n        <ion-label class=\"desc count\">{{item.count}} Discussions</ion-label>\n      </div>\n    </div>\n    <!-- <div class=\"btn_div\" *ngIf=\"item.btn_text != ''\">\n      <ion-button fill=\"outline\" size=\"small\">\n        {{item.btn_text}}\n      </ion-button>\n    </div> -->\n  </div>\n</div>\n  <ion-label class=\"notfound\" *ngIf=\"!forums.length>0\">\n    <ion-icon name=\"alert-circle-outline\"></ion-icon> <ion-label>    No Forums Found. </ion-label>\n  </ion-label>\n  \n</div>\n    <!-- <div class=\"user_div\" *ngFor=\"let item of [1,2,3]\">\n      <ion-label class=\"head_lbl\">People you nay know from GOVERNMENT ENVIRONMENT COLLEGE, BHAVNAGAR</ion-label>\n\n      <ion-grid fixed>\n        <ion-row>\n          <ion-col size=\"6\" *ngFor=\"let item of users\" (click)=\"goToPeopleProfile()\">\n            <div class=\"col_div\">\n              <div class=\"cover\" [style.backgroundImage]=\"'url( ' + item.cover+ ' )'\">\n                <ion-icon slot=\"start\" name=\"close-circle\" class=\"close\"></ion-icon>\n                <div class=\"back_image\" [style.backgroundImage]=\"'url( ' + item.img+ ' )'\"></div>\n              </div>\n\n              <ion-label class=\"username\">John Doe</ion-label>\n              <ion-label class=\"detail\">GEC, BHAVNAGAR</ion-label>\n\n              <div class=\"mutual_div\">\n                <img src=\"assets/imgs/link.png\" class=\"link\">\n                <ion-label class=\"link_lbl\">2 Mutual Connection</ion-label>\n              </div>\n\n              <ion-button expand=\"block\" fill=\"outline\" size=\"small\">\n                CONNECT\n              </ion-button>\n\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>    -->\n\n  </div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/members-activity/members-activity-routing.module.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/pages/members-activity/members-activity-routing.module.ts ***!
    \***************************************************************************/

  /*! exports provided: MembersActivityPageRoutingModule */

  /***/
  function srcAppPagesMembersActivityMembersActivityRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MembersActivityPageRoutingModule", function () {
      return MembersActivityPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _members_activity_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./members-activity.page */
    "./src/app/pages/members-activity/members-activity.page.ts");

    var routes = [{
      path: '',
      component: _members_activity_page__WEBPACK_IMPORTED_MODULE_3__["MembersActivityPage"]
    }];

    var MembersActivityPageRoutingModule = function MembersActivityPageRoutingModule() {
      _classCallCheck(this, MembersActivityPageRoutingModule);
    };

    MembersActivityPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], MembersActivityPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/members-activity/members-activity.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/pages/members-activity/members-activity.module.ts ***!
    \*******************************************************************/

  /*! exports provided: MembersActivityPageModule */

  /***/
  function srcAppPagesMembersActivityMembersActivityModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MembersActivityPageModule", function () {
      return MembersActivityPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _members_activity_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./members-activity-routing.module */
    "./src/app/pages/members-activity/members-activity-routing.module.ts");
    /* harmony import */


    var _members_activity_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./members-activity.page */
    "./src/app/pages/members-activity/members-activity.page.ts");

    var MembersActivityPageModule = function MembersActivityPageModule() {
      _classCallCheck(this, MembersActivityPageModule);
    };

    MembersActivityPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _members_activity_routing_module__WEBPACK_IMPORTED_MODULE_5__["MembersActivityPageRoutingModule"]],
      declarations: [_members_activity_page__WEBPACK_IMPORTED_MODULE_6__["MembersActivityPage"]]
    })], MembersActivityPageModule);
    /***/
  },

  /***/
  "./src/app/pages/members-activity/members-activity.page.scss":
  /*!*******************************************************************!*\
    !*** ./src/app/pages/members-activity/members-activity.page.scss ***!
    \*******************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesMembersActivityMembersActivityPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\nion-card-header {\n  padding: 5px;\n}\n\nion-card-title {\n  font-size: 17px;\n}\n\nion-card-content {\n  font-size: 14px;\n}\n\n.count {\n  font-size: 13px;\n  padding: 5px;\n  color: var(--ion-color-main);\n}\n\n.notfound {\n  border: 4px solid #0e76a8;\n  display: flex !important;\n  margin: 10px;\n  border-radius: 10px;\n}\n\n.notfound ion-icon {\n  background: #0e76a8;\n  color: #fff;\n  padding: 3px;\n  font-size: 25px;\n}\n\n.notfound ion-label {\n  font-size: 17px;\n  margin-top: 5px;\n  margin-left: 7px;\n}\n\nion-scroll[scrollX] {\n  white-space: nowrap;\n  height: 120px;\n  overflow: hidden;\n}\n\nion-scroll[scrollX] .scroll-item {\n  display: inline-block;\n}\n\nion-scroll[scrollX] .selectable-icon {\n  margin: 10px 20px 10px 20px;\n  color: red;\n  font-size: 100px;\n}\n\nion-scroll[scrollX] ion-avatar img {\n  margin: 10px;\n}\n\n.up_lbl {\n  padding-left: 16px;\n  padding-right: 16px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n  background: #e1e9ee;\n}\n\n.chips_div {\n  display: flex;\n  flex-direction: row;\n  overflow: scroll;\n  padding-left: 16px;\n  padding-right: 16px;\n  background: #e1e9ee;\n  padding-top: 10px;\n  padding-bottom: 20px;\n}\n\n.chips_div .chip {\n  background-color: white;\n  border-radius: 3px;\n  padding: 5px 20px;\n  margin-right: 10px;\n  white-space: nowrap;\n  color: var(--ion-color-main);\n  font-size: 12px;\n  font-weight: bold;\n}\n\n.slider_div {\n  background: #e1e9ee;\n  padding-top: 10px;\n}\n\n.slider_div ion-slides {\n  height: 200px;\n  margin-top: -35px;\n}\n\n.slider_div .slide_div {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  border: 1px solid lightgray;\n  background: white;\n  justify-content: center;\n}\n\n.slider_div .slide_div .up_div {\n  display: flex;\n  flex-direction: row;\n  padding: 2px;\n  width: 395px;\n}\n\n.slider_div .slide_div .img_div {\n  width: 40px;\n}\n\n.slider_div .slide_div .img_div .slider_img {\n  width: 30px;\n  height: 30px;\n  min-width: 30px;\n}\n\n.slider_div .slide_div .content_div {\n  margin-left: 10px;\n}\n\n.slider_div .slide_div .content_div .abs_lbl {\n  position: absolute;\n  left: 5px;\n  top: 0;\n  font-size: 12px;\n}\n\n.slider_div .slide_div .content_div .head_lbl {\n  display: block;\n  font-weight: 500;\n  text-align: left;\n  font-size: 14px;\n}\n\n.slider_div .slide_div .content_div .small_lbl {\n  color: #707070;\n  text-align: left;\n  display: block;\n  font-size: 14px;\n}\n\n.slider_div .slide_div .lower_div {\n  border-top: 1px solid lightgray;\n  padding: 3px;\n  display: flex;\n  width: 100%;\n  justify-content: space-around;\n}\n\n.slider_div .slide_div .lower_div ion-button {\n  color: var(--ion-color-main);\n  font-weight: 600;\n}\n\nion-scroll[scroll-avatar] {\n  height: 60px;\n}\n\n.notification_div {\n  padding: 15px 10px;\n  position: relative;\n  background: white;\n}\n\n.notification_div .time_div {\n  position: absolute;\n  right: 10px;\n  top: 5px;\n  color: gray;\n}\n\n.notification_div .flex_div {\n  display: flex;\n  width: 100%;\n  align-items: center;\n}\n\n.notification_div .flex_div .back_image {\n  height: 50px;\n  width: 50px;\n  min-width: 50px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.notification_div .flex_div .header_lbl {\n  font-weight: 600;\n  margin-left: 10px;\n  font-size: 14px;\n  width: 88%;\n}\n\n.notification_div .flex_div .desc {\n  margin-left: 10px;\n  font-size: 14px;\n  width: 88%;\n}\n\n.notification_div .btn_div {\n  padding-left: 60px;\n  margin-top: 5px;\n}\n\n.notification_div .btn_div ion-button {\n  font-weight: 600;\n  --border-radius: 3px;\n  --border-color: var(--ion-color-main);\n  --color: var(--ion-color-main);\n  font-size: 16px;\n}\n\n/* Hide ion-content scrollbar */\n\n::-webkit-scrollbar {\n  display: none;\n}\n\n.bck-orange {\n  background: #ff9800;\n}\n\n.bck-red {\n  background: #f4511e;\n}\n\n.bck-green {\n  background: #afb42b;\n}\n\n.bck-yellow {\n  background: #ffc400;\n}\n\n.bck-blue {\n  background: #64b5f6;\n}\n\n.bck-pink {\n  background: #f48fb1;\n}\n\n.md-layout .mbsc-scv-item {\n  color: #fff;\n  font-weight: bold;\n  text-align: center;\n}\n\n.md-layout .mbsc-scv-c {\n  margin: 10px 0;\n}\n\n.variable-1 {\n  width: 60px;\n  height: 60px;\n}\n\n.variable-2 {\n  width: 80px;\n  height: 80px;\n}\n\n.variable-3 {\n  width: 70px;\n  height: 70px;\n}\n\n.variable-4 {\n  width: 50px;\n  height: 50px;\n}\n\n.variable-5 {\n  width: 100px;\n  height: 100px;\n}\n\n.variable-6 {\n  width: 40px;\n  height: 40px;\n}\n\n.md-fixed .mbsc-scv-item {\n  height: 80px;\n  margin: 0 10px;\n}\n\n.md-variable .mbsc-scv-item {\n  margin: auto 10px;\n}\n\n.md-pages .mbsc-scv-item {\n  height: 80px;\n}\n\n.md-fullpage .mbsc-scv-item {\n  height: 630px;\n  font-size: 28px;\n}\n\n.header_div {\n  display: flex;\n  width: 100%;\n  justify-content: space-between;\n  align-items: center;\n  padding-left: 16px;\n  padding-right: 16px;\n  height: 40px;\n}\n\n.header_div .user_image {\n  height: 25px;\n  width: 25px;\n}\n\n.header_div ion-searchbar {\n  --background: white;\n  --placeholder-color: #707070;\n  --icon-color: #707070;\n  border-radius: 0px;\n  height: 30px;\n}\n\nion-content {\n  --background: #F5F5F5;\n}\n\n.blue_lbl {\n  color: var(--ion-color-main);\n  padding-top: 15px;\n  padding-bottom: 15px;\n  font-weight: 600;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div .head_lbl {\n  padding-top: 10px;\n  padding-bottom: 10px;\n}\n\n.main_content_div .white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n\n.main_content_div .white_div ion-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  right: 16px;\n  font-size: 20px;\n}\n\n.main_content_div .white_div ion-button {\n  color: var(--ion-color-main);\n  font-size: 600;\n}\n\n.main_content_div .flex_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  justify-content: space-between;\n  padding-top: 10px;\n  padding-bottom: 15px;\n  border-bottom: 1px solid lightgray;\n  background: white;\n}\n\n.main_content_div .flex_div .f_div {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding-left: 16px;\n}\n\n.main_content_div .flex_div .f_div .detail {\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  margin-left: 10px;\n  line-height: 20px;\n}\n\n.main_content_div .flex_div .f_div .detail .desc {\n  color: gray;\n}\n\n.main_content_div .flex_div .f_div .back_image {\n  height: 60px;\n  width: 60px;\n  min-width: 60px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.main_content_div .flex_div .f_div .time {\n  margin-left: 2px;\n}\n\n.main_content_div .flex_div .s_div {\n  display: flex;\n  align-items: center;\n  padding-right: 16px;\n}\n\n.main_content_div .flex_div .s_div ion-icon {\n  font-size: 40px;\n}\n\n.main_content_div .flex_div .s_div .cross {\n  color: gray;\n}\n\n.main_content_div .flex_div .s_div .check {\n  color: var(--ion-color-main);\n}\n\n.main_content_div .user_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n\n.main_content_div .user_div .col_div {\n  border: 1px solid lightgray;\n}\n\n.main_content_div .user_div ion-grid {\n  padding: 0;\n}\n\n.main_content_div .user_div .cover {\n  height: 50px;\n  width: 100%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  position: relative;\n}\n\n.main_content_div .user_div .cover .close {\n  font-size: 25px;\n  color: black;\n  position: absolute;\n  right: 5px;\n  top: 5px;\n}\n\n.main_content_div .user_div .back_image {\n  height: 75px;\n  width: 75px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  position: absolute;\n  left: 50%;\n  transform: translate(-50%);\n  top: 40%;\n  border: 3px solid white;\n}\n\n.main_content_div .user_div .username {\n  margin-top: 45px;\n  text-align: center;\n  text-align: center;\n}\n\n.main_content_div .user_div .detail {\n  margin-top: 5px;\n  color: gray;\n  text-align: center;\n  font-size: 14px;\n}\n\n.main_content_div .user_div .mutual_div {\n  display: flex;\n  justify-content: center;\n  margin-top: 8px;\n}\n\n.main_content_div .user_div .mutual_div .link {\n  width: 15px;\n  height: 15px;\n}\n\n.main_content_div .user_div .mutual_div .link_lbl {\n  font-size: 12px;\n  color: gray;\n  margin-left: 5px;\n}\n\n.main_content_div .user_div ion-button {\n  --border-radius: 3px;\n  color: var(--ion-color-main);\n  --border-color: var(--ion-color-main);\n  font-weight: 600px;\n  margin: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVtYmVycy1hY3Rpdml0eS9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxtZW1iZXJzLWFjdGl2aXR5XFxtZW1iZXJzLWFjdGl2aXR5LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbWVtYmVycy1hY3Rpdml0eS9tZW1iZXJzLWFjdGl2aXR5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1DQUFBO0FDQ0o7O0FEQ0E7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0FDRUo7O0FEQUE7RUFDSSxZQUFBO0FDR0o7O0FEREE7RUFDSSxlQUFBO0FDSUo7O0FERkE7RUFDSSxlQUFBO0FDS0o7O0FESEE7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLDRCQUFBO0FDTUo7O0FESkE7RUFDSSx5QkFBQTtFQUNBLHdCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FDT0o7O0FETEk7RUFDSSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQ09SOztBRExJO0VBQ0ksZUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ09SOztBREZBO0VBQ0ksbUJBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUNLSjs7QURISTtFQUNFLHFCQUFBO0FDS047O0FERkk7RUFDRSwyQkFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtBQ0lOOztBRERJO0VBQ0UsWUFBQTtBQ0dOOztBREFFO0VBQ0Usa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0dKOztBRERBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0FDSUo7O0FEREk7RUFDSSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNHUjs7QURDQTtFQUNJLG1CQUFBO0VBQ0EsaUJBQUE7QUNFSjs7QURBSTtFQUNJLGFBQUE7RUFDQSxpQkFBQTtBQ0VSOztBRENJO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBRUEsbUJBQUE7RUFDQSwyQkFBQTtFQUVBLGlCQUFBO0VBRUEsdUJBQUE7QUNGUjs7QURJUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FDRlo7O0FES1E7RUFDSSxXQUFBO0FDSFo7O0FES1k7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNIaEI7O0FETVE7RUFDSSxpQkFBQTtBQ0paOztBRE9ZO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsTUFBQTtFQUNBLGVBQUE7QUNMaEI7O0FET1k7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNMaEI7O0FET1k7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQ0xoQjs7QURRUTtFQUNJLCtCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsNkJBQUE7QUNOWjs7QURRWTtFQUNJLDRCQUFBO0VBQ0EsZ0JBQUE7QUNOaEI7O0FEV0U7RUFDRSxZQUFBO0FDUko7O0FEVUU7RUFDRSxrQkFBQTtFQUVBLGtCQUFBO0VBQ0EsaUJBQUE7QUNSSjs7QURVSTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0FDUlI7O0FEVUk7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0FDUlI7O0FEU1E7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQ1BaOztBRFVRO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0FDUlo7O0FEVVE7RUFDSSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0FDUlo7O0FEWUk7RUFDSSxrQkFBQTtFQUNBLGVBQUE7QUNWUjs7QURZUTtFQUNJLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQ0FBQTtFQUNBLDhCQUFBO0VBQ0EsZUFBQTtBQ1ZaOztBRGVFLCtCQUFBOztBQUNBO0VBQ0UsYUFBQTtBQ1pKOztBRGNFO0VBQ0UsbUJBQUE7QUNYSjs7QURjQTtFQUNJLG1CQUFBO0FDWEo7O0FEY0E7RUFDSSxtQkFBQTtBQ1hKOztBRGNBO0VBQ0ksbUJBQUE7QUNYSjs7QURjQTtFQUNJLG1CQUFBO0FDWEo7O0FEY0E7RUFDSSxtQkFBQTtBQ1hKOztBRGNBO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNYSjs7QURjQTtFQUNJLGNBQUE7QUNYSjs7QURjQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDWEo7O0FEY0E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ1hKOztBRGNBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNYSjs7QURjQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDWEo7O0FEY0E7RUFDSSxZQUFBO0VBQ0EsYUFBQTtBQ1hKOztBRGNBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNYSjs7QURjQTtFQUNJLFlBQUE7RUFDQSxjQUFBO0FDWEo7O0FEY0E7RUFDSSxpQkFBQTtBQ1hKOztBRGNBO0VBQ0ksWUFBQTtBQ1hKOztBRGNBO0VBQ0ksYUFBQTtFQUNBLGVBQUE7QUNYSjs7QURhQTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FDVko7O0FEWUk7RUFDSSxZQUFBO0VBQ0EsV0FBQTtBQ1ZSOztBRFlJO0VBQ0ksbUJBQUE7RUFDQSw0QkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDVlI7O0FEY0E7RUFDSSxxQkFBQTtBQ1hKOztBRGFBO0VBQ0ksNEJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7QUNWSjs7QURZQTtFQUNJLFdBQUE7QUNUSjs7QURXSTtFQUNJLGNBQUE7QUNUUjs7QURZSTtFQUNJLGlCQUFBO0VBQ0Esb0JBQUE7QUNWUjs7QURhSTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDWFI7O0FEMEJRO0VBQ0ksNEJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FDeEJaOztBRDJCUTtFQUNJLDRCQUFBO0VBQ0EsY0FBQTtBQ3pCWjs7QUQ2Qkk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsOEJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0NBQUE7RUFDQSxpQkFBQTtBQzNCUjs7QUQ4QlE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDNUJaOztBRDhCWTtFQUNJLDBCQUFBO0VBQUEsdUJBQUE7RUFBQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUM1QmhCOztBRDZCZ0I7RUFDSSxXQUFBO0FDM0JwQjs7QUQ4Qlk7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQzVCaEI7O0FEOEJZO0VBQ0ksZ0JBQUE7QUM1QmhCOztBRGlDUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDL0JaOztBRGdDWTtFQUNJLGVBQUE7QUM5QmhCOztBRGlDWTtFQUNJLFdBQUE7QUMvQmhCOztBRGlDWTtFQUNJLDRCQUFBO0FDL0JoQjs7QURvQ0k7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ2xDUjs7QURvQ1E7RUFDSSwyQkFBQTtBQ2xDWjs7QURxQ1E7RUFDSSxVQUFBO0FDbkNaOztBRHNDUTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7QUNwQ1o7O0FEc0NZO0VBQ0ksZUFBQTtFQUVBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0FDckNoQjs7QUR3Q1E7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsMEJBQUE7RUFDQSxRQUFBO0VBQ0EsdUJBQUE7QUN0Q1o7O0FEeUNRO0VBQ0ksZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDdkNaOztBRHlDUTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDdkNaOztBRDBDUTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7QUN4Q1o7O0FEeUNZO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUN2Q2hCOztBRHlDWTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUN2Q2hCOztBRDJDUTtFQUNJLG9CQUFBO0VBQ0EsNEJBQUE7RUFDQSxxQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ3pDWiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21lbWJlcnMtYWN0aXZpdHkvbWVtYmVycy1hY3Rpdml0eS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaHtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5pb24tY2FyZC1oZWFkZXJ7XG4gICAgcGFkZGluZzogNXB4O1xufVxuaW9uLWNhcmQtdGl0bGV7XG4gICAgZm9udC1zaXplOiAxN3B4O1xufVxuaW9uLWNhcmQtY29udGVudHtcbiAgICBmb250LXNpemU6IDE0cHg7XG59XG4uY291bnR7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuLm5vdGZvdW5ke1xuICAgIGJvcmRlcjogNHB4IHNvbGlkICMwZTc2YTg7XG4gICAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xuICAgIG1hcmdpbjogMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuXG4gICAgaW9uLWljb257XG4gICAgICAgIGJhY2tncm91bmQ6ICMwZTc2YTg7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBwYWRkaW5nOiAzcHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICB9XG4gICAgaW9uLWxhYmVse1xuICAgICAgICBmb250LXNpemU6IDE3cHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDdweDtcbiAgICB9XG5cbn1cblxuaW9uLXNjcm9sbFtzY3JvbGxYXSB7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICBoZWlnaHQ6IDEyMHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG5cbiAgICAuc2Nyb2xsLWl0ZW0ge1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIH1cblxuICAgIC5zZWxlY3RhYmxlLWljb257XG4gICAgICBtYXJnaW46IDEwcHggMjBweCAxMHB4IDIwcHg7XG4gICAgICBjb2xvcjogcmVkO1xuICAgICAgZm9udC1zaXplOiAxMDBweDtcbiAgICB9XG5cbiAgICBpb24tYXZhdGFyIGltZ3tcbiAgICAgIG1hcmdpbjogMTBweDtcbiAgICB9XG4gIH1cbiAgLnVwX2xibHtcbiAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZDogI2UxZTllZTtcbn1cbi5jaGlwc19kaXZ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIG92ZXJmbG93OiBzY3JvbGw7XG4gICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgYmFja2dyb3VuZDogI2UxZTllZTtcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcblxuXG4gICAgLmNoaXB7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgIHBhZGRpbmc6IDVweCAyMHB4O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgfVxufVxuXG4uc2xpZGVyX2RpdntcbiAgICBiYWNrZ3JvdW5kOiAjZTFlOWVlOyBcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcblxuICAgIGlvbi1zbGlkZXN7XG4gICAgICAgIGhlaWdodDogMjAwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IC0zNXB4O1xuICAgIH1cblxuICAgIC5zbGlkZV9kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgLy8gcGFkZGluZzogMjBweCAxNXB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgIFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAgICAgICAudXBfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgICAgICBwYWRkaW5nOiAycHg7XG4gICAgICAgICAgICB3aWR0aDogMzk1cHg7XG4gICAgICAgIH1cblxuICAgICAgICAuaW1nX2RpdntcbiAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgLy8gcGFkZGluZzogMTVweDtcbiAgICAgICAgICAgIC5zbGlkZXJfaW1ne1xuICAgICAgICAgICAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgICAgICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgICAgICAgICBtaW4td2lkdGg6IDMwcHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmNvbnRlbnRfZGl2e1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAvLyBwYWRkaW5nOiAxNXB4O1xuXG4gICAgICAgICAgICAuYWJzX2xibHtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgbGVmdDogNXB4O1xuICAgICAgICAgICAgICAgIHRvcDogMDtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuaGVhZF9sYmx7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5zbWFsbF9sYmx7XG4gICAgICAgICAgICAgICAgY29sb3I6ICM3MDcwNzA7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmxvd2VyX2RpdntcbiAgICAgICAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgICAgICBwYWRkaW5nOiAzcHg7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcblxuICAgICAgICAgICAgaW9uLWJ1dHRvbntcbiAgICAgICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG4gIGlvbi1zY3JvbGxbc2Nyb2xsLWF2YXRhcl17XG4gICAgaGVpZ2h0OiA2MHB4O1xuICB9XG4gIC5ub3RpZmljYXRpb25fZGl2e1xuICAgIHBhZGRpbmc6IDE1cHggMTBweDtcbiAgIFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgXG4gICAgLnRpbWVfZGl2e1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHJpZ2h0OiAxMHB4O1xuICAgICAgICB0b3A6IDVweDtcbiAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgfVxuICAgIC5mbGV4X2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIC5iYWNrX2ltYWdle1xuICAgICAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICAgICAgICBtaW4td2lkdGg6IDUwcHg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgfVxuXG4gICAgICAgIC5oZWFkZXJfbGJse1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgd2lkdGg6IDg4JTtcbiAgICAgICAgfVxuICAgICAgICAuZGVzY3tcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgd2lkdGg6IDg4JTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5idG5fZGl2e1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDYwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDVweDtcblxuICAgICAgICBpb24tYnV0dG9ue1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICAgICAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgfVxuICAgIH1cbiAgICBcbn1cbiAgLyogSGlkZSBpb24tY29udGVudCBzY3JvbGxiYXIgKi9cbiAgOjotd2Via2l0LXNjcm9sbGJhcntcbiAgICBkaXNwbGF5Om5vbmU7XG4gIH1cbiAgLmJjay1vcmFuZ2Uge1xuICAgIGJhY2tncm91bmQ6ICNmZjk4MDA7XG59XG5cbi5iY2stcmVkIHtcbiAgICBiYWNrZ3JvdW5kOiAjZjQ1MTFlO1xufVxuXG4uYmNrLWdyZWVuIHtcbiAgICBiYWNrZ3JvdW5kOiAjYWZiNDJiO1xufVxuXG4uYmNrLXllbGxvdyB7XG4gICAgYmFja2dyb3VuZDogI2ZmYzQwMDtcbn1cblxuLmJjay1ibHVlIHtcbiAgICBiYWNrZ3JvdW5kOiAjNjRiNWY2O1xufVxuXG4uYmNrLXBpbmsge1xuICAgIGJhY2tncm91bmQ6ICNmNDhmYjE7XG59XG5cbi5tZC1sYXlvdXQgLm1ic2Mtc2N2LWl0ZW0ge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLm1kLWxheW91dCAubWJzYy1zY3YtYyB7XG4gICAgbWFyZ2luOiAxMHB4IDA7XG59XG5cbi52YXJpYWJsZS0xIHtcbiAgICB3aWR0aDogNjBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG59XG5cbi52YXJpYWJsZS0yIHtcbiAgICB3aWR0aDogODBweDtcbiAgICBoZWlnaHQ6IDgwcHg7XG59XG5cbi52YXJpYWJsZS0zIHtcbiAgICB3aWR0aDogNzBweDtcbiAgICBoZWlnaHQ6IDcwcHg7XG59XG5cbi52YXJpYWJsZS00IHtcbiAgICB3aWR0aDogNTBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG59XG5cbi52YXJpYWJsZS01IHtcbiAgICB3aWR0aDogMTAwcHg7XG4gICAgaGVpZ2h0OiAxMDBweDtcbn1cblxuLnZhcmlhYmxlLTYge1xuICAgIHdpZHRoOiA0MHB4O1xuICAgIGhlaWdodDogNDBweDtcbn1cblxuLm1kLWZpeGVkIC5tYnNjLXNjdi1pdGVtIHtcbiAgICBoZWlnaHQ6IDgwcHg7XG4gICAgbWFyZ2luOiAwIDEwcHg7XG59XG5cbi5tZC12YXJpYWJsZSAubWJzYy1zY3YtaXRlbSB7XG4gICAgbWFyZ2luOiBhdXRvIDEwcHg7XG59XG5cbi5tZC1wYWdlcyAubWJzYy1zY3YtaXRlbSB7XG4gICAgaGVpZ2h0OiA4MHB4O1xufVxuXG4ubWQtZnVsbHBhZ2UgLm1ic2Mtc2N2LWl0ZW0ge1xuICAgIGhlaWdodDogNjMwcHg7XG4gICAgZm9udC1zaXplOiAyOHB4O1xufVxuLmhlYWRlcl9kaXZ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgICAgXG4gICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xuXG4gICAgLnVzZXJfaW1hZ2V7XG4gICAgICAgIGhlaWdodDogMjVweDtcbiAgICAgICAgd2lkdGg6IDI1cHg7XG4gICAgfVxuICAgIGlvbi1zZWFyY2hiYXJ7XG4gICAgICAgIC0tYmFja2dyb3VuZDogd2hpdGU7ICAgXG4gICAgICAgIC0tcGxhY2Vob2xkZXItY29sb3I6ICAjNzA3MDcwOyBcbiAgICAgICAgLS1pY29uLWNvbG9yIDogIzcwNzA3MDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMHB4O1xuICAgICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgfVxufVxuXG5pb24tY29udGVudHtcbiAgICAtLWJhY2tncm91bmQ6ICNGNUY1RjU7XG59XG4uYmx1ZV9sYmx7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICBwYWRkaW5nLXRvcDogMTVweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXZ7XG4gICAgd2lkdGg6IDEwMCU7XG5cbiAgICBpb24tbGFiZWwge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG5cbiAgICAuaGVhZF9sYmx7XG4gICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgICB9XG5cbiAgICAud2hpdGVfZGl2e1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICAvLyBpb24tYnV0dG9ue1xuICAgICAgICAvLyAgICAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgICAgIC8vICAgICBpb24tbGFiZWwge1xuICAgICAgICAvLyAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIC8vICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIC8vICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAvLyAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIC8vICAgICB9ICAgXG4gICAgICAgIC8vICAgICBpb24taWNvbntcbiAgICAgICAgLy8gICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAvLyAgICAgfVxuICAgICAgICAvLyB9XG5cbiAgICAgICAgXG4gICAgICAgIGlvbi1pY29ue1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIHJpZ2h0OiAxNnB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICB9XG5cbiAgICAgICAgaW9uLWJ1dHRvbntcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBmb250LXNpemU6IDYwMDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5mbGV4X2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcblxuXG4gICAgICAgIC5mX2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTZweDtcblxuICAgICAgICAgICAgLmRldGFpbHtcbiAgICAgICAgICAgICAgICB3aWR0aDogbWF4LWNvbnRlbnQ7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICAgICAgICAgICAgLmRlc2N7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOmdyYXk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmJhY2tfaW1hZ2V7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA2MHB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiA2MHB4O1xuICAgICAgICAgICAgICAgIG1pbi13aWR0aDogNjBweDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC50aW1le1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAycHg7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAuc19kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgICAgICAgICBpb24taWNvbntcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDQwcHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5jcm9zc3tcbiAgICAgICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5jaGVja3tcbiAgICAgICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLnVzZXJfZGl2e1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuXG4gICAgICAgIC5jb2xfZGl2e1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICB9XG5cbiAgICAgICAgaW9uLWdyaWR7XG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICAuY292ZXJ7XG4gICAgICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICAgICAgICAgICAuY2xvc2V7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgICAgICAgICAgIC8vIGNvbG9yOiAjNTA1MDUwO1xuICAgICAgICAgICAgICAgIGNvbG9yOiBibGFjaztcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IDVweDtcbiAgICAgICAgICAgICAgICB0b3A6IDVweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuYmFja19pbWFnZXtcbiAgICAgICAgICAgIGhlaWdodDogNzVweDtcbiAgICAgICAgICAgIHdpZHRoOiA3NXB4O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICBsZWZ0OiA1MCU7XG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlKTtcbiAgICAgICAgICAgIHRvcDogNDAlO1xuICAgICAgICAgICAgYm9yZGVyOiAzcHggc29saWQgd2hpdGU7XG4gICAgICAgIH1cblxuICAgICAgICAudXNlcm5hbWV7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA0NXB4O1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICB9XG4gICAgICAgIC5kZXRhaWx7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5tdXR1YWxfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogOHB4O1xuICAgICAgICAgICAgLmxpbmt7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDE1cHg7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmxpbmtfbGJse1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaW9uLWJ1dHRvbntcbiAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIC0tYm9yZGVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwcHg7XG4gICAgICAgICAgICBtYXJnaW46IDEwcHg7XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG5pb24tY2FyZC1oZWFkZXIge1xuICBwYWRkaW5nOiA1cHg7XG59XG5cbmlvbi1jYXJkLXRpdGxlIHtcbiAgZm9udC1zaXplOiAxN3B4O1xufVxuXG5pb24tY2FyZC1jb250ZW50IHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uY291bnQge1xuICBmb250LXNpemU6IDEzcHg7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuLm5vdGZvdW5kIHtcbiAgYm9yZGVyOiA0cHggc29saWQgIzBlNzZhODtcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xuICBtYXJnaW46IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG4ubm90Zm91bmQgaW9uLWljb24ge1xuICBiYWNrZ3JvdW5kOiAjMGU3NmE4O1xuICBjb2xvcjogI2ZmZjtcbiAgcGFkZGluZzogM3B4O1xuICBmb250LXNpemU6IDI1cHg7XG59XG4ubm90Zm91bmQgaW9uLWxhYmVsIHtcbiAgZm9udC1zaXplOiAxN3B4O1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1sZWZ0OiA3cHg7XG59XG5cbmlvbi1zY3JvbGxbc2Nyb2xsWF0ge1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBoZWlnaHQ6IDEyMHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuaW9uLXNjcm9sbFtzY3JvbGxYXSAuc2Nyb2xsLWl0ZW0ge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5pb24tc2Nyb2xsW3Njcm9sbFhdIC5zZWxlY3RhYmxlLWljb24ge1xuICBtYXJnaW46IDEwcHggMjBweCAxMHB4IDIwcHg7XG4gIGNvbG9yOiByZWQ7XG4gIGZvbnQtc2l6ZTogMTAwcHg7XG59XG5pb24tc2Nyb2xsW3Njcm9sbFhdIGlvbi1hdmF0YXIgaW1nIHtcbiAgbWFyZ2luOiAxMHB4O1xufVxuXG4udXBfbGJsIHtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJhY2tncm91bmQ6ICNlMWU5ZWU7XG59XG5cbi5jaGlwc19kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBvdmVyZmxvdzogc2Nyb2xsO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIGJhY2tncm91bmQ6ICNlMWU5ZWU7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbn1cbi5jaGlwc19kaXYgLmNoaXAge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICBwYWRkaW5nOiA1cHggMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uc2xpZGVyX2RpdiB7XG4gIGJhY2tncm91bmQ6ICNlMWU5ZWU7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuLnNsaWRlcl9kaXYgaW9uLXNsaWRlcyB7XG4gIGhlaWdodDogMjAwcHg7XG4gIG1hcmdpbi10b3A6IC0zNXB4O1xufVxuLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAudXBfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgcGFkZGluZzogMnB4O1xuICB3aWR0aDogMzk1cHg7XG59XG4uc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5pbWdfZGl2IHtcbiAgd2lkdGg6IDQwcHg7XG59XG4uc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5pbWdfZGl2IC5zbGlkZXJfaW1nIHtcbiAgd2lkdGg6IDMwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgbWluLXdpZHRoOiAzMHB4O1xufVxuLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAuY29udGVudF9kaXYge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmNvbnRlbnRfZGl2IC5hYnNfbGJsIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1cHg7XG4gIHRvcDogMDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAuY29udGVudF9kaXYgLmhlYWRfbGJsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmNvbnRlbnRfZGl2IC5zbWFsbF9sYmwge1xuICBjb2xvcjogIzcwNzA3MDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmxvd2VyX2RpdiB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIHBhZGRpbmc6IDNweDtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xufVxuLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAubG93ZXJfZGl2IGlvbi1idXR0b24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG5pb24tc2Nyb2xsW3Njcm9sbC1hdmF0YXJdIHtcbiAgaGVpZ2h0OiA2MHB4O1xufVxuXG4ubm90aWZpY2F0aW9uX2RpdiB7XG4gIHBhZGRpbmc6IDE1cHggMTBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbi5ub3RpZmljYXRpb25fZGl2IC50aW1lX2RpdiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDEwcHg7XG4gIHRvcDogNXB4O1xuICBjb2xvcjogZ3JheTtcbn1cbi5ub3RpZmljYXRpb25fZGl2IC5mbGV4X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLm5vdGlmaWNhdGlvbl9kaXYgLmZsZXhfZGl2IC5iYWNrX2ltYWdlIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogNTBweDtcbiAgbWluLXdpZHRoOiA1MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cbi5ub3RpZmljYXRpb25fZGl2IC5mbGV4X2RpdiAuaGVhZGVyX2xibCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIHdpZHRoOiA4OCU7XG59XG4ubm90aWZpY2F0aW9uX2RpdiAuZmxleF9kaXYgLmRlc2Mge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICB3aWR0aDogODglO1xufVxuLm5vdGlmaWNhdGlvbl9kaXYgLmJ0bl9kaXYge1xuICBwYWRkaW5nLWxlZnQ6IDYwcHg7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5ub3RpZmljYXRpb25fZGl2IC5idG5fZGl2IGlvbi1idXR0b24ge1xuICBmb250LXdlaWdodDogNjAwO1xuICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXNpemU6IDE2cHg7XG59XG5cbi8qIEhpZGUgaW9uLWNvbnRlbnQgc2Nyb2xsYmFyICovXG46Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLmJjay1vcmFuZ2Uge1xuICBiYWNrZ3JvdW5kOiAjZmY5ODAwO1xufVxuXG4uYmNrLXJlZCB7XG4gIGJhY2tncm91bmQ6ICNmNDUxMWU7XG59XG5cbi5iY2stZ3JlZW4ge1xuICBiYWNrZ3JvdW5kOiAjYWZiNDJiO1xufVxuXG4uYmNrLXllbGxvdyB7XG4gIGJhY2tncm91bmQ6ICNmZmM0MDA7XG59XG5cbi5iY2stYmx1ZSB7XG4gIGJhY2tncm91bmQ6ICM2NGI1ZjY7XG59XG5cbi5iY2stcGluayB7XG4gIGJhY2tncm91bmQ6ICNmNDhmYjE7XG59XG5cbi5tZC1sYXlvdXQgLm1ic2Mtc2N2LWl0ZW0ge1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLm1kLWxheW91dCAubWJzYy1zY3YtYyB7XG4gIG1hcmdpbjogMTBweCAwO1xufVxuXG4udmFyaWFibGUtMSB7XG4gIHdpZHRoOiA2MHB4O1xuICBoZWlnaHQ6IDYwcHg7XG59XG5cbi52YXJpYWJsZS0yIHtcbiAgd2lkdGg6IDgwcHg7XG4gIGhlaWdodDogODBweDtcbn1cblxuLnZhcmlhYmxlLTMge1xuICB3aWR0aDogNzBweDtcbiAgaGVpZ2h0OiA3MHB4O1xufVxuXG4udmFyaWFibGUtNCB7XG4gIHdpZHRoOiA1MHB4O1xuICBoZWlnaHQ6IDUwcHg7XG59XG5cbi52YXJpYWJsZS01IHtcbiAgd2lkdGg6IDEwMHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xufVxuXG4udmFyaWFibGUtNiB7XG4gIHdpZHRoOiA0MHB4O1xuICBoZWlnaHQ6IDQwcHg7XG59XG5cbi5tZC1maXhlZCAubWJzYy1zY3YtaXRlbSB7XG4gIGhlaWdodDogODBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59XG5cbi5tZC12YXJpYWJsZSAubWJzYy1zY3YtaXRlbSB7XG4gIG1hcmdpbjogYXV0byAxMHB4O1xufVxuXG4ubWQtcGFnZXMgLm1ic2Mtc2N2LWl0ZW0ge1xuICBoZWlnaHQ6IDgwcHg7XG59XG5cbi5tZC1mdWxscGFnZSAubWJzYy1zY3YtaXRlbSB7XG4gIGhlaWdodDogNjMwcHg7XG4gIGZvbnQtc2l6ZTogMjhweDtcbn1cblxuLmhlYWRlcl9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIGhlaWdodDogNDBweDtcbn1cbi5oZWFkZXJfZGl2IC51c2VyX2ltYWdlIHtcbiAgaGVpZ2h0OiAyNXB4O1xuICB3aWR0aDogMjVweDtcbn1cbi5oZWFkZXJfZGl2IGlvbi1zZWFyY2hiYXIge1xuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLXBsYWNlaG9sZGVyLWNvbG9yOiAjNzA3MDcwO1xuICAtLWljb24tY29sb3I6ICM3MDcwNzA7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgaGVpZ2h0OiAzMHB4O1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogI0Y1RjVGNTtcbn1cblxuLmJsdWVfbGJsIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4ubWFpbl9jb250ZW50X2RpdiB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm1haW5fY29udGVudF9kaXYgaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGVhZF9sYmwge1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAud2hpdGVfZGl2IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC53aGl0ZV9kaXYgaW9uLWljb24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxNnB4O1xuICBmb250LXNpemU6IDIwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAud2hpdGVfZGl2IGlvbi1idXR0b24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXNpemU6IDYwMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIHdpZHRoOiAxMDAlO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmZfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5mX2RpdiAuZGV0YWlsIHtcbiAgd2lkdGg6IG1heC1jb250ZW50O1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmZfZGl2IC5kZXRhaWwgLmRlc2Mge1xuICBjb2xvcjogZ3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuZl9kaXYgLmJhY2tfaW1hZ2Uge1xuICBoZWlnaHQ6IDYwcHg7XG4gIHdpZHRoOiA2MHB4O1xuICBtaW4td2lkdGg6IDYwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5mX2RpdiAudGltZSB7XG4gIG1hcmdpbi1sZWZ0OiAycHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLnNfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuc19kaXYgaW9uLWljb24ge1xuICBmb250LXNpemU6IDQwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLnNfZGl2IC5jcm9zcyB7XG4gIGNvbG9yOiBncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5zX2RpdiAuY2hlY2sge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiAuY29sX2RpdiB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiBpb24tZ3JpZCB7XG4gIHBhZGRpbmc6IDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgLmNvdmVyIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgLmNvdmVyIC5jbG9zZSB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgY29sb3I6IGJsYWNrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiA1cHg7XG4gIHRvcDogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IC5iYWNrX2ltYWdlIHtcbiAgaGVpZ2h0OiA3NXB4O1xuICB3aWR0aDogNzVweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlKTtcbiAgdG9wOiA0MCU7XG4gIGJvcmRlcjogM3B4IHNvbGlkIHdoaXRlO1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IC51c2VybmFtZSB7XG4gIG1hcmdpbi10b3A6IDQ1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IC5kZXRhaWwge1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIGNvbG9yOiBncmF5O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiAubXV0dWFsX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBtYXJnaW4tdG9wOiA4cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgLm11dHVhbF9kaXYgLmxpbmsge1xuICB3aWR0aDogMTVweDtcbiAgaGVpZ2h0OiAxNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IC5tdXR1YWxfZGl2IC5saW5rX2xibCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6IGdyYXk7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgaW9uLWJ1dHRvbiB7XG4gIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogNjAwcHg7XG4gIG1hcmdpbjogMTBweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/members-activity/members-activity.page.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/members-activity/members-activity.page.ts ***!
    \*****************************************************************/

  /*! exports provided: MembersActivityPage */

  /***/
  function srcAppPagesMembersActivityMembersActivityPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MembersActivityPage", function () {
      return MembersActivityPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/services/dummy-data.service */
    "./src/app/services/dummy-data.service.ts");
    /* harmony import */


    var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic-native/native-page-transitions/ngx */
    "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var MembersActivityPage = /*#__PURE__*/function () {
      function MembersActivityPage(navCtrl, toastCtrl, nativePageTransitions, router, storage, dummy, api, menuCtrl) {
        var _this = this;

        _classCallCheck(this, MembersActivityPage);

        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.router = router;
        this.storage = storage;
        this.dummy = dummy;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.members = [];
        this.activities = [];
        this.groups = [];
        this.notifications = [];
        this.forums = [];
        this.users = [{
          img: 'assets/imgs/user.jpg',
          cover: 'assets/imgs/back1.jpg'
        }, {
          img: 'assets/imgs/user2.jpg',
          cover: 'assets/imgs/back2.jpg'
        }, {
          img: 'assets/imgs/user3.jpg',
          cover: 'assets/imgs/back3.jpg'
        }, {
          img: 'assets/imgs/user4.jpg',
          cover: 'assets/imgs/back4.jpg'
        }];
        this.load = false;
        this.showSlider = false;
        this.options = {
          direction: 'up',
          duration: 500,
          slowdownfactor: 3,
          slidePixels: 20,
          iosdelay: 100,
          androiddelay: 150,
          fixedPixelsTop: 0,
          fixedPixelsBottom: 60
        };
        this.showtab = true;
        this.load = true;
        this.storage.get('COMPLETE_USER_INFO').then(function (result) {
          if (result != null) {
            console.log(result);
            _this.token = result.token;
            _this.user_id = result.user_id;
            console.log('user_id: ' + result.user_id);
            console.log('token: ' + result.token);
          }
        })["catch"](function (e) {
          console.log('error: ' + e); // Handle errors here
        });
        this.loadMembers();
        this.dummyData = this.dummy.users;
      }

      _createClass(MembersActivityPage, [{
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.nativePageTransitions.slide(this.options);
        }
      }, {
        key: "connectuser",
        value: function connectuser(id) {
          var _this2 = this;

          this.api.connectfrds(id, this.token, this.user_id).subscribe(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var index, data, toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      console.log(res);
                      index = this.members.findIndex(function (data) {
                        return data.id === id;
                      });
                      data = this.members.find(function (data) {
                        return data.id === id;
                      });
                      console.log(data);

                      if (typeof index !== 'undefined') {
                        data.friendship_status = "pending";
                        this.members.splice(index, 1, data);
                      }

                      _context.next = 7;
                      return this.toastCtrl.create({
                        message: "Friend Request Sent Successfully",
                        duration: 3000
                      });

                    case 7:
                      toast = _context.sent;
                      _context.next = 10;
                      return toast.present();

                    case 10:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          });
        } // example of adding a transition when pushing a new page

      }, {
        key: "openPage",
        value: function openPage(page) {
          this.nativePageTransitions.slide(this.options);
          this.navCtrl.navigateForward(page);
        }
      }, {
        key: "loadMembers",
        value: function loadMembers() {
          var _this3 = this;

          this.api.getMembers(this.token).subscribe(function (res) {
            console.log(res);
            _this3.members = res;
          });
          this.api.getActivities().subscribe(function (res) {
            console.log(res);
            _this3.activities = res;
          });
          this.api.getGroups().subscribe(function (res) {
            console.log(res);
            _this3.groups = res;
          });
          this.api.getNotifications().subscribe(function (res) {
            console.log(res);
            _this3.notifications = res;
          });
          this.api.getForums().subscribe(function (res) {
            console.log(res);
            _this3.forums = res;
          });
          this.load = false;
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "myHeaderFn",
        value: function myHeaderFn(record, recordIndex, records) {
          if (recordIndex % 20 === 0) {
            return 'Header ' + recordIndex;
          }

          return null;
        }
      }, {
        key: "goToPeopleProfile",
        value: function goToPeopleProfile() {
          this.router.navigate(['/people-profile']);
        }
      }, {
        key: "openMenu",
        value: function openMenu() {
          this.menuCtrl.open();
        }
      }, {
        key: "goToMemberslist",
        value: function goToMemberslist() {
          this.router.navigate(['/members-list']);
        }
      }, {
        key: "goToGroupslist",
        value: function goToGroupslist() {
          this.router.navigate(['/groups-list']);
        }
      }, {
        key: "sliderShow",
        value: function sliderShow(value) {
          this.showSlider = value;
        }
      }, {
        key: "gotoactivitylist",
        value: function gotoactivitylist() {
          this.router.navigate(['/activity-list']);
        }
      }, {
        key: "goToForumslist",
        value: function goToForumslist() {
          this.router.navigate(['/forums-list']);
        }
      }, {
        key: "gotoGroup",
        value: function gotoGroup() {
          this.router.navigate(['/group-detail']);
        }
      }, {
        key: "onSearchChange",
        value: function onSearchChange(event) {}
      }, {
        key: "goToChatList",
        value: function goToChatList() {
          this.router.navigate(['/chatlist']);
        }
      }]);

      return MembersActivityPage;
    }();

    MembersActivityPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"]
      }, {
        type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__["NativePageTransitions"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"]
      }, {
        type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__["dummyDataService"]
      }, {
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"]
      }];
    };

    MembersActivityPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-members-activity',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./members-activity.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/members-activity/members-activity.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./members-activity.page.scss */
      "./src/app/pages/members-activity/members-activity.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"], _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__["NativePageTransitions"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__["dummyDataService"], _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"]])], MembersActivityPage);
    /***/
  },

  /***/
  "./src/app/services/dummy-data.service.ts":
  /*!************************************************!*\
    !*** ./src/app/services/dummy-data.service.ts ***!
    \************************************************/

  /*! exports provided: dummyDataService */

  /***/
  function srcAppServicesdummyDataServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "dummyDataService", function () {
      return dummyDataService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var dummyDataService = function dummyDataService() {
      _classCallCheck(this, dummyDataService);

      this.users = [{
        img: 'assets/imgs/user.jpg',
        cover: 'assets/imgs/back1.jpg',
        btn_text: 'VIEW JOBS'
      }, {
        img: 'assets/imgs/user2.jpg',
        cover: 'assets/imgs/back2.jpg',
        btn_text: 'FOLLOW HASHTAG'
      }, {
        img: 'assets/imgs/user3.jpg',
        cover: 'assets/imgs/back3.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user4.jpg',
        cover: 'assets/imgs/back4.jpg',
        btn_text: 'SEE ALL VIEW'
      }, {
        img: 'assets/imgs/user.jpg',
        cover: 'assets/imgs/back1.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user2.jpg',
        cover: 'assets/imgs/back2.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user3.jpg',
        cover: 'assets/imgs/back3.jpg',
        btn_text: 'EXPAND YOUR NETWORK'
      }, {
        img: 'assets/imgs/user4.jpg',
        cover: 'assets/imgs/back4.jpg',
        btn_text: 'SEE ALL VIEW'
      }];
      this.jobs = [{
        img: 'assets/imgs/company/initappz.png',
        title: 'SQL Server',
        addr: 'Bhavnagar, Gujrat, India',
        time: '2 school alumni',
        status: '1 day'
      }, {
        img: 'assets/imgs/company/google.png',
        title: 'Web Designer',
        addr: 'Vadodara, Gujrat, India',
        time: 'Be an early applicant',
        status: '2 days'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        title: 'Business Analyst',
        addr: 'Ahmedabad, Gujrat, India',
        time: 'Be an early applicant',
        status: '1 week'
      }, {
        img: 'assets/imgs/company/initappz.png',
        title: 'PHP Developer',
        addr: 'Pune, Maharashtra, India',
        time: '2 school alumni',
        status: 'New'
      }, {
        img: 'assets/imgs/company/google.png',
        title: 'Graphics Designer',
        addr: 'Bhavnagar, Gujrat, India',
        time: 'Be an early applicant',
        status: '1 day'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        title: 'Phython Developer',
        addr: 'Ahmedabad, Gujrat, India',
        time: '2 school alumni',
        status: '5 days'
      }];
      this.company = [{
        img: 'assets/imgs/company/initappz.png',
        name: 'Initappz Shop'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        name: 'Microsoft'
      }, {
        img: 'assets/imgs/company/google.png',
        name: 'Google'
      }];
    };

    dummyDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], dummyDataService);
    /***/
  }
}]);
//# sourceMappingURL=pages-members-activity-members-activity-module-es5.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-jobs-jobs-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/jobs.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/jobs.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n      <img src=\"assets/imgs/user.png\" class=\"user_image\">\n      <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar>\n      <ion-icon name=\"chatbubbles\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\"></ion-icon>\n    </div>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n\n      <ion-buttons slot=\"start\">\n        <!-- <ion-menu-button color=\"light\"></ion-menu-button> -->\n        <img src=\"assets/imgs/user.png\" class=\"user_image\" (click)=\"openMenu()\">\n      </ion-buttons>\n      <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n      (ionChange)=\"onSearchChange($event)\" [(ngModel)]='searchtext' [debounce]=\"250\"></ion-searchbar>\n      <ion-icon name=\"chatbubbles\" (click)=\"goToChatList()\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\"></ion-icon>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <img style=\"width: -webkit-fill-available;\" *ngIf=\"!jobs.length>0\" src=\"assets/imgs/no_result.jpg\">\n  <div *ngIf=\"jobs.length>0\" class=\"main_content_div\">\n    <!-- <ion-grid fixed>\n      <ion-row>\n        <ion-col size=\"4\" style=\"border-right: 1px solid lightgrey;\">\n          <img src=\"assets/imgs/edit.png\" alt=\"\">\n          <ion-label>Post a job</ion-label>\n        </ion-col>\n        <ion-col size=\"4\" style=\"border-right: 1px solid lightgrey;\">\n          <span>0</span>\n          <ion-label>Saved Jobs</ion-label>\n        </ion-col>\n        <ion-col size=\"4\">\n          <img src=\"assets/imgs/pen.png\" alt=\"\">\n          <ion-label>Career Interest</ion-label>\n        </ion-col>\n      </ion-row>\n    </ion-grid> -->\n  <div style=\"    overflow: scroll;\">\n      <ion-list class=\"flex_div2\">\n      \n        <ion-item *ngFor=\"let entry of jobtypes\">\n          <ion-label>{{entry.name}}</ion-label>\n          <ion-checkbox (ionChange)=\"onFillterChange($event)\"  slot=\"end\" [(ngModel)]=\"entry.isChecked\"></ion-checkbox>\n        </ion-item>\n        \n      </ion-list>\n    </div>\n  \n    <div class=\"content_div\">\n      <!-- <ion-label>Based on your profile</ion-label> -->\n\n  \n\n\n      <div  *ngFor=\"let item of jobs; let i = index\" (click)=\"goToJobDetail()\">\n        <div class=\"flex_div\" *ngIf=\"item.status=='publish'\">\n\n        \n        <img [src]=\"item.media_url\" class=\"image\">\n        <div class=\"content\">\n\n          <div class=\"popover_div\" *ngIf=\"actionDiv == i\">\n            <div class=\"pop_flex_div\">\n              <img src=\"assets/imgs/bookmark.png\" alt=\"\">\n              <ion-label>Save</ion-label>\n            </div>\n            <div class=\"pop_flex_div\" style=\"padding-bottom: 0px;\">\n              <img src=\"assets/imgs/cancel.png\" alt=\"\">\n              <ion-label>Not for me</ion-label>\n            </div>\n          </div>\n\n          <ion-icon name=\"ellipsis-vertical\" (click)=\"action(i)\"></ion-icon>\n\n          <ion-label class=\"job_title\">{{item.title}}</ion-label>\n          <ion-label class=\"adress\">{{item.company_name}}</ion-label>\n\n          <div class=\"easy_div\">\n            <img src=\"assets/imgs/location.png\">\n            <ion-label class=\"easy\">{{item.location}}</ion-label>\n          </div>\n\n          <div class=\"easy_div\">\n            <img src=\"assets/imgs/clock.png\">\n            <ion-label class=\"time\">{{item.date}} ago</ion-label>\n          </div>\n          <!-- <ion-label class=\"status\">{{item.status}}</ion-label> -->\n        </div>\n      </div>\n    </div>\n\n\n      \n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/jobs/jobs-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/jobs/jobs-routing.module.ts ***!
  \***************************************************/
/*! exports provided: JobsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobsPageRoutingModule", function() { return JobsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _jobs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./jobs.page */ "./src/app/pages/jobs/jobs.page.ts");




const routes = [
    {
        path: '',
        component: _jobs_page__WEBPACK_IMPORTED_MODULE_3__["JobsPage"]
    }
];
let JobsPageRoutingModule = class JobsPageRoutingModule {
};
JobsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], JobsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/jobs/jobs.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/jobs/jobs.module.ts ***!
  \*******************************************/
/*! exports provided: JobsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobsPageModule", function() { return JobsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _jobs_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./jobs-routing.module */ "./src/app/pages/jobs/jobs-routing.module.ts");
/* harmony import */ var _jobs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./jobs.page */ "./src/app/pages/jobs/jobs.page.ts");







let JobsPageModule = class JobsPageModule {
};
JobsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _jobs_routing_module__WEBPACK_IMPORTED_MODULE_5__["JobsPageRoutingModule"]
        ],
        declarations: [_jobs_page__WEBPACK_IMPORTED_MODULE_6__["JobsPage"]]
    })
], JobsPageModule);



/***/ }),

/***/ "./src/app/pages/jobs/jobs.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/jobs/jobs.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\nion-content {\n  --background: #F5F5F5;\n}\n\n.flex_div2 {\n  font-size: 12px;\n  white-space: nowrap;\n  display: flex;\n  align-items: center;\n  overflow-x: scroll;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div ion-grid {\n  background: white;\n}\n\n.main_content_div ion-grid ion-col {\n  position: relative;\n  height: 50px;\n}\n\n.main_content_div ion-grid ion-col img {\n  width: 20px;\n  display: block;\n  margin: auto;\n}\n\n.main_content_div ion-grid ion-col ion-label {\n  color: gray;\n  font-size: 14px;\n  position: absolute;\n  bottom: 0;\n  left: 50%;\n  transform: translate(-50%);\n  width: 100%;\n  text-align: center;\n}\n\n.main_content_div ion-grid ion-col span {\n  display: block;\n  text-align: center;\n  color: #505050;\n  font-weight: 600;\n}\n\n.main_content_div .content_div {\n  margin-top: 10px;\n  background: white;\n  padding: 15px;\n}\n\n.main_content_div .content_div .flex_div {\n  display: flex;\n  width: 100%;\n  padding-top: 10px;\n}\n\n.main_content_div .content_div .flex_div .image {\n  width: 50px;\n  min-width: 50px;\n  height: 50px;\n  border-radius: 3px;\n}\n\n.main_content_div .content_div .flex_div .content {\n  width: 100%;\n  border-bottom: 1px solid lightgray;\n  padding-top: 10px;\n  padding-bottom: 10px;\n  position: relative;\n  padding-left: 10px;\n}\n\n.main_content_div .content_div .flex_div .content .popover_div {\n  padding: 10px;\n  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.5);\n  position: absolute;\n  right: 0;\n  top: 40px;\n  border-radius: 3px;\n  z-index: 999;\n  background: white;\n}\n\n.main_content_div .content_div .flex_div .content .popover_div .pop_flex_div {\n  display: flex;\n  padding-bottom: 10px;\n}\n\n.main_content_div .content_div .flex_div .content .popover_div .pop_flex_div img {\n  width: 20px;\n  height: 20px;\n}\n\n.main_content_div .content_div .flex_div .content .popover_div .pop_flex_div ion-label {\n  margin-left: 10px;\n  font-size: 14px;\n}\n\n.main_content_div .content_div .flex_div .content ion-icon {\n  position: absolute;\n  right: 0;\n  top: 15px;\n  font-size: 20px;\n  color: #505050;\n}\n\n.main_content_div .content_div .flex_div .content ion-label {\n  padding-bottom: 5px;\n}\n\n.main_content_div .content_div .flex_div .content .job_title {\n  font-weight: 600;\n  font-size: 16px;\n}\n\n.main_content_div .content_div .flex_div .content .adress {\n  font-size: 14px;\n}\n\n.main_content_div .content_div .flex_div .content .easy_div {\n  display: flex;\n  padding-bottom: 5px;\n}\n\n.main_content_div .content_div .flex_div .content .easy_div img {\n  width: 15px;\n  height: 15px;\n}\n\n.main_content_div .content_div .flex_div .content .easy_div .easy {\n  font-size: 12px;\n  margin-left: 10px;\n  padding: 0;\n}\n\n.main_content_div .content_div .flex_div .content .easy_div .time {\n  margin-left: 10px;\n  padding: 0;\n}\n\n.main_content_div .content_div .flex_div .content .status {\n  color: gray;\n  font-size: 14px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvam9icy9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxqb2JzXFxqb2JzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvam9icy9qb2JzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1DQUFBO0FDQ0o7O0FEQ0E7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0FDRUo7O0FEQUE7RUFDSSxxQkFBQTtBQ0dKOztBRERBO0VBRUksZUFBQTtFQUVBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtFQUFBLHVCQUFBO0VBQUEsa0JBQUE7QUNFSjs7QURBQTtFQUNJLFdBQUE7QUNHSjs7QURESTtFQUNJLGNBQUE7QUNHUjs7QURBSTtFQUNJLGlCQUFBO0FDRVI7O0FEQVE7RUFDSSxrQkFBQTtFQUNBLFlBQUE7QUNFWjs7QUREWTtFQUNJLFdBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ0doQjs7QUREWTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsU0FBQTtFQUNBLDBCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDR2hCOztBRERZO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FDR2hCOztBREVJO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7QUNBUjs7QURFUTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7QUNBWjs7QURFWTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDQWhCOztBREdZO0VBQ0ksV0FBQTtFQUNBLGtDQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBc0NBLGtCQUFBO0FDdENoQjs7QURFZ0I7RUFDSSxhQUFBO0VBQ0EsMENBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUNBcEI7O0FERW9CO0VBQ0ksYUFBQTtFQUNBLG9CQUFBO0FDQXhCOztBREN3QjtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDQzVCOztBREN3QjtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQ0M1Qjs7QURLZ0I7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUNIcEI7O0FETWdCO0VBQ0ksbUJBQUE7QUNKcEI7O0FET2dCO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0FDTHBCOztBRE9nQjtFQUNJLGVBQUE7QUNMcEI7O0FEUWdCO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FDTnBCOztBRE9vQjtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDTHhCOztBRE9vQjtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7QUNMeEI7O0FET29CO0VBQ0ksaUJBQUE7RUFDQSxVQUFBO0FDTHhCOztBRFNnQjtFQUNJLFdBQUE7RUFDQSxlQUFBO0FDUHBCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvam9icy9qb2JzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZCA6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cbmlvbi10b29sYmFyLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgsIGlvbi10b29sYmFyIC5zYy1pb24tc2VhcmNoYmFyLWlvcy1oe1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcbn1cbmlvbi1jb250ZW50e1xuICAgIC0tYmFja2dyb3VuZDogI0Y1RjVGNTtcbn1cbi5mbGV4X2RpdjJ7XG4gICBcbiAgICBmb250LXNpemU6IDEycHg7XG4gICBcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBvdmVyZmxvdy14OiBzY3JvbGw7XG4gICAgd2lkdGg6IG1heC1jb250ZW50O1xufVxuLm1haW5fY29udGVudF9kaXZ7XG4gICAgd2lkdGg6IDEwMCU7XG5cbiAgICBpb24tbGFiZWwge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG5cbiAgICBpb24tZ3JpZHtcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG5cbiAgICAgICAgaW9uLWNvbHtcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgICAgIGltZ3tcbiAgICAgICAgICAgICAgICB3aWR0aDogMjBweDtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpb24tbGFiZWx7XG4gICAgICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICBib3R0b206IDA7XG4gICAgICAgICAgICAgICAgbGVmdDogNTAlO1xuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUpO1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHNwYW57XG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGNvbG9yOiAjNTA1MDUwO1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuY29udGVudF9kaXZ7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwYWRkaW5nOiAxNXB4O1xuXG4gICAgICAgIC5mbGV4X2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICAgICAgLy8gcGFkZGluZy1ib3R0b206IDEwcHg7XG4gICAgICAgICAgICAuaW1hZ2V7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICAgICAgICAgICAgbWluLXdpZHRoOiA1MHB4O1xuICAgICAgICAgICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5jb250ZW50e1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICAgICAgICAgICAgICAgLnBvcG92ZXJfZGl2e1xuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICBib3gtc2hhZG93OiAwcHggM3B4IDZweCByZ2JhKDAsMCwwLDAuNSk7XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDA7XG4gICAgICAgICAgICAgICAgICAgIHRvcDogNDBweDtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICAgICAgICAgICAgICB6LWluZGV4OiA5OTk7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuXG4gICAgICAgICAgICAgICAgICAgIC5wb3BfZmxleF9kaXZ7XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XG4gICAgICAgICAgICAgICAgICAgICAgICBpbWd7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAyMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgaW9uLWxhYmVsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaW9uLWljb257XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDA7XG4gICAgICAgICAgICAgICAgICAgIHRvcDogMTVweDtcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzUwNTA1MDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpb24tbGFiZWwge1xuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgLmpvYl90aXRsZXtcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAuYWRyZXNze1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLmVhc3lfZGl2e1xuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgICAgICAgICAgICBpbWd7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTVweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTVweDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAuZWFzeXtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAudGltZXtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC5zdGF0dXN7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgXG59IiwiaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogI0Y1RjVGNTtcbn1cblxuLmZsZXhfZGl2MiB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgb3ZlcmZsb3cteDogc2Nyb2xsO1xuICB3aWR0aDogbWF4LWNvbnRlbnQ7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiBpb24tbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IGlvbi1ncmlkIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG4ubWFpbl9jb250ZW50X2RpdiBpb24tZ3JpZCBpb24tY29sIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDUwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiBpb24tZ3JpZCBpb24tY29sIGltZyB7XG4gIHdpZHRoOiAyMHB4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiBhdXRvO1xufVxuLm1haW5fY29udGVudF9kaXYgaW9uLWdyaWQgaW9uLWNvbCBpb24tbGFiZWwge1xuICBjb2xvcjogZ3JheTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlKTtcbiAgd2lkdGg6IDEwMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IGlvbi1ncmlkIGlvbi1jb2wgc3BhbiB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjNTA1MDUwO1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmZsZXhfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5mbGV4X2RpdiAuaW1hZ2Uge1xuICB3aWR0aDogNTBweDtcbiAgbWluLXdpZHRoOiA1MHB4O1xuICBoZWlnaHQ6IDUwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAuZmxleF9kaXYgLmNvbnRlbnQge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAuZmxleF9kaXYgLmNvbnRlbnQgLnBvcG92ZXJfZGl2IHtcbiAgcGFkZGluZzogMTBweDtcbiAgYm94LXNoYWRvdzogMHB4IDNweCA2cHggcmdiYSgwLCAwLCAwLCAwLjUpO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDQwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgei1pbmRleDogOTk5O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAuZmxleF9kaXYgLmNvbnRlbnQgLnBvcG92ZXJfZGl2IC5wb3BfZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAuZmxleF9kaXYgLmNvbnRlbnQgLnBvcG92ZXJfZGl2IC5wb3BfZmxleF9kaXYgaW1nIHtcbiAgd2lkdGg6IDIwcHg7XG4gIGhlaWdodDogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAuZmxleF9kaXYgLmNvbnRlbnQgLnBvcG92ZXJfZGl2IC5wb3BfZmxleF9kaXYgaW9uLWxhYmVsIHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAuZmxleF9kaXYgLmNvbnRlbnQgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDE1cHg7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgY29sb3I6ICM1MDUwNTA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmZsZXhfZGl2IC5jb250ZW50IGlvbi1sYWJlbCB7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmZsZXhfZGl2IC5jb250ZW50IC5qb2JfdGl0bGUge1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDE2cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmZsZXhfZGl2IC5jb250ZW50IC5hZHJlc3Mge1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmZsZXhfZGl2IC5jb250ZW50IC5lYXN5X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmZsZXhfZGl2IC5jb250ZW50IC5lYXN5X2RpdiBpbWcge1xuICB3aWR0aDogMTVweDtcbiAgaGVpZ2h0OiAxNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5mbGV4X2RpdiAuY29udGVudCAuZWFzeV9kaXYgLmVhc3kge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nOiAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5mbGV4X2RpdiAuY29udGVudCAuZWFzeV9kaXYgLnRpbWUge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgcGFkZGluZzogMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAuZmxleF9kaXYgLmNvbnRlbnQgLnN0YXR1cyB7XG4gIGNvbG9yOiBncmF5O1xuICBmb250LXNpemU6IDE0cHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/jobs/jobs.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/jobs/jobs.page.ts ***!
  \*****************************************/
/*! exports provided: JobsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobsPage", function() { return JobsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/





let JobsPage = class JobsPage {
    constructor(api, storage, router, menuCtrl, loadingController) {
        this.api = api;
        this.storage = storage;
        this.router = router;
        this.menuCtrl = menuCtrl;
        this.loadingController = loadingController;
        this.jobs = ["0"];
        this.fillter = "";
        this.searchtext = "";
        this.newest = false;
        this.alphabetical = false;
        this.active = true;
        this.popular = false;
        this.storage.get('COMPLETE_USER_INFO').then(result => {
            if (result != null) {
                console.log(result);
                this.token = result.token;
                this.user_id = result.user_id;
                console.log('user_id: ' + result.user_id);
                console.log('token: ' + result.token);
            }
        }).catch(e => {
            console.log('error: ' + e);
            // Handle errors here
        });
    }
    onSearchChange(event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Please wait...',
                duration: 1500
            });
            yield loading.present();
            var dataval = this.jobtypes.filter(data => data.isChecked === true);
            console.log("filter", dataval);
            if (typeof (dataval) !== 'undefined') {
                let result = dataval.map(a => a.id);
                ;
                this.api.joblistings(this.token, 1, result, this.searchtext).subscribe(res => {
                    this.jobs = res;
                });
                console.log("filter", result);
                console.log("searchtext", this.searchtext);
                console.log("filter", this.fillter);
            }
            else {
                this.api.joblistings(this.token, 1, [], this.searchtext).subscribe(res => {
                    this.jobs = res;
                });
                console.log("searchtext", this.searchtext);
            }
        });
    }
    onFillterChange($event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Please wait...',
                duration: 1500
            });
            yield loading.present();
            var dataval = this.jobtypes.filter(data => data.isChecked === true);
            console.log("filter", dataval);
            if (typeof (dataval) !== 'undefined') {
                let result = dataval.map(a => a.id);
                this.api.joblistings(this.token, 1, result, this.searchtext).subscribe(res => {
                    this.jobs = res;
                });
                console.log("filter", result);
            }
            else {
                this.api.joblistings(this.token, 1, [], this.searchtext).subscribe(res => {
                    this.jobs = res;
                });
            }
        });
    }
    action(val) {
        this.actionDiv = val;
    }
    ngOnInit() {
        this.api.joblistings(this.token).subscribe(res => {
            console.log(res);
            this.jobs = res;
        });
        this.api.jobtypes(this.token).subscribe(res => {
            console.log(res);
            this.jobtypes = res;
        });
    }
    goToJobDetail() {
        this.router.navigate(['/job-detail']);
    }
    openMenu() {
        this.menuCtrl.open();
    }
    goToChatList() {
        this.router.navigate(['/chatlist']);
    }
};
JobsPage.ctorParameters = () => [
    { type: _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] }
];
JobsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-jobs',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./jobs.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/jobs.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./jobs.page.scss */ "./src/app/pages/jobs/jobs.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]])
], JobsPage);



/***/ })

}]);
//# sourceMappingURL=pages-jobs-jobs-module-es2015.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-forum-reply-forum-reply-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forum-reply/forum-reply.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forum-reply/forum-reply.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-button (click)=\"BackButton()\" slot=\"start\">\n      <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n    </ion-button>\n    <ion-title>{{post.forum_name}}</ion-title>\n   \n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n  <div class=\"main_content_div\">\n    <div class=\"content_div\">\n      <ion-label style=\"font-size: 18px;\"  class=\"head_lbl\">{{post.title}} </ion-label>\n      <ion-label  [innerHTML]=\"post.description\" class=\"desc_lbl\">\n       \n      </ion-label>\n\n\n      <!-- <div (click)=\"singlefeed(post.id)\" *ngIf=\"post.media\">\n        <div *ngFor=\"let item of post.media\">\n                  <div class=\"card_image\" [style.backgroundImage]=\"'url('+item+')'\"></div>\n        \n                </div> </div> -->\n               </div>\n    <div class=\"desc_div\">\n  \n      <div class=\"flex_div\">\n      \n        <div class=\"content_div\" style=\"display: flex;\">\n          <div>\n          <img [routerLink]=\"['/members-detail', post.author]\" src=\"{{post.author_image}}\" class=\"image\">\n          </div>\n          <div style=\"margin-left: 15px;\">\n          <ion-label  class=\"head_lbl\">{{post.author_name}} </ion-label>\n        \n          <ion-label class=\"small_lbl\" >{{post.date}}</ion-label>\n        </div>\n \n        </div>\n\n        <div>\n          {{post.voice_count}} Members  . {{post.total_reply_count}} Replies\n        </div>\n      </div>\n    </div>\n\n  \n          <ion-button (click)=\"commentfocus()\" fill=\"clear\" size=\"small\">\n            <ion-icon name=\"arrow-undo-outline\"></ion-icon>&nbsp;&nbsp;Reply\n          </ion-button>\n      \n       \n      <!-- <div class=\"reaction_div\">\n        <ion-label>Reaction</ion-label>\n\n        <div class=\"users_div\">\n          <div class=\"user_img\" *ngFor=\"let item of (users | slice: 0:5)\" [style.backgroundImage]=\"'url('+item.img+')'\">\n          </div>\n          <div class=\"round\">\n            <ion-icon name=\"ellipsis-horizontal\" class=\"dots\"></ion-icon>\n          </div>\n        </div>\n      </div> -->\n\n      <div class=\"comments_div\">\n        <div class=\"lbls\">\n          <ion-label>Replies</ion-label>\n          <!-- <ion-label class=\"relevent\">Most Relevant</ion-label> -->\n        </div>\n        <div #processContainer>\n        </div>\n        <div *ngFor=\"let item of data\">\n        <div class=\"flex_div\" >\n          <div class=\"user_img\" [style.backgroundImage]=\"'url('+item.author_image+')'\"></div>\n          <div id=\"id{{item.id}}\" class=\"blue_div\" >\n            <ion-label class=\"replytxt\" *ngIf=\"item.replycomment_text\" (click)=\"hightlight(item.reply_to)\">Reply Comment For <p>{{item.replycomment_text}}</p></ion-label>\n            <ion-icon (click)=\"deletepost(item.id)\"  *ngIf=\"item.can_delete\"  class=\"abs_icn2\" name=\"trash\"></ion-icon>\n            <ion-icon (click)=\"commentreply(item.id,item.content,item.author_image)\" name=\"arrow-undo-outline\" class=\"abs_icn\"></ion-icon>\n\n            <ion-label class=\"head_lbl\">{{item.author_name}}</ion-label>\n           \n            <ion-label  class=\"small_lbl\">{{item.date}}</ion-label>\n\n            <ion-label [innerHTML]=\"item.content\"  class=\"text\">\n                 </ion-label>\n          </div>\n        \n\n         </div> \n         <!-- <div style=\"padding-left: 40px;\" *ngIf=\"item.reply\"> \n\n\n          <div class=\"flex_div\" *ngFor=\"let itemreply of item.reply\">\n            <div class=\"user_img\" [style.backgroundImage]=\"'url('+itemreply.user_avatar+')'\"></div>\n           \n            <div  id=\"id{{itemreply.id}}\" class=\"blue_div\" style=\"\n            background: #aed8f3;\n        \">\n              <ion-icon (click)=\"deletepost(itemreply.id)\" class=\"abs_icn2\" name=\"trash\"></ion-icon>\n              <ion-icon (click)=\"commentreply(itemreply.id,itemreply.content,itemreply.author_image)\" name=\"arrow-undo-outline\" class=\"abs_icn\"></ion-icon>\n              <ion-label class=\"replytxt\" (click)=\"hightlight(itemreply.reply_to)\">Reply Comment For <p>{{itemreply.replycomment_text}}</p></ion-label>\n              <ion-label style=\"padding-top: 5px;\" class=\"head_lbl\">{{itemreply.author_name}}</ion-label>\n             \n              <ion-label  class=\"small_lbl\">{{itemreply.date}}</ion-label>\n  \n              <ion-label  [innerHTML]=\"itemreply.content\"  class=\"text\">\n                   </ion-label>\n            </div>\n           </div>\n           <div style=\"padding-left: 80px;\" *ngIf=\"itemreply.reply\"> \n\n\n            <div class=\"flex_div\" *ngFor=\"let itemreply2 of itemreply.reply\">\n              <div class=\"user_img\" [style.backgroundImage]=\"'url('+itemreply2.user_avatar+')'\"></div>\n             \n              <div  id=\"id{{itemreply2.id}}\" class=\"blue_div\" style=\"\n              background: #aed8f3;\n          \">\n                <ion-icon (click)=\"deletepost(itemreply2.id)\" class=\"abs_icn2\" name=\"trash\"></ion-icon>\n                 <ion-icon (click)=\"commentreply(itemreply2.id,itemreply2.content,itemreply2.author_image)\" name=\"arrow-undo-outline\" class=\"abs_icn\"></ion-icon> \n                <ion-label class=\"replytxt\" (click)=\"hightlight(itemreply2.reply_to)\">Reply Comment For <p>{{itemreply2.replycomment_text}}</p></ion-label>\n                <ion-label style=\"padding-top: 5px;\" class=\"head_lbl\">{{itemreply2.author_name}}</ion-label>\n               \n                <ion-label  class=\"small_lbl\">{{itemreply2.date}}</ion-label>\n    \n                <ion-label  [innerHTML]=\"itemreply2.content\"  class=\"text\">\n                     </ion-label>\n              </div>\n             </div>\n  \n             \n            </div>\n\n          </div> -->\n        \n        \n        </div>\n        </div>\n        </div>\n\n     \n    \n</ion-content>\n\n<ion-footer>\n  <ion-grid>\n    <ion-row>\n    <ion-col size=\"4\" style=\"margin-top: 15px;\" *ngFor=\"let item of file_imgs\">\n      <ion-icon  (click)=\"dltmedia(item.id)\" style=\"position: relative; bottom: 75px; left: 85px;\" name=\"close-circle-outline\"></ion-icon>\n      <img style=\"height: 80px;\n      width: 80px; border-radius: 10px;\" src=\"{{item.img}}\" >\n    </ion-col>\n  </ion-row>\n  </ion-grid>\n  <input\n  type=\"file\"\n  #fileInput\n  (change)=\"uploadFile($event)\"\n  hidden=\"true\"\n  accept=\"image/*\"\n/>\n  <!-- <div style=\"position: relative;\">\n     <ion-button  (click)=\"addImage()\" size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"camera-outline\"></ion-icon>\n    </ion-button> -->\n  <!--  <ion-button size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"videocam-outline\"></ion-icon>\n    </ion-button> -->\n    <!-- <ion-button (click)=\"selectImage()\" size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"image-outline\"></ion-icon>\n    </ion-button>\n    <ion-button *ngIf=\"!plt.is('hybrid')\" (click)=\"attach()\" size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"ellipsis-horizontal\"></ion-icon>\n    </ion-button> -->\n    <!-- <ion-button size=\"small\" fill=\"clear\" class=\"at_btn\">\n      <ion-icon slot=\"icon-only\" name=\"at-outline\"></ion-icon>\n    </ion-button> -->\n  <!-- </div> -->\n  <div class=\"footer_div\">\n    <ion-label class=\"reply\" *ngIf=\"commentid !='' \">Reply To</ion-label> \n      <div class=\"flex_div\" *ngIf=\"commentid!=''\">\n        <div class=\"user_img\" [style.backgroundImage]=\"'url('+commentavatar+')'\"></div>\n      <div class=\"blue_div\">\n        <ion-label *ngIf=\"commenttext!=''\"\n         #commenttextid [innerHTML]=\"commenttext\"  class=\"text\"></ion-label>\n          <ion-input #commentidid type=\"hidden\" *ngIf=\"commentid!=''\" [(ngModel)]=\"commentid\" placeholder=\"Leave your thoughts\"></ion-input>\n        \n </div>\n\n</div>  <div class=\"flex_foot\" > <div class=\"first_div\">\n      <div class=\"user_img\" [style.backgroundImage]=\"'url('+mydata+')'\"></div>\n      <ion-input #commentfoc type=\"text\" [(ngModel)]=\"comment\" placeholder=\"Leave your thoughts\"></ion-input>\n    </div>\n    <div class=\"second_div\">\n      <!-- <ion-label class=\"at\">@</ion-label> -->\n      <ion-button (click)=\"postcomment()\" fill=\"clear\">\n        <ion-spinner *ngIf=\"loading\"></ion-spinner> Post\n      </ion-button>\n    </div>\n  </div>\n  </div>\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/forum-reply/forum-reply-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/forum-reply/forum-reply-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: ForumReplyPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForumReplyPageRoutingModule", function() { return ForumReplyPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _forum_reply_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./forum-reply.page */ "./src/app/pages/forum-reply/forum-reply.page.ts");




const routes = [
    {
        path: '',
        component: _forum_reply_page__WEBPACK_IMPORTED_MODULE_3__["ForumReplyPage"]
    }
];
let ForumReplyPageRoutingModule = class ForumReplyPageRoutingModule {
};
ForumReplyPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ForumReplyPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/forum-reply/forum-reply.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/forum-reply/forum-reply.module.ts ***!
  \*********************************************************/
/*! exports provided: ForumReplyPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForumReplyPageModule", function() { return ForumReplyPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _forum_reply_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./forum-reply-routing.module */ "./src/app/pages/forum-reply/forum-reply-routing.module.ts");
/* harmony import */ var _forum_reply_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./forum-reply.page */ "./src/app/pages/forum-reply/forum-reply.page.ts");







let ForumReplyPageModule = class ForumReplyPageModule {
};
ForumReplyPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _forum_reply_routing_module__WEBPACK_IMPORTED_MODULE_5__["ForumReplyPageRoutingModule"]
        ],
        declarations: [_forum_reply_page__WEBPACK_IMPORTED_MODULE_6__["ForumReplyPage"]]
    })
], ForumReplyPageModule);



/***/ }),

/***/ "./src/app/pages/forum-reply/forum-reply.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/forum-reply/forum-reply.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\nion-button {\n  --box-shadow:none;\n}\n\nion-title {\n  color: white;\n}\n\n.main_content_div {\n  width: 100%;\n  padding: 16px;\n}\n\n.main_content_div .comments_div {\n  margin-top: 15px;\n  border-top: 1px solid #0e76a8;\n  padding: 10px;\n}\n\n.main_content_div .comments_div .lbls {\n  display: flex;\n  justify-content: space-between;\n}\n\n.main_content_div .comments_div .lbls .relevent {\n  font-weight: 600;\n  color: #505050;\n}\n\n.main_content_div .comments_div .flex_div {\n  display: flex;\n  margin-top: 15px;\n}\n\n.main_content_div .comments_div .flex_div .user_img {\n  height: 40px;\n  width: 40px;\n  min-width: 40px;\n  border-radius: 50%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-right: 10px;\n}\n\n.main_content_div .comments_div .flex_div .blue_div {\n  padding: 10px;\n  background: #E1E9EE;\n  border-radius: 10px;\n  border-top-left-radius: 0px;\n  position: relative;\n  width: 100%;\n}\n\n.main_content_div .comments_div .flex_div .blue_div .abs_icn {\n  right: 10px;\n  top: 10px;\n  position: absolute;\n  color: #505050;\n  font-size: 17px;\n}\n\n.main_content_div .comments_div .flex_div .blue_div .abs_icn2 {\n  right: 30px;\n  top: 10px;\n  position: absolute;\n  color: #505050;\n  font-size: 17px;\n}\n\n.main_content_div .comments_div .flex_div .blue_div .head_lbl {\n  font-weight: 600;\n}\n\n.main_content_div .comments_div .flex_div .blue_div .small_lbl {\n  font-size: 14px;\n  color: gray;\n}\n\n.main_content_div .comments_div .flex_div .blue_div .text {\n  margin-top: 7px;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div .liked {\n  color: #047ead !important;\n}\n\n.main_content_div .unliked {\n  color: gray;\n}\n\n.main_content_div .head_lbl {\n  display: block;\n  font-weight: 500;\n  text-align: left;\n  font-size: 14px;\n  -webkit-margin-after: 5px;\n          margin-block-end: 5px;\n}\n\n.main_content_div .desc_div {\n  background: white;\n  position: relative;\n}\n\n.main_content_div .desc_div .flex_div {\n  display: flex;\n  align-items: center;\n  width: 100%;\n  justify-content: space-between;\n}\n\n.main_content_div .desc_div .flex_div .image {\n  height: 45px;\n  width: 45px;\n  min-width: 45px;\n  border-radius: 3px;\n}\n\n.main_content_div .desc_div .flex_div .content_div {\n  margin-left: 10px;\n}\n\n.main_content_div .desc_div .flex_div .content_div .head_lbl {\n  font-weight: 600;\n}\n\n.main_content_div .desc_div .flex_div .content_div .small_lbl {\n  font-size: 14px;\n}\n\n.main_content_div .desc_div .flex_div .fallow_btn {\n  font-size: 14px;\n  font-weight: 600;\n  margin: 0;\n}\n\n.main_content_div .content_div {\n  margin-top: 10px;\n}\n\n.main_content_div .content_div .desc_lbl {\n  font-size: 14px;\n  margin-bottom: 20px;\n}\n\n.main_content_div .content_div span {\n  margin-right: 10px;\n  font-weight: 600;\n  color: var(--ion-color-main);\n}\n\n.main_content_div .content_div .card_image {\n  height: 200px;\n  width: 100%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.main_content_div .content_div .like_div {\n  padding-top: 10px;\n  padding-bottom: 5px;\n  display: flex;\n  justify-content: space-evenly;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .content_div .like_div img {\n  width: 20px;\n  margin-right: 5px;\n}\n\n.main_content_div .content_div .like_div ion-button {\n  color: gray;\n  font-size: 16px;\n}\n\n.main_content_div .content_div .like_div2 {\n  padding-top: 10px;\n  padding-bottom: 5px;\n  display: flex;\n  justify-content: space-between;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .content_div .like_div2 img {\n  width: 20px;\n  margin-right: 5px;\n}\n\n.main_content_div .content_div .like_div2 ion-button {\n  color: gray;\n  font-size: 16px;\n}\n\n.main_content_div .content_div .reaction_div {\n  margin-top: 15px;\n}\n\n.main_content_div .content_div .reaction_div .users_div {\n  margin-top: 5px;\n  display: flex;\n}\n\n.main_content_div .content_div .reaction_div .users_div .user_img {\n  height: 40px;\n  width: 40px;\n  min-width: 40px;\n  border-radius: 50%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-right: 10px;\n}\n\n.main_content_div .content_div .reaction_div .users_div .round {\n  height: 40px;\n  width: 40px;\n  min-width: 40px;\n  border-radius: 50%;\n  border: 2px solid #505050;\n  position: relative;\n}\n\n.main_content_div .content_div .reaction_div .users_div .round .dots {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  color: #505050;\n  font-size: 20px;\n}\n\n.replytxt {\n  font-size: 12px;\n  color: #0a46a0;\n  border-bottom: 3px solid #fffefe;\n}\n\n.footer_div {\n  padding-left: 16px;\n  padding-right: 16px;\n}\n\n.footer_div .reply {\n  font-size: 13px;\n  font-weight: bold;\n}\n\n.footer_div .flex_div {\n  display: flex;\n  margin-top: 15px;\n  margin-bottom: 10px;\n}\n\n.footer_div .flex_div .user_img {\n  height: 40px;\n  width: 40px;\n  min-width: 40px;\n  border-radius: 50%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-right: 10px;\n}\n\n.footer_div .flex_div .blue_div {\n  padding: 10px;\n  background: #E1E9EE;\n  border-radius: 10px;\n  border-top-left-radius: 0px;\n  position: relative;\n  width: 100%;\n}\n\n.footer_div .flex_foot {\n  display: flex;\n  justify-content: space-between;\n}\n\n.footer_div .first_div {\n  display: flex;\n  align-items: center;\n}\n\n.footer_div .first_div .user_img {\n  height: 30px;\n  width: 30px;\n  min-width: 30px;\n  border-radius: 50%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-right: 10px;\n}\n\n.footer_div .second_div {\n  display: flex;\n  align-items: center;\n}\n\n.footer_div .second_div .at {\n  font-size: 20px;\n  color: #505050;\n  font-weight: 600;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZm9ydW0tcmVwbHkvRDpcXFJhZ3V2YXJhbiBpbWFnZVxcZGlzZWxzaGlwL3NyY1xcYXBwXFxwYWdlc1xcZm9ydW0tcmVwbHlcXGZvcnVtLXJlcGx5LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZm9ydW0tcmVwbHkvZm9ydW0tcmVwbHkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUNBQUE7QUNDSjs7QURDQTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUNFSjs7QURBQTtFQUNJLGlCQUFBO0FDR0o7O0FEREE7RUFDSSxZQUFBO0FDSUo7O0FERkE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtBQ0tKOztBREpJO0VBQ0ksZ0JBQUE7RUFDQSw2QkFBQTtFQUNGLGFBQUE7QUNNTjs7QURMUTtFQUNJLGFBQUE7RUFDQSw4QkFBQTtBQ09aOztBRExZO0VBQ0ksZ0JBQUE7RUFDQSxjQUFBO0FDT2hCOztBREhRO0VBQ0ksYUFBQTtFQUNBLGdCQUFBO0FDS1o7O0FESlk7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0FDTWhCOztBREhZO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ0toQjs7QURKZ0I7RUFDSSxXQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNNcEI7O0FESmdCO0VBQ0ksV0FBQTtFQUNBLFNBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDTXBCOztBREhnQjtFQUNJLGdCQUFBO0FDS3BCOztBREhnQjtFQUNJLGVBQUE7RUFDQSxXQUFBO0FDS3BCOztBREhnQjtFQUNJLGVBQUE7QUNLcEI7O0FEQUk7RUFDSSxjQUFBO0FDRVI7O0FEQUk7RUFDSSx5QkFBQTtBQ0VSOztBRERJO0VBQ1EsV0FBQTtBQ0daOztBRERJO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7VUFBQSxxQkFBQTtBQ0dSOztBRERJO0VBRUksaUJBQUE7RUFDQSxrQkFBQTtBQ0VSOztBREFRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0FDRVo7O0FERFk7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0doQjs7QURBWTtFQUNJLGlCQUFBO0FDRWhCOztBRERnQjtFQUNJLGdCQUFBO0FDR3BCOztBRERnQjtFQUNJLGVBQUE7QUNHcEI7O0FEQ1k7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0FDQ2hCOztBRElJO0VBQ0ksZ0JBQUE7QUNGUjs7QURHUTtFQUNJLGVBQUE7RUFDQSxtQkFBQTtBQ0RaOztBRElRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLDRCQUFBO0FDRlo7O0FES1E7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQ0haOztBRE1RO0VBQ0ksaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSw2QkFBQTtFQUNBLGtDQUFBO0FDSlo7O0FES1k7RUFDSSxXQUFBO0VBQ0EsaUJBQUE7QUNIaEI7O0FETVk7RUFDSSxXQUFBO0VBQ0EsZUFBQTtBQ0poQjs7QURPUTtFQUNJLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQ0FBQTtBQ0xaOztBRE1ZO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0FDSmhCOztBRE9ZO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUNMaEI7O0FEU1E7RUFDSSxnQkFBQTtBQ1BaOztBRFFZO0VBQ0ksZUFBQTtFQUNBLGFBQUE7QUNOaEI7O0FET2dCO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtBQ0xwQjs7QURPZ0I7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUNMcEI7O0FET29CO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNMeEI7O0FEY0E7RUFDSSxlQUFBO0VBRUEsY0FBQTtFQUNBLGdDQUFBO0FDWko7O0FEY0E7RUFFSSxrQkFBQTtFQUNBLG1CQUFBO0FDWko7O0FEZUE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7QUNiSjs7QURjSTtFQUNJLGFBQUE7RUFDSixnQkFBQTtFQUNBLG1CQUFBO0FDWko7O0FEYVE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0FDWFo7O0FEY1E7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDWlo7O0FEYUk7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7QUNYUjs7QURhSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtBQ1hSOztBRFlRO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtBQ1ZaOztBRGFJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FDWFI7O0FEYVE7RUFDSSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FDWFoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9mb3J1bS1yZXBseS9mb3J1bS1yZXBseS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaHtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5pb24tYnV0dG9ue1xuICAgIC0tYm94LXNoYWRvdzpub25lO1xufVxuaW9uLXRpdGxle1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2e1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgLmNvbW1lbnRzX2RpdntcbiAgICAgICAgbWFyZ2luLXRvcDogMTVweDtcbiAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICMwZTc2YTg7XG4gICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICAubGJsc3tcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG5cbiAgICAgICAgICAgIC5yZWxldmVudHtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgICAgIGNvbG9yOiAjNTA1MDUwO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLmZsZXhfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDE1cHg7XG4gICAgICAgICAgICAudXNlcl9pbWd7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgICAgIG1pbi13aWR0aDogNDBweDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5ibHVlX2RpdntcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNFMUU5RUU7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgICAgICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwcHg7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgIC5hYnNfaWNue1xuICAgICAgICAgICAgICAgICAgICByaWdodDogMTBweDtcbiAgICAgICAgICAgICAgICAgICAgdG9wOiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjNTA1MDUwO1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE3cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5hYnNfaWNuMntcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDMwcHg7XG4gICAgICAgICAgICAgICAgICAgIHRvcDogMTBweDtcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzUwNTA1MDtcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxN3B4O1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC5oZWFkX2xibHtcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLnNtYWxsX2xibHtcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLnRleHR7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDdweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuICAgIC5saWtlZHtcbiAgICAgICAgY29sb3I6ICMwNDdlYWQgIWltcG9ydGFudDt9XG4gICAgLnVubGlrZWR7XG4gICAgICAgICAgICBjb2xvcjogZ3JheTt9XG5cbiAgICAuaGVhZF9sYmx7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIG1hcmdpbi1ibG9jay1lbmQ6IDVweDtcbiAgICB9XG4gICAgLmRlc2NfZGl2e1xuICAgICAgICAvLyBwYWRkaW5nOiAxNnB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICAgICAgIC5mbGV4X2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgICAgICAuaW1hZ2V7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA0NXB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiA0NXB4O1xuICAgICAgICAgICAgICAgIG1pbi13aWR0aDogNDVweDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5jb250ZW50X2RpdntcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgICAgICAgICAuaGVhZF9sYmx7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5zbWFsbF9sYmx7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5mYWxsb3dfYnRue1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5jb250ZW50X2RpdntcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgLmRlc2NfbGJse1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweFxuICAgICAgICB9XG5cbiAgICAgICAgc3BhbntcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICB9XG5cbiAgICAgICAgLmNhcmRfaW1hZ2V7XG4gICAgICAgICAgICBoZWlnaHQ6IDIwMHB4O1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5saWtlX2RpdntcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgICAgICBpbWd7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlvbi1idXR0b24ge1xuICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAubGlrZV9kaXYye1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgICAgICBpbWd7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlvbi1idXR0b24ge1xuICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5yZWFjdGlvbl9kaXZ7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxNXB4O1xuICAgICAgICAgICAgLnVzZXJzX2RpdntcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICAudXNlcl9pbWd7XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDQwcHg7XG4gICAgICAgICAgICAgICAgICAgIG1pbi13aWR0aDogNDBweDtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLnJvdW5ke1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgICAgICAgICBtaW4td2lkdGg6IDQwcHg7XG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiAycHggc29saWQgIzUwNTA1MDtcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICAgICAgICAgICAgICAgICAgIC5kb3Rze1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdG9wOiA1MCU7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OiA1MCU7XG4gICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLC01MCUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM1MDUwNTA7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgXG4gICAgfVxufVxuLnJlcGx5dHh0e1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgIFxuICAgIGNvbG9yOiAjMGE0NmEwO1xuICAgIGJvcmRlci1ib3R0b206IDNweCBzb2xpZCAjZmZmZWZlO1xufVxuLmZvb3Rlcl9kaXZ7XG4gICBcbiAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTZweDtcblxuICAgIFxuLnJlcGx5IHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7fVxuICAgIC5mbGV4X2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIC51c2VyX2ltZ3tcbiAgICAgICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgbWluLXdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgICAgIH1cblxuICAgICAgICAuYmx1ZV9kaXZ7XG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICAgICAgYmFja2dyb3VuZDogI0UxRTlFRTtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgICAgICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwcHg7XG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTt9fVxuICAgIC5mbGV4X2Zvb3R7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICB9XG4gICAgLmZpcnN0X2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgLnVzZXJfaW1ne1xuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgICAgICBtaW4td2lkdGg6IDMwcHg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICAgICAgfVxuICAgIH1cbiAgICAuc2Vjb25kX2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcblxuICAgICAgICAuYXR7XG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICBjb2xvcjogIzUwNTA1MDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG5pb24tYnV0dG9uIHtcbiAgLS1ib3gtc2hhZG93Om5vbmU7XG59XG5cbmlvbi10aXRsZSB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb21tZW50c19kaXYge1xuICBtYXJnaW4tdG9wOiAxNXB4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgIzBlNzZhODtcbiAgcGFkZGluZzogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb21tZW50c19kaXYgLmxibHMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29tbWVudHNfZGl2IC5sYmxzIC5yZWxldmVudCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjNTA1MDUwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbW1lbnRzX2RpdiAuZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbW1lbnRzX2RpdiAuZmxleF9kaXYgLnVzZXJfaW1nIHtcbiAgaGVpZ2h0OiA0MHB4O1xuICB3aWR0aDogNDBweDtcbiAgbWluLXdpZHRoOiA0MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbW1lbnRzX2RpdiAuZmxleF9kaXYgLmJsdWVfZGl2IHtcbiAgcGFkZGluZzogMTBweDtcbiAgYmFja2dyb3VuZDogI0UxRTlFRTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbW1lbnRzX2RpdiAuZmxleF9kaXYgLmJsdWVfZGl2IC5hYnNfaWNuIHtcbiAgcmlnaHQ6IDEwcHg7XG4gIHRvcDogMTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBjb2xvcjogIzUwNTA1MDtcbiAgZm9udC1zaXplOiAxN3B4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbW1lbnRzX2RpdiAuZmxleF9kaXYgLmJsdWVfZGl2IC5hYnNfaWNuMiB7XG4gIHJpZ2h0OiAzMHB4O1xuICB0b3A6IDEwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgY29sb3I6ICM1MDUwNTA7XG4gIGZvbnQtc2l6ZTogMTdweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb21tZW50c19kaXYgLmZsZXhfZGl2IC5ibHVlX2RpdiAuaGVhZF9sYmwge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbW1lbnRzX2RpdiAuZmxleF9kaXYgLmJsdWVfZGl2IC5zbWFsbF9sYmwge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOiBncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbW1lbnRzX2RpdiAuZmxleF9kaXYgLmJsdWVfZGl2IC50ZXh0IHtcbiAgbWFyZ2luLXRvcDogN3B4O1xufVxuLm1haW5fY29udGVudF9kaXYgaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAubGlrZWQge1xuICBjb2xvcjogIzA0N2VhZCAhaW1wb3J0YW50O1xufVxuLm1haW5fY29udGVudF9kaXYgLnVubGlrZWQge1xuICBjb2xvcjogZ3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5oZWFkX2xibCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi1ibG9jay1lbmQ6IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgd2lkdGg6IDEwMCU7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYgLmltYWdlIHtcbiAgaGVpZ2h0OiA0NXB4O1xuICB3aWR0aDogNDVweDtcbiAgbWluLXdpZHRoOiA0NXB4O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiAuY29udGVudF9kaXYgLmhlYWRfbGJsIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IC5zbWFsbF9sYmwge1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IC5mYWxsb3dfYnRuIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBtYXJnaW46IDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5kZXNjX2xibCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiBzcGFuIHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvbnRlbnRfZGl2IC5jYXJkX2ltYWdlIHtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAubGlrZV9kaXYge1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmxpa2VfZGl2IGltZyB7XG4gIHdpZHRoOiAyMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAubGlrZV9kaXYgaW9uLWJ1dHRvbiB7XG4gIGNvbG9yOiBncmF5O1xuICBmb250LXNpemU6IDE2cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmxpa2VfZGl2MiB7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmxpa2VfZGl2MiBpbWcge1xuICB3aWR0aDogMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLmxpa2VfZGl2MiBpb24tYnV0dG9uIHtcbiAgY29sb3I6IGdyYXk7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAucmVhY3Rpb25fZGl2IHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAucmVhY3Rpb25fZGl2IC51c2Vyc19kaXYge1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY29udGVudF9kaXYgLnJlYWN0aW9uX2RpdiAudXNlcnNfZGl2IC51c2VyX2ltZyB7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDQwcHg7XG4gIG1pbi13aWR0aDogNDBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAucmVhY3Rpb25fZGl2IC51c2Vyc19kaXYgLnJvdW5kIHtcbiAgaGVpZ2h0OiA0MHB4O1xuICB3aWR0aDogNDBweDtcbiAgbWluLXdpZHRoOiA0MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJvcmRlcjogMnB4IHNvbGlkICM1MDUwNTA7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb250ZW50X2RpdiAucmVhY3Rpb25fZGl2IC51c2Vyc19kaXYgLnJvdW5kIC5kb3RzIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgY29sb3I6ICM1MDUwNTA7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuLnJlcGx5dHh0IHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzBhNDZhMDtcbiAgYm9yZGVyLWJvdHRvbTogM3B4IHNvbGlkICNmZmZlZmU7XG59XG5cbi5mb290ZXJfZGl2IHtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xufVxuLmZvb3Rlcl9kaXYgLnJlcGx5IHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5mb290ZXJfZGl2IC5mbGV4X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG4uZm9vdGVyX2RpdiAuZmxleF9kaXYgLnVzZXJfaW1nIHtcbiAgaGVpZ2h0OiA0MHB4O1xuICB3aWR0aDogNDBweDtcbiAgbWluLXdpZHRoOiA0MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuLmZvb3Rlcl9kaXYgLmZsZXhfZGl2IC5ibHVlX2RpdiB7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJhY2tncm91bmQ6ICNFMUU5RUU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5mb290ZXJfZGl2IC5mbGV4X2Zvb3Qge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uZm9vdGVyX2RpdiAuZmlyc3RfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5mb290ZXJfZGl2IC5maXJzdF9kaXYgLnVzZXJfaW1nIHtcbiAgaGVpZ2h0OiAzMHB4O1xuICB3aWR0aDogMzBweDtcbiAgbWluLXdpZHRoOiAzMHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuLmZvb3Rlcl9kaXYgLnNlY29uZF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmZvb3Rlcl9kaXYgLnNlY29uZF9kaXYgLmF0IHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBjb2xvcjogIzUwNTA1MDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/forum-reply/forum-reply.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/forum-reply/forum-reply.page.ts ***!
  \*******************************************************/
/*! exports provided: ForumReplyPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForumReplyPage", function() { return ForumReplyPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/dummy-data.service */ "./src/app/services/dummy-data.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/native-page-transitions/ngx */ "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
/* harmony import */ var _components_replycomment_replycomment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../components/replycomment/replycomment */ "./src/app/components/replycomment/replycomment.ts");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

















let ForumReplyPage = class ForumReplyPage {
    constructor(file, camera, imagePicker, plt, androidPermissions, loadingController, resolver, navCtrl, nativePageTransitions, dummy, storage, fb, api, alertCtrl, location, toastCtrl, router, route, actionSheet) {
        this.file = file;
        this.camera = camera;
        this.imagePicker = imagePicker;
        this.plt = plt;
        this.androidPermissions = androidPermissions;
        this.loadingController = loadingController;
        this.resolver = resolver;
        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.dummy = dummy;
        this.storage = storage;
        this.fb = fb;
        this.api = api;
        this.alertCtrl = alertCtrl;
        this.location = location;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.route = route;
        this.actionSheet = actionSheet;
        this.commentid = '';
        this.options = {
            direction: 'up',
            duration: 500,
            slowdownfactor: 3,
            slidePixels: 20,
            iosdelay: 100,
            androiddelay: 150,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 60
        };
        this.file_ids = [];
        this.file_imgs = [];
        this.privacy = "public";
        this.users = this.dummy.users;
        this.storage.get('USER_INFO').then(result => {
            console.log("USER_INFO", result);
            this.mydata = result.avatar_urls[24];
            console.log(result.avatar_urls[24]);
            if (typeof result.avatar_urls[24] === "undefined")
                this.mydata = result.avatar_urls['full'];
            this.user_id = result.id;
        }).catch(e => {
            console.log('error: ' + e);
            // Handle errors here
        });
    }
    ionViewWillLeave() {
        document.getElementById('autocomplete');
        this.nativePageTransitions.slide(this.options);
    }
    // example of adding a transition when pushing a new page
    openPage(page) {
        this.nativePageTransitions.slide(this.options);
        this.navCtrl.navigateForward(page);
    }
    loaddata(data1) {
        const factory = this.resolver.resolveComponentFactory(_components_replycomment_replycomment__WEBPACK_IMPORTED_MODULE_10__["ReplyComponent"]);
        let componentRef = this.container.createComponent(factory);
        componentRef.instance.data = data1;
    }
    ngOnInit() {
        this.commentForm = this.fb.group({
            comment: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]
        });
        this.loading = false;
        this.route.queryParams.subscribe(params => {
            if (params && params.special) {
                this.post = JSON.parse(params.special);
            }
            console.log("data pass", this.post);
            this.api.getForumReplies(this.post.id).subscribe(res => {
                this.data = res;
                console.log("data", res);
            });
            console.log("data", this.data);
        });
        this.api.getuser(this.post.user_id).subscribe(res => {
            this.userdata = res;
            if (this.userdata.is_following)
                this.unfollow = true;
            else
                this.follow = true;
            console.log("data", this.userdata.is_following);
        });
    }
    BackButton() {
        this.location.back();
    }
    selectImage() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE));
            this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
            // this.loading=true;
            let options = {
                // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
                // selection of a single image, the plugin will return it.
                //maximumImagesCount: 3,
                // max width and height to allow the images to be.  Will keep aspect
                // ratio no matter what.  So if both are 800, the returned image
                // will be at most 800 pixels wide and 800 pixels tall.  If the width is
                // 800 and height 0 the image will be 800 pixels wide if the source
                // is at least that wide.
                width: 200,
                //height: 200,
                // quality of resized image, defaults to 100
                quality: 25,
                // output type, defaults to FILE_URIs.
                // available options are 
                // window.imagePicker.OutputType.FILE_URI (0) or 
                // window.imagePicker.OutputType.BASE64_STRING (1)
                outputType: 1
            };
            let imageResponse = [];
            this.imagePicker.getPictures(options).then((results) => {
                for (var i = 0; i < results.length; i++) {
                    this.file.resolveLocalFilesystemUrl(results[i]).then((entry) => {
                        entry.file(file => {
                            console.log(file);
                            this.readFile(file);
                        });
                    });
                }
            }, (err) => {
                alert(err);
            });
        });
    }
    addImage() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const options = {
                quality: 100,
                destinationType: this.camera.DestinationType.FILE_URI,
                encodingType: this.camera.EncodingType.JPEG,
                mediaType: this.camera.MediaType.PICTURE
            };
            this.camera.getPicture(options).then((imageData) => {
                this.file.resolveLocalFilesystemUrl(imageData).then((entry) => {
                    entry.file(file => {
                        console.log(file);
                        this.readFile(file);
                    });
                });
            }, (err) => {
                // Handle error
            });
        });
    }
    selectcoverImageSource() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const buttons = [
                {
                    text: 'Take Photo',
                    icon: 'camera',
                    handler: () => {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA));
                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                        this.addImage();
                    }
                },
                {
                    text: 'Choose From Photos Photo',
                    icon: 'image',
                    handler: () => {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE));
                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                        this.selectImage();
                    }
                }
            ];
            // Only allow file selection inside a browser
            if (!this.plt.is('hybrid')) {
                buttons.push({
                    text: 'Choose a File',
                    icon: 'attach',
                    handler: () => {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => console.log('Has permission?', result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE));
                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                        this.fileInput.nativeElement.click();
                    }
                });
            }
            const actionSheet = yield this.actionSheet.create({
                header: 'Select Image Source',
                buttons
            });
            yield actionSheet.present();
        });
    }
    dltmedia(id) {
        var index = this.file_imgs.findIndex(data => data.id === id);
        this.file_imgs.splice(index, 1);
        var index2 = this.file_ids.findIndex(data => data === id);
        this.file_ids.splice(index2, 1);
        // console.log(this.file_imgs);
        // console.log(index);
        //     this.api.deltmedia(id,this.token).subscribe(
        //       async (res : any) => {
        //       console.log("---------------");
        //       console.log(res);
        //       const toast = await this.toastCtrl.create({
        //         message: 'Image Sucessfully Removed',
        //         duration: 3000
        //       });
        //       toast.present();
        // // Get the entire data
        //     },
        //     err => {
        //       console.log(err);
        //       this.showError(err);
        //     }
        //     );
    }
    attach() {
        this.fileInput.nativeElement.click();
    }
    uploadFile(event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(this.file_ids);
            const loading = yield this.loadingController.create({
                message: 'Please wait...',
                duration: 2000
            });
            yield loading.present();
            const eventObj = event;
            const target = eventObj.target;
            const file1 = target.files[0];
            this.api.uploadMedia(file1).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                this.file_ids.push(res.upload_id);
                this.file_imgs.push({
                    img: res.upload_thumb,
                    id: res.upload_id
                });
                console.log("---------------");
                console.log(this.file_ids);
                const toast = yield this.toastCtrl.create({
                    message: 'Image Uploaded',
                    duration: 3000
                });
                toast.present();
                // Get the entire data
            }), err => {
                console.log(err);
                this.showError(err);
            });
        });
    }
    readFile(file) {
        console.log(file.type);
        const reader = new FileReader();
        reader.onload = () => {
            this.blob = new Blob([reader.result], {
                type: file.type
            });
            this.file_name = file.name;
            this.file_type = file.type;
        };
        reader.readAsArrayBuffer(file);
    }
    ;
    ionViewWillEnter() {
        // this.selectcoverImageSource();
    }
    unfollowuser() {
        console.log(jquery__WEBPACK_IMPORTED_MODULE_11__("#id75").text());
        this.api.followuser("unfollow", this.userdata.id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(res);
            this.follow = true;
            this.unfollow = false;
            const toast = yield this.toastCtrl.create({
                message: "Unfollow User Successfully",
                duration: 3000
            });
            yield toast.present();
        }));
    }
    hightlight(id) { console.log(id); jquery__WEBPACK_IMPORTED_MODULE_11__('#id' + id).delay(100).fadeOut().fadeIn('slow'); }
    followuser() {
        this.api.followuser("follow", this.userdata.id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(res);
            this.unfollow = true;
            this.follow = false;
            const toast = yield this.toastCtrl.create({
                message: "Follow User Successfully",
                duration: 3000
            });
            yield toast.present();
        }));
    }
    likefeed(idd) {
        this.api.activitylike(idd).subscribe((res) => {
            console.log(res);
            this.post.favorited = res.favorited;
            this.post.favorite_count = res.favorite_count;
        });
    }
    deletepost(idd) {
        this.api.Replydelete(idd).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: "Comment Deleted Successfully",
                duration: 3000
            });
            yield toast.present();
            this.api.getForumReplies(this.post.id).subscribe(res => {
                this.data = res;
                console.log("data", res);
            });
        }));
    }
    commentfocus() {
        this.searchInput.setFocus();
    }
    commentreply(id, comment, commentavatar) {
        this.commenttext = comment;
        this.commentid = id;
        this.commentavatar = commentavatar;
        console.log(id, comment);
        this.searchInput.setFocus();
    }
    postcomment() {
        this.loading = true;
        if (this.commentid != '') {
            this.api.postreply(this.comment, this.post.id, this.commentid, this.file_ids).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                console.log(res);
                // this.data = res;
                const toast = yield this.toastCtrl.create({
                    message: "Comment Posted Successfully",
                    duration: 3000
                });
                yield toast.present();
                this.comment = "";
                this.commenttext = "";
                this.commentid = "";
                this.commentavatar = "";
                this.loading = false;
                this.api.getForumReplies(this.post.id).subscribe(res => {
                    this.data = res;
                    console.log("data", res);
                });
                console.log("data", this.data);
                //  window.location.reload(); 
                //  location.href = "http://localhost:8100/";
                // this.router.navigate(['/']);
            }), err => {
                this.loading = false;
                console.log(err);
                this.showError(err);
            });
        }
        else {
            this.api.postreply(this.comment, this.post.id, 0, this.file_ids).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                console.log(res);
                // this.data = res;
                const toast = yield this.toastCtrl.create({
                    message: "Comment Posted Successfully",
                    duration: 3000
                });
                yield toast.present();
                this.comment = "";
                this.commenttext = "";
                this.commentid = "";
                this.commentavatar = "";
                this.loading = false;
                this.api.getForumReplies(this.post.id).subscribe(res => {
                    this.data = res;
                    console.log("data", res);
                });
                //  window.location.reload(); 
                //  location.href = "http://localhost:8100/";
                console.log("data", this.data);
                // this.router.navigate(['/']);
            }), err => {
                this.loading = false;
                console.log(err);
                this.showError(err);
            });
        }
    }
    showError(err) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: "Post Comment Error",
                message: err.error.message,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    openActionSheet() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheet.create({
                mode: 'md',
                buttons: [
                    {
                        text: 'Save',
                        role: 'destructive',
                        icon: 'bookmark-outline',
                        handler: () => {
                            console.log('Delete clicked');
                        }
                    },
                    {
                        text: 'Send in a private Message',
                        icon: 'chatbox-ellipses-outline',
                        handler: () => {
                            console.log('Share clicked');
                        }
                    },
                    {
                        text: 'Share via',
                        icon: 'share-social',
                        handler: () => {
                            console.log('Play clicked');
                        }
                    },
                    {
                        text: 'Hide this post',
                        icon: 'close-circle-outline',
                        handler: () => {
                            console.log('Favorite clicked');
                        }
                    },
                    {
                        text: 'Unfollow',
                        icon: 'person-remove-outline',
                        handler: () => {
                            console.log('Favorite clicked');
                        }
                    },
                    {
                        text: 'Report this post',
                        icon: 'flag-outline',
                        handler: () => {
                            console.log('Favorite clicked');
                        }
                    },
                    {
                        text: 'Improve my feed',
                        icon: 'options-outline',
                        handler: () => {
                            console.log('Favorite clicked');
                        }
                    },
                    {
                        text: 'Who can see this post?',
                        icon: 'eye-outline',
                        handler: () => {
                            console.log('Favorite clicked');
                        }
                    }
                ]
            });
            yield actionSheet.present();
        });
    }
};
ForumReplyPage.ctorParameters = () => [
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_15__["File"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_12__["Camera"] },
    { type: _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_13__["ImagePicker"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
    { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_14__["AndroidPermissions"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_9__["NativePageTransitions"] },
    { type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_3__["dummyDataService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"] },
    { type: _services_api_service__WEBPACK_IMPORTED_MODULE_5__["apiService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('commentfoc', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], ForumReplyPage.prototype, "searchInput", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('processContainer', { read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], ForumReplyPage.prototype, "container", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], ForumReplyPage.prototype, "fileInput", void 0);
ForumReplyPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-forum-reply',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./forum-reply.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forum-reply/forum-reply.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./forum-reply.page.scss */ "./src/app/pages/forum-reply/forum-reply.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_15__["File"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_12__["Camera"],
        _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_13__["ImagePicker"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"],
        _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_14__["AndroidPermissions"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
        _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_9__["NativePageTransitions"], src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_3__["dummyDataService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
        _services_api_service__WEBPACK_IMPORTED_MODULE_5__["apiService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"]])
], ForumReplyPage);



/***/ })

}]);
//# sourceMappingURL=pages-forum-reply-forum-reply-module-es2015.js.map
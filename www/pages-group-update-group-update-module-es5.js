function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-group-update-group-update-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/group-update/group-update.page.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/group-update/group-update.page.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesGroupUpdateGroupUpdatePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button color=\"light\" mode=\"md\"></ion-back-button>\n    </ion-buttons>\n   \n    <ion-title *ngIf=\"!update\"  color=\"light\">Create Group</ion-title>\n    <ion-title *ngIf=\"update\"  color=\"light\">Update Group</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n   \n\n\n    <div class=\"form_div\">\n   \n\n\n       <ion-card id=\"content\" *ngIf=\"update\"  [style.backgroundImage]=\"'url('+groupdata.cover+')'\">\n        <ion-label (click)=\"selectcoverImageSource()\" class=\"edit-btn\"> <ion-spinner style=\"color:white\" *ngIf=\"loading3\"></ion-spinner> <ion-icon *ngIf=\"edit_icon2\" name=\"create-outline\"></ion-icon></ion-label>\n        \n        <ion-label (click)=\"selectImageSource()\" class=\"edit-btn2\"><ion-spinner style=\"color:white\" *ngIf=\"loading\"></ion-spinner>  <ion-icon *ngIf=\"edit_icon\" name=\"create-outline\"></ion-icon></ion-label>\n          \n        <ion-avatar id=\"profile-info\">\n          <img id=\"profile-image\" src=\"{{groupdata.media_url}}\">\n        </ion-avatar>\n        <input\n      type=\"file\"\n      #fileInput\n      (change)=\"uploadFile($event)\"\n      hidden=\"true\"\n      accept=\"image/*\"\n    />\n    <input\n    type=\"file\"\n    #fileInput2\n    (change)=\"uploadCoverFile($event)\"\n    hidden=\"true\"\n    accept=\"image/*\"\n  />\n      </ion-card>\n      <form [formGroup]=\"userForm\" (ngSubmit)=\"userupdate()\">\n      <ion-item>\n        <ion-label position=\"floating\">Group Name</ion-label>\n        <ion-input  formControlName=\"groupname\" [(ngModel)]=\"groupdata.name\" class=\"ip\"></ion-input>\n      </ion-item>\n     \n      <ion-item>\n        <ion-label position=\"floating\">Group Description</ion-label>\n        <ion-textarea formControlName=\"groupdescription\" [(ngModel)]=\"groupdata.description\" class=\"ip\"></ion-textarea>\n      </ion-item>\n     \n\n      <ion-button class=\"join_now\" *ngIf=\"update\" type=\"submit\" expand=\"full\">\n         <ion-spinner *ngIf=\"loading2\"></ion-spinner> <ion-label >   Update </ion-label>\n      </ion-button>\n      <ion-button class=\"join_now\" *ngIf=\"!update\" type=\"submit\" expand=\"full\">\n        <ion-spinner *ngIf=\"loading2\"></ion-spinner> <ion-label >   Create </ion-label>\n     </ion-button>\n      </form>\n    </div>\n\n  \n  </div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/group-update/group-update-routing.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/pages/group-update/group-update-routing.module.ts ***!
    \*******************************************************************/

  /*! exports provided: GroupUpdatePageRoutingModule */

  /***/
  function srcAppPagesGroupUpdateGroupUpdateRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GroupUpdatePageRoutingModule", function () {
      return GroupUpdatePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _group_update_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./group-update.page */
    "./src/app/pages/group-update/group-update.page.ts");

    var routes = [{
      path: '',
      component: _group_update_page__WEBPACK_IMPORTED_MODULE_3__["GroupUpdatePage"]
    }];

    var GroupUpdatePageRoutingModule = function GroupUpdatePageRoutingModule() {
      _classCallCheck(this, GroupUpdatePageRoutingModule);
    };

    GroupUpdatePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], GroupUpdatePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/group-update/group-update.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/pages/group-update/group-update.module.ts ***!
    \***********************************************************/

  /*! exports provided: GroupUpdatePageModule */

  /***/
  function srcAppPagesGroupUpdateGroupUpdateModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GroupUpdatePageModule", function () {
      return GroupUpdatePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _group_update_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./group-update-routing.module */
    "./src/app/pages/group-update/group-update-routing.module.ts");
    /* harmony import */


    var _group_update_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./group-update.page */
    "./src/app/pages/group-update/group-update.page.ts");

    var GroupUpdatePageModule = function GroupUpdatePageModule() {
      _classCallCheck(this, GroupUpdatePageModule);
    };

    GroupUpdatePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _group_update_routing_module__WEBPACK_IMPORTED_MODULE_5__["GroupUpdatePageRoutingModule"]],
      declarations: [_group_update_page__WEBPACK_IMPORTED_MODULE_6__["GroupUpdatePage"]]
    })], GroupUpdatePageModule);
    /***/
  },

  /***/
  "./src/app/pages/group-update/group-update.page.scss":
  /*!***********************************************************!*\
    !*** ./src/app/pages/group-update/group-update.page.scss ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesGroupUpdateGroupUpdatePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".header_div {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n.header_div .logo_div {\n  display: flex;\n}\n.header_div .logo_div .header_lbl {\n  color: var(--ion-color-main);\n  font-weight: bold;\n  font-size: 18px;\n  margin-left: 10px;\n}\n.header_div .logo_div .logo_img {\n  margin-left: 1px;\n  width: 20px;\n  height: 20px;\n}\n.header_div ion-button {\n  color: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 14px;\n}\nion-toolbar {\n  --background: var(--ion-color-main);\n}\nion-toolbar ion-input {\n  border-bottom: 1px solid white;\n  --color: white;\n  --placeholder-color: white;\n  --padding-start: 8px;\n}\nion-toolbar ion-icon {\n  color: white;\n  font-size: 24px;\n}\n#content {\n  background-color: #ffffff;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n  height: 305px;\n  overflow: unset;\n  display: block;\n  border-radius: 15px;\n  background-size: cover;\n}\n.edit-btn {\n  font-size: 23px;\n  float: right;\n  margin: 10px;\n  color: white;\n  padding: 7px;\n  background: var(--ion-color-main);\n  border-radius: 10px;\n  position: absolute;\n  right: 0%;\n}\n.edit-btn2 {\n  font-size: 23px;\n  float: right;\n  margin: 10px;\n  color: white;\n  padding: 7px;\n  background: var(--ion-color-main);\n  border-radius: 10px;\n  position: absolute;\n  right: 28%;\n  z-index: 10;\n  top: 51%;\n}\n#profile-info {\n  width: 100%;\n  z-index: 2;\n  padding-top: 1px;\n  text-align: center;\n  position: absolute;\n  top: 45%;\n}\n#profile-image {\n  display: block;\n  border-radius: 120px;\n  border: 3px solid #fff;\n  width: 128px;\n  background: white;\n  height: 128px;\n  margin: 30px auto 0;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n}\n.main_content_div {\n  width: 100%;\n  padding: 20px;\n  height: 100%;\n}\n.main_content_div .header_lbl {\n  font-size: 26px;\n  font-weight: 600;\n}\n.main_content_div .join_btn {\n  --background: white;\n  --color: #0077B5;\n  border: 1px solid var(--ion-color-main);\n}\n.main_content_div .btn_search {\n  height: 25px;\n  width: 25px;\n  margin-right: 20px;\n}\n.main_content_div .or_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  height: 50px;\n  align-items: center;\n  justify-content: space-between;\n}\n.main_content_div .or_div .line_img {\n  height: 10px;\n  width: 40%;\n}\n.main_content_div .or_div .or_text {\n  color: black;\n}\n.main_content_div .form_div ion-item {\n  border-bottom: none;\n  --padding-start: 0px;\n}\n.main_content_div .form_div ion-item .ip {\n  border-bottom: 1px solid white;\n}\n.main_content_div .form_div .join_now {\n  margin-top: 20px;\n  border: 1px solid white;\n  --background: var(--ion-color-main);\n  font-weight: bold;\n  font-size: 18px;\n}\n.main_content_div .form_div .bottom_lbl {\n  display: block;\n  font-size: 14px;\n  margin-top: 20px;\n}\n.main_content_div .form_div .bottom_lbl span {\n  color: var(--ion-color-main) !important;\n  font-weight: bold;\n  color: gra;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZ3JvdXAtdXBkYXRlL0Q6XFxSYWd1dmFyYW4gaW1hZ2VcXGRpc2Vsc2hpcC9zcmNcXGFwcFxccGFnZXNcXGdyb3VwLXVwZGF0ZVxcZ3JvdXAtdXBkYXRlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZ3JvdXAtdXBkYXRlL2dyb3VwLXVwZGF0ZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FDQ0o7QURBSTtFQUNJLGFBQUE7QUNFUjtBRERRO0VBQ0ksNEJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0daO0FERFE7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDR1o7QURBSTtFQUNJLDRCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDRVI7QURDQTtFQUNJLG1DQUFBO0FDRUo7QURESTtFQUNJLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0VBQ0Esb0JBQUE7QUNHUjtBRERJO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUNHUjtBREFBO0VBQ0kseUJBQUE7RUFDQSw0Q0FBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNHSjtBRERFO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxpQ0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0FDSUo7QURGQTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUNBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0FDS0o7QURIRTtFQUNFLFdBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBRUEsUUFBQTtBQ0tKO0FESEU7RUFDRSxjQUFBO0VBQ0Esb0JBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFFQSxtQkFBQTtFQUNBLDRDQUFBO0FDS0o7QUREQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQ0lKO0FERkk7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUNJUjtBREZJO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtFQUVBLHVDQUFBO0FDR1I7QURESTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUNHUjtBRERJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FDR1I7QUREUTtFQUNJLFlBQUE7RUFDQSxVQUFBO0FDR1o7QUREUTtFQUNJLFlBQUE7QUNHWjtBREVRO0VBQ0ksbUJBQUE7RUFDQSxvQkFBQTtBQ0FaO0FEQ1k7RUFDSSw4QkFBQTtBQ0NoQjtBREdRO0VBQ0ksZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1DQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDRFo7QURJUTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNGWjtBRElZO0VBQ0ksdUNBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7QUNGaEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ncm91cC11cGRhdGUvZ3JvdXAtdXBkYXRlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXJfZGl2e1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAubG9nb19kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIC5oZWFkZXJfbGJse1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgIH1cbiAgICAgICAgLmxvZ29faW1ne1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDFweDtcbiAgICAgICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICAgICAgaGVpZ2h0OiAyMHB4O1xuICAgICAgICB9XG4gICAgfVxuICAgIGlvbi1idXR0b257XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICB9XG59XG5pb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgaW9uLWlucHV0e1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG4gICAgICAgIC0tY29sb3I6IHdoaXRlO1xuICAgICAgICAtLXBsYWNlaG9sZGVyLWNvbG9yIDogd2hpdGU7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydCA6IDhweDtcbiAgICB9XG4gICAgaW9uLWljb257XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xuICAgIH1cbn1cbiNjb250ZW50IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgIGJveC1zaGFkb3c6IDBweCAtMXB4IDEwcHggcmdiKDAgMCAwIC8gNDAlKTtcbiAgICBoZWlnaHQ6IDMwNXB4O1xuICAgIG92ZXJmbG93OiB1bnNldDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIH1cbiAgLmVkaXQtYnRue1xuICAgIGZvbnQtc2l6ZTogMjNweDtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgbWFyZ2luOiAxMHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBwYWRkaW5nOiA3cHg7XG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAwJTtcbn1cbi5lZGl0LWJ0bjJ7XG4gICAgZm9udC1zaXplOiAyM3B4O1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBtYXJnaW46IDEwcHg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHBhZGRpbmc6IDdweDtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDI4JTtcbiAgICB6LWluZGV4OiAxMDtcbiAgICB0b3A6IDUxJTtcbn1cbiAgI3Byb2ZpbGUtaW5mbyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgei1pbmRleDogMjtcbiAgICBwYWRkaW5nLXRvcDogMXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgXG4gICAgdG9wOiA0NSU7XG4gIH1cbiAgI3Byb2ZpbGUtaW1hZ2Uge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGJvcmRlci1yYWRpdXM6IDEyMHB4O1xuICAgIGJvcmRlcjogM3B4IHNvbGlkICNmZmY7XG4gICAgd2lkdGg6IDEyOHB4O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIGhlaWdodDogMTI4cHg7XG4gICBcbiAgICBtYXJnaW46IDMwcHggYXV0byAwO1xuICAgIGJveC1zaGFkb3c6IDBweCAtMXB4IDEwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAgLy8gYm94LXNoYWRvdzogMHB4IDBweCA0cHggcmdiYSgwLCAwLCAwLCAwLjcpO1xuICB9XG5cbi5tYWluX2NvbnRlbnRfZGl2e1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgaGVpZ2h0OiAxMDAlO1xuXG4gICAgLmhlYWRlcl9sYmx7XG4gICAgICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICB9XG4gICAgLmpvaW5fYnRue1xuICAgICAgICAtLWJhY2tncm91bmQgOiB3aGl0ZTtcbiAgICAgICAgLS1jb2xvcjogIzAwNzdCNTtcbiAgICAgICAgLy8gLS1ib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgfVxuICAgIC5idG5fc2VhcmNoe1xuICAgICAgICBoZWlnaHQ6IDI1cHg7XG4gICAgICAgIHdpZHRoOiAyNXB4O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG4gICAgfVxuICAgIC5vcl9kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcblxuICAgICAgICAubGluZV9pbWd7XG4gICAgICAgICAgICBoZWlnaHQ6IDEwcHg7XG4gICAgICAgICAgICB3aWR0aDogNDAlO1xuICAgICAgICB9XG4gICAgICAgIC5vcl90ZXh0e1xuICAgICAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmZvcm1fZGl2e1xuICAgICAgICBpb24taXRlbXtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IG5vbmU7XG4gICAgICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgICAgICAgICAgIC5pcHtcbiAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAuam9pbl9ub3d7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgd2hpdGU7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5ib3R0b21fbGJse1xuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuXG4gICAgICAgICAgICBzcGFue1xuICAgICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbikgIWltcG9ydGFudDtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgICAgICBjb2xvcjogZ3JhO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufSIsIi5oZWFkZXJfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmhlYWRlcl9kaXYgLmxvZ29fZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5oZWFkZXJfZGl2IC5sb2dvX2RpdiAuaGVhZGVyX2xibCB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE4cHg7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLmhlYWRlcl9kaXYgLmxvZ29fZGl2IC5sb2dvX2ltZyB7XG4gIG1hcmdpbi1sZWZ0OiAxcHg7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG59XG4uaGVhZGVyX2RpdiBpb24tYnV0dG9uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXRvb2xiYXIgaW9uLWlucHV0IHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAtLWNvbG9yOiB3aGl0ZTtcbiAgLS1wbGFjZWhvbGRlci1jb2xvcjogd2hpdGU7XG4gIC0tcGFkZGluZy1zdGFydDogOHB4O1xufVxuaW9uLXRvb2xiYXIgaW9uLWljb24ge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMjRweDtcbn1cblxuI2NvbnRlbnQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgaGVpZ2h0OiAzMDVweDtcbiAgb3ZlcmZsb3c6IHVuc2V0O1xuICBkaXNwbGF5OiBibG9jaztcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cblxuLmVkaXQtYnRuIHtcbiAgZm9udC1zaXplOiAyM3B4O1xuICBmbG9hdDogcmlnaHQ7XG4gIG1hcmdpbjogMTBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiA3cHg7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMCU7XG59XG5cbi5lZGl0LWJ0bjIge1xuICBmb250LXNpemU6IDIzcHg7XG4gIGZsb2F0OiByaWdodDtcbiAgbWFyZ2luOiAxMHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDdweDtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAyOCU7XG4gIHotaW5kZXg6IDEwO1xuICB0b3A6IDUxJTtcbn1cblxuI3Byb2ZpbGUtaW5mbyB7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAyO1xuICBwYWRkaW5nLXRvcDogMXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA0NSU7XG59XG5cbiNwcm9maWxlLWltYWdlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJvcmRlci1yYWRpdXM6IDEyMHB4O1xuICBib3JkZXI6IDNweCBzb2xpZCAjZmZmO1xuICB3aWR0aDogMTI4cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEyOHB4O1xuICBtYXJnaW46IDMwcHggYXV0byAwO1xuICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMjBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmhlYWRlcl9sYmwge1xuICBmb250LXNpemU6IDI2cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuam9pbl9idG4ge1xuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLWNvbG9yOiAjMDA3N0I1O1xuICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuYnRuX3NlYXJjaCB7XG4gIGhlaWdodDogMjVweDtcbiAgd2lkdGg6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5vcl9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA1MHB4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4ubWFpbl9jb250ZW50X2RpdiAub3JfZGl2IC5saW5lX2ltZyB7XG4gIGhlaWdodDogMTBweDtcbiAgd2lkdGg6IDQwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5vcl9kaXYgLm9yX3RleHQge1xuICBjb2xvcjogYmxhY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZm9ybV9kaXYgaW9uLWl0ZW0ge1xuICBib3JkZXItYm90dG9tOiBub25lO1xuICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mb3JtX2RpdiBpb24taXRlbSAuaXAge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZm9ybV9kaXYgLmpvaW5fbm93IHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgd2hpdGU7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxOHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZvcm1fZGl2IC5ib3R0b21fbGJsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mb3JtX2RpdiAuYm90dG9tX2xibCBzcGFuIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IGdyYTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/group-update/group-update.page.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/group-update/group-update.page.ts ***!
    \*********************************************************/

  /*! exports provided: GroupUpdatePage */

  /***/
  function srcAppPagesGroupUpdateGroupUpdatePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GroupUpdatePage", function () {
      return GroupUpdatePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! sweetalert2 */
    "./node_modules/sweetalert2/dist/sweetalert2.all.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);
    /* harmony import */


    var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/android-permissions/ngx */
    "./node_modules/@ionic-native/android-permissions/ngx/index.js");
    /* harmony import */


    var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/camera/ngx */
    "./node_modules/@ionic-native/camera/ngx/index.js");
    /* harmony import */


    var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/image-picker/ngx */
    "./node_modules/@ionic-native/image-picker/ngx/index.js");
    /* harmony import */


    var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @ionic-native/file/ngx */
    "./node_modules/@ionic-native/file/ngx/index.js");
    /* harmony import */


    var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @ionic-native/native-page-transitions/ngx */
    "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var GroupUpdatePage = /*#__PURE__*/function () {
      function GroupUpdatePage(navCtrl, nativePageTransitions, imagePicker, plt, file, loadingController, router, route, api, camera, androidPermissions, storage, fb, actionSheetCtrl, alertCtrl, toastCtrl) {
        var _this = this;

        _classCallCheck(this, GroupUpdatePage);

        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.imagePicker = imagePicker;
        this.plt = plt;
        this.file = file;
        this.loadingController = loadingController;
        this.router = router;
        this.route = route;
        this.api = api;
        this.camera = camera;
        this.androidPermissions = androidPermissions;
        this.storage = storage;
        this.fb = fb;
        this.actionSheetCtrl = actionSheetCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.data = {};
        this.update = false;
        this.images = [];
        this.user = this.api.getCurrentUser();
        this.posts = [];
        this.groupdata = [];
        this.options1 = {
          direction: 'up',
          duration: 500,
          slowdownfactor: 3,
          slidePixels: 20,
          iosdelay: 100,
          androiddelay: 150,
          fixedPixelsTop: 0,
          fixedPixelsBottom: 60
        };
        this.route.queryParams.subscribe(function (data) {
          console.log("data.id", data.id);
          if (typeof data.id !== "undefined") _this.update = true;
          console.log(data); // this.userForm.value.groupname = data.name;
          // this.userForm.value.groupdescription = data.full_description;

          _this.groupdata.id = data.id;
          _this.groupdata.name = data.name;
          _this.groupdata.description = data.description;
          _this.groupdata.cover = data.cover;
          _this.groupdata.media_url = data.media_url;
        });
        this.edit_icon = true;
        this.edit_icon2 = true;
        this.user.subscribe(function (user) {
          if (user) {
            //
            console.log(user);

            _this.storage.get('COMPLETE_USER_INFO').then(function (result) {
              if (result != null) {
                console.log(result);
                _this.token = result.token;
                _this.user_id = result.user_id;
                console.log('user_id: ' + result.user_id);
                console.log('token: ' + result.token);
              }
            })["catch"](function (e) {
              console.log('error: ' + e); // Handle errors here
            });
          } else {
            _this.posts = [];
          }
        });
      }

      _createClass(GroupUpdatePage, [{
        key: "userupdate",
        value: function userupdate() {
          var _this2 = this;

          this.loading2 = true;
          this.api.groupCreate(this.userForm.value.groupname, this.userForm.value.groupdescription).subscribe(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      this.loading2 = false;
                      console.log(res);
                      _context.next = 4;
                      return this.toastCtrl.create({
                        message: 'Profile Updated',
                        duration: 3000
                      });

                    case 4:
                      toast = _context.sent;
                      toast.present();
                      this.update = true;
                      this.groupdata = res;
                      this.groupdata.id = res.id;
                      this.groupdata.name = res.name;
                      this.groupdata.description = res.description.raw;
                      this.groupdata.media_url = res.avatar_urls.thumb;
                      this.groupdata.cover = res.cover_url.raw;

                    case 13:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }, function (err) {
            _this2.loading2 = false;
            console.log(err);

            _this2.showError(err);
          });
        }
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.nativePageTransitions.slide(this.options1);
        } // example of adding a transition when pushing a new page

      }, {
        key: "openPage",
        value: function openPage(page) {
          this.nativePageTransitions.slide(this.options1);
          this.navCtrl.navigateForward(page);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.userForm = this.fb.group({
            groupname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            groupdescription: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]
          });
        }
      }, {
        key: "loadPrivatePosts",
        value: function loadPrivatePosts() {
          var _this3 = this;

          this.api.getPrivatePosts().subscribe(function (res) {
            _this3.posts = res;
          });
        } // Used for browser direct file upload

      }, {
        key: "selectImage",
        value: function selectImage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var _this4 = this;

            var options, imageResponse;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                      return console.log('Has permission?', result.hasPermission);
                    }, function (err) {
                      return _this4.androidPermissions.requestPermission(_this4.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                    });
                    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]); // this.loading=true;

                    options = {
                      maximumImagesCount: 1,
                      width: 200,
                      quality: 25,
                      outputType: 1
                    };
                    imageResponse = [];
                    this.imagePicker.getPictures(options).then(function (results) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                        var _this5 = this;

                        var imageData, byteCharacters, byteNumbers, i, byteArray, blob1;
                        return regeneratorRuntime.wrap(function _callee3$(_context3) {
                          while (1) {
                            switch (_context3.prev = _context3.next) {
                              case 0:
                                imageData = results[0].toString(); //  var imageData1 = imageData.toString();

                                byteCharacters = atob(imageData.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
                                byteNumbers = new Array(byteCharacters.length);

                                for (i = 0; i < byteCharacters.length; i++) {
                                  byteNumbers[i] = byteCharacters.charCodeAt(i);
                                }

                                byteArray = new Uint8Array(byteNumbers);
                                blob1 = new Blob([byteArray], {
                                  type: "jpg"
                                });
                                console.log(blob1);
                                this.api.uploadgroup(blob1, "jpg", "test", this.token, "bp_avatar_upload", this.groupdata.id, "avatar").subscribe(function (res) {
                                  return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                                    var toast;
                                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                      while (1) {
                                        switch (_context2.prev = _context2.next) {
                                          case 0:
                                            this.groupdata.media_url = res.thumb;
                                            _context2.next = 3;
                                            return this.toastCtrl.create({
                                              message: 'Image Uploaded',
                                              duration: 3000
                                            });

                                          case 3:
                                            toast = _context2.sent;
                                            toast.present(); // Get the entire data

                                          case 5:
                                          case "end":
                                            return _context2.stop();
                                        }
                                      }
                                    }, _callee2, this);
                                  }));
                                }, function (err) {
                                  console.log(err);

                                  _this5.showError(err);
                                }); //   });
                                //  });
                                //  } 

                              case 8:
                              case "end":
                                return _context3.stop();
                            }
                          }
                        }, _callee3, this);
                      }));
                    }, function (err) {
                      alert(err);
                    });

                  case 5:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "uploadFile",
        value: function uploadFile(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var _this6 = this;

            var loading, eventObj, target, file1;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.loadingController.create({
                      message: 'Please wait...',
                      duration: 2000
                    });

                  case 2:
                    loading = _context6.sent;
                    _context6.next = 5;
                    return loading.present();

                  case 5:
                    eventObj = event;
                    target = eventObj.target;
                    file1 = target.files[0];
                    this.api.uploadgroup2(file1, "bp_avatar_upload", this.groupdata.id, "avatar").subscribe(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this6, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                        var toast;
                        return regeneratorRuntime.wrap(function _callee5$(_context5) {
                          while (1) {
                            switch (_context5.prev = _context5.next) {
                              case 0:
                                this.groupdata.media_url = res.thumb;
                                _context5.next = 3;
                                return this.toastCtrl.create({
                                  message: 'Image Uploaded',
                                  duration: 3000
                                });

                              case 3:
                                toast = _context5.sent;
                                toast.present(); // Get the entire data

                              case 5:
                              case "end":
                                return _context5.stop();
                            }
                          }
                        }, _callee5, this);
                      }));
                    }, function (err) {
                      console.log(err);

                      _this6.showError(err);
                    });

                  case 9:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }, {
        key: "selectImage2",
        value: function selectImage2() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
            var _this7 = this;

            var options, imageResponse;
            return regeneratorRuntime.wrap(function _callee9$(_context9) {
              while (1) {
                switch (_context9.prev = _context9.next) {
                  case 0:
                    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                      return console.log('Has permission?', result.hasPermission);
                    }, function (err) {
                      return _this7.androidPermissions.requestPermission(_this7.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                    });
                    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]); // this.loading=true;

                    options = {
                      maximumImagesCount: 1,
                      width: 200,
                      quality: 25,
                      outputType: 1
                    };
                    imageResponse = [];
                    this.imagePicker.getPictures(options).then(function (results) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this7, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
                        var _this8 = this;

                        var imageData, byteCharacters, byteNumbers, i, byteArray, blob1;
                        return regeneratorRuntime.wrap(function _callee8$(_context8) {
                          while (1) {
                            switch (_context8.prev = _context8.next) {
                              case 0:
                                imageData = results[0].toString(); //  var imageData1 = imageData.toString();

                                byteCharacters = atob(imageData.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
                                byteNumbers = new Array(byteCharacters.length);

                                for (i = 0; i < byteCharacters.length; i++) {
                                  byteNumbers[i] = byteCharacters.charCodeAt(i);
                                }

                                byteArray = new Uint8Array(byteNumbers);
                                blob1 = new Blob([byteArray], {
                                  type: "jpg"
                                });
                                console.log(blob1);
                                this.api.uploadgroup(blob1, "jpg", "test", this.token, "bp_cover_image_upload", this.groupdata.id, "cover").subscribe(function (res) {
                                  return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this8, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
                                    var toast;
                                    return regeneratorRuntime.wrap(function _callee7$(_context7) {
                                      while (1) {
                                        switch (_context7.prev = _context7.next) {
                                          case 0:
                                            this.groupdata.cover = res.image;
                                            _context7.next = 3;
                                            return this.toastCtrl.create({
                                              message: 'Image Uploaded',
                                              duration: 3000
                                            });

                                          case 3:
                                            toast = _context7.sent;
                                            toast.present(); // Get the entire data

                                          case 5:
                                          case "end":
                                            return _context7.stop();
                                        }
                                      }
                                    }, _callee7, this);
                                  }));
                                }, function (err) {
                                  console.log(err);

                                  _this8.showError(err);
                                }); //   });
                                //  });
                                //  } 

                              case 8:
                              case "end":
                                return _context8.stop();
                            }
                          }
                        }, _callee8, this);
                      }));
                    }, function (err) {
                      alert(err);
                    });

                  case 5:
                  case "end":
                    return _context9.stop();
                }
              }
            }, _callee9, this);
          }));
        }
      }, {
        key: "uploadCoverFile",
        value: function uploadCoverFile(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
            var _this9 = this;

            var loading, eventObj, target, file1;
            return regeneratorRuntime.wrap(function _callee11$(_context11) {
              while (1) {
                switch (_context11.prev = _context11.next) {
                  case 0:
                    _context11.next = 2;
                    return this.loadingController.create({
                      message: 'Please wait...',
                      duration: 2000
                    });

                  case 2:
                    loading = _context11.sent;
                    _context11.next = 5;
                    return loading.present();

                  case 5:
                    eventObj = event;
                    target = eventObj.target;
                    file1 = target.files[0];
                    this.api.uploadgroup2(file1, "bp_cover_image_upload", this.groupdata.id, "cover").subscribe(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this9, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
                        var toast;
                        return regeneratorRuntime.wrap(function _callee10$(_context10) {
                          while (1) {
                            switch (_context10.prev = _context10.next) {
                              case 0:
                                this.groupdata.cover = res.image;
                                _context10.next = 3;
                                return this.toastCtrl.create({
                                  message: 'Image Uploaded',
                                  duration: 3000
                                });

                              case 3:
                                toast = _context10.sent;
                                toast.present(); // Get the entire data

                              case 5:
                              case "end":
                                return _context10.stop();
                            }
                          }
                        }, _callee10, this);
                      }));
                    }, function (err) {
                      console.log(err);

                      _this9.showError(err);
                    });

                  case 9:
                  case "end":
                    return _context11.stop();
                }
              }
            }, _callee11, this);
          }));
        }
      }, {
        key: "selectImageSource",
        value: function selectImageSource() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
            var _this10 = this;

            var buttons, actionSheet;
            return regeneratorRuntime.wrap(function _callee12$(_context12) {
              while (1) {
                switch (_context12.prev = _context12.next) {
                  case 0:
                    buttons = [{
                      text: 'Choose From Photos Photo',
                      icon: 'image',
                      handler: function handler() {
                        _this10.androidPermissions.checkPermission(_this10.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                          return console.log('Has permission?', result.hasPermission);
                        }, function (err) {
                          return _this10.androidPermissions.requestPermission(_this10.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                        });

                        _this10.androidPermissions.requestPermissions([_this10.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, _this10.androidPermissions.PERMISSION.GET_ACCOUNTS]);

                        _this10.selectImage();
                      }
                    }]; // Only allow file selection inside a browser

                    if (!this.plt.is('hybrid')) {
                      buttons.push({
                        text: 'Choose a File',
                        icon: 'attach',
                        handler: function handler() {
                          _this10.androidPermissions.checkPermission(_this10.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                            return console.log('Has permission?', result.hasPermission);
                          }, function (err) {
                            return _this10.androidPermissions.requestPermission(_this10.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                          });

                          _this10.androidPermissions.requestPermissions([_this10.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, _this10.androidPermissions.PERMISSION.GET_ACCOUNTS]);

                          _this10.fileInput.nativeElement.click();
                        }
                      });
                    }

                    _context12.next = 4;
                    return this.actionSheetCtrl.create({
                      header: 'Select Image Source',
                      buttons: buttons
                    });

                  case 4:
                    actionSheet = _context12.sent;
                    _context12.next = 7;
                    return actionSheet.present();

                  case 7:
                  case "end":
                    return _context12.stop();
                }
              }
            }, _callee12, this);
          }));
        }
      }, {
        key: "selectcoverImageSource",
        value: function selectcoverImageSource() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
            var _this11 = this;

            var buttons, actionSheet;
            return regeneratorRuntime.wrap(function _callee13$(_context13) {
              while (1) {
                switch (_context13.prev = _context13.next) {
                  case 0:
                    buttons = [{
                      text: 'Choose From Photos Photo',
                      icon: 'image',
                      handler: function handler() {
                        _this11.androidPermissions.checkPermission(_this11.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                          return console.log('Has permission?', result.hasPermission);
                        }, function (err) {
                          return _this11.androidPermissions.requestPermission(_this11.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                        });

                        _this11.androidPermissions.requestPermissions([_this11.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, _this11.androidPermissions.PERMISSION.GET_ACCOUNTS]);

                        _this11.selectImage2();
                      }
                    }]; // Only allow file selection inside a browser

                    if (!this.plt.is('hybrid')) {
                      buttons.push({
                        text: 'Choose a File',
                        icon: 'attach',
                        handler: function handler() {
                          _this11.androidPermissions.checkPermission(_this11.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                            return console.log('Has permission?', result.hasPermission);
                          }, function (err) {
                            return _this11.androidPermissions.requestPermission(_this11.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                          });

                          _this11.androidPermissions.requestPermissions([_this11.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, _this11.androidPermissions.PERMISSION.GET_ACCOUNTS]);

                          _this11.fileInput2.nativeElement.click();
                        }
                      });
                    }

                    _context13.next = 4;
                    return this.actionSheetCtrl.create({
                      header: 'Select Image Source',
                      buttons: buttons
                    });

                  case 4:
                    actionSheet = _context13.sent;
                    _context13.next = 7;
                    return actionSheet.present();

                  case 7:
                  case "end":
                    return _context13.stop();
                }
              }
            }, _callee13, this);
          }));
        }
      }, {
        key: "goToprofile",
        value: function goToprofile() {
          this.router.navigate(['/member-detail']);
        }
      }, {
        key: "goToHome",
        value: function goToHome(res) {
          sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire({
            title: 'Success',
            text: 'Thank you for registrations',
            icon: 'success',
            backdrop: false
          });
          this.router.navigate(['/tabs/home-new']);
        } //   uploadCoverFile(event: EventTarget) {
        //     this.loading3=true;
        //     this.edit_icon2=false;
        //     const eventObj: MSInputMethodContext = event as MSInputMethodContext;
        //     const target: HTMLInputElement = eventObj.target as HTMLInputElement;
        //     const file: File = target.files[0];
        //     this.api.uploadImageFile2(file,this.data.id).subscribe(
        //       async (res : any) => {
        //         this.loading3=false;
        //         this.edit_icon2=true;
        //       console.log("---------------");
        //       console.log(res);
        //       const toast = await this.toastCtrl.create({
        //         message: 'Profile Cover Image Updated',
        //         duration: 3000
        //       });
        //       toast.present();
        // // Get the entire data
        // this.storage.get('USER_INFO').then(valueStr => {
        //   let value = valueStr ? valueStr : {};
        //    // Modify just that property
        //    value.cover_url = res.image;
        // this.data.cover_url=res.image;
        //    // Save the entire data again
        //    this.storage.set('USER_INFO', value);
        //     window.location.reload();
        // });
        //     },
        //     err => {
        //       this.loading3=false;
        //       this.edit_icon2=true;
        //       console.log(err);
        //       this.showError(err);
        //     }
        //     );
        //   }

      }, {
        key: "showError",
        value: function showError(err) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee14() {
            var alert;
            return regeneratorRuntime.wrap(function _callee14$(_context14) {
              while (1) {
                switch (_context14.prev = _context14.next) {
                  case 0:
                    _context14.next = 2;
                    return this.alertCtrl.create({
                      header: err.error.code,
                      subHeader: err.error.data,
                      message: err.error.message,
                      buttons: ['OK']
                    });

                  case 2:
                    alert = _context14.sent;
                    _context14.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context14.stop();
                }
              }
            }, _callee14, this);
          }));
        }
      }]);

      return GroupUpdatePage;
    }();

    GroupUpdatePage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"]
      }, {
        type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_12__["NativePageTransitions"]
      }, {
        type: _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_10__["ImagePicker"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"]
      }, {
        type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_11__["File"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"]
      }, {
        type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"]
      }, {
        type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_8__["AndroidPermissions"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ActionSheetController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], GroupUpdatePage.prototype, "fileInput", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput2', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], GroupUpdatePage.prototype, "fileInput2", void 0);
    GroupUpdatePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-group-update',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./group-update.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/group-update/group-update.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./group-update.page.scss */
      "./src/app/pages/group-update/group-update.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"], _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_12__["NativePageTransitions"], _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_10__["ImagePicker"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_11__["File"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_8__["AndroidPermissions"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ActionSheetController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"]])], GroupUpdatePage);
    /***/
  }
}]);
//# sourceMappingURL=pages-group-update-group-update-module-es5.js.map
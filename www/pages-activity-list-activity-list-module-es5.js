function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-activity-list-activity-list-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/activity-list/activity-list.page.html":
  /*!***************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/activity-list/activity-list.page.html ***!
    \***************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesActivityListActivityListPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n      <img src=\"assets/imgs/user.png\" class=\"user_image\">\n      <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar>\n      <ion-icon name=\"chatbubbles\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\"></ion-icon>\n    </div>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n\n      <!-- <ion-buttons slot=\"start\"> -->\n        <!-- <ion-menu-button color=\"light\"></ion-menu-button> -->\n        <!-- <img src=\"assets/imgs/user.png\" class=\"user_image\" (click)=\"openMenu()\">\n      </ion-buttons> -->\n      <ion-label style=\"color: white;\">Activities</ion-label>\n      <!-- <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar> -->\n      <!-- <ion-icon name=\"chatbubbles\" (click)=\"goToChatList()\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\"></ion-icon> -->\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n\n   \n     \n  \n\n    <div class=\"flex_div\" *ngFor=\"let item of activities\">\n      <div class=\"f_div\">\n        <div class=\"back_image\" [ngStyle]= \"{'background-image': 'url('+ item.media_url +')'}\" ></div>\n        <!-- <ion-grid><ion-row>\n          <ion-col size=\"\">\n\n          </ion-col>\n        </ion-row>\n      </ion-grid> -->\n        <div class=\"detail\">\n          <ion-label  style=\"font-size: 15px;\" class=\"name\">{{item.name}}</ion-label>\n          <ion-label style=\"font-size: 14px;\" class=\"desc\">{{item.title}}</ion-label>\n          <ion-label style=\"font-size: small; \" class=\"desc\" >{{item.content}}</ion-label>\n          <ion-label style=\"font-size: small; color: black;\" class=\"desc\" >{{item.date}} ago</ion-label>\n        </div>\n        <!-- <div class=\"time\">\n          \n         \n        </div> -->\n      </div>\n    \n    </div>\n    \n\n\n\n  </div>\n  <ion-infinite-scroll (ionInfinite)=\"loadData($event)\">\n    <ion-infinite-scroll-content\n    loadingSpinner=\"bubbles\"\n    loadingText=\"Loading more posts ...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/activity-list/activity-list-routing.module.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/pages/activity-list/activity-list-routing.module.ts ***!
    \*********************************************************************/

  /*! exports provided: ActivityListPageRoutingModule */

  /***/
  function srcAppPagesActivityListActivityListRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ActivityListPageRoutingModule", function () {
      return ActivityListPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _activity_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./activity-list.page */
    "./src/app/pages/activity-list/activity-list.page.ts");

    var routes = [{
      path: '',
      component: _activity_list_page__WEBPACK_IMPORTED_MODULE_3__["ActivityListPage"]
    }];

    var ActivityListPageRoutingModule = function ActivityListPageRoutingModule() {
      _classCallCheck(this, ActivityListPageRoutingModule);
    };

    ActivityListPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ActivityListPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/activity-list/activity-list.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/pages/activity-list/activity-list.module.ts ***!
    \*************************************************************/

  /*! exports provided: ActivityListPageModule */

  /***/
  function srcAppPagesActivityListActivityListModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ActivityListPageModule", function () {
      return ActivityListPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _activity_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./activity-list-routing.module */
    "./src/app/pages/activity-list/activity-list-routing.module.ts");
    /* harmony import */


    var _activity_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./activity-list.page */
    "./src/app/pages/activity-list/activity-list.page.ts");

    var ActivityListPageModule = function ActivityListPageModule() {
      _classCallCheck(this, ActivityListPageModule);
    };

    ActivityListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _activity_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["ActivityListPageRoutingModule"]],
      declarations: [_activity_list_page__WEBPACK_IMPORTED_MODULE_6__["ActivityListPage"]]
    })], ActivityListPageModule);
    /***/
  },

  /***/
  "./src/app/pages/activity-list/activity-list.page.scss":
  /*!*************************************************************!*\
    !*** ./src/app/pages/activity-list/activity-list.page.scss ***!
    \*************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesActivityListActivityListPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\nion-card-header {\n  padding: 5px;\n}\n\nion-card-title {\n  font-size: 17px;\n}\n\nion-card-content {\n  font-size: 14px;\n}\n\n.count {\n  font-size: 13px;\n  padding: 5px;\n  color: var(--ion-color-main);\n}\n\nion-scroll[scrollX] {\n  white-space: nowrap;\n  height: 120px;\n  overflow: hidden;\n}\n\nion-scroll[scrollX] .scroll-item {\n  display: inline-block;\n}\n\nion-scroll[scrollX] .selectable-icon {\n  margin: 10px 20px 10px 20px;\n  color: red;\n  font-size: 100px;\n}\n\nion-scroll[scrollX] ion-avatar img {\n  margin: 10px;\n}\n\n.up_lbl {\n  padding-left: 16px;\n  padding-right: 16px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n  background: #e1e9ee;\n}\n\n.chips_div {\n  display: flex;\n  flex-direction: row;\n  overflow: scroll;\n  padding-left: 16px;\n  padding-right: 16px;\n  background: #e1e9ee;\n  padding-top: 10px;\n  padding-bottom: 20px;\n}\n\n.chips_div .chip {\n  background-color: white;\n  border-radius: 3px;\n  padding: 5px 20px;\n  margin-right: 10px;\n  white-space: nowrap;\n  color: var(--ion-color-main);\n  font-size: 12px;\n  font-weight: bold;\n}\n\n.slider_div {\n  background: #e1e9ee;\n  padding-top: 10px;\n}\n\n.slider_div ion-slides {\n  height: 200px;\n  margin-top: -35px;\n}\n\n.slider_div .slide_div {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  border: 1px solid lightgray;\n  background: white;\n  justify-content: center;\n}\n\n.slider_div .slide_div .up_div {\n  display: flex;\n  flex-direction: row;\n  padding: 2px;\n  width: 395px;\n}\n\n.slider_div .slide_div .img_div {\n  width: 40px;\n}\n\n.slider_div .slide_div .img_div .slider_img {\n  width: 30px;\n  height: 30px;\n  min-width: 30px;\n}\n\n.slider_div .slide_div .content_div {\n  margin-left: 10px;\n}\n\n.slider_div .slide_div .content_div .abs_lbl {\n  position: absolute;\n  left: 5px;\n  top: 0;\n  font-size: 12px;\n}\n\n.slider_div .slide_div .content_div .head_lbl {\n  display: block;\n  font-weight: 500;\n  text-align: left;\n  font-size: 14px;\n}\n\n.slider_div .slide_div .content_div .small_lbl {\n  color: #707070;\n  text-align: left;\n  display: block;\n  font-size: 14px;\n}\n\n.slider_div .slide_div .lower_div {\n  border-top: 1px solid lightgray;\n  padding: 3px;\n  display: flex;\n  width: 100%;\n  justify-content: space-around;\n}\n\n.slider_div .slide_div .lower_div ion-button {\n  color: var(--ion-color-main);\n  font-weight: 600;\n}\n\nion-scroll[scroll-avatar] {\n  height: 60px;\n}\n\n.notification_div {\n  padding: 15px 10px;\n  position: relative;\n  background: white;\n}\n\n.notification_div .time_div {\n  position: absolute;\n  right: 10px;\n  top: 5px;\n  color: gray;\n}\n\n.notification_div .flex_div {\n  display: flex;\n  width: 100%;\n  align-items: center;\n}\n\n.notification_div .flex_div .back_image {\n  height: 50px;\n  width: 50px;\n  min-width: 50px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.notification_div .flex_div .header_lbl {\n  font-weight: 600;\n  margin-left: 10px;\n  font-size: 14px;\n  width: 88%;\n}\n\n.notification_div .flex_div .desc {\n  margin-left: 10px;\n  font-size: 14px;\n  width: 88%;\n}\n\n.notification_div .btn_div {\n  padding-left: 60px;\n  margin-top: 5px;\n}\n\n.notification_div .btn_div ion-button {\n  font-weight: 600;\n  --border-radius: 3px;\n  --border-color: var(--ion-color-main);\n  --color: var(--ion-color-main);\n  font-size: 16px;\n}\n\n/* Hide ion-content scrollbar */\n\n::-webkit-scrollbar {\n  display: none;\n}\n\n.bck-orange {\n  background: #ff9800;\n}\n\n.bck-red {\n  background: #f4511e;\n}\n\n.bck-green {\n  background: #afb42b;\n}\n\n.bck-yellow {\n  background: #ffc400;\n}\n\n.bck-blue {\n  background: #64b5f6;\n}\n\n.bck-pink {\n  background: #f48fb1;\n}\n\n.md-layout .mbsc-scv-item {\n  color: #fff;\n  font-weight: bold;\n  text-align: center;\n}\n\n.md-layout .mbsc-scv-c {\n  margin: 10px 0;\n}\n\n.variable-1 {\n  width: 60px;\n  height: 60px;\n}\n\n.variable-2 {\n  width: 80px;\n  height: 80px;\n}\n\n.variable-3 {\n  width: 70px;\n  height: 70px;\n}\n\n.variable-4 {\n  width: 50px;\n  height: 50px;\n}\n\n.variable-5 {\n  width: 100px;\n  height: 100px;\n}\n\n.variable-6 {\n  width: 40px;\n  height: 40px;\n}\n\n.md-fixed .mbsc-scv-item {\n  height: 80px;\n  margin: 0 10px;\n}\n\n.md-variable .mbsc-scv-item {\n  margin: auto 10px;\n}\n\n.md-pages .mbsc-scv-item {\n  height: 80px;\n}\n\n.md-fullpage .mbsc-scv-item {\n  height: 630px;\n  font-size: 28px;\n}\n\n.header_div {\n  display: flex;\n  width: 100%;\n  justify-content: space-between;\n  align-items: center;\n  padding-left: 16px;\n  padding-right: 16px;\n  height: 40px;\n}\n\n.header_div .user_image {\n  height: 25px;\n  width: 25px;\n}\n\n.header_div ion-searchbar {\n  --background: white;\n  --placeholder-color: #707070;\n  --icon-color: #707070;\n  border-radius: 0px;\n  height: 30px;\n}\n\nion-content {\n  --background: #F5F5F5;\n}\n\n.blue_lbl {\n  color: var(--ion-color-main);\n  padding-top: 15px;\n  padding-bottom: 15px;\n  font-weight: 600;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div .head_lbl {\n  padding-top: 10px;\n  padding-bottom: 10px;\n}\n\n.main_content_div .white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n\n.main_content_div .white_div ion-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  right: 16px;\n  font-size: 20px;\n}\n\n.main_content_div .white_div ion-button {\n  color: var(--ion-color-main);\n  font-size: 600;\n}\n\n.main_content_div .flex_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  justify-content: space-between;\n  padding-top: 10px;\n  padding-bottom: 15px;\n  border-bottom: 1px solid lightgray;\n  background: white;\n}\n\n.main_content_div .flex_div .f_div {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding-left: 16px;\n}\n\n.main_content_div .flex_div .f_div .detail {\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  margin-left: 10px;\n  line-height: 20px;\n}\n\n.main_content_div .flex_div .f_div .detail .desc {\n  color: gray;\n}\n\n.main_content_div .flex_div .f_div .back_image {\n  height: 60px;\n  width: 60px;\n  min-width: 60px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.main_content_div .flex_div .f_div .time {\n  margin-left: 2px;\n}\n\n.main_content_div .flex_div .s_div {\n  display: flex;\n  align-items: center;\n  padding-right: 16px;\n}\n\n.main_content_div .flex_div .s_div ion-icon {\n  font-size: 40px;\n}\n\n.main_content_div .flex_div .s_div .cross {\n  color: gray;\n}\n\n.main_content_div .flex_div .s_div .check {\n  color: var(--ion-color-main);\n}\n\n.main_content_div .user_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n\n.main_content_div .user_div .col_div {\n  border: 1px solid lightgray;\n}\n\n.main_content_div .user_div ion-grid {\n  padding: 0;\n}\n\n.main_content_div .user_div .cover {\n  height: 50px;\n  width: 100%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  position: relative;\n}\n\n.main_content_div .user_div .cover .close {\n  font-size: 25px;\n  color: black;\n  position: absolute;\n  right: 5px;\n  top: 5px;\n}\n\n.main_content_div .user_div .back_image {\n  height: 75px;\n  width: 75px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  position: absolute;\n  left: 50%;\n  transform: translate(-50%);\n  top: 40%;\n  border: 3px solid white;\n}\n\n.main_content_div .user_div .username {\n  margin-top: 45px;\n  text-align: center;\n  text-align: center;\n}\n\n.main_content_div .user_div .detail {\n  margin-top: 5px;\n  color: gray;\n  text-align: center;\n  font-size: 14px;\n}\n\n.main_content_div .user_div .mutual_div {\n  display: flex;\n  justify-content: center;\n  margin-top: 8px;\n}\n\n.main_content_div .user_div .mutual_div .link {\n  width: 15px;\n  height: 15px;\n}\n\n.main_content_div .user_div .mutual_div .link_lbl {\n  font-size: 12px;\n  color: gray;\n  margin-left: 5px;\n}\n\n.main_content_div .user_div ion-button {\n  --border-radius: 3px;\n  color: var(--ion-color-main);\n  --border-color: var(--ion-color-main);\n  font-weight: 600px;\n  margin: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWN0aXZpdHktbGlzdC9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxhY3Rpdml0eS1saXN0XFxhY3Rpdml0eS1saXN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvYWN0aXZpdHktbGlzdC9hY3Rpdml0eS1saXN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1DQUFBO0FDQ0o7O0FEQ0E7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0FDRUo7O0FEQUE7RUFDSSxZQUFBO0FDR0o7O0FEREE7RUFDSSxlQUFBO0FDSUo7O0FERkE7RUFDSSxlQUFBO0FDS0o7O0FESEE7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLDRCQUFBO0FDTUo7O0FESkE7RUFDSSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQ09KOztBRExJO0VBQ0UscUJBQUE7QUNPTjs7QURKSTtFQUNFLDJCQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0FDTU47O0FESEk7RUFDRSxZQUFBO0FDS047O0FERkU7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDS0o7O0FESEE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7QUNNSjs7QURISTtFQUNJLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSw0QkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0tSOztBRERBO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtBQ0lKOztBREZJO0VBQ0ksYUFBQTtFQUNBLGlCQUFBO0FDSVI7O0FEREk7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFFQSxtQkFBQTtFQUNBLDJCQUFBO0VBRUEsaUJBQUE7RUFFQSx1QkFBQTtBQ0FSOztBREVRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUNBWjs7QURHUTtFQUNJLFdBQUE7QUNEWjs7QURHWTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQ0RoQjs7QURJUTtFQUNJLGlCQUFBO0FDRlo7O0FES1k7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxNQUFBO0VBQ0EsZUFBQTtBQ0hoQjs7QURLWTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQ0hoQjs7QURLWTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDSGhCOztBRE1RO0VBQ0ksK0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSw2QkFBQTtBQ0paOztBRE1ZO0VBQ0ksNEJBQUE7RUFDQSxnQkFBQTtBQ0poQjs7QURTRTtFQUNFLFlBQUE7QUNOSjs7QURRRTtFQUNFLGtCQUFBO0VBRUEsa0JBQUE7RUFDQSxpQkFBQTtBQ05KOztBRFFJO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7QUNOUjs7QURRSTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7QUNOUjs7QURPUTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDTFo7O0FEUVE7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7QUNOWjs7QURRUTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7QUNOWjs7QURVSTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtBQ1JSOztBRFVRO0VBQ0ksZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLHFDQUFBO0VBQ0EsOEJBQUE7RUFDQSxlQUFBO0FDUlo7O0FEYUUsK0JBQUE7O0FBQ0E7RUFDRSxhQUFBO0FDVko7O0FEWUU7RUFDRSxtQkFBQTtBQ1RKOztBRFlBO0VBQ0ksbUJBQUE7QUNUSjs7QURZQTtFQUNJLG1CQUFBO0FDVEo7O0FEWUE7RUFDSSxtQkFBQTtBQ1RKOztBRFlBO0VBQ0ksbUJBQUE7QUNUSjs7QURZQTtFQUNJLG1CQUFBO0FDVEo7O0FEWUE7RUFDSSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ1RKOztBRFlBO0VBQ0ksY0FBQTtBQ1RKOztBRFlBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNUSjs7QURZQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDVEo7O0FEWUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ1RKOztBRFlBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNUSjs7QURZQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0FDVEo7O0FEWUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ1RKOztBRFlBO0VBQ0ksWUFBQTtFQUNBLGNBQUE7QUNUSjs7QURZQTtFQUNJLGlCQUFBO0FDVEo7O0FEWUE7RUFDSSxZQUFBO0FDVEo7O0FEWUE7RUFDSSxhQUFBO0VBQ0EsZUFBQTtBQ1RKOztBRFdBO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNSSjs7QURVSTtFQUNJLFlBQUE7RUFDQSxXQUFBO0FDUlI7O0FEVUk7RUFDSSxtQkFBQTtFQUNBLDRCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNSUjs7QURZQTtFQUNJLHFCQUFBO0FDVEo7O0FEV0E7RUFDSSw0QkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtBQ1JKOztBRFVBO0VBQ0ksV0FBQTtBQ1BKOztBRFNJO0VBQ0ksY0FBQTtBQ1BSOztBRFVJO0VBQ0ksaUJBQUE7RUFDQSxvQkFBQTtBQ1JSOztBRFdJO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNUUjs7QUR3QlE7RUFDSSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUN0Qlo7O0FEeUJRO0VBQ0ksNEJBQUE7RUFDQSxjQUFBO0FDdkJaOztBRDJCSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSw4QkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQ0FBQTtFQUNBLGlCQUFBO0FDekJSOztBRDRCUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUMxQlo7O0FENEJZO0VBQ0ksMEJBQUE7RUFBQSx1QkFBQTtFQUFBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQzFCaEI7O0FEMkJnQjtFQUNJLFdBQUE7QUN6QnBCOztBRDRCWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDMUJoQjs7QUQ0Qlk7RUFDSSxnQkFBQTtBQzFCaEI7O0FEK0JRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUM3Qlo7O0FEOEJZO0VBQ0ksZUFBQTtBQzVCaEI7O0FEK0JZO0VBQ0ksV0FBQTtBQzdCaEI7O0FEK0JZO0VBQ0ksNEJBQUE7QUM3QmhCOztBRGtDSTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDaENSOztBRGtDUTtFQUNJLDJCQUFBO0FDaENaOztBRG1DUTtFQUNJLFVBQUE7QUNqQ1o7O0FEb0NRO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtBQ2xDWjs7QURvQ1k7RUFDSSxlQUFBO0VBRUEsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7QUNuQ2hCOztBRHNDUTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSwwQkFBQTtFQUNBLFFBQUE7RUFDQSx1QkFBQTtBQ3BDWjs7QUR1Q1E7RUFDSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNyQ1o7O0FEdUNRO0VBQ0ksZUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUNyQ1o7O0FEd0NRO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQ3RDWjs7QUR1Q1k7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ3JDaEI7O0FEdUNZO0VBQ0ksZUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ3JDaEI7O0FEeUNRO0VBQ0ksb0JBQUE7RUFDQSw0QkFBQTtFQUNBLHFDQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDdkNaIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYWN0aXZpdHktbGlzdC9hY3Rpdml0eS1saXN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZCA6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cbmlvbi10b29sYmFyLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgsIGlvbi10b29sYmFyIC5zYy1pb24tc2VhcmNoYmFyLWlvcy1oe1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcbn1cbmlvbi1jYXJkLWhlYWRlcntcbiAgICBwYWRkaW5nOiA1cHg7XG59XG5pb24tY2FyZC10aXRsZXtcbiAgICBmb250LXNpemU6IDE3cHg7XG59XG5pb24tY2FyZC1jb250ZW50e1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5jb3VudHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5pb24tc2Nyb2xsW3Njcm9sbFhdIHtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIGhlaWdodDogMTIwcHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcblxuICAgIC5zY3JvbGwtaXRlbSB7XG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgfVxuXG4gICAgLnNlbGVjdGFibGUtaWNvbntcbiAgICAgIG1hcmdpbjogMTBweCAyMHB4IDEwcHggMjBweDtcbiAgICAgIGNvbG9yOiByZWQ7XG4gICAgICBmb250LXNpemU6IDEwMHB4O1xuICAgIH1cblxuICAgIGlvbi1hdmF0YXIgaW1ne1xuICAgICAgbWFyZ2luOiAxMHB4O1xuICAgIH1cbiAgfVxuICAudXBfbGJse1xuICAgIHBhZGRpbmctbGVmdDogMTZweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kOiAjZTFlOWVlO1xufVxuLmNoaXBzX2RpdntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgb3ZlcmZsb3c6IHNjcm9sbDtcbiAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICBiYWNrZ3JvdW5kOiAjZTFlOWVlO1xuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xuXG5cbiAgICAuY2hpcHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICAgICAgcGFkZGluZzogNXB4IDIwcHg7XG4gICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICB9XG59XG5cbi5zbGlkZXJfZGl2e1xuICAgIGJhY2tncm91bmQ6ICNlMWU5ZWU7IFxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuXG4gICAgaW9uLXNsaWRlc3tcbiAgICAgICAgaGVpZ2h0OiAyMDBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogLTM1cHg7XG4gICAgfVxuXG4gICAgLnNsaWRlX2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICBcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICAvLyBwYWRkaW5nOiAyMHB4IDE1cHg7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgICAgIC51cF9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgICAgIHBhZGRpbmc6IDJweDtcbiAgICAgICAgICAgIHdpZHRoOiAzOTVweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5pbWdfZGl2e1xuICAgICAgICAgICAgd2lkdGg6IDQwcHg7XG4gICAgICAgICAgICAvLyBwYWRkaW5nOiAxNXB4O1xuICAgICAgICAgICAgLnNsaWRlcl9pbWd7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICAgICAgICAgIG1pbi13aWR0aDogMzBweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuY29udGVudF9kaXZ7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgICAgIC8vIHBhZGRpbmc6IDE1cHg7XG5cbiAgICAgICAgICAgIC5hYnNfbGJse1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICBsZWZ0OiA1cHg7XG4gICAgICAgICAgICAgICAgdG9wOiAwO1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5oZWFkX2xibHtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnNtYWxsX2xibHtcbiAgICAgICAgICAgICAgICBjb2xvcjogIzcwNzA3MDtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAubG93ZXJfZGl2e1xuICAgICAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgICAgIHBhZGRpbmc6IDNweDtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuXG4gICAgICAgICAgICBpb24tYnV0dG9ue1xuICAgICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cbiAgaW9uLXNjcm9sbFtzY3JvbGwtYXZhdGFyXXtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gIH1cbiAgLm5vdGlmaWNhdGlvbl9kaXZ7XG4gICAgcGFkZGluZzogMTVweCAxMHB4O1xuICAgXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBcbiAgICAudGltZV9kaXZ7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgcmlnaHQ6IDEwcHg7XG4gICAgICAgIHRvcDogNXB4O1xuICAgICAgICBjb2xvcjogZ3JheTtcbiAgICB9XG4gICAgLmZsZXhfZGl2e1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgLmJhY2tfaW1hZ2V7XG4gICAgICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgICAgICB3aWR0aDogNTBweDtcbiAgICAgICAgICAgIG1pbi13aWR0aDogNTBweDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICB9XG5cbiAgICAgICAgLmhlYWRlcl9sYmx7XG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICB3aWR0aDogODglO1xuICAgICAgICB9XG4gICAgICAgIC5kZXNje1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICB3aWR0aDogODglO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmJ0bl9kaXZ7XG4gICAgICAgIHBhZGRpbmctbGVmdDogNjBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuXG4gICAgICAgIGlvbi1idXR0b257XG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICB9XG4gICAgfVxuICAgIFxufVxuICAvKiBIaWRlIGlvbi1jb250ZW50IHNjcm9sbGJhciAqL1xuICA6Oi13ZWJraXQtc2Nyb2xsYmFye1xuICAgIGRpc3BsYXk6bm9uZTtcbiAgfVxuICAuYmNrLW9yYW5nZSB7XG4gICAgYmFja2dyb3VuZDogI2ZmOTgwMDtcbn1cblxuLmJjay1yZWQge1xuICAgIGJhY2tncm91bmQ6ICNmNDUxMWU7XG59XG5cbi5iY2stZ3JlZW4ge1xuICAgIGJhY2tncm91bmQ6ICNhZmI0MmI7XG59XG5cbi5iY2steWVsbG93IHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZjNDAwO1xufVxuXG4uYmNrLWJsdWUge1xuICAgIGJhY2tncm91bmQ6ICM2NGI1ZjY7XG59XG5cbi5iY2stcGluayB7XG4gICAgYmFja2dyb3VuZDogI2Y0OGZiMTtcbn1cblxuLm1kLWxheW91dCAubWJzYy1zY3YtaXRlbSB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubWQtbGF5b3V0IC5tYnNjLXNjdi1jIHtcbiAgICBtYXJnaW46IDEwcHggMDtcbn1cblxuLnZhcmlhYmxlLTEge1xuICAgIHdpZHRoOiA2MHB4O1xuICAgIGhlaWdodDogNjBweDtcbn1cblxuLnZhcmlhYmxlLTIge1xuICAgIHdpZHRoOiA4MHB4O1xuICAgIGhlaWdodDogODBweDtcbn1cblxuLnZhcmlhYmxlLTMge1xuICAgIHdpZHRoOiA3MHB4O1xuICAgIGhlaWdodDogNzBweDtcbn1cblxuLnZhcmlhYmxlLTQge1xuICAgIHdpZHRoOiA1MHB4O1xuICAgIGhlaWdodDogNTBweDtcbn1cblxuLnZhcmlhYmxlLTUge1xuICAgIHdpZHRoOiAxMDBweDtcbiAgICBoZWlnaHQ6IDEwMHB4O1xufVxuXG4udmFyaWFibGUtNiB7XG4gICAgd2lkdGg6IDQwcHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xufVxuXG4ubWQtZml4ZWQgLm1ic2Mtc2N2LWl0ZW0ge1xuICAgIGhlaWdodDogODBweDtcbiAgICBtYXJnaW46IDAgMTBweDtcbn1cblxuLm1kLXZhcmlhYmxlIC5tYnNjLXNjdi1pdGVtIHtcbiAgICBtYXJnaW46IGF1dG8gMTBweDtcbn1cblxuLm1kLXBhZ2VzIC5tYnNjLXNjdi1pdGVtIHtcbiAgICBoZWlnaHQ6IDgwcHg7XG59XG5cbi5tZC1mdWxscGFnZSAubWJzYy1zY3YtaXRlbSB7XG4gICAgaGVpZ2h0OiA2MzBweDtcbiAgICBmb250LXNpemU6IDI4cHg7XG59XG4uaGVhZGVyX2RpdntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyOyAgICBcbiAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG5cbiAgICAudXNlcl9pbWFnZXtcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgICAgICB3aWR0aDogMjVweDtcbiAgICB9XG4gICAgaW9uLXNlYXJjaGJhcntcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTsgICBcbiAgICAgICAgLS1wbGFjZWhvbGRlci1jb2xvcjogICM3MDcwNzA7IFxuICAgICAgICAtLWljb24tY29sb3IgOiAjNzA3MDcwO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgICAgIGhlaWdodDogMzBweDtcbiAgICB9XG59XG5cbmlvbi1jb250ZW50e1xuICAgIC0tYmFja2dyb3VuZDogI0Y1RjVGNTtcbn1cbi5ibHVlX2xibHtcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIHBhZGRpbmctdG9wOiAxNXB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdntcbiAgICB3aWR0aDogMTAwJTtcblxuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIH1cblxuICAgIC5oZWFkX2xibHtcbiAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICAgIH1cblxuICAgIC53aGl0ZV9kaXZ7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIC8vIGlvbi1idXR0b257XG4gICAgICAgIC8vICAgICAtLWJvcmRlci1yYWRpdXM6IDBweDtcbiAgICAgICAgLy8gICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIC8vICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgLy8gICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgLy8gICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIC8vICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgLy8gICAgIH0gICBcbiAgICAgICAgLy8gICAgIGlvbi1pY29ue1xuICAgICAgICAvLyAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIC8vICAgICB9XG4gICAgICAgIC8vIH1cblxuICAgICAgICBcbiAgICAgICAgaW9uLWljb257XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgcmlnaHQ6IDE2cHg7XG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIH1cblxuICAgICAgICBpb24tYnV0dG9ue1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNjAwO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmZsZXhfZGl2e1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuXG5cbiAgICAgICAgLmZfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuXG4gICAgICAgICAgICAuZGV0YWlse1xuICAgICAgICAgICAgICAgIHdpZHRoOiBtYXgtY29udGVudDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgICAgICAgICAgICAuZGVzY3tcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6Z3JheTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuYmFja19pbWFnZXtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDYwcHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDYwcHg7XG4gICAgICAgICAgICAgICAgbWluLXdpZHRoOiA2MHB4O1xuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnRpbWV7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5zX2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICAgICAgICAgIGlvbi1pY29ue1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogNDBweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmNyb3Nze1xuICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmNoZWNre1xuICAgICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAudXNlcl9kaXZ7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG5cbiAgICAgICAgLmNvbF9kaXZ7XG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgIH1cblxuICAgICAgICBpb24tZ3JpZHtcbiAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIC5jb3ZlcntcbiAgICAgICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgICAgICAgICAgIC5jbG9zZXtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDI1cHg7XG4gICAgICAgICAgICAgICAgLy8gY29sb3I6ICM1MDUwNTA7XG4gICAgICAgICAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICByaWdodDogNXB4O1xuICAgICAgICAgICAgICAgIHRvcDogNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5iYWNrX2ltYWdle1xuICAgICAgICAgICAgaGVpZ2h0OiA3NXB4O1xuICAgICAgICAgICAgd2lkdGg6IDc1cHg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIGxlZnQ6IDUwJTtcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUpO1xuICAgICAgICAgICAgdG9wOiA0MCU7XG4gICAgICAgICAgICBib3JkZXI6IDNweCBzb2xpZCB3aGl0ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIC51c2VybmFtZXtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDQ1cHg7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgICAgICAgLmRldGFpbHtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICB9XG5cbiAgICAgICAgLm11dHVhbF9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA4cHg7XG4gICAgICAgICAgICAubGlua3tcbiAgICAgICAgICAgICAgICB3aWR0aDogMTVweDtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDE1cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAubGlua19sYmx7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpb24tYnV0dG9ue1xuICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDBweDtcbiAgICAgICAgICAgIG1hcmdpbjogMTBweDtcbiAgICAgICAgfVxuICAgIH1cbn0iLCJpb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuXG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCB7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5cbmlvbi1jYXJkLWhlYWRlciB7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuaW9uLWNhcmQtdGl0bGUge1xuICBmb250LXNpemU6IDE3cHg7XG59XG5cbmlvbi1jYXJkLWNvbnRlbnQge1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5jb3VudCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuXG5pb24tc2Nyb2xsW3Njcm9sbFhdIHtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgaGVpZ2h0OiAxMjBweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbmlvbi1zY3JvbGxbc2Nyb2xsWF0gLnNjcm9sbC1pdGVtIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuaW9uLXNjcm9sbFtzY3JvbGxYXSAuc2VsZWN0YWJsZS1pY29uIHtcbiAgbWFyZ2luOiAxMHB4IDIwcHggMTBweCAyMHB4O1xuICBjb2xvcjogcmVkO1xuICBmb250LXNpemU6IDEwMHB4O1xufVxuaW9uLXNjcm9sbFtzY3JvbGxYXSBpb24tYXZhdGFyIGltZyB7XG4gIG1hcmdpbjogMTBweDtcbn1cblxuLnVwX2xibCB7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBiYWNrZ3JvdW5kOiAjZTFlOWVlO1xufVxuXG4uY2hpcHNfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgb3ZlcmZsb3c6IHNjcm9sbDtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICBiYWNrZ3JvdW5kOiAjZTFlOWVlO1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG59XG4uY2hpcHNfZGl2IC5jaGlwIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgcGFkZGluZzogNXB4IDIwcHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLnNsaWRlcl9kaXYge1xuICBiYWNrZ3JvdW5kOiAjZTFlOWVlO1xuICBwYWRkaW5nLXRvcDogMTBweDtcbn1cbi5zbGlkZXJfZGl2IGlvbi1zbGlkZXMge1xuICBoZWlnaHQ6IDIwMHB4O1xuICBtYXJnaW4tdG9wOiAtMzVweDtcbn1cbi5zbGlkZXJfZGl2IC5zbGlkZV9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLnVwX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIHBhZGRpbmc6IDJweDtcbiAgd2lkdGg6IDM5NXB4O1xufVxuLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAuaW1nX2RpdiB7XG4gIHdpZHRoOiA0MHB4O1xufVxuLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAuaW1nX2RpdiAuc2xpZGVyX2ltZyB7XG4gIHdpZHRoOiAzMHB4O1xuICBoZWlnaHQ6IDMwcHg7XG4gIG1pbi13aWR0aDogMzBweDtcbn1cbi5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmNvbnRlbnRfZGl2IHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG4uc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5jb250ZW50X2RpdiAuYWJzX2xibCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNXB4O1xuICB0b3A6IDA7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbi5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmNvbnRlbnRfZGl2IC5oZWFkX2xibCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXNpemU6IDE0cHg7XG59XG4uc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5jb250ZW50X2RpdiAuc21hbGxfbGJsIHtcbiAgY29sb3I6ICM3MDcwNzA7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IDE0cHg7XG59XG4uc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5sb3dlcl9kaXYge1xuICBib3JkZXItdG9wOiAxcHggc29saWQgbGlnaHRncmF5O1xuICBwYWRkaW5nOiAzcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbn1cbi5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmxvd2VyX2RpdiBpb24tYnV0dG9uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuaW9uLXNjcm9sbFtzY3JvbGwtYXZhdGFyXSB7XG4gIGhlaWdodDogNjBweDtcbn1cblxuLm5vdGlmaWNhdGlvbl9kaXYge1xuICBwYWRkaW5nOiAxNXB4IDEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG4ubm90aWZpY2F0aW9uX2RpdiAudGltZV9kaXYge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxMHB4O1xuICB0b3A6IDVweDtcbiAgY29sb3I6IGdyYXk7XG59XG4ubm90aWZpY2F0aW9uX2RpdiAuZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5ub3RpZmljYXRpb25fZGl2IC5mbGV4X2RpdiAuYmFja19pbWFnZSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDUwcHg7XG4gIG1pbi13aWR0aDogNTBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4ubm90aWZpY2F0aW9uX2RpdiAuZmxleF9kaXYgLmhlYWRlcl9sYmwge1xuICBmb250LXdlaWdodDogNjAwO1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICB3aWR0aDogODglO1xufVxuLm5vdGlmaWNhdGlvbl9kaXYgLmZsZXhfZGl2IC5kZXNjIHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgd2lkdGg6IDg4JTtcbn1cbi5ub3RpZmljYXRpb25fZGl2IC5idG5fZGl2IHtcbiAgcGFkZGluZy1sZWZ0OiA2MHB4O1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG4ubm90aWZpY2F0aW9uX2RpdiAuYnRuX2RpdiBpb24tYnV0dG9uIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gIC0tYm9yZGVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuXG4vKiBIaWRlIGlvbi1jb250ZW50IHNjcm9sbGJhciAqL1xuOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5iY2stb3JhbmdlIHtcbiAgYmFja2dyb3VuZDogI2ZmOTgwMDtcbn1cblxuLmJjay1yZWQge1xuICBiYWNrZ3JvdW5kOiAjZjQ1MTFlO1xufVxuXG4uYmNrLWdyZWVuIHtcbiAgYmFja2dyb3VuZDogI2FmYjQyYjtcbn1cblxuLmJjay15ZWxsb3cge1xuICBiYWNrZ3JvdW5kOiAjZmZjNDAwO1xufVxuXG4uYmNrLWJsdWUge1xuICBiYWNrZ3JvdW5kOiAjNjRiNWY2O1xufVxuXG4uYmNrLXBpbmsge1xuICBiYWNrZ3JvdW5kOiAjZjQ4ZmIxO1xufVxuXG4ubWQtbGF5b3V0IC5tYnNjLXNjdi1pdGVtIHtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5tZC1sYXlvdXQgLm1ic2Mtc2N2LWMge1xuICBtYXJnaW46IDEwcHggMDtcbn1cblxuLnZhcmlhYmxlLTEge1xuICB3aWR0aDogNjBweDtcbiAgaGVpZ2h0OiA2MHB4O1xufVxuXG4udmFyaWFibGUtMiB7XG4gIHdpZHRoOiA4MHB4O1xuICBoZWlnaHQ6IDgwcHg7XG59XG5cbi52YXJpYWJsZS0zIHtcbiAgd2lkdGg6IDcwcHg7XG4gIGhlaWdodDogNzBweDtcbn1cblxuLnZhcmlhYmxlLTQge1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xufVxuXG4udmFyaWFibGUtNSB7XG4gIHdpZHRoOiAxMDBweDtcbiAgaGVpZ2h0OiAxMDBweDtcbn1cblxuLnZhcmlhYmxlLTYge1xuICB3aWR0aDogNDBweDtcbiAgaGVpZ2h0OiA0MHB4O1xufVxuXG4ubWQtZml4ZWQgLm1ic2Mtc2N2LWl0ZW0ge1xuICBoZWlnaHQ6IDgwcHg7XG4gIG1hcmdpbjogMCAxMHB4O1xufVxuXG4ubWQtdmFyaWFibGUgLm1ic2Mtc2N2LWl0ZW0ge1xuICBtYXJnaW46IGF1dG8gMTBweDtcbn1cblxuLm1kLXBhZ2VzIC5tYnNjLXNjdi1pdGVtIHtcbiAgaGVpZ2h0OiA4MHB4O1xufVxuXG4ubWQtZnVsbHBhZ2UgLm1ic2Mtc2N2LWl0ZW0ge1xuICBoZWlnaHQ6IDYzMHB4O1xuICBmb250LXNpemU6IDI4cHg7XG59XG5cbi5oZWFkZXJfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICBoZWlnaHQ6IDQwcHg7XG59XG4uaGVhZGVyX2RpdiAudXNlcl9pbWFnZSB7XG4gIGhlaWdodDogMjVweDtcbiAgd2lkdGg6IDI1cHg7XG59XG4uaGVhZGVyX2RpdiBpb24tc2VhcmNoYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgLS1wbGFjZWhvbGRlci1jb2xvcjogIzcwNzA3MDtcbiAgLS1pY29uLWNvbG9yOiAjNzA3MDcwO1xuICBib3JkZXItcmFkaXVzOiAwcHg7XG4gIGhlaWdodDogMzBweDtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6ICNGNUY1RjU7XG59XG5cbi5ibHVlX2xibCB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIHBhZGRpbmctdG9wOiAxNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IGlvbi1sYWJlbCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuLm1haW5fY29udGVudF9kaXYgLmhlYWRfbGJsIHtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLndoaXRlX2RpdiB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAud2hpdGVfZGl2IGlvbi1pY29uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTZweDtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLndoaXRlX2RpdiBpb24tYnV0dG9uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC1zaXplOiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICB3aWR0aDogMTAwJTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5mX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuZl9kaXYgLmRldGFpbCB7XG4gIHdpZHRoOiBtYXgtY29udGVudDtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5mX2RpdiAuZGV0YWlsIC5kZXNjIHtcbiAgY29sb3I6IGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmZfZGl2IC5iYWNrX2ltYWdlIHtcbiAgaGVpZ2h0OiA2MHB4O1xuICB3aWR0aDogNjBweDtcbiAgbWluLXdpZHRoOiA2MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuZl9kaXYgLnRpbWUge1xuICBtYXJnaW4tbGVmdDogMnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5zX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLnNfZGl2IGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiA0MHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5zX2RpdiAuY3Jvc3Mge1xuICBjb2xvcjogZ3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuc19kaXYgLmNoZWNrIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgLmNvbF9kaXYge1xuICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgaW9uLWdyaWQge1xuICBwYWRkaW5nOiAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IC5jb3ZlciB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IC5jb3ZlciAuY2xvc2Uge1xuICBmb250LXNpemU6IDI1cHg7XG4gIGNvbG9yOiBibGFjaztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogNXB4O1xuICB0b3A6IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiAuYmFja19pbWFnZSB7XG4gIGhlaWdodDogNzVweDtcbiAgd2lkdGg6IDc1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSk7XG4gIHRvcDogNDAlO1xuICBib3JkZXI6IDNweCBzb2xpZCB3aGl0ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiAudXNlcm5hbWUge1xuICBtYXJnaW4tdG9wOiA0NXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiAuZGV0YWlsIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBjb2xvcjogZ3JheTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXNlcl9kaXYgLm11dHVhbF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogOHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IC5tdXR1YWxfZGl2IC5saW5rIHtcbiAgd2lkdGg6IDE1cHg7XG4gIGhlaWdodDogMTVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51c2VyX2RpdiAubXV0dWFsX2RpdiAubGlua19sYmwge1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiBncmF5O1xuICBtYXJnaW4tbGVmdDogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnVzZXJfZGl2IGlvbi1idXR0b24ge1xuICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IDYwMHB4O1xuICBtYXJnaW46IDEwcHg7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/pages/activity-list/activity-list.page.ts":
  /*!***********************************************************!*\
    !*** ./src/app/pages/activity-list/activity-list.page.ts ***!
    \***********************************************************/

  /*! exports provided: ActivityListPage */

  /***/
  function srcAppPagesActivityListActivityListPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ActivityListPage", function () {
      return ActivityListPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/services/dummy-data.service */
    "./src/app/services/dummy-data.service.ts");
    /* harmony import */


    var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic-native/native-page-transitions/ngx */
    "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var ActivityListPage = /*#__PURE__*/function () {
      function ActivityListPage(navCtrl, toastCtrl, nativePageTransitions, router, storage, dummy, api, menuCtrl) {
        var _this = this;

        _classCallCheck(this, ActivityListPage);

        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.router = router;
        this.storage = storage;
        this.dummy = dummy;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.members = [];
        this.activities = [];
        this.groups = [];
        this.notifications = [];
        this.forums = [];
        this.users = [{
          img: 'assets/imgs/user.jpg',
          cover: 'assets/imgs/back1.jpg'
        }, {
          img: 'assets/imgs/user2.jpg',
          cover: 'assets/imgs/back2.jpg'
        }, {
          img: 'assets/imgs/user3.jpg',
          cover: 'assets/imgs/back3.jpg'
        }, {
          img: 'assets/imgs/user4.jpg',
          cover: 'assets/imgs/back4.jpg'
        }];
        this.load = false;
        this.showSlider = false;
        this.options = {
          direction: 'up',
          duration: 500,
          slowdownfactor: 3,
          slidePixels: 20,
          iosdelay: 100,
          androiddelay: 150,
          fixedPixelsTop: 0,
          fixedPixelsBottom: 60
        };
        this.showtab = true;
        this.load = true;
        this.storage.get('COMPLETE_USER_INFO').then(function (result) {
          if (result != null) {
            console.log(result);
            _this.token = result.token;
            _this.user_id = result.user_id;
            console.log('user_id: ' + result.user_id);
            console.log('token: ' + result.token);
          }
        })["catch"](function (e) {
          console.log('error: ' + e); // Handle errors here
        });
        this.loadMembers();
        this.dummyData = this.dummy.users;
      }

      _createClass(ActivityListPage, [{
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.nativePageTransitions.slide(this.options);
        }
      }, {
        key: "connectuser",
        value: function connectuser(id) {
          var _this2 = this;

          this.api.connectfrds(id, this.token, this.user_id).subscribe(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var index, data, toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      console.log(res);
                      index = this.members.findIndex(function (data) {
                        return data.id === id;
                      });
                      data = this.members.find(function (data) {
                        return data.id === id;
                      });
                      console.log(data);

                      if (typeof index !== 'undefined') {
                        data.friendship_status = "pending";
                        this.members.splice(index, 1, data);
                      }

                      _context.next = 7;
                      return this.toastCtrl.create({
                        message: "Friend Request Sent Successfully",
                        duration: 3000
                      });

                    case 7:
                      toast = _context.sent;
                      _context.next = 10;
                      return toast.present();

                    case 10:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          });
        }
      }, {
        key: "loadData",
        value: function loadData(event) {
          var _this3 = this;

          var page = Math.ceil(this.activities.length / 10) + 1;
          this.api.getActivities(page).subscribe(function (newPagePosts) {
            var _this3$activities;

            (_this3$activities = _this3.activities).push.apply(_this3$activities, _toConsumableArray(newPagePosts));

            event.target.complete();
          }, function (err) {
            // there are no more posts available
            event.target.disabled = true;
          });
        } // example of adding a transition when pushing a new page

      }, {
        key: "openPage",
        value: function openPage(page) {
          this.nativePageTransitions.slide(this.options);
          this.navCtrl.navigateForward(page);
        }
      }, {
        key: "loadMembers",
        value: function loadMembers() {
          var _this4 = this;

          this.api.getActivities().subscribe(function (res) {
            console.log(res);
            _this4.activities = res;
          });
          this.load = false;
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "myHeaderFn",
        value: function myHeaderFn(record, recordIndex, records) {
          if (recordIndex % 20 === 0) {
            return 'Header ' + recordIndex;
          }

          return null;
        }
      }, {
        key: "goToPeopleProfile",
        value: function goToPeopleProfile() {
          this.router.navigate(['/people-profile']);
        }
      }, {
        key: "openMenu",
        value: function openMenu() {
          this.menuCtrl.open();
        }
      }, {
        key: "goToMemberslist",
        value: function goToMemberslist() {
          this.router.navigate(['/members-list']);
        }
      }, {
        key: "goToGroupslist",
        value: function goToGroupslist() {
          this.router.navigate(['/groups-list']);
        }
      }, {
        key: "sliderShow",
        value: function sliderShow(value) {
          this.showSlider = value;
        }
      }, {
        key: "gotoInvitations",
        value: function gotoInvitations() {
          this.router.navigate(['/invitations']);
        }
      }, {
        key: "gotoGroup",
        value: function gotoGroup() {
          this.router.navigate(['/group-detail']);
        }
      }, {
        key: "onSearchChange",
        value: function onSearchChange(event) {}
      }, {
        key: "goToChatList",
        value: function goToChatList() {
          this.router.navigate(['/chatlist']);
        }
      }]);

      return ActivityListPage;
    }();

    ActivityListPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"]
      }, {
        type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__["NativePageTransitions"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"]
      }, {
        type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__["dummyDataService"]
      }, {
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"]
      }];
    };

    ActivityListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-activity-list',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./activity-list.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/activity-list/activity-list.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./activity-list.page.scss */
      "./src/app/pages/activity-list/activity-list.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"], _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__["NativePageTransitions"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__["dummyDataService"], _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"]])], ActivityListPage);
    /***/
  },

  /***/
  "./src/app/services/dummy-data.service.ts":
  /*!************************************************!*\
    !*** ./src/app/services/dummy-data.service.ts ***!
    \************************************************/

  /*! exports provided: dummyDataService */

  /***/
  function srcAppServicesdummyDataServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "dummyDataService", function () {
      return dummyDataService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var dummyDataService = function dummyDataService() {
      _classCallCheck(this, dummyDataService);

      this.users = [{
        img: 'assets/imgs/user.jpg',
        cover: 'assets/imgs/back1.jpg',
        btn_text: 'VIEW JOBS'
      }, {
        img: 'assets/imgs/user2.jpg',
        cover: 'assets/imgs/back2.jpg',
        btn_text: 'FOLLOW HASHTAG'
      }, {
        img: 'assets/imgs/user3.jpg',
        cover: 'assets/imgs/back3.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user4.jpg',
        cover: 'assets/imgs/back4.jpg',
        btn_text: 'SEE ALL VIEW'
      }, {
        img: 'assets/imgs/user.jpg',
        cover: 'assets/imgs/back1.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user2.jpg',
        cover: 'assets/imgs/back2.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user3.jpg',
        cover: 'assets/imgs/back3.jpg',
        btn_text: 'EXPAND YOUR NETWORK'
      }, {
        img: 'assets/imgs/user4.jpg',
        cover: 'assets/imgs/back4.jpg',
        btn_text: 'SEE ALL VIEW'
      }];
      this.jobs = [{
        img: 'assets/imgs/company/initappz.png',
        title: 'SQL Server',
        addr: 'Bhavnagar, Gujrat, India',
        time: '2 school alumni',
        status: '1 day'
      }, {
        img: 'assets/imgs/company/google.png',
        title: 'Web Designer',
        addr: 'Vadodara, Gujrat, India',
        time: 'Be an early applicant',
        status: '2 days'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        title: 'Business Analyst',
        addr: 'Ahmedabad, Gujrat, India',
        time: 'Be an early applicant',
        status: '1 week'
      }, {
        img: 'assets/imgs/company/initappz.png',
        title: 'PHP Developer',
        addr: 'Pune, Maharashtra, India',
        time: '2 school alumni',
        status: 'New'
      }, {
        img: 'assets/imgs/company/google.png',
        title: 'Graphics Designer',
        addr: 'Bhavnagar, Gujrat, India',
        time: 'Be an early applicant',
        status: '1 day'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        title: 'Phython Developer',
        addr: 'Ahmedabad, Gujrat, India',
        time: '2 school alumni',
        status: '5 days'
      }];
      this.company = [{
        img: 'assets/imgs/company/initappz.png',
        name: 'Initappz Shop'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        name: 'Microsoft'
      }, {
        img: 'assets/imgs/company/google.png',
        name: 'Google'
      }];
    };

    dummyDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], dummyDataService);
    /***/
  }
}]);
//# sourceMappingURL=pages-activity-list-activity-list-module-es5.js.map
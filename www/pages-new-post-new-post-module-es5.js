function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-new-post-new-post-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/new-post/new-post.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/new-post/new-post.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesNewPostNewPostPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title color=\"light\">Share Post</ion-title>\n    <ion-button slot=\"end\" expand=\"block\" mode=\"md\" color=\"light\" style=\"    font-size: 23px;\n    box-shadow: 0 3px 1px -2px rgb(0 0 0 / 20%), 0 2px 2px 0 rgb(0 0 0 / 14%), 0 1px 5px 0 rgb(0 0 0 / 12%);\" fill=\"clear\" size=\"small\" (click)=\"createpost()\">\n      <ion-icon name=\"paper-plane-outline\"></ion-icon>\n    </ion-button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n   \n    <div class=\"flex_div\">\n      <div class=\"back_image\" [style.backgroundImage]=\"'url('+mydata.avatar+')'\"></div>\n      <div class=\"content_div\">\n        <ion-label class=\"username\">{{mydata.name}}</ion-label>\n        <ion-select value=\"public\"  [(ngModel)]=\"privacy\" okText=\"Okay\" cancelText=\"Dismiss\">\n          <ion-select-option value=\"public\">Anyone</ion-select-option>\n          <ion-select-option value=\"friends\">Friends</ion-select-option>\n          <ion-select-option value=\"onlyme\">Onlyme</ion-select-option>\n        </ion-select>\n      </div>\n    </div>\n<ion-grid>\n  <ion-row>\n  <ion-col size=\"4\" style=\"margin-top: 15px;\" *ngFor=\"let item of file_imgs\">\n    <ion-icon  (click)=\"dltmedia(item.id)\" style=\"position: relative; bottom: 75px; left: 85px;\" name=\"close-circle-outline\"></ion-icon>\n    <img style=\"max-width: 80px; border-radius: 10px;\" src=\"{{item.img}}\" >\n  </ion-col>\n</ion-row>\n</ion-grid>\n    <div class=\"textarea_div\">\n      <ion-textarea autoGrow=\"true\"  [(ngModel)]=\"content\" placeholder=\"What do you want to talk about? \"></ion-textarea>\n    </div>\n\n  </div>\n</ion-content>\n<ion-footer>\n  <!-- <div class=\"hashtag_div\">\n    <ion-label class=\"blue_lbl\"># Add Hashtag</ion-label>\n    <ion-label class=\"gray_lbl\">Help thi right people see your post</ion-label>\n  </div> -->\n  <input\n  type=\"file\"\n  #fileInput\n  (change)=\"uploadFile($event)\"\n  hidden=\"true\"\n  accept=\"image/*\"\n/>\n  <div style=\"position: relative;\">\n     <!-- <ion-button  (click)=\"addImage()\" size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"camera-outline\"></ion-icon>\n    </ion-button> -->\n  <!--  <ion-button size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"videocam-outline\"></ion-icon>\n    </ion-button> -->\n    <ion-button (click)=\"selectImage()\" size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"image-outline\"></ion-icon>\n    </ion-button>\n    <ion-button *ngIf=\"!plt.is('hybrid')\" (click)=\"attach()\" size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"ellipsis-horizontal\"></ion-icon>\n    </ion-button>\n    <!-- <ion-button size=\"small\" fill=\"clear\" class=\"at_btn\">\n      <ion-icon slot=\"icon-only\" name=\"at-outline\"></ion-icon>\n    </ion-button> -->\n  </div>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/pages/new-post/new-post-routing.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/pages/new-post/new-post-routing.module.ts ***!
    \***********************************************************/

  /*! exports provided: NewPostPageRoutingModule */

  /***/
  function srcAppPagesNewPostNewPostRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewPostPageRoutingModule", function () {
      return NewPostPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _new_post_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./new-post.page */
    "./src/app/pages/new-post/new-post.page.ts");

    var routes = [{
      path: '',
      component: _new_post_page__WEBPACK_IMPORTED_MODULE_3__["NewPostPage"]
    }];

    var NewPostPageRoutingModule = function NewPostPageRoutingModule() {
      _classCallCheck(this, NewPostPageRoutingModule);
    };

    NewPostPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], NewPostPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/new-post/new-post.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/new-post/new-post.module.ts ***!
    \***************************************************/

  /*! exports provided: NewPostPageModule */

  /***/
  function srcAppPagesNewPostNewPostModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewPostPageModule", function () {
      return NewPostPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _new_post_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./new-post-routing.module */
    "./src/app/pages/new-post/new-post-routing.module.ts");
    /* harmony import */


    var _new_post_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./new-post.page */
    "./src/app/pages/new-post/new-post.page.ts");

    var NewPostPageModule = function NewPostPageModule() {
      _classCallCheck(this, NewPostPageModule);
    };

    NewPostPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _new_post_routing_module__WEBPACK_IMPORTED_MODULE_5__["NewPostPageRoutingModule"]],
      declarations: [_new_post_page__WEBPACK_IMPORTED_MODULE_6__["NewPostPage"]]
    })], NewPostPageModule);
    /***/
  },

  /***/
  "./src/app/pages/new-post/new-post.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/pages/new-post/new-post.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesNewPostNewPostPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\nion-button {\n  color: white;\n  font-weight: 600px;\n  font-size: 16px;\n}\n\n.main_content_div {\n  width: 100%;\n  padding: 16px;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div .flex_div {\n  display: flex;\n  align-items: center;\n}\n\n.main_content_div .flex_div .back_image {\n  height: 50px;\n  width: 50px;\n  min-width: 50px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.main_content_div .flex_div .content_div {\n  margin-left: 10px;\n}\n\n.main_content_div .flex_div .content_div .username {\n  font-weight: 600;\n  margin-bottom: 3px;\n}\n\n.main_content_div .flex_div .content_div ion-select {\n  border: 1px solid lightgrey;\n  border-radius: 5px;\n  padding: 3px 10px;\n  font-size: 14px;\n}\n\n.main_content_div .textarea_div {\n  margin-top: 20px;\n}\n\n.main_content_div .textarea_div ion-textarea {\n  width: 100%;\n  height: 200px;\n}\n\nion-footer .hashtag_div {\n  display: flex;\n  justify-content: space-between;\n  font-size: 14px;\n  padding: 16px;\n  font-weight: 600;\n}\n\nion-footer .hashtag_div .blue_lbl {\n  color: var(--ion-color-main);\n}\n\nion-footer .hashtag_div .gray_lbl {\n  color: gray;\n}\n\nion-footer ion-icon {\n  color: var(--ion-color-main);\n  font-size: 20px;\n}\n\nion-footer ion-button {\n  margin: 0;\n}\n\nion-footer .at_btn {\n  position: absolute;\n  right: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbmV3LXBvc3QvRDpcXFJhZ3V2YXJhbiBpbWFnZVxcZGlzZWxzaGlwL3NyY1xcYXBwXFxwYWdlc1xcbmV3LXBvc3RcXG5ldy1wb3N0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbmV3LXBvc3QvbmV3LXBvc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUNBQUE7QUNDSjs7QURDQTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUNFSjs7QURDQTtFQUNJLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUNFSjs7QURDQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0FDRUo7O0FEQUk7RUFDSSxjQUFBO0FDRVI7O0FEQ0k7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUNDUjs7QURBUTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDRVo7O0FEQVE7RUFDSSxpQkFBQTtBQ0VaOztBREFZO0VBQ0ksZ0JBQUE7RUFDQSxrQkFBQTtBQ0VoQjs7QURDWTtFQUNJLDJCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNDaEI7O0FESUk7RUFDSSxnQkFBQTtBQ0ZSOztBREdRO0VBQ0ksV0FBQTtFQUNBLGFBQUE7QUNEWjs7QURRSTtFQUNJLGFBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUNMUjs7QURPUTtFQUNJLDRCQUFBO0FDTFo7O0FET1E7RUFDSSxXQUFBO0FDTFo7O0FEUUk7RUFDSSw0QkFBQTtFQUNBLGVBQUE7QUNOUjs7QURRSTtFQUNJLFNBQUE7QUNOUjs7QURRSTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtBQ05SIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbmV3LXBvc3QvbmV3LXBvc3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWh7XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG5pb24tYnV0dG9ue1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXdlaWdodDogNjAwcHg7XG4gICAgZm9udC1zaXplOiAxNnB4O1xufVxuXG4ubWFpbl9jb250ZW50X2RpdntcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAxNnB4O1xuXG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuXG4gICAgLmZsZXhfZGl2e1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAuYmFja19pbWFnZXtcbiAgICAgICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgICAgIHdpZHRoOiA1MHB4O1xuICAgICAgICAgICAgbWluLXdpZHRoOiA1MHB4O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgIH1cbiAgICAgICAgLmNvbnRlbnRfZGl2e1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG5cbiAgICAgICAgICAgIC51c2VybmFtZXtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDNweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaW9uLXNlbGVjdHtcbiAgICAgICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyZXk7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDNweCAxMHB4O1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC50ZXh0YXJlYV9kaXZ7XG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICAgIGlvbi10ZXh0YXJlYXtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgaGVpZ2h0OiAyMDBweDtcbiAgICAgICAgICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgfVxuICAgIH1cbn1cbmlvbi1mb290ZXJ7XG5cbiAgICAuaGFzaHRhZ19kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuXG4gICAgICAgIC5ibHVlX2xibHtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIH1cbiAgICAgICAgLmdyYXlfbGJse1xuICAgICAgICAgICAgY29sb3IgOiBncmF5O1xuICAgICAgICB9XG4gICAgfVxuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgfVxuICAgIGlvbi1idXR0b257XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICB9XG4gICAgLmF0X2J0bntcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICByaWdodDogMDtcbiAgICB9XG59IiwiaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG5pb24tYnV0dG9uIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDogNjAwcHg7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IGlvbi1sYWJlbCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuYmFja19pbWFnZSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDUwcHg7XG4gIG1pbi13aWR0aDogNTBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IC51c2VybmFtZSB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIG1hcmdpbi1ib3R0b206IDNweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuY29udGVudF9kaXYgaW9uLXNlbGVjdCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JleTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwYWRkaW5nOiAzcHggMTBweDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnRleHRhcmVhX2RpdiB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudGV4dGFyZWFfZGl2IGlvbi10ZXh0YXJlYSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDIwMHB4O1xufVxuXG5pb24tZm9vdGVyIC5oYXNodGFnX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBwYWRkaW5nOiAxNnB4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuaW9uLWZvb3RlciAuaGFzaHRhZ19kaXYgLmJsdWVfbGJsIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cbmlvbi1mb290ZXIgLmhhc2h0YWdfZGl2IC5ncmF5X2xibCB7XG4gIGNvbG9yOiBncmF5O1xufVxuaW9uLWZvb3RlciBpb24taWNvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cbmlvbi1mb290ZXIgaW9uLWJ1dHRvbiB7XG4gIG1hcmdpbjogMDtcbn1cbmlvbi1mb290ZXIgLmF0X2J0biB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDA7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/pages/new-post/new-post.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/pages/new-post/new-post.page.ts ***!
    \*************************************************/

  /*! exports provided: NewPostPage */

  /***/
  function srcAppPagesNewPostNewPostPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewPostPage", function () {
      return NewPostPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! sweetalert2 */
    "./node_modules/sweetalert2/dist/sweetalert2.all.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic-native/camera/ngx */
    "./node_modules/@ionic-native/camera/ngx/index.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/image-picker/ngx */
    "./node_modules/@ionic-native/image-picker/ngx/index.js");
    /* harmony import */


    var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/android-permissions/ngx */
    "./node_modules/@ionic-native/android-permissions/ngx/index.js");
    /* harmony import */


    var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/file/ngx */
    "./node_modules/@ionic-native/file/ngx/index.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var NewPostPage = /*#__PURE__*/function () {
      function NewPostPage(file, camera, router, imagePicker, plt, storage, androidPermissions, actionSheet, alertCtrl, toastCtrl, api, loadingController) {
        var _this = this;

        _classCallCheck(this, NewPostPage);

        this.file = file;
        this.camera = camera;
        this.router = router;
        this.imagePicker = imagePicker;
        this.plt = plt;
        this.storage = storage;
        this.androidPermissions = androidPermissions;
        this.actionSheet = actionSheet;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.api = api;
        this.loadingController = loadingController;
        this.file_ids = [];
        this.file_imgs = [];
        this.mydata = [];
        this.privacy = "public";
        this.storage.get('COMPLETE_USER_INFO').then(function (result) {
          if (result != null) {
            console.log(result);
            _this.token = result.token;
            _this.user_id = result.user_id;
            console.log('user_id: ' + result.user_id);
            console.log('token: ' + result.token);
          }
        })["catch"](function (e) {
          console.log('error: ' + e); // Handle errors here
        });
        this.storage.get('USER_INFO').then(function (result) {
          console.log("USER_INFO", result);
          _this.mydata.avatar = result.avatar_urls[24];
          console.log(result.avatar_urls[24]);
          if (typeof result.avatar_urls[24] === "undefined") _this.mydata.avatar = result.avatar_urls['full'];
          _this.mydata.name = result.profile_name;
        })["catch"](function (e) {
          console.log('error: ' + e); // Handle errors here
        });
      }

      _createClass(NewPostPage, [{
        key: "selectImage",
        value: function selectImage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this2 = this;

            var options, imageResponse;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                      return console.log('Has permission?', result.hasPermission);
                    }, function (err) {
                      return _this2.androidPermissions.requestPermission(_this2.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                    });
                    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]); // this.loading=true;

                    options = {
                      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
                      // selection of a single image, the plugin will return it.
                      maximumImagesCount: 1,
                      // max width and height to allow the images to be.  Will keep aspect
                      // ratio no matter what.  So if both are 800, the returned image
                      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
                      // 800 and height 0 the image will be 800 pixels wide if the source
                      // is at least that wide.
                      width: 200,
                      //height: 200,
                      // quality of resized image, defaults to 100
                      quality: 25,
                      // output type, defaults to FILE_URIs.
                      // available options are 
                      // window.imagePicker.OutputType.FILE_URI (0) or 
                      // window.imagePicker.OutputType.BASE64_STRING (1)
                      outputType: 1
                    };
                    imageResponse = [];
                    this.imagePicker.getPictures(options).then(function (results) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                        var _this3 = this;

                        var imageData, byteCharacters, byteNumbers, i, byteArray, blob1;
                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                          while (1) {
                            switch (_context2.prev = _context2.next) {
                              case 0:
                                // for (var i = 0; i < results.length; i++) {
                                //   this.file.resolveLocalFilesystemUrl(results[i]).then((entry: FileEntry) => 
                                // {
                                //   entry.file(file => {
                                //     console.log(file);
                                //    this.readFile(file);
                                // this.file_imgs.push({
                                //   img: 'data:image/jpeg;base64,' + results[0]});
                                // const base64 = await fetch(results[0]);
                                // const blob = await base64.blob();
                                //   let blob = this.getBlob(results, ".jpg")
                                //     this.api.uploadMedia1(blob,"test","jpg").subscribe(
                                //       async (res : any) => {
                                //        this.file_ids.push(res.upload_id);
                                //        this.file_imgs.push({
                                //         img: res.upload_thumb, 
                                //         id:  res.upload_id
                                //     });
                                //       console.log("---------------");
                                //       console.log(this.file_ids);
                                //       const toast = await this.toastCtrl.create({
                                //         message: 'Image Uploaded',
                                //         duration: 3000
                                //       });
                                //       toast.present();
                                // // Get the entire data
                                //     },
                                //     err => {
                                //       console.log(err);
                                //       this.showError(err);
                                //     }
                                //     );
                                imageData = results[0].toString(); //  var imageData1 = imageData.toString();

                                byteCharacters = atob(imageData.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
                                byteNumbers = new Array(byteCharacters.length);

                                for (i = 0; i < byteCharacters.length; i++) {
                                  byteNumbers[i] = byteCharacters.charCodeAt(i);
                                }

                                byteArray = new Uint8Array(byteNumbers);
                                blob1 = new Blob([byteArray], {
                                  type: "jpg"
                                });
                                console.log(blob1);
                                this.api.uploadMedia1(blob1, "jpg", "test", this.token).subscribe(function (res) {
                                  return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                                    var toast;
                                    return regeneratorRuntime.wrap(function _callee$(_context) {
                                      while (1) {
                                        switch (_context.prev = _context.next) {
                                          case 0:
                                            this.file_ids.push(res.upload_id);
                                            this.file_imgs.push({
                                              img: res.upload_thumb,
                                              id: res.upload_id
                                            });
                                            console.log("---------------");
                                            console.log(this.file_ids);
                                            _context.next = 6;
                                            return this.toastCtrl.create({
                                              message: 'Image Uploaded',
                                              duration: 3000
                                            });

                                          case 6:
                                            toast = _context.sent;
                                            toast.present(); // Get the entire data

                                          case 8:
                                          case "end":
                                            return _context.stop();
                                        }
                                      }
                                    }, _callee, this);
                                  }));
                                }, function (err) {
                                  console.log(err);

                                  _this3.showError(err);
                                }); //   });
                                //  });
                                //  } 

                              case 8:
                              case "end":
                                return _context2.stop();
                            }
                          }
                        }, _callee2, this);
                      }));
                    }, function (err) {
                      alert(err);
                    });

                  case 5:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "addImage",
        value: function addImage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var _this4 = this;

            var options;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    options = {
                      quality: 100,
                      destinationType: this.camera.DestinationType.DATA_URL,
                      encodingType: this.camera.EncodingType.JPEG,
                      mediaType: this.camera.MediaType.PICTURE
                    };
                    this.camera.getPicture(options).then(function (imageData) {
                      // console.log(imageData);
                      //       let mimeType = imageData.profilepic.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
                      //       mimeType = mimeType.replace("image/", "");
                      //       console.log(mimeType);
                      // let blob = this.getBlob(imageData, ".jpg")
                      var imageData1 = imageData.toString();
                      var byteCharacters = atob(imageData1.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
                      var byteNumbers = new Array(byteCharacters.length);

                      for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                      }

                      var byteArray = new Uint8Array(byteNumbers);
                      var blob1 = new Blob([byteArray], {
                        type: "jpg"
                      });
                      console.log(blob1);

                      _this4.api.uploadMedia1(blob1, "jpg", "test", _this4.token).subscribe(function (res) {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                          var toast;
                          return regeneratorRuntime.wrap(function _callee4$(_context4) {
                            while (1) {
                              switch (_context4.prev = _context4.next) {
                                case 0:
                                  this.file_ids.push(res.upload_id);
                                  this.file_imgs.push({
                                    img: res.upload_thumb,
                                    id: res.upload_id
                                  }); // this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => 
                                  // {
                                  //   entry.file(file => {
                                  //     console.log(file);
                                  //     this.readFile(file);
                                  //     this.api.uploadMedia1(this.blob,this.file_name,this.file_type).subscribe(
                                  //       async (res : any) => {
                                  //        this.file_ids.push(res.upload_id);
                                  //        this.file_imgs.push({
                                  //         img: res.upload_thumb, 
                                  //         id:  res.upload_id
                                  //     });

                                  console.log("---------------");
                                  console.log(this.file_ids);
                                  _context4.next = 6;
                                  return this.toastCtrl.create({
                                    message: 'Image Uploaded',
                                    duration: 3000
                                  });

                                case 6:
                                  toast = _context4.sent;
                                  toast.present(); // Get the entire data

                                case 8:
                                case "end":
                                  return _context4.stop();
                              }
                            }
                          }, _callee4, this);
                        }));
                      }, function (err) {
                        console.log(err);

                        _this4.showError(err);
                      }); //  this.readFile(file);
                      //   });
                      // });

                    }, function (err) {// Handle error
                    });

                  case 2:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "getBlob",
        value: function getBlob(b64Data, contentType) {
          var sliceSize = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 512;
          contentType = contentType || '';
          sliceSize = sliceSize || 512;
          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);

            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
          }

          var blob = new Blob(byteArrays, {
            type: contentType
          });
          return blob;
        }
      }, {
        key: "selectcoverImageSource",
        value: function selectcoverImageSource() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var _this5 = this;

            var buttons, actionSheet;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    buttons = [// {
                    //   text: 'Take Photo',
                    //   icon: 'camera',
                    //   handler: () => {
                    //     this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
                    //       result => console.log('Has permission?',result.hasPermission),
                    //       err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
                    //     );
                    //     this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
                    //     this.addImage();
                    //   }
                    // },
                    {
                      text: 'Choose From Photos Photo',
                      icon: 'image',
                      handler: function handler() {
                        _this5.androidPermissions.checkPermission(_this5.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                          return console.log('Has permission?', result.hasPermission);
                        }, function (err) {
                          return _this5.androidPermissions.requestPermission(_this5.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                        });

                        _this5.androidPermissions.requestPermissions([_this5.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, _this5.androidPermissions.PERMISSION.GET_ACCOUNTS]);

                        _this5.selectImage();
                      }
                    }]; // Only allow file selection inside a browser

                    if (!this.plt.is('hybrid')) {
                      buttons.push({
                        text: 'Choose a File',
                        icon: 'attach',
                        handler: function handler() {
                          _this5.androidPermissions.checkPermission(_this5.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                            return console.log('Has permission?', result.hasPermission);
                          }, function (err) {
                            return _this5.androidPermissions.requestPermission(_this5.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                          });

                          _this5.androidPermissions.requestPermissions([_this5.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, _this5.androidPermissions.PERMISSION.GET_ACCOUNTS]);

                          _this5.fileInput.nativeElement.click();
                        }
                      });
                    }

                    _context6.next = 4;
                    return this.actionSheet.create({
                      header: 'Select Image Source',
                      buttons: buttons
                    });

                  case 4:
                    actionSheet = _context6.sent;
                    _context6.next = 7;
                    return actionSheet.present();

                  case 7:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }, {
        key: "dltmedia",
        value: function dltmedia(id) {
          var index = this.file_imgs.findIndex(function (data) {
            return data.id === id;
          });
          this.file_imgs.splice(index, 1);
          var index2 = this.file_ids.findIndex(function (data) {
            return data === id;
          });
          this.file_ids.splice(index2, 1); // console.log(this.file_imgs);
          // console.log(index);
          //     this.api.deltmedia(id,this.token).subscribe(
          //       async (res : any) => {
          //       console.log("---------------");
          //       console.log(res);
          //       const toast = await this.toastCtrl.create({
          //         message: 'Image Sucessfully Removed',
          //         duration: 3000
          //       });
          //       toast.present();
          // // Get the entire data
          //     },
          //     err => {
          //       console.log(err);
          //       this.showError(err);
          //     }
          //     );
        }
      }, {
        key: "createpost",
        value: function createpost() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
            var _this6 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee8$(_context8) {
              while (1) {
                switch (_context8.prev = _context8.next) {
                  case 0:
                    _context8.next = 2;
                    return this.loadingController.create({
                      message: 'Please wait...',
                      duration: 2000
                    });

                  case 2:
                    loading = _context8.sent;
                    _context8.next = 5;
                    return loading.present();

                  case 5:
                    this.api.createPost(this.user_id, this.file_ids, this.content, this.privacy).subscribe(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this6, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
                        var toast;
                        return regeneratorRuntime.wrap(function _callee7$(_context7) {
                          while (1) {
                            switch (_context7.prev = _context7.next) {
                              case 0:
                                console.log("---------------");
                                console.log(res);
                                _context7.next = 4;
                                return this.toastCtrl.create({
                                  message: 'Post Sucessfully created',
                                  duration: 3000
                                });

                              case 4:
                                toast = _context7.sent;
                                toast.present(); // Get the entire data

                                this.router.navigate(['/news-feed']);

                              case 7:
                              case "end":
                                return _context7.stop();
                            }
                          }
                        }, _callee7, this);
                      }));
                    }, function (err) {
                      console.log(err);

                      _this6.showError(err);
                    });

                  case 6:
                  case "end":
                    return _context8.stop();
                }
              }
            }, _callee8, this);
          }));
        }
      }, {
        key: "attach",
        value: function attach() {
          this.fileInput.nativeElement.click();
        }
      }, {
        key: "uploadFile",
        value: function uploadFile(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
            var _this7 = this;

            var loading, eventObj, target, file1;
            return regeneratorRuntime.wrap(function _callee10$(_context10) {
              while (1) {
                switch (_context10.prev = _context10.next) {
                  case 0:
                    console.log(this.file_ids);
                    _context10.next = 3;
                    return this.loadingController.create({
                      message: 'Please wait...',
                      duration: 2000
                    });

                  case 3:
                    loading = _context10.sent;
                    _context10.next = 6;
                    return loading.present();

                  case 6:
                    eventObj = event;
                    target = eventObj.target;
                    file1 = target.files[0];
                    this.api.uploadMedia(file1).subscribe(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this7, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
                        var toast;
                        return regeneratorRuntime.wrap(function _callee9$(_context9) {
                          while (1) {
                            switch (_context9.prev = _context9.next) {
                              case 0:
                                this.file_ids.push(res.upload_id);
                                this.file_imgs.push({
                                  img: res.upload_thumb,
                                  id: res.upload_id
                                });
                                console.log("---------------");
                                console.log(this.file_ids);
                                _context9.next = 6;
                                return this.toastCtrl.create({
                                  message: 'Image Uploaded',
                                  duration: 3000
                                });

                              case 6:
                                toast = _context9.sent;
                                toast.present(); // Get the entire data

                              case 8:
                              case "end":
                                return _context9.stop();
                            }
                          }
                        }, _callee9, this);
                      }));
                    }, function (err) {
                      console.log(err);

                      _this7.showError(err);
                    });

                  case 10:
                  case "end":
                    return _context10.stop();
                }
              }
            }, _callee10, this);
          }));
        }
      }, {
        key: "showError",
        value: function showError(err) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
            var alert;
            return regeneratorRuntime.wrap(function _callee11$(_context11) {
              while (1) {
                switch (_context11.prev = _context11.next) {
                  case 0:
                    _context11.next = 2;
                    return this.alertCtrl.create({
                      header: err.error.code,
                      subHeader: err.error.data,
                      message: err.error.message,
                      buttons: ['OK']
                    });

                  case 2:
                    alert = _context11.sent;
                    _context11.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context11.stop();
                }
              }
            }, _callee11, this);
          }));
        }
      }, {
        key: "readFile",
        value: function readFile(file) {
          var _this8 = this;

          console.log(file.type);
          var reader = new FileReader();

          reader.onload = function () {
            _this8.blob = new Blob([reader.result], {
              type: file.type
            });
            _this8.file_name = file.name;
            _this8.file_type = file.type;
          };

          reader.readAsArrayBuffer(file);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.selectcoverImageSource();
        }
      }, {
        key: "openActionSheet",
        value: function openActionSheet() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
            var actionSheet;
            return regeneratorRuntime.wrap(function _callee12$(_context12) {
              while (1) {
                switch (_context12.prev = _context12.next) {
                  case 0:
                    _context12.next = 2;
                    return this.actionSheet.create({
                      mode: 'md',
                      buttons: [{
                        text: 'Add Photo',
                        icon: 'image-outline',
                        handler: function handler() {
                          console.log('Delete clicked');
                        }
                      }, {
                        text: 'Take a video',
                        icon: 'videocam-outline',
                        handler: function handler() {
                          console.log('Share clicked');
                        }
                      }, {
                        text: 'Celebrate a teammate',
                        icon: 'medal-outline',
                        handler: function handler() {
                          console.log('Play clicked');
                        }
                      }, {
                        text: 'Add a document',
                        icon: 'document-text-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Share that you are hiring',
                        icon: 'briefcase-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Find an expert',
                        icon: 'person-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }]
                    });

                  case 2:
                    actionSheet = _context12.sent;
                    _context12.next = 5;
                    return actionSheet.present();

                  case 5:
                  case "end":
                    return _context12.stop();
                }
              }
            }, _callee12, this);
          }));
        }
      }, {
        key: "post",
        value: function post() {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'Success',
            text: 'post created successfully',
            icon: 'success',
            backdrop: false
          });
        }
      }]);

      return NewPostPage;
    }();

    NewPostPage.ctorParameters = function () {
      return [{
        type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"]
      }, {
        type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__["Camera"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
      }, {
        type: _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_8__["ImagePicker"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__["AndroidPermissions"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_7__["apiService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], NewPostPage.prototype, "fileInput", void 0);
    NewPostPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-new-post',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./new-post.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/new-post/new-post.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./new-post.page.scss */
      "./src/app/pages/new-post/new-post.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__["Camera"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_8__["ImagePicker"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__["AndroidPermissions"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _services_api_service__WEBPACK_IMPORTED_MODULE_7__["apiService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])], NewPostPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-new-post-new-post-module-es5.js.map
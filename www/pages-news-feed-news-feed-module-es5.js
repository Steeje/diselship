function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-news-feed-news-feed-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/news-feed/news-feed.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/news-feed/news-feed.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesNewsFeedNewsFeedPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n      <ion-button (click)=\"BackButton()\" slot=\"start\">\n        <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n      </ion-button>\n      <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [(ngModel)]='searchtext'  [debounce]=\"250\"></ion-searchbar>\n      <ion-icon name=\"add-circle-outline\" mode=\"md\" color=\"light\" style=\"font-size: 40px;\" (click)=\"goTonewpost()\"></ion-icon>\n    </div>\n    <!-- <ion-title *ngIf=\"!categoryTitle\">Recent posts</ion-title> -->\n    <ion-title *ngIf=\"categoryTitle\">{{categoryTitle}} posts</ion-title>\n    <ion-buttons slot=\"start\" *ngIf=\"categoryTitle\">\n      <ion-back-button defaultHref=\"posts\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n   <div *ngIf=\"posts.length>0\" class=\"flex_div\">\n   \n    <ion-buttons (click)=\"fillternews('reset')\" *ngIf=\"!allupdates\" class=\"filterbtn\">\n   All Updates\n    </ion-buttons>\n  \n    <ion-buttons  (click)=\"fillternews('reset')\" *ngIf=\"allupdates\" class=\"filterbtn2\">\n      All Updates\n       </ion-buttons>\n    <ion-buttons (click)=\"fillternews('friends')\" *ngIf=\"!friends\" class=\"filterbtn\">\n   Friends\n    </ion-buttons>\n  \n    <ion-buttons (click)=\"fillternews('reset')\"  *ngIf=\"friends\" class=\"filterbtn2\">\n      Friends\n       </ion-buttons>\n    <ion-buttons  (click)=\"fillternews('favorites')\" *ngIf=\"!favorites\" class=\"filterbtn\">\n      Favorites\n    </ion-buttons>\n    <ion-buttons (click)=\"fillternews('reset')\" *ngIf=\"favorites\" class=\"filterbtn2\">\n      Favorites\n        </ion-buttons>\n    <ion-buttons (click)=\"fillternews('groups')\" *ngIf=\"!groups\" class=\"filterbtn\">\n      Groups\n        </ion-buttons>\n        <ion-buttons (click)=\"fillternews('reset')\" *ngIf=\"groups\" class=\"filterbtn2\">\n          Groups\n            </ion-buttons>\n        <ion-buttons (click)=\"fillternews('following')\" *ngIf=\"!following\" class=\"filterbtn\">\n          Follows\n            </ion-buttons>\n            <ion-buttons (click)=\"fillternews('reset')\" *ngIf=\"following\" class=\"filterbtn2\">\n              Follows\n                </ion-buttons>\n  \n</div> \n  <!-- <ion-slides style=\"height: 120px;\" mode=\"ios\"  pager=\"ios\" [options]=\"slideoptions\">\n    <ion-slide style=\"font-size: 14px;\">\n      <ion-buttons class=\"filterbtn\">\n        All Updates\n         </ion-buttons>\n       \n      \n         <ion-buttons class=\"filterbtn\">\n        Friends\n         </ion-buttons>\n       \n      \n         <ion-buttons class=\"filterbtn\">\n       Favouties\n         </ion-buttons>\n        \n        \n            \n         \n    </ion-slide> <ion-slide style=\"font-size: 14px;\">  <ion-buttons class=\"filterbtn\">\n      Groups\n        </ion-buttons>\n    <ion-buttons class=\"filterbtn\">\n      Follows\n        </ion-buttons>\n</ion-slide>\n  </ion-slides> -->\n\n  <img style=\"width: -webkit-fill-available;\" *ngIf=\"!posts.length>0\" src=\"assets/imgs/no_result.jpg\">\n  <div *ngIf=\"posts.length>0\" class=\"main_content_div\">\n\n   \n\n\n    \n    <div style=\"margin-top: 10px;\">\n      \n        <!-- Card 1 [routerLink]=\"['/post', post.id]\"  -->\n        <ion-content class=\"page-content\"></ion-content>\n        <div class=\"div_card\" *ngFor=\"let post of posts\" >\n          <div  class=\"head_div\">\n\n            <div class=\"img_div\"  [routerLink]=\"['/members-detail', post.user_id]\" [style.backgroundImage]=\"'url('+post.user_avatar+')'\"></div>\n\n            <div class=\"content_div\" (click)=\"singlefeed(post.id)\">\n              <p *ngIf=\"post.discussion!='' && grpid\" class=\"head_lbl\">{{post.name}} <ion-label style=\"color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.type_name}}</ion-label> \n                {{post.discussion}} <ion-label style=\"color: #959595; margin-right: 8px;\" class=\"head_type\">in the group</ion-label> <img src=\"{{data.media_url}}\" [routerLink]=\"['/group-detail', grpid]\" style=\"margin: -5px; border-radius: 10px;\"  width=\"20\" height=\"20\" alt=\"Group logo of {{post.group_name}}\">\n               <ion-label style=\" margin-left: 8px;\" [routerLink]=\"['/group-detail', grpid]\">{{post.group_name}}</ion-label></p>\n\n\n              <p *ngIf=\"post.discussion=='' && grpid \" class=\"head_lbl\">{{post.name}} <ion-label style=\"color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.type_name}}</ion-label> \n              <img src=\"{{data.media_url}}\" style=\"margin: -5px; border-radius: 10px;\"  width=\"20\" height=\"20\" [routerLink]=\"['/group-detail', grpid]\" alt=\"Group logo of {{post.group_name}}\">\n              <ion-label style=\" margin-left: 8px;\" [routerLink]=\"['/group-detail', grpid]\">{{post.group_name}}</ion-label></p>\n\n\n\n              <p *ngIf=\"post.component=='bbpress' && !grpid\" class=\"head_lbl\">{{post.name}} <ion-label style=\"color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.type_name}}</ion-label> \n                {{post.discussion}} </p>\n\n\n              <p *ngIf=\"post.component=='groups' && !grpid\" class=\"head_lbl\">{{post.name}} <ion-label style=\"color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.type_name}}</ion-label> \n              <img src=\"{{post.media_url}}\" style=\"margin: -5px; border-radius: 10px;\" [routerLink]=\"['/group-detail', post.grpid]\" width=\"20\" height=\"20\" alt=\"Group logo of {{post.group_name}}\">\n              <ion-label style=\" margin-left: 8px;\" [routerLink]=\"['/group-detail', post.grpid]\">{{post.group_name}}</ion-label></p>\n              \n              <p *ngIf=\"post.component!='groups' && post.component!='bbpress' && !grpid\"  class=\"head_lbl\">{{post.name}} <ion-label style=\"color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.title}}</ion-label> \n                </p>\n\n\n\n            </div>\n\n            <!-- <ion-buttons class=\"card_arrow\" (click)=\"openActionSheet()\">\n              <ion-button color=\"dark\">\n                <ion-icon name=\"chevron-down-outline\" mode=\"ios\"></ion-icon>\n              </ion-button>\n            </ion-buttons> -->\n          </div>\n          <ion-card-title (click)=\"singlefeed(post.id)\" style=\"    font-size: 16px;\n          margin-bottom: 10px;\" [innerHTML]=\"post.content_stripped\"></ion-card-title>\n          <!-- <div class=\"desc_div\">\n            <ion-label  [innerHTML]=\"post.excerpt.rendered\">\n              \n            </ion-label>\n\n\n           \n            <ion-label class=\"see_lbl\">See more</ion-label>\n           </div> -->\n           <div (click)=\"singlefeed(post.id)\" *ngIf=\"post.media\">\n<div *ngFor=\"let item of post.media\">\n          <div class=\"card_image\" [style.backgroundImage]=\"'url('+item+')'\"></div>\n\n        </div> </div>\n          <div class=\"like_div2\">\n            <div>\n              <img src=\"assets/imgs/like.png\" alt=\"\">\n              {{post.favorite_count}}\n            </div>\n            <div>{{post.comment_count}} Comments</div>\n          </div>\n\n          <div class=\"like_div\" style=\"padding: 0;border: none;\">\n            <div>\n              <ion-button  [class]=\"post.favorited\" fill=\"clear\" size=\"small\" (click)=\"likefeed(post.id)\">\n                <ion-icon name=\"thumbs-up-sharp\" style=\"margin-right:5px\"></ion-icon>Like\n              </ion-button>\n            </div>\n\n            <div>\n              <ion-button (click)=\"singlefeed(post.id)\"  fill=\"clear\" size=\"small\">\n                <ion-icon name=\"chatbox-ellipses-sharp\" style=\"margin-right:5px\"></ion-icon>Comment\n              </ion-button>\n            </div>\n            <div>\n              <ion-button (click)=\"openShareSheet(post.id)\" fill=\"clear\" size=\"small\">\n                <ion-icon name=\"share-social\" style=\"margin-right:5px\"></ion-icon>Share\n              </ion-button>\n            </div>\n\n          </div>\n        </div>\n     \n\n        <!-- card 2 -->\n <!--        <div class=\"div_card\">\n          <div class=\"head_div\">\n\n            <div class=\"img_div\" [style.backgroundImage]=\"'url(assets/imgs/user2.jpg)'\"></div>\n\n            <div class=\"content_div\">\n              <ion-label class=\"head_lbl\">John Doe</ion-label>\n              <ion-label class=\"small_lbl\">InitApps shop</ion-label>\n              <ion-label class=\"small_lbl\">1 w • Edited</ion-label>\n            </div>\n\n            <ion-buttons class=\"card_arrow\" (click)=\"openActionSheet()\">\n              <ion-button color=\"dark\">\n                <ion-icon name=\"chevron-down-outline\" mode=\"ios\"></ion-icon>\n              </ion-button>\n            </ion-buttons>\n          </div>\n\n          <div class=\"desc_div\">\n            <ion-label>\n              Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n              Lorem Ipsum has been the industry's standard dummy text\n            </ion-label>\n            <ion-label class=\"see_lbl\">See more</ion-label>\n          </div>\n\n          <div class=\"masonry\">\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid1.jpg\">\n            </div>\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid2.jpg\">\n            </div>\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid3.jpg\">\n            </div>\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid4.jpg\">\n            </div>\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid5.jpg\">\n            </div>\n          </div>\n\n          <div class=\"like_div2\">\n            <div>\n              <img src=\"assets/imgs/like.png\" alt=\"\">\n              <img src=\"assets/imgs/heart.png\" alt=\"\">\n            </div>\n            <div>25 Comments</div>\n          </div>\n\n          <div class=\"like_div\" style=\"padding: 0;border: none;\">\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"thumbs-up-sharp\" style=\"margin-right:5px\"></ion-icon>Like\n              </ion-button>\n            </div>\n\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"chatbox-ellipses-sharp\" style=\"margin-right:5px\"></ion-icon>Comment\n              </ion-button>\n            </div>\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"share-social\" style=\"margin-right:5px\"></ion-icon>Share\n              </ion-button>\n            </div>\n\n          </div>\n        </div>\n\n        Card 3 \n\n        <div class=\"div_card\">\n          <div class=\"head_div\">\n\n            <div class=\"img_div\" [style.backgroundImage]=\"'url(assets/imgs/user3.jpg)'\"></div>\n\n            <div class=\"content_div\">\n              <ion-label class=\"head_lbl\">John Doe</ion-label>\n              <ion-label class=\"small_lbl\">InitApps shop</ion-label>\n              <ion-label class=\"small_lbl\">1 w • Edited</ion-label>\n            </div>\n\n            <ion-buttons class=\"card_arrow\" (click)=\"openActionSheet()\">\n              <ion-button color=\"dark\">\n                <ion-icon name=\"chevron-down-outline\" mode=\"ios\"></ion-icon>\n              </ion-button>\n            </ion-buttons>\n          </div>\n\n          <div class=\"desc_div\">\n            <ion-label>\n              Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n              Lorem Ipsum has been the industry's standard dummy text\n            </ion-label>\n            <ion-label class=\"see_lbl\">See more</ion-label>\n          </div>\n\n          <video width=\"100%\" height=\"240\" controls poster=\"assets/imgs/grid/grid4.jpg\">\n            <source src=\"assets/imgs/video.mp4\" type=\"video/mp4\">\n          </video>\n\n          <div class=\"like_div2\">\n            <div>\n              <img src=\"assets/imgs/like.png\" alt=\"\">\n              <img src=\"assets/imgs/heart.png\" alt=\"\">\n            </div>\n            <div>25 Comments</div>\n          </div>\n\n          <div class=\"like_div\" style=\"padding: 0;border: none;\">\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"thumbs-up-sharp\"></ion-icon>Like\n              </ion-button>\n            </div>\n\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"chatbox-ellipses-sharp\"></ion-icon>Comment\n              </ion-button>\n            </div>\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"share-social\"></ion-icon>Share\n              </ion-button>\n            </div>\n\n          </div>\n        </div> -->\n      \n    </div>\n  </div>\n  <ion-infinite-scroll (ionInfinite)=\"loadData($event)\">\n    <ion-infinite-scroll-content\n    loadingSpinner=\"bubbles\"\n    loadingText=\"Loading more posts ...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/news-feed/news-feed-routing.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/pages/news-feed/news-feed-routing.module.ts ***!
    \*************************************************************/

  /*! exports provided: NewsFeedPageRoutingModule */

  /***/
  function srcAppPagesNewsFeedNewsFeedRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewsFeedPageRoutingModule", function () {
      return NewsFeedPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _news_feed_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./news-feed.page */
    "./src/app/pages/news-feed/news-feed.page.ts");

    var routes = [{
      path: '',
      component: _news_feed_page__WEBPACK_IMPORTED_MODULE_3__["NewsFeedPage"]
    }];

    var NewsFeedPageRoutingModule = function NewsFeedPageRoutingModule() {
      _classCallCheck(this, NewsFeedPageRoutingModule);
    };

    NewsFeedPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], NewsFeedPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/news-feed/news-feed.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/news-feed/news-feed.module.ts ***!
    \*****************************************************/

  /*! exports provided: NewsFeedPageModule */

  /***/
  function srcAppPagesNewsFeedNewsFeedModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewsFeedPageModule", function () {
      return NewsFeedPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _news_feed_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./news-feed-routing.module */
    "./src/app/pages/news-feed/news-feed-routing.module.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _news_feed_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./news-feed.page */
    "./src/app/pages/news-feed/news-feed.page.ts");
    /* harmony import */


    var _news_feed_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./news-feed.resolver */
    "./src/app/pages/news-feed/news-feed.resolver.ts");

    var routes = [{
      path: '',
      component: _news_feed_page__WEBPACK_IMPORTED_MODULE_7__["NewsFeedPage"],
      resolve: {
        data: _news_feed_resolver__WEBPACK_IMPORTED_MODULE_8__["PostsResolver"]
      },
      runGuardsAndResolvers: 'paramsOrQueryParamsChange' // because we use the same route for all posts and for category posts, we need to add this to refetch data 

    }];

    var NewsFeedPageModule = function NewsFeedPageModule() {
      _classCallCheck(this, NewsFeedPageModule);
    };

    NewsFeedPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _news_feed_routing_module__WEBPACK_IMPORTED_MODULE_5__["NewsFeedPageRoutingModule"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(routes)],
      declarations: [_news_feed_page__WEBPACK_IMPORTED_MODULE_7__["NewsFeedPage"]],
      providers: [_news_feed_resolver__WEBPACK_IMPORTED_MODULE_8__["PostsResolver"]]
    })], NewsFeedPageModule);
    /***/
  },

  /***/
  "./src/app/pages/news-feed/news-feed.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/pages/news-feed/news-feed.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesNewsFeedNewsFeedPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\n.header_div {\n  display: flex;\n  width: 100%;\n  justify-content: space-between;\n  align-items: center;\n  padding-left: 16px;\n  padding-right: 16px;\n  height: 40px;\n}\n\n.header_div .user_image {\n  height: 25px;\n  width: 25px;\n}\n\n.header_div ion-searchbar {\n  --background: white;\n  --placeholder-color: #707070;\n  --icon-color: #707070;\n  border-radius: 0px;\n  height: 30px;\n}\n\n.flex_div {\n  font-size: 12px;\n  display: flex;\n}\n\n.filterbtn2 {\n  padding: 7px;\n  border-radius: 20px;\n  cursor: pointer;\n  margin: 4px;\n  color: white;\n  background: var(--ion-color-main);\n}\n\n.filterbtn {\n  border: 2px solid var(--ion-color-main);\n  padding: 7px;\n  border-radius: 20px;\n  cursor: pointer;\n  margin: 4px;\n  background: aliceblue;\n}\n\nion-content {\n  --background: #F5F5F5;\n}\n\n.true {\n  color: #047ead !important;\n}\n\n.false {\n  color: gray;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div .up_lbl {\n  padding-left: 16px;\n  padding-right: 16px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n  background: #e1e9ee;\n}\n\n.main_content_div .chips_div {\n  display: flex;\n  flex-direction: row;\n  overflow: scroll;\n  padding-left: 16px;\n  padding-right: 16px;\n  background: #e1e9ee;\n  padding-top: 10px;\n  padding-bottom: 20px;\n}\n\n.main_content_div .chips_div .chip {\n  background-color: white;\n  border-radius: 3px;\n  padding: 5px 20px;\n  margin-right: 10px;\n  white-space: nowrap;\n  color: var(--ion-color-main);\n  font-size: 12px;\n  font-weight: bold;\n}\n\n.main_content_div .slider_div {\n  background: #e1e9ee;\n  padding-top: 10px;\n}\n\n.main_content_div .slider_div ion-slides {\n  height: 200px;\n  margin-top: -35px;\n}\n\n.main_content_div .slider_div .slide_div {\n  display: flex;\n  flex-direction: column;\n  width: 90%;\n  align-items: center;\n  border: 1px solid lightgray;\n  background: white;\n  height: 125px;\n  justify-content: center;\n}\n\n.main_content_div .slider_div .slide_div .up_div {\n  display: flex;\n  flex-direction: row;\n  padding: 15px;\n}\n\n.main_content_div .slider_div .slide_div .img_div {\n  width: 40px;\n}\n\n.main_content_div .slider_div .slide_div .img_div .slider_img {\n  width: 30px;\n  height: 30px;\n  min-width: 30px;\n}\n\n.main_content_div .slider_div .slide_div .content_div {\n  margin-left: 10px;\n}\n\n.main_content_div .slider_div .slide_div .content_div .abs_lbl {\n  position: absolute;\n  left: 5px;\n  top: 0;\n  font-size: 12px;\n}\n\n.main_content_div .slider_div .slide_div .content_div .head_lbl {\n  display: block;\n  font-weight: 500;\n  text-align: left;\n  font-size: 14px;\n  -webkit-margin-after: 5px;\n          margin-block-end: 5px;\n}\n\n.main_content_div .slider_div .slide_div .content_div .small_lbl {\n  color: #707070;\n  text-align: left;\n  display: block;\n  font-size: 14px;\n}\n\n.main_content_div .slider_div .slide_div .lower_div {\n  border-top: 1px solid lightgray;\n  padding: 3px;\n  display: flex;\n  width: 100%;\n  justify-content: space-around;\n}\n\n.main_content_div .slider_div .slide_div .lower_div ion-button {\n  color: var(--ion-color-main);\n  font-weight: 600;\n}\n\n.main_content_div .div_card {\n  width: 100%;\n  background: white;\n  margin-bottom: 10px;\n  padding-left: 16px;\n  padding-right: 16px;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .div_card .head_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  padding-top: 10px;\n  padding-bottom: 10px;\n}\n\n.main_content_div .div_card .head_div .img_div {\n  height: 40px;\n  width: 40px;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover;\n  border-radius: 50%;\n  min-width: 40px;\n}\n\n.main_content_div .div_card .head_div .img_div .slider_img {\n  width: 40px;\n  height: 40px;\n  min-width: 40px;\n}\n\n.main_content_div .div_card .head_div .content_div {\n  width: 88%;\n  margin-left: 10px;\n  margin-top: -10px;\n  position: relative;\n}\n\n.main_content_div .div_card .head_div .content_div .head_lbl {\n  display: block;\n  font-weight: 500;\n  text-align: left;\n  font-size: 14px;\n  -webkit-margin-after: 5px;\n          margin-block-end: 5px;\n}\n\n.main_content_div .div_card .head_div .content_div .small_lbl {\n  color: #707070;\n  text-align: left;\n  display: block;\n  font-size: 14px;\n}\n\n.main_content_div .div_card .head_div .content_div .card_arrow {\n  position: absolute;\n  right: 0;\n}\n\n.main_content_div .div_card .desc_div .small_lbl {\n  color: #707070;\n  text-align: left;\n  display: block;\n  font-size: 14px;\n}\n\n.main_content_div .div_card .desc_div .see_lbl {\n  color: #707070;\n  text-align: right;\n  display: block;\n  font-size: 14px;\n  margin-bottom: 5px;\n}\n\n.main_content_div .div_card .card_image {\n  height: 200px;\n  width: 100%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.main_content_div .div_card .like_div {\n  padding-top: 10px;\n  padding-bottom: 5px;\n  display: flex;\n  justify-content: space-evenly;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .div_card .like_div img {\n  width: 20px;\n  margin-right: 5px;\n}\n\n.main_content_div .div_card .like_div ion-button {\n  color: gray;\n  font-size: 16px;\n}\n\n.main_content_div .div_card .like_div2 {\n  padding-top: 10px;\n  padding-bottom: 5px;\n  display: flex;\n  justify-content: space-between;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .div_card .like_div2 img {\n  width: 20px;\n  margin-right: 5px;\n}\n\n.main_content_div .div_card .like_div2 ion-button {\n  color: gray;\n  font-size: 16px;\n}\n\n.main_content_div .masonry {\n  /* Masonry container */\n  -moz-column-count: 4;\n  column-count: 2;\n  -moz-column-gap: 1em;\n  column-gap: 1em;\n  margin: 0px;\n  padding: 0;\n  -moz-column-gap: 1.5em;\n  column-gap: 3px;\n  font-size: 0.85em;\n}\n\n.main_content_div .item {\n  display: inline-block;\n  background: #fff;\n  padding: 0px;\n  margin: 0px;\n  width: 100%;\n  box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  -webkit-box-sizing: border-box;\n}\n\n.main_content_div .item img {\n  max-width: 100%;\n}\n\nion-slides {\n  --bullet-background-active: black;\n  --bullet-background: transparent;\n}\n\n.swiper-pagination-bullet {\n  border: 1px solid;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbmV3cy1mZWVkL0Q6XFxSYWd1dmFyYW4gaW1hZ2VcXGRpc2Vsc2hpcC9zcmNcXGFwcFxccGFnZXNcXG5ld3MtZmVlZFxcbmV3cy1mZWVkLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbmV3cy1mZWVkL25ld3MtZmVlZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQ0FBQTtBQ0NKOztBRENBO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtBQ0VKOztBREFBO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNHSjs7QURESTtFQUNJLFlBQUE7RUFDQSxXQUFBO0FDR1I7O0FEREk7RUFDSSxtQkFBQTtFQUNBLDRCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNHUjs7QURBQTtFQUVJLGVBQUE7RUFFQSxhQUFBO0FDQ0o7O0FEQ0E7RUFDSSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxpQ0FBQTtBQ0VKOztBREFBO0VBQ0ksdUNBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLHFCQUFBO0FDR0o7O0FEREE7RUFDSSxxQkFBQTtBQ0lKOztBREZBO0VBQ0kseUJBQUE7QUNLSjs7QURKQTtFQUNRLFdBQUE7QUNPUjs7QUROQTtFQUNJLFdBQUE7QUNTSjs7QURSSTtFQUNJLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNVUjs7QURSSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtBQ1VSOztBRFBRO0VBQ0ksdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDU1o7O0FETEk7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0FDT1I7O0FETFE7RUFDSSxhQUFBO0VBQ0EsaUJBQUE7QUNPWjs7QURKUTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0VBRUEsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7QUNLWjs7QURIWTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7QUNLaEI7O0FERlk7RUFDSSxXQUFBO0FDSWhCOztBREZnQjtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQ0lwQjs7QUREWTtFQUNJLGlCQUFBO0FDR2hCOztBREFnQjtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLE1BQUE7RUFDQSxlQUFBO0FDRXBCOztBREFnQjtFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO1VBQUEscUJBQUE7QUNFcEI7O0FEQWdCO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNFcEI7O0FEQ1k7RUFDSSwrQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDZCQUFBO0FDQ2hCOztBRENnQjtFQUNJLDRCQUFBO0VBQ0EsZ0JBQUE7QUNDcEI7O0FES0k7RUFDSSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQ0FBQTtBQ0hSOztBRE1RO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7QUNKWjs7QURNWTtFQUdJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsNEJBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDTmhCOztBRE9nQjtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQ0xwQjs7QURRWTtFQUNJLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNOaEI7O0FET2dCO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7VUFBQSxxQkFBQTtBQ0xwQjs7QURPZ0I7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQ0xwQjs7QURPZ0I7RUFDSSxrQkFBQTtFQUNBLFFBQUE7QUNMcEI7O0FEVVk7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQ1JoQjs7QURVWTtFQUNJLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNSaEI7O0FEV1E7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQ1RaOztBRFlRO0VBQ0ksaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSw2QkFBQTtFQUNBLGtDQUFBO0FDVlo7O0FEV1k7RUFDSSxXQUFBO0VBQ0EsaUJBQUE7QUNUaEI7O0FEWVk7RUFDSSxXQUFBO0VBQ0EsZUFBQTtBQ1ZoQjs7QURhUztFQUNHLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQ0FBQTtBQ1haOztBRFlZO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0FDVmhCOztBRGFZO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUNYaEI7O0FENEJJO0VBQVcsc0JBQUE7RUFFUCxvQkFBQTtFQUNBLGVBQUE7RUFFQSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLHNCQUFBO0VBRUEsZUFBQTtFQUNBLGlCQUFBO0FDekJSOztBRDJCSTtFQUNJLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFFQSxzQkFBQTtFQUNBLDJCQUFBO0VBQ0EsOEJBQUE7QUMxQlI7O0FENEJJO0VBQVUsZUFBQTtBQ3pCZDs7QUQ4QkE7RUFDSSxpQ0FBQTtFQUNBLGdDQUFBO0FDM0JKOztBRDhCQTtFQUNJLGlCQUFBO0FDM0JKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbmV3cy1mZWVkL25ld3MtZmVlZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaHtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG4uaGVhZGVyX2RpdntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyOyAgICBcbiAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG5cbiAgICAudXNlcl9pbWFnZXtcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgICAgICB3aWR0aDogMjVweDtcbiAgICB9XG4gICAgaW9uLXNlYXJjaGJhcntcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTsgICBcbiAgICAgICAgLS1wbGFjZWhvbGRlci1jb2xvcjogICM3MDcwNzA7IFxuICAgICAgICAtLWljb24tY29sb3IgOiAjNzA3MDcwO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgICAgIGhlaWdodDogMzBweDtcbiAgICB9XG59XG4uZmxleF9kaXZ7XG4gICBcbiAgICBmb250LXNpemU6IDEycHg7XG5cbiAgICBkaXNwbGF5OiBmbGV4O1xufVxuLmZpbHRlcmJ0bjJ7XG4gICAgcGFkZGluZzogN3B4O1xuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIG1hcmdpbjogNHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG4uZmlsdGVyYnRue1xuICAgIGJvcmRlcjogMnB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICBwYWRkaW5nOiA3cHg7XG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgbWFyZ2luOiA0cHg7XG4gICAgYmFja2dyb3VuZDogYWxpY2VibHVlO1xufVxuaW9uLWNvbnRlbnR7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRjVGNUY1O1xufVxuLnRydWV7XG4gICAgY29sb3I6ICMwNDdlYWQgIWltcG9ydGFudDt9XG4uZmFsc2V7XG4gICAgICAgIGNvbG9yOiBncmF5O31cbi5tYWluX2NvbnRlbnRfZGl2e1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIC51cF9sYmx7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMTZweDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZTFlOWVlO1xuICAgIH1cbiAgICAuY2hpcHNfZGl2e1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBvdmVyZmxvdzogc2Nyb2xsO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgICAgIGJhY2tncm91bmQ6ICNlMWU5ZWU7XG4gICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcblxuXG4gICAgICAgIC5jaGlwe1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICBwYWRkaW5nOiA1cHggMjBweDtcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuc2xpZGVyX2RpdntcbiAgICAgICAgYmFja2dyb3VuZDogI2UxZTllZTsgXG4gICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuXG4gICAgICAgIGlvbi1zbGlkZXN7XG4gICAgICAgICAgICBoZWlnaHQ6IDIwMHB4O1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTM1cHg7XG4gICAgICAgIH1cblxuICAgICAgICAuc2xpZGVfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICB3aWR0aDogOTAlO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgICAgIC8vIHBhZGRpbmc6IDIwcHggMTVweDtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICAgICAgaGVpZ2h0OiAxMjVweDtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgICAgICAgICAudXBfZGl2e1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAxNXB4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAuaW1nX2RpdntcbiAgICAgICAgICAgICAgICB3aWR0aDogNDBweDtcbiAgICAgICAgICAgICAgICAvLyBwYWRkaW5nOiAxNXB4O1xuICAgICAgICAgICAgICAgIC5zbGlkZXJfaW1ne1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMzBweDtcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICAgICAgICAgICAgICBtaW4td2lkdGg6IDMwcHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmNvbnRlbnRfZGl2e1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgIC8vIHBhZGRpbmc6IDE1cHg7XG5cbiAgICAgICAgICAgICAgICAuYWJzX2xibHtcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgICAgICBsZWZ0OiA1cHg7XG4gICAgICAgICAgICAgICAgICAgIHRvcDogMDtcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAuaGVhZF9sYmx7XG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1ibG9jay1lbmQ6IDVweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLnNtYWxsX2xibHtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM3MDcwNzA7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmxvd2VyX2RpdntcbiAgICAgICAgICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDNweDtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuXG4gICAgICAgICAgICAgICAgaW9uLWJ1dHRvbntcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuZGl2X2NhcmR7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuXG5cbiAgICAgICAgLmhlYWRfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XG5cbiAgICAgICAgICAgIC5pbWdfZGl2e1xuICAgICAgICAgICAgICAgIC8vIHdpZHRoOiAxMiU7XG5cbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDQwcHg7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgbWluLXdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgICAgIC5zbGlkZXJfaW1ne1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogNDBweDtcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgICAgICAgICBtaW4td2lkdGg6IDQwcHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmNvbnRlbnRfZGl2e1xuICAgICAgICAgICAgICAgIHdpZHRoOiA4OCU7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogLTEwcHg7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgICAgIC5oZWFkX2xibHtcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJsb2NrLWVuZDogNXB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAuc21hbGxfbGJse1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzcwNzA3MDtcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLmNhcmRfYXJyb3d7XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5kZXNjX2RpdntcbiAgICAgICAgICAgIC5zbWFsbF9sYmx7XG4gICAgICAgICAgICAgICAgY29sb3I6ICM3MDcwNzA7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuc2VlX2xibHtcbiAgICAgICAgICAgICAgICBjb2xvcjogIzcwNzA3MDtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5jYXJkX2ltYWdle1xuICAgICAgICAgICAgaGVpZ2h0OiAyMDBweDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgIH1cblxuICAgICAgICAubGlrZV9kaXZ7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICAgICAgaW1ne1xuICAgICAgICAgICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogNXB4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgIC5saWtlX2RpdjJ7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgICAgIGltZ3tcbiAgICAgICAgICAgICAgICB3aWR0aDogMjBweDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaW9uLWJ1dHRvbiB7XG4gICAgICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gLm1hc29ucnkgeyAvKiBNYXNvbnJ5IGNvbnRhaW5lciAqL1xuICAgIC8vICAgICBjb2x1bW4tY291bnQ6IDQ7XG4gICAgLy8gICAgIGNvbHVtbi1nYXA6IDFlbTtcbiAgICAvLyAgIH1cbiAgICAgIFxuICAgIC8vICAgLml0ZW0geyAvKiBNYXNvbnJ5IGJyaWNrcyBvciBjaGlsZCBlbGVtZW50cyAqL1xuICAgIC8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xuICAgIC8vICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgLy8gICAgIG1hcmdpbjogMCAwIDFlbTtcbiAgICAvLyAgICAgd2lkdGg6IDEwMCU7XG4gICAgLy8gICB9XG5cbiAgICAubWFzb25yeSB7IC8qIE1hc29ucnkgY29udGFpbmVyICovXG4gICAgICAgIC13ZWJraXQtY29sdW1uLWNvdW50OiA0O1xuICAgICAgICAtbW96LWNvbHVtbi1jb3VudDo0O1xuICAgICAgICBjb2x1bW4tY291bnQ6IDI7XG4gICAgICAgIC13ZWJraXQtY29sdW1uLWdhcDogMWVtO1xuICAgICAgICAtbW96LWNvbHVtbi1nYXA6IDFlbTtcbiAgICAgICAgY29sdW1uLWdhcDogMWVtO1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgLW1vei1jb2x1bW4tZ2FwOiAxLjVlbTtcbiAgICAgICAgLXdlYmtpdC1jb2x1bW4tZ2FwOiAxLjVlbTtcbiAgICAgICAgY29sdW1uLWdhcDogM3B4O1xuICAgICAgICBmb250LXNpemU6IC44NWVtO1xuICAgIH1cbiAgICAuaXRlbSB7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICAgICAgcGFkZGluZzogMHB4O1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIC8vIC13ZWJraXQtdHJhbnNpdGlvbjoxcyBlYXNlIGFsbDtcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAgICAgLW1vei1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgICAgICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgfVxuICAgIC5pdGVtIGltZ3ttYXgtd2lkdGg6MTAwJTt9XG4gICAgICBcbn1cblxuXG5pb24tc2xpZGVze1xuICAgIC0tYnVsbGV0LWJhY2tncm91bmQtYWN0aXZlIDogYmxhY2s7XG4gICAgLS1idWxsZXQtYmFja2dyb3VuZCA6IHRyYW5zcGFyZW50OyAgIFxufVxuXG4uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0e1xuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xufVxuIiwiaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG4uaGVhZGVyX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgaGVpZ2h0OiA0MHB4O1xufVxuLmhlYWRlcl9kaXYgLnVzZXJfaW1hZ2Uge1xuICBoZWlnaHQ6IDI1cHg7XG4gIHdpZHRoOiAyNXB4O1xufVxuLmhlYWRlcl9kaXYgaW9uLXNlYXJjaGJhciB7XG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gIC0tcGxhY2Vob2xkZXItY29sb3I6ICM3MDcwNzA7XG4gIC0taWNvbi1jb2xvcjogIzcwNzA3MDtcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xuICBoZWlnaHQ6IDMwcHg7XG59XG5cbi5mbGV4X2RpdiB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZGlzcGxheTogZmxleDtcbn1cblxuLmZpbHRlcmJ0bjIge1xuICBwYWRkaW5nOiA3cHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgbWFyZ2luOiA0cHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuXG4uZmlsdGVyYnRuIHtcbiAgYm9yZGVyOiAycHggc29saWQgdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBwYWRkaW5nOiA3cHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgbWFyZ2luOiA0cHg7XG4gIGJhY2tncm91bmQ6IGFsaWNlYmx1ZTtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6ICNGNUY1RjU7XG59XG5cbi50cnVlIHtcbiAgY29sb3I6ICMwNDdlYWQgIWltcG9ydGFudDtcbn1cblxuLmZhbHNlIHtcbiAgY29sb3I6IGdyYXk7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXBfbGJsIHtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJhY2tncm91bmQ6ICNlMWU5ZWU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY2hpcHNfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgb3ZlcmZsb3c6IHNjcm9sbDtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICBiYWNrZ3JvdW5kOiAjZTFlOWVlO1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY2hpcHNfZGl2IC5jaGlwIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgcGFkZGluZzogNXB4IDIwcHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IHtcbiAgYmFja2dyb3VuZDogI2UxZTllZTtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2xpZGVyX2RpdiBpb24tc2xpZGVzIHtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgbWFyZ2luLXRvcDogLTM1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2xpZGVyX2RpdiAuc2xpZGVfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgd2lkdGg6IDkwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgaGVpZ2h0OiAxMjVweDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC51cF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAuaW1nX2RpdiB7XG4gIHdpZHRoOiA0MHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAuaW1nX2RpdiAuc2xpZGVyX2ltZyB7XG4gIHdpZHRoOiAzMHB4O1xuICBoZWlnaHQ6IDMwcHg7XG4gIG1pbi13aWR0aDogMzBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmNvbnRlbnRfZGl2IHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5jb250ZW50X2RpdiAuYWJzX2xibCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNXB4O1xuICB0b3A6IDA7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmNvbnRlbnRfZGl2IC5oZWFkX2xibCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi1ibG9jay1lbmQ6IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmNvbnRlbnRfZGl2IC5zbWFsbF9sYmwge1xuICBjb2xvcjogIzcwNzA3MDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmxvd2VyX2RpdiB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIHBhZGRpbmc6IDNweDtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAubG93ZXJfZGl2IGlvbi1idXR0b24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmhlYWRfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuaGVhZF9kaXYgLmltZ19kaXYge1xuICBoZWlnaHQ6IDQwcHg7XG4gIHdpZHRoOiA0MHB4O1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgbWluLXdpZHRoOiA0MHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5oZWFkX2RpdiAuaW1nX2RpdiAuc2xpZGVyX2ltZyB7XG4gIHdpZHRoOiA0MHB4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIG1pbi13aWR0aDogNDBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuaGVhZF9kaXYgLmNvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDg4JTtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIG1hcmdpbi10b3A6IC0xMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmhlYWRfZGl2IC5jb250ZW50X2RpdiAuaGVhZF9sYmwge1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tYmxvY2stZW5kOiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmhlYWRfZGl2IC5jb250ZW50X2RpdiAuc21hbGxfbGJsIHtcbiAgY29sb3I6ICM3MDcwNzA7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmhlYWRfZGl2IC5jb250ZW50X2RpdiAuY2FyZF9hcnJvdyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmRlc2NfZGl2IC5zbWFsbF9sYmwge1xuICBjb2xvcjogIzcwNzA3MDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuZGVzY19kaXYgLnNlZV9sYmwge1xuICBjb2xvcjogIzcwNzA3MDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuY2FyZF9pbWFnZSB7XG4gIGhlaWdodDogMjAwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmxpa2VfZGl2IHtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtZXZlbmx5O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5saWtlX2RpdiBpbWcge1xuICB3aWR0aDogMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmxpa2VfZGl2IGlvbi1idXR0b24ge1xuICBjb2xvcjogZ3JheTtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5saWtlX2RpdjIge1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5saWtlX2RpdjIgaW1nIHtcbiAgd2lkdGg6IDIwcHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5saWtlX2RpdjIgaW9uLWJ1dHRvbiB7XG4gIGNvbG9yOiBncmF5O1xuICBmb250LXNpemU6IDE2cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAubWFzb25yeSB7XG4gIC8qIE1hc29ucnkgY29udGFpbmVyICovXG4gIC13ZWJraXQtY29sdW1uLWNvdW50OiA0O1xuICAtbW96LWNvbHVtbi1jb3VudDogNDtcbiAgY29sdW1uLWNvdW50OiAyO1xuICAtd2Via2l0LWNvbHVtbi1nYXA6IDFlbTtcbiAgLW1vei1jb2x1bW4tZ2FwOiAxZW07XG4gIGNvbHVtbi1nYXA6IDFlbTtcbiAgbWFyZ2luOiAwcHg7XG4gIHBhZGRpbmc6IDA7XG4gIC1tb3otY29sdW1uLWdhcDogMS41ZW07XG4gIC13ZWJraXQtY29sdW1uLWdhcDogMS41ZW07XG4gIGNvbHVtbi1nYXA6IDNweDtcbiAgZm9udC1zaXplOiAwLjg1ZW07XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaXRlbSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgcGFkZGluZzogMHB4O1xuICBtYXJnaW46IDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIC1tb3otYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xufVxuLm1haW5fY29udGVudF9kaXYgLml0ZW0gaW1nIHtcbiAgbWF4LXdpZHRoOiAxMDAlO1xufVxuXG5pb24tc2xpZGVzIHtcbiAgLS1idWxsZXQtYmFja2dyb3VuZC1hY3RpdmU6IGJsYWNrO1xuICAtLWJ1bGxldC1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/news-feed/news-feed.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/news-feed/news-feed.page.ts ***!
    \***************************************************/

  /*! exports provided: NewsFeedPage */

  /***/
  function srcAppPagesNewsFeedNewsFeedPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewsFeedPage", function () {
      return NewsFeedPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic-native/social-sharing/ngx */
    "./node_modules/@ionic-native/social-sharing/ngx/index.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic-native/native-page-transitions/ngx */
    "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var NewsFeedPage = /*#__PURE__*/function () {
      function NewsFeedPage(navCtrl, loadingController, nativePageTransitions, location, socialSharing, actionSheet, route, api, menuCtrl, router) {
        var _this = this;

        _classCallCheck(this, NewsFeedPage);

        this.navCtrl = navCtrl;
        this.loadingController = loadingController;
        this.nativePageTransitions = nativePageTransitions;
        this.location = location;
        this.socialSharing = socialSharing;
        this.actionSheet = actionSheet;
        this.route = route;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.router = router;
        this.user = this.api.getCurrentUser();
        this.posts = ["0"];
        this.fillter = "";
        this.searchtext = "";
        this.slideoptions = {
          slidesPerView: 1.2
        };
        this.friends = false;
        this.groups = false;
        this.favorites = false;
        this.allupdates = true;
        this.following = false;
        this.showSlider = false;
        this.chips = ['ADD PHOTOS', 'ADD CONNECTIONS', 'HASHTAGS FOLLOWED'];
        this.options = {
          direction: 'up',
          duration: 500,
          slowdownfactor: 3,
          slidePixels: 20,
          iosdelay: 100,
          androiddelay: 150,
          fixedPixelsTop: 0,
          fixedPixelsBottom: 60
        };
        this.route.queryParams.subscribe(function (data) {
          console.log("data.id", data.id);
          _this.grpid = data.id;
        });

        if (this.grpid) {
          this.api.getActivitydetails('group_id', this.grpid).subscribe(function (res) {
            _this.posts = res;
          });
          this.api.getGroupdetails(this.grpid).subscribe(function (res) {
            _this.data = res;
          });
        } else {
          this.api.getActivityall().subscribe(function (res) {
            _this.posts = res;
          });
        }
      }

      _createClass(NewsFeedPage, [{
        key: "fillternews",
        value: function fillternews(filter) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var _this2 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.loadingController.create({
                      message: 'Please wait...',
                      duration: 1500
                    });

                  case 2:
                    loading = _context.sent;
                    _context.next = 5;
                    return loading.present();

                  case 5:
                    if (filter == "friends") {
                      this.friends = true;
                      this.groups = false;
                      this.favorites = false;
                      this.allupdates = false;
                      this.following = false;
                    } else if (filter == "groups") {
                      this.friends = false;
                      this.groups = true;
                      this.favorites = false;
                      this.allupdates = false;
                      this.following = false;
                    } else if (filter == "favorites") {
                      this.friends = false;
                      this.groups = false;
                      this.favorites = true;
                      this.allupdates = false;
                      this.following = false;
                    } else if (filter == "reset") {
                      this.friends = false;
                      this.groups = false;
                      this.favorites = false;
                      this.allupdates = true;
                      this.following = false;
                    } else if (filter == "allupdates") {
                      this.friends = false;
                      this.groups = false;
                      this.favorites = false;
                      this.allupdates = true;
                      this.following = false;
                    }

                    if (filter == "following") {
                      this.friends = false;
                      this.groups = false;
                      this.favorites = false;
                      this.allupdates = false;
                      this.following = true;
                    }

                    if (filter != "allupdates" && filter != "reset") {
                      this.fillter = filter;

                      if (this.searchtext == "") {
                        this.api.getActivityall(1, filter, "").subscribe(function (res) {
                          _this2.posts = res;
                        });
                        console.log("filter", filter);
                      } else if (this.searchtext != "") {
                        this.api.getActivityall(1, filter, this.searchtext).subscribe(function (res) {
                          _this2.posts = res;
                        });
                        console.log("filter", filter);
                      }
                    } else {
                      if (this.searchtext == "") {
                        this.fillter = "";
                        this.api.getActivityall().subscribe(function (res) {
                          _this2.posts = res;
                        });
                        console.log("filter11", filter);
                      } else if (this.searchtext != "") {
                        this.api.getActivityall(1, "", this.searchtext).subscribe(function (res) {
                          _this2.posts = res;
                        });
                      }
                    }

                  case 8:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "onSearchChange",
        value: function onSearchChange(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this3 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.loadingController.create({
                      message: 'Please wait...',
                      duration: 1500
                    });

                  case 2:
                    loading = _context2.sent;
                    _context2.next = 5;
                    return loading.present();

                  case 5:
                    if (this.searchtext != "" && this.fillter != "") {
                      this.api.getActivityall(1, this.fillter, this.searchtext).subscribe(function (res) {
                        _this3.posts = res;
                      });
                      console.log("searchtext", this.searchtext);
                      console.log("filter", this.fillter);
                    } else if (this.searchtext != "") {
                      this.api.getActivityall(1, "", this.searchtext).subscribe(function (res) {
                        _this3.posts = res;
                      });
                      console.log("searchtext", this.searchtext);
                    } else {
                      this.api.getActivityall().subscribe(function (res) {
                        _this3.posts = res;
                      });
                    }

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.nativePageTransitions.slide(this.options);
        } // example of adding a transition when pushing a new page

      }, {
        key: "openPage",
        value: function openPage(page) {
          this.nativePageTransitions.slide(this.options);
          this.navCtrl.navigateForward(page);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {// this.route.data.subscribe(routeData => {
          //   const data = routeData['data'];
          //     for (let post of data) {
          //       if (post['_embedded']['wp:featuredmedia']) {
          //         post.media_url =
          //           post['_embedded']['wp:featuredmedia'][0]['media_details'].sizes['medium'].source_url;
          //       }
          //     }
          //   this.posts = data.posts;
          //   this.categoryId = data.categoryId;
          //   this.categoryTitle = data.categoryTitle;
          // })
        }
      }, {
        key: "BackButton",
        value: function BackButton() {
          this.location.back();
        }
      }, {
        key: "sliderShow",
        value: function sliderShow(value) {
          this.showSlider = value;
        }
      }, {
        key: "loadData",
        value: function loadData(event) {
          var _this4 = this;

          var page = Math.ceil(this.posts.length / 10) + 1;

          if (this.searchtext != "" && this.fillter != "") {
            this.api.getActivityall(page, this.fillter, this.searchtext).subscribe(function (newPagePosts) {
              var _this4$posts;

              (_this4$posts = _this4.posts).push.apply(_this4$posts, _toConsumableArray(newPagePosts));

              event.target.complete();
            }, function (err) {
              // there are no more posts available
              event.target.disabled = true;
            });
            console.log("searchtext", this.searchtext);
            console.log("filter", this.fillter);
          } else if (this.searchtext != "") {
            this.api.getActivityall(page, "", this.searchtext).subscribe(function (newPagePosts) {
              var _this4$posts2;

              (_this4$posts2 = _this4.posts).push.apply(_this4$posts2, _toConsumableArray(newPagePosts));

              event.target.complete();
            }, function (err) {
              // there are no more posts available
              event.target.disabled = true;
            });
            console.log("searchtext", this.searchtext);
          } else {
            this.api.getActivityall(page).subscribe(function (newPagePosts) {
              var _this4$posts3;

              (_this4$posts3 = _this4.posts).push.apply(_this4$posts3, _toConsumableArray(newPagePosts));

              event.target.complete();
            }, function (err) {
              // there are no more posts available
              event.target.disabled = true;
            });
          }
        }
      }, {
        key: "ionViewDidLoad",
        value: function ionViewDidLoad() {
          var _this5 = this;

          this.api.retrieveCategories().subscribe(function (results) {
            _this5.categories = results;
          });
        }
      }, {
        key: "singlefeed",
        value: function singlefeed(idd) {
          var index = this.posts.find(function (_ref) {
            var id = _ref.id;
            return id === idd;
          });
          if (this.grpid) index.media_url = this.data.media_url;
          var navigationExtras = {
            queryParams: {
              special: JSON.stringify(index)
            }
          };
          this.router.navigate(['single-feed'], navigationExtras);
        }
      }, {
        key: "likefeed",
        value: function likefeed(idd) {
          var _this6 = this;

          this.api.activitylike(idd).subscribe(function (res) {
            console.log(res);

            var index = _this6.posts.findIndex(function (_ref2) {
              var id = _ref2.id;
              return id === res.id;
            });

            if (typeof index !== 'undefined') _this6.posts.splice(index, 1, res);
            console.log(_this6.posts);
          });
        }
      }, {
        key: "openActionSheet",
        value: function openActionSheet() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var actionSheet;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.actionSheet.create({
                      mode: 'md',
                      buttons: [{
                        text: 'Save',
                        role: 'destructive',
                        icon: 'bookmark-outline',
                        handler: function handler() {
                          console.log('Delete clicked');
                        }
                      }, {
                        text: 'Send in a private Message',
                        icon: 'chatbox-ellipses-outline',
                        handler: function handler() {
                          console.log('Share clicked');
                        }
                      }, {
                        text: 'Share via',
                        icon: 'share-social',
                        handler: function handler() {
                          console.log('Play clicked');
                        }
                      }, {
                        text: 'Hide this post',
                        icon: 'close-circle-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Unfollow',
                        icon: 'person-remove-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Report this post',
                        icon: 'flag-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Improve my feed',
                        icon: 'options-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Who can see this post?',
                        icon: 'eye-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }]
                    });

                  case 2:
                    actionSheet = _context3.sent;
                    _context3.next = 5;
                    return actionSheet.present();

                  case 5:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "openShareSheet",
        value: function openShareSheet(idd) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var _this7 = this;

            var data, actionSheet;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    data = this.posts.find(function (_ref3) {
                      var id = _ref3.id;
                      return id === idd;
                    });
                    _context4.next = 3;
                    return this.actionSheet.create({
                      mode: 'md',
                      buttons: [{
                        text: 'Share as Text',
                        icon: 'share-social-outline',
                        handler: function handler() {
                          console.log('Delete clicked');
                          if (typeof data !== 'undefined') _this7.socialSharing.share(data.content_stripped, 'MEDIUM', null, data.title);
                        }
                      }, {
                        text: 'Send Mail',
                        icon: 'mail-outline',
                        handler: function handler() {
                          console.log('Share clicked');

                          _this7.socialSharing.shareViaEmail(data.content_stripped, data.title, [_this7.user.user_email]);
                        }
                      }, {
                        text: 'Share via Facebook',
                        icon: 'logo-facebook',
                        handler: function handler() {
                          console.log('facebook clicked');

                          _this7.socialSharing.shareViaFacebookWithPasteMessageHint(data.content_stripped, _this7.data.media_url, data.link);
                        }
                      }, {
                        text: 'Share via Whatsapp',
                        icon: 'logo-whatsapp',
                        handler: function handler() {
                          if (typeof data !== 'undefined') _this7.socialSharing.shareViaWhatsApp(data.content_stripped, _this7.data.media_url, data.link);
                          console.log('whatsapp clicked');
                        }
                      }, {
                        text: 'Share via Twitter',
                        icon: 'logo-twitter',
                        handler: function handler() {
                          if (typeof data !== 'undefined') _this7.socialSharing.shareViaTwitter(data.content_stripped, _this7.data.media_url, data.link);
                        }
                      }, {
                        text: 'Share via Instagram',
                        icon: 'logo-instagram',
                        handler: function handler() {
                          if (typeof data !== 'undefined') _this7.socialSharing.shareViaTwitter(data.content_stripped, _this7.data.media_url);
                        }
                      }]
                    });

                  case 3:
                    actionSheet = _context4.sent;
                    _context4.next = 6;
                    return actionSheet.present();

                  case 6:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "openMenu",
        value: function openMenu() {
          this.menuCtrl.open();
        }
      }, {
        key: "goTonewpost",
        value: function goTonewpost() {
          this.router.navigate(['/new-post']);
        }
      }, {
        key: "goTosinglePost",
        value: function goTosinglePost() {
          this.router.navigate(['/single-post']);
        }
      }]);

      return NewsFeedPage;
    }();

    NewsFeedPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"]
      }, {
        type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_6__["NativePageTransitions"]
      }, {
        type: _angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"]
      }, {
        type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_4__["SocialSharing"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ActionSheetController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }];
    };

    NewsFeedPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-news-feed',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./news-feed.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/news-feed/news-feed.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./news-feed.page.scss */
      "./src/app/pages/news-feed/news-feed.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"], _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_6__["NativePageTransitions"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"], _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_4__["SocialSharing"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ActionSheetController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])], NewsFeedPage);
    /***/
  },

  /***/
  "./src/app/pages/news-feed/news-feed.resolver.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/news-feed/news-feed.resolver.ts ***!
    \*******************************************************/

  /*! exports provided: PostsResolver */

  /***/
  function srcAppPagesNewsFeedNewsFeedResolverTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PostsResolver", function () {
      return PostsResolver;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var PostsResolver = /*#__PURE__*/function () {
      function PostsResolver(api) {
        _classCallCheck(this, PostsResolver);

        this.api = api;
      }

      _createClass(PostsResolver, [{
        key: "resolve",
        value: function resolve(route) {
          var categoryId = route.queryParams['categoryId'];
          var categoryTitle = route.queryParams['title'];
          return this.api.getRecentPosts(categoryId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (posts) {
            return {
              posts: posts,
              categoryTitle: categoryTitle,
              categoryId: categoryId
            };
          }));
        }
      }]);

      return PostsResolver;
    }();

    PostsResolver.ctorParameters = function () {
      return [{
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_2__["apiService"]
      }];
    };

    PostsResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_2__["apiService"]])], PostsResolver);
    /***/
  }
}]);
//# sourceMappingURL=pages-news-feed-news-feed-module-es5.js.map
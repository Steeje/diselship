function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-group-feed-group-feed-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/group-feed/group-feed.page.html":
  /*!*********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/group-feed/group-feed.page.html ***!
    \*********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesGroupFeedGroupFeedPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n      <ion-button (click)=\"BackButton()\" slot=\"start\">\n        <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n      </ion-button>\n      <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar>\n      <ion-icon name=\"chatbubbles\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\" (click)=\"goToChatList()\"></ion-icon>\n    </div>\n    <!-- <ion-title *ngIf=\"!categoryTitle\">Recent posts</ion-title> -->\n    <ion-title *ngIf=\"categoryTitle\">{{categoryTitle}} posts</ion-title>\n    <ion-buttons slot=\"start\" *ngIf=\"categoryTitle\">\n      <ion-back-button defaultHref=\"posts\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <img style=\"width: -webkit-fill-available;\" *ngIf=\"!posts.length>0\" src=\"assets/imgs/no_result.jpg\">\n  <div *ngIf=\"posts.length>0\" class=\"main_content_div\">\n\n   \n\n\n    \n    <div style=\"margin-top: 10px;\">\n      \n        <!-- Card 1 [routerLink]=\"['/post', post.id]\"  -->\n        <ion-content class=\"page-content\"></ion-content>\n        <div class=\"div_card\" *ngFor=\"let post of posts\" >\n          <div  class=\"head_div\">\n\n            <div class=\"img_div\" [routerLink]=\"['/members-detail', post.user_id]\" [style.backgroundImage]=\"'url('+post.user_avatar+')'\"></div>\n\n            <div class=\"content_div\" (click)=\"singlefeed(post.id)\">\n              <p *ngIf=\"post.discussion!='' && grpid\" class=\"head_lbl\">{{post.name}} <ion-label style=\"color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.type_name}}</ion-label> \n                {{post.discussion}} <ion-label style=\"color: #959595; margin-right: 8px;\" class=\"head_type\">in the group</ion-label> <img src=\"{{data.media_url}}\" [routerLink]=\"['/group-detail', grpid]\" style=\"margin: -5px; border-radius: 10px;\"  width=\"20\" height=\"20\" alt=\"Group logo of {{post.group_name}}\">\n               <ion-label style=\" margin-left: 8px;\" [routerLink]=\"['/group-detail', grpid]\">{{post.group_name}}</ion-label></p>\n\n\n              <p *ngIf=\"post.discussion=='' && grpid \" class=\"head_lbl\">{{post.name}} <ion-label style=\"color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.type_name}}</ion-label> \n              <img src=\"{{data.media_url}}\" style=\"margin: -5px; border-radius: 10px;\"  width=\"20\" height=\"20\" [routerLink]=\"['/group-detail', grpid]\" alt=\"Group logo of {{post.group_name}}\">\n              <ion-label style=\" margin-left: 8px;\" [routerLink]=\"['/group-detail', grpid]\">{{post.group_name}}</ion-label></p>\n\n\n\n              <p *ngIf=\"post.component=='bbpress' && !grpid\" class=\"head_lbl\">{{post.name}} <ion-label style=\"color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.type_name}}</ion-label> \n                {{post.discussion}} </p>\n\n\n              <p *ngIf=\"post.component=='groups' && !grpid\" class=\"head_lbl\">{{post.name}} <ion-label style=\"color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.type_name}}</ion-label> \n              <img src=\"{{post.media_url}}\" style=\"margin: -5px; border-radius: 10px;\" [routerLink]=\"['/group-detail', post.grpid]\" width=\"20\" height=\"20\" alt=\"Group logo of {{post.group_name}}\">\n              <ion-label style=\" margin-left: 8px;\" [routerLink]=\"['/group-detail', post.grpid]\">{{post.group_name}}</ion-label></p>\n              \n              <p *ngIf=\"post.component!='groups' && post.component!='bbpress' && !grpid\"  class=\"head_lbl\">{{post.name}} <ion-label style=\"color: #959595; margin-right: 8px;\" class=\"head_type\">{{post.title}}</ion-label> \n                </p>\n\n\n\n            </div>\n\n            <!-- <ion-buttons class=\"card_arrow\" (click)=\"openActionSheet()\">\n              <ion-button color=\"dark\">\n                <ion-icon name=\"chevron-down-outline\" mode=\"ios\"></ion-icon>\n              </ion-button>\n            </ion-buttons> -->\n          </div>\n          <ion-card-title (click)=\"singlefeed(post.id)\" style=\"    font-size: 16px;\n          margin-bottom: 10px;\" [innerHTML]=\"post.content_stripped\"></ion-card-title>\n          <!-- <div class=\"desc_div\">\n            <ion-label  [innerHTML]=\"post.excerpt.rendered\">\n              \n            </ion-label>\n\n\n           \n            <ion-label class=\"see_lbl\">See more</ion-label>\n           </div> -->\n           <div (click)=\"singlefeed(post.id)\" *ngIf=\"post.media\">\n<div *ngFor=\"let item of post.media\">\n          <div class=\"card_image\" [style.backgroundImage]=\"'url('+item+')'\"></div>\n\n        </div> </div>\n          <div class=\"like_div2\">\n            <div>\n              <img src=\"assets/imgs/like.png\" alt=\"\">\n              {{post.favorite_count}}\n            </div>\n            <div>{{post.comment_count}} Comments</div>\n          </div>\n\n          <div class=\"like_div\" style=\"padding: 0;border: none;\">\n            <div>\n              <ion-button  [class]=\"post.favorited\" fill=\"clear\" size=\"small\" (click)=\"likefeed(post.id)\">\n                <ion-icon name=\"thumbs-up-sharp\" style=\"margin-right:5px\"></ion-icon>Like\n              </ion-button>\n            </div>\n\n            <div>\n              <ion-button (click)=\"singlefeed(post.id)\"  fill=\"clear\" size=\"small\">\n                <ion-icon name=\"chatbox-ellipses-sharp\" style=\"margin-right:5px\"></ion-icon>Comment\n              </ion-button>\n            </div>\n            <div>\n              <ion-button (click)=\"openShareSheet(post.id)\" fill=\"clear\" size=\"small\">\n                <ion-icon name=\"share-social\" style=\"margin-right:5px\"></ion-icon>Share\n              </ion-button>\n            </div>\n\n          </div>\n        </div>\n     \n\n        <!-- card 2 -->\n <!--        <div class=\"div_card\">\n          <div class=\"head_div\">\n\n            <div class=\"img_div\" [style.backgroundImage]=\"'url(assets/imgs/user2.jpg)'\"></div>\n\n            <div class=\"content_div\">\n              <ion-label class=\"head_lbl\">John Doe</ion-label>\n              <ion-label class=\"small_lbl\">InitApps shop</ion-label>\n              <ion-label class=\"small_lbl\">1 w • Edited</ion-label>\n            </div>\n\n            <ion-buttons class=\"card_arrow\" (click)=\"openActionSheet()\">\n              <ion-button color=\"dark\">\n                <ion-icon name=\"chevron-down-outline\" mode=\"ios\"></ion-icon>\n              </ion-button>\n            </ion-buttons>\n          </div>\n\n          <div class=\"desc_div\">\n            <ion-label>\n              Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n              Lorem Ipsum has been the industry's standard dummy text\n            </ion-label>\n            <ion-label class=\"see_lbl\">See more</ion-label>\n          </div>\n\n          <div class=\"masonry\">\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid1.jpg\">\n            </div>\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid2.jpg\">\n            </div>\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid3.jpg\">\n            </div>\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid4.jpg\">\n            </div>\n            <div class=\"item\">\n              <img src=\"assets/imgs/grid/grid5.jpg\">\n            </div>\n          </div>\n\n          <div class=\"like_div2\">\n            <div>\n              <img src=\"assets/imgs/like.png\" alt=\"\">\n              <img src=\"assets/imgs/heart.png\" alt=\"\">\n            </div>\n            <div>25 Comments</div>\n          </div>\n\n          <div class=\"like_div\" style=\"padding: 0;border: none;\">\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"thumbs-up-sharp\" style=\"margin-right:5px\"></ion-icon>Like\n              </ion-button>\n            </div>\n\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"chatbox-ellipses-sharp\" style=\"margin-right:5px\"></ion-icon>Comment\n              </ion-button>\n            </div>\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"share-social\" style=\"margin-right:5px\"></ion-icon>Share\n              </ion-button>\n            </div>\n\n          </div>\n        </div>\n\n        Card 3 \n\n        <div class=\"div_card\">\n          <div class=\"head_div\">\n\n            <div class=\"img_div\" [style.backgroundImage]=\"'url(assets/imgs/user3.jpg)'\"></div>\n\n            <div class=\"content_div\">\n              <ion-label class=\"head_lbl\">John Doe</ion-label>\n              <ion-label class=\"small_lbl\">InitApps shop</ion-label>\n              <ion-label class=\"small_lbl\">1 w • Edited</ion-label>\n            </div>\n\n            <ion-buttons class=\"card_arrow\" (click)=\"openActionSheet()\">\n              <ion-button color=\"dark\">\n                <ion-icon name=\"chevron-down-outline\" mode=\"ios\"></ion-icon>\n              </ion-button>\n            </ion-buttons>\n          </div>\n\n          <div class=\"desc_div\">\n            <ion-label>\n              Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n              Lorem Ipsum has been the industry's standard dummy text\n            </ion-label>\n            <ion-label class=\"see_lbl\">See more</ion-label>\n          </div>\n\n          <video width=\"100%\" height=\"240\" controls poster=\"assets/imgs/grid/grid4.jpg\">\n            <source src=\"assets/imgs/video.mp4\" type=\"video/mp4\">\n          </video>\n\n          <div class=\"like_div2\">\n            <div>\n              <img src=\"assets/imgs/like.png\" alt=\"\">\n              <img src=\"assets/imgs/heart.png\" alt=\"\">\n            </div>\n            <div>25 Comments</div>\n          </div>\n\n          <div class=\"like_div\" style=\"padding: 0;border: none;\">\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"thumbs-up-sharp\"></ion-icon>Like\n              </ion-button>\n            </div>\n\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"chatbox-ellipses-sharp\"></ion-icon>Comment\n              </ion-button>\n            </div>\n            <div>\n              <ion-button fill=\"clear\" size=\"small\">\n                <ion-icon name=\"share-social\"></ion-icon>Share\n              </ion-button>\n            </div>\n\n          </div>\n        </div> -->\n      \n    </div>\n  </div>\n  <ion-infinite-scroll (ionInfinite)=\"loadData($event)\">\n    <ion-infinite-scroll-content\n    loadingSpinner=\"bubbles\"\n    loadingText=\"Loading more posts ...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/group-feed/group-feed-routing.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/pages/group-feed/group-feed-routing.module.ts ***!
    \***************************************************************/

  /*! exports provided: GroupFeedPageRoutingModule */

  /***/
  function srcAppPagesGroupFeedGroupFeedRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GroupFeedPageRoutingModule", function () {
      return GroupFeedPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _group_feed_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./group-feed.page */
    "./src/app/pages/group-feed/group-feed.page.ts");

    var routes = [{
      path: '',
      component: _group_feed_page__WEBPACK_IMPORTED_MODULE_3__["GroupFeedPage"]
    }];

    var GroupFeedPageRoutingModule = function GroupFeedPageRoutingModule() {
      _classCallCheck(this, GroupFeedPageRoutingModule);
    };

    GroupFeedPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], GroupFeedPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/group-feed/group-feed.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/group-feed/group-feed.module.ts ***!
    \*******************************************************/

  /*! exports provided: GroupFeedPageModule */

  /***/
  function srcAppPagesGroupFeedGroupFeedModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GroupFeedPageModule", function () {
      return GroupFeedPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _group_feed_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./group-feed-routing.module */
    "./src/app/pages/group-feed/group-feed-routing.module.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _group_feed_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./group-feed.page */
    "./src/app/pages/group-feed/group-feed.page.ts");
    /* harmony import */


    var _group_feed_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./group-feed.resolver */
    "./src/app/pages/group-feed/group-feed.resolver.ts");

    var routes = [{
      path: '',
      component: _group_feed_page__WEBPACK_IMPORTED_MODULE_7__["GroupFeedPage"],
      resolve: {
        data: _group_feed_resolver__WEBPACK_IMPORTED_MODULE_8__["PostsResolver"]
      },
      runGuardsAndResolvers: 'paramsOrQueryParamsChange' // because we use the same route for all posts and for category posts, we need to add this to refetch data 

    }];

    var GroupFeedPageModule = function GroupFeedPageModule() {
      _classCallCheck(this, GroupFeedPageModule);
    };

    GroupFeedPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _group_feed_routing_module__WEBPACK_IMPORTED_MODULE_5__["GroupFeedPageRoutingModule"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(routes)],
      declarations: [_group_feed_page__WEBPACK_IMPORTED_MODULE_7__["GroupFeedPage"]],
      providers: [_group_feed_resolver__WEBPACK_IMPORTED_MODULE_8__["PostsResolver"]]
    })], GroupFeedPageModule);
    /***/
  },

  /***/
  "./src/app/pages/group-feed/group-feed.page.scss":
  /*!*******************************************************!*\
    !*** ./src/app/pages/group-feed/group-feed.page.scss ***!
    \*******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesGroupFeedGroupFeedPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\n.header_div {\n  display: flex;\n  width: 100%;\n  justify-content: space-between;\n  align-items: center;\n  padding-left: 16px;\n  padding-right: 16px;\n  height: 40px;\n}\n\n.header_div .user_image {\n  height: 25px;\n  width: 25px;\n}\n\n.header_div ion-searchbar {\n  --background: white;\n  --placeholder-color: #707070;\n  --icon-color: #707070;\n  border-radius: 0px;\n  height: 30px;\n}\n\nion-content {\n  --background: #F5F5F5;\n}\n\n.true {\n  color: #047ead !important;\n}\n\n.false {\n  color: gray;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div .up_lbl {\n  padding-left: 16px;\n  padding-right: 16px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n  background: #e1e9ee;\n}\n\n.main_content_div .chips_div {\n  display: flex;\n  flex-direction: row;\n  overflow: scroll;\n  padding-left: 16px;\n  padding-right: 16px;\n  background: #e1e9ee;\n  padding-top: 10px;\n  padding-bottom: 20px;\n}\n\n.main_content_div .chips_div .chip {\n  background-color: white;\n  border-radius: 3px;\n  padding: 5px 20px;\n  margin-right: 10px;\n  white-space: nowrap;\n  color: var(--ion-color-main);\n  font-size: 12px;\n  font-weight: bold;\n}\n\n.main_content_div .slider_div {\n  background: #e1e9ee;\n  padding-top: 10px;\n}\n\n.main_content_div .slider_div ion-slides {\n  height: 200px;\n  margin-top: -35px;\n}\n\n.main_content_div .slider_div .slide_div {\n  display: flex;\n  flex-direction: column;\n  width: 90%;\n  align-items: center;\n  border: 1px solid lightgray;\n  background: white;\n  height: 125px;\n  justify-content: center;\n}\n\n.main_content_div .slider_div .slide_div .up_div {\n  display: flex;\n  flex-direction: row;\n  padding: 15px;\n}\n\n.main_content_div .slider_div .slide_div .img_div {\n  width: 40px;\n}\n\n.main_content_div .slider_div .slide_div .img_div .slider_img {\n  width: 30px;\n  height: 30px;\n  min-width: 30px;\n}\n\n.main_content_div .slider_div .slide_div .content_div {\n  margin-left: 10px;\n}\n\n.main_content_div .slider_div .slide_div .content_div .abs_lbl {\n  position: absolute;\n  left: 5px;\n  top: 0;\n  font-size: 12px;\n}\n\n.main_content_div .slider_div .slide_div .content_div .head_lbl {\n  display: block;\n  font-weight: 500;\n  text-align: left;\n  font-size: 14px;\n  -webkit-margin-after: 5px;\n          margin-block-end: 5px;\n}\n\n.main_content_div .slider_div .slide_div .content_div .small_lbl {\n  color: #707070;\n  text-align: left;\n  display: block;\n  font-size: 14px;\n}\n\n.main_content_div .slider_div .slide_div .lower_div {\n  border-top: 1px solid lightgray;\n  padding: 3px;\n  display: flex;\n  width: 100%;\n  justify-content: space-around;\n}\n\n.main_content_div .slider_div .slide_div .lower_div ion-button {\n  color: var(--ion-color-main);\n  font-weight: 600;\n}\n\n.main_content_div .div_card {\n  width: 100%;\n  background: white;\n  margin-bottom: 10px;\n  padding-left: 16px;\n  padding-right: 16px;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .div_card .head_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  padding-top: 10px;\n  padding-bottom: 10px;\n}\n\n.main_content_div .div_card .head_div .img_div {\n  height: 40px;\n  width: 40px;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover;\n  border-radius: 50%;\n  min-width: 40px;\n}\n\n.main_content_div .div_card .head_div .img_div .slider_img {\n  width: 40px;\n  height: 40px;\n  min-width: 40px;\n}\n\n.main_content_div .div_card .head_div .content_div {\n  width: 88%;\n  margin-left: 10px;\n  margin-top: -10px;\n  position: relative;\n}\n\n.main_content_div .div_card .head_div .content_div .head_lbl {\n  display: block;\n  font-weight: 500;\n  text-align: left;\n  font-size: 14px;\n  -webkit-margin-after: 5px;\n          margin-block-end: 5px;\n}\n\n.main_content_div .div_card .head_div .content_div .small_lbl {\n  color: #707070;\n  text-align: left;\n  display: block;\n  font-size: 14px;\n}\n\n.main_content_div .div_card .head_div .content_div .card_arrow {\n  position: absolute;\n  right: 0;\n}\n\n.main_content_div .div_card .desc_div .small_lbl {\n  color: #707070;\n  text-align: left;\n  display: block;\n  font-size: 14px;\n}\n\n.main_content_div .div_card .desc_div .see_lbl {\n  color: #707070;\n  text-align: right;\n  display: block;\n  font-size: 14px;\n  margin-bottom: 5px;\n}\n\n.main_content_div .div_card .card_image {\n  height: 200px;\n  width: 100%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.main_content_div .div_card .like_div {\n  padding-top: 10px;\n  padding-bottom: 5px;\n  display: flex;\n  justify-content: space-evenly;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .div_card .like_div img {\n  width: 20px;\n  margin-right: 5px;\n}\n\n.main_content_div .div_card .like_div ion-button {\n  color: gray;\n  font-size: 16px;\n}\n\n.main_content_div .div_card .like_div2 {\n  padding-top: 10px;\n  padding-bottom: 5px;\n  display: flex;\n  justify-content: space-between;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .div_card .like_div2 img {\n  width: 20px;\n  margin-right: 5px;\n}\n\n.main_content_div .div_card .like_div2 ion-button {\n  color: gray;\n  font-size: 16px;\n}\n\n.main_content_div .masonry {\n  /* Masonry container */\n  -moz-column-count: 4;\n  column-count: 2;\n  -moz-column-gap: 1em;\n  column-gap: 1em;\n  margin: 0px;\n  padding: 0;\n  -moz-column-gap: 1.5em;\n  column-gap: 3px;\n  font-size: 0.85em;\n}\n\n.main_content_div .item {\n  display: inline-block;\n  background: #fff;\n  padding: 0px;\n  margin: 0px;\n  width: 100%;\n  box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  -webkit-box-sizing: border-box;\n}\n\n.main_content_div .item img {\n  max-width: 100%;\n}\n\nion-slides {\n  --bullet-background-active: black;\n  --bullet-background: transparent;\n}\n\n.swiper-pagination-bullet {\n  border: 1px solid;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZ3JvdXAtZmVlZC9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxncm91cC1mZWVkXFxncm91cC1mZWVkLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZ3JvdXAtZmVlZC9ncm91cC1mZWVkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1DQUFBO0FDQ0o7O0FEQ0E7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0FDRUo7O0FEQUE7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ0dKOztBRERJO0VBQ0ksWUFBQTtFQUNBLFdBQUE7QUNHUjs7QURESTtFQUNJLG1CQUFBO0VBQ0EsNEJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ0dSOztBRENBO0VBQ0kscUJBQUE7QUNFSjs7QURBQTtFQUNJLHlCQUFBO0FDR0o7O0FERkE7RUFDUSxXQUFBO0FDS1I7O0FESkE7RUFDSSxXQUFBO0FDT0o7O0FETkk7RUFDSSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDUVI7O0FETkk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7QUNRUjs7QURMUTtFQUNJLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSw0QkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ09aOztBREhJO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtBQ0tSOztBREhRO0VBQ0ksYUFBQTtFQUNBLGlCQUFBO0FDS1o7O0FERlE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtFQUVBLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0FDR1o7O0FERFk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0FDR2hCOztBREFZO0VBQ0ksV0FBQTtBQ0VoQjs7QURBZ0I7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNFcEI7O0FEQ1k7RUFDSSxpQkFBQTtBQ0NoQjs7QURFZ0I7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxNQUFBO0VBQ0EsZUFBQTtBQ0FwQjs7QURFZ0I7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSx5QkFBQTtVQUFBLHFCQUFBO0FDQXBCOztBREVnQjtFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDQXBCOztBREdZO0VBQ0ksK0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSw2QkFBQTtBQ0RoQjs7QURHZ0I7RUFDSSw0QkFBQTtFQUNBLGdCQUFBO0FDRHBCOztBRE9JO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0NBQUE7QUNMUjs7QURRUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0FDTlo7O0FEUVk7RUFHSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLDRCQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ1JoQjs7QURTZ0I7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNQcEI7O0FEVVk7RUFDSSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDUmhCOztBRFNnQjtFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO1VBQUEscUJBQUE7QUNQcEI7O0FEU2dCO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNQcEI7O0FEU2dCO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0FDUHBCOztBRFlZO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNWaEI7O0FEWVk7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDVmhCOztBRGFRO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUNYWjs7QURjUTtFQUNJLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxrQ0FBQTtBQ1paOztBRGFZO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0FDWGhCOztBRGNZO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUNaaEI7O0FEZVM7RUFDRyxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0VBQ0Esa0NBQUE7QUNiWjs7QURjWTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtBQ1poQjs7QURlWTtFQUNJLFdBQUE7RUFDQSxlQUFBO0FDYmhCOztBRDhCSTtFQUFXLHNCQUFBO0VBRVAsb0JBQUE7RUFDQSxlQUFBO0VBRUEsb0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxzQkFBQTtFQUVBLGVBQUE7RUFDQSxpQkFBQTtBQzNCUjs7QUQ2Qkk7RUFDSSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBRUEsc0JBQUE7RUFDQSwyQkFBQTtFQUNBLDhCQUFBO0FDNUJSOztBRDhCSTtFQUFVLGVBQUE7QUMzQmQ7O0FEZ0NBO0VBQ0ksaUNBQUE7RUFDQSxnQ0FBQTtBQzdCSjs7QURnQ0E7RUFDSSxpQkFBQTtBQzdCSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2dyb3VwLWZlZWQvZ3JvdXAtZmVlZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaHtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG4uaGVhZGVyX2RpdntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyOyAgICBcbiAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG5cbiAgICAudXNlcl9pbWFnZXtcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgICAgICB3aWR0aDogMjVweDtcbiAgICB9XG4gICAgaW9uLXNlYXJjaGJhcntcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTsgICBcbiAgICAgICAgLS1wbGFjZWhvbGRlci1jb2xvcjogICM3MDcwNzA7IFxuICAgICAgICAtLWljb24tY29sb3IgOiAjNzA3MDcwO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgICAgIGhlaWdodDogMzBweDtcbiAgICB9XG59XG5cbmlvbi1jb250ZW50e1xuICAgIC0tYmFja2dyb3VuZDogI0Y1RjVGNTtcbn1cbi50cnVle1xuICAgIGNvbG9yOiAjMDQ3ZWFkICFpbXBvcnRhbnQ7fVxuLmZhbHNle1xuICAgICAgICBjb2xvcjogZ3JheTt9XG4ubWFpbl9jb250ZW50X2RpdntcbiAgICB3aWR0aDogMTAwJTtcbiAgICAudXBfbGJse1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgYmFja2dyb3VuZDogI2UxZTllZTtcbiAgICB9XG4gICAgLmNoaXBzX2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgb3ZlcmZsb3c6IHNjcm9sbDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZTFlOWVlO1xuICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDIwcHg7XG5cblxuICAgICAgICAuY2hpcHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICAgICAgcGFkZGluZzogNXB4IDIwcHg7XG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLnNsaWRlcl9kaXZ7XG4gICAgICAgIGJhY2tncm91bmQ6ICNlMWU5ZWU7IFxuICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcblxuICAgICAgICBpb24tc2xpZGVze1xuICAgICAgICAgICAgaGVpZ2h0OiAyMDBweDtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC0zNXB4O1xuICAgICAgICB9XG5cbiAgICAgICAgLnNsaWRlX2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAgd2lkdGg6IDkwJTtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgICAgICAvLyBwYWRkaW5nOiAyMHB4IDE1cHg7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgICAgIGhlaWdodDogMTI1cHg7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAgICAgICAgICAgLnVwX2RpdntcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogMTVweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmltZ19kaXZ7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDQwcHg7XG4gICAgICAgICAgICAgICAgLy8gcGFkZGluZzogMTVweDtcbiAgICAgICAgICAgICAgICAuc2xpZGVyX2ltZ3tcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgICAgICAgICAgICAgbWluLXdpZHRoOiAzMHB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5jb250ZW50X2RpdntcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgICAgICAgICAvLyBwYWRkaW5nOiAxNXB4O1xuXG4gICAgICAgICAgICAgICAgLmFic19sYmx7XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICAgICAgbGVmdDogNXB4O1xuICAgICAgICAgICAgICAgICAgICB0b3A6IDA7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLmhlYWRfbGJse1xuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYmxvY2stZW5kOiA1cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5zbWFsbF9sYmx7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjNzA3MDcwO1xuICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5sb3dlcl9kaXZ7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAzcHg7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcblxuICAgICAgICAgICAgICAgIGlvbi1idXR0b257XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmRpdl9jYXJke1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMTZweDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcblxuXG4gICAgICAgIC5oZWFkX2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuXG4gICAgICAgICAgICAuaW1nX2RpdntcbiAgICAgICAgICAgICAgICAvLyB3aWR0aDogMTIlO1xuXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgICAgIG1pbi13aWR0aDogNDBweDtcbiAgICAgICAgICAgICAgICAuc2xpZGVyX2ltZ3tcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDQwcHg7XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgICAgICAgICAgICAgbWluLXdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5jb250ZW50X2RpdntcbiAgICAgICAgICAgICAgICB3aWR0aDogODglO1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IC0xMHB4O1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgICAgICAuaGVhZF9sYmx7XG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1ibG9jay1lbmQ6IDVweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLnNtYWxsX2xibHtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM3MDcwNzA7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5jYXJkX2Fycm93e1xuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgICAgIHJpZ2h0OiAwO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuZGVzY19kaXZ7XG4gICAgICAgICAgICAuc21hbGxfbGJse1xuICAgICAgICAgICAgICAgIGNvbG9yOiAjNzA3MDcwO1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnNlZV9sYmx7XG4gICAgICAgICAgICAgICAgY29sb3I6ICM3MDcwNzA7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuY2FyZF9pbWFnZXtcbiAgICAgICAgICAgIGhlaWdodDogMjAwcHg7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICB9XG5cbiAgICAgICAgLmxpa2VfZGl2e1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtZXZlbmx5O1xuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgICAgIGltZ3tcbiAgICAgICAgICAgICAgICB3aWR0aDogMjBweDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaW9uLWJ1dHRvbiB7XG4gICAgICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgICAubGlrZV9kaXYye1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgICAgICBpbWd7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlvbi1idXR0b24ge1xuICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIC5tYXNvbnJ5IHsgLyogTWFzb25yeSBjb250YWluZXIgKi9cbiAgICAvLyAgICAgY29sdW1uLWNvdW50OiA0O1xuICAgIC8vICAgICBjb2x1bW4tZ2FwOiAxZW07XG4gICAgLy8gICB9XG4gICAgICBcbiAgICAvLyAgIC5pdGVtIHsgLyogTWFzb25yeSBicmlja3Mgb3IgY2hpbGQgZWxlbWVudHMgKi9cbiAgICAvLyAgICAgYmFja2dyb3VuZC1jb2xvcjogI2VlZTtcbiAgICAvLyAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIC8vICAgICBtYXJnaW46IDAgMCAxZW07XG4gICAgLy8gICAgIHdpZHRoOiAxMDAlO1xuICAgIC8vICAgfVxuXG4gICAgLm1hc29ucnkgeyAvKiBNYXNvbnJ5IGNvbnRhaW5lciAqL1xuICAgICAgICAtd2Via2l0LWNvbHVtbi1jb3VudDogNDtcbiAgICAgICAgLW1vei1jb2x1bW4tY291bnQ6NDtcbiAgICAgICAgY29sdW1uLWNvdW50OiAyO1xuICAgICAgICAtd2Via2l0LWNvbHVtbi1nYXA6IDFlbTtcbiAgICAgICAgLW1vei1jb2x1bW4tZ2FwOiAxZW07XG4gICAgICAgIGNvbHVtbi1nYXA6IDFlbTtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgIC1tb3otY29sdW1uLWdhcDogMS41ZW07XG4gICAgICAgIC13ZWJraXQtY29sdW1uLWdhcDogMS41ZW07XG4gICAgICAgIGNvbHVtbi1nYXA6IDNweDtcbiAgICAgICAgZm9udC1zaXplOiAuODVlbTtcbiAgICB9XG4gICAgLml0ZW0ge1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgIHBhZGRpbmc6IDBweDtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAvLyAtd2Via2l0LXRyYW5zaXRpb246MXMgZWFzZSBhbGw7XG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgICAgIC1tb3otYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAgICAgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIH1cbiAgICAuaXRlbSBpbWd7bWF4LXdpZHRoOjEwMCU7fVxuICAgICAgXG59XG5cblxuaW9uLXNsaWRlc3tcbiAgICAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZSA6IGJsYWNrO1xuICAgIC0tYnVsbGV0LWJhY2tncm91bmQgOiB0cmFuc3BhcmVudDsgICBcbn1cblxuLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHtcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcbn1cbiIsImlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5cbmlvbi10b29sYmFyLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgsIGlvbi10b29sYmFyIC5zYy1pb24tc2VhcmNoYmFyLWlvcy1oIHtcbiAgcGFkZGluZy10b3A6IDBweDtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbn1cblxuLmhlYWRlcl9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIGhlaWdodDogNDBweDtcbn1cbi5oZWFkZXJfZGl2IC51c2VyX2ltYWdlIHtcbiAgaGVpZ2h0OiAyNXB4O1xuICB3aWR0aDogMjVweDtcbn1cbi5oZWFkZXJfZGl2IGlvbi1zZWFyY2hiYXIge1xuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLXBsYWNlaG9sZGVyLWNvbG9yOiAjNzA3MDcwO1xuICAtLWljb24tY29sb3I6ICM3MDcwNzA7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgaGVpZ2h0OiAzMHB4O1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogI0Y1RjVGNTtcbn1cblxuLnRydWUge1xuICBjb2xvcjogIzA0N2VhZCAhaW1wb3J0YW50O1xufVxuXG4uZmFsc2Uge1xuICBjb2xvcjogZ3JheTtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51cF9sYmwge1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYmFja2dyb3VuZDogI2UxZTllZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jaGlwc19kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBvdmVyZmxvdzogc2Nyb2xsO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIGJhY2tncm91bmQ6ICNlMWU5ZWU7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jaGlwc19kaXYgLmNoaXAge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICBwYWRkaW5nOiA1cHggMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYge1xuICBiYWNrZ3JvdW5kOiAjZTFlOWVlO1xuICBwYWRkaW5nLXRvcDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IGlvbi1zbGlkZXMge1xuICBoZWlnaHQ6IDIwMHB4O1xuICBtYXJnaW4tdG9wOiAtMzVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IC5zbGlkZV9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB3aWR0aDogOTAlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEyNXB4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLnVwX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5pbWdfZGl2IHtcbiAgd2lkdGg6IDQwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5pbWdfZGl2IC5zbGlkZXJfaW1nIHtcbiAgd2lkdGg6IDMwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgbWluLXdpZHRoOiAzMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAuY29udGVudF9kaXYge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IC5zbGlkZV9kaXYgLmNvbnRlbnRfZGl2IC5hYnNfbGJsIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1cHg7XG4gIHRvcDogMDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAuY29udGVudF9kaXYgLmhlYWRfbGJsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLWJsb2NrLWVuZDogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAuY29udGVudF9kaXYgLnNtYWxsX2xibCB7XG4gIGNvbG9yOiAjNzA3MDcwO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYgLnNsaWRlX2RpdiAubG93ZXJfZGl2IHtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgcGFkZGluZzogM3B4O1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2xpZGVyX2RpdiAuc2xpZGVfZGl2IC5sb3dlcl9kaXYgaW9uLWJ1dHRvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQge1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuaGVhZF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5oZWFkX2RpdiAuaW1nX2RpdiB7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDQwcHg7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBtaW4td2lkdGg6IDQwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmhlYWRfZGl2IC5pbWdfZGl2IC5zbGlkZXJfaW1nIHtcbiAgd2lkdGg6IDQwcHg7XG4gIGhlaWdodDogNDBweDtcbiAgbWluLXdpZHRoOiA0MHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5oZWFkX2RpdiAuY29udGVudF9kaXYge1xuICB3aWR0aDogODglO1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgbWFyZ2luLXRvcDogLTEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuaGVhZF9kaXYgLmNvbnRlbnRfZGl2IC5oZWFkX2xibCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi1ibG9jay1lbmQ6IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuaGVhZF9kaXYgLmNvbnRlbnRfZGl2IC5zbWFsbF9sYmwge1xuICBjb2xvcjogIzcwNzA3MDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuaGVhZF9kaXYgLmNvbnRlbnRfZGl2IC5jYXJkX2Fycm93IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAuZGVzY19kaXYgLnNtYWxsX2xibCB7XG4gIGNvbG9yOiAjNzA3MDcwO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5kZXNjX2RpdiAuc2VlX2xibCB7XG4gIGNvbG9yOiAjNzA3MDcwO1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRpdl9jYXJkIC5jYXJkX2ltYWdlIHtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAubGlrZV9kaXYge1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmxpa2VfZGl2IGltZyB7XG4gIHdpZHRoOiAyMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kaXZfY2FyZCAubGlrZV9kaXYgaW9uLWJ1dHRvbiB7XG4gIGNvbG9yOiBncmF5O1xuICBmb250LXNpemU6IDE2cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmxpa2VfZGl2MiB7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmxpa2VfZGl2MiBpbWcge1xuICB3aWR0aDogMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGl2X2NhcmQgLmxpa2VfZGl2MiBpb24tYnV0dG9uIHtcbiAgY29sb3I6IGdyYXk7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5tYXNvbnJ5IHtcbiAgLyogTWFzb25yeSBjb250YWluZXIgKi9cbiAgLXdlYmtpdC1jb2x1bW4tY291bnQ6IDQ7XG4gIC1tb3otY29sdW1uLWNvdW50OiA0O1xuICBjb2x1bW4tY291bnQ6IDI7XG4gIC13ZWJraXQtY29sdW1uLWdhcDogMWVtO1xuICAtbW96LWNvbHVtbi1nYXA6IDFlbTtcbiAgY29sdW1uLWdhcDogMWVtO1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMDtcbiAgLW1vei1jb2x1bW4tZ2FwOiAxLjVlbTtcbiAgLXdlYmtpdC1jb2x1bW4tZ2FwOiAxLjVlbTtcbiAgY29sdW1uLWdhcDogM3B4O1xuICBmb250LXNpemU6IDAuODVlbTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5pdGVtIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBwYWRkaW5nOiAwcHg7XG4gIG1hcmdpbjogMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgLW1vei1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaXRlbSBpbWcge1xuICBtYXgtd2lkdGg6IDEwMCU7XG59XG5cbmlvbi1zbGlkZXMge1xuICAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZTogYmxhY2s7XG4gIC0tYnVsbGV0LWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0IHtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/pages/group-feed/group-feed.page.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/group-feed/group-feed.page.ts ***!
    \*****************************************************/

  /*! exports provided: GroupFeedPage */

  /***/
  function srcAppPagesGroupFeedGroupFeedPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GroupFeedPage", function () {
      return GroupFeedPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic-native/social-sharing/ngx */
    "./node_modules/@ionic-native/social-sharing/ngx/index.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic-native/native-page-transitions/ngx */
    "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var GroupFeedPage = /*#__PURE__*/function () {
      function GroupFeedPage(navCtrl, nativePageTransitions, location, socialSharing, actionSheet, route, api, menuCtrl, router) {
        _classCallCheck(this, GroupFeedPage);

        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.location = location;
        this.socialSharing = socialSharing;
        this.actionSheet = actionSheet;
        this.route = route;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.router = router;
        this.user = this.api.getCurrentUser();
        this.posts = ["0"];
        this.slideoptions = {
          slidesPerView: 1.2
        };
        this.showSlider = false;
        this.chips = ['ADD PHOTOS', 'ADD CONNECTIONS', 'HASHTAGS FOLLOWED'];
        this.options = {
          direction: 'up',
          duration: 500,
          slowdownfactor: 3,
          slidePixels: 20,
          iosdelay: 100,
          androiddelay: 150,
          fixedPixelsTop: 0,
          fixedPixelsBottom: 60
        };
        this.posts.avatar_urls.thumb = "";
      }

      _createClass(GroupFeedPage, [{
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.nativePageTransitions.slide(this.options);
        } // example of adding a transition when pushing a new page

      }, {
        key: "openPage",
        value: function openPage(page) {
          this.nativePageTransitions.slide(this.options);
          this.navCtrl.navigateForward(page);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.route.queryParams.subscribe(function (data) {
            console.log("data.id", data.id);
            _this.grpid = data.id;
          });

          if (this.grpid) {
            this.api.getActivitydetails('group_id', this.grpid).subscribe(function (res) {
              _this.posts = res;
            });
            this.api.getGroupdetails(this.grpid).subscribe(function (res) {
              _this.data = res;
            });
          } else {
            this.api.getActivityall().subscribe(function (res) {
              _this.posts = res;
            });
          } // this.route.data.subscribe(routeData => {
          //   const data = routeData['data'];
          //     for (let post of data) {
          //       if (post['_embedded']['wp:featuredmedia']) {
          //         post.media_url =
          //           post['_embedded']['wp:featuredmedia'][0]['media_details'].sizes['medium'].source_url;
          //       }
          //     }
          //   this.posts = data.posts;
          //   this.categoryId = data.categoryId;
          //   this.categoryTitle = data.categoryTitle;
          // })

        }
      }, {
        key: "BackButton",
        value: function BackButton() {
          this.location.back();
        }
      }, {
        key: "sliderShow",
        value: function sliderShow(value) {
          this.showSlider = value;
        }
      }, {
        key: "loadData",
        value: function loadData(event) {
          var _this2 = this;

          var page = Math.ceil(this.posts.length / 10) + 1;

          if (this.grpid) {
            this.api.getgroupfeeds('group_id', this.grpid, page).subscribe(function (newPagePosts) {
              var _this2$posts;

              (_this2$posts = _this2.posts).push.apply(_this2$posts, _toConsumableArray(newPagePosts));

              event.target.complete();
            }, function (err) {
              // there are no more posts available
              event.target.disabled = true;
            });
          } else {
            this.api.getActivityall(page).subscribe(function (newPagePosts) {
              var _this2$posts2;

              (_this2$posts2 = _this2.posts).push.apply(_this2$posts2, _toConsumableArray(newPagePosts));

              event.target.complete();
            }, function (err) {
              // there are no more posts available
              event.target.disabled = true;
            });
          }
        }
      }, {
        key: "ionViewDidLoad",
        value: function ionViewDidLoad() {
          var _this3 = this;

          this.api.retrieveCategories().subscribe(function (results) {
            _this3.categories = results;
          });
        }
      }, {
        key: "singlefeed",
        value: function singlefeed(idd) {
          var index = this.posts.find(function (_ref) {
            var id = _ref.id;
            return id === idd;
          });
          if (this.grpid) index.media_url = this.data.media_url;
          var navigationExtras = {
            queryParams: {
              special: JSON.stringify(index)
            }
          };
          this.router.navigate(['single-feed'], navigationExtras);
        }
      }, {
        key: "likefeed",
        value: function likefeed(idd) {
          var _this4 = this;

          this.api.activitylike(idd).subscribe(function (res) {
            console.log(res);

            var index = _this4.posts.findIndex(function (_ref2) {
              var id = _ref2.id;
              return id === res.id;
            });

            if (typeof index !== 'undefined') _this4.posts.splice(index, 1, res);
            console.log(_this4.posts);
          });
        }
      }, {
        key: "openActionSheet",
        value: function openActionSheet() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var actionSheet;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.actionSheet.create({
                      mode: 'md',
                      buttons: [{
                        text: 'Save',
                        role: 'destructive',
                        icon: 'bookmark-outline',
                        handler: function handler() {
                          console.log('Delete clicked');
                        }
                      }, {
                        text: 'Send in a private Message',
                        icon: 'chatbox-ellipses-outline',
                        handler: function handler() {
                          console.log('Share clicked');
                        }
                      }, {
                        text: 'Share via',
                        icon: 'share-social',
                        handler: function handler() {
                          console.log('Play clicked');
                        }
                      }, {
                        text: 'Hide this post',
                        icon: 'close-circle-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Unfollow',
                        icon: 'person-remove-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Report this post',
                        icon: 'flag-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Improve my feed',
                        icon: 'options-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Who can see this post?',
                        icon: 'eye-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }]
                    });

                  case 2:
                    actionSheet = _context.sent;
                    _context.next = 5;
                    return actionSheet.present();

                  case 5:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "openShareSheet",
        value: function openShareSheet(idd) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this5 = this;

            var data, actionSheet;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    data = this.posts.find(function (_ref3) {
                      var id = _ref3.id;
                      return id === idd;
                    });
                    _context2.next = 3;
                    return this.actionSheet.create({
                      mode: 'md',
                      buttons: [{
                        text: 'Share as Text',
                        icon: 'share-social-outline',
                        handler: function handler() {
                          console.log('Delete clicked');
                          if (typeof data !== 'undefined') _this5.socialSharing.share(data.content_stripped, 'MEDIUM', null, data.title);
                        }
                      }, {
                        text: 'Send Mail',
                        icon: 'mail-outline',
                        handler: function handler() {
                          console.log('Share clicked');

                          _this5.socialSharing.shareViaEmail(data.content_stripped, data.title, [_this5.user.user_email]);
                        }
                      }, {
                        text: 'Share via Facebook',
                        icon: 'logo-facebook',
                        handler: function handler() {
                          console.log('facebook clicked');

                          _this5.socialSharing.shareViaFacebookWithPasteMessageHint(data.content_stripped, _this5.data.media_url, data.link);
                        }
                      }, {
                        text: 'Share via Whatsapp',
                        icon: 'logo-whatsapp',
                        handler: function handler() {
                          if (typeof data !== 'undefined') _this5.socialSharing.shareViaWhatsApp(data.content_stripped, _this5.data.media_url, data.link);
                          console.log('whatsapp clicked');
                        }
                      }, {
                        text: 'Share via Twitter',
                        icon: 'logo-twitter',
                        handler: function handler() {
                          if (typeof data !== 'undefined') _this5.socialSharing.shareViaTwitter(data.content_stripped, _this5.data.media_url, data.link);
                        }
                      }, {
                        text: 'Share via Instagram',
                        icon: 'logo-instagram',
                        handler: function handler() {
                          if (typeof data !== 'undefined') _this5.socialSharing.shareViaTwitter(data.content_stripped, _this5.data.media_url);
                        }
                      }]
                    });

                  case 3:
                    actionSheet = _context2.sent;
                    _context2.next = 6;
                    return actionSheet.present();

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "openMenu",
        value: function openMenu() {
          this.menuCtrl.open();
        }
      }, {
        key: "onSearchChange",
        value: function onSearchChange(event) {}
      }, {
        key: "goToChatList",
        value: function goToChatList() {
          this.router.navigate(['/chatlist']);
        }
      }, {
        key: "goTosinglePost",
        value: function goTosinglePost() {
          this.router.navigate(['/single-post']);
        }
      }]);

      return GroupFeedPage;
    }();

    GroupFeedPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"]
      }, {
        type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_6__["NativePageTransitions"]
      }, {
        type: _angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"]
      }, {
        type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_4__["SocialSharing"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ActionSheetController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }];
    };

    GroupFeedPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-group-feed',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./group-feed.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/group-feed/group-feed.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./group-feed.page.scss */
      "./src/app/pages/group-feed/group-feed.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"], _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_6__["NativePageTransitions"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"], _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_4__["SocialSharing"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ActionSheetController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])], GroupFeedPage);
    /***/
  },

  /***/
  "./src/app/pages/group-feed/group-feed.resolver.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/group-feed/group-feed.resolver.ts ***!
    \*********************************************************/

  /*! exports provided: PostsResolver */

  /***/
  function srcAppPagesGroupFeedGroupFeedResolverTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PostsResolver", function () {
      return PostsResolver;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var PostsResolver = /*#__PURE__*/function () {
      function PostsResolver(api) {
        _classCallCheck(this, PostsResolver);

        this.api = api;
      }

      _createClass(PostsResolver, [{
        key: "resolve",
        value: function resolve(route) {
          var categoryId = route.queryParams['categoryId'];
          var categoryTitle = route.queryParams['title'];
          return this.api.getRecentPosts(categoryId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (posts) {
            return {
              posts: posts,
              categoryTitle: categoryTitle,
              categoryId: categoryId
            };
          }));
        }
      }]);

      return PostsResolver;
    }();

    PostsResolver.ctorParameters = function () {
      return [{
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_2__["apiService"]
      }];
    };

    PostsResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_2__["apiService"]])], PostsResolver);
    /***/
  }
}]);
//# sourceMappingURL=pages-group-feed-group-feed-module-es5.js.map
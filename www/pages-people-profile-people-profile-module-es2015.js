(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-people-profile-people-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/people-profile/people-profile.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/people-profile/people-profile.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button mode='md' color=\"light\"></ion-back-button>\n    </ion-buttons>\n    <ion-input type=\"text\" placeholder=\"Jonh Doe\">\n      <ion-icon slot=\"start\" name=\"search\"></ion-icon>\n    </ion-input>\n    <!-- <ion-searchbar></ion-searchbar> -->\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n    <div class=\"cover\" [style.backgroundImage]=\"'url(assets/imgs/back1.jpg)'\">\n      <div class=\"user_image\" [style.backgroundImage]=\"'url(assets/imgs/user.jpg)'\"></div>\n    </div>\n\n    <div class=\"personal_info\">\n      <ion-label class=\"username\">John Doe <span style=\"color: #505050;\">•</span><span style=\"color: #505050;font-size: 14px;\"> 1 st</span></ion-label>\n      <ion-label>Software Engineer</ion-label>\n      <ion-label>Upwork • <span style=\"font-size: 14px;\">GOVERNMENT ENGINEERING COLLEGE, BHAVNAGAR</span></ion-label>\n      <ion-label>Bhavnagar, Gujrat India • <span style=\"color: var(--ion-color-main); font-weight: 600\"> 20 Connections</span></ion-label>\n\n      <ion-button size=\"small\" class=\"msg_btn\" >\n        MESSAGE\n      </ion-button>\n\n      <ion-button size=\"small\" class=\"more_btn\"  fill=\"outline\">\n        MORE...\n      </ion-button>\n    </div>\n\n    <div class=\"highlight_div\">\n      <ion-label>Highlights</ion-label>\n\n      <div class=\"flex_div\">\n        <div class=\"multi_images\">\n          <div class=\"user_image2\" [style.backgroundImage]=\"'url(assets/imgs/user3.jpg)'\"></div>\n          <div class=\"user_image3\" [style.backgroundImage]=\"'url(assets/imgs/user2.jpg)'\"></div>\n        </div>\n        <div class=\"content_div\">\n          <ion-label class=\"head_lbl\">3 mutual connections</ion-label>\n          <ion-label class=\"light_lbl\">You and Jonh Both know Mike and 2 others</ion-label>\n        </div>\n      </div>\n\n      <div class=\"flex_div\">\n        <div class=\"multi_images\">\n          <div class=\"user_image\" [style.backgroundImage]=\"'url(assets/imgs/user3.jpg)'\"></div>\n        </div>\n        <div class=\"content_div\" style=\"border: none;padding-bottom: 0px;\">\n          <ion-label class=\"head_lbl\">You both worked at Initappz Shop</ion-label>\n          <ion-label class=\"light_lbl\">You both worked at Initappz from February 2019 to March 2020</ion-label>\n          <ion-button size=\"small\"   fill=\"outline\">\n            SAY HELLO\n          </ion-button>\n        </div>\n      </div>\n    </div>\n    <div class=\"about_div\">\n      <ion-label>About</ion-label>\n      <ion-label class=\"about_detail\">\n        Lorem Ipsum is simply dummy text of the printing and typesetting industry. \n        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...\n      </ion-label>\n      <ion-label class=\"see_more_lbl\">See more</ion-label>\n    </div>\n\n    <div class=\"highlight_div\">\n      <ion-label>Experience</ion-label>\n\n      <div class=\"flex_div\" *ngFor=\"let item of (company | slice : 0: 3);let i = index\">\n        <div class=\"multi_images\">\n          <div class=\"user_image\" [style.backgroundImage]=\"'url('+item.img+')'\"></div>\n        </div>\n        <div class=\"content_div\" [class.no_border]=\"i == 2\">\n          <ion-label class=\"head_lbl\">Software Engineer</ion-label>\n          <ion-label class=\"simple_lbl\">{{item.name}} • Full-time</ion-label>\n          <ion-label class=\"light_lbl\" style=\"font-size: 14px;\">Feb 2020 - Present • 2 mos</ion-label>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"highlight_div\">\n      <ion-label>Education</ion-label>\n\n      <div class=\"flex_div\" *ngFor=\"let item of [1,2,3];let i = index\">\n        <div class=\"multi_images\">\n          <div class=\"user_image\" [style.backgroundImage]=\"'url(assets/imgs/university.png)'\"></div>\n        </div>\n        <div class=\"content_div\" [class.no_border]=\"i == 2\">\n          <ion-label class=\"head_lbl\" style=\"font-size: 13px;\">GOVERNMENT ENGINEERING COLLEGE, BHAVNAGAR</ion-label>\n          <ion-label class=\"simple_lbl\">Bachelor's degree, Information Technology</ion-label>\n          <ion-label class=\"light_lbl\" style=\"font-size: 14px;\">2015 - 2018</ion-label>\n        </div>\n      </div>\n    </div>\n    <div class=\"white_div\">\n      <ion-label class=\"blue_lbl\" style=\"text-align: center;\">SHOW MORE</ion-label>\n    </div>\n\n    <div class=\"skill_div\">\n      <ion-label>Skills & Endorsements</ion-label>\n\n      <div *ngFor=\"let item of (users | slice : 0: 3); let i = index\" class=\"main_div\" [class.no_border]=\"i == 2\">\n        <div class=\"inner_div\">\n          <ion-icon name=\"add-circle-outline\"></ion-icon> <ion-label class=\"head_lbl\">BootStrap</ion-label> \n        </div>\n        <div class=\"inner_div\" style=\"padding-left: 25px;\">\n          <div class=\"user_image\" [style.backgroundImage]=\"'url('+item.img+')'\"></div> <ion-label class=\"small_lbl\">Endorsed by Mike (mutual Connection)</ion-label> \n        </div>\n      </div>\n    </div>\n    <div class=\"white_div\">\n      <ion-label class=\"blue_lbl\" style=\"text-align: center;\">SEE 5 MORE SKILLS</ion-label>\n    </div>\n\n    <div class=\"skill_div\">\n      <ion-label>Accomplishments</ion-label>\n\n        <div class=\"inner_div\" style=\"padding-top: 10px;\">\n          <ion-label class=\"blue_lbl\">3</ion-label> <ion-label class=\"blue_lbl\" style=\"margin-left: 20px;\">LANGUAGES</ion-label> \n        </div>  \n         <ion-label class=\"small_lbl\" style=\"padding-left: 30px;\">English</ion-label> \n    </div>\n\n    <div class=\"highlight_div\">\n      <ion-label>Contact</ion-label>\n\n      <div class=\"flex_div\">\n        <div class=\"multi_images\">\n          <img src=\"assets/imgs/linkedin.png\" class=\"image\">\n        </div>\n        <div class=\"content_div\">\n          <ion-label class=\"head_lbl\">Jonh's Profile</ion-label>\n          <ion-label class=\"blue_lbl\">https://www.linkedin.com/test</ion-label>\n        </div>\n      </div>\n\n      <div class=\"flex_div\">\n        <div class=\"multi_images\">\n          <img src=\"assets/imgs/email_2.png\" class=\"image\">\n        </div>\n        <div class=\"content_div\" style=\"border-bottom: none;\">\n          <ion-label class=\"head_lbl\">Email</ion-label>\n          <ion-label class=\"blue_lbl\">jonh@gmail.com</ion-label>\n        </div>\n      </div>\n\n    </div>\n\n    <div class=\"highlight_div\">\n      <ion-label>Interests</ion-label>\n\n      <div class=\"flex_div\" style=\"border-bottom: 1px solid lightgray;padding-top: 10px;padding-bottom: 10px;\">\n        <img src=\"{{item.img}}\" *ngFor=\"let item of company\" class=\"interest\">\n      </div>\n    \n      <ion-label style=\"padding-top: 10px;\">Causes Jonh cares about</ion-label>\n      <div class=\"flex_div\" style=\"padding-top: 10px;\">\n        <img src=\"assets/imgs/heart_2.png\" class=\"image\">\n        <ion-label style=\"margin-left: 15px;\">Health, Social Service</ion-label>\n      </div>\n\n    </div>\n    <div class=\"white_div\">\n      <ion-label class=\"blue_lbl\" style=\"text-align: center;\">SEE ALL</ion-label>\n    </div>\n\n    <div class=\"people_div\">\n      <ion-label style=\"padding-bottom: 10px;\">People also viewed</ion-label>\n\n      <div class=\"flex_div\" *ngFor=\"let item of (users | slice : 0: 3);let i = index\">\n        <div class=\"user_image\" [style.backgroundImage]=\"'url('+item.img+')'\"></div>\n        <div class=\"content_div\" [class.no_border]=\"i == 2\">\n          <ion-label class=\"head_lbl\">Jonh Doe • <span style=\"font-weight: normal\">3rd</span></ion-label>\n          <ion-label class=\"simple_lbl\">Student at GEC-BHAVNAGAR</ion-label>\n        </div>\n      </div>\n    </div>\n\n\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/people-profile/people-profile-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/people-profile/people-profile-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: PeopleProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeopleProfilePageRoutingModule", function() { return PeopleProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _people_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./people-profile.page */ "./src/app/pages/people-profile/people-profile.page.ts");




const routes = [
    {
        path: '',
        component: _people_profile_page__WEBPACK_IMPORTED_MODULE_3__["PeopleProfilePage"]
    }
];
let PeopleProfilePageRoutingModule = class PeopleProfilePageRoutingModule {
};
PeopleProfilePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PeopleProfilePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/people-profile/people-profile.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/people-profile/people-profile.module.ts ***!
  \***************************************************************/
/*! exports provided: PeopleProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeopleProfilePageModule", function() { return PeopleProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _people_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./people-profile-routing.module */ "./src/app/pages/people-profile/people-profile-routing.module.ts");
/* harmony import */ var _people_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./people-profile.page */ "./src/app/pages/people-profile/people-profile.page.ts");







let PeopleProfilePageModule = class PeopleProfilePageModule {
};
PeopleProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _people_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["PeopleProfilePageRoutingModule"]
        ],
        declarations: [_people_profile_page__WEBPACK_IMPORTED_MODULE_6__["PeopleProfilePage"]]
    })
], PeopleProfilePageModule);



/***/ }),

/***/ "./src/app/pages/people-profile/people-profile.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/people-profile/people-profile.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: var(--ion-color-main);\n}\nion-toolbar ion-input {\n  border-bottom: 1px solid white;\n  --color: white;\n  --placeholder-color: white;\n  --padding-start: 8px;\n}\nion-toolbar ion-icon {\n  color: white;\n  font-size: 24px;\n}\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\nion-content {\n  --background: #F5F5F5;\n}\n.main_content_div {\n  width: 100%;\n}\n.main_content_div .no_border {\n  border-bottom: none !important;\n}\n.main_content_div ion-label {\n  display: block;\n}\n.main_content_div .cover {\n  width: 100%;\n  height: 100px;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  position: relative;\n}\n.main_content_div .cover .user_image {\n  width: 100px;\n  height: 100px;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  border-radius: 50%;\n  border: 4px solid white;\n  position: absolute;\n  left: 16px;\n  top: 50%;\n}\n.main_content_div .personal_info {\n  padding: 16px;\n  padding-top: 55px;\n  background: white;\n}\n.main_content_div .personal_info ion-label {\n  margin-bottom: 7px;\n}\n.main_content_div .personal_info .username {\n  font-size: 20px;\n}\n.main_content_div .personal_info .msg_btn {\n  --background: var(--ion-color-main);\n  font-weight: 600;\n  --border-radius: 3px;\n  font-size: 18px;\n}\n.main_content_div .personal_info .more_btn {\n  color: gray;\n  --border-color: gray;\n  font-weight: 600;\n  --border-radius: 3px;\n  --border-width: 2px;\n  margin-left: 10px;\n  font-size: 18px;\n}\n.main_content_div .highlight_div {\n  margin-top: 10px;\n  padding: 16px;\n  background: white;\n  border-bottom: 1px solid lightgray;\n}\n.main_content_div .highlight_div .image {\n  width: 20px;\n}\n.main_content_div .highlight_div .flex_div {\n  display: flex;\n  align-items: flex-start;\n}\n.main_content_div .highlight_div .flex_div .interest {\n  width: 50px;\n  margin-right: 15px;\n}\n.main_content_div .highlight_div .flex_div .multi_images {\n  height: 45px;\n  width: 45px;\n  position: relative;\n  padding-top: 10px;\n}\n.main_content_div .highlight_div .flex_div .multi_images .user_image2 {\n  height: 25px;\n  width: 25px;\n  border-radius: 50%;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n}\n.main_content_div .highlight_div .flex_div .multi_images .user_image3 {\n  height: 25px;\n  width: 25px;\n  border-radius: 50%;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  position: absolute;\n  right: 5px;\n}\n.main_content_div .highlight_div .flex_div .multi_images .user_image {\n  height: 45px;\n  width: 45px;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  border-radius: 3px;\n}\n.main_content_div .highlight_div .flex_div .content_div {\n  padding-left: 10px;\n  border-bottom: 1px solid lightgray;\n  padding: 10px;\n  width: 100%;\n}\n.main_content_div .highlight_div .flex_div .content_div .head_lbl {\n  font-weight: 600;\n}\n.main_content_div .highlight_div .flex_div .content_div .light_lbl {\n  color: gray;\n}\n.main_content_div .highlight_div .flex_div .content_div .simple_lbl {\n  font-size: 14px;\n  color: #505050;\n}\n.main_content_div .highlight_div .flex_div .content_div .blue_lbl {\n  color: var(--ion-color-main);\n  font-size: 14px;\n}\n.main_content_div .highlight_div .flex_div .content_div ion-button {\n  color: var(--ion-color-main);\n  --border-color: var(--ion-color-main);\n  font-weight: 600;\n  --border-radius: 3px;\n  --border-width: 2px;\n  font-size: 16px;\n  margin-top: 5px;\n}\n.main_content_div .about_div {\n  margin-top: 10px;\n  background: white;\n  padding: 16px;\n}\n.main_content_div .about_div .about_detail {\n  font-size: 14px;\n  margin-top: 5px;\n  color: #505050;\n}\n.main_content_div .about_div .see_more_lbl {\n  text-align: right;\n  color: gray;\n  font-size: 14px;\n}\n.main_content_div .white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n.main_content_div .white_div .blue_lbl {\n  color: var(--ion-color-main);\n  padding-top: 15px;\n  padding-bottom: 15px;\n  font-weight: 600;\n}\n.main_content_div .skill_div {\n  margin-top: 10px;\n  padding: 16px;\n  background: white;\n  border-bottom: 1px solid lightgray;\n}\n.main_content_div .skill_div .main_div {\n  padding-top: 10px;\n  padding-bottom: 10px;\n  border-bottom: 1px solid lightgray;\n}\n.main_content_div .skill_div .inner_div {\n  display: flex;\n  align-items: center;\n}\n.main_content_div .skill_div .inner_div ion-icon {\n  font-size: 30px;\n  color: var(--ion-color-main);\n}\n.main_content_div .skill_div .inner_div .head_lbl {\n  font-weight: 600;\n  padding-left: 10px;\n}\n.main_content_div .skill_div .inner_div .user_image {\n  width: 25px;\n  height: 25px;\n  min-width: 25px;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  border-radius: 50%;\n}\n.main_content_div .skill_div .inner_div .blue_lbl {\n  color: var(--ion-color-main);\n  font-weight: 600;\n}\n.main_content_div .skill_div .small_lbl {\n  font-size: 14px;\n  padding-left: 10px;\n}\n.main_content_div .people_div {\n  padding: 16px;\n}\n.main_content_div .people_div .flex_div {\n  display: flex;\n  width: 100%;\n  align-items: center;\n}\n.main_content_div .people_div .flex_div .user_image {\n  height: 45px;\n  width: 45px;\n  min-width: 45px;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  border-radius: 50%;\n}\n.main_content_div .people_div .content_div {\n  padding-left: 10px;\n  border-bottom: 1px solid lightgray;\n  padding: 10px;\n  width: 100%;\n}\n.main_content_div .people_div .content_div .head_lbl {\n  font-weight: 600;\n  color: #505050;\n  font-size: 15px;\n}\n.main_content_div .people_div .content_div .simple_lbl {\n  font-size: 14px;\n  color: #505050;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcGVvcGxlLXByb2ZpbGUvRDpcXFJhZ3V2YXJhbiBpbWFnZVxcZGlzZWxzaGlwL3NyY1xcYXBwXFxwYWdlc1xccGVvcGxlLXByb2ZpbGVcXHBlb3BsZS1wcm9maWxlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvcGVvcGxlLXByb2ZpbGUvcGVvcGxlLXByb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUNBQUE7QUNDSjtBREFJO0VBQ0ksOEJBQUE7RUFDQSxjQUFBO0VBQ0EsMEJBQUE7RUFDQSxvQkFBQTtBQ0VSO0FEQUk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtBQ0VSO0FEQ0E7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0FDRUo7QURBQTtFQUNJLHFCQUFBO0FDR0o7QURBQTtFQUNJLFdBQUE7QUNHSjtBRERJO0VBQ0ksOEJBQUE7QUNHUjtBREFJO0VBQ0ksY0FBQTtBQ0VSO0FEQ0k7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0FDQ1I7QURBUTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7QUNFWjtBRENJO0VBQ0ksYUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUNDUjtBRENRO0VBQ0ksa0JBQUE7QUNDWjtBREVRO0VBQ0ksZUFBQTtBQ0FaO0FER1E7RUFDSSxtQ0FBQTtFQUNBLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0FDRFo7QURHUTtFQUNJLFdBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ0RaO0FES0k7RUFDSSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLGtDQUFBO0FDSFI7QURLUTtFQUNJLFdBQUE7QUNIWjtBREtRO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0FDSFo7QURLWTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtBQ0hoQjtBRE1ZO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDSmhCO0FEUWdCO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtBQ05wQjtBRFFnQjtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUNOcEI7QURTZ0I7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0FDUHBCO0FEV1k7RUFDSSxrQkFBQTtFQUNBLGtDQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7QUNUaEI7QURXZ0I7RUFDSSxnQkFBQTtBQ1RwQjtBRFdnQjtFQUNJLFdBQUE7QUNUcEI7QURXZ0I7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQ1RwQjtBRFdnQjtFQUNJLDRCQUFBO0VBQ0EsZUFBQTtBQ1RwQjtBRFlnQjtFQUNJLDRCQUFBO0VBQ0EscUNBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQ1ZwQjtBRGdCSTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0FDZFI7QURnQlE7RUFDSSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUNkWjtBRGdCUTtFQUNJLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUNkWjtBRGtCSTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDaEJSO0FEa0JRO0VBQ0ksNEJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7QUNoQlo7QURvQkk7RUFDSSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLGtDQUFBO0FDbEJSO0FEb0JRO0VBQ0ksaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtDQUFBO0FDbEJaO0FEcUJRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FDbkJaO0FEb0JZO0VBQ0ksZUFBQTtFQUNBLDRCQUFBO0FDbEJoQjtBRG9CWTtFQUNJLGdCQUFBO0VBQ0Esa0JBQUE7QUNsQmhCO0FEcUJZO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7QUNuQmhCO0FEc0JZO0VBQ0ksNEJBQUE7RUFDQSxnQkFBQTtBQ3BCaEI7QUR5QlE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7QUN2Qlo7QUQyQkk7RUFDSSxhQUFBO0FDekJSO0FEMkJRO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQ3pCWjtBRDJCWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0FDekJoQjtBRDRCUTtFQUNJLGtCQUFBO0VBQ0Esa0NBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtBQzFCWjtBRDRCWTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUMxQmhCO0FENEJZO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUMxQmhCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcGVvcGxlLXByb2ZpbGUvcGVvcGxlLXByb2ZpbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGlvbi1pbnB1dHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAgICAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgLS1wbGFjZWhvbGRlci1jb2xvciA6IHdoaXRlO1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQgOiA4cHg7XG4gICAgfVxuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICB9XG59XG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaHtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5pb24tY29udGVudHtcbiAgICAtLWJhY2tncm91bmQ6ICNGNUY1RjU7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2e1xuICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgLm5vX2JvcmRlcntcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogbm9uZSAhaW1wb3J0YW50O1xuICAgIH1cblxuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIH1cblxuICAgIC5jb3ZlcntcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMTAwcHg7XG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAudXNlcl9pbWFnZXtcbiAgICAgICAgICAgIHdpZHRoOiAxMDBweDtcbiAgICAgICAgICAgIGhlaWdodDogMTAwcHg7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgIGJvcmRlcjogNHB4IHNvbGlkIHdoaXRlO1xuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgbGVmdDogMTZweDtcbiAgICAgICAgICAgIHRvcDogNTAlO1xuICAgICAgICB9XG4gICAgfVxuICAgIC5wZXJzb25hbF9pbmZve1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICBwYWRkaW5nLXRvcDogNTVweDtcbiAgICAgICAgYmFja2dyb3VuZCA6IHdoaXRlO1xuXG4gICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA3cHg7XG4gICAgICAgIH1cblxuICAgICAgICAudXNlcm5hbWV7XG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIH1cblxuICAgICAgICAubXNnX2J0bntcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICB9XG4gICAgICAgIC5tb3JlX2J0bntcbiAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgLS1ib3JkZXItY29sb3I6IGdyYXk7XG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICAtLWJvcmRlci13aWR0aDogMnB4O1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuaGlnaGxpZ2h0X2RpdntcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgcGFkZGluZzogMTZweDtcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG5cbiAgICAgICAgLmltYWdle1xuICAgICAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgIH1cbiAgICAgICAgLmZsZXhfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuXG4gICAgICAgICAgICAuaW50ZXJlc3R7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAubXVsdGlfaW1hZ2Vze1xuICAgICAgICAgICAgICAgIGhlaWdodDogNDVweDtcbiAgICAgICAgICAgICAgICB3aWR0aDogNDVweDtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICAgICAgcGFkZGluZy10b3AgOiAxMHB4O1xuXG4gICAgICAgICAgICAgICAgXG5cbiAgICAgICAgICAgICAgICAudXNlcl9pbWFnZTJ7XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMjVweDtcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDI1cHg7XG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAudXNlcl9pbWFnZTN7XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMjVweDtcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDI1cHg7XG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgICAgIHJpZ2h0OiA1cHg7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLnVzZXJfaW1hZ2V7XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDVweDtcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDQ1cHg7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmNvbnRlbnRfZGl2e1xuICAgICAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG5cbiAgICAgICAgICAgICAgICAuaGVhZF9sYmx7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5saWdodF9sYmx7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAuc2ltcGxlX2xibHtcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzUwNTA1MDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLmJsdWVfbGJse1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaW9uLWJ1dHRvbntcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgICAgICAgICAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICAgICAgICAgIC0tYm9yZGVyLXdpZHRoOiAycHg7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5hYm91dF9kaXZ7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuXG4gICAgICAgIC5hYm91dF9kZXRhaWx7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgICAgICBjb2xvcjogIzUwNTA1MDtcbiAgICAgICAgfVxuICAgICAgICAuc2VlX21vcmVfbGJse1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC53aGl0ZV9kaXZ7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG5cbiAgICAgICAgLmJsdWVfbGJse1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxNXB4O1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLnNraWxsX2RpdntcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgcGFkZGluZzogMTZweDtcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG5cbiAgICAgICAgLm1haW5fZGl2e1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIC5pbm5lcl9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgICAgXG4gICAgICAgICAgICBpb24taWNvbntcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5oZWFkX2xibHtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLnVzZXJfaW1hZ2V7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDI1cHg7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgICAgICAgICAgICAgIG1pbi13aWR0aDogMjVweDtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmJsdWVfbGJse1xuICAgICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9XG5cbiAgICAgICAgLnNtYWxsX2xibHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5wZW9wbGVfZGl2e1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICBcbiAgICAgICAgLmZsZXhfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcblxuICAgICAgICAgICAgLnVzZXJfaW1hZ2V7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA0NXB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiA0NXB4O1xuICAgICAgICAgICAgICAgIG1pbi13aWR0aDogNDVweDtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuY29udGVudF9kaXZ7XG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgICAgICAgICAuaGVhZF9sYmx7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICBjb2xvcjogIzUwNTA1MDtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuc2ltcGxlX2xibHtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgY29sb3I6ICM1MDUwNTA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cbmlvbi10b29sYmFyIGlvbi1pbnB1dCB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcbiAgLS1jb2xvcjogd2hpdGU7XG4gIC0tcGxhY2Vob2xkZXItY29sb3I6IHdoaXRlO1xuICAtLXBhZGRpbmctc3RhcnQ6IDhweDtcbn1cbmlvbi10b29sYmFyIGlvbi1pY29uIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDI0cHg7XG59XG5cbmlvbi10b29sYmFyLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgsIGlvbi10b29sYmFyIC5zYy1pb24tc2VhcmNoYmFyLWlvcy1oIHtcbiAgcGFkZGluZy10b3A6IDBweDtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6ICNGNUY1RjU7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAubm9fYm9yZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogbm9uZSAhaW1wb3J0YW50O1xufVxuLm1haW5fY29udGVudF9kaXYgaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY292ZXIge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY292ZXIgLnVzZXJfaW1hZ2Uge1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBib3JkZXI6IDRweCBzb2xpZCB3aGl0ZTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAxNnB4O1xuICB0b3A6IDUwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9pbmZvIHtcbiAgcGFkZGluZzogMTZweDtcbiAgcGFkZGluZy10b3A6IDU1cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2luZm8gaW9uLWxhYmVsIHtcbiAgbWFyZ2luLWJvdHRvbTogN3B4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2luZm8gLnVzZXJuYW1lIHtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2luZm8gLm1zZ19idG4ge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9pbmZvIC5tb3JlX2J0biB7XG4gIGNvbG9yOiBncmF5O1xuICAtLWJvcmRlci1jb2xvcjogZ3JheTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gIC0tYm9yZGVyLXdpZHRoOiAycHg7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBmb250LXNpemU6IDE4cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGlnaGxpZ2h0X2RpdiB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIHBhZGRpbmc6IDE2cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmhpZ2hsaWdodF9kaXYgLmltYWdlIHtcbiAgd2lkdGg6IDIwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGlnaGxpZ2h0X2RpdiAuZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5oaWdobGlnaHRfZGl2IC5mbGV4X2RpdiAuaW50ZXJlc3Qge1xuICB3aWR0aDogNTBweDtcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmhpZ2hsaWdodF9kaXYgLmZsZXhfZGl2IC5tdWx0aV9pbWFnZXMge1xuICBoZWlnaHQ6IDQ1cHg7XG4gIHdpZHRoOiA0NXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmhpZ2hsaWdodF9kaXYgLmZsZXhfZGl2IC5tdWx0aV9pbWFnZXMgLnVzZXJfaW1hZ2UyIHtcbiAgaGVpZ2h0OiAyNXB4O1xuICB3aWR0aDogMjVweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGlnaGxpZ2h0X2RpdiAuZmxleF9kaXYgLm11bHRpX2ltYWdlcyAudXNlcl9pbWFnZTMge1xuICBoZWlnaHQ6IDI1cHg7XG4gIHdpZHRoOiAyNXB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmhpZ2hsaWdodF9kaXYgLmZsZXhfZGl2IC5tdWx0aV9pbWFnZXMgLnVzZXJfaW1hZ2Uge1xuICBoZWlnaHQ6IDQ1cHg7XG4gIHdpZHRoOiA0NXB4O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5oaWdobGlnaHRfZGl2IC5mbGV4X2RpdiAuY29udGVudF9kaXYge1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmhpZ2hsaWdodF9kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiAuaGVhZF9sYmwge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmhpZ2hsaWdodF9kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiAubGlnaHRfbGJsIHtcbiAgY29sb3I6IGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGlnaGxpZ2h0X2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IC5zaW1wbGVfbGJsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogIzUwNTA1MDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5oaWdobGlnaHRfZGl2IC5mbGV4X2RpdiAuY29udGVudF9kaXYgLmJsdWVfbGJsIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmhpZ2hsaWdodF9kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiBpb24tYnV0dG9uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gIC0tYm9yZGVyLXdpZHRoOiAycHg7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmFib3V0X2RpdiB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nOiAxNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmFib3V0X2RpdiAuYWJvdXRfZGV0YWlsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIGNvbG9yOiAjNTA1MDUwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmFib3V0X2RpdiAuc2VlX21vcmVfbGJsIHtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIGNvbG9yOiBncmF5O1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAud2hpdGVfZGl2IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC53aGl0ZV9kaXYgLmJsdWVfbGJsIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLnNraWxsX2RpdiB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIHBhZGRpbmc6IDE2cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLnNraWxsX2RpdiAubWFpbl9kaXYge1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2tpbGxfZGl2IC5pbm5lcl9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLm1haW5fY29udGVudF9kaXYgLnNraWxsX2RpdiAuaW5uZXJfZGl2IGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuLm1haW5fY29udGVudF9kaXYgLnNraWxsX2RpdiAuaW5uZXJfZGl2IC5oZWFkX2xibCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5za2lsbF9kaXYgLmlubmVyX2RpdiAudXNlcl9pbWFnZSB7XG4gIHdpZHRoOiAyNXB4O1xuICBoZWlnaHQ6IDI1cHg7XG4gIG1pbi13aWR0aDogMjVweDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuc2tpbGxfZGl2IC5pbm5lcl9kaXYgLmJsdWVfbGJsIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5za2lsbF9kaXYgLnNtYWxsX2xibCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlb3BsZV9kaXYge1xuICBwYWRkaW5nOiAxNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlb3BsZV9kaXYgLmZsZXhfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVvcGxlX2RpdiAuZmxleF9kaXYgLnVzZXJfaW1hZ2Uge1xuICBoZWlnaHQ6IDQ1cHg7XG4gIHdpZHRoOiA0NXB4O1xuICBtaW4td2lkdGg6IDQ1cHg7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlb3BsZV9kaXYgLmNvbnRlbnRfZGl2IHtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICBwYWRkaW5nOiAxMHB4O1xuICB3aWR0aDogMTAwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZW9wbGVfZGl2IC5jb250ZW50X2RpdiAuaGVhZF9sYmwge1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzUwNTA1MDtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlb3BsZV9kaXYgLmNvbnRlbnRfZGl2IC5zaW1wbGVfbGJsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogIzUwNTA1MDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/people-profile/people-profile.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/people-profile/people-profile.page.ts ***!
  \*************************************************************/
/*! exports provided: PeopleProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeopleProfilePage", function() { return PeopleProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/dummy-data.service */ "./src/app/services/dummy-data.service.ts");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/


let PeopleProfilePage = class PeopleProfilePage {
    constructor(dummy) {
        this.dummy = dummy;
    }
    ngOnInit() {
        this.users = this.dummy.users;
        this.company = this.dummy.company;
    }
};
PeopleProfilePage.ctorParameters = () => [
    { type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__["dummyDataService"] }
];
PeopleProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-people-profile',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./people-profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/people-profile/people-profile.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./people-profile.page.scss */ "./src/app/pages/people-profile/people-profile.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__["dummyDataService"]])
], PeopleProfilePage);



/***/ }),

/***/ "./src/app/services/dummy-data.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/dummy-data.service.ts ***!
  \************************************************/
/*! exports provided: dummyDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dummyDataService", function() { return dummyDataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

let dummyDataService = class dummyDataService {
    constructor() {
        this.users = [
            {
                img: 'assets/imgs/user.jpg',
                cover: 'assets/imgs/back1.jpg',
                btn_text: 'VIEW JOBS'
            },
            {
                img: 'assets/imgs/user2.jpg',
                cover: 'assets/imgs/back2.jpg',
                btn_text: 'FOLLOW HASHTAG'
            },
            {
                img: 'assets/imgs/user3.jpg',
                cover: 'assets/imgs/back3.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user4.jpg',
                cover: 'assets/imgs/back4.jpg',
                btn_text: 'SEE ALL VIEW'
            },
            {
                img: 'assets/imgs/user.jpg',
                cover: 'assets/imgs/back1.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user2.jpg',
                cover: 'assets/imgs/back2.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user3.jpg',
                cover: 'assets/imgs/back3.jpg',
                btn_text: 'EXPAND YOUR NETWORK'
            },
            {
                img: 'assets/imgs/user4.jpg',
                cover: 'assets/imgs/back4.jpg',
                btn_text: 'SEE ALL VIEW'
            }
        ];
        this.jobs = [
            {
                img: 'assets/imgs/company/initappz.png',
                title: 'SQL Server',
                addr: 'Bhavnagar, Gujrat, India',
                time: '2 school alumni',
                status: '1 day'
            },
            {
                img: 'assets/imgs/company/google.png',
                title: 'Web Designer',
                addr: 'Vadodara, Gujrat, India',
                time: 'Be an early applicant',
                status: '2 days'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                title: 'Business Analyst',
                addr: 'Ahmedabad, Gujrat, India',
                time: 'Be an early applicant',
                status: '1 week'
            },
            {
                img: 'assets/imgs/company/initappz.png',
                title: 'PHP Developer',
                addr: 'Pune, Maharashtra, India',
                time: '2 school alumni',
                status: 'New'
            },
            {
                img: 'assets/imgs/company/google.png',
                title: 'Graphics Designer',
                addr: 'Bhavnagar, Gujrat, India',
                time: 'Be an early applicant',
                status: '1 day'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                title: 'Phython Developer',
                addr: 'Ahmedabad, Gujrat, India',
                time: '2 school alumni',
                status: '5 days'
            },
        ];
        this.company = [
            {
                img: 'assets/imgs/company/initappz.png',
                name: 'Initappz Shop'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                name: 'Microsoft'
            },
            {
                img: 'assets/imgs/company/google.png',
                name: 'Google'
            },
        ];
    }
};
dummyDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], dummyDataService);



/***/ })

}]);
//# sourceMappingURL=pages-people-profile-people-profile-module-es2015.js.map
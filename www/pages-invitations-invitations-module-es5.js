function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-invitations-invitations-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/invitations/invitations.page.html":
  /*!***********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/invitations/invitations.page.html ***!
    \***********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesInvitationsInvitationsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <div class=\"div_header\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button mode=\"md\" color=\"light\"></ion-back-button>\n    </ion-buttons>\n    <ion-segment (ionChange)=\"segmentChanged($event)\" mode=\"md\">\n      <ion-segment-button>\n        <ion-label>RECEIVED</ion-label>\n      </ion-segment-button>\n      <ion-segment-button>\n        <ion-label>SENT</ion-label>\n      </ion-segment-button>\n    </ion-segment>\n\n    <ion-button fill=\"clear\" size=\"small\">\n      <ion-icon name=\"settings-outline\" color=\"light\" style=\"font-size: 22px;\"></ion-icon>\n    </ion-button>\n  </div>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n\n    <span *ngIf=\"segId == 'ion-sb-0'\">\n      <div class=\"chip_div\">\n        <ion-chip [class.active_chip]=\"chipId == 'all'\" (click)=\"chipChange('all')\">\n          <ion-label>All (7)</ion-label>\n        </ion-chip>\n        <ion-chip [class.active_chip]=\"chipId == 'people'\" (click)=\"chipChange('people')\">\n          <ion-label>People (7)</ion-label>\n        </ion-chip>\n      </div>\n\n      <div class=\"flex_div\" *ngFor=\"let item of users\">\n        <div class=\"f_div\">\n          <div class=\"back_image\" [style.backgroundImage]=\"'url( ' + item.img + ' )'\"></div>\n          <div class=\"detail\">\n            <ion-label class=\"name\">John Doe</ion-label>\n            <ion-label class=\"desc\">At Initapps Shop</ion-label>\n          </div>\n        </div>\n        <div class=\"s_div\">\n          <ion-icon slot=\"icon-only\" name=\"close-circle-outline\" class=\"cross\"></ion-icon>\n          <ion-icon slot=\"icon-only\" name=\"checkmark-circle-outline\" class=\"check\"></ion-icon>\n        </div>      \n      </div>\n\n    </span>\n\n    <span *ngIf=\"segId == 'ion-sb-1'\">\n      <div class=\"chip_div\">\n        <ion-chip [class.active_chip]=\"chipId2 == 'People'\" (click)=\"chipChange1('People')\">\n          <ion-label>People (3)</ion-label>\n        </ion-chip>\n        <ion-chip [class.active_chip]=\"chipId2 == 'Pages'\" (click)=\"chipChange1('Pages')\">\n          <ion-label>Pages (10)</ion-label>\n        </ion-chip>\n        <ion-chip [class.active_chip]=\"chipId2 == 'Events'\" (click)=\"chipChange1('Events')\">\n          <ion-label>Events (2)</ion-label>\n        </ion-chip>\n      </div>\n\n      <div class=\"flex_div\" *ngFor=\"let item of users\">\n        <div class=\"f_div\">\n          <div class=\"back_image\" [style.backgroundImage]=\"'url( ' + item.img + ' )'\"></div>\n          <div class=\"detail\">\n            <ion-label class=\"name\">John Doe</ion-label>\n            <ion-label class=\"desc\">At Initapps Shop</ion-label>\n          </div>\n        </div>\n        <div class=\"s_div\">\n          <ion-icon slot=\"icon-only\" name=\"close-circle-outline\" class=\"cross\"></ion-icon>\n          <ion-icon slot=\"icon-only\" name=\"checkmark-circle-outline\" class=\"check\"></ion-icon>\n        </div>      \n      </div>\n\n    </span>\n\n    \n    \n  </div>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/pages/invitations/invitations-routing.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/invitations/invitations-routing.module.ts ***!
    \*****************************************************************/

  /*! exports provided: InvitationsPageRoutingModule */

  /***/
  function srcAppPagesInvitationsInvitationsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InvitationsPageRoutingModule", function () {
      return InvitationsPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _invitations_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./invitations.page */
    "./src/app/pages/invitations/invitations.page.ts");

    var routes = [{
      path: '',
      component: _invitations_page__WEBPACK_IMPORTED_MODULE_3__["InvitationsPage"]
    }];

    var InvitationsPageRoutingModule = function InvitationsPageRoutingModule() {
      _classCallCheck(this, InvitationsPageRoutingModule);
    };

    InvitationsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], InvitationsPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/invitations/invitations.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/invitations/invitations.module.ts ***!
    \*********************************************************/

  /*! exports provided: InvitationsPageModule */

  /***/
  function srcAppPagesInvitationsInvitationsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InvitationsPageModule", function () {
      return InvitationsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _invitations_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./invitations-routing.module */
    "./src/app/pages/invitations/invitations-routing.module.ts");
    /* harmony import */


    var _invitations_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./invitations.page */
    "./src/app/pages/invitations/invitations.page.ts");

    var InvitationsPageModule = function InvitationsPageModule() {
      _classCallCheck(this, InvitationsPageModule);
    };

    InvitationsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _invitations_routing_module__WEBPACK_IMPORTED_MODULE_5__["InvitationsPageRoutingModule"]],
      declarations: [_invitations_page__WEBPACK_IMPORTED_MODULE_6__["InvitationsPage"]]
    })], InvitationsPageModule);
    /***/
  },

  /***/
  "./src/app/pages/invitations/invitations.page.scss":
  /*!*********************************************************!*\
    !*** ./src/app/pages/invitations/invitations.page.scss ***!
    \*********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesInvitationsInvitationsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\n.div_header {\n  display: flex;\n  height: 50px;\n  justify-content: space-between;\n  background: var(--ion-color-main);\n  align-items: center;\n}\n\n.div_header ion-segment-button {\n  --color-checked: white;\n  color: white;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div .chip_div {\n  padding: 10px 16px;\n}\n\n.main_content_div .chip_div ion-chip {\n  font-weight: 600;\n  color: #505050;\n  background: #E1E9EE;\n}\n\n.main_content_div .chip_div .active_chip {\n  color: white;\n  background: var(--ion-color-main);\n}\n\n.main_content_div .flex_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  justify-content: space-between;\n  padding-top: 10px;\n  padding-bottom: 15px;\n  border-bottom: 1px solid lightgray;\n  background: white;\n}\n\n.main_content_div .flex_div .f_div {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding-left: 16px;\n}\n\n.main_content_div .flex_div .f_div .detail {\n  margin-left: 10px;\n}\n\n.main_content_div .flex_div .f_div .detail .desc {\n  color: gray;\n}\n\n.main_content_div .flex_div .f_div .back_image {\n  height: 60px;\n  width: 60px;\n  min-width: 60px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.main_content_div .flex_div .s_div {\n  display: flex;\n  align-items: center;\n  padding-right: 16px;\n}\n\n.main_content_div .flex_div .s_div ion-icon {\n  font-size: 40px;\n}\n\n.main_content_div .flex_div .s_div .cross {\n  color: gray;\n}\n\n.main_content_div .flex_div .s_div .check {\n  color: var(--ion-color-main);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaW52aXRhdGlvbnMvRDpcXFJhZ3V2YXJhbiBpbWFnZVxcZGlzZWxzaGlwL3NyY1xcYXBwXFxwYWdlc1xcaW52aXRhdGlvbnNcXGludml0YXRpb25zLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvaW52aXRhdGlvbnMvaW52aXRhdGlvbnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUNBQUE7QUNDSjs7QURDQTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUNFSjs7QURDQTtFQUNJLGFBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7RUFDQSxpQ0FBQTtFQUNBLG1CQUFBO0FDRUo7O0FEQUk7RUFDSSxzQkFBQTtFQUNBLFlBQUE7QUNFUjs7QURDQTtFQUNJLFdBQUE7QUNFSjs7QURBSTtFQUNJLGNBQUE7QUNFUjs7QURDSTtFQUNJLGtCQUFBO0FDQ1I7O0FEQ1E7RUFDSSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQ0NaOztBREVRO0VBQ0UsWUFBQTtFQUNBLGlDQUFBO0FDQVY7O0FESUk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsOEJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0NBQUE7RUFDQSxpQkFBQTtBQ0ZSOztBREtRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ0haOztBREtZO0VBQ0ksaUJBQUE7QUNIaEI7O0FESWdCO0VBQ0ksV0FBQTtBQ0ZwQjs7QURLWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDSGhCOztBRE9RO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNMWjs7QURNWTtFQUNJLGVBQUE7QUNKaEI7O0FET1k7RUFDSSxXQUFBO0FDTGhCOztBRE9ZO0VBQ0ksNEJBQUE7QUNMaEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9pbnZpdGF0aW9ucy9pbnZpdGF0aW9ucy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaHtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5cbi5kaXZfaGVhZGVye1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcblxuICAgIGlvbi1zZWdtZW50LWJ1dHRvbntcbiAgICAgICAgLS1jb2xvci1jaGVja2VkOiB3aGl0ZTtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgIH1cbn1cbi5tYWluX2NvbnRlbnRfZGl2e1xuICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuXG4gICAgLmNoaXBfZGl2e1xuICAgICAgICBwYWRkaW5nOiAxMHB4IDE2cHg7XG5cbiAgICAgICAgaW9uLWNoaXB7XG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgY29sb3I6ICM1MDUwNTA7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjRTFFOUVFO1xuICAgICAgICB9XG5cbiAgICAgICAgLmFjdGl2ZV9jaGlwe1xuICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuZmxleF9kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG5cblxuICAgICAgICAuZl9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG5cbiAgICAgICAgICAgIC5kZXRhaWx7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgLmRlc2N7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOmdyYXk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmJhY2tfaW1hZ2V7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA2MHB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiA2MHB4O1xuICAgICAgICAgICAgICAgIG1pbi13aWR0aDogNjBweDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5zX2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICAgICAgICAgIGlvbi1pY29ue1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogNDBweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmNyb3Nze1xuICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmNoZWNre1xuICAgICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG4uZGl2X2hlYWRlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGhlaWdodDogNTBweDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uZGl2X2hlYWRlciBpb24tc2VnbWVudC1idXR0b24ge1xuICAtLWNvbG9yLWNoZWNrZWQ6IHdoaXRlO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiBpb24tbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jaGlwX2RpdiB7XG4gIHBhZGRpbmc6IDEwcHggMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jaGlwX2RpdiBpb24tY2hpcCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjNTA1MDUwO1xuICBiYWNrZ3JvdW5kOiAjRTFFOUVFO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNoaXBfZGl2IC5hY3RpdmVfY2hpcCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgd2lkdGg6IDEwMCU7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuZl9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmZfZGl2IC5kZXRhaWwge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuZl9kaXYgLmRldGFpbCAuZGVzYyB7XG4gIGNvbG9yOiBncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5mX2RpdiAuYmFja19pbWFnZSB7XG4gIGhlaWdodDogNjBweDtcbiAgd2lkdGg6IDYwcHg7XG4gIG1pbi13aWR0aDogNjBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLnNfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuc19kaXYgaW9uLWljb24ge1xuICBmb250LXNpemU6IDQwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLnNfZGl2IC5jcm9zcyB7XG4gIGNvbG9yOiBncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5zX2RpdiAuY2hlY2sge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/invitations/invitations.page.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/invitations/invitations.page.ts ***!
    \*******************************************************/

  /*! exports provided: InvitationsPage */

  /***/
  function srcAppPagesInvitationsInvitationsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InvitationsPage", function () {
      return InvitationsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/services/dummy-data.service */
    "./src/app/services/dummy-data.service.ts");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var InvitationsPage = /*#__PURE__*/function () {
      function InvitationsPage(dummy) {
        _classCallCheck(this, InvitationsPage);

        this.dummy = dummy;
        this.segId = 'ion-sb-0';
        this.users = this.dummy.users;
      }

      _createClass(InvitationsPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "segmentChanged",
        value: function segmentChanged(eve) {
          console.log(eve.detail.value);
          this.segId = eve.detail.value;
        }
      }, {
        key: "chipChange",
        value: function chipChange(val) {
          this.chipId = val;
        }
      }, {
        key: "chipChange1",
        value: function chipChange1(val) {
          this.chipId2 = val;
        }
      }]);

      return InvitationsPage;
    }();

    InvitationsPage.ctorParameters = function () {
      return [{
        type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__["dummyDataService"]
      }];
    };

    InvitationsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-invitations',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./invitations.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/invitations/invitations.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./invitations.page.scss */
      "./src/app/pages/invitations/invitations.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__["dummyDataService"]])], InvitationsPage);
    /***/
  },

  /***/
  "./src/app/services/dummy-data.service.ts":
  /*!************************************************!*\
    !*** ./src/app/services/dummy-data.service.ts ***!
    \************************************************/

  /*! exports provided: dummyDataService */

  /***/
  function srcAppServicesdummyDataServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "dummyDataService", function () {
      return dummyDataService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var dummyDataService = function dummyDataService() {
      _classCallCheck(this, dummyDataService);

      this.users = [{
        img: 'assets/imgs/user.jpg',
        cover: 'assets/imgs/back1.jpg',
        btn_text: 'VIEW JOBS'
      }, {
        img: 'assets/imgs/user2.jpg',
        cover: 'assets/imgs/back2.jpg',
        btn_text: 'FOLLOW HASHTAG'
      }, {
        img: 'assets/imgs/user3.jpg',
        cover: 'assets/imgs/back3.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user4.jpg',
        cover: 'assets/imgs/back4.jpg',
        btn_text: 'SEE ALL VIEW'
      }, {
        img: 'assets/imgs/user.jpg',
        cover: 'assets/imgs/back1.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user2.jpg',
        cover: 'assets/imgs/back2.jpg',
        btn_text: ''
      }, {
        img: 'assets/imgs/user3.jpg',
        cover: 'assets/imgs/back3.jpg',
        btn_text: 'EXPAND YOUR NETWORK'
      }, {
        img: 'assets/imgs/user4.jpg',
        cover: 'assets/imgs/back4.jpg',
        btn_text: 'SEE ALL VIEW'
      }];
      this.jobs = [{
        img: 'assets/imgs/company/initappz.png',
        title: 'SQL Server',
        addr: 'Bhavnagar, Gujrat, India',
        time: '2 school alumni',
        status: '1 day'
      }, {
        img: 'assets/imgs/company/google.png',
        title: 'Web Designer',
        addr: 'Vadodara, Gujrat, India',
        time: 'Be an early applicant',
        status: '2 days'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        title: 'Business Analyst',
        addr: 'Ahmedabad, Gujrat, India',
        time: 'Be an early applicant',
        status: '1 week'
      }, {
        img: 'assets/imgs/company/initappz.png',
        title: 'PHP Developer',
        addr: 'Pune, Maharashtra, India',
        time: '2 school alumni',
        status: 'New'
      }, {
        img: 'assets/imgs/company/google.png',
        title: 'Graphics Designer',
        addr: 'Bhavnagar, Gujrat, India',
        time: 'Be an early applicant',
        status: '1 day'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        title: 'Phython Developer',
        addr: 'Ahmedabad, Gujrat, India',
        time: '2 school alumni',
        status: '5 days'
      }];
      this.company = [{
        img: 'assets/imgs/company/initappz.png',
        name: 'Initappz Shop'
      }, {
        img: 'assets/imgs/company/microsoft.png',
        name: 'Microsoft'
      }, {
        img: 'assets/imgs/company/google.png',
        name: 'Google'
      }];
    };

    dummyDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], dummyDataService);
    /***/
  }
}]);
//# sourceMappingURL=pages-invitations-invitations-module-es5.js.map
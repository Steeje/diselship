function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-group-detail-group-detail-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/group-detail/group-detail.page.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/group-detail/group-detail.page.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesGroupDetailGroupDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-button (click)=\"BackButton()\" slot=\"start\">\n      <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n    </ion-button>\n    <ion-title color=\"light\">{{data.name}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n    <div class=\"cover\" [style.backgroundImage]=\"'url('+data.cover+')'\">\n      <div class=\"user_image\" style=\"    background-position: center;\n      background-size: cover;\n      background-repeat: no-repeat;\" [style.backgroundImage]=\"'url('+data.media_url+')'\">\n       \n        \n      </div>\n      <div class=\"data_div\">\n        <ion-label class=\"group_title\">{{data.name}}</ion-label>\n        <ion-label class=\"group_type\">{{data.status}} / {{data.group_type_label}} . {{data.members_count}} Memers</ion-label>\n        <ion-label class=\"description\">\n          {{data.description}}\n          \n          </ion-label>\n         \n        <!-- <div class=\"admin\">\n          <img src=\"assets/imgs/clock.png\">\n          <ion-label class=\"admin_name\">John</ion-label>\n        </div> -->\n\n        <div style=\"width: 50%;position: relative;margin: auto;\">\n          <ion-chip style=\"background-color: white;\">\n            <ion-avatar>\n              <img src=\"{{data.admin_image}}\">\n            </ion-avatar>\n            <ion-label>{{data.admin_name}}</ion-label>\n          </ion-chip>\n        </div>\n        </div>\n       \n    </div>\n\n    <div class=\"personal_div\">\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px; border-top: 0px\" (click)=\" feeds()\" >\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"pulse-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Feed </p>\n         \n        </ion-label>\n      </div>\n      <div  class=\"white_div\" style=\"margin-bottom: 0px;\" (click)=\" members()\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"person-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Members </p>\n         \n        </ion-label>\n      </div>\n      <div  class=\"white_div\" style=\"margin-bottom: 0px;\" (click)=\"photos()\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"images-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Photos </p>\n         \n        </ion-label>\n      </div>\n      <div  class=\"white_div\" style=\"margin-bottom: 0px;\" (click)=\"editProfile()\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"chatbubbles-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Discussion </p>\n         \n        </ion-label>\n      </div>\n      <div  class=\"white_div\" style=\"margin-bottom: 0px;\"  [routerLink]=\"['/group-invites', id1]\" >\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"person-add-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Send Invites </p>\n         \n        </ion-label>\n      </div>\n      <!-- <div  class=\"white_div\" style=\"margin-bottom: 0px;\" (click)=\"editProfile()\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"chatbox-ellipses-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Send messages </p>\n         \n        </ion-label>\n      </div> -->\n      <div  class=\"white_div\" style=\"margin-bottom: 0px;\" (click)=\"craetegroup()\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"settings-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\">Manage</p>\n         \n        </ion-label>\n      </div>\n     \n\n      <!-- <ion-grid fixed>\n        <ion-row>\n          <ion-col size=\"6\">\n            <ion-button expand=\"block\" class=\"save\" size=\"small\" fill=\"outline\">\n              SAVE\n            </ion-button>\n          </ion-col>\n          <ion-col size=\"6\">\n            <ion-button size=\"small\" class=\"apply\" expand=\"block\">\n              APPLY\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid> -->\n    </div>\n\n    <!-- <div class=\"job_div\">\n      <ion-label class=\"job_header\">Job Description</ion-label>\n      <ion-label>Web Designer - No of opening : 6</ion-label>\n      <ion-label>Required Experience : 1 - 3 Years</ion-label>\n      <ion-label>Job brief We are looking for a talented Web..</ion-label>\n    </div>\n\n    <div class=\"white_div\">\n      <ion-label class=\"blue_lbl\" style=\"text-align: center;\">SEE MORE</ion-label>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>Set alert for similar jobs</ion-label>\n      <ion-label class=\"light_lbl\">Web Designer, Bhavnagar, India</ion-label>\n      <ion-toggle></ion-toggle>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>See company insights for Initappz Shop with Premiun</ion-label>\n      <ion-button class=\"premium_btn\" expand=\"block\">\n        TRY PREMIUM FOR FREE\n      </ion-button>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>Keep up with updates and jobs</ion-label>\n      <div class=\"flex_div\">\n        <img src=\"assets/imgs/user.jpg\" class=\"image\">\n        <div class=\"content_div\">\n          <ion-label class=\"head_lbl\">Initappz Shop</ion-label>\n          <ion-label class=\"small_lbl\">E-learning 22,300 followers</ion-label>\n        </div>\n        <div>\n          <ion-button class=\"fallow_btn\" expand=\"block\" fill=\"clear\" size=\"small\">\n            + Follow\n          </ion-button>\n        </div>\n      </div>\n\n    </div> -->\n  </div>\n</ion-content>\n<!-- <ion-footer>\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button expand=\"block\" class=\"save\" size=\"small\" fill=\"outline\">\n          SAVE\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-button size=\"small\" class=\"apply\" expand=\"block\">\n          APPLY\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer> -->";
    /***/
  },

  /***/
  "./src/app/pages/group-detail/group-detail-routing.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/pages/group-detail/group-detail-routing.module.ts ***!
    \*******************************************************************/

  /*! exports provided: GroupDetailPageRoutingModule */

  /***/
  function srcAppPagesGroupDetailGroupDetailRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GroupDetailPageRoutingModule", function () {
      return GroupDetailPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _group_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./group-detail.page */
    "./src/app/pages/group-detail/group-detail.page.ts");

    var routes = [{
      path: '',
      component: _group_detail_page__WEBPACK_IMPORTED_MODULE_3__["GroupDetailPage"]
    }];

    var GroupDetailPageRoutingModule = function GroupDetailPageRoutingModule() {
      _classCallCheck(this, GroupDetailPageRoutingModule);
    };

    GroupDetailPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], GroupDetailPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/group-detail/group-detail.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/pages/group-detail/group-detail.module.ts ***!
    \***********************************************************/

  /*! exports provided: GroupDetailPageModule */

  /***/
  function srcAppPagesGroupDetailGroupDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GroupDetailPageModule", function () {
      return GroupDetailPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _group_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./group-detail-routing.module */
    "./src/app/pages/group-detail/group-detail-routing.module.ts");
    /* harmony import */


    var _group_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./group-detail.page */
    "./src/app/pages/group-detail/group-detail.page.ts");

    var GroupDetailPageModule = function GroupDetailPageModule() {
      _classCallCheck(this, GroupDetailPageModule);
    };

    GroupDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _group_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["GroupDetailPageRoutingModule"]],
      declarations: [_group_detail_page__WEBPACK_IMPORTED_MODULE_6__["GroupDetailPage"]]
    })], GroupDetailPageModule);
    /***/
  },

  /***/
  "./src/app/pages/group-detail/group-detail.page.scss":
  /*!***********************************************************!*\
    !*** ./src/app/pages/group-detail/group-detail.page.scss ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesGroupDetailGroupDetailPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\nion-toolbar ion-input {\n  border-bottom: 1px solid white;\n  --color: white;\n  --placeholder-color: white;\n  --padding-start: 8px;\n}\nion-toolbar ion-icon {\n  color: white;\n  font-size: 24px;\n}\n.save {\n  --border-color: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 16px;\n  --border-radius: 3px;\n  color: var(--ion-color-main);\n}\n.apply {\n  --background: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 16px;\n  --border-radius: 3px;\n}\nion-content {\n  --background: #F5F5F5;\n}\n.white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n.white_div ion-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  right: 16px;\n  font-size: 20px;\n}\n.white_div ion-button {\n  color: var(--ion-color-main);\n  font-size: 600;\n}\n.main_content_div {\n  width: 100%;\n}\n.main_content_div .no_border {\n  border-bottom: none !important;\n}\n.main_content_div ion-label {\n  display: block;\n}\n.main_content_div .cover {\n  width: 100%;\n  height: 250px;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  position: relative;\n}\n.main_content_div .cover .user_image {\n  width: 70px;\n  height: 70px;\n  border-radius: 15px;\n  position: absolute;\n  left: 40%;\n  top: 25%;\n  background: white;\n  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.2);\n}\n.main_content_div .cover .data_div {\n  padding: 16px;\n  padding-top: 114px;\n  padding-right: 10%;\n}\n.main_content_div .cover .data_div ion-label {\n  margin-bottom: 2px;\n}\n.main_content_div .cover .data_div .group_title {\n  text-align: center;\n  font-size: 20px;\n  padding-top: 30px;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n  font-weight: bolder;\n}\n.main_content_div .cover .data_div .group_type {\n  font-size: 14px;\n  text-align: center;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n}\n.main_content_div .cover .data_div .description {\n  font-size: 14px;\n  text-align: center;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n}\n.main_content_div .cover .data_div .admin {\n  display: flex;\n  padding-bottom: 5px;\n  text-align: center;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n}\n.main_content_div .cover .data_div .admin img {\n  width: 15px;\n  height: 15px;\n  text-align: center;\n}\n.main_content_div .cover .data_div .admin .admin_name {\n  text-align: center;\n  margin-left: 10px;\n  padding: 0;\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .personal_div {\n  background: white;\n  padding: 16px;\n}\n.main_content_div .personal_div ion-label {\n  margin-bottom: 2px;\n}\n.main_content_div .personal_div .job_title {\n  font-size: 20px;\n  padding-top: 30px;\n}\n.main_content_div .personal_div .compamy_name {\n  font-size: 14px;\n}\n.main_content_div .personal_div .address {\n  font-size: 14px;\n}\n.main_content_div .personal_div .easy_div {\n  display: flex;\n  padding-bottom: 5px;\n}\n.main_content_div .personal_div .easy_div img {\n  width: 15px;\n  height: 15px;\n}\n.main_content_div .personal_div .easy_div .easy {\n  font-size: 12px;\n  margin-left: 10px;\n  padding: 0;\n}\n.main_content_div .personal_div .easy_div .time {\n  margin-left: 10px;\n  padding: 0;\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .job_div {\n  background: white;\n  padding: 16px;\n  margin-top: 10px;\n}\n.main_content_div .job_div .job_header {\n  padding-bottom: 5px;\n  font-size: 16px !important;\n}\n.main_content_div .job_div ion-label {\n  font-size: 14px;\n  padding-bottom: 5px;\n}\n.main_content_div .white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n  border-top: 1px solid lightgray;\n}\n.main_content_div .white_div .blue_lbl {\n  color: var(--ion-color-main);\n  padding-top: 15px;\n  padding-bottom: 15px;\n  font-weight: 600;\n}\n.main_content_div .white_div .left-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  left: 16px;\n  font-size: 26px;\n}\n.main_content_div .white_div .lable-icon {\n  padding-left: 22px;\n  padding-top: 5px;\n  color: var(--ion-color-main);\n  font-size: 17px;\n  cursor: pointer;\n}\n.main_content_div .white_div .right-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  right: 16px;\n  font-size: 20px;\n}\n.main_content_div .desc_div {\n  margin-top: 10px;\n  padding: 16px;\n  background: white;\n  position: relative;\n}\n.main_content_div .desc_div .light_lbl {\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .desc_div ion-toggle {\n  position: absolute;\n  right: 16px;\n  top: 50%;\n  transform: translateY(-50%);\n}\n.main_content_div .desc_div .premium_btn {\n  margin-top: 10px;\n  --border-radius: 3px;\n  height: 40px;\n  --background: #78710c;\n}\n.main_content_div .desc_div .flex_div {\n  display: flex;\n  align-items: center;\n  padding-top: 10px;\n  width: 100%;\n  justify-content: space-between;\n}\n.main_content_div .desc_div .flex_div .image {\n  height: 45px;\n  width: 45px;\n  max-width: 45px;\n  border-radius: 3px;\n}\n.main_content_div .desc_div .flex_div .content_div {\n  margin-left: 10px;\n}\n.main_content_div .desc_div .flex_div .content_div .head_lbl {\n  font-weight: 600;\n}\n.main_content_div .desc_div .flex_div .content_div .small_lbl {\n  font-size: 14px;\n}\n.main_content_div .desc_div .flex_div .fallow_btn {\n  font-size: 14px;\n  font-weight: 600;\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZ3JvdXAtZGV0YWlsL0Q6XFxSYWd1dmFyYW4gaW1hZ2VcXGRpc2Vsc2hpcC9zcmNcXGFwcFxccGFnZXNcXGdyb3VwLWRldGFpbFxcZ3JvdXAtZGV0YWlsLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZ3JvdXAtZGV0YWlsL2dyb3VwLWRldGFpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQ0FBQTtBQ0NKO0FEQUk7RUFDSSw4QkFBQTtFQUNBLGNBQUE7RUFDQSwwQkFBQTtFQUNBLG9CQUFBO0FDRVI7QURBSTtFQUNJLFlBQUE7RUFDQSxlQUFBO0FDRVI7QURDQTtFQUNJLHFDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7RUFDQSw0QkFBQTtBQ0VKO0FEQUE7RUFDSSxtQ0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLG9CQUFBO0FDR0o7QUREQTtFQUNJLHFCQUFBO0FDSUo7QURGQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDS0o7QURVSTtFQUNJLDRCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQ1JSO0FEV0k7RUFDSSw0QkFBQTtFQUNBLGNBQUE7QUNUUjtBRFlBO0VBQ0ksV0FBQTtBQ1RKO0FEV0k7RUFDSSw4QkFBQTtBQ1RSO0FEWUk7RUFDSSxjQUFBO0FDVlI7QURhSTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7QUNYUjtBRFlRO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFLQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSxpQkFBQTtFQUNBLDBDQUFBO0FDZFo7QURnQlE7RUFDSSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ2RaO0FEZ0JZO0VBQ0ksa0JBQUE7QUNkaEI7QURpQlk7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FDZmhCO0FEbUJZO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0FDakJoQjtBRG1CWTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtBQ2pCaEI7QURtQlk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtBQ2pCaEI7QURrQmdCO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ2hCcEI7QURrQmdCO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2hCcEI7QUR3Qkk7RUFDSSxpQkFBQTtFQUNBLGFBQUE7QUN0QlI7QUR3QlE7RUFDSSxrQkFBQTtBQ3RCWjtBRHlCUTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtBQ3ZCWjtBRHlCUTtFQUNJLGVBQUE7QUN2Qlo7QUR5QlE7RUFDSSxlQUFBO0FDdkJaO0FEeUJRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FDdkJaO0FEd0JZO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUN0QmhCO0FEd0JZO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtBQ3RCaEI7QUR3Qlk7RUFDSSxpQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ3RCaEI7QUQyQkk7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQ3pCUjtBRDRCUTtFQUNJLG1CQUFBO0VBQ0EsMEJBQUE7QUMxQlo7QUQ2QlE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QUMzQlo7QUQ4Qkk7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLCtCQUFBO0FDNUJSO0FENkJRO0VBQ0ksNEJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7QUMzQlo7QUQ4Qkk7RUFDSSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUM1QlI7QUQ4Qkk7RUFDTSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQzVCVjtBRDhCSTtFQUNJLDRCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQzVCUjtBRGlDSTtFQUNJLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUMvQlI7QURpQ1E7RUFDSSxlQUFBO0VBQ0EsV0FBQTtBQy9CWjtBRGtDUTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtBQ2hDWjtBRG1DUTtFQUNJLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7QUNqQ1o7QURvQ1E7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSw4QkFBQTtBQ2xDWjtBRG1DWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDakNoQjtBRG9DWTtFQUNJLGlCQUFBO0FDbENoQjtBRG1DZ0I7RUFDSSxnQkFBQTtBQ2pDcEI7QURtQ2dCO0VBQ0ksZUFBQTtBQ2pDcEI7QURxQ1k7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0FDbkNoQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2dyb3VwLWRldGFpbC9ncm91cC1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGlvbi1pbnB1dHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAgICAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgLS1wbGFjZWhvbGRlci1jb2xvciA6IHdoaXRlO1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQgOiA4cHg7XG4gICAgfVxuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICB9XG59XG4uc2F2ZXtcbiAgICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG4uYXBwbHl7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG59XG5pb24tY29udGVudHtcbiAgICAtLWJhY2tncm91bmQ6ICNGNUY1RjU7XG59XG4ud2hpdGVfZGl2e1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHBhZGRpbmctbGVmdDogMTZweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgLy8gaW9uLWJ1dHRvbntcbiAgICAvLyAgICAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgLy8gICAgIGlvbi1sYWJlbCB7XG4gICAgLy8gICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIC8vICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgLy8gICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgLy8gICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIC8vICAgICB9ICAgXG4gICAgLy8gICAgIGlvbi1pY29ue1xuICAgIC8vICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAvLyAgICAgfVxuICAgIC8vIH1cblxuICAgIFxuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHJpZ2h0OiAxNnB4O1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgfVxuXG4gICAgaW9uLWJ1dHRvbntcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgZm9udC1zaXplOiA2MDA7XG4gICAgfVxufVxuLm1haW5fY29udGVudF9kaXZ7XG4gICAgd2lkdGg6IDEwMCU7XG5cbiAgICAubm9fYm9yZGVye1xuICAgICAgICBib3JkZXItYm90dG9tOiBub25lICFpbXBvcnRhbnQ7XG4gICAgfVxuXG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuXG4gICAgLmNvdmVye1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAyNTBweDtcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIC51c2VyX2ltYWdle1xuICAgICAgICAgICAgd2lkdGg6IDcwcHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDcwcHg7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIGxlZnQ6IDQwJTtcbiAgICAgICAgICAgIHRvcDogMjUlO1xuICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggM3B4IDZweCByZ2JhKDAsMCwwLDAuMik7XG4gICAgICAgIH1cbiAgICAgICAgLmRhdGFfZGl2e1xuICAgICAgICAgICAgcGFkZGluZzogMTZweDtcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMTRweDtcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDEwJTtcbiAgICBcbiAgICAgICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMnB4O1xuICAgICAgICAgICAgfVxuICAgIFxuICAgICAgICAgICAgLmdyb3VwX3RpdGxle1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICAgICAgcGFkZGluZy10b3A6IDMwcHg7XG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgICAgIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICAuZ3JvdXBfdHlwZXtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgICAgICB0ZXh0LXNoYWRvdzogMnB4IDJweCA4cHggYmxhY2s7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuZGVzY3JpcHRpb257XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgICAgICAgICAgdGV4dC1zaGFkb3c6IDJweCAycHggOHB4IGJsYWNrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmFkbWlue1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgICAgIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbiAgICAgICAgICAgICAgICBpbWd7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxNXB4O1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDE1cHg7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLmFkbWluX25hbWV7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgXG4gICAgfVxuXG4gICAgICAgXG4gICAgLnBlcnNvbmFsX2RpdntcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XG5cbiAgICAgICAgaW9uLWxhYmVsIHtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDJweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5qb2JfdGl0bGV7XG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMzBweDtcbiAgICAgICAgfVxuICAgICAgICAuY29tcGFteV9uYW1le1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICB9XG4gICAgICAgIC5hZGRyZXNze1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICB9XG4gICAgICAgIC5lYXN5X2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgICAgaW1ne1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxNXB4O1xuICAgICAgICAgICAgICAgIGhlaWdodDogMTVweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5lYXN5e1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnRpbWV7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuam9iX2RpdntcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG5cblxuICAgICAgICAuam9iX2hlYWRlcntcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHggIWltcG9ydGFudDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICB9XG4gICAgfVxuICAgIC53aGl0ZV9kaXZ7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgIC5ibHVlX2xibHtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTVweDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAubGVmdC1pY29ue1xuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGxlZnQ6IDE2cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICB9XG4gICAgLmxhYmxlLWljb257XG4gICAgICAgICAgcGFkZGluZy1sZWZ0OiAyMnB4O1xuICAgICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XG4gICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICBmb250LXNpemU6IDE3cHg7XG4gICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIH1cbiAgICAucmlnaHQtaWNvbntcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICByaWdodDogMTZweDtcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgIH1cblxuICAgIH1cblxuICAgIC5kZXNjX2RpdntcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgcGFkZGluZzogMTZweDtcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICAgICAgICAubGlnaHRfbGJse1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgIH1cblxuICAgICAgICBpb24tdG9nZ2xle1xuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgcmlnaHQ6IDE2cHg7XG4gICAgICAgICAgICB0b3A6IDUwJTtcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC5wcmVtaXVtX2J0bntcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgICAgICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDogIzc4NzEwYztcbiAgICAgICAgfVxuXG4gICAgICAgIC5mbGV4X2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgICAgIC5pbWFnZXtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDQ1cHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDQ1cHg7XG4gICAgICAgICAgICAgICAgbWF4LXdpZHRoOiA0NXB4O1xuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmNvbnRlbnRfZGl2e1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgIC5oZWFkX2xibHtcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLnNtYWxsX2xibHtcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmZhbGxvd19idG57XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufSIsImlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5pb24tdG9vbGJhciBpb24taW5wdXQge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG4gIC0tY29sb3I6IHdoaXRlO1xuICAtLXBsYWNlaG9sZGVyLWNvbG9yOiB3aGl0ZTtcbiAgLS1wYWRkaW5nLXN0YXJ0OiA4cHg7XG59XG5pb24tdG9vbGJhciBpb24taWNvbiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAyNHB4O1xufVxuXG4uc2F2ZSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5cbi5hcHBseSB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDE2cHg7XG4gIC0tYm9yZGVyLXJhZGl1czogM3B4O1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogI0Y1RjVGNTtcbn1cblxuLndoaXRlX2RpdiB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG4ud2hpdGVfZGl2IGlvbi1pY29uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTZweDtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuLndoaXRlX2RpdiBpb24tYnV0dG9uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC1zaXplOiA2MDA7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAubm9fYm9yZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogbm9uZSAhaW1wb3J0YW50O1xufVxuLm1haW5fY29udGVudF9kaXYgaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY292ZXIge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyNTBweDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY292ZXIgLnVzZXJfaW1hZ2Uge1xuICB3aWR0aDogNzBweDtcbiAgaGVpZ2h0OiA3MHB4O1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDQwJTtcbiAgdG9wOiAyNSU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3gtc2hhZG93OiAwcHggM3B4IDZweCByZ2JhKDAsIDAsIDAsIDAuMik7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY292ZXIgLmRhdGFfZGl2IHtcbiAgcGFkZGluZzogMTZweDtcbiAgcGFkZGluZy10b3A6IDExNHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY292ZXIgLmRhdGFfZGl2IGlvbi1sYWJlbCB7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgLmdyb3VwX3RpdGxlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDIwcHg7XG4gIHBhZGRpbmctdG9wOiAzMHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgLmdyb3VwX3R5cGUge1xuICBmb250LXNpemU6IDE0cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6IHdoaXRlO1xuICB0ZXh0LXNoYWRvdzogMnB4IDJweCA4cHggYmxhY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY292ZXIgLmRhdGFfZGl2IC5kZXNjcmlwdGlvbiB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgLmFkbWluIHtcbiAgZGlzcGxheTogZmxleDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgLmFkbWluIGltZyB7XG4gIHdpZHRoOiAxNXB4O1xuICBoZWlnaHQ6IDE1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgLmFkbWluIC5hZG1pbl9uYW1lIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgcGFkZGluZzogMDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogZ3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZzogMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgaW9uLWxhYmVsIHtcbiAgbWFyZ2luLWJvdHRvbTogMnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiAuam9iX3RpdGxlIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nLXRvcDogMzBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgLmNvbXBhbXlfbmFtZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgLmFkZHJlc3Mge1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfZGl2IC5lYXN5X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfZGl2IC5lYXN5X2RpdiBpbWcge1xuICB3aWR0aDogMTVweDtcbiAgaGVpZ2h0OiAxNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiAuZWFzeV9kaXYgLmVhc3kge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nOiAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiAuZWFzeV9kaXYgLnRpbWUge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgcGFkZGluZzogMDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogZ3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5qb2JfZGl2IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDE2cHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuam9iX2RpdiAuam9iX2hlYWRlciB7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gIGZvbnQtc2l6ZTogMTZweCAhaW1wb3J0YW50O1xufVxuLm1haW5fY29udGVudF9kaXYgLmpvYl9kaXYgaW9uLWxhYmVsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLndoaXRlX2RpdiB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAud2hpdGVfZGl2IC5ibHVlX2xibCB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIHBhZGRpbmctdG9wOiAxNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC53aGl0ZV9kaXYgLmxlZnQtaWNvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMTZweDtcbiAgZm9udC1zaXplOiAyNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLndoaXRlX2RpdiAubGFibGUtaWNvbiB7XG4gIHBhZGRpbmctbGVmdDogMjJweDtcbiAgcGFkZGluZy10b3A6IDVweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC1zaXplOiAxN3B4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAud2hpdGVfZGl2IC5yaWdodC1pY29uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTZweDtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgcGFkZGluZzogMTZweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAubGlnaHRfbGJsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogZ3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiBpb24tdG9nZ2xlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTZweDtcbiAgdG9wOiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAucHJlbWl1bV9idG4ge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICAtLWJhY2tncm91bmQ6ICM3ODcxMGM7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IC5pbWFnZSB7XG4gIGhlaWdodDogNDVweDtcbiAgd2lkdGg6IDQ1cHg7XG4gIG1heC13aWR0aDogNDVweDtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiAuY29udGVudF9kaXYge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IC5oZWFkX2xibCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiAuc21hbGxfbGJsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiAuZmFsbG93X2J0biB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbWFyZ2luOiAwO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/group-detail/group-detail.page.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/group-detail/group-detail.page.ts ***!
    \*********************************************************/

  /*! exports provided: GroupDetailPage */

  /***/
  function srcAppPagesGroupDetailGroupDetailPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GroupDetailPage", function () {
      return GroupDetailPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic-native/native-page-transitions/ngx */
    "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var GroupDetailPage = /*#__PURE__*/function () {
      function GroupDetailPage(navCtrl, nativePageTransitions, router, location, route, api) {
        _classCallCheck(this, GroupDetailPage);

        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.router = router;
        this.location = location;
        this.route = route;
        this.api = api;
        this.data = {};
        this.creator = {};
        this.options = {
          direction: 'up',
          duration: 500,
          slowdownfactor: 3,
          slidePixels: 20,
          iosdelay: 100,
          androiddelay: 150,
          fixedPixelsTop: 0,
          fixedPixelsBottom: 60
        };
      } // gotoGroupFeed() {
      //   this.router.navigate(['/group-feed']);
      // }


      _createClass(GroupDetailPage, [{
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.nativePageTransitions.slide(this.options);
        } // example of adding a transition when pushing a new page

      }, {
        key: "openPage",
        value: function openPage(page) {
          this.nativePageTransitions.slide(this.options);
          this.navCtrl.navigateForward(page);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.id1 = this.route.snapshot.paramMap.get('id');
          this.api.getGroupdetails(this.id1).subscribe(function (res) {
            console.log(res);
            _this.data = res;
            console.log(_this.data);

            _this.api.getuser(_this.data['creator_id']).subscribe(function (res) {
              console.log(res);
              _this.creator = res;
              console.log(_this.creator['name']);
              _this.data.admin_name = res.name;
              _this.data.admin_image = res.avatar_urls['full'];
            });
          });
        }
      }, {
        key: "BackButton",
        value: function BackButton() {
          this.location.back();
        }
      }, {
        key: "members",
        value: function members() {
          var navigationExtras = {
            queryParams: {
              id: this.id1,
              name: this.data.name
            }
          };
          this.router.navigate(['members-list'], navigationExtras);
        }
      }, {
        key: "feeds",
        value: function feeds() {
          var navigationExtras = {
            queryParams: {
              id: this.id1,
              name: this.data.name
            }
          };
          this.router.navigate(['group-feed'], navigationExtras);
        }
      }, {
        key: "craetegroup",
        value: function craetegroup() {
          var navigationExtras = {
            queryParams: {
              id: this.id1,
              name: this.data.name,
              description: this.data.full_description,
              cover: this.data.cover,
              media_url: this.data.media_url
            }
          };
          this.router.navigate(['group-update'], navigationExtras);
        }
      }, {
        key: "photos",
        value: function photos() {
          var navigationExtras = {
            queryParams: {
              id: this.id1,
              name: this.data.name,
              type: "group_id"
            }
          };
          this.router.navigate(['photo-gallery'], navigationExtras);
        }
      }]);

      return GroupDetailPage;
    }();

    GroupDetailPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"]
      }, {
        type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__["NativePageTransitions"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"]
      }];
    };

    GroupDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-group-detail',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./group-detail.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/group-detail/group-detail.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./group-detail.page.scss */
      "./src/app/pages/group-detail/group-detail.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"], _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__["NativePageTransitions"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"]])], GroupDetailPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-group-detail-group-detail-module-es5.js.map
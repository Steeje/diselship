function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-job-seagoing-job-seagoing-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-seagoing/job-seagoing.page.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-seagoing/job-seagoing.page.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesJobSeagoingJobSeagoingPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-button (click)=\"BackButton()\" slot=\"start\">\n      <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n    </ion-button>\n    <ion-title color=\"light\"> \n      Sea Going Service </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n\n\n\n    <div class=\"form_div\">\n   \n      <form [formGroup]=\"userForm\" novalidate (ngSubmit)=\"userupdate()\">\n\n\n\n        \n\n        <ion-item>\n          <ion-label position=\"floating\">Ship Name</ion-label>\n          <ion-input  formControlName=\"sea_ship_name1\" [(ngModel)]=\"data.sea_ship_name1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_ship_name1.errors?.required\">\n          Ship Name is required.\n        </span>\n\n\n        <ion-item>\n          <ion-label position=\"floating\">Official No </ion-label>\n          <ion-input  formControlName=\"sea_official_no1\" [(ngModel)]=\"data.sea_official_no1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_official_no1.errors?.required\">\n          Official No is required.\n        </span>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_official_no1.errors?.pattern\">\n          Only numerical values allowed.\n        </span>\n\n        <ion-item>\n          <ion-label position=\"floating\">IMO Number</ion-label>\n          <ion-input  formControlName=\"sea_imo_number1\" [(ngModel)]=\"data.sea_imo_number1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_imo_number1.errors?.required\">\n          IMO Number is required.\n        </span>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_imo_number1.errors?.pattern\">\n          Only numerical values allowed.\n        </span>\n  \n        <ion-item>\n          <ion-label position=\"floating\">GT</ion-label>\n          <ion-input type=\"text\" formControlName=\"sea_gt1\" [(ngModel)]=\"data.sea_gt1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_gt1.errors?.required\">\n          GT is required.\n        </span>\n\n        <ion-item>\n          <ion-label position=\"floating\">Rank </ion-label>\n          <ion-input type=\"text\" formControlName=\"sea_rank1\" [(ngModel)]=\"data.sea_rank1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_rank1.errors?.required\">\n          Rank is required.\n        </span>\n\n        <ion-item>\n          <ion-label position=\"floating\">Nature of watch keeping </ion-label>\n          <ion-input type=\"text\" formControlName=\"sea_nature_of_watch_keeping1\" [(ngModel)]=\"data.sea_nature_of_watch_keeping1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_nature_of_watch_keeping1.errors?.required\">\n          Nature of watch keeping is required.\n        </span>\n        <ion-item>\n          <ion-label position=\"floating\">Service From (Date)</ion-label>\n          <ion-datetime (ionChange)=\"getDate2($event)\"  [(ngModel)]=\"data.sea_service_from_date1\"  class=\"ip\" formControlName=\"sea_service_from_date1\" [value]=\"defaultDate\" required></ion-datetime>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_service_from_date1.errors?.required\">\n          Service From (Date) is required.\n        </span>\n        <ion-item>\n          <ion-label position=\"floating\">Article of Months </ion-label>\n          <ion-input type=\"text\" formControlName=\"sea_article_of_months1\" [(ngModel)]=\"data.sea_article_of_months1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_article_of_months1.errors?.required\">\n          Article of Months is required.\n        </span>\n        <ion-item>\n          <ion-label position=\"floating\">Propulsion power(KW) </ion-label>\n          <ion-input type=\"text\" formControlName=\"sea_propulsion_power1\" [(ngModel)]=\"data.sea_propulsion_power1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_propulsion_power1.errors?.required\">\n          Propulsion power(KW) is required.\n        </span>\n\n\n        <ion-item>\n          <ion-label position=\"floating\">Propelling days </ion-label>\n          <ion-input type=\"text\" formControlName=\"sea_propelling_days1\" [(ngModel)]=\"data.sea_propelling_days1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_propelling_days1.errors?.required\">\n          Propelling days is required.\n        </span>\n\n\n        <ion-item>\n          <ion-label position=\"floating\">Remarks </ion-label>\n          <ion-input type=\"text\" formControlName=\"sea_remarks1\" [(ngModel)]=\"data.sea_remarks1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_remarks1.errors?.required\">\n          Remarks is required.\n        </span>\n\n\n        <ion-item>\n          <ion-label position=\"floating\">Flag  </ion-label>\n          <ion-input type=\"text\" formControlName=\"sea_flag1\" [(ngModel)]=\"data.sea_flag1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_flag1.errors?.required\">\n          Flag is required.\n        </span>\n\n\n        <ion-item>\n          <ion-label position=\"floating\">Port of Registry  </ion-label>\n          <ion-input type=\"text\" formControlName=\"sea_port_of_registry1\" [(ngModel)]=\"data.sea_port_of_registry1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_port_of_registry1.errors?.required\">\n          Port of Registry is required.\n        </span>\n\n        <ion-item>\n          <ion-label position=\"floating\">Ship Type  </ion-label>\n          <ion-input type=\"text\" formControlName=\"sea_ship_type1\" [(ngModel)]=\"data.sea_ship_type1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_ship_type1.errors?.required\">\n          Ship Type is required.\n        </span>\n       \n\n        <ion-item>\n          <ion-label position=\"floating\">Trade Area  </ion-label>\n          <ion-input type=\"text\" formControlName=\"sea_trade_area1\" [(ngModel)]=\"data.sea_trade_area1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_trade_area1.errors?.required\">\n          Trade Area is required.\n        </span>\n       \n        <ion-item>\n          <ion-label position=\"floating\">Service To (Date)</ion-label>\n          <ion-datetime (ionChange)=\"getDate($event)\"  [(ngModel)]=\"data.sea_service_to_date1\"  class=\"ip\" formControlName=\"sea_service_to_date1\" [value]=\"defaultDate\" required></ion-datetime>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_service_to_date1.errors?.required\">\n          Service To (Date) is required.\n        </span>\n       \n\n        <ion-item>\n          <ion-label position=\"floating\">Article Days  </ion-label>\n          <ion-input type=\"text\" formControlName=\"sea_article_days1\" [(ngModel)]=\"data.sea_article_days1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_article_days1.errors?.required\">\n          Article Days is required.\n        </span>\n\n\n        \n        <ion-item>\n          <ion-label position=\"floating\">Propulsion Type  </ion-label>\n          <ion-input type=\"text\" formControlName=\"sea_propulsion_type1\" [(ngModel)]=\"data.sea_propulsion_type1\" class=\"ip\" required></ion-input>\n        </ion-item>\n        <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.sea_propulsion_type1.errors?.required\">\n          Propulsion Type is required.\n        </span>\n\n      \n\n\n      <ion-button class=\"join_now\"  type=\"submit\" expand=\"full\">\n         <ion-spinner *ngIf=\"loading2\"></ion-spinner> <ion-label >   Update </ion-label>\n      </ion-button>\n      </form>\n    </div>\n\n  \n  </div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/job-seagoing/job-seagoing-routing.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/pages/job-seagoing/job-seagoing-routing.module.ts ***!
    \*******************************************************************/

  /*! exports provided: JobSeagoingPageRoutingModule */

  /***/
  function srcAppPagesJobSeagoingJobSeagoingRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "JobSeagoingPageRoutingModule", function () {
      return JobSeagoingPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _job_seagoing_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./job-seagoing.page */
    "./src/app/pages/job-seagoing/job-seagoing.page.ts");

    var routes = [{
      path: '',
      component: _job_seagoing_page__WEBPACK_IMPORTED_MODULE_3__["JobSeagoingPage"]
    }];

    var JobSeagoingPageRoutingModule = function JobSeagoingPageRoutingModule() {
      _classCallCheck(this, JobSeagoingPageRoutingModule);
    };

    JobSeagoingPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], JobSeagoingPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/job-seagoing/job-seagoing.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/pages/job-seagoing/job-seagoing.module.ts ***!
    \***********************************************************/

  /*! exports provided: JobSeagoingPageModule */

  /***/
  function srcAppPagesJobSeagoingJobSeagoingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "JobSeagoingPageModule", function () {
      return JobSeagoingPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _job_seagoing_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./job-seagoing-routing.module */
    "./src/app/pages/job-seagoing/job-seagoing-routing.module.ts");
    /* harmony import */


    var _job_seagoing_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./job-seagoing.page */
    "./src/app/pages/job-seagoing/job-seagoing.page.ts");

    var JobSeagoingPageModule = function JobSeagoingPageModule() {
      _classCallCheck(this, JobSeagoingPageModule);
    };

    JobSeagoingPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _job_seagoing_routing_module__WEBPACK_IMPORTED_MODULE_5__["JobSeagoingPageRoutingModule"]],
      declarations: [_job_seagoing_page__WEBPACK_IMPORTED_MODULE_6__["JobSeagoingPage"]]
    })], JobSeagoingPageModule);
    /***/
  },

  /***/
  "./src/app/pages/job-seagoing/job-seagoing.page.scss":
  /*!***********************************************************!*\
    !*** ./src/app/pages/job-seagoing/job-seagoing.page.scss ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesJobSeagoingJobSeagoingPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".header_div {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n.header_div .logo_div {\n  display: flex;\n}\n.header_div .logo_div .header_lbl {\n  color: var(--ion-color-main);\n  font-weight: bold;\n  font-size: 18px;\n  margin-left: 10px;\n}\n.header_div .logo_div .logo_img {\n  margin-left: 1px;\n  width: 20px;\n  height: 20px;\n}\n.header_div ion-button {\n  color: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 14px;\n}\n.error {\n  color: red;\n  padding: 5px;\n}\nion-toolbar {\n  --background: var(--ion-color-main);\n}\nion-toolbar ion-input {\n  border-bottom: 1px solid white;\n  --color: white;\n  --placeholder-color: white;\n  --padding-start: 8px;\n}\nion-toolbar ion-icon {\n  color: white;\n  font-size: 24px;\n}\n#content {\n  background-color: #ffffff;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n  height: 305px;\n  overflow: unset;\n  display: block;\n  border-radius: 15px;\n  background-size: cover;\n}\n.edit-btn {\n  font-size: 23px;\n  float: right;\n  margin: 10px;\n  color: white;\n  padding: 7px;\n  background: var(--ion-color-main);\n  border-radius: 10px;\n  position: absolute;\n  right: 0%;\n}\n.edit-btn2 {\n  font-size: 23px;\n  float: right;\n  margin: 10px;\n  color: white;\n  padding: 7px;\n  background: var(--ion-color-main);\n  border-radius: 10px;\n  position: absolute;\n  right: 28%;\n  z-index: 10;\n  top: 51%;\n}\n#profile-info {\n  width: 100%;\n  z-index: 2;\n  padding-top: 1px;\n  text-align: center;\n  position: absolute;\n  top: 45%;\n}\n#profile-image {\n  display: block;\n  border-radius: 120px;\n  border: 3px solid #fff;\n  width: 128px;\n  background: white;\n  height: 128px;\n  margin: 30px auto 0;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n}\n.main_content_div {\n  width: 100%;\n  padding: 20px;\n  height: 100%;\n}\n.main_content_div .header_lbl {\n  font-size: 26px;\n  font-weight: 600;\n}\n.main_content_div .join_btn {\n  --background: white;\n  --color: #0077B5;\n  border: 1px solid var(--ion-color-main);\n}\n.main_content_div .btn_search {\n  height: 25px;\n  width: 25px;\n  margin-right: 20px;\n}\n.main_content_div .or_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  height: 50px;\n  align-items: center;\n  justify-content: space-between;\n}\n.main_content_div .or_div .line_img {\n  height: 10px;\n  width: 40%;\n}\n.main_content_div .or_div .or_text {\n  color: black;\n}\n.main_content_div .form_div ion-item {\n  border-bottom: none;\n  --padding-start: 0px;\n}\n.main_content_div .form_div ion-item .ip {\n  border-bottom: 1px solid white;\n}\n.main_content_div .form_div .join_now {\n  margin-top: 20px;\n  border: 1px solid white;\n  --background: var(--ion-color-main);\n  font-weight: bold;\n  font-size: 18px;\n}\n.main_content_div .form_div .bottom_lbl {\n  display: block;\n  font-size: 14px;\n  margin-top: 20px;\n}\n.main_content_div .form_div .bottom_lbl span {\n  color: var(--ion-color-main) !important;\n  font-weight: bold;\n  color: gra;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvam9iLXNlYWdvaW5nL0Q6XFxSYWd1dmFyYW4gaW1hZ2VcXGRpc2Vsc2hpcC9zcmNcXGFwcFxccGFnZXNcXGpvYi1zZWFnb2luZ1xcam9iLXNlYWdvaW5nLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvam9iLXNlYWdvaW5nL2pvYi1zZWFnb2luZy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FDQ0o7QURBSTtFQUNJLGFBQUE7QUNFUjtBRERRO0VBQ0ksNEJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0daO0FERFE7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDR1o7QURBSTtFQUNJLDRCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDRVI7QURDQTtFQUNJLFVBQUE7RUFDQSxZQUFBO0FDRUo7QURBQTtFQUNJLG1DQUFBO0FDR0o7QURGSTtFQUNJLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0VBQ0Esb0JBQUE7QUNJUjtBREZJO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUNJUjtBRERBO0VBQ0kseUJBQUE7RUFDQSw0Q0FBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNJSjtBREZFO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxpQ0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0FDS0o7QURIQTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUNBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0FDTUo7QURKRTtFQUNFLFdBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBRUEsUUFBQTtBQ01KO0FESkU7RUFDRSxjQUFBO0VBQ0Esb0JBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFFQSxtQkFBQTtFQUNBLDRDQUFBO0FDTUo7QURGQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQ0tKO0FESEk7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUNLUjtBREhJO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtFQUVBLHVDQUFBO0FDSVI7QURGSTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUNJUjtBREZJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FDSVI7QURGUTtFQUNJLFlBQUE7RUFDQSxVQUFBO0FDSVo7QURGUTtFQUNJLFlBQUE7QUNJWjtBRENRO0VBQ0ksbUJBQUE7RUFDQSxvQkFBQTtBQ0NaO0FEQVk7RUFDSSw4QkFBQTtBQ0VoQjtBREVRO0VBQ0ksZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1DQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDQVo7QURHUTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNEWjtBREdZO0VBQ0ksdUNBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7QUNEaEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9qb2Itc2VhZ29pbmcvam9iLXNlYWdvaW5nLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXJfZGl2e1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAubG9nb19kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIC5oZWFkZXJfbGJse1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgIH1cbiAgICAgICAgLmxvZ29faW1ne1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDFweDtcbiAgICAgICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICAgICAgaGVpZ2h0OiAyMHB4O1xuICAgICAgICB9XG4gICAgfVxuICAgIGlvbi1idXR0b257XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICB9XG59XG4uZXJyb3J7XG4gICAgY29sb3I6IHJlZDtcbiAgICBwYWRkaW5nOiA1cHg7XG59XG5pb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgaW9uLWlucHV0e1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG4gICAgICAgIC0tY29sb3I6IHdoaXRlO1xuICAgICAgICAtLXBsYWNlaG9sZGVyLWNvbG9yIDogd2hpdGU7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydCA6IDhweDtcbiAgICB9XG4gICAgaW9uLWljb257XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xuICAgIH1cbn1cbiNjb250ZW50IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgIGJveC1zaGFkb3c6IDBweCAtMXB4IDEwcHggcmdiKDAgMCAwIC8gNDAlKTtcbiAgICBoZWlnaHQ6IDMwNXB4O1xuICAgIG92ZXJmbG93OiB1bnNldDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIH1cbiAgLmVkaXQtYnRue1xuICAgIGZvbnQtc2l6ZTogMjNweDtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgbWFyZ2luOiAxMHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBwYWRkaW5nOiA3cHg7XG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAwJTtcbn1cbi5lZGl0LWJ0bjJ7XG4gICAgZm9udC1zaXplOiAyM3B4O1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBtYXJnaW46IDEwcHg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHBhZGRpbmc6IDdweDtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDI4JTtcbiAgICB6LWluZGV4OiAxMDtcbiAgICB0b3A6IDUxJTtcbn1cbiAgI3Byb2ZpbGUtaW5mbyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgei1pbmRleDogMjtcbiAgICBwYWRkaW5nLXRvcDogMXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgXG4gICAgdG9wOiA0NSU7XG4gIH1cbiAgI3Byb2ZpbGUtaW1hZ2Uge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGJvcmRlci1yYWRpdXM6IDEyMHB4O1xuICAgIGJvcmRlcjogM3B4IHNvbGlkICNmZmY7XG4gICAgd2lkdGg6IDEyOHB4O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIGhlaWdodDogMTI4cHg7XG4gICBcbiAgICBtYXJnaW46IDMwcHggYXV0byAwO1xuICAgIGJveC1zaGFkb3c6IDBweCAtMXB4IDEwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAgLy8gYm94LXNoYWRvdzogMHB4IDBweCA0cHggcmdiYSgwLCAwLCAwLCAwLjcpO1xuICB9XG5cbi5tYWluX2NvbnRlbnRfZGl2e1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgaGVpZ2h0OiAxMDAlO1xuXG4gICAgLmhlYWRlcl9sYmx7XG4gICAgICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICB9XG4gICAgLmpvaW5fYnRue1xuICAgICAgICAtLWJhY2tncm91bmQgOiB3aGl0ZTtcbiAgICAgICAgLS1jb2xvcjogIzAwNzdCNTtcbiAgICAgICAgLy8gLS1ib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgfVxuICAgIC5idG5fc2VhcmNoe1xuICAgICAgICBoZWlnaHQ6IDI1cHg7XG4gICAgICAgIHdpZHRoOiAyNXB4O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG4gICAgfVxuICAgIC5vcl9kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcblxuICAgICAgICAubGluZV9pbWd7XG4gICAgICAgICAgICBoZWlnaHQ6IDEwcHg7XG4gICAgICAgICAgICB3aWR0aDogNDAlO1xuICAgICAgICB9XG4gICAgICAgIC5vcl90ZXh0e1xuICAgICAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmZvcm1fZGl2e1xuICAgICAgICBpb24taXRlbXtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IG5vbmU7XG4gICAgICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgICAgICAgICAgIC5pcHtcbiAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAuam9pbl9ub3d7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgd2hpdGU7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5ib3R0b21fbGJse1xuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuXG4gICAgICAgICAgICBzcGFue1xuICAgICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbikgIWltcG9ydGFudDtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgICAgICBjb2xvcjogZ3JhO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufSIsIi5oZWFkZXJfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmhlYWRlcl9kaXYgLmxvZ29fZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5oZWFkZXJfZGl2IC5sb2dvX2RpdiAuaGVhZGVyX2xibCB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE4cHg7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLmhlYWRlcl9kaXYgLmxvZ29fZGl2IC5sb2dvX2ltZyB7XG4gIG1hcmdpbi1sZWZ0OiAxcHg7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG59XG4uaGVhZGVyX2RpdiBpb24tYnV0dG9uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uZXJyb3Ige1xuICBjb2xvcjogcmVkO1xuICBwYWRkaW5nOiA1cHg7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5pb24tdG9vbGJhciBpb24taW5wdXQge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG4gIC0tY29sb3I6IHdoaXRlO1xuICAtLXBsYWNlaG9sZGVyLWNvbG9yOiB3aGl0ZTtcbiAgLS1wYWRkaW5nLXN0YXJ0OiA4cHg7XG59XG5pb24tdG9vbGJhciBpb24taWNvbiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAyNHB4O1xufVxuXG4jY29udGVudCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gIGJveC1zaGFkb3c6IDBweCAtMXB4IDEwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xuICBoZWlnaHQ6IDMwNXB4O1xuICBvdmVyZmxvdzogdW5zZXQ7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4uZWRpdC1idG4ge1xuICBmb250LXNpemU6IDIzcHg7XG4gIGZsb2F0OiByaWdodDtcbiAgbWFyZ2luOiAxMHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDdweDtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAwJTtcbn1cblxuLmVkaXQtYnRuMiB7XG4gIGZvbnQtc2l6ZTogMjNweDtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW46IDEwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogN3B4O1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDI4JTtcbiAgei1pbmRleDogMTA7XG4gIHRvcDogNTElO1xufVxuXG4jcHJvZmlsZS1pbmZvIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHotaW5kZXg6IDI7XG4gIHBhZGRpbmctdG9wOiAxcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDQ1JTtcbn1cblxuI3Byb2ZpbGUtaW1hZ2Uge1xuICBkaXNwbGF5OiBibG9jaztcbiAgYm9yZGVyLXJhZGl1czogMTIwcHg7XG4gIGJvcmRlcjogM3B4IHNvbGlkICNmZmY7XG4gIHdpZHRoOiAxMjhweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGhlaWdodDogMTI4cHg7XG4gIG1hcmdpbjogMzBweCBhdXRvIDA7XG4gIGJveC1zaGFkb3c6IDBweCAtMXB4IDEwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xufVxuXG4ubWFpbl9jb250ZW50X2RpdiB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAyMHB4O1xuICBoZWlnaHQ6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuaGVhZGVyX2xibCB7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5qb2luX2J0biB7XG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gIC0tY29sb3I6ICMwMDc3QjU7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5idG5fc2VhcmNoIHtcbiAgaGVpZ2h0OiAyNXB4O1xuICB3aWR0aDogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLm9yX2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDUwcHg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5vcl9kaXYgLmxpbmVfaW1nIHtcbiAgaGVpZ2h0OiAxMHB4O1xuICB3aWR0aDogNDAlO1xufVxuLm1haW5fY29udGVudF9kaXYgLm9yX2RpdiAub3JfdGV4dCB7XG4gIGNvbG9yOiBibGFjaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mb3JtX2RpdiBpb24taXRlbSB7XG4gIGJvcmRlci1ib3R0b206IG5vbmU7XG4gIC0tcGFkZGluZy1zdGFydDogMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZvcm1fZGl2IGlvbi1pdGVtIC5pcCB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mb3JtX2RpdiAuam9pbl9ub3cge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCB3aGl0ZTtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE4cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZm9ybV9kaXYgLmJvdHRvbV9sYmwge1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZvcm1fZGl2IC5ib3R0b21fbGJsIHNwYW4ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogZ3JhO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/job-seagoing/job-seagoing.page.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/job-seagoing/job-seagoing.page.ts ***!
    \*********************************************************/

  /*! exports provided: JobSeagoingPage */

  /***/
  function srcAppPagesJobSeagoingJobSeagoingPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "JobSeagoingPage", function () {
      return JobSeagoingPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! sweetalert2 */
    "./node_modules/sweetalert2/dist/sweetalert2.all.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_8__);
    /* harmony import */


    var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/android-permissions/ngx */
    "./node_modules/@ionic-native/android-permissions/ngx/index.js");
    /* harmony import */


    var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/file/ngx */
    "./node_modules/@ionic-native/file/ngx/index.js");
    /* harmony import */


    var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @ionic-native/native-page-transitions/ngx */
    "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var JobSeagoingPage = /*#__PURE__*/function () {
      function JobSeagoingPage(navCtrl, nativePageTransitions, loadingController, plt, file, router, api, location, androidPermissions, storage, fb, actionSheetCtrl, alertCtrl, toastCtrl) {
        var _this = this;

        _classCallCheck(this, JobSeagoingPage);

        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.loadingController = loadingController;
        this.plt = plt;
        this.file = file;
        this.router = router;
        this.api = api;
        this.location = location;
        this.androidPermissions = androidPermissions;
        this.storage = storage;
        this.fb = fb;
        this.actionSheetCtrl = actionSheetCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.data = {};
        this.images = [];
        this.user = this.api.getCurrentUser();
        this.posts = [];
        this.country = [{
          value: 'TELENGANA'
        }, {
          value: 'India'
        }, {
          value: 'USA'
        }, {
          value: 'Pakistan'
        }, {
          value: 'China'
        }];
        this.defaultDate = "1987-06-30";
        this.isSubmitted = false;
        this.options1 = {
          direction: 'up',
          duration: 500,
          slowdownfactor: 3,
          slidePixels: 20,
          iosdelay: 100,
          androiddelay: 150,
          fixedPixelsTop: 0,
          fixedPixelsBottom: 60
        };
        this.loader();
        this.api.getJobdetails(this.token, "seagoing").subscribe(function (res) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    this.data = res;
                    console.log(res);

                  case 2:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }, function (err) {
          _this.loading2 = false;
          console.log(err);

          _this.showError(err);
        });
        this.edit_icon = true;
        this.edit_icon2 = true;
        this.user.subscribe(function (user) {
          if (user) {
            console.log(user);
            _this.token = user.token;
          } else {
            _this.posts = [];
          }
        });
      }

      _createClass(JobSeagoingPage, [{
        key: "errorControl",
        get: function get() {
          return this.userForm.controls;
        }
      }, {
        key: "loader",
        value: function loader() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.loadingController.create({
                      message: 'Please wait...',
                      duration: 1500
                    });

                  case 2:
                    loading = _context2.sent;
                    _context2.next = 5;
                    return loading.present();

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "getDate",
        value: function getDate(e) {
          var date = new Date(e.target.value).toISOString().substring(0, 10);
          this.userForm.get('sea_service_to_date1').setValue(date, {
            onlyself: true
          });
        }
      }, {
        key: "getDate2",
        value: function getDate2(e) {
          var date = new Date(e.target.value).toISOString().substring(0, 10);
          this.userForm.get('sea_service_from_date1').setValue(date, {
            onlyself: true
          });
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.userForm = this.fb.group({
            sea_ship_name1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_gt1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_official_no1: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern('^[0-9]+$')]],
            sea_nature_of_watch_keeping1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_imo_number1: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern('^[0-9]+$')]],
            sea_service_from_date1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_article_of_months1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_propulsion_power1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_propelling_days1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_remarks1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_flag1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_port_of_registry1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_ship_type1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_trade_area1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_propulsion_type1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_service_to_date1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_article_days1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sea_rank1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]
          });
        }
      }, {
        key: "userupdate",
        value: function userupdate() {
          var _this2 = this;

          this.loading2 = true;
          this.isSubmitted = true;

          if (!this.userForm.valid) {
            console.log('Please provide all the required values!');
            this.loading2 = false;
            return false;
          } else {
            console.log(this.userForm.value);
          }

          var formdat = [];
          formdat.push({
            meta_key: 'sea_ship_name1',
            meta_value: this.userForm.value.sea_ship_name1
          }, {
            meta_key: 'sea_official_no1',
            meta_value: this.userForm.value.sea_official_no1
          }, {
            meta_key: 'sea_imo_number1',
            meta_value: this.userForm.value.sea_imo_number1
          }, {
            meta_key: 'sea_gt1',
            meta_value: this.userForm.value.sea_gt1
          }, {
            meta_key: 'sea_rank1',
            meta_value: this.userForm.value.sea_rank1
          }, {
            meta_key: 'sea_nature_of_watch_keeping1',
            meta_value: this.userForm.value.sea_nature_of_watch_keeping1
          }, {
            meta_key: 'sea_service_from_date1',
            meta_value: this.userForm.value.sea_service_from_date1
          }, {
            meta_key: 'sea_article_of_months1',
            meta_value: this.userForm.value.sea_article_of_months1
          }, {
            meta_key: 'sea_propulsion_power1',
            meta_value: this.userForm.value.sea_propulsion_power1
          }, {
            meta_key: 'sea_propelling_days1',
            meta_value: this.userForm.value.sea_propelling_days1
          }, {
            meta_key: 'sea_remarks1',
            meta_value: this.userForm.value.sea_remarks1
          }, {
            meta_key: 'sea_flag1',
            meta_value: this.userForm.value.sea_flag1
          }, {
            meta_key: 'sea_port_of_registry1',
            meta_value: this.userForm.value.sea_port_of_registry1
          }, {
            meta_key: 'sea_ship_type1',
            meta_value: this.userForm.value.sea_ship_type1
          }, {
            meta_key: 'sea_trade_area1',
            meta_value: this.userForm.value.sea_trade_area1
          }, {
            meta_key: 'sea_service_to_date1',
            meta_value: this.userForm.value.sea_service_to_date1
          }, {
            meta_key: 'sea_article_days1',
            meta_value: this.userForm.value.sea_article_days1
          }, {
            meta_key: 'sea_propulsion_type1',
            meta_value: this.userForm.value.sea_propulsion_type1
          });
          var formdata = [];
          formdata.push({
            meta_data: formdat
          });
          this.api.userjobupdate(formdata, this.token).subscribe(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var toast;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      this.loading2 = false;
                      console.log(res);
                      _context3.next = 4;
                      return this.toastCtrl.create({
                        message: 'Profile Updated',
                        duration: 3000
                      });

                    case 4:
                      toast = _context3.sent;
                      toast.present();
                      console.log('Profile Updated', res); // this.router.navigate(['/member-detail']);
                      // window.location.reload();

                    case 7:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }, function (err) {
            _this2.loading2 = false;
            console.log(err);

            _this2.showError(err);
          });
        }
      }, {
        key: "BackButton",
        value: function BackButton() {
          this.location.back();
        }
      }, {
        key: "showError",
        value: function showError(err) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var alert;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.alertCtrl.create({
                      header: err.error.code,
                      subHeader: err.error.data,
                      message: err.error.message,
                      buttons: ['OK']
                    });

                  case 2:
                    alert = _context4.sent;
                    _context4.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.nativePageTransitions.slide(this.options1);
        } // example of adding a transition when pushing a new page

      }, {
        key: "openPage",
        value: function openPage(page) {
          this.nativePageTransitions.slide(this.options1);
          this.navCtrl.navigateForward(page);
        }
      }, {
        key: "loadPrivatePosts",
        value: function loadPrivatePosts() {
          var _this3 = this;

          this.api.getPrivatePosts().subscribe(function (res) {
            _this3.posts = res;
          });
        }
      }, {
        key: "selectImageSource",
        value: function selectImageSource() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var _this4 = this;

            var buttons, actionSheet;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    buttons = [{
                      text: 'Take Photo',
                      icon: 'camera',
                      handler: function handler() {
                        _this4.androidPermissions.checkPermission(_this4.androidPermissions.PERMISSION.CAMERA).then(function (result) {
                          return console.log('Has permission?', result.hasPermission);
                        }, function (err) {
                          return _this4.androidPermissions.requestPermission(_this4.androidPermissions.PERMISSION.CAMERA);
                        });

                        _this4.androidPermissions.requestPermissions([_this4.androidPermissions.PERMISSION.CAMERA, _this4.androidPermissions.PERMISSION.GET_ACCOUNTS]); // this.addImage();

                      }
                    }, {
                      text: 'Choose From Photos Photo',
                      icon: 'image',
                      handler: function handler() {
                        _this4.androidPermissions.checkPermission(_this4.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                          return console.log('Has permission?', result.hasPermission);
                        }, function (err) {
                          return _this4.androidPermissions.requestPermission(_this4.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                        });

                        _this4.androidPermissions.requestPermissions([_this4.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, _this4.androidPermissions.PERMISSION.GET_ACCOUNTS]); // this.selectImage();

                      }
                    }]; // Only allow file selection inside a browser

                    if (!this.plt.is('hybrid')) {
                      buttons.push({
                        text: 'Choose a File',
                        icon: 'attach',
                        handler: function handler() {
                          _this4.androidPermissions.checkPermission(_this4.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(function (result) {
                            return console.log('Has permission?', result.hasPermission);
                          }, function (err) {
                            return _this4.androidPermissions.requestPermission(_this4.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
                          });

                          _this4.androidPermissions.requestPermissions([_this4.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, _this4.androidPermissions.PERMISSION.GET_ACCOUNTS]); // this.fileInput.nativeElement.click();

                        }
                      });
                    }

                    _context5.next = 4;
                    return this.actionSheetCtrl.create({
                      header: 'Select Image Source',
                      buttons: buttons
                    });

                  case 4:
                    actionSheet = _context5.sent;
                    _context5.next = 7;
                    return actionSheet.present();

                  case 7:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "goToprofile",
        value: function goToprofile() {
          this.router.navigate(['/member-detail']);
        }
      }, {
        key: "goToHome",
        value: function goToHome(res) {
          sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
            title: 'Success',
            text: 'Thank you for registrations',
            icon: 'success',
            backdrop: false
          });
          this.router.navigate(['/tabs/home-new']);
        }
      }]);

      return JobSeagoingPage;
    }();

    JobSeagoingPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"]
      }, {
        type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_11__["NativePageTransitions"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]
      }, {
        type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"]
      }, {
        type: _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"]
      }, {
        type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__["AndroidPermissions"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ActionSheetController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], JobSeagoingPage.prototype, "fileInput", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput2', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], JobSeagoingPage.prototype, "fileInput2", void 0);
    JobSeagoingPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-job-seagoing',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./job-seagoing.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-seagoing/job-seagoing.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./job-seagoing.page.scss */
      "./src/app/pages/job-seagoing/job-seagoing.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"], _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_11__["NativePageTransitions"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__["AndroidPermissions"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ActionSheetController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"]])], JobSeagoingPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-job-seagoing-job-seagoing-module-es5.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-groups-list-groups-list-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/groups-list/groups-list.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/groups-list/groups-list.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-button (click)=\"BackButton()\" slot=\"start\">\n      <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n    </ion-button>\n    <ion-title color=\"light\">Groups</ion-title>\n    <ion-icon name=\"add-circle-outline\" mode=\"md\" color=\"light\" style=\"font-size: 40px;\" slot=\"end\" (click)=\"goTonewgroup()\"></ion-icon>\n  </ion-toolbar>\n  <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n  (ionChange)=\"onSearchChange($event)\" [(ngModel)]='searchtext'  [debounce]=\"250\"></ion-searchbar>\n</ion-header>\n<div  *ngIf=\"groups.length>0\" class=\"flex_div2\">\n\n  <ion-buttons (click)=\"fillternews('active')\" *ngIf=\"!active\" class=\"filterbtn\">\n Recently Active\n  </ion-buttons>\n\n  <ion-buttons  (click)=\"fillternews('active')\" *ngIf=\"active\" class=\"filterbtn2\">\n    Recently Active\n     </ion-buttons>\n  <ion-buttons (click)=\"fillternews('newest')\" *ngIf=\"!newest\" class=\"filterbtn\">\n Newly created\n  </ion-buttons>\n\n  <ion-buttons (click)=\"fillternews('active')\"  *ngIf=\"newest\" class=\"filterbtn2\">\n    Newly created\n     </ion-buttons>\n  <ion-buttons  (click)=\"fillternews('popular')\" *ngIf=\"!popular\" class=\"filterbtn\">\n    Most Members\n  </ion-buttons>\n  <ion-buttons (click)=\"fillternews('active')\" *ngIf=\"popular\" class=\"filterbtn2\">\n    Most Members\n      </ion-buttons>\n  <ion-buttons (click)=\"fillternews('alphabetical')\" *ngIf=\"!alphabetical\" class=\"filterbtn\">\n    Alphabetical\n      </ion-buttons>\n      <ion-buttons (click)=\"fillternews('active')\" *ngIf=\"alphabetical\" class=\"filterbtn2\">\n        Alphabetical\n          </ion-buttons>\n     \n\n</div> \n<ion-content>\n  <img style=\"width: -webkit-fill-available;\" *ngIf=\"!groups.length>0\" src=\"assets/imgs/no_result.jpg\">\n  <div *ngIf=\"groups.length>0\" class=\"main_content_div\">\n\n    <!-- <span *ngIf=\"text != 'Connections'\">\n      <div class=\"flex_div\" *ngFor=\"let item of users\">\n\n        <div class=\"first_div\">\n          <div [style.backgroundImage]=\"'url('+item.img+')'\" class=\"image\"></div>\n\n          <div class=\"content_div\">\n            <ion-label class=\"head_lbl\">Initappz Shop</ion-label>\n            <ion-label class=\"small_lbl\">E-learning 22,300 followers</ion-label>\n          </div>\n        </div>\n\n        <div class=\"second_div\">\n          <ion-button class=\"fallow_btn\" expand=\"block\" fill=\"clear\" size=\"small\">\n            Unfollow\n          </ion-button>\n        </div>\n      </div>\n    </span> -->\n\n    <span >\n      <div class=\"flex_div\" *ngFor=\"let item of groups\">\n        <div class=\"first_div\">\n          <div [routerLink]=\"['/group-detail', item.id]\"  [style.backgroundImage]=\"'url('+item.avatar_urls.thumb+')'\" class=\"image\"></div>\n          <div class=\"content_div\">\n            <ion-label class=\"head_lbl\" [routerLink]=\"['/group-detail', item.id]\" >{{item.name}}</ion-label>\n            <ion-label class=\"small_lbl\"> {{item.status}} / {{item.group_type_label}} . {{item.members_count}} Members</ion-label>\n            <ion-button *ngIf=\"!item.is_member && !item.is_admin\" (click)=\"join(item.id)\" size=\"small\" class=\"join\" expand=\"block\">\n              JOIN\n            </ion-button>\n            <ion-button *ngIf=\"item.is_member && !item.is_admin\" (click)=\"leave(item.id)\" size=\"small\" class=\"leave\" expand=\"block\">\n              <ion-icon style=\"\n              padding: 3px;\n          \" name=\"checkmark-outline\"></ion-icon>  Member\n            </ion-button>\n            <ion-button *ngIf=\"item.is_admin\" (click)=\"leave(item.id)\" size=\"small\" class=\"leave\" expand=\"block\">\n              <ion-icon style=\"\n              padding: 3px;\n          \" name=\"checkmark-outline\"></ion-icon>  Organizer\n            </ion-button>\n          </div>\n        </div>\n        <div class=\"second_div\">\n          <!-- <div class=\"chat_div\">\n            <ion-icon (click)=\"goToChatList()\" name=\"chatbubbles\" mode=\"md\"></ion-icon>\n          </div> -->\n          <!-- <div class=\"menu_div\">\n            <ion-icon (click)=\"followuser(item.id)\" *ngIf=\"!item.is_member\" name=\"person-add-outline\" mode=\"md\"></ion-icon>\n            <ion-icon (click)=\"unfollowuser(item.id)\" *ngIf=\"item.is_following\" name=\"person-remove\" mode=\"md\"></ion-icon>\n          </div> -->\n        </div>\n      </div>\n    </span>\n\n  </div>\n  <ion-infinite-scroll (ionInfinite)=\"loadData($event)\">\n    <ion-infinite-scroll-content\n    loadingSpinner=\"bubbles\"\n    loadingText=\"Loading more alphabetical ...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/groups-list/groups-list-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/groups-list/groups-list-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: GroupsListPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupsListPageRoutingModule", function() { return GroupsListPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _groups_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./groups-list.page */ "./src/app/pages/groups-list/groups-list.page.ts");




const routes = [
    {
        path: '',
        component: _groups_list_page__WEBPACK_IMPORTED_MODULE_3__["GroupsListPage"]
    }
];
let GroupsListPageRoutingModule = class GroupsListPageRoutingModule {
};
GroupsListPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], GroupsListPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/groups-list/groups-list.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/groups-list/groups-list.module.ts ***!
  \*********************************************************/
/*! exports provided: GroupsListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupsListPageModule", function() { return GroupsListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _groups_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./groups-list-routing.module */ "./src/app/pages/groups-list/groups-list-routing.module.ts");
/* harmony import */ var _groups_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./groups-list.page */ "./src/app/pages/groups-list/groups-list.page.ts");







let GroupsListPageModule = class GroupsListPageModule {
};
GroupsListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _groups_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["GroupsListPageRoutingModule"]
        ],
        declarations: [_groups_list_page__WEBPACK_IMPORTED_MODULE_6__["GroupsListPage"]]
    })
], GroupsListPageModule);



/***/ }),

/***/ "./src/app/pages/groups-list/groups-list.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/groups-list/groups-list.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\n.flex_div2 {\n  font-size: 12px;\n  display: flex;\n}\n\n.filterbtn2 {\n  padding: 7px;\n  border-radius: 20px;\n  cursor: pointer;\n  margin: 4px;\n  color: white;\n  background: var(--ion-color-main);\n}\n\n.filterbtn {\n  border: 2px solid var(--ion-color-main);\n  padding: 7px;\n  border-radius: 20px;\n  cursor: pointer;\n  margin: 4px;\n  background: aliceblue;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div .flex_div {\n  display: flex;\n  align-items: center;\n  padding-top: 10px;\n  width: 100%;\n  justify-content: space-between;\n  padding: 10px 16px;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .flex_div .first_div {\n  display: flex;\n  align-items: center;\n}\n\n.main_content_div .flex_div .second_div {\n  display: flex;\n  align-items: center;\n}\n\n.main_content_div .flex_div .second_div ion-icon {\n  font-size: 20px;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n\n.main_content_div .flex_div .second_div .chat_div {\n  height: 35px;\n  width: 35px;\n  border-radius: 50%;\n  border: 1px solid var(--ion-color-main);\n  position: relative;\n  margin-right: 5px;\n}\n\n.main_content_div .flex_div .second_div .chat_div ion-icon {\n  color: var(--ion-color-main);\n}\n\n.main_content_div .flex_div .second_div .menu_div {\n  height: 35px;\n  width: 35px;\n  border-radius: 50%;\n  border: 1px solid #505050;\n  position: relative;\n}\n\n.main_content_div .flex_div .second_div .menu_div ion-icon {\n  color: #505050;\n}\n\n.main_content_div .flex_div .leave {\n  --background: #d0d0d0;\n  font-weight: 600;\n  font-size: 16px;\n  color: #2e5163;\n  --border-radius: 10px;\n  margin-left: 0px;\n  margin-right: 0px;\n  margin-top: 10px;\n  margin-bottom: 10px;\n  display: block;\n  font-size: 14px;\n  width: 85%;\n  clear: both;\n}\n\n.main_content_div .flex_div .join {\n  --background: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 16px;\n  --border-radius: 10px;\n  margin-left: 0px;\n  margin-right: 0px;\n  margin-top: 10px;\n  margin-bottom: 10px;\n  display: block;\n  font-size: 14px;\n  width: 70%;\n  clear: both;\n}\n\n.main_content_div .flex_div .image {\n  height: 90px;\n  width: 90px;\n  max-width: 90px;\n  border-radius: 25%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.main_content_div .flex_div .content_div {\n  margin-left: 10px;\n}\n\n.main_content_div .flex_div .content_div .head_lbl {\n  font-weight: 600;\n}\n\n.main_content_div .flex_div .content_div .small_lbl {\n  font-size: 13px;\n  margin-top: 5px;\n}\n\n.main_content_div .flex_div .fallow_btn {\n  font-size: 14px;\n  font-weight: 600;\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZ3JvdXBzLWxpc3QvRDpcXFJhZ3V2YXJhbiBpbWFnZVxcZGlzZWxzaGlwL3NyY1xcYXBwXFxwYWdlc1xcZ3JvdXBzLWxpc3RcXGdyb3Vwcy1saXN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZ3JvdXBzLWxpc3QvZ3JvdXBzLWxpc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUNBQUE7QUNDSjs7QURDQTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUNFSjs7QURDQTtFQUVJLGVBQUE7RUFFQSxhQUFBO0FDQUo7O0FERUE7RUFDSSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxpQ0FBQTtBQ0NKOztBRENBO0VBQ0ksdUNBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLHFCQUFBO0FDRUo7O0FEQUE7RUFDSSxXQUFBO0FDR0o7O0FEREk7RUFDSSxjQUFBO0FDR1I7O0FEREk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0NBQUE7QUNHUjs7QUREUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtBQ0daOztBREFRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FDRVo7O0FEQVk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0FDRWhCOztBRENZO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHVDQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0NoQjs7QURDZ0I7RUFDSSw0QkFBQTtBQ0NwQjs7QURFWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0FDQWhCOztBREVnQjtFQUNJLGNBQUE7QUNBcEI7O0FESVE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFFQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUVBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQ0pSOztBREtRO0VBQ0ksbUNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQ0haOztBRE1RO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUNKWjs7QURPUTtFQUNJLGlCQUFBO0FDTFo7O0FETVk7RUFDSSxnQkFBQTtBQ0poQjs7QURNWTtFQUNJLGVBQUE7RUFDQSxlQUFBO0FDSmhCOztBRFFRO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsU0FBQTtBQ05aIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZ3JvdXBzLWxpc3QvZ3JvdXBzLWxpc3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWh7XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG4uZmxleF9kaXYye1xuICAgXG4gICAgZm9udC1zaXplOiAxMnB4O1xuXG4gICAgZGlzcGxheTogZmxleDtcbn1cbi5maWx0ZXJidG4ye1xuICAgIHBhZGRpbmc6IDdweDtcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBtYXJnaW46IDRweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuLmZpbHRlcmJ0bntcbiAgICBib3JkZXI6IDJweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgcGFkZGluZzogN3B4O1xuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIG1hcmdpbjogNHB4O1xuICAgIGJhY2tncm91bmQ6IGFsaWNlYmx1ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2e1xuICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuICAgIC5mbGV4X2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIHBhZGRpbmc6IDEwcHggMTZweDtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcblxuICAgICAgICAuZmlyc3RfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIH1cblxuICAgICAgICAuc2Vjb25kX2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuXG4gICAgICAgICAgICBpb24taWNvbntcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgIHRvcDogNTAlO1xuICAgICAgICAgICAgICAgIGxlZnQ6IDUwJTtcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLC01MCUpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAuY2hhdF9kaXZ7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAzNXB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiAzNXB4O1xuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogNXB4O1xuXG4gICAgICAgICAgICAgICAgaW9uLWljb257XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLm1lbnVfZGl2e1xuICAgICAgICAgICAgICAgIGhlaWdodDogMzVweDtcbiAgICAgICAgICAgICAgICB3aWR0aDogMzVweDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgIzUwNTA1MDtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgICAgICAgICAgICAgICBpb24taWNvbntcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM1MDUwNTA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5sZWF2ZXtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjZDBkMGQwO1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGNvbG9yOiAjMmU1MTYzO1xuICAgICAgXG4gICAgICAgIC0tYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgd2lkdGg6IDg1JTtcbiAgICAgICAgY2xlYXI6IGJvdGg7fVxuICAgICAgICAuam9pbntcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICB3aWR0aDogNzAlO1xuICAgICAgICAgICAgY2xlYXI6IGJvdGg7XG4gICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgLmltYWdle1xuICAgICAgICAgICAgaGVpZ2h0OiA5MHB4O1xuICAgICAgICAgICAgd2lkdGg6IDkwcHg7XG4gICAgICAgICAgICBtYXgtd2lkdGg6IDkwcHg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAyNSU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgfVxuXG4gICAgICAgIC5jb250ZW50X2RpdntcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgLmhlYWRfbGJse1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuc21hbGxfbGJse1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAuZmFsbG93X2J0bntcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG4uZmxleF9kaXYyIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBkaXNwbGF5OiBmbGV4O1xufVxuXG4uZmlsdGVyYnRuMiB7XG4gIHBhZGRpbmc6IDdweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBtYXJnaW46IDRweDtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5cbi5maWx0ZXJidG4ge1xuICBib3JkZXI6IDJweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIHBhZGRpbmc6IDdweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBtYXJnaW46IDRweDtcbiAgYmFja2dyb3VuZDogYWxpY2VibHVlO1xufVxuXG4ubWFpbl9jb250ZW50X2RpdiB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm1haW5fY29udGVudF9kaXYgaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcGFkZGluZzogMTBweCAxNnB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5maXJzdF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5zZWNvbmRfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuc2Vjb25kX2RpdiBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuc2Vjb25kX2RpdiAuY2hhdF9kaXYge1xuICBoZWlnaHQ6IDM1cHg7XG4gIHdpZHRoOiAzNXB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuc2Vjb25kX2RpdiAuY2hhdF9kaXYgaW9uLWljb24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5zZWNvbmRfZGl2IC5tZW51X2RpdiB7XG4gIGhlaWdodDogMzVweDtcbiAgd2lkdGg6IDM1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYm9yZGVyOiAxcHggc29saWQgIzUwNTA1MDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5zZWNvbmRfZGl2IC5tZW51X2RpdiBpb24taWNvbiB7XG4gIGNvbG9yOiAjNTA1MDUwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5sZWF2ZSB7XG4gIC0tYmFja2dyb3VuZDogI2QwZDBkMDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBjb2xvcjogIzJlNTE2MztcbiAgLS1ib3JkZXItcmFkaXVzOiAxMHB4O1xuICBtYXJnaW4tbGVmdDogMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgd2lkdGg6IDg1JTtcbiAgY2xlYXI6IGJvdGg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmpvaW4ge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxNnB4O1xuICAtLWJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiAxNHB4O1xuICB3aWR0aDogNzAlO1xuICBjbGVhcjogYm90aDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuaW1hZ2Uge1xuICBoZWlnaHQ6IDkwcHg7XG4gIHdpZHRoOiA5MHB4O1xuICBtYXgtd2lkdGg6IDkwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDI1JTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiAuaGVhZF9sYmwge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiAuc21hbGxfbGJsIHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmZhbGxvd19idG4ge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIG1hcmdpbjogMDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/groups-list/groups-list.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/groups-list/groups-list.page.ts ***!
  \*******************************************************/
/*! exports provided: GroupsListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupsListPage", function() { return GroupsListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/dummy-data.service */ "./src/app/services/dummy-data.service.ts");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/native-page-transitions/ngx */ "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/









let GroupsListPage = class GroupsListPage {
    constructor(route, navCtrl, nativePageTransitions, router, loadingController, toastCtrl, api, location, storage, dummy) {
        this.route = route;
        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.router = router;
        this.loadingController = loadingController;
        this.toastCtrl = toastCtrl;
        this.api = api;
        this.location = location;
        this.storage = storage;
        this.dummy = dummy;
        this.fillter = "";
        this.searchtext = "";
        this.groups = [];
        this.newest = false;
        this.alphabetical = false;
        this.active = true;
        this.popular = false;
        this.options = {
            direction: 'up',
            duration: 500,
            slowdownfactor: 3,
            slidePixels: 20,
            iosdelay: 100,
            androiddelay: 150,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 60
        };
        var members2 = [];
        members2.push({
            thumb: 'surname',
        });
        this.groups.push({
            avatar_urls: members2,
        });
        this.storage.get('COMPLETE_USER_INFO').then(result => {
            if (result != null) {
                console.log(result);
                this.token = result.token;
                this.user_id = result.user_id;
                console.log('user_id: ' + result.user_id);
                console.log('token: ' + result.token);
            }
        }).catch(e => {
            console.log('error: ' + e);
            // Handle errors here
        });
        this.users = this.dummy.users;
    }
    ionViewWillLeave() {
        this.nativePageTransitions.slide(this.options);
    }
    // example of adding a transition when pushing a new page
    openPage(page) {
        this.nativePageTransitions.slide(this.options);
        this.navCtrl.navigateForward(page);
    }
    loadData(event) {
        const page = (Math.ceil(this.groups.length / 10)) + 1;
        if (this.searchtext != "" && this.fillter != "") {
            this.api.getGroups(page, this.fillter, this.searchtext).subscribe((newPagePosts) => {
                this.groups.push(...newPagePosts);
                event.target.complete();
            }, err => {
                // there are no more posts available
                event.target.disabled = true;
            });
            console.log("searchtext", this.searchtext);
            console.log("filter", this.fillter);
        }
        else if (this.searchtext != "") {
            this.api.getGroups(page, "", this.searchtext).subscribe((newPagePosts) => {
                this.groups.push(...newPagePosts);
                event.target.complete();
            }, err => {
                // there are no more posts available
                event.target.disabled = true;
            });
            console.log("searchtext", this.searchtext);
        }
        else {
            this.api.getGroups(page).subscribe((newPagePosts) => {
                this.groups.push(...newPagePosts);
                event.target.complete();
            }, err => {
                // there are no more posts available
                event.target.disabled = true;
            });
        }
    }
    fillternews(filter) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Please wait...',
                duration: 1500
            });
            yield loading.present();
            if (filter == "newest") {
                this.newest = true;
                this.alphabetical = false;
                this.active = false;
                this.popular = false;
            }
            else if (filter == "alphabetical") {
                this.newest = false;
                this.alphabetical = true;
                this.active = false;
                this.popular = false;
            }
            else if (filter == "active") {
                this.newest = false;
                this.alphabetical = false;
                this.active = true;
                this.popular = false;
            }
            if (filter == "popular") {
                this.newest = false;
                this.alphabetical = false;
                this.active = false;
                this.popular = true;
            }
            if (filter != "active") {
                this.fillter = filter;
                if (this.searchtext == "") {
                    this.api.getGroups(1, filter, "").subscribe(res => {
                        this.groups = res;
                    });
                    console.log("filter", filter);
                }
                else if (this.searchtext != "") {
                    this.api.getGroups(1, filter, this.searchtext).subscribe(res => {
                        this.groups = res;
                    });
                    console.log("filter", filter);
                }
            }
            else {
                if (this.searchtext == "") {
                    this.fillter = "";
                    this.api.getGroups().subscribe(res => {
                        this.groups = res;
                    });
                    console.log("filter11", filter);
                }
                else if (this.searchtext != "") {
                    this.api.getGroups(1, "", this.searchtext).subscribe(res => {
                        this.groups = res;
                    });
                }
            }
        });
    }
    onSearchChange(event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Please wait...',
                duration: 1500
            });
            yield loading.present();
            if (this.searchtext != "" && this.fillter != "") {
                this.api.getGroups(1, this.fillter, this.searchtext).subscribe(res => {
                    this.groups = res;
                });
                console.log("searchtext", this.searchtext);
                console.log("filter", this.fillter);
            }
            else if (this.searchtext != "") {
                this.api.getGroups(1, "", this.searchtext).subscribe(res => {
                    this.groups = res;
                });
                console.log("searchtext", this.searchtext);
            }
            else {
                this.api.getGroups().subscribe(res => {
                    this.groups = res;
                });
            }
        });
    }
    leave(id) {
        this.api.leavegroup("leave", id, this.token, this.user_id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            var index = this.groups.findIndex(data => data.id === id);
            var data = this.groups.find(data => data.id === id);
            if (typeof (index) !== 'undefined') {
                data.is_member = false;
                data.members_count -= 1;
                this.groups.splice(index, 1, data);
            }
            const toast = yield this.toastCtrl.create({
                message: "Leave Group Successfully",
                duration: 3000
            });
            yield toast.present();
        }));
    }
    join(id) {
        this.api.joingroup("join", id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(res.data);
            var index = this.groups.findIndex(data => data.id === id);
            var data = this.groups.find(data => data.id === id);
            if (typeof (index) !== 'undefined') {
                data.is_member = true;
                data.members_count += 1;
                this.groups.splice(index, 1, data);
            }
            const toast = yield this.toastCtrl.create({
                message: "Join Group Successfully",
                duration: 3000
            });
            yield toast.present();
        }));
    }
    ngOnInit() {
        this.route.queryParams.subscribe(data => {
            console.log("data.id", data.id);
            this.id = data.id;
            this.name = data.name;
        });
        if (this.id) {
            this.api.getgroupMembers(this.id).subscribe(res => {
                console.log(res);
                this.groups = res;
            });
        }
        else {
            this.api.getGroups().subscribe(res => {
                console.log(res);
                this.groups = res;
            });
        }
    }
    goTonewgroup() {
        this.router.navigate(['/group-update']);
    }
    goTomemberDetail() {
        this.router.navigate(['/member-detail']);
    }
    BackButton() {
        this.location.back();
    }
};
GroupsListPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_8__["NativePageTransitions"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _services_api_service__WEBPACK_IMPORTED_MODULE_5__["apiService"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__["dummyDataService"] }
];
GroupsListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-groups-list',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./groups-list.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/groups-list/groups-list.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./groups-list.page.scss */ "./src/app/pages/groups-list/groups-list.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_8__["NativePageTransitions"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _services_api_service__WEBPACK_IMPORTED_MODULE_5__["apiService"],
        _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"],
        src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__["dummyDataService"]])
], GroupsListPage);



/***/ }),

/***/ "./src/app/services/dummy-data.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/dummy-data.service.ts ***!
  \************************************************/
/*! exports provided: dummyDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dummyDataService", function() { return dummyDataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

let dummyDataService = class dummyDataService {
    constructor() {
        this.users = [
            {
                img: 'assets/imgs/user.jpg',
                cover: 'assets/imgs/back1.jpg',
                btn_text: 'VIEW JOBS'
            },
            {
                img: 'assets/imgs/user2.jpg',
                cover: 'assets/imgs/back2.jpg',
                btn_text: 'FOLLOW HASHTAG'
            },
            {
                img: 'assets/imgs/user3.jpg',
                cover: 'assets/imgs/back3.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user4.jpg',
                cover: 'assets/imgs/back4.jpg',
                btn_text: 'SEE ALL VIEW'
            },
            {
                img: 'assets/imgs/user.jpg',
                cover: 'assets/imgs/back1.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user2.jpg',
                cover: 'assets/imgs/back2.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user3.jpg',
                cover: 'assets/imgs/back3.jpg',
                btn_text: 'EXPAND YOUR NETWORK'
            },
            {
                img: 'assets/imgs/user4.jpg',
                cover: 'assets/imgs/back4.jpg',
                btn_text: 'SEE ALL VIEW'
            }
        ];
        this.jobs = [
            {
                img: 'assets/imgs/company/initappz.png',
                title: 'SQL Server',
                addr: 'Bhavnagar, Gujrat, India',
                time: '2 school alumni',
                status: '1 day'
            },
            {
                img: 'assets/imgs/company/google.png',
                title: 'Web Designer',
                addr: 'Vadodara, Gujrat, India',
                time: 'Be an early applicant',
                status: '2 days'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                title: 'Business Analyst',
                addr: 'Ahmedabad, Gujrat, India',
                time: 'Be an early applicant',
                status: '1 week'
            },
            {
                img: 'assets/imgs/company/initappz.png',
                title: 'PHP Developer',
                addr: 'Pune, Maharashtra, India',
                time: '2 school alumni',
                status: 'New'
            },
            {
                img: 'assets/imgs/company/google.png',
                title: 'Graphics Designer',
                addr: 'Bhavnagar, Gujrat, India',
                time: 'Be an early applicant',
                status: '1 day'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                title: 'Phython Developer',
                addr: 'Ahmedabad, Gujrat, India',
                time: '2 school alumni',
                status: '5 days'
            },
        ];
        this.company = [
            {
                img: 'assets/imgs/company/initappz.png',
                name: 'Initappz Shop'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                name: 'Microsoft'
            },
            {
                img: 'assets/imgs/company/google.png',
                name: 'Google'
            },
        ];
    }
};
dummyDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], dummyDataService);



/***/ })

}]);
//# sourceMappingURL=pages-groups-list-groups-list-module-es2015.js.map
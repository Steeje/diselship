(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-members-detail-members-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/members-detail/members-detail.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/members-detail/members-detail.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-button (click)=\"BackButton()\" slot=\"start\">\n      <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n    </ion-button>\n    <ion-title color=\"light\">{{data.name}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n\n\n\n\n\n<ion-content>\n  <div class=\"main_content_div\">\n    <div class=\"cover\" [style.backgroundImage]=\"'url('+data.cover+')'\">\n      <ion-card id=\"content\" >\n        <ion-avatar id=\"profile-info\">\n          <img id=\"profile-image\" src=\"{{data.avatar}}\">\n        </ion-avatar>\n      <ion-label style=\"    text-align: center;\n      font-size: 18px;\n      font-weight: bold;\n      padding-top: 70px;\n      padding-bottom: 10px;\">{{data.name}} </ion-label>\n              <ion-label style=\"text-align: center;\n             font-size: 16px;\n    padding-bottom: 10px;\n              \">@{{data.username}}</ion-label>\n            <ion-grid fixed><ion-row style=\" margin-top: 5px;\">\n            <ion-col size=\"4\">\n              <!-- <ion-icon  name=\"newspaper-outline\" mode=\"md\"></ion-icon> -->\n              <!-- <ion-icon (click)=\"unfollowuser(item.id)\" *ngIf=\"item.is_following\" name=\"newspaper\" mode=\"md\"></ion-icon>\n            </div> -->\n            <!-- <div class=\"menu_div\"  *ngIf=\"item.create_friendship\"> -->\n            \n\n              <ion-label class=\"icon-label\"  *ngIf=\"data.friendship_status=='not_friends'\" (click)=\"connectuser()\"> <ion-icon  class=\"icon-style\" name=\"person-add-outline\"></ion-icon>\n                \n              </ion-label>\n              <ion-label  *ngIf=\"data.friendship_status=='not_friends'\"  class=\"text-label\">Send Requset </ion-label>\n\n              <ion-label *ngIf=\"data.friendship_status!='not_friends'\" class=\"icon-label\" (click)=\"deleteuser()\" style=\"background-color: #328af5 !important; color:white\"> <ion-icon  class=\"icon-style\" name=\"person-remove-outline\"></ion-icon>\n                \n              </ion-label>\n              <ion-label *ngIf=\"data.friendship_status!='not_friends'\" class=\"text-label\">Friend Requset </ion-label>\n            </ion-col>\n            <ion-col size=\"4\">\n              <ion-label class=\"icon-label\" (click)=\"followuser()\" *ngIf=\"!data.is_following\"> <ion-icon  class=\"icon-style\" name=\"receipt-outline\"></ion-icon></ion-label>\n              <ion-label  *ngIf=\"!data.is_following\" class=\"text-label\" >Follow</ion-label>\n\n              <ion-label class=\"icon-label\" (click)=\"unfollowuser()\" *ngIf=\"data.is_following\" style=\"background-color: #328af5 !important; color:white\"> <ion-icon  class=\"icon-style\" name=\"receipt-outline\"></ion-icon></ion-label>\n              <ion-label  *ngIf=\"data.is_following\" class=\"text-label\" >Unfollow</ion-label>\n            </ion-col>\n            <ion-col size=\"4\">\n              <ion-label class=\"icon-label\" [routerLink]=\"['/members-profile', data.id]\" > <ion-icon  class=\"icon-style\" name=\"person-outline\"></ion-icon></ion-label>\n              <ion-label  class=\"text-label\">Profile </ion-label>\n            </ion-col>\n            <!-- <ion-col size=\"3\">\n              <ion-label class=\"icon-label\"> <ion-icon  class=\"icon-style\" name=\"eye-outline\"></ion-icon></ion-label>\n              <ion-label  class=\"text-label\" >View As </ion-label>\n            </ion-col> -->\n        </ion-row>\n       \n\n        \n        </ion-grid>\n      </ion-card>\n      <!-- <div class=\"user_image\">\n        <img src=\"assets/imgs/company/initappz.png\" alt=\"\">\n       \n      </div>\n      <div class=\"data_div\">\n        <ion-label class=\"group_title\">Web Developers</ion-label>\n        <ion-label class=\"group_type\">Public / Group . 10 Memers</ion-label>\n        <ion-label class=\"description\">Lorem ipsum\n          dolor sit amet\n          \n          consectetur adipiscing elit. Duis ut urna neque.\n          \n          </ion-label>\n        \n         <div class=\"admin\">\n          <img src=\"assets/imgs/clock.png\">\n          <ion-label class=\"admin_name\">John</ion-label>\n        </div> \n        </div> -->\n        \n    </div>\n   \n    <div style=\"display: none;\" class=\"personal_div\">\n     \n   \n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"pulse-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Timeline </p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"person-add-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Connections </p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"people-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Groups </p>\n         \n        </ion-label>\n  \n      </div> <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"images-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Photos </p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"chatbox-ellipses-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Forums </p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"star-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Points </p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"stats-chart-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Achivements </p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"podium-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Ranks </p>\n         \n        </ion-label>\n      </div>\n     \n\n      <!-- <ion-grid fixed>\n        <ion-row>\n          <ion-col size=\"6\">\n            <ion-button expand=\"block\" class=\"save\" size=\"small\" fill=\"outline\">\n              SAVE\n            </ion-button>\n          </ion-col>\n          <ion-col size=\"6\">\n            <ion-button size=\"small\" class=\"apply\" expand=\"block\">\n              APPLY\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid> -->\n    </div>\n\n    <!-- <div class=\"job_div\">\n      <ion-label class=\"job_header\">Job Description</ion-label>\n      <ion-label>Web Designer - No of opening : 6</ion-label>\n      <ion-label>Required Experience : 1 - 3 Years</ion-label>\n      <ion-label>Job brief We are looking for a talented Web..</ion-label>\n    </div>\n\n    <div class=\"white_div\">\n      <ion-label class=\"blue_lbl\" style=\"text-align: center;\">SEE MORE</ion-label>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>Set alert for similar jobs</ion-label>\n      <ion-label class=\"light_lbl\">Web Designer, Bhavnagar, India</ion-label>\n      <ion-toggle></ion-toggle>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>See company insights for Initappz Shop with Premiun</ion-label>\n      <ion-button class=\"premium_btn\" expand=\"block\">\n        TRY PREMIUM FOR FREE\n      </ion-button>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>Keep up with updates and jobs</ion-label>\n      <div class=\"flex_div\">\n        <img src=\"assets/imgs/user.jpg\" class=\"image\">\n        <div class=\"content_div\">\n          <ion-label class=\"head_lbl\">Initappz Shop</ion-label>\n          <ion-label class=\"small_lbl\">E-learning 22,300 followers</ion-label>\n        </div>\n        <div>\n          <ion-button class=\"fallow_btn\" expand=\"block\" fill=\"clear\" size=\"small\">\n            + Follow\n          </ion-button>\n        </div>\n      </div>\n\n    </div> -->\n  </div>\n</ion-content>\n<!-- <ion-footer>\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button expand=\"block\" class=\"save\" size=\"small\" fill=\"outline\">\n          SAVE\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-button size=\"small\" class=\"apply\" expand=\"block\">\n          APPLY\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer> -->");

/***/ }),

/***/ "./src/app/pages/members-detail/members-detail-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/members-detail/members-detail-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: MembersDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MembersDetailPageRoutingModule", function() { return MembersDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _members_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./members-detail.page */ "./src/app/pages/members-detail/members-detail.page.ts");




const routes = [
    {
        path: '',
        component: _members_detail_page__WEBPACK_IMPORTED_MODULE_3__["MembersDetailPage"]
    }
];
let MembersDetailPageRoutingModule = class MembersDetailPageRoutingModule {
};
MembersDetailPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MembersDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/members-detail/members-detail.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/members-detail/members-detail.module.ts ***!
  \***************************************************************/
/*! exports provided: MembersDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MembersDetailPageModule", function() { return MembersDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _members_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./members-detail-routing.module */ "./src/app/pages/members-detail/members-detail-routing.module.ts");
/* harmony import */ var _members_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./members-detail.page */ "./src/app/pages/members-detail/members-detail.page.ts");







let MembersDetailPageModule = class MembersDetailPageModule {
};
MembersDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _members_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["MembersDetailPageRoutingModule"]
        ],
        declarations: [_members_detail_page__WEBPACK_IMPORTED_MODULE_6__["MembersDetailPage"]]
    })
], MembersDetailPageModule);



/***/ }),

/***/ "./src/app/pages/members-detail/members-detail.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/members-detail/members-detail.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: var(--ion-color-main);\n}\nion-toolbar ion-input {\n  border-bottom: 1px solid white;\n  --color: white;\n  --placeholder-color: white;\n  --padding-start: 8px;\n}\nion-toolbar ion-icon {\n  color: white;\n  font-size: 24px;\n}\n#content {\n  position: relative;\n  background-color: #ffffff;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n  /* padding-top:200px; */\n  border-radius: 15px;\n  height: 225px;\n  overflow: unset;\n  display: block;\n  top: 135px;\n}\n#profile-info {\n  position: absolute;\n  top: -95px;\n  width: 100%;\n  z-index: 2;\n  text-align: center;\n}\n.icon-style {\n  width: 100%;\n  height: 53%;\n  margin-top: 12px;\n}\n.text-label {\n  margin-top: 55px;\n  text-align: center;\n  font-size: 12px;\n  font-weight: bold;\n}\n.icon-label {\n  border-radius: 50%;\n  background: #f5f5f5;\n  left: 33%;\n  width: 50px;\n  box-shadow: 0px 3px 7px #8f8989;\n  height: 50px;\n  position: absolute;\n  top: 0;\n  cursor: pointer;\n  z-index: 9;\n  box-sizing: border-box;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n#profile-image {\n  display: block;\n  border-radius: 120px;\n  border: 3px solid #fff;\n  width: 128px;\n  background: white;\n  height: 128px;\n  margin: 30px auto 0;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n}\n.save {\n  --border-color: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 16px;\n  --border-radius: 3px;\n  color: var(--ion-color-main);\n}\n.apply {\n  --background: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 16px;\n  --border-radius: 3px;\n}\nion-content {\n  --background: #F5F5F5;\n}\n.white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n.white_div .left-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  left: 16px;\n  font-size: 26px;\n}\n.white_div .lable-icon {\n  padding-left: 22px;\n  padding-top: 5px;\n  color: var(--ion-color-main);\n  font-size: 17px;\n  cursor: pointer;\n}\n.white_div .right-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  right: 16px;\n  font-size: 20px;\n}\n.white_div ion-button {\n  color: var(--ion-color-main);\n  font-size: 600;\n}\n.main_content_div {\n  width: 100%;\n}\n.main_content_div .no_border {\n  border-bottom: none !important;\n}\n.main_content_div ion-label {\n  display: block;\n}\n.main_content_div .cover {\n  width: 100%;\n  height: 200px;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  position: relative;\n}\n.main_content_div .cover .user_image {\n  width: 70px;\n  height: 70px;\n  border-radius: 15px;\n  position: absolute;\n  left: 40%;\n  top: 25%;\n  background: white;\n  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.2);\n}\n.main_content_div .cover .data_div {\n  padding: 16px;\n  padding-top: 114px;\n  padding-right: 10%;\n}\n.main_content_div .cover .data_div ion-label {\n  margin-bottom: 2px;\n}\n.main_content_div .cover .data_div .group_title {\n  text-align: center;\n  font-size: 20px;\n  padding-top: 30px;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n  font-weight: bolder;\n}\n.main_content_div .cover .data_div .group_type {\n  font-size: 14px;\n  text-align: center;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n}\n.main_content_div .cover .data_div .description {\n  font-size: 14px;\n  text-align: center;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n}\n.main_content_div .cover .data_div .admin {\n  display: flex;\n  padding-bottom: 5px;\n  text-align: center;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n}\n.main_content_div .cover .data_div .admin img {\n  width: 15px;\n  height: 15px;\n  text-align: center;\n}\n.main_content_div .cover .data_div .admin .admin_name {\n  text-align: center;\n  margin-left: 10px;\n  padding: 0;\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .personal_div {\n  background: white;\n  padding: 16px;\n  margin-top: 145px;\n}\n.main_content_div .personal_div ion-label {\n  margin-bottom: 2px;\n}\n.main_content_div .personal_div .job_title {\n  font-size: 20px;\n  padding-top: 30px;\n}\n.main_content_div .personal_div .compamy_name {\n  font-size: 14px;\n}\n.main_content_div .personal_div .address {\n  font-size: 14px;\n}\n.main_content_div .personal_div .easy_div {\n  display: flex;\n  padding-bottom: 5px;\n}\n.main_content_div .personal_div .easy_div img {\n  width: 15px;\n  height: 15px;\n}\n.main_content_div .personal_div .easy_div .easy {\n  font-size: 12px;\n  margin-left: 10px;\n  padding: 0;\n}\n.main_content_div .personal_div .easy_div .time {\n  margin-left: 10px;\n  padding: 0;\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .job_div {\n  background: white;\n  padding: 16px;\n  margin-top: 10px;\n}\n.main_content_div .job_div .job_header {\n  padding-bottom: 5px;\n  font-size: 16px !important;\n}\n.main_content_div .job_div ion-label {\n  font-size: 14px;\n  padding-bottom: 5px;\n}\n.main_content_div .white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n  border-top: 1px solid lightgray;\n}\n.main_content_div .white_div .blue_lbl {\n  color: var(--ion-color-main);\n  padding-top: 15px;\n  padding-bottom: 15px;\n  font-weight: 600;\n}\n.main_content_div .desc_div {\n  margin-top: 10px;\n  padding: 16px;\n  background: white;\n  position: relative;\n}\n.main_content_div .desc_div .light_lbl {\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .desc_div ion-toggle {\n  position: absolute;\n  right: 16px;\n  top: 50%;\n  transform: translateY(-50%);\n}\n.main_content_div .desc_div .premium_btn {\n  margin-top: 10px;\n  --border-radius: 3px;\n  height: 40px;\n  --background: #78710c;\n}\n.main_content_div .desc_div .flex_div {\n  display: flex;\n  align-items: center;\n  padding-top: 10px;\n  width: 100%;\n  justify-content: space-between;\n}\n.main_content_div .desc_div .flex_div .image {\n  height: 45px;\n  width: 45px;\n  max-width: 45px;\n  border-radius: 3px;\n}\n.main_content_div .desc_div .flex_div .content_div {\n  margin-left: 10px;\n}\n.main_content_div .desc_div .flex_div .content_div .head_lbl {\n  font-weight: 600;\n}\n.main_content_div .desc_div .flex_div .content_div .small_lbl {\n  font-size: 14px;\n}\n.main_content_div .desc_div .flex_div .fallow_btn {\n  font-size: 14px;\n  font-weight: 600;\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVtYmVycy1kZXRhaWwvRDpcXFJhZ3V2YXJhbiBpbWFnZVxcZGlzZWxzaGlwL3NyY1xcYXBwXFxwYWdlc1xcbWVtYmVycy1kZXRhaWxcXG1lbWJlcnMtZGV0YWlsLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbWVtYmVycy1kZXRhaWwvbWVtYmVycy1kZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUNBQUE7QUNDSjtBREFJO0VBQ0ksOEJBQUE7RUFDQSxjQUFBO0VBQ0EsMEJBQUE7RUFDQSxvQkFBQTtBQ0VSO0FEQUk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtBQ0VSO0FEQ0E7RUFDSSxrQkFBQTtFQUVBLHlCQUFBO0VBQ0EsNENBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0UsYUFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsVUFBQTtBQ0NOO0FER0U7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBRUEsVUFBQTtFQUNBLGtCQUFBO0FDREo7QURHRTtFQUNFLFdBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUNBSjtBREVBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0NBO0FERUU7RUFDRSxrQkFBQTtFQUVELG1CQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSwrQkFBQTtFQUNBLFlBQUE7RUFDQyxrQkFBQTtFQUNBLE1BQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUNBSjtBREVFO0VBQ0UsY0FBQTtFQUNBLG9CQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw0Q0FBQTtBQ0NKO0FERUE7RUFDSSxxQ0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLG9CQUFBO0VBQ0EsNEJBQUE7QUNDSjtBRENBO0VBQ0ksbUNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtBQ0VKO0FEQUE7RUFDSSxxQkFBQTtBQ0dKO0FEREE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0lKO0FEVUk7RUFDSSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUNSUjtBRFVJO0VBQ00sa0JBQUE7RUFDQSxnQkFBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7QUNSVjtBRFVJO0VBQ0ksNEJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FDUlI7QURXSTtFQUNJLDRCQUFBO0VBQ0EsY0FBQTtBQ1RSO0FEWUE7RUFDSSxXQUFBO0FDVEo7QURXSTtFQUNJLDhCQUFBO0FDVFI7QURZSTtFQUNJLGNBQUE7QUNWUjtBRGFJO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtBQ1hSO0FEWVE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUlBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLGlCQUFBO0VBQ0EsMENBQUE7QUNiWjtBRGVRO0VBQ0ksYUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNiWjtBRGVZO0VBQ0ksa0JBQUE7QUNiaEI7QURnQlk7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FDZGhCO0FEa0JZO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0FDaEJoQjtBRGtCWTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtBQ2hCaEI7QURrQlk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtBQ2hCaEI7QURpQmdCO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ2ZwQjtBRGlCZ0I7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDZnBCO0FEdUJJO0VBQ0ksaUJBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7QUNyQlI7QURzQlE7RUFDSSxrQkFBQTtBQ3BCWjtBRHVCUTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtBQ3JCWjtBRHVCUTtFQUNJLGVBQUE7QUNyQlo7QUR1QlE7RUFDSSxlQUFBO0FDckJaO0FEdUJRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FDckJaO0FEc0JZO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNwQmhCO0FEc0JZO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtBQ3BCaEI7QURzQlk7RUFDSSxpQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ3BCaEI7QUR5Qkk7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQ3ZCUjtBRDBCUTtFQUNJLG1CQUFBO0VBQ0EsMEJBQUE7QUN4Qlo7QUQyQlE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QUN6Qlo7QUQ0Qkk7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLCtCQUFBO0FDMUJSO0FEMkJRO0VBQ0ksNEJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7QUN6Qlo7QUQ2Qkk7RUFDSSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDM0JSO0FENkJRO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUMzQlo7QUQ4QlE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsMkJBQUE7QUM1Qlo7QUQrQlE7RUFDSSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0FDN0JaO0FEZ0NRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsOEJBQUE7QUM5Qlo7QUQrQlk7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQzdCaEI7QURnQ1k7RUFDSSxpQkFBQTtBQzlCaEI7QUQrQmdCO0VBQ0ksZ0JBQUE7QUM3QnBCO0FEK0JnQjtFQUNJLGVBQUE7QUM3QnBCO0FEaUNZO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsU0FBQTtBQy9CaEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tZW1iZXJzLWRldGFpbC9tZW1iZXJzLWRldGFpbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgaW9uLWlucHV0e1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG4gICAgICAgIC0tY29sb3I6IHdoaXRlO1xuICAgICAgICAtLXBsYWNlaG9sZGVyLWNvbG9yIDogd2hpdGU7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydCA6IDhweDtcbiAgICB9XG4gICAgaW9uLWljb257XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xuICAgIH1cbn1cbiNjb250ZW50IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgLy8gbWFyZ2luLXRvcDogYXV0bztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNmZmZmZmY7XG4gICAgYm94LXNoYWRvdzogMHB4IC0xcHggMTBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gICAgLyogcGFkZGluZy10b3A6MjAwcHg7ICovXG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAgIGhlaWdodDogMjI1cHg7XG4gICAgICBvdmVyZmxvdzogdW5zZXQ7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHRvcDogMTM1cHg7XG4gICAgIC8vIG1hcmdpbi10b3A6IDEwMHB4O1xuICB9XG4gIFxuICAjcHJvZmlsZS1pbmZvIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAtOTVweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBcbiAgICB6LWluZGV4OiAyO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICAuaWNvbi1zdHlsZXtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDUzJTtcbiAgICBtYXJnaW4tdG9wOiAxMnB4O1xufVxuLnRleHQtbGFiZWwge1xubWFyZ2luLXRvcDogNTVweDtcbnRleHQtYWxpZ246IGNlbnRlcjtcbmZvbnQtc2l6ZTogMTJweDtcbmZvbnQtd2VpZ2h0OiBib2xkO1xuXG59XG4gIC5pY29uLWxhYmVsIHtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAvLyBib3JkZXI6IDJweCBzb2xpZCByZWQ7XG4gICBiYWNrZ3JvdW5kOiAjZjVmNWY1O1xuICAgbGVmdDogMzMlO1xuICAgd2lkdGg6NTBweDtcbiAgIGJveC1zaGFkb3c6IDBweCAzcHggN3B4ICM4Zjg5ODk7XG4gICBoZWlnaHQ6IDUwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgei1pbmRleDogOTtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgfVxuICAjcHJvZmlsZS1pbWFnZSB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgYm9yZGVyLXJhZGl1czogMTIwcHg7XG4gICAgYm9yZGVyOiAzcHggc29saWQgI2ZmZjtcbiAgICB3aWR0aDogMTI4cHg7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgaGVpZ2h0OiAxMjhweDtcbiAgICBtYXJnaW46IDMwcHggYXV0byAwO1xuICAgIGJveC1zaGFkb3c6IDBweCAtMXB4IDEwcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAgLy8gYm94LXNoYWRvdzogMHB4IDBweCA0cHggcmdiYSgwLCAwLCAwLCAwLjcpO1xuICB9XG4uc2F2ZXtcbiAgICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG4uYXBwbHl7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG59XG5pb24tY29udGVudHtcbiAgICAtLWJhY2tncm91bmQ6ICNGNUY1RjU7XG59XG4ud2hpdGVfZGl2e1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHBhZGRpbmctbGVmdDogMTZweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgLy8gaW9uLWJ1dHRvbntcbiAgICAvLyAgICAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgLy8gICAgIGlvbi1sYWJlbCB7XG4gICAgLy8gICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIC8vICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgLy8gICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgLy8gICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIC8vICAgICB9ICAgXG4gICAgLy8gICAgIGlvbi1pY29ue1xuICAgIC8vICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAvLyAgICAgfVxuICAgIC8vIH1cblxuICAgIC5sZWZ0LWljb257XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgbGVmdDogMTZweDtcbiAgICAgICAgZm9udC1zaXplOiAyNnB4O1xuICAgIH1cbiAgICAubGFibGUtaWNvbntcbiAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDIycHg7XG4gICAgICAgICAgcGFkZGluZy10b3A6IDVweDtcbiAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxuICAgIC5yaWdodC1pY29ue1xuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHJpZ2h0OiAxNnB4O1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgfVxuXG4gICAgaW9uLWJ1dHRvbntcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgZm9udC1zaXplOiA2MDA7XG4gICAgfVxufVxuLm1haW5fY29udGVudF9kaXZ7XG4gICAgd2lkdGg6IDEwMCU7XG5cbiAgICAubm9fYm9yZGVye1xuICAgICAgICBib3JkZXItYm90dG9tOiBub25lICFpbXBvcnRhbnQ7XG4gICAgfVxuXG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuXG4gICAgLmNvdmVye1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAyMDBweDtcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIC51c2VyX2ltYWdle1xuICAgICAgICAgICAgd2lkdGg6IDcwcHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDcwcHg7XG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICBsZWZ0OiA0MCU7XG4gICAgICAgICAgICB0b3A6IDI1JTtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDNweCA2cHggcmdiYSgwLDAsMCwwLjIpO1xuICAgICAgICB9XG4gICAgICAgIC5kYXRhX2RpdntcbiAgICAgICAgICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTE0cHg7XG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMCU7XG4gICAgXG4gICAgICAgICAgICBpb24tbGFiZWwge1xuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDJweDtcbiAgICAgICAgICAgIH1cbiAgICBcbiAgICAgICAgICAgIC5ncm91cF90aXRsZXtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgICAgIHBhZGRpbmctdG9wOiAzMHB4O1xuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgICAgICB0ZXh0LXNoYWRvdzogMnB4IDJweCA4cHggYmxhY2s7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgLmdyb3VwX3R5cGV7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgICAgICAgICAgdGV4dC1zaGFkb3c6IDJweCAycHggOHB4IGJsYWNrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmRlc2NyaXB0aW9ue1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgICAgIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5hZG1pbntcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgICAgICB0ZXh0LXNoYWRvdzogMnB4IDJweCA4cHggYmxhY2s7XG4gICAgICAgICAgICAgICAgaW1ne1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTVweDtcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxNXB4O1xuICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5hZG1pbl9uYW1le1xuICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIFxuICAgIH1cblxuICAgICAgIFxuICAgIC5wZXJzb25hbF9kaXZ7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICBtYXJnaW4tdG9wOiAxNDVweDtcbiAgICAgICAgaW9uLWxhYmVsIHtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDJweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5qb2JfdGl0bGV7XG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMzBweDtcbiAgICAgICAgfVxuICAgICAgICAuY29tcGFteV9uYW1le1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICB9XG4gICAgICAgIC5hZGRyZXNze1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICB9XG4gICAgICAgIC5lYXN5X2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgICAgaW1ne1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxNXB4O1xuICAgICAgICAgICAgICAgIGhlaWdodDogMTVweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5lYXN5e1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnRpbWV7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuam9iX2RpdntcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG5cblxuICAgICAgICAuam9iX2hlYWRlcntcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHggIWltcG9ydGFudDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICB9XG4gICAgfVxuICAgIC53aGl0ZV9kaXZ7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgIC5ibHVlX2xibHtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTVweDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5kZXNjX2RpdntcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgcGFkZGluZzogMTZweDtcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICAgICAgICAubGlnaHRfbGJse1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgIH1cblxuICAgICAgICBpb24tdG9nZ2xle1xuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgcmlnaHQ6IDE2cHg7XG4gICAgICAgICAgICB0b3A6IDUwJTtcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC5wcmVtaXVtX2J0bntcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgICAgICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDogIzc4NzEwYztcbiAgICAgICAgfVxuXG4gICAgICAgIC5mbGV4X2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgICAgIC5pbWFnZXtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDQ1cHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDQ1cHg7XG4gICAgICAgICAgICAgICAgbWF4LXdpZHRoOiA0NXB4O1xuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmNvbnRlbnRfZGl2e1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgIC5oZWFkX2xibHtcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLnNtYWxsX2xibHtcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmZhbGxvd19idG57XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufSIsImlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5pb24tdG9vbGJhciBpb24taW5wdXQge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XG4gIC0tY29sb3I6IHdoaXRlO1xuICAtLXBsYWNlaG9sZGVyLWNvbG9yOiB3aGl0ZTtcbiAgLS1wYWRkaW5nLXN0YXJ0OiA4cHg7XG59XG5pb24tdG9vbGJhciBpb24taWNvbiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAyNHB4O1xufVxuXG4jY29udGVudCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgYm94LXNoYWRvdzogMHB4IC0xcHggMTBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gIC8qIHBhZGRpbmctdG9wOjIwMHB4OyAqL1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBoZWlnaHQ6IDIyNXB4O1xuICBvdmVyZmxvdzogdW5zZXQ7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0b3A6IDEzNXB4O1xufVxuXG4jcHJvZmlsZS1pbmZvIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IC05NXB4O1xuICB3aWR0aDogMTAwJTtcbiAgei1pbmRleDogMjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uaWNvbi1zdHlsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDUzJTtcbiAgbWFyZ2luLXRvcDogMTJweDtcbn1cblxuLnRleHQtbGFiZWwge1xuICBtYXJnaW4tdG9wOiA1NXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5pY29uLWxhYmVsIHtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kOiAjZjVmNWY1O1xuICBsZWZ0OiAzMyU7XG4gIHdpZHRoOiA1MHB4O1xuICBib3gtc2hhZG93OiAwcHggM3B4IDdweCAjOGY4OTg5O1xuICBoZWlnaHQ6IDUwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIHotaW5kZXg6IDk7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4jcHJvZmlsZS1pbWFnZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBib3JkZXItcmFkaXVzOiAxMjBweDtcbiAgYm9yZGVyOiAzcHggc29saWQgI2ZmZjtcbiAgd2lkdGg6IDEyOHB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgaGVpZ2h0OiAxMjhweDtcbiAgbWFyZ2luOiAzMHB4IGF1dG8gMDtcbiAgYm94LXNoYWRvdzogMHB4IC0xcHggMTBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XG59XG5cbi5zYXZlIHtcbiAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxNnB4O1xuICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuLmFwcGx5IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG59XG5cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjRjVGNUY1O1xufVxuXG4ud2hpdGVfZGl2IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmctbGVmdDogMTZweDtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cbi53aGl0ZV9kaXYgLmxlZnQtaWNvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMTZweDtcbiAgZm9udC1zaXplOiAyNnB4O1xufVxuLndoaXRlX2RpdiAubGFibGUtaWNvbiB7XG4gIHBhZGRpbmctbGVmdDogMjJweDtcbiAgcGFkZGluZy10b3A6IDVweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC1zaXplOiAxN3B4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4ud2hpdGVfZGl2IC5yaWdodC1pY29uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTZweDtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuLndoaXRlX2RpdiBpb24tYnV0dG9uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC1zaXplOiA2MDA7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAubm9fYm9yZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogbm9uZSAhaW1wb3J0YW50O1xufVxuLm1haW5fY29udGVudF9kaXYgaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY292ZXIge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY292ZXIgLnVzZXJfaW1hZ2Uge1xuICB3aWR0aDogNzBweDtcbiAgaGVpZ2h0OiA3MHB4O1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDQwJTtcbiAgdG9wOiAyNSU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3gtc2hhZG93OiAwcHggM3B4IDZweCByZ2JhKDAsIDAsIDAsIDAuMik7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY292ZXIgLmRhdGFfZGl2IHtcbiAgcGFkZGluZzogMTZweDtcbiAgcGFkZGluZy10b3A6IDExNHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY292ZXIgLmRhdGFfZGl2IGlvbi1sYWJlbCB7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgLmdyb3VwX3RpdGxlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDIwcHg7XG4gIHBhZGRpbmctdG9wOiAzMHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgLmdyb3VwX3R5cGUge1xuICBmb250LXNpemU6IDE0cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6IHdoaXRlO1xuICB0ZXh0LXNoYWRvdzogMnB4IDJweCA4cHggYmxhY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY292ZXIgLmRhdGFfZGl2IC5kZXNjcmlwdGlvbiB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgLmFkbWluIHtcbiAgZGlzcGxheTogZmxleDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgLmFkbWluIGltZyB7XG4gIHdpZHRoOiAxNXB4O1xuICBoZWlnaHQ6IDE1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgLmFkbWluIC5hZG1pbl9uYW1lIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgcGFkZGluZzogMDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogZ3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZzogMTZweDtcbiAgbWFyZ2luLXRvcDogMTQ1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfZGl2IGlvbi1sYWJlbCB7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgLmpvYl90aXRsZSB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgcGFkZGluZy10b3A6IDMwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfZGl2IC5jb21wYW15X25hbWUge1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfZGl2IC5hZGRyZXNzIHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiAuZWFzeV9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiAuZWFzeV9kaXYgaW1nIHtcbiAgd2lkdGg6IDE1cHg7XG4gIGhlaWdodDogMTVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgLmVhc3lfZGl2IC5lYXN5IHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgcGFkZGluZzogMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgLmVhc3lfZGl2IC50aW1lIHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIHBhZGRpbmc6IDA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6IGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuam9iX2RpdiB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nOiAxNnB4O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmpvYl9kaXYgLmpvYl9oZWFkZXIge1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICBmb250LXNpemU6IDE2cHggIWltcG9ydGFudDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5qb2JfZGl2IGlvbi1sYWJlbCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC53aGl0ZV9kaXYge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgbGlnaHRncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLndoaXRlX2RpdiAuYmx1ZV9sYmwge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBwYWRkaW5nOiAxNnB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5saWdodF9sYmwge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOiBncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IGlvbi10b2dnbGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxNnB4O1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5wcmVtaXVtX2J0biB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIC0tYmFja2dyb3VuZDogIzc4NzEwYztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYgLmltYWdlIHtcbiAgaGVpZ2h0OiA0NXB4O1xuICB3aWR0aDogNDVweDtcbiAgbWF4LXdpZHRoOiA0NXB4O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiAuY29udGVudF9kaXYgLmhlYWRfbGJsIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IC5zbWFsbF9sYmwge1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IC5mYWxsb3dfYnRuIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBtYXJnaW46IDA7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/members-detail/members-detail.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/members-detail/members-detail.page.ts ***!
  \*************************************************************/
/*! exports provided: MembersDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MembersDetailPage", function() { return MembersDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/






let MembersDetailPage = class MembersDetailPage {
    constructor(api, storage, location, route, toastCtrl, router) {
        this.api = api;
        this.storage = storage;
        this.location = location;
        this.route = route;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.data = {};
        this.user = this.api.getCurrentUser();
        this.id = this.route.snapshot.paramMap.get('id');
        this.storage.get('COMPLETE_USER_INFO').then(result => {
            if (result != null) {
                console.log(result);
                this.token = result.token;
                this.user_id = result.user_id;
                console.log('user_id: ' + result.user_id);
                console.log('token: ' + result.token);
            }
        }).catch(e => {
            console.log('error: ' + e);
            // Handle errors here
        });
        this.user.subscribe(user => {
            if (user) {
                // location.href = "/home-new";
                this.api.getuser(this.id).subscribe((result) => {
                    console.log(result);
                    this.data = result;
                    console.log(this.data);
                    this.data.id = result.id;
                    this.data.username = result.user_login;
                    this.data.cover = result.cover_url;
                    this.data.fullname = result.xprofile.groups[1].fields[1].value.raw + " " + result.xprofile.groups[1].fields[2].value.raw;
                    this.data.description = result.description;
                    this.data.avatar = result.avatar_urls['full'];
                });
            }
        });
    }
    unfollowuser() {
        this.api.followuser("unfollow", this.data.id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(res.data);
            var result = res.data;
            this.data = result;
            console.log(this.data);
            this.data.id = result.id;
            this.data.username = result.user_login;
            this.data.cover = result.cover_url;
            this.data.fullname = result.xprofile.groups[1].fields[1].value.raw + " " + result.xprofile.groups[1].fields[2].value.raw;
            this.data.description = result.description;
            this.data.avatar = result.avatar_urls['full'];
            const toast = yield this.toastCtrl.create({
                message: "Unfollow User Successfully",
                duration: 3000
            });
            yield toast.present();
        }));
    }
    connectuser() {
        this.api.connectfrds(this.data.id, this.token, this.user_id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            var result = res.data;
            // this.data = result;
            //   console.log( this.data);
            //   this.data.id = result.id;
            //   this.data.username = result.user_login;
            //   this.data.cover=result.cover_url;
            //   this.data.fullname=result.xprofile.groups[1].fields[1].value.raw+" "+ result.xprofile.groups[1].fields[2].value.raw;
            //   this.data.description = result.description;
            this.data.friendship_status = "pending";
            const toast = yield this.toastCtrl.create({
                message: "Friend Request Sent Successfully",
                duration: 3000
            });
            yield toast.present();
        }));
    }
    deleteuser() {
        this.api.deletefrds(this.data.id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            var result = res.data;
            this.data.friendship_status = "not_friends";
            const toast = yield this.toastCtrl.create({
                message: "Unfriend Successfully",
                duration: 3000
            });
            yield toast.present();
        }));
    }
    BackButton() {
        this.location.back();
    }
    followuser() {
        this.api.followuser("follow", this.data.id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(res.data);
            var result = res.data;
            this.data = result;
            console.log(this.data);
            this.data.id = result.id;
            this.data.username = result.user_login;
            this.data.cover = result.cover_url;
            this.data.fullname = result.xprofile.groups[1].fields[1].value.raw + " " + result.xprofile.groups[1].fields[2].value.raw;
            this.data.description = result.description;
            this.data.avatar = result.avatar_urls['full'];
            const toast = yield this.toastCtrl.create({
                message: "Follow User Successfully",
                duration: 3000
            });
            yield toast.present();
        }));
    }
    editProfile() {
        this.router.navigate(['/members-profile']);
    }
    detailsProfile() {
        this.router.navigate(['/profile-detail']);
    }
    ngOnInit() {
    }
};
MembersDetailPage.ctorParameters = () => [
    { type: _services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
MembersDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-members-detail',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./members-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/members-detail/members-detail.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./members-detail.page.scss */ "./src/app/pages/members-detail/members-detail.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_4__["apiService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
], MembersDetailPage);



/***/ })

}]);
//# sourceMappingURL=pages-members-detail-members-detail-module-es2015.js.map
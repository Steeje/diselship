function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-job-detail-job-detail-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-detail/job-detail.page.html":
  /*!*********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-detail/job-detail.page.html ***!
    \*********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesJobDetailJobDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button mode=\"md\" color=\"light\"></ion-back-button>\n    </ion-buttons>\n    <ion-input type=\"text\" placeholder=\"Search\"></ion-input>\n    <ion-icon slot=\"end\" name=\"ellipsis-vertical\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n    <div class=\"cover\" [style.backgroundImage]=\"'url(assets/imgs/back1.jpg)'\">\n      <div class=\"user_image\">\n        <img src=\"assets/imgs/company/initappz.png\" alt=\"\">\n      </div>\n    </div>\n\n    <div class=\"personal_div\">\n\n      <ion-label class=\"job_title\">Web Designer (HTML Developer)</ion-label>\n      <ion-label class=\"compamy_name\">Initappz Shpp</ion-label>\n      <ion-label class=\"address\">Bhavnagar, IN</ion-label>\n      <div class=\"easy_div\">\n        <img src=\"assets/imgs/clock.png\">\n        <ion-label class=\"time\">Be among the first 20 Applicants</ion-label>\n      </div>\n\n      <ion-grid fixed>\n        <ion-row>\n          <ion-col size=\"6\">\n            <ion-button expand=\"block\" class=\"save\" size=\"small\" fill=\"outline\">\n              SAVE\n            </ion-button>\n          </ion-col>\n          <ion-col size=\"6\">\n            <ion-button size=\"small\" class=\"apply\" expand=\"block\">\n              APPLY\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n\n    <div class=\"job_div\">\n      <ion-label class=\"job_header\">Job Description</ion-label>\n      <ion-label>Web Designer - No of opening : 6</ion-label>\n      <ion-label>Required Experience : 1 - 3 Years</ion-label>\n      <ion-label>Job brief We are looking for a talented Web..</ion-label>\n    </div>\n\n    <div class=\"white_div\">\n      <ion-label class=\"blue_lbl\" style=\"text-align: center;\">SEE MORE</ion-label>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>Set alert for similar jobs</ion-label>\n      <ion-label class=\"light_lbl\">Web Designer, Bhavnagar, India</ion-label>\n      <ion-toggle></ion-toggle>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>See company insights for Initappz Shop with Premiun</ion-label>\n      <ion-button class=\"premium_btn\" expand=\"block\">\n        TRY PREMIUM FOR FREE\n      </ion-button>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>Keep up with updates and jobs</ion-label>\n      <div class=\"flex_div\">\n        <img src=\"assets/imgs/user.jpg\" class=\"image\">\n        <div class=\"content_div\">\n          <ion-label class=\"head_lbl\">Initappz Shop</ion-label>\n          <ion-label class=\"small_lbl\">E-learning 22,300 followers</ion-label>\n        </div>\n        <div>\n          <ion-button class=\"fallow_btn\" expand=\"block\" fill=\"clear\" size=\"small\">\n            + Follow\n          </ion-button>\n        </div>\n      </div>\n\n    </div>\n  </div>\n</ion-content>\n<ion-footer>\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button expand=\"block\" class=\"save\" size=\"small\" fill=\"outline\">\n          SAVE\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-button size=\"small\" class=\"apply\" expand=\"block\">\n          APPLY\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/pages/job-detail/job-detail-routing.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/pages/job-detail/job-detail-routing.module.ts ***!
    \***************************************************************/

  /*! exports provided: JobDetailPageRoutingModule */

  /***/
  function srcAppPagesJobDetailJobDetailRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "JobDetailPageRoutingModule", function () {
      return JobDetailPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _job_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./job-detail.page */
    "./src/app/pages/job-detail/job-detail.page.ts");

    var routes = [{
      path: '',
      component: _job_detail_page__WEBPACK_IMPORTED_MODULE_3__["JobDetailPage"]
    }];

    var JobDetailPageRoutingModule = function JobDetailPageRoutingModule() {
      _classCallCheck(this, JobDetailPageRoutingModule);
    };

    JobDetailPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], JobDetailPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/job-detail/job-detail.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/job-detail/job-detail.module.ts ***!
    \*******************************************************/

  /*! exports provided: JobDetailPageModule */

  /***/
  function srcAppPagesJobDetailJobDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "JobDetailPageModule", function () {
      return JobDetailPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _job_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./job-detail-routing.module */
    "./src/app/pages/job-detail/job-detail-routing.module.ts");
    /* harmony import */


    var _job_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./job-detail.page */
    "./src/app/pages/job-detail/job-detail.page.ts");

    var JobDetailPageModule = function JobDetailPageModule() {
      _classCallCheck(this, JobDetailPageModule);
    };

    JobDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _job_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["JobDetailPageRoutingModule"]],
      declarations: [_job_detail_page__WEBPACK_IMPORTED_MODULE_6__["JobDetailPage"]]
    })], JobDetailPageModule);
    /***/
  },

  /***/
  "./src/app/pages/job-detail/job-detail.page.scss":
  /*!*******************************************************!*\
    !*** ./src/app/pages/job-detail/job-detail.page.scss ***!
    \*******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesJobDetailJobDetailPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\nion-toolbar ion-input {\n  border-bottom: 1px solid white;\n  --color: white;\n  --placeholder-color: white;\n  --padding-start: 8px;\n}\nion-toolbar ion-icon {\n  color: white;\n  font-size: 24px;\n}\n.save {\n  --border-color: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 16px;\n  --border-radius: 3px;\n  color: var(--ion-color-main);\n}\n.apply {\n  --background: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 16px;\n  --border-radius: 3px;\n}\nion-content {\n  --background: #F5F5F5;\n}\n.main_content_div {\n  width: 100%;\n}\n.main_content_div .no_border {\n  border-bottom: none !important;\n}\n.main_content_div ion-label {\n  display: block;\n}\n.main_content_div .cover {\n  width: 100%;\n  height: 60px;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  position: relative;\n}\n.main_content_div .cover .user_image {\n  width: 70px;\n  height: 70px;\n  border-radius: 5px;\n  position: absolute;\n  left: 16px;\n  top: 45%;\n  background: white;\n  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.2);\n}\n.main_content_div .personal_div {\n  background: white;\n  padding: 16px;\n}\n.main_content_div .personal_div ion-label {\n  margin-bottom: 2px;\n}\n.main_content_div .personal_div .job_title {\n  font-size: 20px;\n  padding-top: 30px;\n}\n.main_content_div .personal_div .compamy_name {\n  font-size: 14px;\n}\n.main_content_div .personal_div .address {\n  font-size: 14px;\n}\n.main_content_div .personal_div .easy_div {\n  display: flex;\n  padding-bottom: 5px;\n}\n.main_content_div .personal_div .easy_div img {\n  width: 15px;\n  height: 15px;\n}\n.main_content_div .personal_div .easy_div .easy {\n  font-size: 12px;\n  margin-left: 10px;\n  padding: 0;\n}\n.main_content_div .personal_div .easy_div .time {\n  margin-left: 10px;\n  padding: 0;\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .job_div {\n  background: white;\n  padding: 16px;\n  margin-top: 10px;\n}\n.main_content_div .job_div .job_header {\n  padding-bottom: 5px;\n  font-size: 16px !important;\n}\n.main_content_div .job_div ion-label {\n  font-size: 14px;\n  padding-bottom: 5px;\n}\n.main_content_div .white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n  border-top: 1px solid lightgray;\n}\n.main_content_div .white_div .blue_lbl {\n  color: var(--ion-color-main);\n  padding-top: 15px;\n  padding-bottom: 15px;\n  font-weight: 600;\n}\n.main_content_div .desc_div {\n  margin-top: 10px;\n  padding: 16px;\n  background: white;\n  position: relative;\n}\n.main_content_div .desc_div .light_lbl {\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .desc_div ion-toggle {\n  position: absolute;\n  right: 16px;\n  top: 50%;\n  transform: translateY(-50%);\n}\n.main_content_div .desc_div .premium_btn {\n  margin-top: 10px;\n  --border-radius: 3px;\n  height: 40px;\n  --background: #78710c;\n}\n.main_content_div .desc_div .flex_div {\n  display: flex;\n  align-items: center;\n  padding-top: 10px;\n  width: 100%;\n  justify-content: space-between;\n}\n.main_content_div .desc_div .flex_div .image {\n  height: 45px;\n  width: 45px;\n  max-width: 45px;\n  border-radius: 3px;\n}\n.main_content_div .desc_div .flex_div .content_div {\n  margin-left: 10px;\n}\n.main_content_div .desc_div .flex_div .content_div .head_lbl {\n  font-weight: 600;\n}\n.main_content_div .desc_div .flex_div .content_div .small_lbl {\n  font-size: 14px;\n}\n.main_content_div .desc_div .flex_div .fallow_btn {\n  font-size: 14px;\n  font-weight: 600;\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvam9iLWRldGFpbC9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxqb2ItZGV0YWlsXFxqb2ItZGV0YWlsLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvam9iLWRldGFpbC9qb2ItZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1DQUFBO0FDQ0o7QURBSTtFQUNJLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0VBQ0Esb0JBQUE7QUNFUjtBREFJO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUNFUjtBRENBO0VBQ0kscUNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtFQUNBLDRCQUFBO0FDRUo7QURBQTtFQUNJLG1DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7QUNHSjtBRERBO0VBQ0kscUJBQUE7QUNJSjtBREZBO0VBQ0ksV0FBQTtBQ0tKO0FESEk7RUFDSSw4QkFBQTtBQ0tSO0FERkk7RUFDSSxjQUFBO0FDSVI7QURESTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7QUNHUjtBREZRO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFJQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7RUFDQSxpQkFBQTtFQUNBLDBDQUFBO0FDQ1o7QURJSTtFQUNJLGlCQUFBO0VBQ0EsYUFBQTtBQ0ZSO0FESVE7RUFDSSxrQkFBQTtBQ0ZaO0FES1E7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7QUNIWjtBREtRO0VBQ0ksZUFBQTtBQ0haO0FES1E7RUFDSSxlQUFBO0FDSFo7QURLUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtBQ0haO0FESVk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ0ZoQjtBRElZO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtBQ0ZoQjtBRElZO0VBQ0ksaUJBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNGaEI7QURPSTtFQUNJLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0FDTFI7QURRUTtFQUNJLG1CQUFBO0VBQ0EsMEJBQUE7QUNOWjtBRFNRO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0FDUFo7QURVSTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsK0JBQUE7QUNSUjtBRFNRO0VBQ0ksNEJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7QUNQWjtBRFdJO0VBQ0ksZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ1RSO0FEV1E7RUFDSSxlQUFBO0VBQ0EsV0FBQTtBQ1RaO0FEWVE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsMkJBQUE7QUNWWjtBRGFRO0VBQ0ksZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtBQ1haO0FEY1E7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSw4QkFBQTtBQ1paO0FEYVk7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ1hoQjtBRGNZO0VBQ0ksaUJBQUE7QUNaaEI7QURhZ0I7RUFDSSxnQkFBQTtBQ1hwQjtBRGFnQjtFQUNJLGVBQUE7QUNYcEI7QURlWTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFNBQUE7QUNiaEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9qb2ItZGV0YWlsL2pvYi1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGlvbi1pbnB1dHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAgICAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgLS1wbGFjZWhvbGRlci1jb2xvciA6IHdoaXRlO1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQgOiA4cHg7XG4gICAgfVxuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICB9XG59XG4uc2F2ZXtcbiAgICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG4uYXBwbHl7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG59XG5pb24tY29udGVudHtcbiAgICAtLWJhY2tncm91bmQ6ICNGNUY1RjU7XG59XG4ubWFpbl9jb250ZW50X2RpdntcbiAgICB3aWR0aDogMTAwJTtcblxuICAgIC5ub19ib3JkZXJ7XG4gICAgICAgIGJvcmRlci1ib3R0b206IG5vbmUgIWltcG9ydGFudDtcbiAgICB9XG5cbiAgICBpb24tbGFiZWwge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG5cbiAgICAuY292ZXJ7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDYwcHg7XG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAudXNlcl9pbWFnZXtcbiAgICAgICAgICAgIHdpZHRoOiA3MHB4O1xuICAgICAgICAgICAgaGVpZ2h0OiA3MHB4O1xuICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICBsZWZ0OiAxNnB4O1xuICAgICAgICAgICAgdG9wOiA0NSU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAzcHggNnB4IHJnYmEoMCwwLDAsMC4yKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgICAgIFxuICAgIC5wZXJzb25hbF9kaXZ7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuXG4gICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAycHg7XG4gICAgICAgIH1cblxuICAgICAgICAuam9iX3RpdGxle1xuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDMwcHg7XG4gICAgICAgIH1cbiAgICAgICAgLmNvbXBhbXlfbmFtZXtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgfVxuICAgICAgICAuYWRkcmVzc3tcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgfVxuICAgICAgICAuZWFzeV9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgICAgICAgICAgIGltZ3tcbiAgICAgICAgICAgICAgICB3aWR0aDogMTVweDtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDE1cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuZWFzeXtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC50aW1le1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmpvYl9kaXZ7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuXG5cbiAgICAgICAgLmpvYl9oZWFkZXJ7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cblxuICAgICAgICBpb24tbGFiZWwge1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgICAgICAgfVxuICAgIH1cbiAgICAud2hpdGVfZGl2e1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICAuYmx1ZV9sYmx7XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuZGVzY19kaXZ7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgICAgICAgLmxpZ2h0X2xibHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICB9XG5cbiAgICAgICAgaW9uLXRvZ2dsZXtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIHJpZ2h0OiAxNnB4O1xuICAgICAgICAgICAgdG9wOiA1MCU7XG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gICAgICAgIH1cblxuICAgICAgICAucHJlbWl1bV9idG57XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQ6ICM3ODcxMGM7XG4gICAgICAgIH1cblxuICAgICAgICAuZmxleF9kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgICAgICAuaW1hZ2V7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA0NXB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiA0NXB4O1xuICAgICAgICAgICAgICAgIG1heC13aWR0aDogNDVweDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5jb250ZW50X2RpdntcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgICAgICAgICAuaGVhZF9sYmx7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5zbWFsbF9sYmx7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5mYWxsb3dfYnRue1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0iLCJpb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXRvb2xiYXIgaW9uLWlucHV0IHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAtLWNvbG9yOiB3aGl0ZTtcbiAgLS1wbGFjZWhvbGRlci1jb2xvcjogd2hpdGU7XG4gIC0tcGFkZGluZy1zdGFydDogOHB4O1xufVxuaW9uLXRvb2xiYXIgaW9uLWljb24ge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMjRweDtcbn1cblxuLnNhdmUge1xuICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDE2cHg7XG4gIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuXG4uYXBwbHkge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxNnB4O1xuICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6ICNGNUY1RjU7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAubm9fYm9yZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogbm9uZSAhaW1wb3J0YW50O1xufVxuLm1haW5fY29udGVudF9kaXYgaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuY292ZXIge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA2MHB4O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAudXNlcl9pbWFnZSB7XG4gIHdpZHRoOiA3MHB4O1xuICBoZWlnaHQ6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAxNnB4O1xuICB0b3A6IDQ1JTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJveC1zaGFkb3c6IDBweCAzcHggNnB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZzogMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgaW9uLWxhYmVsIHtcbiAgbWFyZ2luLWJvdHRvbTogMnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiAuam9iX3RpdGxlIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nLXRvcDogMzBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgLmNvbXBhbXlfbmFtZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgLmFkZHJlc3Mge1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfZGl2IC5lYXN5X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfZGl2IC5lYXN5X2RpdiBpbWcge1xuICB3aWR0aDogMTVweDtcbiAgaGVpZ2h0OiAxNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiAuZWFzeV9kaXYgLmVhc3kge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nOiAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiAuZWFzeV9kaXYgLnRpbWUge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgcGFkZGluZzogMDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogZ3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5qb2JfZGl2IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDE2cHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuam9iX2RpdiAuam9iX2hlYWRlciB7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gIGZvbnQtc2l6ZTogMTZweCAhaW1wb3J0YW50O1xufVxuLm1haW5fY29udGVudF9kaXYgLmpvYl9kaXYgaW9uLWxhYmVsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLndoaXRlX2RpdiB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAud2hpdGVfZGl2IC5ibHVlX2xibCB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIHBhZGRpbmctdG9wOiAxNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIHBhZGRpbmc6IDE2cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmxpZ2h0X2xibCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6IGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgaW9uLXRvZ2dsZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDE2cHg7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLnByZW1pdW1fYnRuIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gIGhlaWdodDogNDBweDtcbiAgLS1iYWNrZ3JvdW5kOiAjNzg3MTBjO1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICB3aWR0aDogMTAwJTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiAuaW1hZ2Uge1xuICBoZWlnaHQ6IDQ1cHg7XG4gIHdpZHRoOiA0NXB4O1xuICBtYXgtd2lkdGg6IDQ1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiAuaGVhZF9sYmwge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiAuY29udGVudF9kaXYgLnNtYWxsX2xibCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYgLmZhbGxvd19idG4ge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIG1hcmdpbjogMDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/job-detail/job-detail.page.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/job-detail/job-detail.page.ts ***!
    \*****************************************************/

  /*! exports provided: JobDetailPage */

  /***/
  function srcAppPagesJobDetailJobDetailPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "JobDetailPage", function () {
      return JobDetailPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var JobDetailPage = /*#__PURE__*/function () {
      function JobDetailPage() {
        _classCallCheck(this, JobDetailPage);
      }

      _createClass(JobDetailPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return JobDetailPage;
    }();

    JobDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-job-detail',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./job-detail.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/job-detail/job-detail.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./job-detail.page.scss */
      "./src/app/pages/job-detail/job-detail.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], JobDetailPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-job-detail-job-detail-module-es5.js.map
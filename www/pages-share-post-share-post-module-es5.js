function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-share-post-share-post-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/share-post/share-post.page.html":
  /*!*********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/share-post/share-post.page.html ***!
    \*********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesSharePostSharePostPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title color=\"light\">Share Post</ion-title>\n    <ion-button slot=\"end\" expand=\"block\" fill=\"clear\" size=\"small\" (click)=\"post()\">\n      Post\n    </ion-button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n\n    <div class=\"flex_div\">\n      <div class=\"back_image\" [style.backgroundImage]=\"'url(assets/imgs/user2.jpg)'\"></div>\n      <div class=\"content_div\">\n        <ion-label class=\"username\">Jonh Doe</ion-label>\n        <ion-select value=\"brown\" okText=\"Okay\" cancelText=\"Dismiss\">\n          <ion-select-option value=\"brown\">Anyone</ion-select-option>\n          <ion-select-option value=\"blonde\">Connections only</ion-select-option>\n          <ion-select-option value=\"black\">Advance setings</ion-select-option>\n        </ion-select>\n      </div>\n    </div>\n\n    <div class=\"textarea_div\">\n      <ion-textarea autoGrow=\"true\" placeholder=\"What do you want to talk about? \"></ion-textarea>\n    </div>\n\n  </div>\n</ion-content>\n<ion-footer>\n  <div class=\"hashtag_div\">\n    <ion-label class=\"blue_lbl\"># Add Hashtag</ion-label>\n    <ion-label class=\"gray_lbl\">Help thi right people see your post</ion-label>\n  </div>\n\n  <div style=\"position: relative;\">\n    <ion-button size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"camera-outline\"></ion-icon>\n    </ion-button>\n    <ion-button size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"videocam-outline\"></ion-icon>\n    </ion-button>\n    <ion-button size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"image-outline\"></ion-icon>\n    </ion-button>\n    <ion-button (click)=\"openActionSheet()\" size=\"small\" fill=\"clear\">\n      <ion-icon slot=\"icon-only\" name=\"ellipsis-horizontal\"></ion-icon>\n    </ion-button>\n    <ion-button size=\"small\" fill=\"clear\" class=\"at_btn\">\n      <ion-icon slot=\"icon-only\" name=\"at-outline\"></ion-icon>\n    </ion-button>\n  </div>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/pages/share-post/share-post-routing.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/pages/share-post/share-post-routing.module.ts ***!
    \***************************************************************/

  /*! exports provided: SharePostPageRoutingModule */

  /***/
  function srcAppPagesSharePostSharePostRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharePostPageRoutingModule", function () {
      return SharePostPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _share_post_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./share-post.page */
    "./src/app/pages/share-post/share-post.page.ts");

    var routes = [{
      path: '',
      component: _share_post_page__WEBPACK_IMPORTED_MODULE_3__["SharePostPage"]
    }];

    var SharePostPageRoutingModule = function SharePostPageRoutingModule() {
      _classCallCheck(this, SharePostPageRoutingModule);
    };

    SharePostPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], SharePostPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/share-post/share-post.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/share-post/share-post.module.ts ***!
    \*******************************************************/

  /*! exports provided: SharePostPageModule */

  /***/
  function srcAppPagesSharePostSharePostModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharePostPageModule", function () {
      return SharePostPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _share_post_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./share-post-routing.module */
    "./src/app/pages/share-post/share-post-routing.module.ts");
    /* harmony import */


    var _share_post_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./share-post.page */
    "./src/app/pages/share-post/share-post.page.ts");

    var SharePostPageModule = function SharePostPageModule() {
      _classCallCheck(this, SharePostPageModule);
    };

    SharePostPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _share_post_routing_module__WEBPACK_IMPORTED_MODULE_5__["SharePostPageRoutingModule"]],
      declarations: [_share_post_page__WEBPACK_IMPORTED_MODULE_6__["SharePostPage"]]
    })], SharePostPageModule);
    /***/
  },

  /***/
  "./src/app/pages/share-post/share-post.page.scss":
  /*!*******************************************************!*\
    !*** ./src/app/pages/share-post/share-post.page.scss ***!
    \*******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesSharePostSharePostPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\nion-button {\n  color: white;\n  font-weight: 600px;\n  font-size: 16px;\n}\n\n.main_content_div {\n  width: 100%;\n  padding: 16px;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div .flex_div {\n  display: flex;\n  align-items: center;\n}\n\n.main_content_div .flex_div .back_image {\n  height: 50px;\n  width: 50px;\n  min-width: 50px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.main_content_div .flex_div .content_div {\n  margin-left: 10px;\n}\n\n.main_content_div .flex_div .content_div .username {\n  font-weight: 600;\n  margin-bottom: 3px;\n}\n\n.main_content_div .flex_div .content_div ion-select {\n  border: 1px solid lightgrey;\n  border-radius: 5px;\n  padding: 3px 10px;\n  font-size: 14px;\n}\n\n.main_content_div .textarea_div {\n  margin-top: 20px;\n}\n\n.main_content_div .textarea_div ion-textarea {\n  width: 100%;\n  height: 200px;\n}\n\nion-footer .hashtag_div {\n  display: flex;\n  justify-content: space-between;\n  font-size: 14px;\n  padding: 16px;\n  font-weight: 600;\n}\n\nion-footer .hashtag_div .blue_lbl {\n  color: var(--ion-color-main);\n}\n\nion-footer .hashtag_div .gray_lbl {\n  color: gray;\n}\n\nion-footer ion-icon {\n  color: var(--ion-color-main);\n  font-size: 20px;\n}\n\nion-footer ion-button {\n  margin: 0;\n}\n\nion-footer .at_btn {\n  position: absolute;\n  right: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2hhcmUtcG9zdC9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxzaGFyZS1wb3N0XFxzaGFyZS1wb3N0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvc2hhcmUtcG9zdC9zaGFyZS1wb3N0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1DQUFBO0FDQ0o7O0FEQ0E7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0FDRUo7O0FEQ0E7RUFDSSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDRUo7O0FEQ0E7RUFDSSxXQUFBO0VBQ0EsYUFBQTtBQ0VKOztBREFJO0VBQ0ksY0FBQTtBQ0VSOztBRENJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FDQ1I7O0FEQVE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQ0VaOztBREFRO0VBQ0ksaUJBQUE7QUNFWjs7QURBWTtFQUNJLGdCQUFBO0VBQ0Esa0JBQUE7QUNFaEI7O0FEQ1k7RUFDSSwyQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDQ2hCOztBRElJO0VBQ0ksZ0JBQUE7QUNGUjs7QURHUTtFQUNJLFdBQUE7RUFDQSxhQUFBO0FDRFo7O0FEUUk7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0FDTFI7O0FET1E7RUFDSSw0QkFBQTtBQ0xaOztBRE9RO0VBQ0ksV0FBQTtBQ0xaOztBRFFJO0VBQ0ksNEJBQUE7RUFDQSxlQUFBO0FDTlI7O0FEUUk7RUFDSSxTQUFBO0FDTlI7O0FEUUk7RUFDSSxrQkFBQTtFQUNBLFFBQUE7QUNOUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NoYXJlLXBvc3Qvc2hhcmUtcG9zdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQgOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG59XG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaHtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5cbmlvbi1idXR0b257XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDBweDtcbiAgICBmb250LXNpemU6IDE2cHg7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2e1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDE2cHg7XG5cbiAgICBpb24tbGFiZWwge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG5cbiAgICAuZmxleF9kaXZ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIC5iYWNrX2ltYWdle1xuICAgICAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICAgICAgICBtaW4td2lkdGg6IDUwcHg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgfVxuICAgICAgICAuY29udGVudF9kaXZ7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcblxuICAgICAgICAgICAgLnVzZXJuYW1le1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogM3B4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpb24tc2VsZWN0e1xuICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JleTtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogM3B4IDEwcHg7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLnRleHRhcmVhX2RpdntcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgICAgaW9uLXRleHRhcmVhe1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBoZWlnaHQ6IDIwMHB4O1xuICAgICAgICAgICAgLy8gYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICB9XG4gICAgfVxufVxuaW9uLWZvb3RlcntcblxuICAgIC5oYXNodGFnX2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG5cbiAgICAgICAgLmJsdWVfbGJse1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgfVxuICAgICAgICAuZ3JheV9sYmx7XG4gICAgICAgICAgICBjb2xvciA6IGdyYXk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgaW9uLWljb257XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICB9XG4gICAgaW9uLWJ1dHRvbntcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgIH1cbiAgICAuYXRfYnRue1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHJpZ2h0OiAwO1xuICAgIH1cbn0iLCJpb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuXG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCB7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5cbmlvbi1idXR0b24ge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtd2VpZ2h0OiA2MDBweDtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuXG4ubWFpbl9jb250ZW50X2RpdiB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAxNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5iYWNrX2ltYWdlIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogNTBweDtcbiAgbWluLXdpZHRoOiA1MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuY29udGVudF9kaXYge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuY29udGVudF9kaXYgLnVzZXJuYW1lIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbWFyZ2luLWJvdHRvbTogM3B4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiBpb24tc2VsZWN0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmV5O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmc6IDNweCAxMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudGV4dGFyZWFfZGl2IHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC50ZXh0YXJlYV9kaXYgaW9uLXRleHRhcmVhIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjAwcHg7XG59XG5cbmlvbi1mb290ZXIgLmhhc2h0YWdfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBmb250LXNpemU6IDE0cHg7XG4gIHBhZGRpbmc6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5pb24tZm9vdGVyIC5oYXNodGFnX2RpdiAuYmx1ZV9sYmwge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLWZvb3RlciAuaGFzaHRhZ19kaXYgLmdyYXlfbGJsIHtcbiAgY29sb3I6IGdyYXk7XG59XG5pb24tZm9vdGVyIGlvbi1pY29uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuaW9uLWZvb3RlciBpb24tYnV0dG9uIHtcbiAgbWFyZ2luOiAwO1xufVxuaW9uLWZvb3RlciAuYXRfYnRuIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/share-post/share-post.page.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/share-post/share-post.page.ts ***!
    \*****************************************************/

  /*! exports provided: SharePostPage */

  /***/
  function srcAppPagesSharePostSharePostPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharePostPage", function () {
      return SharePostPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! sweetalert2 */
    "./node_modules/sweetalert2/dist/sweetalert2.all.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var SharePostPage = /*#__PURE__*/function () {
      function SharePostPage(actionSheet) {
        _classCallCheck(this, SharePostPage);

        this.actionSheet = actionSheet;
      }

      _createClass(SharePostPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.openActionSheet();
        }
      }, {
        key: "openActionSheet",
        value: function openActionSheet() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var actionSheet;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.actionSheet.create({
                      mode: 'md',
                      buttons: [{
                        text: 'Add Photo',
                        icon: 'image-outline',
                        handler: function handler() {
                          console.log('Delete clicked');
                        }
                      }, {
                        text: 'Take a video',
                        icon: 'videocam-outline',
                        handler: function handler() {
                          console.log('Share clicked');
                        }
                      }, {
                        text: 'Celebrate a teammate',
                        icon: 'medal-outline',
                        handler: function handler() {
                          console.log('Play clicked');
                        }
                      }, {
                        text: 'Add a document',
                        icon: 'document-text-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Share that you are hiring',
                        icon: 'briefcase-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }, {
                        text: 'Find an expert',
                        icon: 'person-outline',
                        handler: function handler() {
                          console.log('Favorite clicked');
                        }
                      }]
                    });

                  case 2:
                    actionSheet = _context.sent;
                    _context.next = 5;
                    return actionSheet.present();

                  case 5:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "post",
        value: function post() {
          sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
            title: 'Success',
            text: 'post created successfully',
            icon: 'success',
            backdrop: false
          });
        }
      }]);

      return SharePostPage;
    }();

    SharePostPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"]
      }];
    };

    SharePostPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-share-post',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./share-post.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/share-post/share-post.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./share-post.page.scss */
      "./src/app/pages/share-post/share-post.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"]])], SharePostPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-share-post-share-post-module-es5.js.map
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-member-detail-member-detail-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/member-detail/member-detail.page.html":
  /*!***************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/member-detail/member-detail.page.html ***!
    \***************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesMemberDetailMemberDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-button (click)=\"BackButton()\" slot=\"start\">\n      <ion-icon  name=\"chevron-back-outline\"></ion-icon>\n    </ion-button>\n    <ion-title color=\"light\">{{data.name}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n\n\n\n\n\n<ion-content>\n  <div class=\"main_content_div\">\n    <div class=\"cover\" [style.backgroundImage]=\"'url('+data.cover+')'\">\n      <ion-card id=\"content\" >\n        <ion-avatar id=\"profile-info\">\n          <img id=\"profile-image\" src=\"{{data.avatar}}\">\n        </ion-avatar>\n      <ion-label style=\"    text-align: center;\n      font-size: 18px;\n      font-weight: bold;\n      padding-top: 70px;\n      padding-bottom: 10px;\">{{data.name}} </ion-label>\n              <ion-label style=\"text-align: center;\n             font-size: 16px;\n    padding-bottom: 10px;\n              \">@{{data.username}}</ion-label>\n            <ion-grid fixed><ion-row style=\" margin-top: 5px;\">\n            <ion-col size=\"3\">\n              <ion-label class=\"icon-label\" style=\"background-color: #328af5 !important; color:white\"> <ion-icon  class=\"icon-style\" name=\"person-remove-outline\"></ion-icon>\n                \n              </ion-label>\n              <ion-label  class=\"text-label\">Friend Requset </ion-label>\n            </ion-col>\n            <ion-col size=\"3\">\n              <ion-label class=\"icon-label\"> <ion-icon  class=\"icon-style\" name=\"receipt-outline\"></ion-icon></ion-label>\n              <ion-label class=\"text-label\" >Follow</ion-label>\n            </ion-col>\n            <ion-col size=\"3\">\n              <ion-label (click)=\"editProfile()\" class=\"icon-label\"> <ion-icon  class=\"icon-style\" name=\"person-outline\"></ion-icon></ion-label>\n              <ion-label  class=\"text-label\">Profile </ion-label>\n            </ion-col>\n            <ion-col size=\"3\">\n              <ion-label (click)=\"detailsProfile()\" class=\"icon-label\"> <ion-icon  class=\"icon-style\" name=\"reader-outline\"></ion-icon></ion-label>\n              <ion-label  class=\"text-label\" >Job Profile </ion-label>\n            </ion-col>\n        </ion-row>\n       \n\n        \n        </ion-grid>\n      </ion-card>\n      <!-- <div class=\"user_image\">\n        <img src=\"assets/imgs/company/initappz.png\" alt=\"\">\n       \n      </div>\n      <div class=\"data_div\">\n        <ion-label class=\"group_title\">Web Developers</ion-label>\n        <ion-label class=\"group_type\">Public / Group . 10 Memers</ion-label>\n        <ion-label class=\"description\">Lorem ipsum\n          dolor sit amet\n          \n          consectetur adipiscing elit. Duis ut urna neque.\n          \n          </ion-label>\n        \n         <div class=\"admin\">\n          <img src=\"assets/imgs/clock.png\">\n          <ion-label class=\"admin_name\">John</ion-label>\n        </div> \n        </div> -->\n        \n    </div>\n   \n    <div style=\"display: none;\" class=\"personal_div\">\n     \n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\" (click)=\"editProfile()\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"person-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Profile </p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\" (click)=\"detailsProfile()\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"reader-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Job Profile Details</p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"pulse-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Timeline </p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"person-add-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Connections </p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"people-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Groups </p>\n         \n        </ion-label>\n  \n      </div> <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"images-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Photos </p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"chatbox-ellipses-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Forums </p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"star-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Points </p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"stats-chart-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Achivements </p>\n         \n        </ion-label>\n      </div>\n      <div style=\"margin-top: 10px;\" class=\"white_div\" style=\"margin-bottom: 0px;\">\n        <ion-label class=\"blue_lbl\"><ion-icon class=\"left-icon\" name=\"podium-outline\"></ion-icon>\n          <ion-icon  class=\"right-icon\"  name=\"chevron-forward-outline\"></ion-icon>\n          <p class=\"lable-icon\"> Ranks </p>\n         \n        </ion-label>\n      </div>\n     \n\n      <!-- <ion-grid fixed>\n        <ion-row>\n          <ion-col size=\"6\">\n            <ion-button expand=\"block\" class=\"save\" size=\"small\" fill=\"outline\">\n              SAVE\n            </ion-button>\n          </ion-col>\n          <ion-col size=\"6\">\n            <ion-button size=\"small\" class=\"apply\" expand=\"block\">\n              APPLY\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid> -->\n    </div>\n\n    <!-- <div class=\"job_div\">\n      <ion-label class=\"job_header\">Job Description</ion-label>\n      <ion-label>Web Designer - No of opening : 6</ion-label>\n      <ion-label>Required Experience : 1 - 3 Years</ion-label>\n      <ion-label>Job brief We are looking for a talented Web..</ion-label>\n    </div>\n\n    <div class=\"white_div\">\n      <ion-label class=\"blue_lbl\" style=\"text-align: center;\">SEE MORE</ion-label>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>Set alert for similar jobs</ion-label>\n      <ion-label class=\"light_lbl\">Web Designer, Bhavnagar, India</ion-label>\n      <ion-toggle></ion-toggle>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>See company insights for Initappz Shop with Premiun</ion-label>\n      <ion-button class=\"premium_btn\" expand=\"block\">\n        TRY PREMIUM FOR FREE\n      </ion-button>\n    </div>\n\n    <div class=\"desc_div\">\n      <ion-label>Keep up with updates and jobs</ion-label>\n      <div class=\"flex_div\">\n        <img src=\"assets/imgs/user.jpg\" class=\"image\">\n        <div class=\"content_div\">\n          <ion-label class=\"head_lbl\">Initappz Shop</ion-label>\n          <ion-label class=\"small_lbl\">E-learning 22,300 followers</ion-label>\n        </div>\n        <div>\n          <ion-button class=\"fallow_btn\" expand=\"block\" fill=\"clear\" size=\"small\">\n            + Follow\n          </ion-button>\n        </div>\n      </div>\n\n    </div> -->\n  </div>\n</ion-content>\n<!-- <ion-footer>\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button expand=\"block\" class=\"save\" size=\"small\" fill=\"outline\">\n          SAVE\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-button size=\"small\" class=\"apply\" expand=\"block\">\n          APPLY\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer> -->";
    /***/
  },

  /***/
  "./src/app/pages/member-detail/member-detail-routing.module.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/pages/member-detail/member-detail-routing.module.ts ***!
    \*********************************************************************/

  /*! exports provided: MemberDetailPageRoutingModule */

  /***/
  function srcAppPagesMemberDetailMemberDetailRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MemberDetailPageRoutingModule", function () {
      return MemberDetailPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _member_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./member-detail.page */
    "./src/app/pages/member-detail/member-detail.page.ts");

    var routes = [{
      path: '',
      component: _member_detail_page__WEBPACK_IMPORTED_MODULE_3__["MemberDetailPage"]
    }];

    var MemberDetailPageRoutingModule = function MemberDetailPageRoutingModule() {
      _classCallCheck(this, MemberDetailPageRoutingModule);
    };

    MemberDetailPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], MemberDetailPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/member-detail/member-detail.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/pages/member-detail/member-detail.module.ts ***!
    \*************************************************************/

  /*! exports provided: MemberDetailPageModule */

  /***/
  function srcAppPagesMemberDetailMemberDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MemberDetailPageModule", function () {
      return MemberDetailPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _member_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./member-detail-routing.module */
    "./src/app/pages/member-detail/member-detail-routing.module.ts");
    /* harmony import */


    var _member_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./member-detail.page */
    "./src/app/pages/member-detail/member-detail.page.ts");

    var MemberDetailPageModule = function MemberDetailPageModule() {
      _classCallCheck(this, MemberDetailPageModule);
    };

    MemberDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _member_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["MemberDetailPageRoutingModule"]],
      declarations: [_member_detail_page__WEBPACK_IMPORTED_MODULE_6__["MemberDetailPage"]]
    })], MemberDetailPageModule);
    /***/
  },

  /***/
  "./src/app/pages/member-detail/member-detail.page.scss":
  /*!*************************************************************!*\
    !*** ./src/app/pages/member-detail/member-detail.page.scss ***!
    \*************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesMemberDetailMemberDetailPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-main);\n}\nion-toolbar ion-input {\n  border-bottom: 1px solid white;\n  --color: white;\n  --placeholder-color: white;\n  --padding-start: 8px;\n}\nion-toolbar ion-icon {\n  color: white;\n  font-size: 24px;\n}\n#content {\n  position: relative;\n  background-color: #ffffff;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n  /* padding-top:200px; */\n  border-radius: 15px;\n  height: 225px;\n  overflow: unset;\n  display: block;\n  top: 135px;\n}\n#profile-info {\n  position: absolute;\n  top: -95px;\n  width: 100%;\n  z-index: 2;\n  text-align: center;\n}\n.icon-style {\n  width: 100%;\n  height: 53%;\n  margin-top: 12px;\n}\n.text-label {\n  margin-top: 55px;\n  text-align: center;\n  font-size: 12px;\n  font-weight: bold;\n}\n.icon-label {\n  border-radius: 50%;\n  background: #f5f5f5;\n  left: 25%;\n  width: 50px;\n  box-shadow: 0px 3px 7px #8f8989;\n  height: 50px;\n  position: absolute;\n  top: 0;\n  cursor: pointer;\n  z-index: 9;\n  box-sizing: border-box;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n#profile-image {\n  display: block;\n  border-radius: 120px;\n  border: 3px solid #fff;\n  width: 128px;\n  background: white;\n  height: 128px;\n  margin: 30px auto 0;\n  box-shadow: 0px -1px 10px rgba(0, 0, 0, 0.4);\n}\n.save {\n  --border-color: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 16px;\n  --border-radius: 3px;\n  color: var(--ion-color-main);\n}\n.apply {\n  --background: var(--ion-color-main);\n  font-weight: 600;\n  font-size: 16px;\n  --border-radius: 3px;\n}\nion-content {\n  --background: #F5F5F5;\n}\n.white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n}\n.white_div .left-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  left: 16px;\n  font-size: 26px;\n}\n.white_div .lable-icon {\n  padding-left: 22px;\n  padding-top: 5px;\n  color: var(--ion-color-main);\n  font-size: 17px;\n  cursor: pointer;\n}\n.white_div .right-icon {\n  color: var(--ion-color-main);\n  position: absolute;\n  right: 16px;\n  font-size: 20px;\n}\n.white_div ion-button {\n  color: var(--ion-color-main);\n  font-size: 600;\n}\n.main_content_div {\n  width: 100%;\n}\n.main_content_div .no_border {\n  border-bottom: none !important;\n}\n.main_content_div ion-label {\n  display: block;\n}\n.main_content_div .cover {\n  width: 100%;\n  height: 200px;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n  position: relative;\n}\n.main_content_div .cover .user_image {\n  width: 70px;\n  height: 70px;\n  border-radius: 15px;\n  position: absolute;\n  left: 40%;\n  top: 25%;\n  background: white;\n  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.2);\n}\n.main_content_div .cover .data_div {\n  padding: 16px;\n  padding-top: 114px;\n  padding-right: 10%;\n}\n.main_content_div .cover .data_div ion-label {\n  margin-bottom: 2px;\n}\n.main_content_div .cover .data_div .group_title {\n  text-align: center;\n  font-size: 20px;\n  padding-top: 30px;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n  font-weight: bolder;\n}\n.main_content_div .cover .data_div .group_type {\n  font-size: 14px;\n  text-align: center;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n}\n.main_content_div .cover .data_div .description {\n  font-size: 14px;\n  text-align: center;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n}\n.main_content_div .cover .data_div .admin {\n  display: flex;\n  padding-bottom: 5px;\n  text-align: center;\n  color: white;\n  text-shadow: 2px 2px 8px black;\n}\n.main_content_div .cover .data_div .admin img {\n  width: 15px;\n  height: 15px;\n  text-align: center;\n}\n.main_content_div .cover .data_div .admin .admin_name {\n  text-align: center;\n  margin-left: 10px;\n  padding: 0;\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .personal_div {\n  background: white;\n  padding: 16px;\n  margin-top: 145px;\n}\n.main_content_div .personal_div ion-label {\n  margin-bottom: 2px;\n}\n.main_content_div .personal_div .job_title {\n  font-size: 20px;\n  padding-top: 30px;\n}\n.main_content_div .personal_div .compamy_name {\n  font-size: 14px;\n}\n.main_content_div .personal_div .address {\n  font-size: 14px;\n}\n.main_content_div .personal_div .easy_div {\n  display: flex;\n  padding-bottom: 5px;\n}\n.main_content_div .personal_div .easy_div img {\n  width: 15px;\n  height: 15px;\n}\n.main_content_div .personal_div .easy_div .easy {\n  font-size: 12px;\n  margin-left: 10px;\n  padding: 0;\n}\n.main_content_div .personal_div .easy_div .time {\n  margin-left: 10px;\n  padding: 0;\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .job_div {\n  background: white;\n  padding: 16px;\n  margin-top: 10px;\n}\n.main_content_div .job_div .job_header {\n  padding-bottom: 5px;\n  font-size: 16px !important;\n}\n.main_content_div .job_div ion-label {\n  font-size: 14px;\n  padding-bottom: 5px;\n}\n.main_content_div .white_div {\n  background: white;\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 10px;\n  border-top: 1px solid lightgray;\n}\n.main_content_div .white_div .blue_lbl {\n  color: var(--ion-color-main);\n  padding-top: 15px;\n  padding-bottom: 15px;\n  font-weight: 600;\n}\n.main_content_div .desc_div {\n  margin-top: 10px;\n  padding: 16px;\n  background: white;\n  position: relative;\n}\n.main_content_div .desc_div .light_lbl {\n  font-size: 14px;\n  color: gray;\n}\n.main_content_div .desc_div ion-toggle {\n  position: absolute;\n  right: 16px;\n  top: 50%;\n  transform: translateY(-50%);\n}\n.main_content_div .desc_div .premium_btn {\n  margin-top: 10px;\n  --border-radius: 3px;\n  height: 40px;\n  --background: #78710c;\n}\n.main_content_div .desc_div .flex_div {\n  display: flex;\n  align-items: center;\n  padding-top: 10px;\n  width: 100%;\n  justify-content: space-between;\n}\n.main_content_div .desc_div .flex_div .image {\n  height: 45px;\n  width: 45px;\n  max-width: 45px;\n  border-radius: 3px;\n}\n.main_content_div .desc_div .flex_div .content_div {\n  margin-left: 10px;\n}\n.main_content_div .desc_div .flex_div .content_div .head_lbl {\n  font-weight: 600;\n}\n.main_content_div .desc_div .flex_div .content_div .small_lbl {\n  font-size: 14px;\n}\n.main_content_div .desc_div .flex_div .fallow_btn {\n  font-size: 14px;\n  font-weight: 600;\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVtYmVyLWRldGFpbC9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxtZW1iZXItZGV0YWlsXFxtZW1iZXItZGV0YWlsLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbWVtYmVyLWRldGFpbC9tZW1iZXItZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1DQUFBO0FDQ0o7QURBSTtFQUNJLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0VBQ0Esb0JBQUE7QUNFUjtBREFJO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUNFUjtBRENBO0VBQ0ksa0JBQUE7RUFFQSx5QkFBQTtFQUNBLDRDQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNFLGFBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7QUNDTjtBREdFO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUVBLFVBQUE7RUFDQSxrQkFBQTtBQ0RKO0FER0U7RUFDRSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDQUo7QURFQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNDQTtBREVFO0VBQ0Usa0JBQUE7RUFFRCxtQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsK0JBQUE7RUFDQSxZQUFBO0VBQ0Msa0JBQUE7RUFDQSxNQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7RUFDQSxzQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDQUo7QURFRTtFQUNFLGNBQUE7RUFDQSxvQkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNENBQUE7QUNDSjtBREVBO0VBQ0kscUNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtFQUNBLDRCQUFBO0FDQ0o7QURDQTtFQUNJLG1DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7QUNFSjtBREFBO0VBQ0kscUJBQUE7QUNHSjtBRERBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNJSjtBRFVJO0VBQ0ksNEJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0FDUlI7QURVSTtFQUNNLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSw0QkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FDUlY7QURVSTtFQUNJLDRCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQ1JSO0FEV0k7RUFDSSw0QkFBQTtFQUNBLGNBQUE7QUNUUjtBRFlBO0VBQ0ksV0FBQTtBQ1RKO0FEV0k7RUFDSSw4QkFBQTtBQ1RSO0FEWUk7RUFDSSxjQUFBO0FDVlI7QURhSTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7QUNYUjtBRFlRO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFJQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSxpQkFBQTtFQUNBLDBDQUFBO0FDYlo7QURlUTtFQUNJLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDYlo7QURlWTtFQUNJLGtCQUFBO0FDYmhCO0FEZ0JZO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtBQ2RoQjtBRGtCWTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtBQ2hCaEI7QURrQlk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7QUNoQmhCO0FEa0JZO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7QUNoQmhCO0FEaUJnQjtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUNmcEI7QURpQmdCO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2ZwQjtBRHVCSTtFQUNJLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0FDckJSO0FEc0JRO0VBQ0ksa0JBQUE7QUNwQlo7QUR1QlE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7QUNyQlo7QUR1QlE7RUFDSSxlQUFBO0FDckJaO0FEdUJRO0VBQ0ksZUFBQTtBQ3JCWjtBRHVCUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtBQ3JCWjtBRHNCWTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDcEJoQjtBRHNCWTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7QUNwQmhCO0FEc0JZO0VBQ0ksaUJBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNwQmhCO0FEeUJJO0VBQ0ksaUJBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUN2QlI7QUQwQlE7RUFDSSxtQkFBQTtFQUNBLDBCQUFBO0FDeEJaO0FEMkJRO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0FDekJaO0FENEJJO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSwrQkFBQTtBQzFCUjtBRDJCUTtFQUNJLDRCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0FDekJaO0FENkJJO0VBQ0ksZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQzNCUjtBRDZCUTtFQUNJLGVBQUE7RUFDQSxXQUFBO0FDM0JaO0FEOEJRO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtFQUNBLDJCQUFBO0FDNUJaO0FEK0JRO0VBQ0ksZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtBQzdCWjtBRGdDUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0FDOUJaO0FEK0JZO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUM3QmhCO0FEZ0NZO0VBQ0ksaUJBQUE7QUM5QmhCO0FEK0JnQjtFQUNJLGdCQUFBO0FDN0JwQjtBRCtCZ0I7RUFDSSxlQUFBO0FDN0JwQjtBRGlDWTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFNBQUE7QUMvQmhCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbWVtYmVyLWRldGFpbC9tZW1iZXItZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZCA6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICBpb24taW5wdXR7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcbiAgICAgICAgLS1jb2xvcjogd2hpdGU7XG4gICAgICAgIC0tcGxhY2Vob2xkZXItY29sb3IgOiB3aGl0ZTtcbiAgICAgICAgLS1wYWRkaW5nLXN0YXJ0IDogOHB4O1xuICAgIH1cbiAgICBpb24taWNvbntcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgfVxufVxuI2NvbnRlbnQge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAvLyBtYXJnaW4tdG9wOiBhdXRvO1xuICAgIGJhY2tncm91bmQtY29sb3I6I2ZmZmZmZjtcbiAgICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgICAvKiBwYWRkaW5nLXRvcDoyMDBweDsgKi9cbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgICAgaGVpZ2h0OiAyMjVweDtcbiAgICAgIG92ZXJmbG93OiB1bnNldDtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgdG9wOiAxMzVweDtcbiAgICAgLy8gbWFyZ2luLXRvcDogMTAwcHg7XG4gIH1cbiAgXG4gICNwcm9maWxlLWluZm8ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IC05NXB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIFxuICAgIHotaW5kZXg6IDI7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG4gIC5pY29uLXN0eWxle1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNTMlO1xuICAgIG1hcmdpbi10b3A6IDEycHg7XG59XG4udGV4dC1sYWJlbCB7XG5tYXJnaW4tdG9wOiA1NXB4O1xudGV4dC1hbGlnbjogY2VudGVyO1xuZm9udC1zaXplOiAxMnB4O1xuZm9udC13ZWlnaHQ6IGJvbGQ7XG5cbn1cbiAgLmljb24tbGFiZWwge1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIHJlZDtcbiAgIGJhY2tncm91bmQ6ICNmNWY1ZjU7XG4gICBsZWZ0OiAyNSU7XG4gICB3aWR0aDo1MHB4O1xuICAgYm94LXNoYWRvdzogMHB4IDNweCA3cHggIzhmODk4OTtcbiAgIGhlaWdodDogNTBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB6LWluZGV4OiA5O1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB9XG4gICNwcm9maWxlLWltYWdlIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBib3JkZXItcmFkaXVzOiAxMjBweDtcbiAgICBib3JkZXI6IDNweCBzb2xpZCAjZmZmO1xuICAgIHdpZHRoOiAxMjhweDtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICBoZWlnaHQ6IDEyOHB4O1xuICAgIG1hcmdpbjogMzBweCBhdXRvIDA7XG4gICAgYm94LXNoYWRvdzogMHB4IC0xcHggMTBweCByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gICAvLyBib3gtc2hhZG93OiAwcHggMHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuNyk7XG4gIH1cbi5zYXZle1xuICAgIC0tYm9yZGVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cbi5hcHBseXtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbn1cbmlvbi1jb250ZW50e1xuICAgIC0tYmFja2dyb3VuZDogI0Y1RjVGNTtcbn1cbi53aGl0ZV9kaXZ7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAvLyBpb24tYnV0dG9ue1xuICAgIC8vICAgICAtLWJvcmRlci1yYWRpdXM6IDBweDtcbiAgICAvLyAgICAgaW9uLWxhYmVsIHtcbiAgICAvLyAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgLy8gICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAvLyAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAvLyAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgLy8gICAgIH0gICBcbiAgICAvLyAgICAgaW9uLWljb257XG4gICAgLy8gICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIC8vICAgICB9XG4gICAgLy8gfVxuXG4gICAgLmxlZnQtaWNvbntcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBsZWZ0OiAxNnB4O1xuICAgICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgfVxuICAgIC5sYWJsZS1pY29ue1xuICAgICAgICAgIHBhZGRpbmctbGVmdDogMjJweDtcbiAgICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xuICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgZm9udC1zaXplOiAxN3B4O1xuICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB9XG4gICAgLnJpZ2h0LWljb257XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgcmlnaHQ6IDE2cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICB9XG5cbiAgICBpb24tYnV0dG9ue1xuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICBmb250LXNpemU6IDYwMDtcbiAgICB9XG59XG4ubWFpbl9jb250ZW50X2RpdntcbiAgICB3aWR0aDogMTAwJTtcblxuICAgIC5ub19ib3JkZXJ7XG4gICAgICAgIGJvcmRlci1ib3R0b206IG5vbmUgIWltcG9ydGFudDtcbiAgICB9XG5cbiAgICBpb24tbGFiZWwge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG5cbiAgICAuY292ZXJ7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDIwMHB4O1xuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgLnVzZXJfaW1hZ2V7XG4gICAgICAgICAgICB3aWR0aDogNzBweDtcbiAgICAgICAgICAgIGhlaWdodDogNzBweDtcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIGxlZnQ6IDQwJTtcbiAgICAgICAgICAgIHRvcDogMjUlO1xuICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggM3B4IDZweCByZ2JhKDAsMCwwLDAuMik7XG4gICAgICAgIH1cbiAgICAgICAgLmRhdGFfZGl2e1xuICAgICAgICAgICAgcGFkZGluZzogMTZweDtcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMTRweDtcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDEwJTtcbiAgICBcbiAgICAgICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMnB4O1xuICAgICAgICAgICAgfVxuICAgIFxuICAgICAgICAgICAgLmdyb3VwX3RpdGxle1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICAgICAgcGFkZGluZy10b3A6IDMwcHg7XG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgICAgIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICAuZ3JvdXBfdHlwZXtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgICAgICB0ZXh0LXNoYWRvdzogMnB4IDJweCA4cHggYmxhY2s7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuZGVzY3JpcHRpb257XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgICAgICAgICAgdGV4dC1zaGFkb3c6IDJweCAycHggOHB4IGJsYWNrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmFkbWlue1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgICAgIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbiAgICAgICAgICAgICAgICBpbWd7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxNXB4O1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDE1cHg7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLmFkbWluX25hbWV7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgXG4gICAgfVxuXG4gICAgICAgXG4gICAgLnBlcnNvbmFsX2RpdntcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDE0NXB4O1xuICAgICAgICBpb24tbGFiZWwge1xuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMnB4O1xuICAgICAgICB9XG5cbiAgICAgICAgLmpvYl90aXRsZXtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAzMHB4O1xuICAgICAgICB9XG4gICAgICAgIC5jb21wYW15X25hbWV7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIH1cbiAgICAgICAgLmFkZHJlc3N7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIH1cbiAgICAgICAgLmVhc3lfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgICAgICAgICBpbWd7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDE1cHg7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmVhc3l7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAudGltZXtcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5qb2JfZGl2e1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgcGFkZGluZzogMTZweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcblxuXG4gICAgICAgIC5qb2JfaGVhZGVye1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweCAhaW1wb3J0YW50O1xuICAgICAgICB9XG5cbiAgICAgICAgaW9uLWxhYmVsIHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgICAgIH1cbiAgICB9XG4gICAgLndoaXRlX2RpdntcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMTZweDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgLmJsdWVfbGJse1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxNXB4O1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmRlc2NfZGl2e1xuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICAgICAgIC5saWdodF9sYmx7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlvbi10b2dnbGV7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICByaWdodDogMTZweDtcbiAgICAgICAgICAgIHRvcDogNTAlO1xuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgICAgICB9XG5cbiAgICAgICAgLnByZW1pdW1fYnRue1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiAjNzg3MTBjO1xuICAgICAgICB9XG5cbiAgICAgICAgLmZsZXhfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICAgICAgLmltYWdle1xuICAgICAgICAgICAgICAgIGhlaWdodDogNDVweDtcbiAgICAgICAgICAgICAgICB3aWR0aDogNDVweDtcbiAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDQ1cHg7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAuY29udGVudF9kaXZ7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgLmhlYWRfbGJse1xuICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAuc21hbGxfbGJse1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAuZmFsbG93X2J0bntcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cbmlvbi10b29sYmFyIGlvbi1pbnB1dCB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcbiAgLS1jb2xvcjogd2hpdGU7XG4gIC0tcGxhY2Vob2xkZXItY29sb3I6IHdoaXRlO1xuICAtLXBhZGRpbmctc3RhcnQ6IDhweDtcbn1cbmlvbi10b29sYmFyIGlvbi1pY29uIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDI0cHg7XG59XG5cbiNjb250ZW50IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgLyogcGFkZGluZy10b3A6MjAwcHg7ICovXG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGhlaWdodDogMjI1cHg7XG4gIG92ZXJmbG93OiB1bnNldDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRvcDogMTM1cHg7XG59XG5cbiNwcm9maWxlLWluZm8ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogLTk1cHg7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5pY29uLXN0eWxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogNTMlO1xuICBtYXJnaW4tdG9wOiAxMnB4O1xufVxuXG4udGV4dC1sYWJlbCB7XG4gIG1hcmdpbi10b3A6IDU1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmljb24tbGFiZWwge1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQ6ICNmNWY1ZjU7XG4gIGxlZnQ6IDI1JTtcbiAgd2lkdGg6IDUwcHg7XG4gIGJveC1zaGFkb3c6IDBweCAzcHggN3B4ICM4Zjg5ODk7XG4gIGhlaWdodDogNTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgei1pbmRleDogOTtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbiNwcm9maWxlLWltYWdlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJvcmRlci1yYWRpdXM6IDEyMHB4O1xuICBib3JkZXI6IDNweCBzb2xpZCAjZmZmO1xuICB3aWR0aDogMTI4cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEyOHB4O1xuICBtYXJnaW46IDMwcHggYXV0byAwO1xuICBib3gtc2hhZG93OiAwcHggLTFweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbn1cblxuLnNhdmUge1xuICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDE2cHg7XG4gIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuXG4uYXBwbHkge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxNnB4O1xuICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6ICNGNUY1RjU7XG59XG5cbi53aGl0ZV9kaXYge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuLndoaXRlX2RpdiAubGVmdC1pY29uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAxNnB4O1xuICBmb250LXNpemU6IDI2cHg7XG59XG4ud2hpdGVfZGl2IC5sYWJsZS1pY29uIHtcbiAgcGFkZGluZy1sZWZ0OiAyMnB4O1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXNpemU6IDE3cHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi53aGl0ZV9kaXYgLnJpZ2h0LWljb24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxNnB4O1xuICBmb250LXNpemU6IDIwcHg7XG59XG4ud2hpdGVfZGl2IGlvbi1idXR0b24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICBmb250LXNpemU6IDYwMDtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5ub19ib3JkZXIge1xuICBib3JkZXItYm90dG9tOiBub25lICFpbXBvcnRhbnQ7XG59XG4ubWFpbl9jb250ZW50X2RpdiBpb24tbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDIwMHB4O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAudXNlcl9pbWFnZSB7XG4gIHdpZHRoOiA3MHB4O1xuICBoZWlnaHQ6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNDAlO1xuICB0b3A6IDI1JTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJveC1zaGFkb3c6IDBweCAzcHggNnB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYge1xuICBwYWRkaW5nOiAxNnB4O1xuICBwYWRkaW5nLXRvcDogMTE0cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwJTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgaW9uLWxhYmVsIHtcbiAgbWFyZ2luLWJvdHRvbTogMnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvdmVyIC5kYXRhX2RpdiAuZ3JvdXBfdGl0bGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgcGFkZGluZy10b3A6IDMwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggOHB4IGJsYWNrO1xuICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvdmVyIC5kYXRhX2RpdiAuZ3JvdXBfdHlwZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtc2hhZG93OiAycHggMnB4IDhweCBibGFjaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jb3ZlciAuZGF0YV9kaXYgLmRlc2NyaXB0aW9uIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggOHB4IGJsYWNrO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvdmVyIC5kYXRhX2RpdiAuYWRtaW4ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggOHB4IGJsYWNrO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvdmVyIC5kYXRhX2RpdiAuYWRtaW4gaW1nIHtcbiAgd2lkdGg6IDE1cHg7XG4gIGhlaWdodDogMTVweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNvdmVyIC5kYXRhX2RpdiAuYWRtaW4gLmFkbWluX25hbWUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nOiAwO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOiBncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nOiAxNnB4O1xuICBtYXJnaW4tdG9wOiAxNDVweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgaW9uLWxhYmVsIHtcbiAgbWFyZ2luLWJvdHRvbTogMnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiAuam9iX3RpdGxlIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nLXRvcDogMzBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgLmNvbXBhbXlfbmFtZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5wZXJzb25hbF9kaXYgLmFkZHJlc3Mge1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfZGl2IC5lYXN5X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAucGVyc29uYWxfZGl2IC5lYXN5X2RpdiBpbWcge1xuICB3aWR0aDogMTVweDtcbiAgaGVpZ2h0OiAxNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiAuZWFzeV9kaXYgLmVhc3kge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nOiAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLnBlcnNvbmFsX2RpdiAuZWFzeV9kaXYgLnRpbWUge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgcGFkZGluZzogMDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogZ3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5qb2JfZGl2IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDE2cHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuam9iX2RpdiAuam9iX2hlYWRlciB7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gIGZvbnQtc2l6ZTogMTZweCAhaW1wb3J0YW50O1xufVxuLm1haW5fY29udGVudF9kaXYgLmpvYl9kaXYgaW9uLWxhYmVsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLndoaXRlX2RpdiB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAud2hpdGVfZGl2IC5ibHVlX2xibCB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIHBhZGRpbmctdG9wOiAxNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIHBhZGRpbmc6IDE2cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmxpZ2h0X2xibCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6IGdyYXk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgaW9uLXRvZ2dsZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDE2cHg7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLnByZW1pdW1fYnRuIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gIGhlaWdodDogNDBweDtcbiAgLS1iYWNrZ3JvdW5kOiAjNzg3MTBjO1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICB3aWR0aDogMTAwJTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiAuaW1hZ2Uge1xuICBoZWlnaHQ6IDQ1cHg7XG4gIHdpZHRoOiA0NXB4O1xuICBtYXgtd2lkdGg6IDQ1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYgLmNvbnRlbnRfZGl2IHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZGVzY19kaXYgLmZsZXhfZGl2IC5jb250ZW50X2RpdiAuaGVhZF9sYmwge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLm1haW5fY29udGVudF9kaXYgLmRlc2NfZGl2IC5mbGV4X2RpdiAuY29udGVudF9kaXYgLnNtYWxsX2xibCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5kZXNjX2RpdiAuZmxleF9kaXYgLmZhbGxvd19idG4ge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIG1hcmdpbjogMDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/member-detail/member-detail.page.ts":
  /*!***********************************************************!*\
    !*** ./src/app/pages/member-detail/member-detail.page.ts ***!
    \***********************************************************/

  /*! exports provided: MemberDetailPage */

  /***/
  function srcAppPagesMemberDetailMemberDetailPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MemberDetailPage", function () {
      return MemberDetailPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var MemberDetailPage = /*#__PURE__*/function () {
      function MemberDetailPage(api, storage, location, router) {
        var _this = this;

        _classCallCheck(this, MemberDetailPage);

        this.api = api;
        this.storage = storage;
        this.location = location;
        this.router = router;
        this.data = {};
        this.user = this.api.getCurrentUser();
        this.user.subscribe(function (user) {
          if (user) {
            // location.href = "/home-new";
            _this.storage.get('USER_INFO').then(function (result) {
              if (result != null) {
                console.log(result);
                _this.data.id = result.id;
                _this.data.username = result.nickname;
                if (typeof result.nickname === "undefined") _this.data.username = result.user_login;
                _this.data.cover = result.cover_url;
                _this.data.name = result.first_name + " " + result.last_name;
                _this.data.description = result.description;
                _this.data.avatar = result.avatar_urls[24];
                if (typeof result.avatar_urls[24] === "undefined") _this.data.avatar = result.avatar_urls['full'];
                _this.data.email = result.email;
                console.log(result.first_name);

                if (typeof result.first_name === "undefined") {
                  console.log(user);
                  _this.data.email = user.user_email;
                  _this.data.nickname = user.user_nicename;
                  _this.data.name = user.user_display_name;
                }
              }
            })["catch"](function (e) {
              console.log('error: ' + e); // Handle errors here
            });
          }
        });
      }

      _createClass(MemberDetailPage, [{
        key: "BackButton",
        value: function BackButton() {
          this.location.back();
        }
      }, {
        key: "editProfile",
        value: function editProfile() {
          this.router.navigate(['/member-profile']);
        }
      }, {
        key: "detailsProfile",
        value: function detailsProfile() {
          this.router.navigate(['/profile-detail']);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return MemberDetailPage;
    }();

    MemberDetailPage.ctorParameters = function () {
      return [{
        type: _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]
      }, {
        type: _angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }];
    };

    MemberDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-member-detail',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./member-detail.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/member-detail/member-detail.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./member-detail.page.scss */
      "./src/app/pages/member-detail/member-detail.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])], MemberDetailPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-member-detail-member-detail-module-es5.js.map
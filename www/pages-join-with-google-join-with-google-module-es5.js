function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-join-with-google-join-with-google-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/join-with-google/join-with-google.page.html":
  /*!*********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/join-with-google/join-with-google.page.html ***!
    \*********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesJoinWithGoogleJoinWithGooglePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content>\n  <div class=\"main_content_div\">\n    <div class=\"upper\">\n      <ion-buttons slot=\"start\" class=\"close_icn\">\n        <ion-button (click)=\"modalClose()\" color=\"light\" style=\"font-size: 30px;\">\n          <ion-icon name=\"close\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n\n      <!-- <ion-icon name=\"close\" class=\"close_icn\" (click)=\"modalClose()\"></ion-icon> -->\n      \n      <div class=\"round_div\">\n        <img src=\"assets/imgs/google.png\" class=\"google_img\">\n      </div>\n      <ion-label class=\"head_lbl\">Get set up quickly with <br> Google. We'll receive : </ion-label>\n\n      <div class=\"profile_div\">\n        <div class=\"email\">\n          <img src=\"assets/imgs/email.png\" class=\"img_size\">Email Address\n        </div>\n        <div class=\"email\">\n          <img src=\"assets/imgs/profile.png\" class=\"img_size\">Your Google Profile\n        </div>\n      </div>\n    </div>\n\n    <div class=\"lower\">\n      <ion-button expand=\"full\" class=\"btn_1\">\n        CONTINUE\n      </ion-button>\n      <ion-button expand=\"full\" fill=\"clear\" color=\"light\" (click)=\"modalClose()\">\n        CANCEL\n      </ion-button>\n    </div>\n  </div>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/pages/join-with-google/join-with-google-routing.module.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/pages/join-with-google/join-with-google-routing.module.ts ***!
    \***************************************************************************/

  /*! exports provided: JoinWithGooglePageRoutingModule */

  /***/
  function srcAppPagesJoinWithGoogleJoinWithGoogleRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "JoinWithGooglePageRoutingModule", function () {
      return JoinWithGooglePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _join_with_google_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./join-with-google.page */
    "./src/app/pages/join-with-google/join-with-google.page.ts");

    var routes = [{
      path: '',
      component: _join_with_google_page__WEBPACK_IMPORTED_MODULE_3__["JoinWithGooglePage"]
    }];

    var JoinWithGooglePageRoutingModule = function JoinWithGooglePageRoutingModule() {
      _classCallCheck(this, JoinWithGooglePageRoutingModule);
    };

    JoinWithGooglePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], JoinWithGooglePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/join-with-google/join-with-google.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/pages/join-with-google/join-with-google.module.ts ***!
    \*******************************************************************/

  /*! exports provided: JoinWithGooglePageModule */

  /***/
  function srcAppPagesJoinWithGoogleJoinWithGoogleModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "JoinWithGooglePageModule", function () {
      return JoinWithGooglePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _join_with_google_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./join-with-google-routing.module */
    "./src/app/pages/join-with-google/join-with-google-routing.module.ts");
    /* harmony import */


    var _join_with_google_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./join-with-google.page */
    "./src/app/pages/join-with-google/join-with-google.page.ts");

    var JoinWithGooglePageModule = function JoinWithGooglePageModule() {
      _classCallCheck(this, JoinWithGooglePageModule);
    };

    JoinWithGooglePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _join_with_google_routing_module__WEBPACK_IMPORTED_MODULE_5__["JoinWithGooglePageRoutingModule"]],
      declarations: [_join_with_google_page__WEBPACK_IMPORTED_MODULE_6__["JoinWithGooglePage"]]
    })], JoinWithGooglePageModule);
    /***/
  },

  /***/
  "./src/app/pages/join-with-google/join-with-google.page.scss":
  /*!*******************************************************************!*\
    !*** ./src/app/pages/join-with-google/join-with-google.page.scss ***!
    \*******************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesJoinWithGoogleJoinWithGooglePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content {\n  --background: #0077B5;\n}\n\n.main_content_div {\n  width: 100%;\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  flex-direction: column;\n  padding: 25px;\n  padding-top: 100px;\n  height: 100vh;\n}\n\n.main_content_div .upper {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  width: 100%;\n  align-items: center;\n  position: relative;\n}\n\n.main_content_div .upper .close_icn {\n  position: absolute;\n  top: -40px;\n  left: 0px;\n}\n\n.main_content_div .upper .round_div {\n  background-color: white;\n  height: 100px;\n  width: 100px;\n  border-radius: 100%;\n  position: relative;\n}\n\n.main_content_div .upper .round_div .google_img {\n  height: 40px;\n  width: 40px;\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n}\n\n.main_content_div .upper .head_lbl {\n  color: white;\n  font-size: 26px;\n  display: block;\n  text-align: center;\n  margin-top: 20px;\n}\n\n.main_content_div .upper .profile_div {\n  margin-top: 50px;\n}\n\n.main_content_div .upper .profile_div .email {\n  display: flex;\n  align-items: center;\n  color: white;\n  margin-top: 20px;\n}\n\n.main_content_div .upper .profile_div .email .img_size {\n  width: 20px;\n  margin-right: 15px;\n}\n\n.main_content_div .lower {\n  width: 100%;\n}\n\n.main_content_div .lower .btn_1 {\n  --background: white;\n  --color: #0077B5;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvam9pbi13aXRoLWdvb2dsZS9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxqb2luLXdpdGgtZ29vZ2xlXFxqb2luLXdpdGgtZ29vZ2xlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvam9pbi13aXRoLWdvb2dsZS9qb2luLXdpdGgtZ29vZ2xlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFBO0FDQ0o7O0FEQ0E7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUNFSjs7QURBSTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUNFUjs7QURBUTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7QUNFWjs7QURFUTtFQUNJLHVCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDQVo7O0FERVk7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSxnQ0FBQTtBQ0FoQjs7QURHUTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNEWjs7QURJUTtFQUNJLGdCQUFBO0FDRlo7O0FER1k7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNEaEI7O0FERWdCO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0FDQXBCOztBRE1JO0VBQ0ksV0FBQTtBQ0pSOztBREtRO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtBQ0haIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvam9pbi13aXRoLWdvb2dsZS9qb2luLXdpdGgtZ29vZ2xlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50e1xuICAgIC0tYmFja2dyb3VuZDogIzAwNzdCNTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2e1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBwYWRkaW5nOiAyNXB4O1xuICAgIHBhZGRpbmctdG9wOiAxMDBweDtcbiAgICBoZWlnaHQ6IDEwMHZoO1xuXG4gICAgLnVwcGVye1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICAgICAgICAuY2xvc2VfaWNue1xuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgdG9wOiAtNDBweDtcbiAgICAgICAgICAgIGxlZnQ6IDBweDtcbiAgICAgICAgfVxuXG4gICAgICAgIFxuICAgICAgICAucm91bmRfZGl2e1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgICAgICAgICBoZWlnaHQ6IDEwMHB4O1xuICAgICAgICAgICAgd2lkdGg6IDEwMHB4O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBcbiAgICAgICAgICAgIC5nb29nbGVfaW1ne1xuICAgICAgICAgICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgICAgICAgICB3aWR0aDogNDBweDtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgbGVmdDogNTAlO1xuICAgICAgICAgICAgICAgIHRvcDogNTAlO1xuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsLTUwJSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmhlYWRfbGJse1xuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgZm9udC1zaXplOiAyNnB4O1xuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICB9XG4gICAgXG4gICAgICAgIC5wcm9maWxlX2RpdntcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgICAgICAgICAuZW1haWx7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICAgICAgICAgIC5pbWdfc2l6ZXtcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMTVweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAubG93ZXJ7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAuYnRuXzF7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgICAgICAgLS1jb2xvcjogIzAwNzdCNTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBcbn0iLCJpb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogIzAwNzdCNTtcbn1cblxuLm1haW5fY29udGVudF9kaXYge1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBwYWRkaW5nOiAyNXB4O1xuICBwYWRkaW5nLXRvcDogMTAwcHg7XG4gIGhlaWdodDogMTAwdmg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXBwZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgd2lkdGg6IDEwMCU7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51cHBlciAuY2xvc2VfaWNuIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IC00MHB4O1xuICBsZWZ0OiAwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXBwZXIgLnJvdW5kX2RpdiB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHB4O1xuICB3aWR0aDogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51cHBlciAucm91bmRfZGl2IC5nb29nbGVfaW1nIHtcbiAgaGVpZ2h0OiA0MHB4O1xuICB3aWR0aDogNDBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1MCU7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51cHBlciAuaGVhZF9sYmwge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC51cHBlciAucHJvZmlsZV9kaXYge1xuICBtYXJnaW4tdG9wOiA1MHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnVwcGVyIC5wcm9maWxlX2RpdiAuZW1haWwge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAudXBwZXIgLnByb2ZpbGVfZGl2IC5lbWFpbCAuaW1nX3NpemUge1xuICB3aWR0aDogMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmxvd2VyIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAubG93ZXIgLmJ0bl8xIHtcbiAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgLS1jb2xvcjogIzAwNzdCNTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/join-with-google/join-with-google.page.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/join-with-google/join-with-google.page.ts ***!
    \*****************************************************************/

  /*! exports provided: JoinWithGooglePage */

  /***/
  function srcAppPagesJoinWithGoogleJoinWithGooglePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "JoinWithGooglePage", function () {
      return JoinWithGooglePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var JoinWithGooglePage = /*#__PURE__*/function () {
      function JoinWithGooglePage(modalCtrl) {
        _classCallCheck(this, JoinWithGooglePage);

        this.modalCtrl = modalCtrl;
      }

      _createClass(JoinWithGooglePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "modalClose",
        value: function modalClose() {
          this.modalCtrl.dismiss();
        }
      }]);

      return JoinWithGooglePage;
    }();

    JoinWithGooglePage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }];
    };

    JoinWithGooglePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-join-with-google',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./join-with-google.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/join-with-google/join-with-google.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./join-with-google.page.scss */
      "./src/app/pages/join-with-google/join-with-google.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])], JoinWithGooglePage);
    /***/
  }
}]);
//# sourceMappingURL=pages-join-with-google-join-with-google-module-es5.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-notification-notification-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/notification/notification.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/notification/notification.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n      <img src=\"assets/imgs/user.png\" class=\"user_image\">\n      <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar>\n      <ion-icon name=\"chatbubbles\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\"></ion-icon>\n    </div>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n\n      <ion-buttons slot=\"start\">\n        <!-- <ion-menu-button color=\"light\"></ion-menu-button> -->\n        <img src=\"assets/imgs/user.png\" class=\"user_image\" (click)=\"openMenu()\">\n      </ion-buttons>\n      <ion-searchbar placeholder=\"Search\" mode=\"ios\" inputmode=\"email\" type=\"decimal\"\n        (ionChange)=\"onSearchChange($event)\" [debounce]=\"250\"></ion-searchbar>\n      <ion-icon name=\"chatbubbles\" (click)=\"goToChatList()\" mode=\"md\" color=\"light\" style=\"font-size: 25px;\"></ion-icon>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n\n    <ion-label class=\"head_lbl\">Recent</ion-label>\n    <div>\n      <div class=\"notification_div\" *ngFor=\"let item of dummyData\" (click)=\"goToPost()\">\n        <div class=\"time_div\">\n          <ion-icon name=\"ellipsis-vertical\"></ion-icon>\n          <ion-label style=\"margin-top: 5px;\">9h</ion-label>\n        </div>\n        <div class=\"flex_div\">\n          <div class=\"back_image\" [style.backgroundImage]=\"'url( '+item.img+' )'\"></div>\n          <div>\n            <ion-label class=\"header_lbl\">What is Lorem Ipsum?</ion-label>\n            <ion-label class=\"desc\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n            </ion-label>\n          </div>\n        </div>\n        <div class=\"btn_div\" *ngIf=\"item.btn_text != ''\">\n          <ion-button fill=\"outline\" size=\"small\">\n            {{item.btn_text}}\n          </ion-button>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/notification/notification-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/notification/notification-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: NotificationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationPageRoutingModule", function() { return NotificationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _notification_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./notification.page */ "./src/app/pages/notification/notification.page.ts");




const routes = [
    {
        path: '',
        component: _notification_page__WEBPACK_IMPORTED_MODULE_3__["NotificationPage"]
    }
];
let NotificationPageRoutingModule = class NotificationPageRoutingModule {
};
NotificationPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NotificationPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/notification/notification.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/notification/notification.module.ts ***!
  \***********************************************************/
/*! exports provided: NotificationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationPageModule", function() { return NotificationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _notification_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./notification-routing.module */ "./src/app/pages/notification/notification-routing.module.ts");
/* harmony import */ var _notification_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notification.page */ "./src/app/pages/notification/notification.page.ts");







let NotificationPageModule = class NotificationPageModule {
};
NotificationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _notification_routing_module__WEBPACK_IMPORTED_MODULE_5__["NotificationPageRoutingModule"]
        ],
        declarations: [_notification_page__WEBPACK_IMPORTED_MODULE_6__["NotificationPage"]]
    })
], NotificationPageModule);



/***/ }),

/***/ "./src/app/pages/notification/notification.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/notification/notification.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\nion-content {\n  --background: #F5F5F5;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div .head_lbl {\n  padding: 10px 16px;\n  border-bottom: 1px solid lightgray;\n}\n\n.main_content_div .notification_div {\n  padding: 15px 10px;\n  border-bottom: 1px solid lightgray;\n  position: relative;\n  background: white;\n}\n\n.main_content_div .notification_div .time_div {\n  position: absolute;\n  right: 10px;\n  top: 5px;\n  color: gray;\n}\n\n.main_content_div .notification_div .flex_div {\n  display: flex;\n  width: 100%;\n  align-items: center;\n}\n\n.main_content_div .notification_div .flex_div .back_image {\n  height: 50px;\n  width: 50px;\n  min-width: 50px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.main_content_div .notification_div .flex_div .header_lbl {\n  font-weight: 600;\n  margin-left: 10px;\n  font-size: 14px;\n  width: 88%;\n}\n\n.main_content_div .notification_div .flex_div .desc {\n  margin-left: 10px;\n  font-size: 14px;\n  width: 88%;\n}\n\n.main_content_div .notification_div .btn_div {\n  padding-left: 60px;\n  margin-top: 5px;\n}\n\n.main_content_div .notification_div .btn_div ion-button {\n  font-weight: 600;\n  --border-radius: 3px;\n  --border-color: var(--ion-color-main);\n  --color: var(--ion-color-main);\n  font-size: 16px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbm90aWZpY2F0aW9uL0Q6XFxSYWd1dmFyYW4gaW1hZ2VcXGRpc2Vsc2hpcC9zcmNcXGFwcFxccGFnZXNcXG5vdGlmaWNhdGlvblxcbm90aWZpY2F0aW9uLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQ0FBQTtBQ0NKOztBRENBO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtBQ0VKOztBRENBO0VBQ0kscUJBQUE7QUNFSjs7QURFSTtFQUNJLGNBQUE7QUNDUjs7QURDSTtFQUNJLGtCQUFBO0VBQ0Esa0NBQUE7QUNDUjs7QURFSTtFQUNJLGtCQUFBO0VBQ0Esa0NBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDQVI7O0FERVE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtBQ0FaOztBREVRO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQ0FaOztBRENZO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUNDaEI7O0FERVk7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7QUNBaEI7O0FERVk7RUFDSSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0FDQWhCOztBRElRO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0FDRlo7O0FESVk7RUFDSSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EscUNBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7QUNGaEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZCA6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cbmlvbi10b29sYmFyLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgsIGlvbi10b29sYmFyIC5zYy1pb24tc2VhcmNoYmFyLWlvcy1oe1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcbn1cblxuaW9uLWNvbnRlbnR7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRjVGNUY1O1xufVxuXG4ubWFpbl9jb250ZW50X2RpdntcbiAgICBpb24tbGFiZWwge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG4gICAgLmhlYWRfbGJse1xuICAgICAgICBwYWRkaW5nOiAxMHB4IDE2cHg7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgfVxuXG4gICAgLm5vdGlmaWNhdGlvbl9kaXZ7XG4gICAgICAgIHBhZGRpbmc6IDE1cHggMTBweDtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgICAgIFxuICAgICAgICAudGltZV9kaXZ7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICByaWdodDogMTBweDtcbiAgICAgICAgICAgIHRvcDogNXB4O1xuICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgIH1cbiAgICAgICAgLmZsZXhfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIC5iYWNrX2ltYWdle1xuICAgICAgICAgICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgICAgICAgICB3aWR0aDogNTBweDtcbiAgICAgICAgICAgICAgICBtaW4td2lkdGg6IDUwcHg7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5oZWFkZXJfbGJse1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiA4OCU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuZGVzY3tcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDg4JTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5idG5fZGl2e1xuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA2MHB4O1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuXG4gICAgICAgICAgICBpb24tYnV0dG9ue1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgICAgICAgICAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICAgICAgICAgICAgICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbn0iLCJpb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuXG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCB7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjRjVGNUY1O1xufVxuXG4ubWFpbl9jb250ZW50X2RpdiBpb24tbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5oZWFkX2xibCB7XG4gIHBhZGRpbmc6IDEwcHggMTZweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5ub3RpZmljYXRpb25fZGl2IHtcbiAgcGFkZGluZzogMTVweCAxMHB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuLm1haW5fY29udGVudF9kaXYgLm5vdGlmaWNhdGlvbl9kaXYgLnRpbWVfZGl2IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTBweDtcbiAgdG9wOiA1cHg7XG4gIGNvbG9yOiBncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLm5vdGlmaWNhdGlvbl9kaXYgLmZsZXhfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAubm90aWZpY2F0aW9uX2RpdiAuZmxleF9kaXYgLmJhY2tfaW1hZ2Uge1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiA1MHB4O1xuICBtaW4td2lkdGg6IDUwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuLm1haW5fY29udGVudF9kaXYgLm5vdGlmaWNhdGlvbl9kaXYgLmZsZXhfZGl2IC5oZWFkZXJfbGJsIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgd2lkdGg6IDg4JTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5ub3RpZmljYXRpb25fZGl2IC5mbGV4X2RpdiAuZGVzYyB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIHdpZHRoOiA4OCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAubm90aWZpY2F0aW9uX2RpdiAuYnRuX2RpdiB7XG4gIHBhZGRpbmctbGVmdDogNjBweDtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLm5vdGlmaWNhdGlvbl9kaXYgLmJ0bl9kaXYgaW9uLWJ1dHRvbiB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIC0tYm9yZGVyLXJhZGl1czogM3B4O1xuICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/notification/notification.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/notification/notification.page.ts ***!
  \*********************************************************/
/*! exports provided: NotificationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationPage", function() { return NotificationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/dummy-data.service */ "./src/app/services/dummy-data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/native-page-transitions/ngx */ "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/





let NotificationPage = class NotificationPage {
    constructor(navCtrl, nativePageTransitions, dummy, router, menuCtrl) {
        this.navCtrl = navCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.dummy = dummy;
        this.router = router;
        this.menuCtrl = menuCtrl;
        this.options = {
            direction: 'up',
            duration: 500,
            slowdownfactor: 3,
            slidePixels: 20,
            iosdelay: 100,
            androiddelay: 150,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 60
        };
        this.dummyData = this.dummy.users;
    }
    ionViewWillLeave() {
        this.nativePageTransitions.slide(this.options);
    }
    // example of adding a transition when pushing a new page
    openPage(page) {
        this.nativePageTransitions.slide(this.options);
        this.navCtrl.navigateForward(page);
    }
    ngOnInit() {
    }
    goToPost() {
        this.router.navigate(['/post']);
    }
    openMenu() {
        this.menuCtrl.open();
    }
    onSearchChange(event) {
    }
    goToChatList() {
        this.router.navigate(['/chatlist']);
    }
};
NotificationPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_4__["NativePageTransitions"] },
    { type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__["dummyDataService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"] }
];
NotificationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-notification',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./notification.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/notification/notification.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./notification.page.scss */ "./src/app/pages/notification/notification.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"],
        _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_4__["NativePageTransitions"], src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_2__["dummyDataService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"]])
], NotificationPage);



/***/ }),

/***/ "./src/app/services/dummy-data.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/dummy-data.service.ts ***!
  \************************************************/
/*! exports provided: dummyDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dummyDataService", function() { return dummyDataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

let dummyDataService = class dummyDataService {
    constructor() {
        this.users = [
            {
                img: 'assets/imgs/user.jpg',
                cover: 'assets/imgs/back1.jpg',
                btn_text: 'VIEW JOBS'
            },
            {
                img: 'assets/imgs/user2.jpg',
                cover: 'assets/imgs/back2.jpg',
                btn_text: 'FOLLOW HASHTAG'
            },
            {
                img: 'assets/imgs/user3.jpg',
                cover: 'assets/imgs/back3.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user4.jpg',
                cover: 'assets/imgs/back4.jpg',
                btn_text: 'SEE ALL VIEW'
            },
            {
                img: 'assets/imgs/user.jpg',
                cover: 'assets/imgs/back1.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user2.jpg',
                cover: 'assets/imgs/back2.jpg',
                btn_text: ''
            },
            {
                img: 'assets/imgs/user3.jpg',
                cover: 'assets/imgs/back3.jpg',
                btn_text: 'EXPAND YOUR NETWORK'
            },
            {
                img: 'assets/imgs/user4.jpg',
                cover: 'assets/imgs/back4.jpg',
                btn_text: 'SEE ALL VIEW'
            }
        ];
        this.jobs = [
            {
                img: 'assets/imgs/company/initappz.png',
                title: 'SQL Server',
                addr: 'Bhavnagar, Gujrat, India',
                time: '2 school alumni',
                status: '1 day'
            },
            {
                img: 'assets/imgs/company/google.png',
                title: 'Web Designer',
                addr: 'Vadodara, Gujrat, India',
                time: 'Be an early applicant',
                status: '2 days'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                title: 'Business Analyst',
                addr: 'Ahmedabad, Gujrat, India',
                time: 'Be an early applicant',
                status: '1 week'
            },
            {
                img: 'assets/imgs/company/initappz.png',
                title: 'PHP Developer',
                addr: 'Pune, Maharashtra, India',
                time: '2 school alumni',
                status: 'New'
            },
            {
                img: 'assets/imgs/company/google.png',
                title: 'Graphics Designer',
                addr: 'Bhavnagar, Gujrat, India',
                time: 'Be an early applicant',
                status: '1 day'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                title: 'Phython Developer',
                addr: 'Ahmedabad, Gujrat, India',
                time: '2 school alumni',
                status: '5 days'
            },
        ];
        this.company = [
            {
                img: 'assets/imgs/company/initappz.png',
                name: 'Initappz Shop'
            },
            {
                img: 'assets/imgs/company/microsoft.png',
                name: 'Microsoft'
            },
            {
                img: 'assets/imgs/company/google.png',
                name: 'Google'
            },
        ];
    }
};
dummyDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], dummyDataService);



/***/ })

}]);
//# sourceMappingURL=pages-notification-notification-module-es2015.js.map
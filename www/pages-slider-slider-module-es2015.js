(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-slider-slider-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/slider/slider.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/slider/slider.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <div class=\"header_div\">\n      <ion-label class=\"header_lbl\">Diselship Job</ion-label>\n      <img class=\"logo_img\" src=\"assets/imgs/linkedin.png\">\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n    <div class=\"slider_div\">\n\n      <ion-slides pager=\"true\" [options]=\"slideoptions\">\n\n        <ion-slide>\n          <div class=\"inner_slider\">\n            <ion-label class=\"slider_lbl\">A more personal way to find a jobs</ion-label>\n            <img class=\"slider_img\" src=\"assets/imgs/network.png\">\n          </div>\n        </ion-slide>\n\n        <ion-slide>\n          <div class=\"inner_slider\">\n            <ion-label class=\"slider_lbl\">Build a relationship to unlock opportunity</ion-label>\n            <img class=\"slider_img\" src=\"assets/imgs/network.png\">\n          </div>\n        </ion-slide>\n\n        <ion-slide>\n          <div class=\"inner_slider\">\n            <ion-label class=\"slider_lbl\">Share your professional profile</ion-label>\n            <img class=\"slider_img\" src=\"assets/imgs/network.png\">\n          </div>\n        </ion-slide>\n\n        <ion-slide>\n          <div class=\"inner_slider\">\n            <ion-label class=\"slider_lbl\">Stay informed on topic , industries and people <br> you care about</ion-label>\n            <img class=\"slider_img\" src=\"assets/imgs/network.png\">\n          </div>\n        </ion-slide>\n\n      </ion-slides>\n\n    </div>\n\n\n    <div class=\"btn_div\">\n      <ion-button class=\"join_btn\" expand=\"full\" (click)=\"goToRegister()\">\n        <img class=\"btn_search\" src=\"assets/imgs/google.png\">JOIN WITH GOOGLE\n      </ion-button>\n      <ion-button class=\"join_now\" expand=\"full\" (click)=\"goToRegister()\">\n        JOIN NOW\n      </ion-button>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/slider/slider-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/slider/slider-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: SliderPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SliderPageRoutingModule", function() { return SliderPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _slider_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./slider.page */ "./src/app/pages/slider/slider.page.ts");




const routes = [
    {
        path: '',
        component: _slider_page__WEBPACK_IMPORTED_MODULE_3__["SliderPage"]
    }
];
let SliderPageRoutingModule = class SliderPageRoutingModule {
};
SliderPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SliderPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/slider/slider.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/slider/slider.module.ts ***!
  \***********************************************/
/*! exports provided: SliderPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SliderPageModule", function() { return SliderPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _slider_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./slider-routing.module */ "./src/app/pages/slider/slider-routing.module.ts");
/* harmony import */ var _slider_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./slider.page */ "./src/app/pages/slider/slider.page.ts");







let SliderPageModule = class SliderPageModule {
};
SliderPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _slider_routing_module__WEBPACK_IMPORTED_MODULE_5__["SliderPageRoutingModule"]
        ],
        declarations: [_slider_page__WEBPACK_IMPORTED_MODULE_6__["SliderPage"]]
    })
], SliderPageModule);



/***/ }),

/***/ "./src/app/pages/slider/slider.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/slider/slider.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: #283E4A ;\n}\n\n.header_div {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.header_div .header_lbl {\n  color: white;\n  font-weight: bold;\n  font-size: 17px;\n}\n\n.header_div .logo_img {\n  width: 20px;\n  height: 20px;\n}\n\n.main_content_div {\n  width: 100%;\n  height: 100vh;\n  background-color: rgba(87, 129, 150, 0.8);\n}\n\n.main_content_div .slider_div ion-slide {\n  height: 60vh;\n}\n\n.main_content_div .slider_div ion-slide .inner_slider {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.main_content_div .slider_div ion-slide .inner_slider .slider_lbl {\n  margin-top: 50px;\n  color: white;\n  display: block;\n  font-size: 16px;\n}\n\n.main_content_div .slider_div ion-slide .inner_slider .slider_img {\n  margin-top: 50px;\n  height: 200px;\n  width: 200px;\n}\n\n.main_content_div .btn_div {\n  width: 100%;\n  padding-left: 20px;\n  padding-right: 20px;\n}\n\n.main_content_div .btn_div .join_btn {\n  --background: white;\n  --color: #0077B5;\n  --border-radius: 10px;\n}\n\n.main_content_div .btn_div .join_now {\n  margin-top: 10px;\n  color: white;\n  border: 2px solid white;\n  --background: transparent;\n}\n\n.main_content_div .btn_div .btn_search {\n  height: 25px;\n  width: 25px;\n  margin-right: 20px;\n}\n\nion-slides {\n  --bullet-background: white;\n  --bullet-background-active: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2xpZGVyL0Q6XFxSYWd1dmFyYW4gaW1hZ2VcXGRpc2Vsc2hpcC9zcmNcXGFwcFxccGFnZXNcXHNsaWRlclxcc2xpZGVyLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvc2xpZGVyL3NsaWRlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxzQkFBQTtBQ0NKOztBRENBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUNFSjs7QURESTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNHUjs7QURESTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDR1I7O0FEQ0E7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHlDQUFBO0FDRUo7O0FEQ1E7RUFDSSxZQUFBO0FDQ1o7O0FEQ1k7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDQ2hCOztBREFnQjtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDRXBCOztBREFnQjtFQUNJLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7QUNFcEI7O0FER0k7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ0RSOztBREVRO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0FDQVo7O0FERVE7RUFDSSxnQkFBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLHlCQUFBO0FDQVo7O0FERVE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDQVo7O0FES0E7RUFDSSwwQkFBQTtFQUNBLGlDQUFBO0FDRkoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9zbGlkZXIvc2xpZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZCA6ICMyODNFNEFcbn1cbi5oZWFkZXJfZGl2e1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAuaGVhZGVyX2xibHtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgZm9udC1zaXplOiAxN3B4O1xuICAgIH1cbiAgICAubG9nb19pbWd7XG4gICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICBoZWlnaHQ6IDIwcHg7XG4gICAgfVxufVxuXG4ubWFpbl9jb250ZW50X2RpdntcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMHZoOztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDg3LCAxMjksIDE1MCwgMC44KTtcblxuICAgIC5zbGlkZXJfZGl2e1xuICAgICAgICBpb24tc2xpZGV7XG4gICAgICAgICAgICBoZWlnaHQ6IDYwdmg7XG5cbiAgICAgICAgICAgIC5pbm5lcl9zbGlkZXJ7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgLnNsaWRlcl9sYmx7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLnNsaWRlcl9pbWd7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMjAwcHg7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAyMDBweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgLmJ0bl9kaXZ7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gICAgICAgIC5qb2luX2J0bntcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZCA6IHdoaXRlO1xuICAgICAgICAgICAgLS1jb2xvcjogIzAwNzdCNTtcbiAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgfVxuICAgICAgICAuam9pbl9ub3d7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgYm9yZGVyOiAycHggc29saWQgd2hpdGU7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQgOiB0cmFuc3BhcmVudDtcbiAgICAgICAgfVxuICAgICAgICAuYnRuX3NlYXJjaHtcbiAgICAgICAgICAgIGhlaWdodDogMjVweDtcbiAgICAgICAgICAgIHdpZHRoOiAyNXB4O1xuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xuICAgICAgICB9XG4gICAgfVxufVxuXG5pb24tc2xpZGVze1xuICAgIC0tYnVsbGV0LWJhY2tncm91bmQgOiB3aGl0ZTtcbiAgICAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZTogd2hpdGU7XG59XG4iLCJpb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogIzI4M0U0QSA7XG59XG5cbi5oZWFkZXJfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4uaGVhZGVyX2RpdiAuaGVhZGVyX2xibCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTdweDtcbn1cbi5oZWFkZXJfZGl2IC5sb2dvX2ltZyB7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwdmg7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoODcsIDEyOSwgMTUwLCAwLjgpO1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYgaW9uLXNsaWRlIHtcbiAgaGVpZ2h0OiA2MHZoO1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYgaW9uLXNsaWRlIC5pbm5lcl9zbGlkZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5zbGlkZXJfZGl2IGlvbi1zbGlkZSAuaW5uZXJfc2xpZGVyIC5zbGlkZXJfbGJsIHtcbiAgbWFyZ2luLXRvcDogNTBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLnNsaWRlcl9kaXYgaW9uLXNsaWRlIC5pbm5lcl9zbGlkZXIgLnNsaWRlcl9pbWcge1xuICBtYXJnaW4tdG9wOiA1MHB4O1xuICBoZWlnaHQ6IDIwMHB4O1xuICB3aWR0aDogMjAwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuYnRuX2RpdiB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuYnRuX2RpdiAuam9pbl9idG4ge1xuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLWNvbG9yOiAjMDA3N0I1O1xuICAtLWJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuYnRuX2RpdiAuam9pbl9ub3cge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlcjogMnB4IHNvbGlkIHdoaXRlO1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuLm1haW5fY29udGVudF9kaXYgLmJ0bl9kaXYgLmJ0bl9zZWFyY2gge1xuICBoZWlnaHQ6IDI1cHg7XG4gIHdpZHRoOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG5cbmlvbi1zbGlkZXMge1xuICAtLWJ1bGxldC1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgLS1idWxsZXQtYmFja2dyb3VuZC1hY3RpdmU6IHdoaXRlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/slider/slider.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/slider/slider.page.ts ***!
  \*********************************************/
/*! exports provided: SliderPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SliderPage", function() { return SliderPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/


let SliderPage = class SliderPage {
    constructor(router) {
        this.router = router;
        this.slideoptions = {
            slidesPerView: 1,
        };
    }
    ngOnInit() {
    }
    goToRegister() {
        this.router.navigate(['/register']);
    }
};
SliderPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
SliderPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-slider',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./slider.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/slider/slider.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./slider.page.scss */ "./src/app/pages/slider/slider.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], SliderPage);



/***/ })

}]);
//# sourceMappingURL=pages-slider-slider-module-es2015.js.map
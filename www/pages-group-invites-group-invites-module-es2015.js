(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-group-invites-group-invites-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/group-invites/group-invites.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/group-invites/group-invites.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <div class=\"div_header\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button mode=\"md\" color=\"light\"></ion-back-button>\n    </ion-buttons>\n    <ion-segment (ionChange)=\"segmentChanged($event)\" mode=\"md\">\n      <ion-segment-button>\n        <ion-label>Pending</ion-label>\n      </ion-segment-button>\n      <ion-segment-button>\n        <ion-label>Sent</ion-label>\n      </ion-segment-button>\n    </ion-segment>\n\n    <!-- <ion-button fill=\"clear\" size=\"small\">\n      <ion-icon name=\"settings-outline\" color=\"light\" style=\"font-size: 22px;\"></ion-icon>\n    </ion-button> -->\n  </div>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n\n    <span *ngIf=\"segId == 'ion-sb-0'\">\n     \n\n   \n      <div class=\"flex_div\" *ngFor=\"let item of members\">\n       \n        <div class=\"f_div\">\n          <div class=\"back_image\" [style.backgroundImage]=\"'url('+item.avatar_urls.thumb+')'\"></div>\n          <div class=\"detail\">\n            <ion-label class=\"name\">{{item.profile_name}}</ion-label>\n            <ion-label class=\"desc\">{{item.followers}} Followers</ion-label>\n          </div>\n        </div>\n        <div class=\"s_div\">\n         \n            <!-- <ion-icon (click)=\"invite(item.id)\" name=\"person-add-outline\" mode=\"md\"></ion-icon> -->\n           \n        \n        </div>      \n      </div>\n\n    </span>\n\n    <span *ngIf=\"segId == 'ion-sb-1'\">\n    \n      <div class=\"flex_div\" *ngFor=\"let item of members\">\n       \n        <div class=\"f_div\">\n          <div class=\"back_image\" [style.backgroundImage]=\"'url('+item.avatar_urls.thumb+')'\"></div>\n          <div class=\"detail\">\n            <ion-label class=\"name\">{{item.profile_name}}</ion-label>\n            <ion-label class=\"desc\">{{item.followers}} Followers</ion-label>\n          </div>\n        </div>\n        <div class=\"s_div\">\n          <div class=\"menu_div\"  >\n            <ion-icon (click)=\"invite(item.id)\" style=\"font-size: 28px;\n            padding: 2px;\" name=\"person-add-outline\" mode=\"md\"></ion-icon>\n           \n          </div>\n        </div>      \n      </div>\n\n    </span>\n\n    \n    \n  </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/group-invites/group-invites-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/group-invites/group-invites-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: GroupInvitesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupInvitesPageRoutingModule", function() { return GroupInvitesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _group_invites_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./group-invites.page */ "./src/app/pages/group-invites/group-invites.page.ts");




const routes = [
    {
        path: '',
        component: _group_invites_page__WEBPACK_IMPORTED_MODULE_3__["GroupInvitesPage"]
    }
];
let GroupInvitesPageRoutingModule = class GroupInvitesPageRoutingModule {
};
GroupInvitesPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], GroupInvitesPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/group-invites/group-invites.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/group-invites/group-invites.module.ts ***!
  \*************************************************************/
/*! exports provided: GroupInvitesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupInvitesPageModule", function() { return GroupInvitesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _group_invites_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./group-invites-routing.module */ "./src/app/pages/group-invites/group-invites-routing.module.ts");
/* harmony import */ var _group_invites_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./group-invites.page */ "./src/app/pages/group-invites/group-invites.page.ts");







let GroupInvitesPageModule = class GroupInvitesPageModule {
};
GroupInvitesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _group_invites_routing_module__WEBPACK_IMPORTED_MODULE_5__["GroupInvitesPageRoutingModule"]
        ],
        declarations: [_group_invites_page__WEBPACK_IMPORTED_MODULE_6__["GroupInvitesPage"]]
    })
], GroupInvitesPageModule);



/***/ }),

/***/ "./src/app/pages/group-invites/group-invites.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/group-invites/group-invites.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\n.div_header {\n  display: flex;\n  height: 50px;\n  justify-content: space-between;\n  background: var(--ion-color-main);\n  align-items: center;\n}\n\n.div_header ion-segment-button {\n  --color-checked: white;\n  color: white;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div ion-label {\n  display: block;\n}\n\n.main_content_div .chip_div {\n  padding: 10px 16px;\n}\n\n.main_content_div .chip_div ion-chip {\n  font-weight: 600;\n  color: #505050;\n  background: #E1E9EE;\n}\n\n.main_content_div .chip_div .active_chip {\n  color: white;\n  background: var(--ion-color-main);\n}\n\n.main_content_div .flex_div {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  justify-content: space-between;\n  padding-top: 10px;\n  padding-bottom: 15px;\n  border-bottom: 1px solid lightgray;\n  background: white;\n}\n\n.main_content_div .flex_div .f_div {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding-left: 16px;\n}\n\n.main_content_div .flex_div .f_div .detail {\n  margin-left: 10px;\n}\n\n.main_content_div .flex_div .f_div .detail .desc {\n  color: gray;\n}\n\n.main_content_div .flex_div .f_div .back_image {\n  height: 60px;\n  width: 60px;\n  min-width: 60px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.main_content_div .flex_div .s_div {\n  display: flex;\n  align-items: center;\n  padding-right: 16px;\n}\n\n.main_content_div .flex_div .s_div ion-icon {\n  font-size: 40px;\n}\n\n.main_content_div .flex_div .s_div .cross {\n  color: gray;\n}\n\n.main_content_div .flex_div .s_div .check {\n  color: var(--ion-color-main);\n}\n\n.main_content_div .menu_div {\n  height: 40px;\n  width: 40px;\n  border-radius: 50%;\n  position: relative;\n}\n\n.main_content_div .menu_div ion-icon {\n  font-size: 27px;\n  padding: 2px;\n  color: var(--ion-color-main);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZ3JvdXAtaW52aXRlcy9EOlxcUmFndXZhcmFuIGltYWdlXFxkaXNlbHNoaXAvc3JjXFxhcHBcXHBhZ2VzXFxncm91cC1pbnZpdGVzXFxncm91cC1pbnZpdGVzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZ3JvdXAtaW52aXRlcy9ncm91cC1pbnZpdGVzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1DQUFBO0FDQ0o7O0FEQ0E7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0FDRUo7O0FEQ0E7RUFDSSxhQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0VBQ0EsaUNBQUE7RUFDQSxtQkFBQTtBQ0VKOztBREFJO0VBQ0ksc0JBQUE7RUFDQSxZQUFBO0FDRVI7O0FEQ0E7RUFDSSxXQUFBO0FDRUo7O0FEQUk7RUFDSSxjQUFBO0FDRVI7O0FEQ0k7RUFDSSxrQkFBQTtBQ0NSOztBRENRO0VBQ0ksZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7QUNDWjs7QURFUTtFQUNFLFlBQUE7RUFDQSxpQ0FBQTtBQ0FWOztBRElJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtDQUFBO0VBQ0EsaUJBQUE7QUNGUjs7QURLUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUNIWjs7QURLWTtFQUNJLGlCQUFBO0FDSGhCOztBRElnQjtFQUNJLFdBQUE7QUNGcEI7O0FES1k7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQ0hoQjs7QURPUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDTFo7O0FETVk7RUFDSSxlQUFBO0FDSmhCOztBRE9ZO0VBQ0ksV0FBQTtBQ0xoQjs7QURPWTtFQUNJLDRCQUFBO0FDTGhCOztBRFNJO0VBQ0ksWUFBQTtFQUNKLFdBQUE7RUFDQSxrQkFBQTtFQUVBLGtCQUFBO0FDUko7O0FEV1E7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLDRCQUFBO0FDVFoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ncm91cC1pbnZpdGVzL2dyb3VwLWludml0ZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWh7XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG4uZGl2X2hlYWRlcntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGhlaWdodDogNTBweDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cbiAgICBpb24tc2VnbWVudC1idXR0b257XG4gICAgICAgIC0tY29sb3ItY2hlY2tlZDogd2hpdGU7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB9XG59XG4ubWFpbl9jb250ZW50X2RpdntcbiAgICB3aWR0aDogMTAwJTtcblxuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIH1cblxuICAgIC5jaGlwX2RpdntcbiAgICAgICAgcGFkZGluZzogMTBweCAxNnB4O1xuXG4gICAgICAgIGlvbi1jaGlwe1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIGNvbG9yOiAjNTA1MDUwO1xuICAgICAgICAgICAgYmFja2dyb3VuZDogI0UxRTlFRTtcbiAgICAgICAgfVxuXG4gICAgICAgIC5hY3RpdmVfY2hpcHtcbiAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmZsZXhfZGl2e1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuXG5cbiAgICAgICAgLmZfZGl2e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuXG4gICAgICAgICAgICAuZGV0YWlse1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgIC5kZXNje1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjpncmF5O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5iYWNrX2ltYWdle1xuICAgICAgICAgICAgICAgIGhlaWdodDogNjBweDtcbiAgICAgICAgICAgICAgICB3aWR0aDogNjBweDtcbiAgICAgICAgICAgICAgICBtaW4td2lkdGg6IDYwcHg7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAuc19kaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgICAgICAgICBpb24taWNvbntcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDQwcHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5jcm9zc3tcbiAgICAgICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5jaGVja3tcbiAgICAgICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIC5tZW51X2RpdntcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgIHdpZHRoOiA0MHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgLy8gIGJvcmRlcjogMnB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1tYWluKTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cblxuICAgICAgICBpb24taWNvbntcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjdweDtcbiAgICAgICAgICAgIHBhZGRpbmc6IDJweDtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn1cblxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWgge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG4uZGl2X2hlYWRlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGhlaWdodDogNTBweDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWFpbik7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uZGl2X2hlYWRlciBpb24tc2VnbWVudC1idXR0b24ge1xuICAtLWNvbG9yLWNoZWNrZWQ6IHdoaXRlO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiBpb24tbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jaGlwX2RpdiB7XG4gIHBhZGRpbmc6IDEwcHggMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5jaGlwX2RpdiBpb24tY2hpcCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjNTA1MDUwO1xuICBiYWNrZ3JvdW5kOiAjRTFFOUVFO1xufVxuLm1haW5fY29udGVudF9kaXYgLmNoaXBfZGl2IC5hY3RpdmVfY2hpcCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgd2lkdGg6IDEwMCU7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuZl9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLmZfZGl2IC5kZXRhaWwge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuZl9kaXYgLmRldGFpbCAuZGVzYyB7XG4gIGNvbG9yOiBncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5mX2RpdiAuYmFja19pbWFnZSB7XG4gIGhlaWdodDogNjBweDtcbiAgd2lkdGg6IDYwcHg7XG4gIG1pbi13aWR0aDogNjBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLnNfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5mbGV4X2RpdiAuc19kaXYgaW9uLWljb24ge1xuICBmb250LXNpemU6IDQwcHg7XG59XG4ubWFpbl9jb250ZW50X2RpdiAuZmxleF9kaXYgLnNfZGl2IC5jcm9zcyB7XG4gIGNvbG9yOiBncmF5O1xufVxuLm1haW5fY29udGVudF9kaXYgLmZsZXhfZGl2IC5zX2RpdiAuY2hlY2sge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuLm1haW5fY29udGVudF9kaXYgLm1lbnVfZGl2IHtcbiAgaGVpZ2h0OiA0MHB4O1xuICB3aWR0aDogNDBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAubWVudV9kaXYgaW9uLWljb24ge1xuICBmb250LXNpemU6IDI3cHg7XG4gIHBhZGRpbmc6IDJweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tYWluKTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/group-invites/group-invites.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/group-invites/group-invites.page.ts ***!
  \***********************************************************/
/*! exports provided: GroupInvitesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupInvitesPage", function() { return GroupInvitesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/native-page-transitions/ngx */ "./node_modules/@ionic-native/native-page-transitions/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/






let GroupInvitesPage = class GroupInvitesPage {
    constructor(navCtrl, toastCtrl, nativePageTransitions, router, location, route, api) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.nativePageTransitions = nativePageTransitions;
        this.router = router;
        this.location = location;
        this.route = route;
        this.api = api;
        this.data = {};
        this.members = {};
        this.creator = {};
        this.options = {
            direction: 'up',
            duration: 500,
            slowdownfactor: 3,
            slidePixels: 20,
            iosdelay: 100,
            androiddelay: 150,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 60
        };
    }
    // gotoGroupFeed() {
    //   this.router.navigate(['/group-feed']);
    // }
    ionViewWillLeave() {
        this.nativePageTransitions.slide(this.options);
    }
    segmentChanged(eve) {
        console.log(eve.detail.value);
        this.segId = eve.detail.value;
        if (this.segId == 'ion-sb-0') {
            this.api.getgroupMembers(this.id1, "invited").subscribe(res => {
                console.log(res);
                this.members = res;
            });
        }
        else {
            this.api.getgroupMembers(this.id1, "invite").subscribe(res => {
                console.log(res);
                this.members = res;
            });
        }
    }
    invite(id) {
        this.api.Groupinvite(this.id1, id).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(res);
            const toast = yield this.toastCtrl.create({
                message: "Invite Sent Successfully",
                duration: 3000
            });
            yield toast.present();
            this.api.getgroupMembers(this.id1, "invite").subscribe(res => {
                console.log(res);
                this.members = res;
            });
        }));
    }
    // example of adding a transition when pushing a new page
    openPage(page) {
        this.nativePageTransitions.slide(this.options);
        this.navCtrl.navigateForward(page);
    }
    ngOnInit() {
        this.id1 = this.route.snapshot.paramMap.get('id');
        this.api.getgroupMembers(this.id1, "invited").subscribe(res => {
            console.log(res);
            this.members = res;
            this.segId = 'ion-sb-0';
        });
    }
    BackButton() {
        this.location.back();
    }
};
GroupInvitesPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"] },
    { type: _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__["NativePageTransitions"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"] }
];
GroupInvitesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-group-invites',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./group-invites.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/group-invites/group-invites.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./group-invites.page.scss */ "./src/app/pages/group-invites/group-invites.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"],
        _ionic_native_native_page_transitions_ngx__WEBPACK_IMPORTED_MODULE_5__["NativePageTransitions"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services_api_service__WEBPACK_IMPORTED_MODULE_3__["apiService"]])
], GroupInvitesPage);



/***/ })

}]);
//# sourceMappingURL=pages-group-invites-group-invites-module-es2015.js.map
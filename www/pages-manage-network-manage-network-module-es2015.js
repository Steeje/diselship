(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-manage-network-manage-network-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/manage-network/manage-network.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/manage-network/manage-network.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button color=\"light\" mode=\"md\"></ion-back-button>\n    </ion-buttons>\n    <ion-title color=\"light\">Manage my Network</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main_content_div\">\n\n    <div class=\"list_div\" (click)=\"goToManageDetail('Connections')\">\n      <div class=\"first_div\">\n        <ion-label>Connections (112)</ion-label>\n      </div>\n      <div class=\"second_div\">\n        <div class=\"flex_div\">\n          <div class=\"back_image\" [style.backgroundImage]=\"'url(assets/imgs/user2.jpg)'\"></div>\n          <div class=\"back_image second\" [style.backgroundImage]=\"'url(assets/imgs/user3.jpg)'\"></div>\n          <div class=\"back_image third\" [style.backgroundImage]=\"'url(assets/imgs/user4.jpg)'\"></div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"list_div\">\n      <div class=\"first_div\">\n        <ion-label>+ Add teammates</ion-label>\n      </div>\n      <div class=\"second_div\">\n        <div class=\"flex_div\">\n          <!-- <div class=\"back_image\" [style.backgroundImage]=\"'url(assets/imgs/user2.jpg)'\"></div>\n          <div class=\"back_image second\" [style.backgroundImage]=\"'url(assets/imgs/user3.jpg)'\"></div>\n          <div class=\"back_image third\" [style.backgroundImage]=\"'url(assets/imgs/user4.jpg)'\"></div> -->\n        </div>\n      </div>\n    </div>\n\n    <div class=\"list_div\" (click)=\"goToManageDetail('Following')\">\n      <div class=\"first_div\">\n        <ion-label>People I follow (7)</ion-label>\n      </div>\n      <div class=\"second_div\">\n        <div class=\"flex_div\">\n          <div class=\"back_image\" [style.backgroundImage]=\"'url(assets/imgs/user2.jpg)'\"></div>\n          <div class=\"back_image second\" [style.backgroundImage]=\"'url(assets/imgs/user3.jpg)'\"></div>\n          <div class=\"back_image third\" [style.backgroundImage]=\"'url(assets/imgs/user4.jpg)'\"></div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"list_div\" (click)=\"goToManageDetail('Pages')\">\n      <div class=\"first_div\">\n        <ion-label>Pages (17)</ion-label>\n      </div>\n      <div class=\"second_div\">\n        <div class=\"flex_div\">\n          <div class=\"back_image\" [style.backgroundImage]=\"'url(assets/imgs/user2.jpg)'\"></div>\n          <div class=\"back_image second\" [style.backgroundImage]=\"'url(assets/imgs/user3.jpg)'\"></div>\n          <div class=\"back_image third\" [style.backgroundImage]=\"'url(assets/imgs/user4.jpg)'\"></div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"list_div\" (click)=\"goToManageDetail('Hashtag')\">\n      <div class=\"first_div\">\n        <ion-label>Hashtag (3)</ion-label>\n      </div>\n      <div class=\"second_div\">\n        <div class=\"flex_div\">\n          <span>#</span>\n        </div>\n      </div>\n    </div>\n\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/manage-network/manage-network-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/manage-network/manage-network-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: ManageNetworkPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManageNetworkPageRoutingModule", function() { return ManageNetworkPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _manage_network_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./manage-network.page */ "./src/app/pages/manage-network/manage-network.page.ts");




const routes = [
    {
        path: '',
        component: _manage_network_page__WEBPACK_IMPORTED_MODULE_3__["ManageNetworkPage"]
    }
];
let ManageNetworkPageRoutingModule = class ManageNetworkPageRoutingModule {
};
ManageNetworkPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ManageNetworkPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/manage-network/manage-network.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/manage-network/manage-network.module.ts ***!
  \***************************************************************/
/*! exports provided: ManageNetworkPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManageNetworkPageModule", function() { return ManageNetworkPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _manage_network_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./manage-network-routing.module */ "./src/app/pages/manage-network/manage-network-routing.module.ts");
/* harmony import */ var _manage_network_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./manage-network.page */ "./src/app/pages/manage-network/manage-network.page.ts");







let ManageNetworkPageModule = class ManageNetworkPageModule {
};
ManageNetworkPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _manage_network_routing_module__WEBPACK_IMPORTED_MODULE_5__["ManageNetworkPageRoutingModule"]
        ],
        declarations: [_manage_network_page__WEBPACK_IMPORTED_MODULE_6__["ManageNetworkPage"]]
    })
], ManageNetworkPageModule);



/***/ }),

/***/ "./src/app/pages/manage-network/manage-network.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/manage-network/manage-network.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: var(--ion-color-main);\n}\n\nion-toolbar.sc-ion-searchbar-ios-h, ion-toolbar .sc-ion-searchbar-ios-h {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\n.main_content_div {\n  width: 100%;\n}\n\n.main_content_div .list_div {\n  display: flex;\n  width: 100%;\n  justify-content: space-between;\n  padding: 10px 16px;\n  border-bottom: 1px solid lightgray;\n  height: 60px;\n  align-items: center;\n}\n\n.main_content_div .list_div .second_div .flex_div {\n  display: flex;\n}\n\n.main_content_div .list_div .second_div .flex_div .back_image {\n  height: 40px;\n  width: 40px;\n  border-radius: 50%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  border: 4px solid white;\n}\n\n.main_content_div .list_div .second_div .flex_div .second {\n  position: absolute;\n  z-index: 99;\n  margin-left: 20px;\n}\n\n.main_content_div .list_div .second_div .flex_div .third {\n  z-index: 99999;\n}\n\n.main_content_div .list_div .second_div .flex_div span {\n  background: #c0c0c0;\n  color: white;\n  padding: 3px 8px;\n  font-size: 20px;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWFuYWdlLW5ldHdvcmsvRDpcXFJhZ3V2YXJhbiBpbWFnZVxcZGlzZWxzaGlwL3NyY1xcYXBwXFxwYWdlc1xcbWFuYWdlLW5ldHdvcmtcXG1hbmFnZS1uZXR3b3JrLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbWFuYWdlLW5ldHdvcmsvbWFuYWdlLW5ldHdvcmsucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUNBQUE7QUNDSjs7QURDQTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUNFSjs7QURDQTtFQUNJLFdBQUE7QUNFSjs7QURBSTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLGtDQUFBO0VBQ0EsWUFBQTtFQUVBLG1CQUFBO0FDQ1I7O0FEQ1k7RUFDSSxhQUFBO0FDQ2hCOztBRENnQjtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtBQ0NwQjs7QURDZ0I7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtBQ0NwQjs7QURDZ0I7RUFFSSxjQUFBO0FDQXBCOztBRElnQjtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDRnBCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbWFuYWdlLW5ldHdvcmsvbWFuYWdlLW5ldHdvcmsucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kIDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuaW9uLXRvb2xiYXIuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCwgaW9uLXRvb2xiYXIgLnNjLWlvbi1zZWFyY2hiYXItaW9zLWh7XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuXG4ubWFpbl9jb250ZW50X2RpdntcbiAgICB3aWR0aDogMTAwJTtcblxuICAgIC5saXN0X2RpdntcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgcGFkZGluZzogMTBweCAxNnB4O1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgICAgICBoZWlnaHQ6IDYwcHg7XG5cbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgLnNlY29uZF9kaXZ7XG4gICAgICAgICAgICAuZmxleF9kaXZ7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcblxuICAgICAgICAgICAgICAgIC5iYWNrX2ltYWdle1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiA0cHggc29saWQgd2hpdGU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5zZWNvbmR7XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICAgICAgei1pbmRleDogOTk7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAudGhpcmR7XG4gICAgICAgICAgICAgICAgICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICAgICAgei1pbmRleDogOTk5OTk7XG4gICAgICAgICAgICAgICAgICAgIC8vIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICAgICAgICAgICAgICAgICAgICAvLyBtYXJnaW4tbGVmdDogNDBweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgc3BhbntcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2MwYzBjMDtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAzcHggOHB4O1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0iLCJpb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1haW4pO1xufVxuXG5pb24tdG9vbGJhci5zYy1pb24tc2VhcmNoYmFyLWlvcy1oLCBpb24tdG9vbGJhciAuc2MtaW9uLXNlYXJjaGJhci1pb3MtaCB7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5cbi5tYWluX2NvbnRlbnRfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWFpbl9jb250ZW50X2RpdiAubGlzdF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBwYWRkaW5nOiAxMHB4IDE2cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIGhlaWdodDogNjBweDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5saXN0X2RpdiAuc2Vjb25kX2RpdiAuZmxleF9kaXYge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmxpc3RfZGl2IC5zZWNvbmRfZGl2IC5mbGV4X2RpdiAuYmFja19pbWFnZSB7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDQwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBib3JkZXI6IDRweCBzb2xpZCB3aGl0ZTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5saXN0X2RpdiAuc2Vjb25kX2RpdiAuZmxleF9kaXYgLnNlY29uZCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogOTk7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xufVxuLm1haW5fY29udGVudF9kaXYgLmxpc3RfZGl2IC5zZWNvbmRfZGl2IC5mbGV4X2RpdiAudGhpcmQge1xuICB6LWluZGV4OiA5OTk5OTtcbn1cbi5tYWluX2NvbnRlbnRfZGl2IC5saXN0X2RpdiAuc2Vjb25kX2RpdiAuZmxleF9kaXYgc3BhbiB7XG4gIGJhY2tncm91bmQ6ICNjMGMwYzA7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogM3B4IDhweDtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/manage-network/manage-network.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/manage-network/manage-network.page.ts ***!
  \*************************************************************/
/*! exports provided: ManageNetworkPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManageNetworkPage", function() { return ManageNetworkPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/


let ManageNetworkPage = class ManageNetworkPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    goToManageDetail(val) {
        console.log(val);
        const navData = {
            queryParams: {
                id: val
            }
        };
        this.router.navigate(['/manage-detail'], navData);
    }
};
ManageNetworkPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
ManageNetworkPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-manage-network',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./manage-network.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/manage-network/manage-network.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./manage-network.page.scss */ "./src/app/pages/manage-network/manage-network.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], ManageNetworkPage);



/***/ })

}]);
//# sourceMappingURL=pages-manage-network-manage-network-module-es2015.js.map